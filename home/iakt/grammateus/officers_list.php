<?php
//Connection statement
require_once('../Connections/sigma_modx.php');

//Aditional Functions
require_once('../includes/functions.inc.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Filter
$tfi_listrsOfficersList1 = new TFI_TableFilter($sigma_modx, "tfi_listrsOfficersList1");
$tfi_listrsOfficersList1->addColumn("CPOSITION", "STRING_TYPE", "CPOSITION", "%");
$tfi_listrsOfficersList1->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listrsOfficersList1->addColumn("STARTDATE", "DATE_TYPE", "STARTDATE", "=");
$tfi_listrsOfficersList1->addColumn("TERMINATIONDATE", "DATE_TYPE", "TERMINATIONDATE", "=");
$tfi_listrsOfficersList1->Execute();

// Sorter
$tso_listrsOfficersList1 = new TSO_TableSorter("rsOfficersList", "tso_listrsOfficersList1");
$tso_listrsOfficersList1->addColumn("CPOSITION");
$tso_listrsOfficersList1->addColumn("FULLNAME");
$tso_listrsOfficersList1->addColumn("STARTDATE");
$tso_listrsOfficersList1->addColumn("TERMINATIONDATE");
$tso_listrsOfficersList1->setDefault("CPOSITION");
$tso_listrsOfficersList1->Execute();

// Navigation
$nav_listrsOfficersList1 = new NAV_Regular("nav_listrsOfficersList1", "rsOfficersList", "../", $_SERVER['PHP_SELF'], 20);

// Begin List Recordset
$maxRows_rsOfficersList = $_SESSION['max_rows_nav_listrsOfficersList1'];
$pageNum_rsOfficersList = 0;
if (isset($_GET['pageNum_rsOfficersList'])) {
  $pageNum_rsOfficersList = $_GET['pageNum_rsOfficersList'];
}
$startRow_rsOfficersList = $pageNum_rsOfficersList * $maxRows_rsOfficersList;
$colname__rsOfficersList = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsOfficersList = $_SESSION['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter__rsOfficersList = "1=1";
if (isset($_SESSION['filter_tfi_listrsOfficersList1'])) {
  $NXTFilter__rsOfficersList = $_SESSION['filter_tfi_listrsOfficersList1'];
}
// Defining List Recordset variable
$NXTSort__rsOfficersList = "FULLNAME";
if (isset($_SESSION['sorter_tso_listrsOfficersList1'])) {
  $NXTSort__rsOfficersList = $_SESSION['sorter_tso_listrsOfficersList1'];
}
$query_rsOfficersList = sprintf("SELECT * FROM vw_boule_officers WHERE COMMITTEESTATUSSTT = 'Active' AND BOULENAME = %s AND  {$NXTFilter__rsOfficersList} ORDER BY {$NXTSort__rsOfficersList} ASC ", GetSQLValueString($colname__rsOfficersList, "text"));
$rsOfficersList = $sigma_modx->SelectLimit($query_rsOfficersList, $maxRows_rsOfficersList, $startRow_rsOfficersList) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsOfficersList'])) {
  $totalRows_rsOfficersList = $_GET['totalRows_rsOfficersList'];
} else {
  $all_rsOfficersList = $sigma_modx->SelectLimit($query_rsOfficersList) or die($sigma_modx->ErrorMsg());
  $totalRows_rsOfficersList = $all_rsOfficersList->RecordCount();
}
$totalPages_rsOfficersList = (int)(($totalRows_rsOfficersList-1)/$maxRows_rsOfficersList);
// End List Recordset

$nav_listrsOfficersList1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: false,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_CPOSITION {width:175px; overflow:hidden;}
  .KT_col_FULLNAME {width:175px; overflow:hidden;}
  .KT_col_STARTDATE {width:140px; overflow:hidden;}
  .KT_col_TERMINATIONDATE {width:140px; overflow:hidden;}
</style><!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->

<div class="KT_tng" id="listrsOfficersList1">
  <h1>[*pagetitle*]
    <?php
  $nav_listrsOfficersList1->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listrsOfficersList1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
            <?php 
  // Show IF Conditional region1 
  if (@$_GET['show_all_nav_listrsOfficersList1'] == 1) {
?>
              <?php echo $_SESSION['default_max_rows_nav_listrsOfficersList1']; ?>
              <?php 
  // else Conditional region1
  } else { ?>
              <?php echo NXT_getResource("all"); ?>
              <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                            <?php 
  // Show IF Conditional region2 
  if (@$_SESSION['has_filter_tfi_listrsOfficersList1'] == 1) {
?>
                              <a href="<?php echo $tfi_listrsOfficersList1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                              <?php 
  // else Conditional region2
  } else { ?>
                              <a href="<?php echo $tfi_listrsOfficersList1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                              <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>            </th>
            <th id="CPOSITION" class="KT_sorter KT_col_CPOSITION <?php echo $tso_listrsOfficersList1->getSortIcon('CPOSITION'); ?>"> <a href="<?php echo $tso_listrsOfficersList1->getSortLink('CPOSITION'); ?>">OFFICE</a> </th>
            <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listrsOfficersList1->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listrsOfficersList1->getSortLink('FULLNAME'); ?>">NAME</a> </th>
            <th id="STARTDATE" class="KT_sorter KT_col_STARTDATE <?php echo $tso_listrsOfficersList1->getSortIcon('STARTDATE'); ?>"> <a href="<?php echo $tso_listrsOfficersList1->getSortLink('STARTDATE'); ?>">START DATE</a> </th>
            <th id="TERMINATIONDATE" class="KT_sorter KT_col_TERMINATIONDATE <?php echo $tso_listrsOfficersList1->getSortIcon('TERMINATIONDATE'); ?>"> <a href="<?php echo $tso_listrsOfficersList1->getSortLink('TERMINATIONDATE'); ?>">END DATE</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3 
  if (@$_SESSION['has_filter_tfi_listrsOfficersList1'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listrsOfficersList1_CPOSITION" id="tfi_listrsOfficersList1_CPOSITION" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsOfficersList1_CPOSITION']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsOfficersList1_FULLNAME" id="tfi_listrsOfficersList1_FULLNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsOfficersList1_FULLNAME']); ?>" size="25" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsOfficersList1_STARTDATE" id="tfi_listrsOfficersList1_STARTDATE" value="<?php echo @$_SESSION['tfi_listrsOfficersList1_STARTDATE']; ?>" size="10" maxlength="22" /></td>
              <td><input type="text" name="tfi_listrsOfficersList1_TERMINATIONDATE" id="tfi_listrsOfficersList1_TERMINATIONDATE" value="<?php echo @$_SESSION['tfi_listrsOfficersList1_TERMINATIONDATE']; ?>" size="10" maxlength="22" /></td>
              <td><input type="submit" name="tfi_listrsOfficersList1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsOfficersList == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="6"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsOfficersList > 0) { // Show if recordset not empty ?>
            <?php
  while (!$rsOfficersList->EOF) { 
?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_vw_boule_officers" class="id_checkbox" value="<?php echo $rsOfficersList->Fields('OFFICERID'); ?>" />
                    <input type="hidden" name="OFFICERID" class="id_field" value="<?php echo $rsOfficersList->Fields('OFFICERID'); ?>" />                </td>
                <td><div class="KT_col_CPOSITION"><?php echo KT_FormatForList($rsOfficersList->Fields('CPOSITION'), 25); ?></div></td>
                <td><div class="KT_col_FULLNAME"><strong><?php echo KT_FormatForList($rsOfficersList->Fields('FULLNAME'), 25); ?></strong></div></td>
                <td><div class="KT_col_STARTDATE"><?php echo KT_formatDate($rsOfficersList->Fields('STARTDATE')); ?></div></td>
                <td><div class="KT_col_TERMINATIONDATE"><?php echo KT_formatDate($rsOfficersList->Fields('TERMINATIONDATE')); ?></div></td>
                <td><a class="KT_edit_link" href="/home/officers_detail.php?OFFICERID=<?php echo $rsOfficersList->Fields('OFFICERID'); ?>&amp;KT_back=1"><?php echo NXT_getResource("remove"); ?></a> </td>
              </tr>
              <?php
    $rsOfficersList->MoveNext(); 
  }
?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsOfficersList1->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
        <span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
        </select>
        <a class="KT_additem_op_link" href="/home/officers_add_new.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsOfficersList->Close();
?>
