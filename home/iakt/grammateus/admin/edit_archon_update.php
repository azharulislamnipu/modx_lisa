<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("PREFIX", true, "text", "", "2", "", "");
$formValidation->addField("FIRSTNAME", true, "text", "", "1", "", "Please enter your first name.");
$formValidation->addField("LASTNAME", true, "text", "", "2", "", "Please enter your last name.");
$formValidation->addField("ADDRESS1", true, "text", "", "", "", "Please enter an address.");
$formValidation->addField("CITY", true, "text", "", "", "", "Please enter the name of your city.");
$formValidation->addField("STATECD", true, "text", "", "", "", "");
$formValidation->addField("ZIP", true, "text", "", "", "", "");
$formValidation->addField("HOMEPHONE", true, "text", "", "", "", "");
$formValidation->addField("EMAIL", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// begin Recordset
$query_rsState = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsState = $sigma_modx->SelectLimit($query_rsState) or die($sigma_modx->ErrorMsg());
$totalRows_rsState = $rsState->RecordCount();
// end Recordset

// begin Recordset
$query_rsOccupations = "SELECT * FROM spp_occupation ORDER BY `DESCRIPTION` ASC";
$rsOccupations = $sigma_modx->SelectLimit($query_rsOccupations) or die($sigma_modx->ErrorMsg());
$totalRows_rsOccupations = $rsOccupations->RecordCount();
// end Recordset

// begin Recordset
$query_rsSkills = "SELECT SKILLID, SKILLCD, `DESCRIPTION` FROM spp_skillref ORDER BY `DESCRIPTION` ASC";
$rsSkills = $sigma_modx->SelectLimit($query_rsSkills) or die($sigma_modx->ErrorMsg());
$totalRows_rsSkills = $rsSkills->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_archons = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_archons);
// Register triggers
$ins_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$ins_spp_archons->setTable("spp_archons");
$ins_spp_archons->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$ins_spp_archons->addColumn("PREFIX", "STRING_TYPE", "POST", "PREFIX");
$ins_spp_archons->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$ins_spp_archons->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$ins_spp_archons->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$ins_spp_archons->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$ins_spp_archons->addColumn("DESIGNATIONLST", "STRING_TYPE", "POST", "DESIGNATIONLST");
$ins_spp_archons->addColumn("ADDRESS1", "STRING_TYPE", "POST", "ADDRESS1");
$ins_spp_archons->addColumn("ADDRESS2", "STRING_TYPE", "POST", "ADDRESS2");
$ins_spp_archons->addColumn("CITY", "STRING_TYPE", "POST", "CITY");
$ins_spp_archons->addColumn("STATECD", "STRING_TYPE", "POST", "STATECD");
$ins_spp_archons->addColumn("ZIP", "STRING_TYPE", "POST", "ZIP");
$ins_spp_archons->addColumn("HOMEPHONE", "STRING_TYPE", "POST", "HOMEPHONE");
$ins_spp_archons->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$ins_spp_archons->addColumn("ALTEMAIL", "STRING_TYPE", "POST", "ALTEMAIL");
$ins_spp_archons->addColumn("SPOUSENAME", "STRING_TYPE", "POST", "SPOUSENAME");
$ins_spp_archons->addColumn("BIRTHDATE", "DATE_TYPE", "POST", "BIRTHDATE");
$ins_spp_archons->addColumn("DECEASEDDATE", "DATE_TYPE", "POST", "DECEASEDDATE");
$ins_spp_archons->addColumn("OCCUPATIONCD", "STRING_TYPE", "POST", "OCCUPATIONCD");
$ins_spp_archons->addColumn("SKILLCDLST", "STRING_TYPE", "POST", "SKILLCDLST");
$ins_spp_archons->addColumn("JOBTITLE", "STRING_TYPE", "POST", "JOBTITLE");
$ins_spp_archons->addColumn("ORGNAME", "STRING_TYPE", "POST", "ORGNAME");
$ins_spp_archons->addColumn("ALTADDRESS1", "STRING_TYPE", "POST", "ALTADDRESS1");
$ins_spp_archons->addColumn("ALTADDRESS2", "STRING_TYPE", "POST", "ALTADDRESS2");
$ins_spp_archons->addColumn("ALTCITY", "STRING_TYPE", "POST", "ALTCITY");
$ins_spp_archons->addColumn("ALTSTATE", "STRING_TYPE", "POST", "ALTSTATE");
$ins_spp_archons->addColumn("ALTZIP", "STRING_TYPE", "POST", "ALTZIP");
$ins_spp_archons->addColumn("WORKPHONE", "STRING_TYPE", "POST", "WORKPHONE");
$ins_spp_archons->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$ins_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_archons = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$upd_spp_archons->addColumn("PREFIX", "STRING_TYPE", "POST", "PREFIX");
$upd_spp_archons->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$upd_spp_archons->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$upd_spp_archons->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$upd_spp_archons->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$upd_spp_archons->addColumn("DESIGNATIONLST", "STRING_TYPE", "POST", "DESIGNATIONLST");
$upd_spp_archons->addColumn("ADDRESS1", "STRING_TYPE", "POST", "ADDRESS1");
$upd_spp_archons->addColumn("ADDRESS2", "STRING_TYPE", "POST", "ADDRESS2");
$upd_spp_archons->addColumn("CITY", "STRING_TYPE", "POST", "CITY");
$upd_spp_archons->addColumn("STATECD", "STRING_TYPE", "POST", "STATECD");
$upd_spp_archons->addColumn("ZIP", "STRING_TYPE", "POST", "ZIP");
$upd_spp_archons->addColumn("HOMEPHONE", "STRING_TYPE", "POST", "HOMEPHONE");
$upd_spp_archons->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$upd_spp_archons->addColumn("ALTEMAIL", "STRING_TYPE", "POST", "ALTEMAIL");
$upd_spp_archons->addColumn("SPOUSENAME", "STRING_TYPE", "POST", "SPOUSENAME");
$upd_spp_archons->addColumn("BIRTHDATE", "DATE_TYPE", "POST", "BIRTHDATE");
$upd_spp_archons->addColumn("DECEASEDDATE", "DATE_TYPE", "POST", "DECEASEDDATE");
$upd_spp_archons->addColumn("OCCUPATIONCD", "STRING_TYPE", "POST", "OCCUPATIONCD");
$upd_spp_archons->addColumn("SKILLCDLST", "STRING_TYPE", "POST", "SKILLCDLST");
$upd_spp_archons->addColumn("JOBTITLE", "STRING_TYPE", "POST", "JOBTITLE");
$upd_spp_archons->addColumn("ORGNAME", "STRING_TYPE", "POST", "ORGNAME");
$upd_spp_archons->addColumn("ALTADDRESS1", "STRING_TYPE", "POST", "ALTADDRESS1");
$upd_spp_archons->addColumn("ALTADDRESS2", "STRING_TYPE", "POST", "ALTADDRESS2");
$upd_spp_archons->addColumn("ALTCITY", "STRING_TYPE", "POST", "ALTCITY");
$upd_spp_archons->addColumn("ALTSTATE", "STRING_TYPE", "POST", "ALTSTATE");
$upd_spp_archons->addColumn("ALTZIP", "STRING_TYPE", "POST", "ALTZIP");
$upd_spp_archons->addColumn("WORKPHONE", "STRING_TYPE", "POST", "WORKPHONE");
$upd_spp_archons->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$upd_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "CURRVAL", "");
$upd_spp_archons->addColumn("GRAMMUPDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archons->addColumn("GRAMM", "STRING_TYPE", "VALUE", "{SESSION.webFullname}");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "GET", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$totalRows_rsspp_archons = $rsspp_archons->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wdg="http://www.interaktonline.com/MXWidgets" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" --><?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    MANAGE ARCHON DATA </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_archons->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_archons > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
            <td class="KT_th"><label for="JOINDATE_<?php echo $cnt1; ?>">JOIN DATE:</label></td>
            <td><input name="JOINDATE_<?php echo $cnt1; ?>" id="JOINDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_archons->Fields('JOINDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                <?php echo $tNGs->displayFieldHint("JOINDATE");?> <?php echo $tNGs->displayFieldError("spp_archons", "JOINDATE", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="KT_th">&nbsp;</td>
            <td><span class="style27"><strong>CONTACT INFORMATION</strong></span>  </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="PREFIX_<?php echo $cnt1; ?>">PREFIX:</label></td>
            <td><input type="text" name="PREFIX_<?php echo $cnt1; ?>" id="PREFIX_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('PREFIX')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("PREFIX");?> <?php echo $tNGs->displayFieldError("spp_archons", "PREFIX", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="FIRSTNAME_<?php echo $cnt1; ?>">FIRST NAME:</label></td>
            <td><input type="text" name="FIRSTNAME_<?php echo $cnt1; ?>" id="FIRSTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('FIRSTNAME')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "FIRSTNAME", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="MIDDLEINITIAL_<?php echo $cnt1; ?>">MIDDLE:</label></td>
            <td><input type="text" name="MIDDLEINITIAL_<?php echo $cnt1; ?>" id="MIDDLEINITIAL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('MIDDLEINITIAL')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("MIDDLEINITIAL");?> <?php echo $tNGs->displayFieldError("spp_archons", "MIDDLEINITIAL", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="LASTNAME_<?php echo $cnt1; ?>">LAST NAME:</label></td>
            <td><input type="text" name="LASTNAME_<?php echo $cnt1; ?>" id="LASTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('LASTNAME')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "LASTNAME", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="SUFFIX_<?php echo $cnt1; ?>">SUFFIX:</label></td>
            <td><input type="text" name="SUFFIX_<?php echo $cnt1; ?>" id="SUFFIX_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('SUFFIX')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("SUFFIX");?> <?php echo $tNGs->displayFieldError("spp_archons", "SUFFIX", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="DESIGNATIONLST_<?php echo $cnt1; ?>">HONORIFIC:</label></td>
            <td><input type="text" name="DESIGNATIONLST_<?php echo $cnt1; ?>" id="DESIGNATIONLST_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('DESIGNATIONLST')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("DESIGNATIONLST");?> <?php echo $tNGs->displayFieldError("spp_archons", "DESIGNATIONLST", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ADDRESS1_<?php echo $cnt1; ?>">ADDRESS 1:</label></td>
            <td><input type="text" name="ADDRESS1_<?php echo $cnt1; ?>" id="ADDRESS1_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ADDRESS1')); ?>" size="32" maxlength="40" />
            <?php echo $tNGs->displayFieldHint("ADDRESS1");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS1", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ADDRESS2_<?php echo $cnt1; ?>">ADDRESS 2:</label></td>
            <td><input type="text" name="ADDRESS2_<?php echo $cnt1; ?>" id="ADDRESS2_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ADDRESS2')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ADDRESS2");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS2", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="CITY_<?php echo $cnt1; ?>">CITY:</label></td>
            <td><input type="text" name="CITY_<?php echo $cnt1; ?>" id="CITY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('CITY')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("CITY");?> <?php echo $tNGs->displayFieldError("spp_archons", "CITY", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="STATECD_<?php echo $cnt1; ?>">STATE:</label></td>
            <td><select name="STATECD_<?php echo $cnt1; ?>" id="STATECD_<?php echo $cnt1; ?>">
              <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
              <?php
  while(!$rsState->EOF){
?>
              <option value="<?php echo $rsState->Fields('st_cd')?>"<?php if (!(strcmp($rsState->Fields('st_cd'), $rsspp_archons->Fields('STATECD')))) {echo "SELECTED";} ?>><?php echo $rsState->Fields('st_nm')?></option>
              <?php
    $rsState->MoveNext();
  }
  $rsState->MoveFirst();
?>
            </select>
                <?php echo $tNGs->displayFieldError("spp_archons", "STATECD", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ZIP_<?php echo $cnt1; ?>">ZIP:</label></td>
            <td><input type="text" name="ZIP_<?php echo $cnt1; ?>" id="ZIP_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ZIP')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ZIP");?> <?php echo $tNGs->displayFieldError("spp_archons", "ZIP", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="HOMEPHONE_<?php echo $cnt1; ?>">HOME PHONE:</label></td>
            <td><input type="text" name="HOMEPHONE_<?php echo $cnt1; ?>" id="HOMEPHONE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('HOMEPHONE')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("HOMEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "HOMEPHONE", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="EMAIL_<?php echo $cnt1; ?>">EMAIL:</label></td>
            <td><input type="text" name="EMAIL_<?php echo $cnt1; ?>" id="EMAIL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('EMAIL')); ?>" size="32" maxlength="125" />
                <?php echo $tNGs->displayFieldHint("EMAIL");?> <?php echo $tNGs->displayFieldError("spp_archons", "EMAIL", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ALTEMAIL_<?php echo $cnt1; ?>">ALTERNATE EMAIL:</label></td>
            <td><input type="text" name="ALTEMAIL_<?php echo $cnt1; ?>" id="ALTEMAIL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTEMAIL')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ALTEMAIL");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTEMAIL", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="KT_th">&nbsp;</td>
            <td><span class="style27"><strong>PERSONAL INFORMATION</strong></span> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="SPOUSENAME_<?php echo $cnt1; ?>">SPOUSE NAME:</label></td>
            <td><input type="text" name="SPOUSENAME_<?php echo $cnt1; ?>" id="SPOUSENAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('SPOUSENAME')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("SPOUSENAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "SPOUSENAME", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="BIRTHDATE_<?php echo $cnt1; ?>">BIRTH DATE:</label></td>
            <td><input name="BIRTHDATE_<?php echo $cnt1; ?>" id="BIRTHDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_archons->Fields('BIRTHDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                <?php echo $tNGs->displayFieldHint("BIRTHDATE");?> <?php echo $tNGs->displayFieldError("spp_archons", "BIRTHDATE", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><a name="deceased"></a><label for="DECEASEDDATE_<?php echo $cnt1; ?>">DECEASED DATE:</label></td>
            <td><input name="DECEASEDDATE_<?php echo $cnt1; ?>" id="DECEASEDDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_archons->Fields('DECEASEDDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
              <?php echo $tNGs->displayFieldHint("DECEASEDDATE");?> <?php echo $tNGs->displayFieldError("spp_archons", "DECEASEDDATE", $cnt1); ?> </td></tr>
          <tr>
            <td class="KT_th">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="KT_th">&nbsp;</td>
            <td><span class="style27"><strong>PROFESSIONAL INFORMATION</strong></span> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="OCCUPATIONCD_<?php echo $cnt1; ?>">TYPE OF OCCUPATION:</label></td>
            <td><select name="OCCUPATIONCD_<?php echo $cnt1; ?>" id="OCCUPATIONCD_<?php echo $cnt1; ?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                <?php
  while(!$rsOccupations->EOF){
?>
                <option value="<?php echo $rsOccupations->Fields('DESCRIPTION')?>"<?php if (!(strcmp($rsOccupations->Fields('DESCRIPTION'), $rsspp_archons->Fields('OCCUPATIONCD')))) {echo "SELECTED";} ?>><?php echo $rsOccupations->Fields('DESCRIPTION')?></option>
                <?php
    $rsOccupations->MoveNext();
  }
  $rsOccupations->MoveFirst();
?>
              </select>
                <?php echo $tNGs->displayFieldError("spp_archons", "OCCUPATIONCD", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="SKILLCDLST_<?php echo $cnt1; ?>">SKILLS AND EXPERTISE:</label></td>
            <td><select name="SKILLCDLST_<?php echo $cnt1; ?>" id="SKILLCDLST_<?php echo $cnt1; ?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                <?php
  while(!$rsSkills->EOF){
?>
                <option value="<?php echo $rsSkills->Fields('DESCRIPTION')?>"<?php if (!(strcmp($rsSkills->Fields('DESCRIPTION'), $rsspp_archons->Fields('SKILLCDLST')))) {echo "SELECTED";} ?>><?php echo $rsSkills->Fields('DESCRIPTION')?></option>
                <?php
    $rsSkills->MoveNext();
  }
  $rsSkills->MoveFirst();
?>
              </select>
                <?php echo $tNGs->displayFieldError("spp_archons", "SKILLCDLST", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th">&nbsp;</td>
            <td >&nbsp;</td>
          </tr>
          <tr>
            <td class="KT_th">&nbsp;</td>
            <td ><span class="style27"><strong>BUSINESS INFORMATION</strong></span> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="JOBTITLE_<?php echo $cnt1; ?>">JOB TITLE:</label></td>
            <td><input type="text" name="JOBTITLE_<?php echo $cnt1; ?>" id="JOBTITLE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('JOBTITLE')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("JOBTITLE");?> <?php echo $tNGs->displayFieldError("spp_archons", "JOBTITLE", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ORGNAME_<?php echo $cnt1; ?>">BUSINESS NAME:</label></td>
            <td><input type="text" name="ORGNAME_<?php echo $cnt1; ?>" id="ORGNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ORGNAME')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ORGNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "ORGNAME", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ALTADDRESS1_<?php echo $cnt1; ?>">BUSINESS ADDRESS:</label></td>
            <td><input type="text" name="ALTADDRESS1_<?php echo $cnt1; ?>" id="ALTADDRESS1_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTADDRESS1')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ALTADDRESS1");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTADDRESS1", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ALTADDRESS2_<?php echo $cnt1; ?>">BUSINESS ADDRESS 2:</label></td>
            <td><input type="text" name="ALTADDRESS2_<?php echo $cnt1; ?>" id="ALTADDRESS2_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTADDRESS2')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ALTADDRESS2");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTADDRESS2", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ALTCITY_<?php echo $cnt1; ?>">BUSINESS CITY:</label></td>
            <td><input type="text" name="ALTCITY_<?php echo $cnt1; ?>" id="ALTCITY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTCITY')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ALTCITY");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTCITY", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ALTSTATE_<?php echo $cnt1; ?>">BUSINESS STATE:</label></td>
            <td><select name="ALTSTATE_<?php echo $cnt1; ?>" id="ALTSTATE_<?php echo $cnt1; ?>">
              <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
              <?php
  while(!$rsState->EOF){
?>
              <option value="<?php echo $rsState->Fields('st_cd')?>"<?php if (!(strcmp($rsState->Fields('st_cd'), $rsspp_archons->Fields('ALTSTATE')))) {echo "SELECTED";} ?>><?php echo $rsState->Fields('st_nm')?></option>
              <?php
    $rsState->MoveNext();
  }
  $rsState->MoveFirst();
?>
            </select>
                <?php echo $tNGs->displayFieldError("spp_archons", "ALTSTATE", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="ALTZIP_<?php echo $cnt1; ?>">BUSINESS ZIP:</label></td>
            <td><input type="text" name="ALTZIP_<?php echo $cnt1; ?>" id="ALTZIP_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTZIP')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ALTZIP");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTZIP", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="WORKPHONE_<?php echo $cnt1; ?>">BUSINESS PHONE:</label></td>
            <td><input type="text" name="WORKPHONE_<?php echo $cnt1; ?>" id="WORKPHONE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('WORKPHONE')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("WORKPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "WORKPHONE", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="COMMENT_<?php echo $cnt1; ?>">COMMENT:</label></td>
            <td><textarea name="COMMENT_<?php echo $cnt1; ?>" id="COMMENT_<?php echo $cnt1; ?>" cols="80" rows="10"><?php echo KT_escapeAttribute($rsspp_archons->Fields('COMMENT')); ?></textarea>
                <?php echo $tNGs->displayFieldHint("COMMENT");?> <?php echo $tNGs->displayFieldError("spp_archons", "COMMENT", $cnt1); ?> </td>
          </tr>
          <?php 
// Show IF Conditional show_LASTUPDATED_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">LAST UPDATE:</td>
              <td><?php echo KT_formatDate($rsspp_archons->Fields('LASTUPDATED')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_LASTUPDATED_on_update_only
?>
          <?php 
// Show IF Conditional show_UPDATEDBY_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            
            <?php } 
// endif Conditional show_UPDATEDBY_on_update_only
?>
          <?php 
// Show IF Conditional show_GRAMMUPDATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">LAST GRAMMATEUS UPDATE:</td>
              <td><?php echo KT_formatDate($rsspp_archons->Fields('GRAMMUPDATE')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_GRAMMUPDATE_on_update_only
?>
          <?php 
// Show IF Conditional show_GRAMM_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">GRAMMATEUS:</td>
              <td><?php echo KT_escapeAttribute($rsspp_archons->Fields('GRAMM')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_GRAMM_on_update_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_archons_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('kt_pk_spp_archons')); ?>" />
        <?php
    $rsspp_archons->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['CUSTOMERID'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <?php } 
      // endif Conditional region1
      ?>
            <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onClick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsState->Close();

$rsOccupations->Close();

$rsSkills->Close();
?>
