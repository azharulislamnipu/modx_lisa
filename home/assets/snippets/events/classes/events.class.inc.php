<?php
/* -----------------------------------------------------------------------------
* Snippet: Events
* -----------------------------------------------------------------------------
* @package  Events
*
* @author       Coroico - www.modx.wangba.fr
* @version      1.0.0
* @date         29/11/2010
*
* Purpose: Manage & display events
*
* -----------------------------------------------------------------------------
*/

class Events {

    //public variables
    var $modx;
    var $config;    // snippet config

    var $ifmt = '%Y-%m-%d %H:%M:%S';    // internal date format
    var $tstart;                        // starting date

    /*
    *  Constructs the Events object
    */
    function Events(& $modx, $cfg ) {

        $this->modx =& $modx;
        $this->config = $cfg;
        $this->tstart = $modx->getMicroTime();

        $infos = "Events " . $this->config['version'] . " - Php" . phpversion() . " - MySql " . mysql_get_server_info();
        $this->logEvent(1,$infos);
        $this->logEvent(1,$cfg);

        // include events parser
        if (!class_exists('EventsParser')) include_once EVT_PATH . "classes/eventsParser.class.inc.php";

    }

    /*
    *  Run Events
    */
    function run() {

        // increase the pcre backtrack limit for improving the parser limits
        ini_set('pcre.backtrack_limit', '16000000');

        date_default_timezone_set($this->config['timeZone']);  // Sets the default timezone used by all date/time functions in a script

        $now = mktime(); // timestamp of now ( when the user display the page)

        // Check the ts parameter passed through GET
        if (isset($_GET['ts'])){
            $tsg = strip_tags($_GET['ts']);
            $ts = ( is_numeric($tsg) && (int)$tsg == $tsg ) ? $tsg : $this->getNextPrevPage($now,1,0); // get the closest event
        }
        else $ts = $this->getNextPrevPage($now,1,0); // get the closest event

        $period = $this->config['period'];
        $tsinfo = getdate($ts); // Get date/time information
        if ($period == 'month') {
            // set the current ts to the first day of the month
            $inc = $tsinfo['mday'] - 1;  // day of the month
            $delta = ($inc > 1) ? '-'.$inc.' days' : '-'.$inc.' day';
            $ts = strtotime(strftime($this->ifmt, $ts) . $delta );
			$tsinfo = getdate($ts);
			$ts = mktime(0, 0, 0, $tsinfo['mon'], $tsinfo['mday'], $tsinfo['year']);
       }
        else if ($period == 'week') {
            // set the current ts to the first day of the week
            $wday = $tsinfo['wday']; //day of the week  - 0 (for Sunday) through 6 (for Saturday)
            $inc = ($wday + 6) % 7;  // week from monday to sunday
            $delta = ($inc > 1) ? '-'.$inc.' days' : '-'.$inc.' day';
            $ts = strtotime(strftime($this->ifmt, $ts) . $delta );
			$tsinfo = getdate($ts);
			$ts = mktime(0, 0, 0, $tsinfo['mon'], $tsinfo['mday'], $tsinfo['year']);
        }

        $tsdate = strftime($this->ifmt, $ts);
        $fmt = ($period == 'day') ? '%j' : (($period == 'week') ? '%U'  : '%m' );
        $no = strftime($fmt,$ts);       // n° of day, week or month
        $year = strftime('%Y',$ts);     // year

        $this->logEvent(1,"ts=".$ts." tsdate=".$tsdate." year=".$year." period=".$period." no=".$no);

        // get events for the period
        $events = $this->getEvents($tsdate, $year, $no);

        // display events related to period parameter & no
        $output = $this->displayEvents($ts, $tsdate, $events);

        $this->logEvent(1,"Elapsed time:".$this->getElapsedTime());

        return $output;
    }

    /*
    *  Returns events
    */
    function getEvents($tsdate, $year='2000', $no='1') {
        $events = array();

        // set up the sql request
        $scfields = array('pagetitle','longtitle','description','alias','parent','introtext','content');
        $lstfields = ' sc.' . implode(', sc.',$scfields);
        $select  = "
                    SELECT sc.id," . $lstfields . ",
                    mxce.title AS event_title, mxce.description AS event_description, mxce.restrictedwebusergroup AS event_restrictedwebusergroup,
                    mxce.linkrel AS event_linkrel, mxce.linktarget AS event_linktarget, mxce.start AS event_start,
                    mxce.startdate AS event_startdate, mxce.starttime AS event_starttime, mxcc.name AS category_event_name,
                    DAYOFYEAR(mxce.startdate) as event_daynb, WEEK(mxce.startdate,7) as event_weeknb,
                    MONTH(mxce.startdate) as event_monthnb, YEAR(mxce.startdate) as event_yearnb, UNIX_TIMESTAMP(mxce.start) AS event_ts
                    FROM " . $this->modx->getFullTableName('mxcalendar_events') . " mxce
                    LEFT JOIN " . $this->modx->getFullTableName('mxcalendar_categories') . " mxcc ON mxcc.id = mxce.category
                    LEFT JOIN " . $this->modx->getFullTableName('site_content') . " sc ON sc.id = mxce.link
                    WHERE (sc.type='document') AND (sc.contentType='text/html') AND (sc.published=1) AND (sc.searchable=1)
                    AND (sc.deleted=0) AND (sc.privateweb=0) AND (mxce.active=1) AND (mxcc.active=1) AND (mxce.start >= '" . $tsdate . "')
                   ";

        // don't display past events
        $nowdate = strftime($this->ifmt);
        $select .= " AND (mxce.start >= '" . $nowdate . "')";

        // select events of the day / week / month of the year or all
        if ($this->config['period'] == 'day') {
            $select .= " AND (YEAR(mxce.start)=" . $year . ")";
            $select .= " AND (DAYOFYEAR(mxce.start)=" . $no . ")";
            $select .= " ORDER BY mxce.start ";  // order by date only
            // add a limit of 1 for day period. Even If several events exist, we would like the first only
            if ($this->config['dayMode'] == 'byEvent') $select .= " LIMIT 0,1";
        }
        else if ($this->config['period'] == 'week') {
            $select .= " AND (YEAR(mxce.start)=" . $year . ")";
            $select .= " AND (WEEK(mxce.start,7)=" . $no . ")";
            $select .= " ORDER BY mxce.start "; // order by date only
        }
        else if ($this->config['period'] == 'month') {
            $select .= " AND (YEAR(mxce.start)=" . $year . ")";
            $select .= " AND (MONTH(mxce.start)=" . $no . ")";
            $select .= " ORDER BY mxce.start "; // order by date only
        }
        else { // get events for 'all' or 'select'
            if ($this->config['period'] == 'all') $select .= " GROUP BY sc.id "; // Keep the first of each group.
            $select .= " ORDER BY mxce.start "; // order by date only
            if ($this->config['allLimit']) $select .= " LIMIT 0," . $this->config['allLimit']; // add a limit
        }

        $this->logEvent(1,$select,'source: get Events - SQL');

        $rs = $this->modx->db->query($select);

        // Append TV rendered output values to selected events
        while($row = $this->modx->db->getRow($rs)) {
            foreach($scfields as $scfield) $row[$scfield.'_show'] = ($row[$scfield] != '')? 1 : 0;
            $tvValues = $this->getdocTvs($row['id']);
            foreach($tvValues as $key => $value) {
                $tvValues[$key.'_show'] = ($value != '')? 1 : 0;
            }
            $events[] = array_merge((array) $tvValues,(array) $row);
        }

        if ($this->config['allOrder']){
            // sort by a tv name
            $allOrder = $this->config['allOrder'];
            foreach ($events as $key => $row) {
                $order[$key] = $row[$allOrder];
                $eventts[$key] = $row['event_ts'];
            }
            array_multisort($order, SORT_DESC, $eventts, SORT_ASC, $events);
        }
        if (count($events)) {
            $this->logEvent(3,"Events with Tvs: ");
            foreach($events as $event) $this->logEvent(3,$event);
        }

        return $events;
    }

    /*
    *  display events related to period parameter & year and no
    */
    function displayEvents($ts, $tsdate, $events) {

        $output = '';
        $nbEvents = count($events);
        $lstEvents = '';
        if ($nbEvents > 0) {

            $tplname = ($nbEvents > 1) ? "Event" : "SingleEvent";
            //! include chunkie class and read template for tplSingleEvent or tplEvent
            $tplEvent = $this->config['tpl'.$tplname];
            $chkEvent = new EventsParser($this->modx, $tplEvent);
            $this->logEvent(2,"\n" . $chkEvent->template, "tplEvent template " . $tplEvent);

            $parseNextEvents = false;
            if (strpos($chkEvent->template, '[+evt.listnextevents+]')) {
                //! include chunkie class and read templates for tplNextEvent
                $tplNextEvent = $this->config['tplNextEvent'];
                $chkNextEvent = new EventsParser($this->modx, $tplNextEvent);
                $this->logEvent(2,"\n" . $chkNextEvent->template, "tplNextEvent template " . $tplNextEvent);

                //! include chunkie class and read templates for tplNextEventsOuter
                $tplNextEventsOuter = $this->config['tplNextEventsOuter'];
                $chkNextEventsOuter = new EventsParser($this->modx, $tplNextEventsOuter);
                $this->logEvent(2,"\n" . $chkNextEventsOuter->template, "tplNextEventsOuter template " . $tplNextEventsOuter);
                $parseNextEvents = true;
            }

            // Parse each events - setup lstEvents
            foreach($events as $event) {
                if ($parseNextEvents){
                    // parse next events
                    $event['listnextevents'] = $this->displayNextEvents($ts,$chkNextEvent,$chkNextEventsOuter,$event);
                    $event['listnextevents_show'] = ($event['lstnextevents']) ? 1 : 0;
                }
                $chkEvent->addVar('evt',$event);
                $lstEvents .= $chkEvent->render(); // parse the template
                $chkEvent->cleanVars();
            }
            unset($chkEvent);
            $this->logEvent(3,"\n" . $lstEvents, "lstEvents");
        }
        $lstEventsShow = ($lstEvents) ? true : false;

        //! include chunkie class and read templates for tplEventsOuter
        $tplEventsOuter = $this->config['tplEventsOuter'];
        $chkEventsOuter = new EventsParser($this->modx, $tplEventsOuter);

        $next = '';
        if (strpos($chkEventsOuter->template, '[+evt.next+]')) {
            //! include chunkie class and read template for tplNext
            $tplNext = $this->config['tplNext'];
            $chkNext = new EventsParser($this->modx, $tplNext);
            $this->logEvent(2,"\n" . $chkNext->template, "tplNext template " . $tplNext);
            $move = (($this->config['period'] == 'day') && ($this->config['dayMode'] == 'byEvent')) ? 0 : 1;
            $nextts = $this->getNextPrevPage($ts,1, $move);
            if ($nextts) $navNext = array('next_show' => 1, 'nextts' => $nextts );
            else $navNext = array('next_show' => 0, 'nextts' => '' );
            $chkNext->addVar('evt', $navNext);
            $next = $chkNext->render();
            unset($chkNext);
        }

        $prev = '';
        if (strpos($chkEventsOuter->template, '[+evt.prev+]')) {
            //! include chunkie class and read template for tplPrev
            $tplPrev = $this->config['tplPrev'];
            $chkPrev = new EventsParser($this->modx, $tplPrev);
            $this->logEvent(2,"\n" . $chkPrev->template, "tplPrev template " . $tplPrev);
            $prevts = $this->getNextPrevPage($ts, -1, 0);
            if ($prevts) $navPrev = array('prev_show' => 1, 'prevts' => $prevts );
            else $navPrev = array('prev_show' => 0, 'prevts' => '' );
            $chkPrev->addVar('evt', $navPrev);
            $prev = $chkPrev->render();
            unset($chkPrev);
        }

        // parse lstEvents, next, prev
        $this->logEvent(2,"\n" . $chkEventsOuter->template, "tplEventsOuter template " . $tplEventsOuter);
        $chkEventsOuter->addVar('evt',array(
                'ts' => $ts,
                'period' => $this->config['period'],
                'tsdate' => strftime($this->ifmt,$ts),
                'listevents_show' => $lstEventsShow,
                'listevents' => $lstEvents,
                'next' => $next,
                'prev' => $prev
            ));
        $output = $chkEventsOuter->render();
        unset($chkEventsOuter);

        $this->logEvent(3,"\n" . $output, "output");
        return $output;
    }

    /*
    *  get the timestamp of the next / prev page or false
    *
    *  dir = -1 : previous, dir = 1 : next
    *  move = 0 : stay on current page, move = 1 : move on the next/prev page
    *
    */
    function getNextPrevPage($newts, $dir=1, $move=1) {

       $period = $this->config['period'];

       // move to next or previous page (next day/week/month)
        if ($move){
            $newts = EventsParser::getNextDate($period, $newts, '', $dir);
        }

        // limit the pagination to incoming events
        $now = mktime(0, 0, 0);
        switch($period) {
            case "day":
                $pastLimit = $now;
                break;
            case "week":
                $pastLimit = EventsParser::getFirstDayWeek($now);
                break;
            case "month":
                $pastLimit = EventsParser::getFirstDayMonth($now);
                break;
        }
        if ( $newts < $pastLimit ) return false;

        // Check if one event exist for the next or prev page
        $select  = "
            SELECT sc.id, mxce.start AS event_start, mxce.startdate AS event_startdate,
            mxce.starttime AS event_starttime, UNIX_TIMESTAMP(mxce.start) AS event_ts
            FROM " . $this->modx->getFullTableName('mxcalendar_events') . " mxce
            LEFT JOIN " . $this->modx->getFullTableName('mxcalendar_categories') . " mxcc ON mxcc.id = mxce.category
            LEFT JOIN " . $this->modx->getFullTableName('site_content') . " sc ON sc.id = mxce.link
            WHERE (sc.type='document') AND (sc.contentType='text/html') AND (sc.published=1) AND (sc.searchable=1)
            AND (sc.deleted=0) AND (sc.privateweb=0) AND (mxce.active=1) AND (mxcc.active=1)
        ";

        $sign = ($dir > 0) ?  '>' :  '<';
        $newdate = strftime($this->ifmt,$newts);
        $select .= '    AND (mxce.start' . $sign . '\''. $newdate . '\')';

        $order = ($dir > 0) ?  'ASC' :  'DESC';
        $select .= ' ORDER BY event_start ' . $order . ' Limit 0,1';

        $this->logEvent(1,$select,'newts='.$newts.' date='. $newdate.' source: getNextPrevPage - SQL' );

        $rs = $this->modx->db->query($select);
        if (!$this->modx->db->getRecordCount($rs)) return false;
        $row = $this->modx->db->getRow($rs);
        $newts = strtotime($row['event_start']); // we take the first event as next/prev event available

        if (( $newts < $pastLimit ) || ($newts < $now)) return false; // limt the pagination to incoming events

        return $newts;
    }

    /*
    *  Get & display Next events of an event
    */
    function displayNextEvents($ts,$chkNextEvent,$chkNextEventsOuter,$event){

        $nextEvents = array(); // next events for an event

        $docid = $event['id'];
        $tsdate = strftime($this->ifmt,$ts);

        $scfields = array('pagetitle','longtitle','description','alias','parent','introtext','content');
        $lstfields = ' sc.' . implode(', sc.',$scfields);
        $select  = "
                    SELECT sc.id," . $lstfields . ",
                    mxce.title AS event_title, mxce.description AS event_description, mxce.restrictedwebusergroup AS event_restrictedwebusergroup,
                    mxce.linkrel AS event_linkrel, mxce.linktarget AS event_linktarget, mxce.start AS event_start,
                    mxce.startdate AS event_startdate, mxce.starttime AS event_starttime, mxcc.name AS category_event_name,
                    DAYOFYEAR(mxce.startdate) as event_daynb, WEEK(mxce.startdate,7) as event_weeknb,
                    MONTH(mxce.startdate) as event_monthnb, YEAR(mxce.startdate) as event_yearnb, UNIX_TIMESTAMP(mxce.start) AS event_ts
                    FROM " . $this->modx->getFullTableName('mxcalendar_events') . " mxce
                    LEFT JOIN " . $this->modx->getFullTableName('mxcalendar_categories') . " mxcc ON mxcc.id = mxce.category
                    LEFT JOIN " . $this->modx->getFullTableName('site_content') . " sc ON sc.id = mxce.link
                    WHERE (sc.type='document') AND (sc.contentType='text/html') AND (sc.published=1) AND (sc.searchable=1)
                    AND (sc.deleted=0) AND (sc.privateweb=0) AND (mxce.active=1) AND (mxcc.active=1) AND (mxce.start >= '" . $tsdate . "')
                   ";

        // don't display past events
        $nowdate = strftime($this->ifmt);
        $select .= " AND (mxce.start >= '" . $nowdate . "') AND (sc.id = '" . $docid . "') ORDER BY mxce.start ";

        $this->logEvent(1,$select,'source: displayNextEvents - SQL');
        $rs = $this->modx->db->query($select);

        // Append TV rendered output values to selected events
        while($row = $this->modx->db->getRow($rs)) {
            foreach($scfields as $scfield) $row[$scfield.'_show'] = ($row[$scfield] != '')? 1 : 0;
            $tvValues = $this->getdocTvs($row['id']);
            foreach($tvValues as $key => $value) {
                $tvValues[$key.'_show'] = ($value != '')? 1 : 0;
            }
            $nextEvents[] = array_merge((array) $tvValues,(array) $row);
        }

        $nbNextEvents = count($nextEvents);
        $lstNextEvents = '';
        if ($nbNextEvents > 0) {        // Parse each events - setup lstNextEvents;
            foreach($nextEvents as $nextEvent) {
                // parse next events
                $chkNextEvent->addVar('evt',$nextEvent);
                $lstNextEvents .= $chkNextEvent->render(); // parse the template
                $chkNextEvent->cleanVars();
            }
            unset($chkEvent);
        }
        $lstNextEventsShow = ($lstNextEvents) ? true : false;

        $chkNextEventsOuter->addVar('evt',array(
                'ts' => $ts,
                'tsdate' => strftime($this->ifmt,$ts),
                'listnextevents_show' => $lstNextEventsShow,
                'listnextevents' => $lstNextEvents
            ));
        $output = $chkNextEventsOuter->render();
        unset($chkNextEventsOuter);

        $this->logEvent(3,"\n" . $output, "output");

        return $output;
    }

    /*
    *  Display a Select tag with forecasted events
    */
    function runOptionsList($ridSelected,$rtsSelected) {

        $now = mktime(); // timestamp of now ( when the user display the page)
        $ts = $this->getNextPrevPage($now,1,0); // get the closest event

        $this->config['period'] = 'select';

        $tsdate = strftime($this->ifmt, $ts);
        $events = $this->getEvents($tsdate);

        // display events and pre-select event related to ridSelected - rtsSelected
        $output = $this->displayOptionsList($ts, $tsdate, $ridSelected, $rtsSelected, $events);

        $this->logEvent(1,"Elapsed time:".$this->getElapsedTime());

        return $output;
    }

    /*
    *  display events related to period parameter & year and no
    */
    function displayOptionsList($ts, $tsdate, $ridSelected, $rtsSelected, $events) {

        $output = '';
        $nbEvents = count($events);
        $lstEvents = '';
        if ($nbEvents > 0) {
            // add each events as <option></option>
            foreach($events as $event) {
                $selected = ($event['event_ts'] == $rtsSelected) && ($event['id'] == $ridSelected) ? "selected='selected'" : "";
                //$tsdate = strftime('%d-%m-%Y %H:%M:%S', $event['event_ts']);
                $tsdate = strftime('%d-%m-%Y %Hh%M', $event['event_ts']);
                $value = $event['pagetitle'] . '&nbsp;&nbsp;&nbsp;' . $tsdate;
                $output .= '<option value="' . $value . '" ' . $selected . '>' . $event['pagetitle'] . '&nbsp;&nbsp;&nbsp;' . $tsdate . '</option>'. "\n\r";
            }
        }
        $this->logEvent(3,"\n" . $output, "displayOptionsList output");
        return $output;
    }

    /*
    *  Return the user defined tvs of a document
    */
    function getDocTvs($docid) {
        $docTvs = array();
        $idnames = array();

        $sql  = "SELECT DISTINCT tv.id AS id ";
        $sql .= "FROM " . $this->modx->getFullTableName('site_tmplvars')." tv ";
        $sql .= "INNER JOIN " . $this->modx->getFullTableName('site_tmplvar_templates')." tvtpl ON tvtpl.tmplvarid = tv.id ";
        $sql .= "LEFT JOIN " . $this->modx->getFullTableName('site_tmplvar_contentvalues')." tvc ON tvc.tmplvarid=tv.id AND tvc.contentid = '" . $docid . "' ";
        $sql .= "WHERE tvc.contentid = '" . $docid . "'" . $where;

        $rs= $this->modx->db->query($sql);
        if ($this->modx->db->getRecordCount($rs)) {
            while($row = $this->modx->db->getRow($rs)){
                array_push($idnames,$row['id']);
            }
            $docTvs = $this->modx->getTemplateVarOutput($idnames,$docid);
            if (!$docTvs) $docTvs = array();
        }
        return $docTvs;
    }

    /*
    *  Set trace in EventLog
    *
    *  debug level : 1 to 3
    *  value as string, array or record set
    *  label
    *
    */
    function logEvent() {
        $args = func_get_args();
        $dbg = ($this->config['debug'] >= $args[0]);
        if ($dbg) {

           // write trace in event log
            $nba = count($args);
            $result = $when . " " . $etime . "  " . $memory;
            if ($nba > 2) {
                $result.= $args[2] . " : ";
            }
            if (is_resource($args[1])) { // resource type
                $result .= "\n";
                while($row = $this->modx->db->getRow($args[1])) $result .= print_r($row, true) . "\n";
            }
            else if (is_array($args[1])) {  // array type
                $result .= "\n" . print_r($args[1], true) . "\n";
            }
            else $result .= $args[1] . "\n"; // value

            $this->modx->logEvent($args[0], 1, $result, 'Events snippet');
            return true;
        }
        return;
    }

    /*
    * Returns the elapsed time between the current time and tstart
    */
    function getElapsedTime($start=0) {
        list($usec, $sec)= explode(' ', microtime());
        $tend= (float) $usec + (float) $sec;
        if ($start) $etime= ($tend - $start);
        else $etime= ($tend - $this->tstart);
        $etime = sprintf("%.4fs",$etime);
        return $etime;
    }
}
?>