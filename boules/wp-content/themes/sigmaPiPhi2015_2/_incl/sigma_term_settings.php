<?php

add_filter( 'kc_term_settings', 'sigma_term_options' );
function sigma_term_options( $groups ) {
	$my_group = array(
		'category'	=> array(		// taxonomy name
			'privacy' => array(
				'id'				=> 'privacy',		// section ID for each metabox
				'title'			=> 'Privacy',		// section title
				'desc'			=> '<p>Controls the privacy of this category</p>',	// section description (optional, default null)
				'priority'	=> 'high',							// section priority, low|high (optional, default high)
				'role'			=> array('administrator', 'editor'),			// user role, only user in this role will get this metabox. use an array for more than one role (optional, default none)
				'fields'		=> array(								// here are the options for this metabox
					'privacy_control' => array(
						'id'		=> 'privacy_control',
						'title'		=> 'Private category',
						'desc'		=> 'Should this category be visible to chapter site members only?',
						'type'		=> 'select',
						'options'	=> array(
							'yes'	=> 'Yes',
							'no'	=> 'No'
						)
					)
				)
			)
		)
	);

	$groups[] = $my_group;
	return $groups;
}

?>