<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("CUSTOMERCD", true, "numeric", "", "", "", "");
$formValidation->addField("CPOSITION", true, "text", "", "", "", "");
$formValidation->addField("JOINDATE", true, "date", "date", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("lisa.jeter@gmail.com");
  $emailObj->setCC("{SESSION.webEmail}");
  $emailObj->setBCC("");
  $emailObj->setSubject("NEW MEMBER BOULE OFFICER");
  //WriteContent method
  $emailObj->setContent("<p>{SESSION.BOULENAME} has a new {CPOSITION}:\n\n<p>CUSTOMERCD: {CUSTOMERCD}</p>\n\nSubmitted by Archon {SESSION.webFullname}</p>\n");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

// begin Recordset
$colname__rsArchons = '-1';
if (isset($_SESSION['REGIONNAME'])) {
  $colname__rsArchons = $_SESSION['REGIONNAME'];
}
$query_rsArchons = sprintf("SELECT * FROM vw_gramm_archons WHERE REGIONNAME = %s AND STATUSSTT = 'Active' ORDER BY FULLNAME ASC", GetSQLValueString($colname__rsArchons, "text"));
$rsArchons = $sigma_modx->SelectLimit($query_rsArchons) or die($sigma_modx->ErrorMsg());
$totalRows_rsArchons = $rsArchons->RecordCount();
// end Recordset

// begin Recordset
$query_rsOffices = "SELECT * FROM spp_cposition WHERE (spp_cposition.CPOSITION NOT LIKE '%grand%') AND    (spp_cposition.CPOSITION NOT LIKE '%region%') ORDER BY spp_cposition.CPOSITION";
$rsOffices = $sigma_modx->SelectLimit($query_rsOffices) or die($sigma_modx->ErrorMsg());
$totalRows_rsOffices = $rsOffices->RecordCount();
// end Recordset

// begin Recordset
$colname__rsGrammDetail = '-1';
if (isset($_SESSION['webInternalkey'])) {
  $colname__rsGrammDetail = $_SESSION['webInternalkey'];
}
$query_rsGrammDetail = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname__rsGrammDetail, "int"));
$rsGrammDetail = $sigma_modx->SelectLimit($query_rsGrammDetail) or die($sigma_modx->ErrorMsg());
$totalRows_rsGrammDetail = $rsGrammDetail->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_officers = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_officers);
// Register triggers
$ins_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
$ins_spp_officers->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_officers->setTable("spp_officers");
$ins_spp_officers->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$ins_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "POST", "CPOSITION");
$ins_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$ins_spp_officers->addColumn("REGIONCD", "STRING_TYPE", "POST", "REGIONCD");
$ins_spp_officers->addColumn("CHAPTERCD", "STRING_TYPE", "POST", "CHAPTERCD");
$ins_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "POST", "COMMITTEESTATUSSTT");
$ins_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "VALUE", "{SESSION.webFullname}");
$ins_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE");

// Make an instance of the transaction object
$del_spp_officers = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_officers);
// Register triggers
$del_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$del_spp_officers->setTable("spp_officers");
$del_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE", "GET", "OFFICERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_officers = $tNGs->getRecordset("spp_officers");
$totalRows_rsspp_officers = $rsspp_officers->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wdg="http://www.interaktonline.com/MXWidgets" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    ADD NEW BOUL&#201; OFFICER</h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_officers->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_officers > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <?php 
// Show IF Conditional show_CUSTOMERCD_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
              <tr>
                <td class="KT_th"><label for="CUSTOMERCD_<?php echo $cnt1; ?>">ARCHON:</label></td>
                <td><select name="CUSTOMERCD_<?php echo $cnt1; ?>" id="CUSTOMERCD_<?php echo $cnt1; ?>">
                    <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                    <?php
  while(!$rsArchons->EOF){
?>
                    <option value="<?php echo $rsArchons->Fields('CUSTOMERID')?>"<?php if (!(strcmp($rsArchons->Fields('CUSTOMERID'), $rsspp_officers->Fields('CUSTOMERCD')))) {echo "SELECTED";} ?>><?php echo $rsArchons->Fields('FULLNAME')?></option>
                    <?php
    $rsArchons->MoveNext();
  }
  $rsArchons->MoveFirst();
?>
                  </select>
                    <?php echo $tNGs->displayFieldError("spp_officers", "CUSTOMERCD", $cnt1); ?> </td>
              </tr>
              <?php } 
// endif Conditional show_CUSTOMERCD_on_insert_only
?>
            <?php 
// Show IF Conditional show_CPOSITION_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
              <tr>
                <td class="KT_th"><label for="CPOSITION_<?php echo $cnt1; ?>">OFFICE:</label></td>
                <td><select name="CPOSITION_<?php echo $cnt1; ?>" id="CPOSITION_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                  <?php
  while(!$rsOffices->EOF){
?>
                  <option value="<?php echo $rsOffices->Fields('CPOSITION')?>"<?php if (!(strcmp($rsOffices->Fields('CPOSITION'), $rsspp_officers->Fields('CPOSITION')))) {echo "SELECTED";} ?>><?php echo $rsOffices->Fields('CPOSITION')?></option>
                  <?php
    $rsOffices->MoveNext();
  }
  $rsOffices->MoveFirst();
?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_officers", "CPOSITION", $cnt1); ?> </td></tr>
              <?php } 
// endif Conditional show_CPOSITION_on_insert_only
?>
            <?php 
// Show IF Conditional show_JOINDATE_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
              <tr>
                <td class="KT_th"><label for="JOINDATE_<?php echo $cnt1; ?>">START DATE:</label></td>
                <td><input name="JOINDATE_<?php echo $cnt1; ?>" id="JOINDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_officers->Fields('JOINDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                    <?php echo $tNGs->displayFieldHint("JOINDATE");?> <?php echo $tNGs->displayFieldError("spp_officers", "JOINDATE", $cnt1); ?> </td>
              </tr>
              <?php } 
// endif Conditional show_JOINDATE_on_insert_only
?>
            <tr>
              <td class="KT_th">DATE CREATED:</td>
              <td><?php echo KT_formatDate($rsspp_officers->Fields('UPDATETMS')); ?></td>
            </tr>
            <tr>
              <td class="KT_th">CREATED BY:</td>
              <td><?php echo KT_escapeAttribute($rsspp_officers->Fields('UPDATEUSERCD')); ?></td>
            </tr>
          </table>
          <input type="hidden" name="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" id="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" value="Active" />
          <input type="hidden" name="CHAPTERCD_<?php echo $cnt1; ?>" id="CHAPTERCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsArchons->Fields('CHAPTERID')); ?>" />
          <input type="hidden" name="REGIONCD_<?php echo $cnt1; ?>" id="REGIONCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsArchons->Fields('REGIONCD')); ?>" />
<input type="hidden" name="kt_pk_spp_officers_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_officers->Fields('kt_pk_spp_officers')); ?>" />
<?php
    $rsspp_officers->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['OFFICERID'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchons->Close();

$rsOffices->Close();

$rsGrammDetail->Close();
?>
