<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Customize page
 *
 * @package svManager
 */
class Sv2CustomizePage extends Page
{
 /**
  * @var string overrides some of head content in Page
  */
  var $customHeadHtml = '
       <link href="colorjack/plugin.css" rel="stylesheet" type="text/css" />
       <style type="text/css">
         #plugin {
           color: #FFFFFF;
           background-color: #999999;
         }
         #H {
           border-color: #999999;
         }
         #plugCLOSE {
           color: #FFFFFF;
           background-color: #999999;
         }
       </style>
       <script src="scripts/mootools-core.js" type="text/javascript"></script>
       <script src="scripts/mootools-more.js" type="text/javascript"></script>
       <script src="colorjack/plugin.js" type="text/javaScript"></script>
       <script src="scripts/customize.js" type="text/javascript"></script>
       <script src="scripts/mooformcheck.js" type="text/javascript"></script>
';
 
 /**
  * constructs CustomizePage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function Sv2CustomizePage()
  {
    parent::Page('svManager &ndash; customize', 'sv2customize', 'navcustomize simpleviewer2 customize');
  }

 /**
  * get html for customize page
  *
  * Note hack in conditional comments from
  * @link http://www.hedgerwow.com/360/bugs/css-select-free.html
  * to prevent select elements showing above the colorpicker in IE6Win
  * @access public
  * @return string html
  * @param object gallery set
  * @param array gallery attributes
  */
  function getCustomizeHtml(&$gallerySet, &$gallery)
  {
    $prefs = $gallery->getPreferences();
    $rootUrl = $gallery->getRootUrl();
    //$prefs['title'] = htmlspecialchars($prefs['title'], ENT_QUOTES, 'UTF-8');
    $tab = 0;
    
    $title = new FormField('Gallery title', 'title', $prefs['title']);
    $textColor = new FormField('Text color', 'textColor', $prefs['textColor']);
    $frameColor = new FormField('Frame color', 'frameColor', $prefs['frameColor']);
    $backgroundColor = new FormField('Background color', 'backgroundColor', $prefs['backgroundColor']);
    $galleryStyle = new FormField('Gallery style', 'galleryStyle', $prefs['galleryStyle']);
    $thumbPosition = new FormField('Thumb position', 'thumbPosition', $prefs['thumbPosition']);
    $thumbRows = new FormField('Thumbnail rows', 'thumbRows', $prefs['thumbRows']);
    $thumbColumns = new FormField('Thumbnail columns', 'thumbColumns', $prefs['thumbColumns']);
    $frameWidth = new FormField('Frame width, px', 'frameWidth', $prefs['frameWidth']);
    $maxImageWidth = new FormField('Max image width, px', 'maxImageWidth', $prefs['maxImageWidth']);
    $maxImageHeight = new FormField('Max image height, px', 'maxImageHeight', $prefs['maxImageHeight']);
    $backgroundTransparent = new FormField('Backgnd transparent', 'backgroundTransparent', $prefs['backgroundTransparent']);
    $addLinks = new FormField('Auto caption links', 'addLinks', $prefs['addLinks']);
    $showOpenButton = new FormField('Show open button', 'showOpenButton', $prefs['showOpenButton']);
    $showFullscreenButton = new FormField('Fullscreen button', 'showFullscreenButton', $prefs['showFullscreenButton']);
    $xmlGallery = new FormField('XML', 'useFlickr', $prefs['useFlickr']);
    $flickrGallery = new FormField('Flickr', 'useFlickr', $prefs['useFlickr']);
    $flickrUserName = new FormField('Flickr user name', 'flickrUserName', $prefs['flickrUserName']);
    $flickrTags = new FormField('Flickr tags', 'flickrTags', $prefs['flickrTags']);
    $urlRelative = new FormField('Relative', 'urlType', $prefs['urlType']);
    $urlAbsolute = new FormField('Absolute', 'urlType', $prefs['urlType']);
    $urlHttp = new FormField('http', 'urlType', $prefs['urlType']);
    $galleryWidth = new FormField('Gallery width', 'galleryWidth', $prefs['galleryWidth']);
    $galleryHeight = new FormField('Gallery height', 'galleryHeight', $prefs['galleryHeight']);
    $useFlash = new FormField('Use Flash', 'useFlash', $prefs['useFlash']);    

$html = $this->getColorjackHtml();
$html .= '
<form class="public" action = "'.$_SERVER['PHP_SELF'].'" id="customizeform" method="post">
  <div class="elementwrap">
    <h3 class="togglers">Basic settings</h3>
    <div class="accordionElements">
      <table id="settings1" cellspacing="0">
        <tr id="titleentry">
          <td class="label">'.$title->label().'</td><td>'.$title->text($tab).'</td>
        </tr>
      </table>
    
      <table id="settings2" cellspacing="0">
        <tr id="textcolorentry">
          <td class="label">'.$textColor->label().'</td><td>'.$textColor->color($tab).'</td>
        </tr>
        <tr id="framecolorentry">
          <td class="label">'.$frameColor->label().'</td><td>'.$frameColor->color($tab).'</td>
        </tr>
        <tr id="backgroundcolorentry">
          <td class="label">'.$backgroundColor->label().'</td><td>'.$backgroundColor->color($tab).'</td>
        </tr>
        <tr id="gallerystyleentry">
          <td class="label">'.$galleryStyle->label().'</td><td>'.$galleryStyle->select($tab, array("MODERN", "CLASSIC","COMPACT")).'</td>
        </tr>
        <tr id="thumbpositionentry">
          <td class="label">'.$thumbPosition->label().'</td><td>'.$thumbPosition->select($tab, array("TOP", "BOTTOM", "LEFT", "RIGHT", "NONE")).'</td>
        </tr>
        <tr id="thumbrowsentry">
          <td class="label">'.$thumbRows->label().'</td><td>'.$thumbRows->text($tab).'</td>
        </tr>
        <tr id="thumbcolumnsentry">
          <td class="label">'.$thumbColumns->label().'</td><td>'.$thumbColumns->text($tab).'</td>
        </tr>
        <tr id="framewidthentry">
          <td class="label">'.$frameWidth->label().'</td><td>'.$frameWidth->text($tab).'</td>
        </tr>
      </table>
    
      <table id="settings3" cellspacing="0">
        <tr id="maximagewidthentry">
          <td class="label">'.$maxImageWidth->label().'</td><td>'.$maxImageWidth->text($tab).'</td>
        </tr>
        <tr id="maximageheightentry">
          <td class="label">'.$maxImageHeight->label().'</td><td>'.$maxImageHeight->text($tab).'</td>
        </tr>
        <tr id="backgroundtransparententry">
          <td class="label">'.$backgroundTransparent->label().'</td><td>'.$backgroundTransparent->checkbox($tab).'</td>
        </tr>
        <tr id="addlinksentry">
          <td class="label">'.$addLinks->label().'</td><td>'.$addLinks->checkbox($tab).'</td>
        </tr>
        <tr id="showopenbuttonentry">
          <td class="label">'.$showOpenButton->label().'</td><td>'.$showOpenButton->checkbox($tab).'</td>
        </tr>
        <tr id="showfullscreenbuttonentry">
          <td class="label">'.$showFullscreenButton->label().'</td><td>'.$showFullscreenButton->checkbox($tab).'</td>
        </tr>
        <tr id="useflickrentry">
          <td class="label">Gallery type</td>
          <td>'.$xmlGallery->radio($tab, 'FALSE').$xmlGallery->label().$flickrGallery->radio($tab, 'TRUE').$flickrGallery->label().'</td>
        </tr>
        <tr id="flickrusernameentry">
          <td class="label">'.$flickrUserName->label().'</td><td>'.$flickrUserName->text($tab).'</td>
        </tr>
        <tr id="flickrtagsentry">
          <td class="label">'.$flickrTags->label().'</td><td>'.$flickrTags->text($tab).'</td>
        </tr>
      </table>
    </div>
  </div>
  <div class="elementwrap" id="advanced">
    <h3 class="togglers">Advanced settings</h3>
    <div class="accordionElements">
      <table id="settingspaths" cellspacing="0">
        <tr id="absurlentry">
          <td class="label"><label for="absurl">Path from web root</label></td><td><input type="text" id="absurl" tabindex="18" class="text" name="absurl" value="'.$rootUrl.'" /></td>
        </tr>
        <tr id="urltypeentry">
          <td class="label">Save paths as</td>
          <td>'.$urlRelative->radio($tab, 'RELATIVE').$urlRelative->label().$urlAbsolute->radio($tab, 'ABSOLUTE').$urlAbsolute->label().$urlHttp->radio($tab, 'HTTP').$urlHttp->label().'</td>
        </tr>
        <tr id="gallerywidthentry">
          <td class="label">'.$galleryWidth->label().'</td><td>'.$galleryWidth->text($tab).'</td>
        </tr>
        <tr id="galleryheightentry">
          <td class="label">'.$galleryHeight->label().'</td><td>'.$galleryHeight->text($tab).'</td>
        </tr>
        <tr id="useflashentry">
          <td class="label">'.$useFlash->label().'</td><td>'.$useFlash->checkBox($tab).'</td>
        </tr>
      </table>
    </div>
  </div>
  <div id="submitinputs">
    <input type="hidden" name="customizesubmitted" value="true" /><input type="submit" name="submit" class="formbutton" value="Update" />
  </div>
</form>';
    return $html;
  }

}
