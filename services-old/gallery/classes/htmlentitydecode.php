<?php
 /**
  * Part of Airtight Interactive gallery management package.
  *
  * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
  * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
  *
  * @package svManager
  * @author Jack Hardie {@link http://www.jhardie.com}
  * @version 1.6.3 build 091231
  * @copyright Copyright (c) 2007 - 2009, Airtight Interactive
  */
 
 /**
  * UTF-8 and php4 safe version of html_entity_decode()
  *
  * @package svManager
  */
  class HtmlEntityDecode
  {
   /**
    * Decode html special characters
    *
    * @access public
    * @return string
    * @param string
    */
    function decode($string)
    {
      if (version_compare(phpversion(), "5.0", ">="))
      {
        return html_entity_decode($string, ENT_QUOTES, 'UTF-8');
      }
      else
      {
        return ($this->html_entity_decode_utf8($string));
      }  
    }

   /**
    * Fix for bug in php4 "Warning: cannot yet handle MBCS in html_entity_decode()!"
    *
    * @author laurynas dot butkus at gmail dot com
    * @access private
    * @return string utf decoded string
    * @param string with html entities to decode
    */
    function html_entity_decode_utf8($string)
    {
      static $trans_tbl;
     
      // replace numeric entities
      $string = preg_replace('~&#x([0-9a-f]+);~ei', '$this->code2utf(hexdec("\\1"))', $string);
      $string = preg_replace('~&#([0-9]+);~e', '$this->code2utf(\\1)', $string);
  
      // replace literal entities
      if (!isset($trans_tbl))
      {
          $trans_tbl = array();
         
          foreach (get_html_translation_table(HTML_ENTITIES) as $val=>$key)
              $trans_tbl[$key] = utf8_encode($val);
      }
     
      return strtr($string, $trans_tbl);
    }

   /**
    * Support function for fix for bug in php4
    *
    * Returns the utf string corresponding to the unicode value
    * @author from php.net, courtesy - romans@void.lv
    * @access private
    * @return string utf decoded string
    * @param string with html entities to decode
    */
    function code2utf($num)
    {
        if ($num < 128) return chr($num);
        if ($num < 2048) return chr(($num >> 6) + 192) . chr(($num & 63) + 128);
        if ($num < 65536) return chr(($num >> 12) + 224) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
        if ($num < 2097152) return chr(($num >> 18) + 240) . chr((($num >> 12) & 63) + 128) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
        return '';
    }
  }
?>
