<?

/* theme: gold */

$dayHighlight["bgColor"]["1"] = "#f6af12";
$dayHighlight_hover["bgColor"]["1"] = "#ff6314";

$dayWeekendHighlight["bgColor"]["1"] = "#cba247";
$dayWeekendHighlight_hover["bgColor"]["1"] = "#d05110";

$dayHighlight["bgColor"]["2"] = "#dc8902";
$dayHighlight_hover["bgColor"]["2"] = "#ff6314";

$dayWeekendHighlight["bgColor"]["2"] = "#cfa343";
$dayWeekendHighlight_hover["bgColor"]["2"] = "#d05110";

$dayHighlight["bgColor"]["3"] = "#fec852";
$dayHighlight_hover["bgColor"]["3"] = "#ff6314";

$dayWeekendHighlight["bgColor"]["3"] = "#dc8902";
$dayWeekendHighlight_hover["bgColor"]["3"] = "#d05110";

$jumpRow["bgColor"] = "#eeeeee";
$jumpRow_hover["bgColor"] = "#d0d0d0";

$jumpCurrentRow["bgColor"] = "#dfdfdf";

?>

