<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("JOBTITLE", true, "text", "", "", "", "");
$formValidation->addField("ORGNAME", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsTitles = "SELECT SKILLID, `DESCRIPTION` FROM spp_skillref ORDER BY `DESCRIPTION` ASC";
$rsTitles = mysql_query($query_rsTitles, $sigma_modx) or die(mysql_error());
$row_rsTitles = mysql_fetch_assoc($rsTitles);
$totalRows_rsTitles = mysql_num_rows($rsTitles);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = mysql_query($query_rsStates, $sigma_modx) or die(mysql_error());
$row_rsStates = mysql_fetch_assoc($rsStates);
$totalRows_rsStates = mysql_num_rows($rsStates);

// Make an update transaction instance
$upd_spp_archons = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/personal-profile.php");
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("JOBTITLE", "STRING_TYPE", "POST", "JOBTITLE");
$upd_spp_archons->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");
$upd_spp_archons->addColumn("ORGNAME", "STRING_TYPE", "POST", "ORGNAME");
$upd_spp_archons->addColumn("ALTADDRESS1", "STRING_TYPE", "POST", "ALTADDRESS1");
$upd_spp_archons->addColumn("ALTADDRESS2", "STRING_TYPE", "POST", "ALTADDRESS2");
$upd_spp_archons->addColumn("ALTCITY", "STRING_TYPE", "POST", "ALTCITY");
$upd_spp_archons->addColumn("ALTSTATE", "STRING_TYPE", "POST", "ALTSTATE");
$upd_spp_archons->addColumn("ALTZIP", "STRING_TYPE", "POST", "ALTZIP");
$upd_spp_archons->addColumn("WORKPHONE", "STRING_TYPE", "POST", "WORKPHONE");
$upd_spp_archons->addColumn("ALTEMAIL", "STRING_TYPE", "POST", "ALTEMAIL");
$upd_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "LASTUPDATED");
$upd_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$row_rsspp_archons = mysql_fetch_assoc($rsspp_archons);
$totalRows_rsspp_archons = mysql_num_rows($rsspp_archons);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<script>
$MXW_relPath = '../';
</script>
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/JSRecordset.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/DynamicInput.js"></script>
<?php
//begin JSRecordset
$jsObject_rsTitles = new WDG_JsRecordset("rsTitles");
echo $jsObject_rsTitles->getOutput();
//end JSRecordset
?>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">EDIT CURRENT EMPLOYMENT</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="JOBTITLE">TITLE:</label></td>
        <td><select name="JOBTITLE" id="JOBTITLE" wdg:subtype="DynamicInput" wdg:type="widget" wdg:recordset="rsTitles" wdg:displayfield="DESCRIPTION" wdg:valuefield="DESCRIPTION" wdg:norec="50" wdg:singleclickselect="no" wdg:defaultoptiontext="no" wdg:restrict="No" wdg:ext="php" wdg:selected="<?php echo $row_rsspp_archons['JOBTITLE'] ?>" wdg:fakeattribute="<?php WDG_registerRecordInsert("sigma_modx", "rsTitles", "DESCRIPTION", "DESCRIPTION");?>">
          </select>
            <?php echo $tNGs->displayFieldError("spp_archons", "JOBTITLE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ORGNAME">BUSINESS NAME:</label></td>
        <td><input type="text" name="ORGNAME" id="ORGNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ORGNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ORGNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "ORGNAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTADDRESS1">BUSINESS ADDRESS 1:</label></td>
        <td><input type="text" name="ALTADDRESS1" id="ALTADDRESS1" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ALTADDRESS1']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTADDRESS1");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTADDRESS1"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTADDRESS2">BUSINESS ADDRESS 2:</label></td>
        <td><input type="text" name="ALTADDRESS2" id="ALTADDRESS2" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ALTADDRESS2']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTADDRESS2");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTADDRESS2"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTCITY">BUSINESS CITY:</label></td>
        <td><input type="text" name="ALTCITY" id="ALTCITY" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ALTCITY']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTCITY");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTCITY"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTSTATE">BUSINESS STATE:</label></td>
        <td><select name="ALTSTATE" id="ALTSTATE">
            <?php 
do {  
?>
            <option value="<?php echo $row_rsStates['st_cd']?>"<?php if (!(strcmp($row_rsStates['st_cd'], $row_rsspp_archons['ALTSTATE']))) {echo "SELECTED";} ?>><?php echo $row_rsStates['st_nm']?></option>
            <?php
} while ($row_rsStates = mysql_fetch_assoc($rsStates));
  $rows = mysql_num_rows($rsStates);
  if($rows > 0) {
      mysql_data_seek($rsStates, 0);
	  $row_rsStates = mysql_fetch_assoc($rsStates);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archons", "ALTSTATE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTZIP">BUSINESS ZIP:</label></td>
        <td><input type="text" name="ALTZIP" id="ALTZIP" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ALTZIP']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTZIP");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTZIP"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="WORKPHONE">BUSINESS PHONE:</label></td>
        <td><input type="text" name="WORKPHONE" id="WORKPHONE" value="<?php echo KT_escapeAttribute($row_rsspp_archons['WORKPHONE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("WORKPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "WORKPHONE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTEMAIL">BUSINESS EMAIL:</label></td>
        <td><input type="text" name="ALTEMAIL" id="ALTEMAIL" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ALTEMAIL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTEMAIL");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTEMAIL"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="UPDATE CURRENT EMPLOYMENT" /> <input type="button" name="Cancel" value="Cancel" onclick="history.go(-1)" />
        </td>
      </tr>
    </table>
    <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo KT_formatDate($row_rsspp_archons['LASTUPDATED']); ?>" />
    <input type="hidden" name="UPDATEDBY" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($row_rsspp_archons['UPDATEDBY']); ?>" />
    <input type="hidden" name="CUSTOMERID" id="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsspp_archons['CUSTOMERID']); ?>" />
  </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsTitles);

mysql_free_result($rsStates);
?>
