<?php setlocale(LC_ALL, 'en_EN'); ?>
<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
    global $sigma_modx;
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($sigma_modx, $theValue) : mysqli_escape_string($sigma_modx, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchons = "-1";
if (isset($_GET['id'])) {
  $colname_rsArchons = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsArchons = sprintf("SELECT * FROM vw_web_elig_no_officers WHERE WEB_ID = %s", GetSQLValueString($colname_rsArchons, "int"));
$rsArchons = mysqli_query($sigma_modx, $query_rsArchons) or die(mysqli_error($sigma_modx));
$row_rsArchons = mysqli_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysqli_num_rows($rsArchons);

$colname_rsProfessional = "-1";
if (isset($_GET['id'])) {
  $colname_rsProfessional = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsProfessional = sprintf("SELECT * FROM vw_web_elig_educ WHERE WEB_ID = %s ORDER BY DEGREEYEAR ASC", GetSQLValueString($colname_rsProfessional, "int"));
$rsProfessional = mysqli_query($sigma_modx, $query_rsProfessional) or die(mysqli_error($sigma_modx));
$row_rsProfessional = mysqli_fetch_assoc($rsProfessional);
$totalRows_rsProfessional = mysqli_num_rows($rsProfessional);

$colname_rsOffices = "-1";
if (isset($row_rsArchons['CUSTOMERID'])) {
  $colname_rsOffices = (get_magic_quotes_gpc()) ? $row_rsArchons['CUSTOMERID'] : addslashes($row_rsArchons['CUSTOMERID']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsOffices = sprintf("SELECT * FROM spp_officers WHERE CUSTOMERCD = %s ORDER BY JOINDATE DESC", GetSQLValueString($colname_rsOffices, "int"));
$rsOffices = mysqli_query($sigma_modx, $query_rsOffices) or die(mysqli_error($sigma_modx));
$row_rsOffices = mysqli_fetch_assoc($rsOffices);
$totalRows_rsOffices = mysqli_num_rows($rsOffices);

$colname_rsBoule = "-1";
if (isset($_SESSION['BOULENAME'])) {
  $colname_rsBoule = (get_magic_quotes_gpc()) ? $_SESSION['BOULENAME'] : addslashes($_SESSION['BOULENAME']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsBoule = sprintf("SELECT * FROM vw_web_elig_no_officers WHERE BOULENAME = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsBoule, "text"));
$rsBoule = mysqli_query($sigma_modx, $query_rsBoule) or die(mysqli_error($sigma_modx));
$row_rsBoule = mysqli_fetch_assoc($rsBoule);
$totalRows_rsBoule = mysqli_num_rows($rsBoule);

$colname_rsJobs = "-1";
if (isset($_GET['id'])) {
  $colname_rsJobs = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsJobs = sprintf("SELECT    spp_prof.PROFID,   spp_prof.TITLEID,   spp_skillref.DESCRIPTION AS JOBTITLE,   spp_prof.CUSTOMERID,   spp_prof.COMPANYNAME,   spp_prof.COMPANYCITY,   spp_prof.COMPANYSTCD,   spp_prof.DESCRIPTION,   spp_prof.COMPANYJOBSTART,   web_user_attributes.id AS WEB_ID FROM   spp_prof   INNER JOIN spp_skillref ON (spp_prof.TITLEID = spp_skillref.SKILLID)   INNER JOIN web_user_attributes ON (spp_prof.CUSTOMERID = web_user_attributes.internalKey) WHERE web_user_attributes.id = %s ORDER BY COMPANYJOBSTART DESC", GetSQLValueString($colname_rsJobs, "int"));
$rsJobs = mysqli_query($sigma_modx, $query_rsJobs) or die(mysqli_error($sigma_modx));
$row_rsJobs = mysqli_fetch_assoc($rsJobs);
$totalRows_rsJobs = mysqli_num_rows($rsJobs);

$colname_rsArchonOcc = "-1";
if (isset($_GET['id'])) {
  $colname_rsArchonOcc = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsArchonOcc = sprintf("SELECT    spp_prof.OCCUPATIONID,   spp_occupation.DESCRIPTION,   spp_prof.CUSTOMERID,   web_user_attributes.id AS WEB_ID FROM   spp_prof   INNER JOIN spp_occupation ON (spp_prof.OCCUPATIONID = spp_occupation.OCCUPATIONID)   INNER JOIN web_user_attributes ON (spp_prof.CUSTOMERID = web_user_attributes.internalKey) WHERE web_user_attributes.id = %s GROUP BY OCCUPATIONID", GetSQLValueString($colname_rsArchonOcc, "int"));
$rsArchonOcc = mysqli_query($sigma_modx, $query_rsArchonOcc) or die(mysqli_error($sigma_modx));
$row_rsArchonOcc = mysqli_fetch_assoc($rsArchonOcc);
$totalRows_rsArchonOcc = mysqli_num_rows($rsArchonOcc);

$colname_rsArchonTitles = "-1";
if (isset($_GET['id'])) {
  $colname_rsArchonTitles = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsArchonTitles = sprintf("SELECT    spp_prof.CUSTOMERID,   spp_prof.TITLEID,   spp_skillref.DESCRIPTION AS JOBTITLE,   web_user_attributes.id AS WEB_ID FROM   spp_prof   INNER JOIN spp_skillref ON (spp_prof.TITLEID = spp_skillref.SKILLID)   INNER JOIN web_user_attributes ON (spp_prof.CUSTOMERID = web_user_attributes.internalKey) WHERE web_user_attributes.id = %s GROUP BY TITLEID", GetSQLValueString($colname_rsArchonTitles, "int"));
$rsArchonTitles = mysqli_query($sigma_modx, $query_rsArchonTitles) or die(mysqli_error($sigma_modx));
$row_rsArchonTitles = mysqli_fetch_assoc($rsArchonTitles);
$totalRows_rsArchonTitles = mysqli_num_rows($rsArchonTitles);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default_2col.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --> 


<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width460">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">Contact Information</h1>
<div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td valign="top">
	  <?php 
// Show IF Conditional region9 
if (@$row_rsArchons['IMAGE'] != NULL) {
?>
	    <img src="/home/assets/images/archons/<?php echo $row_rsArchons['IMAGE']; ?>" width="190"/>
	    <?php 
// else Conditional region9
} else { ?>
	    <img src="../includes/tng/styles/img_not_found.gif" alt="Add your image." width="100" height="100"/>
  <?php } 
// endif Conditional region9
?>	</td>
    <td><p><span class="larger"><?php echo $row_rsArchons['L2R_FULLNAME']; ?></span><br />
          <?php echo $row_rsArchons['ADDRESS1']; ?><br />
          <?php echo $row_rsArchons['CITY']; ?>, <?php echo $row_rsArchons['STATECD']; ?> <?php echo $row_rsArchons['ZIP']; ?> </p>
      <p>Home Phone: <?php echo $row_rsArchons['HOMEPHONE']; ?><br />
        Email: <a href="mailto:<?php echo $row_rsArchons['EMAIL']; ?>"><?php echo $row_rsArchons['EMAIL']; ?></a><br />
      </p></td>
  </tr>
</table>
</div>
<h1 class="yellow">Education</h1>
<div class="contentBlock">
<?php 
// Show IF Conditional region11 
if (@$row_rsProfessional['DEGREE'] != NULL) {
?>
  <tr valign="top">
    <td><table border="0" cellspacing="4" cellpadding="2">
      <tr valign="top">
        <td style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">YEAR</td>
        <td style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">DEGREE</td>
        <td style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">INSTITUTION</td>
        <td style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">CONCENTRATION</td>
      </tr>
      <?php do { ?>
        <tr valign="top">
          <td><?php echo $row_rsProfessional['DEGREEYEAR']; ?></td>
          <td><?php echo $row_rsProfessional['DEGREE']; ?></td>
          <td><?php echo $row_rsProfessional['INSTITUTION']; ?></td>
          <td><?php echo $row_rsProfessional['MAJOR']; ?>
                <?php 
// Show IF Conditional region1 
if (@$row_rsProfessional['MINOR1'] != NULL) {
?>
                  <br />
                  <?php echo $row_rsProfessional['MINOR1']; ?><br />
                  <?php echo $row_rsProfessional['MINOR2']; ?>
                  <?php } 
// endif Conditional region1
?></td>
        </tr>
        <?php 
// Show IF Conditional region2 
if (@$row_rsProfessional['COMMENT'] != NULL) {
?>
          <tr valign="top">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2"><?php echo $row_rsProfessional['COMMENT']; ?></td>
          </tr>
          <?php } 
// endif Conditional region2
?>
        <?php } while ($row_rsProfessional = mysqli_fetch_assoc($rsProfessional)); ?>
    </table></td>
  </tr>
  <?php 
// else Conditional region11
} else { ?>
  Education information is not available for this archon.
  <?php } 
// endif Conditional region11
?>   
</div>

<h1 class="yellow">Employment</h1>
<div class="contentBlock">
    <table border="0" cellspacing="5" cellpadding="5">
      <?php 
// Show IF Conditional region6 
if ((@$row_rsArchons['JOBTITLE'] || @$row_rsArchons['ORGNAME']) != NULL) {
?>
        <tr>
          <td colspan="3" valign="top" style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">Current Position</td>
        </tr>
        <tr>
          <td colspan="3"><?php echo $row_rsArchons['JOBTITLE']; ?><br />
            <?php echo $row_rsArchons['ORGNAME']; ?><br />
            <?php echo $row_rsArchons['ALTCITY']; ?> <?php echo $row_rsArchons['ALTSTATE']; ?></td>
        </tr>
        <?php 
// else Conditional region6
} else { ?>
<tr><td>A current position is not available for this archon.</td></tr>
<?php } 
// endif Conditional region6
?>
        <?php 
// Show IF Conditional region10 
if ($totalRows_rsJobs > 0) {
?>
		<tr>
          <td colspan="3" valign="top" style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">Prior Positions </td>
        </tr>
        <tr>
          <td valign="top" style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">Year</td>
          <td valign="top" style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">Position/Employer</td>
          <td valign="top" style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">Description</td>
        </tr>
        <?php do { ?>
          <tr>
            <td valign="top"><?php echo date('Y',strtotime($row_rsJobs['COMPANYJOBSTART'])); ?></td>
            <td valign="top"><?php echo $row_rsJobs['JOBTITLE']; ?><br />
                <?php echo $row_rsJobs['COMPANYNAME']; ?><br />
                <?php echo $row_rsJobs['COMPANYCITY']; ?> <?php echo $row_rsJobs['COMPANYSTCD']; ?></td>
            <td valign="top"><?php echo $row_rsJobs['DESCRIPTION']; ?></td>
          </tr>
          <?php } while ($row_rsJobs = mysqli_fetch_assoc($rsJobs)); ?>
  <?php 
// else Conditional region10
} else { ?>
          <tr><td>No prior positions are available for this archon.</td></tr>
  <?php } 
// endif Conditional region10
?>
</table> 
</div>

<h1 class="yellow">Personal Biography</h1>
<div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
      <?php 
// Show IF Conditional region7 
if (@$row_rsArchons['BIO'] != NULL) {
?>
        <tr>
          <td><?php echo nl2br($row_rsArchons['BIO']); ?></td>
        </tr>
        <?php 
// else Conditional region7
} else { ?>
<tr><td>A personal biography is not available for this archon.</td></tr>
<?php } 
// endif Conditional region7
?>
        </table>  
</div>

<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->
<!-- #BEGIN SECOND COLUMN -->
<div class="floatRight width305">
<!-- InstanceBeginEditable name="Column2" -->
  <h1 class="yellow">Boul&#233; Information</h1>
  <div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td colspan="2"><?php echo $row_rsArchons['BOULENAME']; ?> Boul&eacute;<br />
        <?php echo $row_rsArchons['BOULECITY']; ?>, <?php echo $row_rsArchons['BOULESTATECD']; ?><br />
        <?php echo $row_rsArchons['REGIONNAME']; ?> Region </td>
  </tr>
  <?php 
// Show IF Conditional region5 
if (@$row_rsArchons['JOINDATE'] != NULL) {
?>
    <tr>
      <td valign="top">MEMBER SINCE : </td>
      <td>
	    <?php 
// Show IF Conditional region10 
if (@$row_rsArchons['JOINDATE'] != NULL) {
?>
	        <?php echo date('F Y',strtotime($row_rsArchons['JOINDATE'])); ?>
	        <?php 
// else Conditional region10
} else { ?>
	      
  <?php } 
// endif Conditional region10
?>	  </td>
    </tr>
    <?php } 
// endif Conditional region5
?>
  <tr>
    <?php if ($totalRows_rsOffices > 0) { // Show if recordset not empty ?>
      <td valign="top">OFFICES HELD </td>
      <td><?php do { ?>
          <div class="list"><?php echo $row_rsOffices['CPOSITION']; ?>, <?php echo date('Y',strtotime($row_rsOffices['JOINDATE'])); ?></div>
          <?php } while ($row_rsOffices = mysqli_fetch_assoc($rsOffices)); ?></td>
      <?php } // Show if recordset not empty ?></tr>
</table>  
</div>
<?php 
// Show IF Conditional region5 
if (@$row_rsArchons['SPOUSENAME'] != NULL) {
?>
<h1 class="yellow">Personal Information </h1>
<div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
    <tr>
      <td valign="top">ARCHOUSA</td>
      <td><?php echo $row_rsArchons['SPOUSENAME']; ?></td>
    </tr>
</table>   
</div>
    <?php } 
// endif Conditional region5
?>

<h1 class="yellow">Skills &amp; Occupations </h1>
<div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
<?php if ($totalRows_rsArchonOcc == 0) { // Show if recordset empty ?>
      <tr>
        <td valign="top" colspan="2">Skills and occupations are  not available for this archon. </td>
      </tr>
<?php } // Show if recordset empty ?>	  
          <?php if ($totalRows_rsArchonOcc > 0) { // Show if recordset not empty ?>
      <tr>
            <td valign="top">OCCUPATION(S): </td>
          <td valign="top"><?php do { ?>
                    <?php echo $row_rsArchonOcc['DESCRIPTION']; ?><br />
          <?php } while ($row_rsArchonOcc = mysqli_fetch_assoc($rsArchonOcc)); ?></td>
      </tr>
          <?php } // Show if recordset not empty ?>
          <?php if ($totalRows_rsArchonTitles > 0) { // Show if recordset not empty ?>
      <tr>
            <td valign="top">AREA(S) OF EXPERTISE:</td>
          <td valign="top"><?php do { ?>
                  <?php echo $row_rsArchonTitles['JOBTITLE']; ?><br />
          <?php } while ($row_rsArchonTitles = mysqli_fetch_assoc($rsArchonTitles)); ?></td>
      </tr>
          <?php } // Show if recordset not empty ?>
    </table>   
</div>

<!-- InstanceEndEditable -->
</div><!-- #END SECOND COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysqli_free_result($rsArchons);

mysqli_free_result($rsProfessional);

mysqli_free_result($rsOffices);

mysqli_free_result($rsBoule);

mysqli_free_result($rsJobs);

mysqli_free_result($rsArchonOcc);

mysqli_free_result($rsArchonTitles);
?>
