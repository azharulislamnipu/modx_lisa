<?php
require_once('../includes/header.php');
require_once('../includes/classes/resize.php');
?>


<?php
	if (cG("pwd")&&is_numeric(cG("post"))){//delete ,activate or deactivate
		$action=cG("action");
		$post_password=cG("pwd");
		$post_id=cG("post");
		
		if ($action=="confirm"){//confirm a new post
			//update table
			$ocdb->update(TABLE_PREFIX."posts","isConfirmed=1","idPost=$post_id and password='$post_password'");
			
			//redirect to the item
			$query="select title,type,friendlyName,
			        (select friendlyName from ".TABLE_PREFIX."categories where idCategory=c.idCategoryParent limit 1) parent
					    from ".TABLE_PREFIX."posts p 
					    inner join ".TABLE_PREFIX."categories c
					    on c.idCategory=p.idCategory
					where idPost=$post_id and password='$post_password' and isConfirmed=1 Limit 1";
			$result=$ocdb->query($query);
			if (mysql_num_rows($result)){
				$row=mysql_fetch_assoc($result);
				$title=$row["title"];
				$postTitle=friendly_url($title);
				$postTypeName=getTypeName($row["type"]);
				$fcategory=$row["friendlyName"];
				$parent=$row["parent"];
					
				$postUrl=itemURL($post_id,$fcategory,$postTypeName,$postTitle,$parent);
				
				sendEmail(NOTIFY_EMAIL,"NEW ad in ".SITE_URL,"NEW ad ".SITE_URL."$postUrl");//email to the NOTIFY_EMAIL	
				
				if ($parent!="") $parent='#'.$parent.' ';
				post_to_twitter($title." #$postTypeName #$fcategory $parent",SITE_URL.$postUrl);//twitt the post into the set twitter account
				
				if (CACHE_DEL_ON_POST) deleteCache();//delete cache on post if is activated
				if (SITEMAP_DEL_ON_POST) generateSitemap();//new item generate sitemap
				
				alert(T_POST_ACTIVE);
				jsRedirect(SITE_URL.$postUrl);
			}
			
		}	
		elseif ($action=="deactivate"){
			$ocdb->update(TABLE_PREFIX."posts","isAvailable=0","idPost=$post_id and password='$post_password'");
			if (CACHE_DEL_ON_POST) deleteCache();//delete cache
			echo "<div id='sysmessage'>".T_POST_DEACTIVE."</div>";
		}
		elseif ($action=="activate"){
			$ocdb->update(TABLE_PREFIX."posts","isAvailable=1","idPost=$post_id and password='$post_password'");
			if (CACHE_DEL_ON_POST) deleteCache();//delete cache 
			echo "<div id='sysmessage'>".T_POST_ACTIVE."</div>";
		}
		elseif ($action=="delete"&&isset($_SESSION['admin'])){//only for admin
			$ocdb->delete(TABLE_PREFIX."posts","idPost=$post_id and password='$post_password'");
			removeRessource(IMG_UPLOAD_DIR.$post_id);//delete images! and folder
			if (CACHE_DEL_ON_POST) deleteCache();//delete cache
			echo "<div id='sysmessage'>".T_POST_DELETED."</div>";
		}
		elseif ($action=="spam"&&isset($_SESSION['admin'])){//only for admin mark as spam
			
			if (AKISMET!=""){//report akismet
				$query="select name,email,description,ip from ".TABLE_PREFIX."posts where idPost=$post_id and password='$post_password' Limit 1";
				$result=$ocdb->query($query);
				if (mysql_num_rows($result)){
					$row=mysql_fetch_assoc($result);
						$akismet = new Akismet(SITE_URL ,AKISMET);
						$akismet->setCommentAuthor($row["name"]);
						$akismet->setCommentAuthorEmail($row["email"]);
						$akismet->setCommentContent($row["description"]);
						$akismet->setUserIP($row["ip"]);//ip of the bastard!
						$akismet->submitSpam();
						$akismet->submitHam();
				}
			}
			
			$ocdb->update(TABLE_PREFIX."posts","isAvailable=2","idPost=$post_id and password='$post_password'");//set post as spam state 2
			$ocdb->delete(TABLE_PREFIX."postsimages","idPost=$post_id");// delete the images cuz of spammer
			removeRessource("../".IMG_UPLOAD_DIR.$post_id);//delete images! and folder
			if (CACHE_DEL_ON_POST) deleteCache();//delete cache
			
			echo "<div id='sysmessage'>".T_SPAM_REPORTED."</div>";
		}
		elseif ($action=="edit"){//edit post
			if ($_POST){//update post
				if(cP("math") == $_SESSION["mathCaptcha"])	{//everything ok

				    $image_check=check_images_form();//echo $image_check;
					
				    if (is_numeric($image_check)){//if the images were right, or not any image uploaded
					    if (is_numeric(cP("price"))) $price=cP("price");
					    else $price=0;
					    //DB update				
					    if (HTML_EDITOR) $desc=cPR("description");
					    else $desc=cP("description");
					    $title=cP("title");	
					    $ocdb->update(TABLE_PREFIX."posts",
								    "idCategory=".cP("category").",type=".cP("type").",
								    title='$title',description='$desc',price=$price,
								    place='".cP("place")."',name='".cP("name")."',
								    phone='".cP("phone")."',ip='$client_ip' ",
								    "idPost=$post_id and password='$post_password' Limit 1");
					    if (CACHE_DEL_ON_POST) deleteCache();//delete cache on post
					    //end DB update
					
					    if ($image_check>1){//something to upload
						    //delete previous images
						    $ocdb->delete(TABLE_PREFIX."postsimages","idPost=$post_id");
						    removeRessource(IMG_UPLOAD_DIR.$post_id);//delete images! and folder
						    upload_images_form($post_id,$title);
					    }
					    //end images	
					    echo "<div id='sysmessage'>".T_POST_UPDATED."</div>";
					    //for preventing f5
					    $_SESSION["mathCaptcha"]="";
					}//image check
				    else echo "<div id='sysmessage'>".$image_check."</div>";//end upload verification
				}//end captcha
			    else echo "<div id='sysmessage'>".T_WRONG_CAPTCHA."</div>";
			}
			
			
			$query="select p.*,friendlyName,
			        (select friendlyName from ".TABLE_PREFIX."categories where idCategory=c.idCategoryParent limit 1) parent
					    from ".TABLE_PREFIX."posts p 
					    inner join ".TABLE_PREFIX."categories c
					    on c.idCategory=p.idCategory
					where idPost=$post_id and password='$post_password' and isAvailable!=2 Limit 1";
			$result=$ocdb->query($query);
			if (mysql_num_rows($result)){
				$row=mysql_fetch_assoc($result);
				
				if($row['isConfirmed']!=1) {//the ad is not confirmed!
					$linkConfirm=SITE_URL."/manage/?post=$post_id&pwd=$post_password&action=confirm";
					echo "<b><a href='$linkConfirm'>".T_NOTIFY_EMAIL_1."</a></b><br />";
				}
				
				if($row['isAvailable']==1){//able to deactivate it
					$linkDeactivate=SITE_URL."/manage/?post=$post_id&pwd=$post_password&action=deactivate";
					echo "<a href='$linkDeactivate'>".T_NOTIFY_EMAIL_2."</a>";
				}
				else {//activate it
					$linkActivate=SITE_URL."/manage/?post=$post_id&pwd=$post_password&action=activate";
					echo "<a href='$linkActivate'>".T_ACTIVATE."</a>";
				}
				
				$postTitle=$row["title"];
				$postTitleF=friendly_url($postTitle);
				$postTypeName=getTypeName($row["type"]);
				$fcategory=$row["friendlyName"];
				$parent=$row["parent"];
				
				$postUrl=itemURL($post_id,$fcategory,$postTypeName,$postTitleF,$parent);	
				
				?>
					<h3><?php echo T_EDIT_ITEM;?>: <a target="_blank" href="<?php echo SITE_URL.$postUrl;?>"><?php echo $postTitle;?></a></h3>
					
					<form action="" method="post" onsubmit="return checkForm(this);" enctype="multipart/form-data">
						<?php echo T_CATEGORY;?>:<br />
						<?php 
						$query="SELECT idCategory,name,(select name from ".TABLE_PREFIX."categories where idCategory=C.idCategoryParent) FROM ".TABLE_PREFIX."categories C order by idCategoryParent";
						sqlOptionGroup($query,"category",$row["idCategory"]);
						?>
						<br />
						
						<?php echo T_TYPE;?>:<br />
						<select id="type" name="type">
							<option value="<?php echo TYPE_OFFER;?>" <?php if($row['type']==TYPE_OFFER)echo 'selected="selected"';?> ><?php echo TYPE_OFFER_NAME;?></option>
							<option value="<?php echo TYPE_NEED;?>"  <?php if($row['type']==TYPE_NEED)echo 'selected="selected"';?> ><?php echo TYPE_NEED_NAME;?></option>
						</select>
						<br />
						<br />
						
						<?php echo T_TITLE;?>*:<br />
						<input id="title" name="title" type="text" value="<?php echo $postTitle;?>" size="61" maxlength="120" onblur="validateText(this);"  lang="false" />
						<input id="price" name="price" type="text" size="3" value="<?php echo $row["price"];?>" maxlength="8"  onkeypress="return isNumberKey(event);"   /><?php echo CURRENCY;?><br />
						<br />
						
						<?php echo T_DESCRIPTION;?>*:<br />
						<?php if (HTML_EDITOR){?>
						    <script type="text/javascript">var SITE_URL="<?php echo SITE_URL;?>";</script>
							<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/js/nicEdit.js"></script>
							<script type="text/javascript">
							//<![CDATA[
								bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
								//]]>
							</script>
							<textarea rows="10" cols="79" name="description" id="description"><?php echo stripslashes($row['description']);?></textarea>
						<?php }else{?>
							<textarea rows="10" cols="79" name="description" id="description" onblur="validateText(this);"  lang="false"><?php echo strip_tags($row['description']);?></textarea><?php }?>
						<br />
						
						<?php echo T_PLACE;?>:<br />
						<?php if (MAP_KEY==""){//not google maps?>
						<input id="place" name="place" type="text" value="<?php echo $row["place"];?>" size="69" maxlength="120" /><br />
						<?php }
						else{//google maps
							if ($_POST["place"]!="") $m_value=$_POST["place"];
							elseif($row["place"]!="") $m_value=$row["place"];
							else $m_value=MAP_INI_POINT;
						?>
						<input id="place" name="place" type="text" value="<?php echo $m_value;?>" onblur="showAddress(this.value);" size="69" maxlength="120" /><br />
						<div id="map" style="width: 100%; height: 200px;"></div>
						<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php echo MAP_KEY;?>" type="text/javascript"></script>
						<script type="text/javascript">var init_street="<?php echo MAP_INI_POINT;?>";</script>
						<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/js/map.js"></script>
						<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/js/mapSmall.js"></script>
						<script type="text/javascript">showAddress("<?php echo $m_value;?>");</script>
						<?php }?>
						
						<?php echo T_NAME;?>*:<br />
						<input id="name" name="name" type="text" value="<?php echo $row["name"];?>" maxlength="75" onblur="validateText(this);"  lang="false"  /><br />
	
						<?php echo T_PHONE;?>:<br />
						<input id="phone" name="phone" type="text" value="<?php echo $row["phone"];?>" maxlength="11"/><br />
						
						<?php if (MAX_IMG_NUM>0){
						echo "<br />".T_UPLOAD_MAX.": ".(MAX_IMG_SIZE/1000000)."Mb ".T_FORMAT." ".IMG_TYPES."<br />";
						echo "<input type='hidden' name='MAX_FILE_SIZE' value='".MAX_IMG_SIZE."' />";
						echo "<b>".T_IMAGE_WARNING."</b><br />";?>
						
							<?php 
								$imgDir=SITE_URL.IMG_UPLOAD.$post_id."/";
								$query="select FileName from ".TABLE_PREFIX."postsimages where idPost=$post_id order by FileName Limit ".MAX_IMG_NUM;	
								$result2 =	$ocdb->query($query);
								$i=1;
								while ($row2=mysql_fetch_assoc($result2))
								{
									$imgName=$row2["FileName"];
								 	echo "<a href='$imgDir$imgName' title='$postTitle ".T_PICTURE." $i' target='_blank'>
								 			<img class='thumb' src='".$imgDir."thumb_$imgName' title='$postTitle ".T_PICTURE." $i' alt='postTitle ".T_PICTURE." $i' /></a>";
								 	$i++;
								}
								for ($i=1;$i<=MAX_IMG_NUM;$i++){
									?><br /><label><?php echo T_PICTURE;?> <?php echo $i?>:</label><input type="file" name="pic<?php echo $i?>" id="pic<?php echo $i?>" value="<?php echo $_POST["pic".$i];?>" /><?php
								 }
						
						 }?>
						<br /><br />
						<?php  mathCaptcha();?>
						<input id="math" name="math" type="text" size="2" maxlength="2"  onblur="validateNumber(this);"  onkeypress="return isNumberKey(event);" lang="false" />
						<br /><br />
						<input type="submit" id="submit" value="<?php echo T_UPDATE;?>" />
						
					</form>
					
				<?php 
			}
			else echo T_NOTHING_FOUND;//nothing returned for that item		
		}
	}
			
	
require_once('../includes/footer.php');
?>


