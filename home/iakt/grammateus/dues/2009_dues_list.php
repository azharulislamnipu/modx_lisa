<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Filter
$tfi_listrsDuesList1 = new TFI_TableFilter($sigma_modx, "tfi_listrsDuesList1");
$tfi_listrsDuesList1->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listrsDuesList1->addColumn("STATUS", "STRING_TYPE", "STATUS", "%");
$tfi_listrsDuesList1->addColumn("dues_start_dt", "DATE_TYPE", "dues_start_dt", "=");
$tfi_listrsDuesList1->addColumn("status_req", "STRING_TYPE", "status_req", "%");
$tfi_listrsDuesList1->addColumn("dues_amt", "NUMERIC_TYPE", "dues_amt", "=");
$tfi_listrsDuesList1->Execute();

// Sorter
$tso_listrsDuesList1 = new TSO_TableSorter("rsDuesList", "tso_listrsDuesList1");
$tso_listrsDuesList1->addColumn("FULLNAME");
$tso_listrsDuesList1->addColumn("STATUS");
$tso_listrsDuesList1->addColumn("dues_start_dt");
$tso_listrsDuesList1->addColumn("status_req");
$tso_listrsDuesList1->addColumn("dues_amt");
$tso_listrsDuesList1->setDefault("FULLNAME");
$tso_listrsDuesList1->Execute();

// Navigation
$nav_listrsDuesList1 = new NAV_Regular("nav_listrsDuesList1", "rsDuesList", "../../", $_SERVER['PHP_SELF'], 10);

// Begin List Recordset
$maxRows_rsDuesList = $_SESSION['max_rows_nav_listrsDuesList1'];
$pageNum_rsDuesList = 0;
if (isset($_GET['pageNum_rsDuesList'])) {
  $pageNum_rsDuesList = $_GET['pageNum_rsDuesList'];
}
$startRow_rsDuesList = $pageNum_rsDuesList * $maxRows_rsDuesList;
$colname__rsDuesList = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsDuesList = $_SESSION['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter__rsDuesList = "1=1";
if (isset($_SESSION['filter_tfi_listrsDuesList1'])) {
  $NXTFilter__rsDuesList = $_SESSION['filter_tfi_listrsDuesList1'];
}
// Defining List Recordset variable
$NXTSort__rsDuesList = "FULLNAME";
if (isset($_SESSION['sorter_tso_listrsDuesList1'])) {
  $NXTSort__rsDuesList = $_SESSION['sorter_tso_listrsDuesList1'];
}
$query_rsDuesList = sprintf("SELECT vw_gramm_archons.CUSTOMERID,   vw_gramm_archons.BOULENAME,   vw_gramm_archons.CHAPTERID,   vw_gramm_archons.`STATUS`,   vw_gramm_archons.FULLNAME,   spp_dues.dues_id,   spp_dues.dues_start_dt,   spp_dues.dues_amt,   spp_dues.status_req FROM vw_gramm_archons   INNER JOIN spp_dues ON (vw_gramm_archons.CUSTOMERID = spp_dues.CUSTOMERCD) WHERE vw_gramm_archons.`STATUS` <> 'Deceased' AND spp_dues.dues_start_dt > '2008-12-31' AND BOULENAME = %s AND  {$NXTFilter__rsDuesList} ORDER BY {$NXTSort__rsDuesList} ", GetSQLValueString($colname__rsDuesList, "text"));
$rsDuesList = $sigma_modx->SelectLimit($query_rsDuesList, $maxRows_rsDuesList, $startRow_rsDuesList) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsDuesList'])) {
  $totalRows_rsDuesList = $_GET['totalRows_rsDuesList'];
} else {
  $all_rsDuesList = $sigma_modx->SelectLimit($query_rsDuesList) or die($sigma_modx->ErrorMsg());
  $totalRows_rsDuesList = $all_rsDuesList->RecordCount();
}
$totalPages_rsDuesList = (int)(($totalRows_rsDuesList-1)/$maxRows_rsDuesList);
// End List Recordset

$nav_listrsDuesList1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: false,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_FULLNAME {width:154px; overflow:hidden;}
  .KT_col_STATUS {width:84px; overflow:hidden;}
  .KT_col_dues_start_dt {width:140px; overflow:hidden;}
  .KT_col_status_req {width:84px; overflow:hidden;}
  .KT_col_dues_amt {width:56px; overflow:hidden;}
</style><!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listrsDuesList1">
  <h1> 2009 DUES PAYMENTS AND ROSTER
    <?php
  $nav_listrsDuesList1->Prepare();
  require("../../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div style="padding: 2px 6px;"><p>INSTRUCTIONS</p>
        <p>Use this page to create the roster of Archons who are members of your boul&eacute; and for whom you will be submitting payments with your 2009 Membership Report. Your final list will consist of the members of your boul&eacute;, the status you have requested for them and the amount of the payment you will be submitting for them in the 2009 Membership Report. Print a copy of your dues payments and submit it with your completed membership report. </p>
        <ul>
	      <li>Use the "add new" link at the bottom right to add 2009 Dues payments for Archons in your Boul&eacute;.<br>
You may add up to nine (9) payments at a time.</li>
          <li>If an Archon's status is Emeritus, Inductee, Voluntary or Involuntary Inactive, the payment should be $0. </li>
          <li>If you do not indicate that a payment is being submitted for a particular Archon, his payment for 2009 will be listed as $0. </li>
          <li>To view all payments on one page, click the 'Show All Records' link below.</li>
	      <li>To print a copy of your 2009 Dues Payments, <a href="/home/dues_print.php">click this link</a>. Please make sure your list includes all the members of your boul&eacute; with any payments collected. Submit this copy with your completed report. </li>
	    </ul>
      </div>
	  <div class="KT_options"> <a href="<?php echo $nav_listrsDuesList1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
        <?php 
  // Show IF Conditional region1 
  if (@$_GET['show_all_nav_listrsDuesList1'] == 1) {
?>
          <?php echo $_SESSION['default_max_rows_nav_listrsDuesList1']; ?>
          <?php 
  // else Conditional region1
  } else { ?>
          <?php echo NXT_getResource("all"); ?>
          <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                            <?php 
  // Show IF Conditional region2 
  if (@$_SESSION['has_filter_tfi_listrsDuesList1'] == 1) {
?>
                              <a href="<?php echo $tfi_listrsDuesList1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                              <?php 
  // else Conditional region2
  } else { ?>
                              <a href="<?php echo $tfi_listrsDuesList1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                              <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>            </th>
            <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listrsDuesList1->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listrsDuesList1->getSortLink('FULLNAME'); ?>">ARCHON</a> </th>
            <th id="STATUS" class="KT_sorter KT_col_STATUS <?php echo $tso_listrsDuesList1->getSortIcon('STATUS'); ?>"> <a href="<?php echo $tso_listrsDuesList1->getSortLink('STATUS'); ?>">CURRENT STATUS</a> </th>
            <th id="dues_start_dt" class="KT_sorter KT_col_dues_start_dt <?php echo $tso_listrsDuesList1->getSortIcon('dues_start_dt'); ?>"> <a href="<?php echo $tso_listrsDuesList1->getSortLink('dues_start_dt'); ?>">DUES STARTING YEAR</a> </th>
            <th id="status_req" class="KT_sorter KT_col_status_req <?php echo $tso_listrsDuesList1->getSortIcon('status_req'); ?>"> <a href="<?php echo $tso_listrsDuesList1->getSortLink('status_req'); ?>">REQUESTED STATUS</a> </th>
            <th id="dues_amt" class="KT_sorter KT_col_dues_amt <?php echo $tso_listrsDuesList1->getSortIcon('dues_amt'); ?>"> <a href="<?php echo $tso_listrsDuesList1->getSortLink('dues_amt'); ?>">DUES COLLECTED</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3 
  if (@$_SESSION['has_filter_tfi_listrsDuesList1'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listrsDuesList1_FULLNAME" id="tfi_listrsDuesList1_FULLNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsDuesList1_FULLNAME']); ?>" size="25" maxlength="20" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="submit" name="tfi_listrsDuesList1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsDuesList == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="7"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsDuesList > 0) { // Show if recordset not empty ?>
            <?php
  while (!$rsDuesList->EOF) { 
?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_spp_dues" class="id_checkbox" value="<?php echo $rsDuesList->Fields('dues_id'); ?>" />
                    <input type="hidden" name="dues_id" class="id_field" value="<?php echo $rsDuesList->Fields('dues_id'); ?>" />                </td>
                <td><div class="KT_col_FULLNAME"><?php echo KT_FormatForList($rsDuesList->Fields('FULLNAME'), 22); ?></div></td>
                <td><div class="KT_col_STATUS"><?php echo KT_FormatForList($rsDuesList->Fields('STATUS'), 12); ?></div></td>
                <td><div class="KT_col_dues_start_dt"><?php echo KT_formatDate($rsDuesList->Fields('dues_start_dt')); ?></div></td>
                <td><div class="KT_col_status_req"><?php echo KT_FormatForList($rsDuesList->Fields('status_req'), 12); ?></div></td>
                <td><div class="KT_col_dues_amt"><?php echo KT_FormatForList($rsDuesList->Fields('dues_amt'), 8); ?></div></td>
                <td><a class="KT_edit_link" href="/home/dues_edit.php?dues_id=<?php echo $rsDuesList->Fields('dues_id'); ?>&amp;CUSTOMERID=<?php echo $rsDuesList->Fields('CUSTOMERID'); ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
              </tr>
              <?php
    $rsDuesList->MoveNext(); 
  }
?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsDuesList1->Prepare();
            require("../../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations">&nbsp;</div>
<span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
        </select>
        <a class="KT_additem_op_link" href="/home/dues_detail.php?KT_back=1" onClick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a></div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
  <!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsDuesList->Close();
?>
