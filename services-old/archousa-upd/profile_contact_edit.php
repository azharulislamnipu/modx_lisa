<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("PREFIX", true, "text", "", "", "", "");
$formValidation->addField("FIRSTNAME", true, "text", "", "", "", "");
$formValidation->addField("LASTNAME", true, "text", "", "", "", "");
$formValidation->addField("EMAIL", true, "text", "", "", "", "You must provide your Archousa's email address.");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsHon = "SELECT * FROM spp_hon ORDER BY DESIGNATIONLST ASC";
$rsHon = mysql_query($query_rsHon, $sigma_modx) or die(mysql_error());
$row_rsHon = mysql_fetch_assoc($rsHon);
$totalRows_rsHon = mysql_num_rows($rsHon);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsSuffix = "SELECT * FROM spp_suffix ORDER BY SUFFIX ASC";
$rsSuffix = mysql_query($query_rsSuffix, $sigma_modx) or die(mysql_error());
$row_rsSuffix = mysql_fetch_assoc($rsSuffix);
$totalRows_rsSuffix = mysql_num_rows($rsSuffix);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsSuffix1 = "SELECT * FROM spp_suffix ORDER BY SUFFIX ASC";
$rsSuffix1 = mysql_query($query_rsSuffix1, $sigma_modx) or die(mysql_error());
$row_rsSuffix1 = mysql_fetch_assoc($rsSuffix1);
$totalRows_rsSuffix1 = mysql_num_rows($rsSuffix1);

// Make an update transaction instance
$upd_spp_archousa = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archousa);
// Register triggers
$upd_spp_archousa->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archousa->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archousa->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-profile.php");
// Add columns
$upd_spp_archousa->setTable("spp_archousa");
$upd_spp_archousa->addColumn("PREFIX", "STRING_TYPE", "POST", "PREFIX");
$upd_spp_archousa->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$upd_spp_archousa->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$upd_spp_archousa->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$upd_spp_archousa->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$upd_spp_archousa->addColumn("DESIGNATIONLST", "STRING_TYPE", "POST", "DESIGNATIONLST");
$upd_spp_archousa->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$upd_spp_archousa->addColumn("BIRTHDATE", "DATE_TYPE", "POST", "BIRTHDATE");
$upd_spp_archousa->addColumn("ANNIVERSARYDATE", "DATE_TYPE", "POST", "ANNIVERSARYDATE");
$upd_spp_archousa->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_archousa->addColumn("MOBILEPHONE", "STRING_TYPE", "POST", "MOBILEPHONE");
$upd_spp_archousa->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "LASTUPDATED");
$upd_spp_archousa->setPrimaryKey("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archousa = $tNGs->getRecordset("spp_archousa");
$row_rsspp_archousa = mysql_fetch_assoc($rsspp_archousa);
$totalRows_rsspp_archousa = mysql_num_rows($rsspp_archousa);
?><!DOCTYPE html>
<html class="no-js"><!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --> <!--<![endif]-->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-meta.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-css.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-javascript.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-links.php"); ?>

    <!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/JSRecordset.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/BaseListSorter.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MenuMover.js"></script>
<?php
//begin JSRecordset
$jsObject_rsHon = new WDG_JsRecordset("rsHon");
echo $jsObject_rsHon->getOutput();
//end JSRecordset
?>
<script type="text/javascript" src="../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../includes/resources/calendar.js"></script>
<!-- InstanceEndEditable -->
</head>

<body class="home blue-bar [!colorPalette? &palette_default=`[*palette_default*]` &palette_loggedin=`[*palette_loggedin*]` !]">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/main-nav.php"); ?>
			<div class="width">
			    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/global-logo-tagline.php"); ?>
			</div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
	<!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- InstanceBeginEditable name="pagetitle" -->
                            <section class="span-12 margin-bottom-medium">
                            <div class="inner">
                                     <h1>Profile</h1>
                                    <ol class="breadcrumbs" style="margin-top:-18px;">
                                      <li><a class="B_homeCrumb" href="/home/archon-home.php" title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                                      <li><span class="B_currentCrumb">Administration</span></li>
                                    </ol>
                                       <div>
                               </section>
                               <!-- InstanceEndEditable -->
                               
                               <section class="span-12 margin-bottom-medium">
                               <div class="inner">
                               <!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>

  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="PREFIX">SALUTATION:</label></td>
        <td><input type="text" name="PREFIX" id="PREFIX" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['PREFIX']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("PREFIX");?> <?php echo $tNGs->displayFieldError("spp_archousa", "PREFIX"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="FIRSTNAME">FIRST NAME:</label></td>
        <td><input type="text" name="FIRSTNAME" id="FIRSTNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['FIRSTNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("spp_archousa", "FIRSTNAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="MIDDLEINITIAL">MIDDLE INITIAL:</label></td>
        <td><input type="text" name="MIDDLEINITIAL" id="MIDDLEINITIAL" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['MIDDLEINITIAL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MIDDLEINITIAL");?> <?php echo $tNGs->displayFieldError("spp_archousa", "MIDDLEINITIAL"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="LASTNAME">LAST NAME:</label></td>
        <td><input type="text" name="LASTNAME" id="LASTNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['LASTNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("spp_archousa", "LASTNAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="SUFFIX">SUFFIX:</label></td>
        <td><select name="SUFFIX" id="SUFFIX">
            <?php 
do {  
?>
            <option value="<?php echo $row_rsSuffix['SUFFIX']?>"<?php if (!(strcmp($row_rsSuffix['SUFFIX'], $row_rsspp_archousa['SUFFIX']))) {echo "SELECTED";} ?>><?php echo $row_rsSuffix['SUFFIX']?></option>
            <?php
} while ($row_rsSuffix = mysql_fetch_assoc($rsSuffix));
  $rows = mysql_num_rows($rsSuffix);
  if($rows > 0) {
      mysql_data_seek($rsSuffix, 0);
	  $row_rsSuffix = mysql_fetch_assoc($rsSuffix);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archousa", "SUFFIX"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="DESIGNATIONLST">HONORIFIC(S):</label></td>
        <td><input name="DESIGNATIONLST" id="DESIGNATIONLST" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['DESIGNATIONLST']); ?>" size="8" wdg:recordset="rsHon" wdg:subtype="MenuMover" wdg:type="widget" wdg:displayfield="DESIGNATIONLST" wdg:valuefield="DESIGNATIONLST" wdg:sortselector="yes" />
            <?php echo $tNGs->displayFieldHint("DESIGNATIONLST");?> <?php echo $tNGs->displayFieldError("spp_archousa", "DESIGNATIONLST"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="EMAIL">CONTACT EMAIL:</label></td>
        <td><input type="text" name="EMAIL" id="EMAIL" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['EMAIL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("EMAIL");?> <?php echo $tNGs->displayFieldError("spp_archousa", "EMAIL"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="MOBILEPHONE">MOBILE PHONE:</label></td>
        <td><input type="text" name="MOBILEPHONE" id="MOBILEPHONE" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['MOBILEPHONE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MOBILEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "MOBILEPHONE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="BIRTHDATE">BIRTH DATE:</label></td>
        <td><input name="BIRTHDATE" id="BIRTHDATE" value="<?php echo KT_formatDate($row_rsspp_archousa['BIRTHDATE']); ?>" size="32" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
            <?php echo $tNGs->displayFieldHint("BIRTHDATE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "BIRTHDATE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ANNIVERSARYDATE">WEDDING ANNIVERSARY DATE:</label></td>
        <td><input name="ANNIVERSARYDATE" id="ANNIVERSARYDATE" value="<?php echo KT_formatDate($row_rsspp_archousa['ANNIVERSARYDATE']); ?>" size="32" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
            <?php echo $tNGs->displayFieldHint("ANNIVERSARYDATE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ANNIVERSARYDATE"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Update Personal Information" /> 
        <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/archousa-profile.php')" />
        </td>
      </tr>
    </table>
    <input type="hidden" name="UPDATEDBY" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['UPDATEDBY']); ?>" />
    <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo KT_formatDate($row_rsspp_archousa['LASTUPDATED']); ?>" />
    <input type="hidden" name="ARCHOUSAID" id="ARCHOUSAID" value="<?php echo KT_formatDate($_POST['ARCHOUSAID']); ?>" />
 </form>

<!-- InstanceEndEditable -->
                                 <div>
                               </section>
                                   <!-- end showing the content, pagetitle, breadcrumb and sidebar -->
                                   <!-- 3 round images toggle start -->
                                   <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/row-spotlight-grid-12.php"); ?>
                                   <!-- 3 round images toggle end -->
                             </div>
    </div>

            <!-- main content body end -->	
	
	<!-- third section event and articles start -->
  	 <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
	<!-- third section event and articles end -->
	
    <footer class="width padding" style="border-top: 10px #52B609 solid;">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-content-default.php"); ?>
      </div>
    </footer>
	
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-scripts.php"); ?>
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsHon);

mysql_free_result($rsSuffix);

mysql_free_result($rsSuffix1);
?>
