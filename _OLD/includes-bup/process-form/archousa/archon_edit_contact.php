<?php
$data = $response = array();
parse_str($_POST['data'], $data);
array_walk($data, 'test_in_array');
$response['type'] = 'success';
/*$show_hide = 'hide';

if($data['MARITALSTT'] == 1){
    $show_hide = '';
}*/

//var_dump($data['BIRTHDATE']);

/*if(empty($data['MARITALSTT'])){
    $data['SPOUSENAME'] = '';
}*/

$date = date_create($data['BIRTHDATE']);
$data['BIRTHDATE'] = date_format($date,"Y-m-d");

//var_dump($data['BIRTHDATE']);

if (filter_var($data['EMAIL'], FILTER_VALIDATE_EMAIL) === false) {
    $response['type'] = 'error';
    $response['html'] = '<p>Email is not valid</p>';
}

$same_values = array(
    'PREFIX', 'SUFFIX', 'FIRSTNAME', 'MIDDLEINITIAL', 'LASTNAME',
    'DESIGNATIONLST', 'ADDRESS1', 'ADDRESS2', 'CITY', 'STATECD',
    'ZIP', 'HOMEPHONE', 'MOBILEPHONE', 'EMAIL', 'BIRTHDATE',
    'MARITALSTT', 'SPOUSENAME',
);

if($response['type'] !== 'error'){
    $db = get_db();

    $res_data = array();

    foreach ($same_values as $key) {
        if(empty($data[$key])){
            $res_data[$key] = '';
        } else {
            $res_data[$key] = $data[$key];
        }
    }
    $data = $res_data;
    $data['LASTUPDATED'] = date("Y-m-d H:i:s");
    $data['UPDATEDBY'] = $updated_by;


    /*$data = Array (
        'PREFIX'            => $data['PREFIX'],
        'SUFFIX'            => $data['SUFFIX'],
        'FIRSTNAME'         => $data['FIRSTNAME'],
        'MIDDLEINITIAL'     => $data['MIDDLEINITIAL'],
        'LASTNAME'          => $data['LASTNAME'],
        'DESIGNATIONLST'    => $data['DESIGNATIONLST'],
        'ADDRESS1'          => $data['ADDRESS1'],
        'ADDRESS2'          => $data['ADDRESS2'],
        'CITY'              => $data['CITY'],
        'STATECD'           => $data['STATECD'],
        'ZIP'               => $data['ZIP'],
        'HOMEPHONE'         => $data['HOMEPHONE'],
        'MOBILEPHONE'       => $data['MOBILEPHONE'],
        'EMAIL'             => $data['EMAIL'],
        'BIRTHDATE'         => $data['BIRTHDATE'],
        'MARITALSTT'        => $data['MARITALSTT'],
        'SPOUSENAME'        => $data['SPOUSENAME'],
        'LASTUPDATED'       => date("Y-m-d H:i:s"),
        'UPDATEDBY'         => $updated_by,
    );*/
    $db->where ('CUSTOMERID', $user_id);
    if ($db->update ('spp_archons', $data)){
        $ardb = get_db();
        $ar_data = array(
            'FIRSTNAME' => $data['SPOUSENAME']
        );
        $ardb->where ('CUSTOMERCD', $user_id);

        $ardb->update ('spp_archousa', $ar_data);

        $response['type'] = 'success';
        $response['html'] = "
<p><strong>Mailing Address</strong><br>
    {$data['ADDRESS1']}{$data['ADDRESS2']} <br>
    {$data['CITY']}, {$data['STATECD']} {$data['ZIP']} </p>

<p><strong>Email</strong><br>
    {$data['EMAIL']}</p>

<p><strong>Home Phone</strong><br>
    {$data['HOMEPHONE']}</p>

<p><strong>Mobile</strong><br>
{$data['MOBILEPHONE']}
    </p>

<p><strong>Birthdate</strong><br>
    " . date('F d', strtotime($data['BIRTHDATE'])) ."
</p>

<div class=\"archousa_block \" data-maritalstt=\"{$data['MARITALSTT']}\">
<strong> Archousa Name</strong>
<p>{$data['SPOUSENAME']}</p>
<p class=\"btn_p\"><a class=\"button small_button\" id=\"view_archon\" href=\"/home/archousa-profile.php\" target=\"_blank\">View</a> /
<a class=\"button small_button\" id=\"edit_archon\" href=\"/home/archousa-profile.php?edit_arc=1\" target=\"_blank\">Edit</a>
</p>
</div>
";
    $response['profile_title'] = "{$data['FIRSTNAME']}' {$data['MIDDLEINITIAL']}' {$data['LASTNAME']}' {$data['SUFFIX']}'";

    } else{
        $response['type'] = 'error';
//        $response['html'] = $db->getLastError();
        $response['html'] = '<p>Error occurred! Please try again.</p>';
    }
}

die(json_encode($response));