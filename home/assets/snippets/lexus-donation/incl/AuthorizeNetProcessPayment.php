<?php

/**
 * Submits a transaction to Authorize.net for processing
 * Author: Pat Heard {http://fullahead.org}
 */

//require_once '/opt/lampp/htdocs/modx_lisa/home/assets/snippets/anet/AuthorizeNet.php';
require_once 'anet_php_sdk/AuthorizeNet.php';
require_once 'AuthorizeNetUserInfo.php';

define("AUTHORIZENET_API_LOGIN_ID", "5T22jZbwWcu");  		// Your API LOGIN ID
define("AUTHORIZENET_TRANSACTION_KEY","7cr5MvY8D5W9v999");	// Your API transaction key
define("AUTHORIZENET_SANDBOX", true);       				// Set to false to test against production
define("TEST_REQUEST", "FALSE");           					// Set to true if testing against production	

/**
 * Process an Authorize.net Payment
 */
class AuthorizeNetProcessPayment{
	
	public $userInfo = null;
	
        public function __construct(array $args = null) {
		if (AUTHORIZENET_API_LOGIN_ID == "") {
			die('Enter your merchant credentials in AuthorizeNetPayment before running the app.');
		} 
		$this->userInfo = new AuthorizeNetUserInfo($args);
        }
	
        public function getUserInfo(){
            return $this->userInfo;
        }
        
	/** Validates that the info is present and correct for an Authorize.net transaction */
	public function validate(){		
		return $this->userInfo->validate();
	}	
	
	/** Process a credit card payment */
	public function process(){	
		$transaction = new AuthorizeNetAIM();
		$transaction->setSandbox(AUTHORIZENET_SANDBOX);
		$transaction->setFields($this->userInfo->getTransactionFields());
		return $transaction->authorizeAndCapture();
	}	
}

?>