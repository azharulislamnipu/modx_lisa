<?php get_header() ?>

    <div id="content">
        <div class="padder">

        <?php do_action( 'bp_before_blog_page' ) ?>

        <div class="page" id="blog-page">

            <?php if (have_posts()) : the_post(); ?>

                <h2 class="pagetitle"><?php the_title(); ?></h2>

                <div class="post" id="post-<?php the_ID(); ?>">

                    <div class="entry">

                        <?php the_content( __( '<p class="serif">Read the rest of this page &rarr;</p>', 'buddypress' ) ); ?>

                        <?php get_page_children_excerpts($post->ID, -1); ?>

                        <div id="paging">
                            <div class="left"><?php next_posts_link('&laquo; Previous Page') ?></div>
                            <div class="right"><?php previous_posts_link('Next Page &raquo;') ?></div>
                        </div>

                        <div class="clear">
                            <?php
                                wp_reset_query();
                                edit_post_link( __( 'Edit this entry.', 'buddypress' ), '<p>', '</p>');
                             ?>
                        </div>

                    </div>

                </div>

            <?php endif; ?>





        </div><!-- .page -->

        <?php do_action( 'bp_after_blog_page' ) ?>

        </div><!-- .padder -->
    </div><!-- #content -->

    <?php locate_template( array( 'sidebar.php' ), true ) ?>

<?php get_footer(); ?>
