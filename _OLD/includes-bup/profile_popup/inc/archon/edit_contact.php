<div id="archon_edit_contact" class="accordion parent-elem edit_contact" data-accordion>
    <div data-control><h3 class="text-center">Edit Contact Information</h3></div>
    <div class="ymp_form_input" data-content>

        <div class="data-content-inner edit_contact_inner">
            <h4>Personal Detail</h4>
            <div class="clearfix ymp_wrapper grid-12">
                <div class="ymp-row">
                    <div class="ymp-names table-wrap">
                        <div class="table-row">
                            <div class="table-cell">
                                <label for="PREFIX">Prefix</label>
                                <select name="PREFIX" id="PREFIX">
                                    <option value=""><?php echo "Select one..."; ?></option>
                                    <?php foreach (array_column($all_prefix['data'], 'PREFIX') as $prefix){
                                        echo "<option value='$prefix'" . selected($prefix, $user_data['PREFIX'], false) . ">$prefix</option>";
                                    }?>
                                </select>

                            </div><!--end cell-->
                            <div class="table-cell">
                                <label for="PREFIX">First Name <span class="required_ind">*</span></label>
                                <input class="input-required input-validated" type="text" name="FIRSTNAME" size="20" maxlength="40" id="FIRSTNAME" value="<?php echo $user_data['FIRSTNAME'];?>" placeholder="Enter First Name">
                            </div>
                            <div class="table-cell">
                                <label for="PREFIX">Middle Name</label>
                                <input type="text" name="MIDDLEINITIAL" size="20" maxlength="40" id="MIDDLEINITIAL" value="<?php echo $user_data['MIDDLEINITIAL'];?>" placeholder="Enter Middle Initail">
                            </div>
                            <div class="table-cell">
                                <label for="PREFIX">Last Name</label>
                                <input type="text" name="LASTNAME" size="20" maxlength="40" id="LASTNAME" value="<?php echo $user_data['LASTNAME'];?>" placeholder="Enter Last Name">
                            </div>
                            <div class="table-cell">
                                <label for="SUFFIX">Suffix</label>
                                <select name="SUFFIX" id="SUFFIX">
                                    <option value=""><?php echo "Select one..."; ?></option>
                                    <?php foreach (array_column($all_suffix['data'], 'SUFFIX') as $suffix){
                                        echo "<option value='$suffix'" . selected($suffix, $user_data['SUFFIX'], false) . ">$suffix</option>";
                                    }?>
                                </select>
                            </div>

                            <div class="table-cell">
                                <label for="DESIGNATIONLST">Honorofix</label>
                                <select multiple size="4" name="DESIGNATIONLST[]" id="DESIGNATIONLST">
                                    <?php
                                    $desig_selected = explode(",", $user_data['DESIGNATIONLST']);
                                    foreach (array_column($all_designation['data'], 'DESIGNATIONLST') as $desig){
                                        $selected = '';
                                        if(in_array($desig, $desig_selected)){
                                            $selected = ' selected="selected"';
                                        }
                                        echo "<option value='$desig'{$selected}>$desig</option>";
                                    }?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="divider_elem">&nbsp;</div>


                    <label class="span-12" for="ADDRESS1" style="">Address <span class="required_ind">*</span></label>
                    <div class="span-6">
                        <input class="input-required input-validated" type="text" placeholder="Enter Address 1 " name="ADDRESS1" id="ADDRESS1" value="<?php echo $user_data['ADDRESS1']; ?>" size="20" maxlength="40" />
                    </div>
                    <div class="span-6">
                        <input type="text" placeholder="Enter Address 2" name="ADDRESS2" id="ADDRESS2" value="<?php echo $user_data['ADDRESS2']; ?>" size="20" maxlength="40" />
                    </div>

                    <div class="span-4">
                        <label for="CITY">City <span class="required_ind">*</span></label>
                        <input class="input-required input-validated" type="text" placeholder="Enter City" name="CITY" id="CITY" value="<?php echo $user_data['CITY']; ?>" size="20" maxlength="40" />
                    </div>

                    <div class="span-4">
                        <label for="STATECD">State <span class="required_ind">*</span></label>
                        <select name="STATECD" id="STATECD" class="input-required input-validated chosen-select" data-placeholder="Choose a State...">
                            <option value=""><?php echo "Select one..."; ?></option>
                            <?php foreach ($all_states['data'] as $state){
                                echo "<option value='{$state['st_cd']}'" . selected($state['st_cd'], $user_data['STATECD'], false) . ">{$state['st_nm']}</option>";
                            }?>
                        </select>
                    </div>

                    <div class="span-4">
                        <label for="ZIP">Zip <span class="required_ind">*</span></label>
                        <input class="input-required input-validated" type="text" placeholder="Enter Zip" name="ZIP" id="ZIP" value="<?php echo $user_data['ZIP']; ?>" size="20" maxlength="40" />
                    </div>

                </div>
            </div>

            <h4>Contact Detail</h4>
            <div class="clearfix ymp_wrapper grid-12">
                <div class="ymp-row">
                    <div class="span-6">
                        <label for="HOMEPHONE">Home Phone <span class="required_ind">*</span></label>
                        <input class="input-required input-validated" type="text" name="HOMEPHONE" size="20" maxlength="40" id="HOMEPHONE" value="<?php echo $user_data['HOMEPHONE'];?>" placeholder="Enter Home Phone">
                    </div>

                    <div class="span-6">
                        <label for="MOBILEPHONE">Mobile Phone</label>
                        <input type="text" name="MOBILEPHONE" size="20" maxlength="40" id="MOBILEPHONE" value="<?php echo $user_data['MOBILEPHONE'];?>" placeholder="Enter Mobile Phone">
                    </div>

                    <div class="clearfix"></div>

                    <div class="span-6">
                        <label for="EMAIL">Contact Email <span class="required_ind">*</span></label>
                        <input class="input-required input-validated" type="email" name="EMAIL" size="20" maxlength="125" id="EMAIL" value="<?php echo $user_data['EMAIL'];?>" placeholder="Enter Contact Email">
                    </div>

                    <div class="span-6">
                        <label for="BIRTHDATE">Birth Date</label>
                        <div class="clearfix birth_date_input_wrap">
                            <a class="birth_date_btn" id="BIRTHDATE_btn" href="#"><img src="<?php echo $base_url; ?>/assets/templates/archon/images/calendar-icon.png" alt="Select Date"></a>
                            <div class="bdinput">
                                <input type="text" data-date="" name="BIRTHDATE" size="20" maxlength="40" id="BIRTHDATE" value="<?php echo $user_data['BIRTHDATE'];?>" placeholder="">
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="span-6" style="padding-bottom: 9px;">
                        <label style="padding-bottom: 5px;">Marital Status</label>
                        <div class="checkbox_radio_wrap radio_wrap">
                            <input id="MARRIED" type="radio" name="MARITALSTT" value="1" <?php checked('1',$user_data['MARITALSTT'] );?>>
                            <label for="MARRIED">Married</label>
                            <input id="UNMARRIED" type="radio" name="MARITALSTT" value="" <?php checked('',$user_data['MARITALSTT'] );?>>
                            <label for="UNMARRIED">Unmarried</label>
                        </div>
                    </div><!--End Item-->

                    <div class="span-6" id="archon_spouse">
                        <label for="EMAIL">Spouse Name</label>
                        <input class="" type="text" name="SPOUSENAME" size="20" maxlength="125" id="EMAIL" value="<?php echo $user_data['SPOUSENAME'];?>" placeholder="Enter Contact Email">
                    </div>

                </div>
            </div>
        </div>

        <?php echo $submit_dv;?>
    </div>
</div>