<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("SPOUSENAME", true, "text", "", "", "", "Please enter the Archousa's name.");
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an insert transaction instance
$ins_spp_archons = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_archons);
// Register triggers
$ins_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$ins_spp_archons->setTable("spp_archons");
$ins_spp_archons->addColumn("SPOUSENAME", "STRING_TYPE", "POST", "SPOUSENAME");
$ins_spp_archons->addColumn("GRAMMID", "NUMERIC_TYPE", "POST", "GRAMMID", "{SESSION.webInternalKey}");
$ins_spp_archons->addColumn("GRAMMUPDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_archons->addColumn("GRAMM", "STRING_TYPE", "VALUE", "{SESSION.webFullname}");
$ins_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_archons = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("SPOUSENAME", "STRING_TYPE", "POST", "SPOUSENAME");
$upd_spp_archons->addColumn("GRAMMID", "NUMERIC_TYPE", "POST", "GRAMMID");
$upd_spp_archons->addColumn("GRAMMUPDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archons->addColumn("GRAMM", "STRING_TYPE", "SESSION", "webFullname");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "GET", "CUSTOMERID");

// Make an instance of the transaction object
$del_spp_archons = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_archons);
// Register triggers
$del_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$del_spp_archons->setTable("spp_archons");
$del_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "GET", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$totalRows_rsspp_archons = $rsspp_archons->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: true
}
</script>

<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['CUSTOMERID'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    ARCHOUSA </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_archons->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_archons > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
            <td class="KT_th"><label for="SPOUSENAME_<?php echo $cnt1; ?>">ARCHOUSA NAME:</label></td>
            <td><input type="text" name="SPOUSENAME_<?php echo $cnt1; ?>" id="SPOUSENAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('SPOUSENAME')); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("SPOUSENAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "SPOUSENAME", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th">LAST GRAMMATEUS UPDATE:</td>
            <td><?php echo KT_formatDate($rsspp_archons->Fields('GRAMMUPDATE')); ?></td>
          </tr>
          <tr>
            <td class="KT_th">UPDATED BY:</td>
            <td><?php echo KT_escapeAttribute($rsspp_archons->Fields('GRAMM')); ?></td>
          </tr>
        </table>
        <input type="hidden" name="kt_pk_spp_archons_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('kt_pk_spp_archons')); ?>" />
        <input type="hidden" name="GRAMMID_<?php echo $cnt1; ?>" id="GRAMMID_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('GRAMMID')); ?>" />
        <?php
    $rsspp_archons->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['CUSTOMERID'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
