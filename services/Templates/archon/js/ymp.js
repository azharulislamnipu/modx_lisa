function regexEscape(str) {
    return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
}
function reg(left, input, right) {
    var flags;
    //could be any combination of 'g', 'i', and 'm'
    flags = '';
    input = regexEscape(input);
    return new RegExp(left + input + right, flags);
}
function getNewUrl(url, param, val) {
    var newUrl;
    if (reg('/[?&]', param, '\s*=/' ).test(url)) {
        newUrl = url.replace(/(?:([?&])param\s*=[^?&]*)/, "$1param=" + val);
    } else if (/\?/.test(url)) {
        newUrl = url + "&"+ param + "=" + val;
    } else {
        newUrl = url + "&"+ param + "=" + val;
    }
    return newUrl;
}
(function($) {
    $body = $('body');

    $body.on('change', '.as_order select', function () {
        var newUrl = window.location.href;
        newUrl = getNewUrl(newUrl, $(this).attr('id'), $(this).val());
        window.location.href = newUrl;
    }).on('click', '.open_sfilter', function (e) {
        e.preventDefault();
        $(".as_filter:not(.filter_open)").find('h3').trigger('click');
    });



}(jQuery));