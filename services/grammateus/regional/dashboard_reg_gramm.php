<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsOfficers = "-1";
if (isset($_SESSION['REGIONNAME'])) {
  $colname_rsOfficers = $_SESSION['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOfficers = sprintf("SELECT CPOSITION, BOULENAME, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, SUFFIX, DESIGNATIONLST, ADDRESS1, CITY, STATECD, ZIP, HOMEPHONE, EMAIL, WORKPHONE, ALTEMAIL FROM vw_boule_officers WHERE REGIONNAME = %s ORDER BY LASTNAME ASC", GetSQLValueString($colname_rsOfficers, "text"));
$rsOfficers = mysql_query($query_rsOfficers, $sigma_modx) or die(mysql_error());
$row_rsOfficers = mysql_fetch_assoc($rsOfficers);
$totalRows_rsOfficers = mysql_num_rows($rsOfficers);

$colname_rsBoules = "-1";
if (isset($_SESSION['REGIONNAME'])) {
  $colname_rsBoules = $_SESSION['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoules = sprintf("SELECT * FROM spp_boule WHERE REGIONNAME = %s ORDER BY BOULENAME ASC", GetSQLValueString($colname_rsBoules, "text"));
$rsBoules = mysql_query($query_rsBoules, $sigma_modx) or die(mysql_error());
$row_rsBoules = mysql_fetch_assoc($rsBoules);
$totalRows_rsBoules = mysql_num_rows($rsBoules);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Regional Grammateus Management</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->

<!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" -->
  <!-- #MAIN COLUMN -->
<div class="floatLeft width460">

  <h1 class="yellow">Manage <?php echo $row_rsBoules['REGIONNAME']; ?> Region Archons</h1>
  <div class="contentBlock">
    <ul>
      <?php do { ?>
      <li><a href="/services/grammateus/regional/boule_list.php?BOULENAME=<?php echo $row_rsBoules['BOULENAME']; ?>">List <?php echo $row_rsBoules['BOULENAME']; ?> Archons</a></li>
        <?php } while ($row_rsBoules = mysql_fetch_assoc($rsBoules)); ?></ul>
  </div>

  

</div>
<!-- #END MAIN COLUMN -->
<!-- #BEGIN SECOND COLUMN -->
<div class="floatRight width305">
  <h1 class="yellow">Manage  <?php echo $_SESSION['REGIONNAME']; ?> Region Officers</h1>
  <div class="contentBlock">
<script language="JavaScript">
    function sires_list(){
        document.forms.sires.submit();
    }
    function grammatei_list(){
        document.forms.grammatei.submit();
    }
    function grapters_list(){
        document.forms.grapters.submit();
    }
    function thesauristes_list(){
        document.forms.thesauristes.submit();
    }
    function agogos_list(){
        document.forms.agogos.submit();
    }
    function rhetoricos_list(){
        document.forms.rhetoricos.submit();
    }
</script>
<form name="sires" action="/services/grammateus/regional/sires_list.php" method="post"></form>
    <ul>
      <li><a href="#" onclick="sires_list();return(FALSE);">List Sire Archons</a></li>
      
<form name="grammatei" action="/services/grammateus/regional/grammatei_list.php" method="post"></form>
     <li><a href="#" onclick="grammatei_list();return(FALSE);">List Grammatei</a></li>
     
<form name="grapters" action="/services/grammateus/regional/grapters_list.php" method="post"></form>
      <li><a href="#" onclick="grapters_list();return(FALSE);">List Grapters</a></li>
      
<form name="thesauristes" action="/services/grammateus/regional/thesauristes_list.php" method="post"></form>
      <li><a href="#" onclick="thesauristes_list();return(FALSE);">List Thesauristes</a></li>
      
<form name="agogos" action="/services/grammateus/regional/agogos_list.php" method="post"></form>
      <li><a href="#" onclick="agogos_list();return(FALSE);">List Agogos</a></li>
      
<form name="rhetoricos" action="/services/grammateus/regional/rhetoricos_list.php" method="post"></form>
      <li><a href="#" onclick="rhetoricos_list();return(FALSE);">List Rhetoricos</a></li>
    </ul>
    </div>


</div><!-- #END SECOND COLUMN -->
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsOfficers);

mysql_free_result($rsBoules);
?>
