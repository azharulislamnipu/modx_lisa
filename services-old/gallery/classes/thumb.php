<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * AvImage
 *
 * @package svManager
 */
class Thumb
{
 /**
  * @var string thumb url according to current path scheme
  */
  var $url = '';
  
 /**
  * @var string file name
  */
  var $fileName = '';
  
 /**
  * @var integer thumbnail width in px
  */
  var $width = 70;
  
 /**
  * @var integer thumbnail height in px
  */
  var $height = 70;
  
 /**
  * Constructs thumb
  *
  * @access public
  * @return void
  * @param string url according to current path scheme
  */
  function Thumb($url)
  {
  }
  
 /**
  * set image url
  *
  * @access public
  * @return void
  * @param string image url
  */
  function setUrl($url)
  {
    $this->url = $url;
  }
  
 /**
  * get image url
  *
  * @access public
  * @return string image url
  */
  function getUrl()
  {
    return $this->url;
  }
  
 /**
  * get file name
  *
  * @access public
  * @return string file name
  */
  function getFileName()
  {
    return $this->fileName;
  }
  
 /**
  * get width
  *
  * @access public
  * @return string file name
  */
  function getWidth()
  {
    return $this->width;
  }
  
 /**
  * get height
  *
  * @access public
  * @return string file name
  */
  function getWidth()
  {
    return $this->width;
  }
}
?>
