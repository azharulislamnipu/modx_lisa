<div id="archon_edit_education" class="accordion parent-elem edit_education" data-accordion>
    <div data-control><h3>Edit Education</h3></div>
    <div data-content>
        <div class="data-content-inner data-content-inner-tabs ymp_form_input">
            <?php

            $i = 0;

            if(!empty($user_edu_data)) {

                foreach ($user_edu_data as $index => $user_edu_single) {
?>
                    <div id="archon_edit_educ_<?php echo $index; ?>"
                         class="accordion parent-elem edit_educ_inner" data-accordion>
                        <div data-control>
                            <h3><?php echo $user_edu_single['INSTITUTION'] . ', ' . $user_edu_single['DEGREEYEAR']; ?></h3></div>
                        <div data-content>
                            <div class="data-content-inner clearfix ymp_wrapper grid-12">

                                <div class="ymp-row">

<div class="span-6">
    <label for="DEGREE_<?php echo $index; ?>">Degree
        <span class="required_ind">*</span></label>
    <input class="input-required input-validated" type="text"
           name="DEGREE" id="DEGREE_<?php echo $index; ?>"
           value="<?php echo $user_edu_single['DEGREE']; ?>"
           placeholder="">
</div><!--End Item-->

<div class="span-6">
    <label for="INSTITUTION_<?php echo $index; ?>">Institution
        <span class="required_ind">*</span></label>
    <input class="input-required input-validated" type="text"
           name="INSTITUTION" id="INSTITUTION_<?php echo $index; ?>"
           value="<?php echo $user_edu_single['INSTITUTION']; ?>"
           placeholder="">
</div><!--End Item-->

<div class="span-6">
    <label for="DEGREEYEAR_<?php echo $index; ?>">Degree Year
        <span class="required_ind">*</span></label>
    <input class="input-required input-validated" type="text"
           name="DEGREEYEAR" id="DEGREEYEAR_<?php echo $index; ?>"
           value="<?php echo $user_edu_single['DEGREEYEAR']; ?>"
           placeholder="">
</div><!--End Item-->

<div class="span-6">
    <label for="MAJOR_<?php echo $index; ?>">Major
        <span class="required_ind">*</span></label>
    <input class="input-required input-validated" type="text"
           name="MAJOR" id="MAJOR_<?php echo $index; ?>"
           value="<?php echo $user_edu_single['MAJOR']; ?>"
           placeholder="">
</div><!--End Item-->


<div class="span-6">
    <label for="MINOR1_<?php echo $index; ?>">Minor 1</label>
    <input class="" type="text"
           name="MINOR1" id="MINOR1_<?php echo $index; ?>"
           value="<?php echo $user_edu_single['MINOR1']; ?>"
           placeholder="">
</div><!--End Item-->

<div class="span-6">
    <label for="MINOR2_<?php echo $index; ?>">Minor2</label>
    <input class="" type="text"
           name="MINOR2" id="MINOR2_<?php echo $index; ?>"
           value="<?php echo $user_edu_single['MINOR2']; ?>"
           placeholder="">
</div><!--End Item-->

<div class="span-12">
    <label for="COMMENT<?php echo $i; ?>">Comment</label>
    <textarea class="" type="text"
              name="DESCRIPTION"
              id="COMMENT<?php echo $i; ?>"><?php echo $user_edu_single['COMMENT']; ?></textarea>
</div><!--End Item-->


                                <div class="clearfix"></div>
                                </div>

                                <input type="hidden" name="DEGREEID"
                                       value="<?php echo $user_edu_single['DEGREEID']; ?>">

                                <?php echo $submit_dv; ?>
                            </div>
                        </div>
                    </div>
                <?php }

                $i = count($user_edu_data);
            }

            while ($i < 10){?>

                <div id="archon_edit_educ_<?php echo $i; ?>"
                     class="accordion parent-elem edit_educ_inner add_educ_inner" data-accordion>
                    <div data-control>
                        <h3>New Education</h3></div>
                    <div data-content>
                        <div class="data-content-inner clearfix ymp_wrapper grid-12">
                            <div class="ymp-row">

<div class="span-6">
    <label for="DEGREE_<?php echo $i; ?>">Degree
        <span class="required_ind">*</span></label>
    <input class="input-required" type="text"
           name="DEGREE" id="DEGREE_<?php echo $i; ?>"
           value=""
           placeholder="">
</div><!--End Item-->

<div class="span-6">
    <label for="INSTITUTION_<?php echo $i; ?>">Institution
        <span class="required_ind">*</span></label>
    <input class="input-required" type="text"
           name="INSTITUTION" id="INSTITUTION_<?php echo $i; ?>"
           value=""
           placeholder="">
</div><!--End Item-->

<div class="span-6">
    <label for="DEGREEYEAR_<?php echo $i; ?>">Degree Year
        <span class="required_ind">*</span></label>
    <input class="input-required" type="text"
           name="DEGREEYEAR" id="DEGREEYEAR_<?php echo $i; ?>"
           value=""
           placeholder="">
</div><!--End Item-->

<div class="span-6">
    <label for="MAJOR_<?php echo $i; ?>">Major
        <span class="required_ind">*</span></label>
    <input class="input-required" type="text"
           name="MAJOR" id="MAJOR_<?php echo $i; ?>"
           value=""
           placeholder="">
</div><!--End Item-->


<div class="span-6">
    <label for="MINOR1_<?php echo $i; ?>">Minor 1</label>
    <input class="" type="text"
           name="MINOR1" id="MINOR1_<?php echo $i; ?>"
           value=""
           placeholder="">
</div><!--End Item-->

<div class="span-6">
    <label for="MINOR2_<?php echo $i; ?>">Minor2</label>
    <input class="" type="text"
           name="MINOR2" id="MINOR2_<?php echo $i; ?>"
           value=""
           placeholder="">
</div><!--End Item-->

<div class="span-12">
    <label for="COMMENT<?php echo $i; ?>">Comment</label>
    <textarea class="" type="text"
              name="DESCRIPTION"
              id="COMMENT<?php echo $i; ?>"></textarea>
</div><!--End Item-->


                                <div class="clearfix"></div>
                            </div>
                            <input type="hidden" name="DEGREEID"
                                   value="">

                            <?php echo $submit_dv; ?>
                        </div>
                    </div>
                </div>
                <?php
                ++$i;
            }


            ?>

        </div>
        <div class="sb_div">
            <button class="button add_new_educ">Add New Education</button>
        </div>

    </div>
</div>