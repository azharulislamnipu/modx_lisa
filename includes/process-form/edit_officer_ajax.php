<?php
error_reporting(0);
include_once "../../home/modxapi.php";
$modx = new MODxAPI();
$modx->connect();
$modx->startSession();

/**
 * Local
 */
$pos = strrpos(__DIR__, "htdocs");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'htdocs/';
}


/**
 * Server
 */

/*$pos = strrpos(__DIR__, "apache");
if($pos !== false){
    $new_path = substr(__DIR__, 0, $pos) . 'apache/';
}*/

require $new_path . "includes/functions.php";

if(!is_ajax()){
	die('Not permitted');
}

$data = $response = array();
parse_str($_POST['data'], $data);
array_walk($data, 'test_in_array');
$response['type'] = 'success';

$date = date_create($data['JOINDATE']);
if (!$date) {
	$response['type'] = 'error';
	$response['html'] = '<p>Wrong Start Date.</p>';
}
$data['JOINDATE'] = date_format($date,"Y-m-d");

$post_data = array();
$columns = array('JOINDATE', 'OFFICERID', 'UPDATEUSERCD');

if($response['type'] !== 'error'){
	foreach ($columns as $column){
		$post_data[$column] = $data[$column];
	}
	$post_data['UPDATETMS'] = date('Y-m-d H:i:s');
	$db = get_db();
	$db->where ('OFFICERID', $post_data['OFFICERID']);
	if ($db->update ('spp_officers', $post_data)){
		$response['type'] = 'success';
	} else{
		$response['type'] = 'error';
		$response['html'] = '<p>Error occurred! Please try again.</p>';
	}
}

die(json_encode($response));
