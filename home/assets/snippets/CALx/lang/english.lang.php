<?php
		/* ------------------------------------------------------------
		:: Snippet: CALx
		---------------------------------------------------------------
		
		---------------------------------------------------------------
		:: Language file  : ENGLISH
		--------------------------------------------------------------*/
		// Name of the month
  	$month[0] = "January" ;
  	$month[1] = "February" ;
  	$month[2] = "March" ;
  	$month[3] = "April" ;
  	$month[4] = "May" ;
  	$month[5] = "June" ;
  	$month[6] = "July" ;
  	$month[7] = "August" ;
  	$month[8] = "September" ;
  	$month[9] = "October" ;
  	$month[10] = "November" ;
  	$month[11] = "December" ;

  	// First letter of the month
  	$day[0] = "mon" ;
  	$day[1] = "tue" ;
  	$day[2] = "wed" ;
  	$day[3] = "thu" ;
  	$day[4] = "fri" ;
  	$day[5] = "sat" ;
  	$day[6] = "sun" ;

   	// Noms des jours de la semaine en entier
  	$dayName[0] = "monday" ;
  	$dayName[1] = "tuesday" ;
  	$dayName[2] = "wednesday" ;
  	$dayName[3] = "thursday" ;
  	$dayName[4] = "friday" ;
  	$dayName[5] = "saturday" ;
  	$dayName[6] = "sunday" ;
		   	
  	// Message d'erreur en cas de date invalide
  	$error01 = "Error : invalide date";
  	
  	// Message d'erreur lorsque le calendrier ne peut pas �tre cr�er
  	$msgCannotCreateCALx = "The calendar cannot be created.";
  	
  	// Message pour les info bulles : overlib
  	$closeText="Close";
		$titleText="Informations";
		$startStringUniqEvent = "";
		$endStringUniqEvent = " events scheduled for this date.";
  	$stringDetails = "Click here for more details.";
  	// Configuration des informations de la page de liens vers le multievent
  	$pagetitle="Events listing";
  	$longtitle="Events listing";
  	$description="Events listing";
  	$introtext="Events listing";
  	$eventOf = "Events of ";
  	$noEvent = "No events scheduled for this date.";
  	$noEventMonth = "No events scheduled for this month.";
  	
  	// Message pour les info bulles des liens de changements de mois
  	$previousYear="Show the previous year";
  	$previousMonth="Show the previous month";
  	$nextMonth="Show the next month";
  	$nextYear="Show the next year";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowAYear="Show a year";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowMonthEvents="Show all the events for this month"
?>