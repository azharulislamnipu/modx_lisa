<?php

############ Configuration ##############
$config = array();
$config["generate_image_file"]		= true;
$config["generate_thumbnails"]		= false;
$config["image_max_size"] 			= 350;
$config["thumbnail_size"]  			= 100;
$config["thumbnail_prefix"]			= "thumb_";
$config["destination_folder"]		= '/var/lib/apache/htdocs/dev4.sigmapiphi.org/home/assets/images/archons/';
$config["thumbnail_destination_folder"]		= $config["destination_folder"] . '/thumbnails/';
$config["upload_url"] 				= "/home/assets/images/archons/";
$config["quality"] 				    = 90;
$config["random_file_name"]			= false;
$config["file_name_prefix"]         = empty($user_id) ? '' : $user_id . '.';
$fname = empty($_POST['UPDATEDBY']) ? '' : $_POST['UPDATEDBY'];

$fname = explode(' ', $fname);

$config['user_name'] = strtolower($fname[0]);

//specify uploaded file variable
$config["file_data"] = $_FILES["__files"];


//include sanwebe impage resize class
include $pform_dir . "image-upload/resize.class.php";

//create class instance
$im = new ImageResize($config);

$response['err_msg'] = '';
try{
    $responses = $im->resize(); //initiate image resize
    $response = array();

    // Save image to db
    $db = get_db();
    $data = array (
        'IMAGE'         => $responses["images"][0],
        'LASTUPDATED'   => date("Y-m-d H:i:s"),
        'UPDATEDBY'   => $updated_by,
    );
    $db->where ('ARCHOUSAID', $archousa_id);
    if ($db->update ('spp_archons', $data)){
        $response['type'] = 'success';
        $response['img_url'] = $config["upload_url"] . $responses["images"][0];
        SPP_ARCHON_DB::update_lastupdate_date($archousa_id, 'ARCHOUSAID', 'spp_archousa');
    } else{
        $response['type'] = 'error';
        $response['err_msg'] = $db->getLastError();
    }
} catch(Exception $e){
    $response['type'] = 'error';
    $response['err_msg'] = $response['err_msg'] . '<p>' . $e->getMessage() . '</p>';
}

die(json_encode($response));
