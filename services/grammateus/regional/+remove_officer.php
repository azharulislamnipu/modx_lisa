<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("TERMINATIONDATE", true, "date", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchon = "-1";
if (isset($_GET['CUSTOMERCD'])) {
  $colname_rsArchon = $_GET['CUSTOMERCD'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchon = sprintf("SELECT * FROM spp_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname_rsArchon, "int"));
$rsArchon = mysql_query($query_rsArchon, $sigma_modx) or die(mysql_error());
$row_rsArchon = mysql_fetch_assoc($rsArchon);
$totalRows_rsArchon = mysql_num_rows($rsArchon);

$colname_rsOfficers = "-1";
if (isset($row_rsArchon['CHAPTERID'])) {
  $colname_rsOfficers = $row_rsArchon['CHAPTERID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOfficers = sprintf("SELECT * FROM vw_boule_officers WHERE CPOSITION = 'Grammateus' AND BOULENAME = %s", GetSQLValueString($colname_rsOfficers, "text"));
$rsOfficers = mysql_query($query_rsOfficers, $sigma_modx) or die(mysql_error());
$row_rsOfficers = mysql_fetch_assoc($rsOfficers);
$totalRows_rsOfficers = mysql_num_rows($rsOfficers);

// Make an update transaction instance
$upd_spp_officers = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_officers);
// Register triggers
$upd_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "../../includes/nxt/back.php");
$upd_spp_officers->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$upd_spp_officers->setTable("spp_officers");
$upd_spp_officers->addColumn("TERMINATIONDATE", "DATE_TYPE", "POST", "TERMINATIONDATE");
$upd_spp_officers->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$upd_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "POST", "CPOSITION");
$upd_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "VALUE", "COMMITTEESTATUSSTT");
$upd_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "CURRVAL", "");
$upd_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "CURRVAL", "");
$upd_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE", "GET", "OFFICERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_officers = $tNGs->getRecordset("spp_officers");
$row_rsspp_officers = mysql_fetch_assoc($rsspp_officers);
$totalRows_rsspp_officers = mysql_num_rows($rsspp_officers);

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("tonny.joseph@sigmapiphi.org");
  $emailObj->setCC("{COOKIE.GRAMMEMAIL}");
  $emailObj->setBCC("webmaster@sigmapiphi.org");
  $emailObj->setSubject("Removal of {rsArchon.CHAPTERID} Boul�'s {CPOSITION} by the Grammateus of the {COOKIE.REGIONNAME} Region");
  //FromFile method
  $emailObj->setContentFile("remove_officer.html");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_regional_grammateus.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1> REMOVE OFFICER FOR <?php echo strtoupper($row_rsArchon['CHAPTERID']); ?> BOUL&Eacute;</h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_officers > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <tr>
              <td class="KT_th"><label for="TERMINATIONDATE_<?php echo $cnt1; ?>">END DATE:</label></td>
              <td><input name="TERMINATIONDATE_<?php echo $cnt1; ?>" id="TERMINATIONDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_officers['TERMINATIONDATE']); ?>" size="10" maxlength="22" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="false" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("TERMINATIONDATE");?> <?php echo $tNGs->displayFieldError("spp_officers", "TERMINATIONDATE", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMMENT_<?php echo $cnt1; ?>">COMMENT:</label></td>
              <td>If the officer is being removed for any other reason than end of term, enter the explanation below.<br />
                <br />
                <textarea name="COMMENT_<?php echo $cnt1; ?>" id="COMMENT_<?php echo $cnt1; ?>" cols="80" rows="10"><?php echo KT_escapeAttribute($row_rsspp_officers['COMMENT']); ?></textarea>
                <?php echo $tNGs->displayFieldHint("COMMENT");?> <?php echo $tNGs->displayFieldError("spp_officers", "COMMENT", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th">LAST UPDATED BY:</td>
              <td><?php echo KT_escapeAttribute($row_rsspp_officers['UPDATEUSERCD']); ?></td>
            </tr>
            <tr>
              <td class="KT_th">LAST DATE UPDATED:</td>
              <td><?php echo KT_formatDate($row_rsspp_officers['UPDATETMS']); ?></td>
            </tr>
          </table>
          <input type="hidden" name="CPOSITION_<?php echo $cnt1; ?>" id="CPOSITION_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_officers['CPOSITION']); ?>" />
          <input type="hidden" name="kt_pk_spp_officers_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_officers['kt_pk_spp_officers']); ?>" />
        <input type="hidden" name="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" id="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_officers['COMMITTEESTATUSSTT']); ?>" />
        <?php } while ($row_rsspp_officers = mysql_fetch_assoc($rsspp_officers)); ?>
      <div class="KT_bottombuttons">
        <div>
          <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
<input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchon);

mysql_free_result($rsOfficers);
?>
