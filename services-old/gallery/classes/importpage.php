<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Import page
 *
 * @package svManager
 */
class ImportPage extends Page
{
 /**
  * constructs ImportPage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function ImportPage()
  {
    parent::Page('svManager &ndash; import', 'import');
  }

  /**
  * get html for import page
  *
  * @access public
  * @return string html
  * @param string gallery path
  */
  function getImportHtml()
  {
    $cwd = getcwd();
    if (substr($cwd, -1) != DIRECTORY_SEPARATOR)
    {
      $cwd = $cwd.DIRECTORY_SEPARATOR;
    }
    $serverName = 'http://'.$_SERVER['SERVER_NAME'].'/';
    // $_SERVER['PHP_SELF'] always returns forward slashes
    $svManagerUrl = dirname($_SERVER['PHP_SELF']).'/';
    $html = <<<EOD
    <form method="post" action="{$_SERVER['PHP_SELF']}" id="copyform">
      <table cellspacing="0">
        <tr>
          <td class="col1">Import to: </td><td class="col2">{$svManagerUrl}</td>
        </tr>
        <tr>
          <td class="col1"><label for="importpath">Import from:</label></td><td class="col2"><input class="text" type="text" id="importpath" name="importpath" value="{$svManagerUrl}"/></td>
        </tr>
        <tr>
          <td class="col1">&nbsp;</td>
          <td class="col2"><input class="formbutton" type="submit" value="Import" name="submit" /></td>
        </tr>
      </table>        
    </form>
    <h3>Notes:</h3>
    <ol>
      <li>1. Back-up your gallery before importing it.</li>
      <li>2. Enter the path (from the web root) of the gallery folder to be imported.</li>
      <li>3. The web user must have read and write permission for the folder and its contents.</li>
    </ol>

EOD;
     return $html;
   }
}
?>