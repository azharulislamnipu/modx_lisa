<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Filter
$tfi_listrsOfficersList2 = new TFI_TableFilter($sigma_modx, "tfi_listrsOfficersList2");
$tfi_listrsOfficersList2->addColumn("REGIONNAME", "STRING_TYPE", "REGIONNAME", "%");
$tfi_listrsOfficersList2->addColumn("CPOSITION", "STRING_TYPE", "CPOSITION", "%");
$tfi_listrsOfficersList2->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listrsOfficersList2->addColumn("STARTDATE", "DATE_TYPE", "STARTDATE", "=");
$tfi_listrsOfficersList2->Execute();

// Sorter
$tso_listrsOfficersList2 = new TSO_TableSorter("rsOfficersList", "tso_listrsOfficersList2");
$tso_listrsOfficersList2->addColumn("REGIONNAME");
$tso_listrsOfficersList2->addColumn("CPOSITION");
$tso_listrsOfficersList2->addColumn("FULLNAME");
$tso_listrsOfficersList2->addColumn("STARTDATE");
$tso_listrsOfficersList2->setDefault("REGIONNAME");
$tso_listrsOfficersList2->Execute();

// Navigation
$nav_listrsOfficersList2 = new NAV_Regular("nav_listrsOfficersList2", "rsOfficersList", "../../", $_SERVER['PHP_SELF'], 25);

// Begin List Recordset
$maxRows_rsOfficersList = $_SESSION['max_rows_nav_listrsOfficersList2'];
$pageNum_rsOfficersList = 0;
if (isset($_GET['pageNum_rsOfficersList'])) {
  $pageNum_rsOfficersList = $_GET['pageNum_rsOfficersList'];
}
$startRow_rsOfficersList = $pageNum_rsOfficersList * $maxRows_rsOfficersList;
// Defining List Recordset variable
$NXTFilter__rsOfficersList = "1=1";
if (isset($_SESSION['filter_tfi_listrsOfficersList2'])) {
  $NXTFilter__rsOfficersList = $_SESSION['filter_tfi_listrsOfficersList2'];
}
// Defining List Recordset variable
$NXTSort__rsOfficersList = "BOULENAME";
if (isset($_SESSION['sorter_tso_listrsOfficersList2'])) {
  $NXTSort__rsOfficersList = $_SESSION['sorter_tso_listrsOfficersList2'];
}
$query_rsOfficersList = "SELECT * FROM vw_region_officers WHERE COMMITTEESTATUSSTT = 'Active' AND  {$NXTFilter__rsOfficersList} ORDER BY {$NXTSort__rsOfficersList} ";
$rsOfficersList = $sigma_modx->SelectLimit($query_rsOfficersList, $maxRows_rsOfficersList, $startRow_rsOfficersList) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsOfficersList'])) {
  $totalRows_rsOfficersList = $_GET['totalRows_rsOfficersList'];
} else {
  $all_rsOfficersList = $sigma_modx->SelectLimit($query_rsOfficersList) or die($sigma_modx->ErrorMsg());
  $totalRows_rsOfficersList = $all_rsOfficersList->RecordCount();
}
$totalPages_rsOfficersList = (int)(($totalRows_rsOfficersList-1)/$maxRows_rsOfficersList);
// End List Recordset

$nav_listrsOfficersList2->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: true,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_REGIONNAME {width:84px; overflow:hidden;}
  .KT_col_CPOSITION {width:175px; overflow:hidden;}
  .KT_col_FULLNAME {width:210px; overflow:hidden;}
  .KT_col_STARTDATE {width:140px; overflow:hidden;}
</style><!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listrsOfficersList2">
  <h1> Vw_boule_officers
    <?php
  $nav_listrsOfficersList2->Prepare();
  require("../../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listrsOfficersList2->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
        <?php 
  // Show IF Conditional region1 
  if (@$_GET['show_all_nav_listrsOfficersList2'] == 1) {
?>
          <?php echo $_SESSION['default_max_rows_nav_listrsOfficersList2']; ?>
          <?php 
  // else Conditional region1
  } else { ?>
          <?php echo NXT_getResource("all"); ?>
          <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                            <?php 
  // Show IF Conditional region2 
  if (@$_SESSION['has_filter_tfi_listrsOfficersList2'] == 1) {
?>
                              <a href="<?php echo $tfi_listrsOfficersList2->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                              <?php 
  // else Conditional region2
  } else { ?>
                              <a href="<?php echo $tfi_listrsOfficersList2->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                              <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>            </th>
            <th id="REGIONNAME" class="KT_sorter KT_col_REGIONNAME <?php echo $tso_listrsOfficersList2->getSortIcon('REGIONNAME'); ?>"> <a href="<?php echo $tso_listrsOfficersList2->getSortLink('REGIONNAME'); ?>">REGION</a></th>
            <th id="CPOSITION" class="KT_sorter KT_col_CPOSITION <?php echo $tso_listrsOfficersList2->getSortIcon('CPOSITION'); ?>"> <a href="<?php echo $tso_listrsOfficersList2->getSortLink('CPOSITION'); ?>">OFFICE</a> </th>
            <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listrsOfficersList2->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listrsOfficersList2->getSortLink('FULLNAME'); ?>">NAME</a> </th>
            <th id="STARTDATE" class="KT_sorter KT_col_STARTDATE <?php echo $tso_listrsOfficersList2->getSortIcon('STARTDATE'); ?>"> <a href="<?php echo $tso_listrsOfficersList2->getSortLink('STARTDATE'); ?>">START DATE</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3 
  if (@$_SESSION['has_filter_tfi_listrsOfficersList2'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listrsOfficersList2_REGIONNAME" id="tfi_listrsOfficersList2_REGIONNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsOfficersList2_REGIONNAME']); ?>" size="10" maxlength="100" /></td>
              <td><input type="text" name="tfi_listrsOfficersList2_CPOSITION" id="tfi_listrsOfficersList2_CPOSITION" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsOfficersList2_CPOSITION']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsOfficersList2_FULLNAME" id="tfi_listrsOfficersList2_FULLNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsOfficersList2_FULLNAME']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsOfficersList2_STARTDATE" id="tfi_listrsOfficersList2_STARTDATE" value="<?php echo @$_SESSION['tfi_listrsOfficersList2_STARTDATE']; ?>" size="10" maxlength="22" /></td>
              <td><input type="submit" name="tfi_listrsOfficersList2" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsOfficersList == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="6"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsOfficersList > 0) { // Show if recordset not empty ?>
            <?php
  while (!$rsOfficersList->EOF) { 
?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_vw_boule_officers" class="id_checkbox" value="<?php echo $rsOfficersList->Fields('OFFICERID'); ?>" />
                    <input type="hidden" name="OFFICERID" class="id_field" value="<?php echo $rsOfficersList->Fields('OFFICERID'); ?>" />                </td>
                <td><div class="KT_col_REGIONNAME"><?php echo KT_FormatForList($rsOfficersList->Fields('REGIONNAME'), 12); ?></div></td>
                <td><div class="KT_col_CPOSITION"><?php echo KT_FormatForList($rsOfficersList->Fields('CPOSITION'), 25); ?></div></td>
                <td><div class="KT_col_FULLNAME"><?php echo KT_FormatForList($rsOfficersList->Fields('FULLNAME'), 30); ?><br />
                    <?php echo $rsOfficersList->Fields('BOULENAME'); ?> Boul&eacute; </div></td>
                <td><div class="KT_col_STARTDATE"><?php echo KT_formatDate($rsOfficersList->Fields('STARTDATE')); ?></div></td>
                <td><a href="/home/s_officers_start.php?OFFICERID=<?php echo $rsOfficersList->Fields('OFFICERID'); ?>&amp;CUSTOMERID=<?php echo $rsOfficersList->Fields('CUSTOMERCD'); ?>&amp;KT_back=1">Change Start Date</a><br>
<a class="KT_edit_link" href="/home/s_officers_detail.php?CUSTOMERID=<?php echo $rsOfficersList->Fields('CUSTOMERCD'); ?>&amp;OFFICERID=<?php echo $rsOfficersList->Fields('OFFICERID'); ?>&amp;KT_back=1">Remove from Office</a></td>
              </tr>
              <?php
    $rsOfficersList->MoveNext(); 
  }
?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsOfficersList2->Prepare();
            require("../../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
<span>&nbsp;</span>
<input name="no_new" id="no_new" type="text" value="1" size="2" maxlength="2" />
<a class="KT_additem_op_link" href="/home/s_officers_detail.php?CUSTOMERID=<?php echo $rsOfficersList->Fields('CUSTOMERCD'); ?>&amp;KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
  <!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsOfficersList->Close();
?>
