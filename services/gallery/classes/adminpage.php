<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Admin page
 *
 * @package svManager
 */
class AdminPage extends Page
{
 /**
  * constructs AdminPage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function AdminPage()
  {
    parent::Page('svManager &ndash; admin', 'admin');
  }
  
 /**
  * Returns admin page content
  *
  * @return string
  * @param boolean
  * @param boolean passwords do not match
  * @access public
  */
  function getAdminContent($okPass, $match, $okNewPass, $changed)
  {
    $message1 = ($okPass) ? '&nbsp;' : 'Current password is incorrect.';
    $message1 = ($changed) ? '<span style="color: #0000CC" >Login details changed.</span>' : $message1;
    $message2 = ($match) ? '&nbsp;' : 'Passwords do not match.';
    $message2 = ($okNewPass) ? $message2 : 'Password minimum 5 characters';
    $version = VERSION;
    $html = <<<EOD
    <form action = "{$_SERVER['PHP_SELF']}" method="post">
      <h2>Change Log-in Details</h2>
      <div id="currentpass">
        <p class="error">{$message1}</p>
        <p style="padding-bottom: 1em"><label for="password" style="display: block">Confirm current password: </label><input class="text" type="password" name="password" id="password" /></p>
        <p id="instructions">For security, please confirm your current password and enter the new user name and password &ndash; minimum five characters.</p>
      </div>
      <div id="changepassform">    
        <p class="error">{$message2}</p>
        <p><label for="user" style="display: block">New user name: </label><input class="text" type="text" name="user" id="user" /></p>
        <p><label for="password1" style="display: block">New Password: </label><input class="text" type="password" name="password1" id="password1" /></p>
        <p><label for="password2" style="display: block">Re-type new Password: </label><input class="text" type="password" name="password2" id="password2" /></p>
        <p><input type="hidden" name="adminsubmitted" value="true" /><input type="submit" value="Update" class="formbutton" /></p>
      </div>
    </form>   
    <p class="version">svManager version {$version}</p>
EOD;
    return $html;
  }
}
?>