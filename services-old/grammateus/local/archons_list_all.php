<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listAllArchons1 = new TFI_TableFilter($conn_sigma_modx, "tfi_listAllArchons1");
$tfi_listAllArchons1->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listAllArchons1->addColumn("JOINDATE", "DATE_TYPE", "JOINDATE", "=");
$tfi_listAllArchons1->addColumn("STATUS", "STRING_TYPE", "STATUS", "%");
$tfi_listAllArchons1->Execute();

// Sorter
$tso_listAllArchons1 = new TSO_TableSorter("AllArchons", "tso_listAllArchons1");
$tso_listAllArchons1->addColumn("FULLNAME");
$tso_listAllArchons1->addColumn("JOINDATE");
$tso_listAllArchons1->addColumn("STATUS");
$tso_listAllArchons1->setDefault("FULLNAME");
$tso_listAllArchons1->Execute();

// Navigation
$nav_listAllArchons1 = new NAV_Regular("nav_listAllArchons1", "AllArchons", "../../", $_SERVER['PHP_SELF'], 50);

//NeXTenesio3 Special List Recordset
$maxRows_AllArchons = $_SESSION['max_rows_nav_listAllArchons1'];
$pageNum_AllArchons = 0;
if (isset($_GET['pageNum_AllArchons'])) {
  $pageNum_AllArchons = $_GET['pageNum_AllArchons'];
}
$startRow_AllArchons = $pageNum_AllArchons * $maxRows_AllArchons;

$colname_AllArchons = "Kappa";
if (isset($_SESSION['BOULENAME'])) {
  $colname_AllArchons = $_SESSION['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter_AllArchons = "1=1";
if (isset($_SESSION['filter_tfi_listAllArchons1'])) {
  $NXTFilter_AllArchons = $_SESSION['filter_tfi_listAllArchons1'];
}
// Defining List Recordset variable
$NXTSort_AllArchons = "FULLNAME";
if (isset($_SESSION['sorter_tso_listAllArchons1'])) {
  $NXTSort_AllArchons = $_SESSION['sorter_tso_listAllArchons1'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_AllArchons = sprintf("SELECT * FROM vw_gramm_archons_no_officers WHERE BOULENAME = %s AND  {$NXTFilter_AllArchons} ORDER BY {$NXTSort_AllArchons} ", GetSQLValueString($colname_AllArchons, "text"));
$query_limit_AllArchons = sprintf("%s LIMIT %d, %d", $query_AllArchons, $startRow_AllArchons, $maxRows_AllArchons);
$AllArchons = mysql_query($query_limit_AllArchons, $sigma_modx) or die(mysql_error());
$row_AllArchons = mysql_fetch_assoc($AllArchons);

if (isset($_GET['totalRows_AllArchons'])) {
  $totalRows_AllArchons = $_GET['totalRows_AllArchons'];
} else {
  $all_AllArchons = mysql_query($query_AllArchons);
  $totalRows_AllArchons = mysql_num_rows($all_AllArchons);
}
$totalPages_AllArchons = ceil($totalRows_AllArchons/$maxRows_AllArchons)-1;
//End NeXTenesio3 Special List Recordset

$nav_listAllArchons1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: false,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_FULLNAME {width:210px; overflow:hidden;}
  .KT_col_JOINDATE {width:84px; overflow:hidden;}
  .KT_col_STATUS {width:105px; overflow:hidden;}
</style>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listAllArchons1">
  <h1> Vw_gramm_archons
    <?php
  $nav_listAllArchons1->Prepare();
  require("../../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listAllArchons1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
            <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listAllArchons1'] == 1) {
?>
              <?php echo $_SESSION['default_max_rows_nav_listAllArchons1']; ?>
              <?php 
  // else Conditional region1
  } else { ?>
              <?php echo NXT_getResource("all"); ?>
              <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listAllArchons1'] == 1) {
?>
                  <a href="<?php echo $tfi_listAllArchons1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                  <?php 
  // else Conditional region2
  } else { ?>
                  <a href="<?php echo $tfi_listAllArchons1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                  <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>            </th>
            <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listAllArchons1->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listAllArchons1->getSortLink('FULLNAME'); ?>">FULL NAME</a> </th>
            <th id="JOINDATE" class="KT_sorter KT_col_JOINDATE <?php echo $tso_listAllArchons1->getSortIcon('JOINDATE'); ?>"> <a href="<?php echo $tso_listAllArchons1->getSortLink('JOINDATE'); ?>">JOIN DATE</a> </th>
            <th id="STATUS" class="KT_sorter KT_col_STATUS <?php echo $tso_listAllArchons1->getSortIcon('STATUS'); ?>"> <a href="<?php echo $tso_listAllArchons1->getSortLink('STATUS'); ?>">STATUS</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listAllArchons1'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listAllArchons1_FULLNAME" id="tfi_listAllArchons1_FULLNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listAllArchons1_FULLNAME']); ?>" size="30" maxlength="20" /></td>
              <td><input type="text" name="tfi_listAllArchons1_JOINDATE" id="tfi_listAllArchons1_JOINDATE" value="<?php echo @$_SESSION['tfi_listAllArchons1_JOINDATE']; ?>" size="10" maxlength="22" /></td>
              <td><input type="text" name="tfi_listAllArchons1_STATUS" id="tfi_listAllArchons1_STATUS" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listAllArchons1_STATUS']); ?>" size="15" maxlength="20" /></td>
              <td><input type="submit" name="tfi_listAllArchons1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_AllArchons == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="5"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_AllArchons > 0) { // Show if recordset not empty ?>
            <?php do { ?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_vw_gramm_archons" class="id_checkbox" value="<?php echo $row_AllArchons['CUSTOMERID']; ?>" />
                    <input type="hidden" name="CUSTOMERID" class="id_field" value="<?php echo $row_AllArchons['CUSTOMERID']; ?>" />                </td>
                <td><div class="KT_col_FULLNAME"><a class="KT_edit_link" rel="<?php echo $row_AllArchons['CUSTOMERID']; ?>"href="archons_edit.php?CUSTOMERID=<?php echo $row_AllArchons['CUSTOMERID']; ?>&amp;KT_back=1"><?php echo KT_FormatForList($row_AllArchons['FULLNAME'], 30); ?></a></div>
                <div id="<?php echo $row_AllArchons['CUSTOMERID']; ?>" class="balloonstyle">
                <strong><?php echo $row_AllArchons['L2R_FULLNAME']; ?></strong><br />
                <?php echo $row_AllArchons['ADDRESS1']; ?>
                <?php 
// Show IF Conditional region4 
if (@$row_AllArchons['ADDRESS2'] != NULL) {
?>
                  , <?php echo $row_AllArchons['ADDRESS2']; ?>
                  <?php } 
// endif Conditional region4
?><br />
                <?php echo $row_AllArchons['CITY']; ?>, <?php echo $row_AllArchons['STATECD']; ?> <?php echo $row_AllArchons['ZIP']; ?><br />
                HOME PHONE: <?php echo $row_AllArchons['HOMEPHONE']; ?><br />
                EMAIL: <?php echo $row_AllArchons['EMAIL']; ?>                </div></td>
                <td><div class="KT_col_JOINDATE"><?php echo KT_formatDate($row_AllArchons['JOINDATE']); ?></div></td>
                <td><div class="KT_col_STATUS"><?php echo KT_FormatForList($row_AllArchons['STATUS'], 15); ?></div></td>
                <td><a class="KT_edit_link" href="archons_edit.php?CUSTOMERID=<?php echo $row_AllArchons['CUSTOMERID']; ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
              </tr>
              <?php } while ($row_AllArchons = mysql_fetch_assoc($AllArchons)); ?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listAllArchons1->Prepare();
            require("../../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
        <span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
          <option value="3">3</option>
          <option value="6">6</option>
        </select>
        <a class="KT_additem_op_link" href="archons_edit.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($AllArchons);
?>
