<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("status_req", true, "text", "", "", "", "Please choose a status for this archon.");
$formValidation->addField("dues_amt", true, "numeric", "", "", "", "Please include a dues amount for this archon.");
$tNGs->prepareValidation($formValidation);
// End trigger

// begin Recordset
$colname__rsDuesElig = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsDuesElig = $_SESSION['BOULENAME'];
}
$query_rsDuesElig = sprintf("SELECT vw_gramm_archons.CUSTOMERID,   vw_gramm_archons.BOULENAME,   vw_gramm_archons.FULLNAME,   vw_gramm_archons.`STATUS`,   vw_2009_dues.dues_start_dt,   vw_2009_dues.dues_amt FROM vw_gramm_archons   LEFT OUTER JOIN vw_2009_dues ON (vw_gramm_archons.CUSTOMERID = vw_2009_dues.CUSTOMERCD) WHERE vw_gramm_archons.BOULENAME = %s AND    vw_gramm_archons.`STATUS` <> 'Deceased' AND    vw_2009_dues.dues_start_dt IS NULL ORDER BY vw_gramm_archons.FULLNAME", GetSQLValueString($colname__rsDuesElig, "text"));
$rsDuesElig = $sigma_modx->SelectLimit($query_rsDuesElig) or die($sigma_modx->ErrorMsg());
$totalRows_rsDuesElig = $rsDuesElig->RecordCount();
// end Recordset

// begin Recordset
$colname__rsIndivid = '-1';
if (isset($_GET['CUSTOMERID'])) {
  $colname__rsIndivid = $_GET['CUSTOMERID'];
}
$query_rsIndivid = sprintf("SELECT CUSTOMERID, FULLNAME FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname__rsIndivid, "text"));
$rsIndivid = $sigma_modx->SelectLimit($query_rsIndivid) or die($sigma_modx->ErrorMsg());
$totalRows_rsIndivid = $rsIndivid->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_dues = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_dues);
// Register triggers
$ins_spp_dues->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_dues->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_dues->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$ins_spp_dues->setTable("spp_dues");
$ins_spp_dues->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$ins_spp_dues->addColumn("dues_start_dt", "DATE_TYPE", "VALUE", "1/1/2009");
$ins_spp_dues->addColumn("status_req", "STRING_TYPE", "POST", "status_req");
$ins_spp_dues->addColumn("dues_amt", "NUMERIC_TYPE", "POST", "dues_amt");
$ins_spp_dues->addColumn("addl_amt", "NUMERIC_TYPE", "POST", "addl_amt");
$ins_spp_dues->addColumn("dues_cmt", "STRING_TYPE", "POST", "dues_cmt");
$ins_spp_dues->addColumn("create_dt", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_dues->addColumn("create_by", "NUMERIC_TYPE", "POST", "create_by", "{SESSION.webInternalKey}");
$ins_spp_dues->addColumn("last_update", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_dues->addColumn("update_by", "NUMERIC_TYPE", "POST", "update_by", "{SESSION.webInternalKey}");
$ins_spp_dues->setPrimaryKey("dues_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_dues = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_dues);
// Register triggers
$upd_spp_dues->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_dues->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_dues->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$upd_spp_dues->setTable("spp_dues");
$upd_spp_dues->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$upd_spp_dues->addColumn("dues_start_dt", "DATE_TYPE", "CURRVAL", "");
$upd_spp_dues->addColumn("status_req", "STRING_TYPE", "POST", "status_req");
$upd_spp_dues->addColumn("dues_amt", "NUMERIC_TYPE", "POST", "dues_amt");
$upd_spp_dues->addColumn("addl_amt", "NUMERIC_TYPE", "POST", "addl_amt");
$upd_spp_dues->addColumn("dues_cmt", "STRING_TYPE", "POST", "dues_cmt");
$upd_spp_dues->addColumn("create_dt", "DATE_TYPE", "CURRVAL", "");
$upd_spp_dues->addColumn("last_update", "DATE_TYPE", "CURRVAL", "");
$upd_spp_dues->addColumn("update_by", "NUMERIC_TYPE", "POST", "update_by");
$upd_spp_dues->setPrimaryKey("dues_id", "NUMERIC_TYPE", "GET", "dues_id");

// Make an instance of the transaction object
$del_spp_dues = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_dues);
// Register triggers
$del_spp_dues->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_dues->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$del_spp_dues->setTable("spp_dues");
$del_spp_dues->setPrimaryKey("dues_id", "NUMERIC_TYPE", "GET", "dues_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_dues = $tNGs->getRecordset("spp_dues");
$totalRows_rsspp_dues = $rsspp_dues->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: true
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['dues_id'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    <?php echo date("Y"); ?> DUES PAYMENT </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_dues->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_dues > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
            <td class="KT_th"><label for="CUSTOMERCD_<?php echo $cnt1; ?>">ARCHON:</label></td>
            <td><p>
              <select name="CUSTOMERCD_<?php echo $cnt1; ?>" id="CUSTOMERCD_<?php echo $cnt1; ?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                <?php
  while(!$rsDuesElig->EOF){
?>
                <option value="<?php echo $rsDuesElig->Fields('CUSTOMERID')?>"<?php if (!(strcmp($rsDuesElig->Fields('CUSTOMERID'), $rsspp_dues->Fields('CUSTOMERCD')))) {echo "SELECTED";} ?>><?php echo $rsDuesElig->Fields('FULLNAME')?></option>
                <?php
    $rsDuesElig->MoveNext();
  }
  $rsDuesElig->MoveFirst();
?>
                  </select>
              <?php echo $tNGs->displayFieldError("spp_dues", "CUSTOMERCD", $cnt1); ?> </p>
            <p>Choose the Archon whose dues you are submitting. If the Archon's name is not in the list,<br />
            you have already created a dues payment for <?php echo date("Y"); ?>. </p></td>
          </tr>
          <tr>
            <td class="KT_th">DUES YEAR STARTING:</td>
            <td><?php echo KT_formatDate($rsspp_dues->Fields('dues_start_dt')); ?></td>
          </tr>
          <tr>
            <td class="KT_th"><label for="status_req_<?php echo $cnt1; ?>_1">REQUESTED STATUS:</label></td>
            <td><div>
              <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('status_req')),"Active"))) {echo "CHECKED";} ?> type="radio" name="status_req_<?php echo $cnt1; ?>" id="status_req_<?php echo $cnt1; ?>_1" value="Active" />
              <label for="status_req_<?php echo $cnt1; ?>_1">Active</label>
            </div>
                <div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('status_req')),"Deceased"))) {echo "CHECKED";} ?> type="radio" name="status_req_<?php echo $cnt1; ?>" id="status_req_<?php echo $cnt1; ?>_2" value="Deceased" />
                  <label for="status_req_<?php echo $cnt1; ?>_2">Deceased</label>
                </div>
              <div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('status_req')),"Delinquent"))) {echo "CHECKED";} ?> type="radio" name="status_req_<?php echo $cnt1; ?>" id="status_req_<?php echo $cnt1; ?>_3" value="Delinquent" />
                  <label for="status_req_<?php echo $cnt1; ?>_3">Delinquent</label>
                </div>
              <div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('status_req')),"Dropped"))) {echo "CHECKED";} ?> type="radio" name="status_req_<?php echo $cnt1; ?>" id="status_req_<?php echo $cnt1; ?>_4" value="Dropped" />
                  <label for="status_req_<?php echo $cnt1; ?>_4">Dropped</label>
                (Include documentation with your report for this status)</div>
              <div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('status_req')),"Emeritus"))) {echo "CHECKED";} ?> type="radio" name="status_req_<?php echo $cnt1; ?>" id="status_req_<?php echo $cnt1; ?>_5" value="Emeritus" />
                  <label for="status_req_<?php echo $cnt1; ?>_5">Emeritus</label>
                </div>
              <div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('status_req')),"Involuntary Inactive"))) {echo "CHECKED";} ?> type="radio" name="status_req_<?php echo $cnt1; ?>" id="status_req_<?php echo $cnt1; ?>_6" value="Involuntary Inactive" />
                  <label for="status_req_<?php echo $cnt1; ?>_6">Involuntary Inactive</label>
                </div>
              <div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('status_req')),"Voluntary Inactive"))) {echo "CHECKED";} ?> type="radio" name="status_req_<?php echo $cnt1; ?>" id="status_req_<?php echo $cnt1; ?>_7" value="Voluntary Inactive" />
                  <label for="status_req_<?php echo $cnt1; ?>_7">Voluntary Inactive</label>
                </div>
              <?php echo $tNGs->displayFieldError("spp_dues", "status_req", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="dues_amt_<?php echo $cnt1; ?>_1">2009 DUES AMOUNT:</label></td>
            <td><div>
              <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('dues_amt')),"320"))) {echo "CHECKED";} ?> type="radio" name="dues_amt_<?php echo $cnt1; ?>" id="dues_amt_<?php echo $cnt1; ?>_1" value="320" />
              <label for="dues_amt_<?php echo $cnt1; ?>_1">$320 (STATUS: Active)</label>
            </div>
                <div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_dues->Fields('dues_amt')),"0"))) {echo "CHECKED";} ?> type="radio" name="dues_amt_<?php echo $cnt1; ?>" id="dues_amt_<?php echo $cnt1; ?>_2" value="0" />
                  <label for="dues_amt_<?php echo $cnt1; ?>_2">$0 (STATUS: Delinquent, Dropped, Emeritus, Inductee, Involuntary Inactive, Voluntary Inactive)</label>
                </div>
              <?php echo $tNGs->displayFieldError("spp_dues", "dues_amt", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="addl_amt_<?php echo $cnt1; ?>">ADDITIONAL DUES AMOUNT(S):</label></td>
            <td><p>
              <input type="text" name="addl_amt_<?php echo $cnt1; ?>" id="addl_amt_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_dues->Fields('addl_amt')); ?>" size="4" maxlength="4" />
              <?php echo $tNGs->displayFieldHint("addl_amt");?> <?php echo $tNGs->displayFieldError("spp_dues", "addl_amt", $cnt1); ?> </p>
            <p>If you are including payment for dues years other than <?php echo date("Y"); ?>,<br />
              please enter the total amount above and explain the payment in the comment box below.              </p></td>
          </tr>
          <tr>
            <td class="KT_th"><label for="dues_cmt_<?php echo $cnt1; ?>">COMMENT:</label></td>
            <td><textarea name="dues_cmt_<?php echo $cnt1; ?>" id="dues_cmt_<?php echo $cnt1; ?>" cols="80" rows="10"><?php echo KT_escapeAttribute($rsspp_dues->Fields('dues_cmt')); ?></textarea>
                <?php echo $tNGs->displayFieldHint("dues_cmt");?> <?php echo $tNGs->displayFieldError("spp_dues", "dues_cmt", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th">DATE CREATED:</td>
            <td><?php echo KT_formatDate($rsspp_dues->Fields('create_dt')); ?></td>
          </tr>
          <tr>
            <td class="KT_th">LAST UPDATED:</td>
            <td><?php echo KT_formatDate($rsspp_dues->Fields('last_update')); ?></td>
          </tr>
        </table>
        <input type="hidden" name="kt_pk_spp_dues_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_dues->Fields('kt_pk_spp_dues')); ?>" />
        <input type="hidden" name="create_by_<?php echo $cnt1; ?>" id="create_by_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_dues->Fields('create_by')); ?>" />
        <input type="hidden" name="update_by_<?php echo $cnt1; ?>" id="update_by_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_dues->Fields('update_by')); ?>" />
        <?php
    $rsspp_dues->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['dues_id'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsDuesElig->Close();

$rsIndivid->Close();
?>
