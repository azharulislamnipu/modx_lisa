<?php
///////////////////////////////////////////////////////////////////////////////////////////////////
// This file includes the functions used in MaxiGallery snippet
//
class maxiGallery {
	// Declaring private variables
	var $mgconfig;
	
	//
	// Constructor class
	//
	function maxiGallery($mgconfig) {
		// Set template variables to empty var
		$this->mgconfig = $mgconfig;
	}
	
	//-------------------------------------------------------------------------------------------------
	//function for paging
	//returns array of pager values
	function getPagerData($numHits, $limit, $page) {
		$numHits = (int) $numHits;
		$limit = max((int) $limit, 1);
		$page = (int) $page;
		$numPages = ceil($numHits / $limit);
		$page = max($page, 1);
		$page = min($page, $numPages);
		$offset = ($page - 1) * $limit;
		$ret['offset'] = $offset; 
		$ret['limit'] = $limit; 
		$ret['numPages'] = $numPages; 
		$ret['page'] = $page; 
		return $ret;
	}
	//-------------------------------------------------------------------------------------------------
	//function to create table
	//returns true or false indicating success
	function createTable($pics_tbl) {
		global $modx;
		$sql="CREATE TABLE $pics_tbl (
			`id` int(10) unsigned NOT NULL auto_increment,
			`gal_id` int(10) unsigned NOT NULL,
			`filename` varchar(50) NOT NULL,
			`title` text NOT NULL,
			`date` datetime NOT NULL,
			`descr` text default NULL,
			`pos` int(10) default NULL,
			`own_id` int(10) default NULL,
			PRIMARY KEY  (`id`)
	    ) TYPE=MyISAM AUTO_INCREMENT=1 ;";
		$query1=$modx->db->query($sql);
		return $query1;
	}
	//-------------------------------------------------------------------------------------------------
	//function to check database table for description field
	//returns true or false
	function isDescValid($pics_tbl, $table_desc){
		global $modx;
		for($i=0;$i<$modx->db->getRecordCount($table_desc);$i++)  {
			$temprow=$modx->db->getRow($table_desc, 'assoc');
			if ($temprow["Field"]=="descr") {
				return true;
			}
		}//if description field was not found, try to create it
		$query=$modx->db->query("ALTER TABLE ".$pics_tbl." ADD `descr` TEXT DEFAULT NULL ;");
		if($query){ 
			return true;
		}
		return false;
	}
	//-------------------------------------------------------------------------------------------------
	//function to check database table for position field
	//returns true or false
	function isPosValid($pics_tbl, $table_desc){
		global $modx;
		for($i=0;$i<$modx->db->getRecordCount($table_desc);$i++)  {
			$temprow=$modx->db->getRow($table_desc, 'assoc');
			if ($temprow["Field"]=="pos") {
				return true;
			}
		}//if position field was not found, try to create it
		$query=$modx->db->query("ALTER TABLE ".$pics_tbl." ADD `pos` int(10) DEFAULT NULL ;");
		if($query){ 
			return true;
		}
		return false;
	}
	//-------------------------------------------------------------------------------------------------
	//function to check database table for picture owner field
	//returns true or false
	function isOwnIDValid($pics_tbl, $table_desc){
		global $modx;
		for($i=0;$i<$modx->db->getRecordCount($table_desc);$i++)  {
			$temprow=$modx->db->getRow($table_desc, 'assoc');
			if ($temprow["Field"]=="own_id") {
				return true;
			}
		}//if position field was not found, try to create it
		$query=$modx->db->query("ALTER TABLE ".$pics_tbl." ADD `own_id` int(10) DEFAULT NULL ;");
		if($query){ 
			return true;
		}
		return false;
	}
	//-------------------------------------------------------------------------------------------------
	//function to check access permissions, fix provided by TobyL
	function checkPermissions($userid,$docid){
		global $modx;
		if($userid) {
			// check whether user is allowed to modify this page (-> $result1=1)
			$rs1=$modx->db->query("SELECT * FROM (" . $modx->db->config['table_prefix'] . "member_groups LEFT JOIN " . $modx->db->config['table_prefix'] . "membergroup_access ON user_group=membergroup) LEFT JOIN " . $modx->db->config['table_prefix'] . "document_groups ON documentgroup=document_group WHERE member='" . $userid . "' and document='" . $docid . "'");
			$result1=$modx->db->getRecordCount($rs1);

			// check if user is administrator (-> $result2=1)
			$rs2=$modx->db->query("SELECT * FROM " . $modx->db->config['table_prefix'] . "user_attributes WHERE id='" . $userid . "' AND role='1'");
			$result2=$modx->db->getRecordCount($rs2);

			if($result1>0 || $result2>0) {
				return true;
			}
		}
		
		//check whether user is logged in and belongs to selected webgroups
		if(count($this->mgconfig['manager_webgroups'])>0 && $modx->isMemberOfWebGroup($this->mgconfig['manager_webgroups'])){
			return true; 
		//check whether user is logged in and is defined in manager_webusers
		} else if ($modx->getLoginUserName()!="" && in_array($modx->getLoginUserName(), $this->mgconfig['manager_webusers'])){
			return true;
		} else {
			return false;
		}
	}
	//-------------------------------------------------------------------------------------------------
	//function to create the pics
	function createthumb($filename,$filetype,$path_to_gal,$prefix="",$resize=true) {
		if($prefix == "tn_"){
			$use_watermark = $this->mgconfig['thumb_use_watermark'];
			$max_thumb_size = $this->mgconfig['max_thumb_size'];
			$quality = $this->mgconfig['quality_thumb'];
			$watermark_type = $this->mgconfig['thumb_watermark_type'];
			$watermark_img = $this->mgconfig['thumb_watermark_img'];
			$watermark_txt = $this->mgconfig['thumb_watermark_txt'];
			$watermark_txt_color = $this->mgconfig['thumb_watermark_txt_color'];
			$watermark_font = $this->mgconfig['thumb_watermark_font'];
			$watermark_valign = $this->mgconfig['thumb_watermark_valign'];
			$watermark_halign = $this->mgconfig['thumb_watermark_halign'];
			$watermark_txt_hmargin = $this->mgconfig['thumb_watermark_txt_hmargin'];
			$watermark_txt_vmargin = $this->mgconfig['thumb_watermark_txt_vmargin'];
			$use_dropshadow = $this->mgconfig['thumb_use_dropshadow'];
			$dropshadow_bg = $this->mgconfig['thumb_shadow_bgcolor'];
			$shadow_path = $this->mgconfig['thumb_shadow_path'];
			$use_imagemask = $this->mgconfig['thumb_use_imagemask'];
			$imagemask_bg = $this->mgconfig['thumb_mask_bgcolor'];
			$imagemask_pos = $this->mgconfig['thumb_mask_position'];
			$imagemask_img = $this->mgconfig['thumb_mask_img'];
		}else if($prefix == "big_"){
			$max_thumb_size = $this->mgconfig['max_big_size'];
			$quality = $this->mgconfig['quality_big'];
			$use_watermark = $this->mgconfig['big_use_watermark'];
			$watermark_type = $this->mgconfig['big_watermark_type'];
			$watermark_img = $this->mgconfig['big_watermark_img'];
			$watermark_txt = $this->mgconfig['big_watermark_txt'];
			$watermark_txt_color = $this->mgconfig['big_watermark_txt_color'];
			$watermark_font = $this->mgconfig['big_watermark_font'];
			$watermark_valign = $this->mgconfig['big_watermark_valign'];
			$watermark_halign = $this->mgconfig['big_watermark_halign'];
			$watermark_txt_hmargin = $this->mgconfig['big_watermark_txt_hmargin'];
			$watermark_txt_vmargin = $this->mgconfig['big_watermark_txt_vmargin'];
			$use_dropshadow = $this->mgconfig['big_use_dropshadow'];
			$dropshadow_bg = $this->mgconfig['big_shadow_bgcolor'];
			$shadow_path = $this->mgconfig['big_shadow_path'];
			$use_imagemask = $this->mgconfig['big_use_imagemask'];
			$imagemask_bg = $this->mgconfig['big_mask_bgcolor'];
			$imagemask_pos = $this->mgconfig['big_mask_position'];
			$imagemask_img = $this->mgconfig['big_mask_img'];
		}else{
			$max_thumb_size = $this->mgconfig['max_pic_size'];
			$quality = $this->mgconfig['quality_pic'];
			$use_watermark = $this->mgconfig['pic_use_watermark'];
			$watermark_type = $this->mgconfig['pic_watermark_type'];
			$watermark_img = $this->mgconfig['pic_watermark_img'];
			$watermark_txt = $this->mgconfig['pic_watermark_txt'];
			$watermark_txt_color = $this->mgconfig['pic_watermark_txt_color'];
			$watermark_font = $this->mgconfig['pic_watermark_font'];
			$watermark_valign = $this->mgconfig['pic_watermark_valign'];
			$watermark_halign = $this->mgconfig['pic_watermark_halign'];
			$watermark_txt_hmargin = $this->mgconfig['pic_watermark_txt_hmargin'];
			$watermark_txt_vmargin = $this->mgconfig['pic_watermark_txt_vmargin'];
			$use_dropshadow = $this->mgconfig['pic_use_dropshadow'];
			$dropshadow_bg = $this->mgconfig['pic_shadow_bgcolor'];
			$shadow_path = $this->mgconfig['pic_shadow_path'];
			$use_imagemask = $this->mgconfig['pic_use_imagemask'];
			$imagemask_bg = $this->mgconfig['pic_mask_bgcolor'];
			$imagemask_pos = $this->mgconfig['pic_mask_position'];
			$imagemask_img = $this->mgconfig['pic_mask_img'];
		}
		//resize and watermark if needed
		include_once($modx->config['base_path'].'assets/snippets/maxigallery/watermark/Thumbnail.class.php');
		$thumb=new Thumbnail($path_to_gal.$filename);
		$thumb->quality=$quality;
		if($filetype=="jpeg"){
			$thumb->output_format='JPG';
		}else if($filetype=="png"){
			$thumb->output_format='PNG';
		}
		if($use_watermark){	
			//apply watermark
			if($watermark_type == "image"){
				$thumb->img_watermark=$watermark_img;
				$thumb->img_watermark_Valing=strtoupper($watermark_valign);
				$thumb->img_watermark_Haling=strtoupper($watermark_halign);
			}else{
				$thumb->txt_watermark=$watermark_txt;
				$thumb->txt_watermark_color=$watermark_txt_color;
				$thumb->txt_watermark_font=$watermark_font;
				$thumb->txt_watermark_Valing=strtoupper($watermark_valign);
				$thumb->txt_watermark_Haling=strtoupper($watermark_halign);
				$thumb->txt_watermark_Hmargin=strtoupper($watermark_txt_hmargin);
				$thumb->txt_watermark_Vmargin=strtoupper($watermark_txt_vmargin);
			}
		}
		if($resize){
			$thumb->size_auto($max_thumb_size);
		}
		$thumb->process();
		$thumb->save($path_to_gal.$prefix.$filename);

		//dropshadow
		if($use_dropshadow){
			include_once($modx->config['base_path'].'assets/snippets/maxigallery/dropshadow/class.dropshadow.php');
			$ds = new dropShadow(FALSE);
			$ds->setShadowPath($shadow_path);
			$ds->loadImage($path_to_gal.$prefix.$filename);
			$ds->applyShadow($dropshadow_bg);
			$ds->saveShadow($path_to_gal.$prefix.$filename,'',100);
			$ds->flushImages();
		}

		//imagemask
		if($use_imagemask){
			switch ($imagemask_pos) {
				case "topleft" :
					$mask_option = 0;
					break;
				case "top" :
					$mask_option = 1;
					break;
				case "topright" :
					$mask_option = 2;
					break;
				case "left" :
					$mask_option = 3;
					break;
				case "center" :
					$mask_option = 4;
					break;
				case "right" :
					$mask_option = 5;
					break;
				case "bottomleft" :
					$mask_option = 6;
					break;
				case "bottom" :
					$mask_option = 7;
					break;
				case "bottomright" :
					$mask_option = 8;
					break;
				case "resize" :
					$mask_option = 9;
					break;
				default :
					$mask_option = 9;
					break;
			}
			include_once($modx->config['base_path'].'assets/snippets/maxigallery/imagemask/class.imagemask.php');
			$im = new imageMask($imagemask_bg);
		    $im->setDebugging(false);
			$im->maskOption($mask_option);
			$im->loadImage($path_to_gal.$prefix.$filename);
			$im->applyMask($imagemask_img);
		    $im->saveImage($path_to_gal.$prefix.$filename);
		}
	}
	//-------------------------------------------------------------------------------------------------
	//function to create the gallery xml for slidebox
	function createGalleryXML($pics_tbl,$gal_id,$path_to_gal){
		global $modx;
		$res=$modx->db->query("SELECT * FROM " . $pics_tbl . " WHERE gal_id='" . $gal_id . "' ORDER BY " . $this->mgconfig['order_by'] . " " . $this->mgconfig['order_direction']);
		//pic_count
		$totalpics = $modx->db->getRecordCount($res);
		if($totalpics>0){
			// create a new XML document
			$xmlstr = '<?xml version="1.0" encoding="'.$modx->config['etomite_charset'].'"?>'."\n";
			$xmlstr .= '<response>'."\n";
			// process pics
			$i = 1;
			while($pic=$modx->fetchRow($res)) {
				$url = $modx->config['site_url'].$path_to_gal.$pic['filename'];
				//create pic nodes
				$xmlstr .= "\t".'<source id="'.$url.'">'."\n";
				$xmlstr .= "\t\t".'<title><![CDATA['.stripslashes($pic['title']).']]></title>'."\n";
				$xmlstr .= "\t\t".'<caption><![CDATA['.stripslashes($pic['descr']).']]></caption>'."\n";
				$xmlstr .= "\t\t".'<number><![CDATA['.$i.'/'.$totalpics.']]></number>'."\n";
				$xmlstr .= "\t".'</source>'."\n";
				$i++;
			}
			$xmlstr .= '</response>';
			//write the xml file
			$fp = fopen($path_to_gal."gallery.xml", "w");
			fwrite($fp, $xmlstr);
			fclose($fp);
			chmod($path_to_gal."gallery.xml",0666);
		}else if(file_exists($path_to_gal."gallery.xml")){ //if gallery xml exists but not in use, delete it
			unlink($path_to_gal."gallery.xml");
		}
	}
	//-------------------------------------------------------------------------------------------------
	//function to delete non empty directory
	function deldir($dir){
	  $current_dir = opendir($dir);
	  while($entryname = readdir($current_dir)){
	     if(is_dir("$dir/$entryname") and ($entryname != "." and $entryname!="..")){
	        $this->deldir("${dir}/${entryname}");
	     }elseif($entryname != "." and $entryname!=".."){
	        unlink("${dir}/${entryname}");
	     }
	  }
	  closedir($current_dir);
	  rmdir(${dir});
	}
	//-------------------------------------------------------------------------------------------------
	//function to convert gif to png
	function gif2png($path_to_gal,$name){
		$src=imagecreatefromgif($path_to_gal.$name);
		//calculate size for the image
		$src_size = getimagesize($path_to_gal.$name);
		//create blank destination image
		$dest=imagecreate($src_size[0],$src_size[1]);
		//delete gif image
		unlink($path_to_gal.$name);
		$name = str_replace(".gif", ".png", $name);
		//resize the image
		if(function_exists('imagecopyresampled')){
			imagecopyresampled($dest,$src,0,0,0,0,$src_size[0],$src_size[1],$src_size[0],$src_size[1]);
		}else{
			imagecopyresized($dest,$src,0,0,0,0,$src_size[0],$src_size[1],$src_size[0],$src_size[1]);
		}
		//create new image
		imagepng($dest,$path_to_gal.$name);
		return $name;
	}
	//-------------------------------------------------------------------------------------------------
	//function to figure out what pics to do from the file and do them
	function handlePics($pics_tbl, $path_to_gal, $gal_id, $name, $type){
		global $modx;
		$pic_date = "NOW()";
		//read EXIF information
		if($type == "jpg"){
			if(function_exists("exif_read_data")) {
				$exif = exif_read_data($path_to_gal.$name, 0, true);
				if(array_key_exists("EXIF", $exif)) {
					$pic_date = " '".$exif["EXIF"]["DateTimeOriginal"]."' ";
				}
			}
		}
		//if gif image, convert to png
		if($type == "gif"){
			$name = $this->gif2png($path_to_gal,$name);
			$type = "png";
		}
		$imagesize=getimagesize($path_to_gal.$name);
		//create bigger image if needed
		if ($this->mgconfig['keep_bigimg'] == true){
			if($this->mgconfig['max_big_size'] != 0){
				if($this->mgconfig['max_pic_size']>0 && ($imagesize[0]>$this->mgconfig['max_pic_size'] || $imagesize[1]>$this->mgconfig['max_pic_size'])) {
					if($this->mgconfig['max_big_size']>0 && ($imagesize[0]>$this->mgconfig['max_big_size'] || $imagesize[1]>$this->mgconfig['max_big_size'])){
						//if picture size is bigger than big pic size, resize
						$this->createthumb($name,$type,$path_to_gal,"big_");
					}else if($this->mgconfig['big_use_watermark'] || $this->mgconfig['big_use_dropshadow'] || $this->mgconfig['big_use_imagemask']){
						//if not bigger, but image still needs to be changed
						$this->createthumb($name,$type,$path_to_gal,"big_",false);
					}else{
						//else just copy
						copy($path_to_gal.$name, $path_to_gal."big_".$name);
					}
				}
			}else if($this->mgconfig['big_use_watermark'] || $this->mgconfig['big_use_dropshadow'] || $this->mgconfig['big_use_imagemask']){
				//if max size is not set for the big pics, but image still needs to be changed
				$this->createthumb($name,$type,$path_to_gal,"big_",false);		
			}else{
				//else just copy
				copy($path_to_gal.$name, $path_to_gal."big_".$name);
			}
		}
		//create thumbnail
		$this->createthumb($name,$type,$path_to_gal,"tn_");
		//create normal 
		if($this->mgconfig['max_pic_size']>0 && ($imagesize[0]>$this->mgconfig['max_pic_size'] || $imagesize[1]>$this->mgconfig['max_pic_size'])) {
			$this->createthumb($name,$type,$path_to_gal,"");
		}else if($this->mgconfig['pic_use_watermark'] || $this->mgconfig['pic_use_dropshadow'] || $this->mgconfig['pic_use_imagemask']){
			//if max image size is not reached, but the image needs some changing to be done
			$this->createthumb($name,$type,$path_to_gal,"",false);
		}
		if($modx->getLoginUserID()!="" && $modx->getLoginUserType()=='web'){ //if web user is posting picture, put owner id
			$rs1=$modx->db->query("INSERT INTO " . $pics_tbl . "(id, gal_id, filename, title, date, own_id) VALUES(NULL,'" . $gal_id . "','" . $name . "',''," . $pic_date . ",'".$modx->getLoginUserID()."')");
		}else{
			$rs1=$modx->db->query("INSERT INTO " . $pics_tbl . "(id, gal_id, filename, title, date) VALUES(NULL,'" . $gal_id . "','" . $name . "',''," . $pic_date . ")");
		}
	}
	//-------------------------------------------------------------------------------------------------
	//function to add slideshow styles
	function regSlideshowCSS($gal_id){
		global $modx;
		$slideshow_css_str = '
			<style type="text/css">
			#mySlideshow'.$gal_id.'
			{
			width: '.$this->mgconfig['slideshow_width'].'px;
			height: '.$this->mgconfig['slideshow_height'].'px;
			}
			</style>
			';
		$slideshow_script_str1 = '
			<script type="text/javascript">
			function startSlideshow() {
			var slideshow = new timedSlideShow($(\'mySlideshow'.$gal_id.'\'), mySlideData'.$gal_id.', '.$this->mgconfig['slideshow_slidedelay'].', '.$this->mgconfig['slideshow_blendduration'].', '.$this->mgconfig['slideshow_showinfo'].', '.$this->mgconfig['slideshow_infodelay'].');
			}
			addLoadEvent(startSlideshow);
			</script>';
		$slideshow_script_str2 = '
			<script type="text/javascript">
			function startSlideshow() {
			var slideshow = new showcaseSlideShow($(\'mySlideshow'.$gal_id.'\'), mySlideData'.$gal_id.', '.$this->mgconfig['slideshow_blendduration'].', '.$this->mgconfig['slideshow_showinfo'].', '.$this->mgconfig['slideshow_infodelay'].');
			}
			addLoadEvent(startSlideshow);
			</script>';
		$modx->regClientCSS($slideshow_css_str);
		if($this->mgconfig['slideshow_mode'] == "timed") {
			$modx->regClientScript($slideshow_script_str1);
		} else {
			$modx->regClientScript($slideshow_script_str2); 		
		}
	}
	//-------------------------------------------------------------------------------------------------
	//function to load correct css and scripts
	function regScriptsAndCSS(){
		global $modx;

		//slidebox css
		$slidebox_css_link = '<link rel="stylesheet" href="'. $modx->config['base_url'].'assets/snippets/maxigallery/slidebox/style.css" type="text/css" media="screen" />';
		$slidebox_css_str = '
			<!--[if gte IE 5.5]>
			<![if lt IE 7]>
			<style type="text/css">
			* html #overlay{
			background-color: #333;
			back\ground-color: transparent;
			background-image: url(' . $modx->config['base_url'] . 'assets/snippets/maxigallery/slidebox/blank.gif);
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader (src="' . $modx->config['base_url'] . 'assets/snippets/maxigallery/slidebox/overlay.png", sizingMethod="scale");
			</style>
			<![endif]>
			<![endif]-->';
		
		//slidebox scripts
		$slidebox_script_link1 = $modx->config['base_url'] . 'assets/snippets/maxigallery/slidebox/slidebox_setup.js';
		$slidebox_settings = 'assets/snippets/maxigallery/slidebox/slidebox_lang_'.$this->mgconfig['lang'].'.js';
		if(file_exists($modx->config['base_path'].$slidebox_settings)){
			$slidebox_script_link2 = $modx->config['base_url'] . $slidebox_settings;
		}else{
			$slidebox_script_link2 = $modx->config['base_url'] . 'assets/snippets/maxigallery/slidebox/slidebox_lang_en.js';
		}
		$slidebox_script_link3 = $modx->config['base_url'] . 'assets/snippets/maxigallery/slidebox/prototype.js';
		$slidebox_script_link4 = $modx->config['base_url'] . 'assets/snippets/maxigallery/slidebox/slidebox.js';

		//lightbox scripts
		$lightboxv2_css_link = '<link rel="stylesheet" href="' . $modx->config['base_url'] . 'assets/snippets/maxigallery/lightboxv2/css/lightbox.css" type="text/css" media="screen" />';
		$lightboxv2_script_link1 = $modx->config['base_url'] . 'assets/snippets/maxigallery/lightboxv2/js/lightbox_setup.js'; 
		$lightboxv2_settings = 'assets/snippets/maxigallery/lightboxv2/js/lightbox_lang_'.$this->mgconfig['lang'].'.js';
		if(file_exists($modx->config['base_path'].$lightboxv2_settings)){
			$lightboxv2_script_link2 = $modx->config['base_url'] . $lightboxv2_settings;
		}else{
			$lightboxv2_script_link2 = $modx->config['base_url'] . 'assets/snippets/maxigallery/lightboxv2/js/lightbox_lang_en.js';
		}
		$lightboxv2_script_link3 = $modx->config['base_url'] . 'assets/snippets/maxigallery/lightboxv2/js/prototype.js';
		$lightboxv2_script_link4 = $modx->config['base_url'] . 'assets/snippets/maxigallery/lightboxv2/js/scriptaculous.js?load=effects';
		$lightboxv2_script_link5 = $modx->config['base_url'] . 'assets/snippets/maxigallery/lightboxv2/js/lightbox.js';
		
		//slimbox scripts
		$slimbox_css_link = '<link rel="stylesheet" href="' . $modx->config['base_url'] . 'assets/snippets/maxigallery/slimbox/css/slimbox.css" type="text/css" media="screen" />';
		$slimbox_script_link1 = $modx->config['base_url'] . 'assets/snippets/maxigallery/slimbox/js/mootools.relese.83.js'; 
		$slimbox_settings = 'assets/snippets/maxigallery/slimbox/js/slimbox_lang_'.$this->mgconfig['lang'].'.js';
		if(file_exists($modx->config['base_path'].$slimbox_settings)){
			$slimbox_script_link2 = $modx->config['base_url'] . $slimbox_settings;
		}else{
			$slimbox_script_link2 = $modx->config['base_url'] . 'assets/snippets/maxigallery/slimbox/js/slimbox_lang_en.js';
		}
		$slimbox_script_link3 = $modx->config['base_url'] . 'assets/snippets/maxigallery/slimbox/js/slimbox.js';		
		
		//slideshow scripts
		$slideshow_css_link = '<link rel="stylesheet" href="' . $modx->config['base_url'] . 'assets/snippets/maxigallery/smoothslideshow2/css/jd.slideshow.css" type="text/css" media="screen" />';
		$slideshow_script_link1 = $modx->config['base_url'] . 'assets/snippets/maxigallery/smoothslideshow2/js/mootools.release.64.js'; 
		$slideshow_script_link3 = $modx->config['base_url'] . 'assets/snippets/maxigallery/smoothslideshow2/js/timed.slideshow.js';
		$slideshow_script_link4 = $modx->config['base_url'] . 'assets/snippets/maxigallery/smoothslideshow2/js/showcase.slideshow.js';		
		
				
		//custom scripts
		$popup_script = $modx->config['base_url'] . 'assets/snippets/maxigallery/js/popup.js';
		$external_script = $modx->config['base_url'] . 'assets/snippets/maxigallery/js/external.js';
		$click_script = $modx->config['base_url'] . 'assets/snippets/maxigallery/js/click.js';

		//register popup helper if in use
		if(($this->mgconfig['display'] == "embedded" && $this->mgconfig['embedtype'] == "popup") || ($this->mgconfig['keep_bigimg'] && $this->mgconfig['big_img_linkstyle'] == "popup" )){
			$modx->regClientStartupScript($popup_script);
		}

		//register external helper if in use
		if(($this->mgconfig['display'] == "embedded" && $this->mgconfig['embedtype'] == "external") || ($this->mgconfig['keep_bigimg'] && $this->mgconfig['big_img_linkstyle'] == "external" )){
			$modx->regClientStartupScript($external_script);
		}
		
		//register rightclick disabling if enabled
		if($this->mgconfig['disable_rightclick']){
			$modx->regClientStartupScript($click_script);
		}
		
		//register slidebox scripts if in use
		if(($this->mgconfig['display'] == "embedded" && $this->mgconfig['embedtype'] == "slidebox") || ($this->mgconfig['keep_bigimg'] && $this->mgconfig['big_img_linkstyle'] == "slidebox")){
			$modx->regClientCSS($slidebox_css_link);
			$modx->regClientCSS($slidebox_css_str);
			$modx->regClientStartupScript($slidebox_script_link1);
			$modx->regClientStartupScript($slidebox_script_link2);
			$modx->regClientStartupScript($slidebox_script_link3);
			$modx->regClientStartupScript($slidebox_script_link4);
		}
		
		//register lightboxv2.0 scripts if in use
		if(($this->mgconfig['display'] == "embedded" && $this->mgconfig['embedtype'] == "lightboxv2") || ($this->mgconfig['keep_bigimg'] && $this->mgconfig['big_img_linkstyle'] == "lightboxv2")){
			$modx->regClientCSS($lightboxv2_css_link);
			$modx->regClientStartupScript($lightboxv2_script_link1);
			$modx->regClientStartupScript($lightboxv2_script_link2);
			$modx->regClientStartupScript($lightboxv2_script_link3);
			$modx->regClientStartupScript($lightboxv2_script_link4);
			$modx->regClientStartupScript($lightboxv2_script_link5);
		}
		
		//register slimbox scripts if in use
		if(($this->mgconfig['display'] == "embedded" && $this->mgconfig['embedtype'] == "slimbox") || ($this->mgconfig['keep_bigimg'] && $this->mgconfig['big_img_linkstyle'] == "slimbox")){
			$modx->regClientCSS($slimbox_css_link);
			$modx->regClientStartupScript($slimbox_script_link1);
			$modx->regClientStartupScript($slimbox_script_link2);
			$modx->regClientStartupScript($slimbox_script_link3);
		}
		
		//register slideshow scripts if in use
		if(($this->mgconfig['display'] == "embedded" && $this->mgconfig['embedtype'] == "slideshow") || ($this->mgconfig['slideshow_pictureview'])) {
			$modx->regClientCSS($slideshow_css_link);
			$modx->regClientStartupScript($slideshow_script_link1);
			if($this->mgconfig['slideshow_mode'] == "timed") {
				$modx->regClientStartupScript($slideshow_script_link3);
			} else {
				$modx->regClientStartupScript($slideshow_script_link4);		
			}
		}
		
		//register css and javascript from snippet parameters
		if ($this->mgconfig['css'] != "") {
			if ($modx->getChunk($this->mgconfig['css']) != "") {
				$modx->regClientCSS($modx->getChunk($this->mgconfig['css']));
			} else if (file_exists($modx->config['base_path'].$this->mgconfig['css'])) {
				$modx->regClientCSS('<link rel="stylesheet" href="'. $modx->config['base_url'].$this->mgconfig['css'].'" type="text/css" media="screen" />');
			} else {	
				$modx->regClientCSS($this->mgconfig['css']);
			}
		}
		if ($this->mgconfig['js'] != "") {
			if ($modx->getChunk($this->mgconfig['js']) != "") {
				$modx->regClientStartupScript($modx->getChunk($this->mgconfig['js']));
			} else if (file_exists($modx->config['base_path'].$this->mgconfig['js'])) {
				$modx->regClientStartupScript($modx->config['base_url'].$this->mgconfig['js']);
			} else {
				$modx->regClientStartupScript($this->mgconfig['js']);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------
	//function to clear the picture filenames
	function clearFilename($filename){
		//try to salvage some of the usual illegal characters
		$filename = strtr($filename, "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöùúûüýþÿ ", "aaaaaaaceeeeiiiidnooooo0uuuuypsaaaaaaaceeeeiiiionooooouuuuypy_");
		//get filename without extension
		$name = substr($filename, 0, strrpos($filename,"."));
		//get extension
		$ext = substr(strrchr($filename, "."), 0);
		//strip illegal characters from the filename and double "_"
		$name = strtolower(preg_replace("/_+/", '_', preg_replace("/[^a-zA-Z0-9\-\_]/", '_', $name))); //clear filename
		//return the cleaned filename
		return $name.$ext;
	}
	//-------------------------------------------------------------------------------------------------
	//function to get all child documents by Mark Kaplan
	function getAllSubDocuments($resourceparent = array (), $sortby = "createdon", $sortdir = "desc", $descendentDepth = 1,$showPublishedOnly = true, $seeThruUnpub = false) {
		global $modx;

		// ---------------------------------------------------
		// Seed list of viable ids
		// ---------------------------------------------------

		$seedArray = explode(',', $resourceparent);

		$kids = array();
		foreach ($seedArray AS $seed) {
			$kids = $this->getChildIds($seed, $descendentDepth, $kids);
		}
		$kids = array_values($kids);

		$resource = $modx->getDocuments($kids, $showPublishedOnly, 0, "*", '', $sortby, $sortdir);
			
		return $resource;

	}
	//-------------------------------------------------------------------------------------------------	
	//function to get all childs by Jason Coward
	function getChildIds($id, $depth= 10, $children= array()) {
		global $modx;
		$c= null;
		foreach ($modx->documentMap as $mapEntry) {
			if (isset ($mapEntry[$id])) {
				$childId= $mapEntry[$id];
				$childKey= array_search($childId, $modx->documentListing);
				if (!$childKey) {
					$childKey= "$childId";
				}
				$c[$childKey]= $childId;
			}
     	}
     	$depth--;
		if (is_array($c)) {
			if (is_array($children)) {
				$children= $children + $c;
			} else {
				$children= $c;
			}
			if ($depth) {
				foreach ($c as $child) {
					$children= $children + $this->getChildIds($child, $depth, $children);
				}
			}
     	}
		return $children;
	}
	
	//-------------------------------------------------------------------------------------------------	
	//function to generate urls for pagination by keeping existing url parameters by bS
	//http://modxcms.com/forums/index.php/topic,5309.0.html
	function getPaginationUrl($docid, $docalias, $array_values) {
		global $modx;
		$array_url = $_GET;
		$urlstring = array();
		
		unset($array_url["id"]);
		unset($array_url["q"]);
		
		$array_url = array_merge($array_url,$array_values);

		foreach ($array_url as $name => $value) {
			if (!is_null($value)) {
			  $urlstring[] = $name . '=' . urlencode($value);
			}
		}
		
		return $modx->makeUrl($docid, $docalias, join('&',$urlstring));
	}
}
//-------------------------------------------------------------------------------------------------
?>