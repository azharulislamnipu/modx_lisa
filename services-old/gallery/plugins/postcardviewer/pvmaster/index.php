<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <?php
      /* Leave this block of php code intact if customizing this page */
      error_reporting(0);
      $preferences = file_get_contents('preferences.txt');
      $preferences = unserialize($preferences);
      $title = htmlspecialchars($preferences['htmlTitle'], ENT_QUOTES, 'UTF-8');
      $backgroundColor = '#'.substr($preferences['backgroundColor'], 2, strlen($preferences['backgroundColor']) - 2);
      $xmlUrl = $preferences['xmlUrl'];
      print '<title>'.$title.'</title>';
    ?>
    <script type="text/javascript" src="swfobject.js"></script>
    <style type="text/css">
      /* If you are adding your own headers and footers then set these heights in pixels */	
    	html {
    		height: 100%;
    		overflow: hidden;
    	}	
    	#flashcontent {
    		height: 100%;
    	}
    	body {
    		height: 100%;
    		margin: 0;
    		padding: 0;
    		background-color: #181818;
    		color:#ffffff;
    		font-family:sans-serif;
    		font-size:40;
    	}	
    	a {	
    		color:#cccccc;
    	}
    </style>
  </head>
  <body>
  	<div id="flashcontent">PostCardViewer requires JavaScript and the Flash Player. <a href="http://get.adobe.com/flashplayer/">Get Flash here.</a> </div>	
  	<script type="text/javascript">
  		var fo = new SWFObject("viewer.swf", "viewer", "100%", "100%", "8", "<?php echo $backgroundColor; ?>");		
  		fo.addVariable("xmlURL", "gallery.xml?nocache=<?php print rand(); ?>");
  		//Optional Configuration
  		//fo.addVariable("langOpenImage", "Open Image in New Window");
  		//fo.addVariable("langAbout", "About");	
  		fo.write("flashcontent");	
  	</script>	
  </body>
</html>