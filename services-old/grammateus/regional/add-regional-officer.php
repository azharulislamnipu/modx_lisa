<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("CHAPTERCD", true, "text", "", "", "", "");
$formValidation->addField("CUSTOMERCD", true, "numeric", "", "", "", "");
$formValidation->addField("CPOSITION", true, "text", "", "", "", "");
$formValidation->addField("JOINDATE", true, "date", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsBoules = "-1";
if (isset($_GET['REGIONCD'])) {
  $colname_rsBoules = $_GET['REGIONCD'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoules = sprintf("SELECT CHAPTERID, BOULENAME, CITY, STATECD, REGIONCD, REGIONNAME FROM spp_boule WHERE REGIONCD = %s ORDER BY BOULENAME ASC", GetSQLValueString($colname_rsBoules, "text"));
$rsBoules = mysql_query($query_rsBoules, $sigma_modx) or die(mysql_error());
$row_rsBoules = mysql_fetch_assoc($rsBoules);
$totalRows_rsBoules = mysql_num_rows($rsBoules);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchons = "SELECT CUSTOMERID, REGIONNAME, REGIONCD, BOULENAME, CHAPTERID, BOULECITY, BOULESTATECD, STATUS, STATUSSTT, CUSTOMERCLASSSTT, CUSTOMERTYPE, JOINDATE, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, SUFFIX, DESIGNATIONLST, L2R_FULLNAME, FULLNAME, FULLNAME2, L2R_FULLNAME2, ADDRESS1, ADDRESS2, CITY, STATECD, ZIP, HOMEPHONE, EMAIL, JOBTITLE, OCCUPATIONCD, SKILLCDLST, ORGNAME, ALTADDRESS1, ALTADDRESS2, ALTCITY, ALTSTATE, ALTZIP, WORKPHONE, ALTEMAIL, BIRTHDATE, SPOUSENAME, LASTUPDATED, UPDATEDBY, GRAMMUPDATE, GRAMM, IMAGE, DECEASEDDATE, WEB_EMAIL, WEB_ID FROM vw_elig_delegate ORDER BY FULLNAME ASC";
$rsArchons = mysql_query($query_rsArchons, $sigma_modx) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysql_num_rows($rsArchons);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsRegionalOffices = "SELECT    spp_cposition.CPOSITIONID,   spp_cposition.CPOSITION FROM   spp_cposition WHERE   spp_cposition.CPOSITION LIKE '%egio%' AND    spp_cposition.CPOSITION NOT LIKE '%ounci%' ORDER BY   spp_cposition.CPOSITIONID";
$rsRegionalOffices = mysql_query($query_rsRegionalOffices, $sigma_modx) or die(mysql_error());
$row_rsRegionalOffices = mysql_fetch_assoc($rsRegionalOffices);
$totalRows_rsRegionalOffices = mysql_num_rows($rsRegionalOffices);

// Make an insert transaction instance
$ins_spp_officers = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_officers);
// Register triggers
$ins_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/duplicate-of-manage-region-archons-and-officers.php");
// Add columns
$ins_spp_officers->setTable("spp_officers");
$ins_spp_officers->addColumn("CHAPTERCD", "STRING_TYPE", "POST", "CHAPTERCD");
$ins_spp_officers->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$ins_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "POST", "CPOSITION");
$ins_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$ins_spp_officers->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$ins_spp_officers->addColumn("REGIONCD", "STRING_TYPE", "POST", "REGIONCD", "{GET.REGIONCD}");
$ins_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "POST", "COMMITTEESTATUSSTT", "Active");
$ins_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "POST", "UPDATEUSERCD", "{GET.UPDATEDBY}");
$ins_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "POST", "UPDATETMS", "{NOW_DT}");
$ins_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_officers = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_officers);
// Register triggers
$upd_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/duplicate-of-manage-region-archons-and-officers.php");
// Add columns
$upd_spp_officers->setTable("spp_officers");
$upd_spp_officers->addColumn("CHAPTERCD", "STRING_TYPE", "POST", "CHAPTERCD");
$upd_spp_officers->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$upd_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "POST", "CPOSITION");
$upd_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$upd_spp_officers->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$upd_spp_officers->addColumn("REGIONCD", "STRING_TYPE", "GET", "REGIONCD");
$upd_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "VALUE", "Active");
$upd_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "GET", "UPDATEDBY");
$upd_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE", "GET", "OFFICERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_officers = $tNGs->getRecordset("spp_officers");
$row_rsspp_officers = mysql_fetch_assoc($rsspp_officers);
$totalRows_rsspp_officers = mysql_num_rows($rsspp_officers);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/JSRecordset.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/DependentDropdown.js"></script>
<?php
//begin JSRecordset
$jsObject_rsArchons = new WDG_JsRecordset("rsArchons");
echo $jsObject_rsArchons->getOutput();
//end JSRecordset
?>
<?php
//begin JSRecordset
$jsObject_rsBoules = new WDG_JsRecordset("rsBoules");
echo $jsObject_rsBoules->getOutput();
//end JSRecordset
?>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<div class="contentBlock">
  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">
    <h1>ADD NEW REGIONAL OFFICER</h1>
    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_officers > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php } 
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <tr>
              <td class="KT_th"><label for="CHAPTERCD_<?php echo $cnt1; ?>">BOULE:</label></td>
              <td><select name="CHAPTERCD_<?php echo $cnt1; ?>" id="CHAPTERCD_<?php echo $cnt1; ?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                <?php 
do {  
?>
                <option value="<?php echo $row_rsBoules['CHAPTERID']?>"<?php if (!(strcmp($row_rsBoules['CHAPTERID'], $row_rsspp_officers['CHAPTERCD']))) {echo "SELECTED";} ?>><?php echo $row_rsBoules['BOULENAME']?></option>
                <?php
} while ($row_rsBoules = mysql_fetch_assoc($rsBoules));
  $rows = mysql_num_rows($rsBoules);
  if($rows > 0) {
      mysql_data_seek($rsBoules, 0);
	  $row_rsBoules = mysql_fetch_assoc($rsBoules);
  }
?>
              </select>
                  <?php echo $tNGs->displayFieldError("spp_officers", "CHAPTERCD", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="CUSTOMERCD_<?php echo $cnt1; ?>">ARCHON:</label></td>
              <td><select name="CUSTOMERCD_<?php echo $cnt1; ?>" id="CUSTOMERCD_<?php echo $cnt1; ?>" wdg:subtype="DependentDropdown" wdg:type="widget" wdg:recordset="rsArchons" wdg:displayfield="FULLNAME" wdg:valuefield="CUSTOMERID" wdg:fkey="CHAPTERID" wdg:triggerobject="CHAPTERCD_<?php echo $cnt1; ?>" wdg:selected="">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
              </select>
                <?php echo $tNGs->displayFieldError("spp_officers", "CUSTOMERCD", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="CPOSITION_<?php echo $cnt1; ?>">OFFICE:</label></td>
              <td><select name="CPOSITION_<?php echo $cnt1; ?>" id="CPOSITION_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                  <?php 
do {  
?>
                  <option value="<?php echo $row_rsRegionalOffices['CPOSITION']?>"<?php if (!(strcmp($row_rsRegionalOffices['CPOSITION'], $row_rsspp_officers['CPOSITION']))) {echo "SELECTED";} ?>><?php echo $row_rsRegionalOffices['CPOSITION']?></option>
                  <?php
} while ($row_rsRegionalOffices = mysql_fetch_assoc($rsRegionalOffices));
  $rows = mysql_num_rows($rsRegionalOffices);
  if($rows > 0) {
      mysql_data_seek($rsRegionalOffices, 0);
	  $row_rsRegionalOffices = mysql_fetch_assoc($rsRegionalOffices);
  }
?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_officers", "CPOSITION", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="JOINDATE_<?php echo $cnt1; ?>">START DATE:</label></td>
              <td><input name="JOINDATE_<?php echo $cnt1; ?>" id="JOINDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_officers['JOINDATE']); ?>" size="10" maxlength="22" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("JOINDATE");?> <?php echo $tNGs->displayFieldError("spp_officers", "JOINDATE", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMMENT_<?php echo $cnt1; ?>">COMMENT:</label></td>
              <td><textarea name="COMMENT_<?php echo $cnt1; ?>" id="COMMENT_<?php echo $cnt1; ?>" cols="80" rows="5"><?php echo KT_escapeAttribute($row_rsspp_officers['COMMENT']); ?></textarea>
                  <?php echo $tNGs->displayFieldHint("COMMENT");?> <?php echo $tNGs->displayFieldError("spp_officers", "COMMENT", $cnt1); ?> </td>
            </tr>
          </table>
          <input type="hidden" name="kt_pk_spp_officers_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_officers['kt_pk_spp_officers']); ?>" />
          <input type="hidden" name="REGIONCD_<?php echo $cnt1; ?>" id="REGIONCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_officers['REGIONCD']); ?>" />
          <input type="hidden" name="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" id="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_officers['COMMITTEESTATUSSTT']); ?>" />
          <input type="hidden" name="UPDATEUSERCD_<?php echo $cnt1; ?>" id="UPDATEUSERCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_officers['UPDATEUSERCD']); ?>" />
          <input type="hidden" name="UPDATETMS_<?php echo $cnt1; ?>" id="UPDATETMS_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_officers['UPDATETMS']); ?>" />
          <?php } while ($row_rsspp_officers = mysql_fetch_assoc($rsspp_officers)); ?>
        <div class="KT_bottombuttons">
          <div>
            <?php 
      // Show IF Conditional region1
      if (@$_GET['OFFICERID'] == "") {
      ?>
              <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
              <?php 
      // else Conditional region1
      } else { ?>
              <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <?php }
      // endif Conditional region1
      ?>
            <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/duplicate-of-manage-region-archons-and-officers.php')" />
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsBoules);

mysql_free_result($rsArchons);

mysql_free_result($rsRegionalOffices);
?>
