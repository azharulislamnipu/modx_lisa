<?php get_header() ?>

	<div id="content">
		<div class="padder">

		<?php do_action( 'sigma_before_blog_single_post' ) ?>

		<div class="page" id="blog-single">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="post" id="post-<?php the_ID(); ?>">

					<div class="post-content">

						<?php // Check that the user is allowed to view the content
							  $can_view_content = can_view_content();
							  $can_view_boule_only = can_view_boule_only(); ?>
						<?php if ( $can_view_content ) : ?>
							<h2 class="posttitle"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( 'Permanent Link to', 'archon-network' ) ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
							<div class="contentBlock">
								<div class="entry">
									<?php the_content(); ?>

									<?php wp_link_pages(array('before' => __( '<p><strong>Pages:</strong> ', 'archon-network' ), 'after' => '</p>', 'next_or_number' => 'number')); ?>
								</div>
							</div>
							<div class="contentBlockFooter">
								<span class="icon archive"><?php _e( 'Posted in', 'archon-network' ) ?> <?php the_category(', ') ?></span>
								<?php
									// Only Boule members can comment
									if ( $can_view_boule_only ) {
										comments_popup_link( __( 'No Comments', 'archon-network' ), __( '1 Comment', 'archon-network' ), __( '% Comments', 'archon-network' ), 'icon comment' );
									}
								?>
							</div>

						<?php else: ?>
							<h2 class="posttitle"><?php _e( 'Unauthorized', 'archon-network' ); ?></h2>
							<div class="contentBlock">
								<div class="entry">
									<p><?php _e( 'You are not allowed to view this content.', 'archon-network' ); ?></p>
								</div>
							</div>
						<?php endif; ?>

					</div>

				</div>

			<?php if ( $can_view_boule_only ) : ?>
				<?php comments_template(); ?>
			<?php endif; ?>

			<?php endwhile; else: ?>

				<p><?php _e( 'Sorry, no posts matched your criteria.', 'archon-network' ) ?></p>

			<?php endif; ?>

		</div>

		<?php do_action( 'sigma_after_blog_single_post' ) ?>

		</div><!-- .padder -->
	</div><!-- #content -->

	<?php locate_template( array( 'sidebar.php' ), true ) ?>

<?php get_footer() ?>