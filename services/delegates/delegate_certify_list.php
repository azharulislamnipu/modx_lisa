<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listrsDelegation1 = new TFI_TableFilter($conn_sigma_modx, "tfi_listrsDelegation1");
$tfi_listrsDelegation1->addColumn("delegate_type", "STRING_TYPE", "delegate_type", "%");
$tfi_listrsDelegation1->addColumn("STATUS", "STRING_TYPE", "STATUS", "%");
$tfi_listrsDelegation1->addColumn("L2R_FULLNAME_NORMAL", "STRING_TYPE", "L2R_FULLNAME_NORMAL", "%");
$tfi_listrsDelegation1->addColumn("HOMEPHONE", "STRING_TYPE", "HOMEPHONE", "%");
$tfi_listrsDelegation1->addColumn("EMAIL", "STRING_TYPE", "EMAIL", "%");
$tfi_listrsDelegation1->Execute();

// Sorter
$tso_listrsDelegation1 = new TSO_TableSorter("rsDelegation", "tso_listrsDelegation1");
$tso_listrsDelegation1->addColumn("delegate_type");
$tso_listrsDelegation1->addColumn("STATUS");
$tso_listrsDelegation1->addColumn("L2R_FULLNAME_NORMAL");
$tso_listrsDelegation1->addColumn("HOMEPHONE");
$tso_listrsDelegation1->addColumn("EMAIL");
$tso_listrsDelegation1->setDefault("delegate_type");
$tso_listrsDelegation1->Execute();

// Navigation
$nav_listrsDelegation1 = new NAV_Regular("nav_listrsDelegation1", "rsDelegation", "../", $_SERVER['PHP_SELF'], 25);

//NeXTenesio3 Special List Recordset
$maxRows_rsDelegation = $_SESSION['max_rows_nav_listrsDelegation1'];
$pageNum_rsDelegation = 0;
if (isset($_GET['pageNum_rsDelegation'])) {
  $pageNum_rsDelegation = $_GET['pageNum_rsDelegation'];
}
$startRow_rsDelegation = $pageNum_rsDelegation * $maxRows_rsDelegation;

$colname_rsDelegation = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname_rsDelegation = $_GET['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter_rsDelegation = "1=1";
if (isset($_SESSION['filter_tfi_listrsDelegation1'])) {
  $NXTFilter_rsDelegation = $_SESSION['filter_tfi_listrsDelegation1'];
}
// Defining List Recordset variable
$NXTSort_rsDelegation = "delegate_type";
if (isset($_SESSION['sorter_tso_listrsDelegation1'])) {
  $NXTSort_rsDelegation = $_SESSION['sorter_tso_listrsDelegation1'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_rsDelegation = sprintf("SELECT * FROM vw_delegations WHERE BOULENAME = %s AND delegate_status = 'A' AND  {$NXTFilter_rsDelegation} ORDER BY {$NXTSort_rsDelegation} ", GetSQLValueString($colname_rsDelegation, "text"));
$query_limit_rsDelegation = sprintf("%s LIMIT %d, %d", $query_rsDelegation, $startRow_rsDelegation, $maxRows_rsDelegation);
$rsDelegation = mysql_query($query_limit_rsDelegation, $sigma_modx) or die(mysql_error());
$row_rsDelegation = mysql_fetch_assoc($rsDelegation);

if (isset($_GET['totalRows_rsDelegation'])) {
  $totalRows_rsDelegation = $_GET['totalRows_rsDelegation'];
} else {
  $all_rsDelegation = mysql_query($query_rsDelegation);
  $totalRows_rsDelegation = mysql_num_rows($all_rsDelegation);
}
$totalPages_rsDelegation = ceil($totalRows_rsDelegation/$maxRows_rsDelegation)-1;
//End NeXTenesio3 Special List Recordset

$nav_listrsDelegation1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_delegate_type {width:70px; overflow:hidden;}
  .KT_col_STATUS {width:140px; overflow:hidden;}
  .KT_col_L2R_FULLNAME_NORMAL {width:140px; overflow:hidden;}
  .KT_col_HOMEPHONE {width:140px; overflow:hidden;}
  .KT_col_EMAIL {width:140px; overflow:hidden;}
</style>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
  <div class="KT_tng" id="listrsDelegation1">
    <h1> Vw_delegations
      <?php
  $nav_listrsDelegation1->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
    </h1>
    <div class="KT_tnglist">
      <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
        <div class="KT_options"> <a href="<?php echo $nav_listrsDelegation1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
              <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listrsDelegation1'] == 1) {
?>
                <?php echo $_SESSION['default_max_rows_nav_listrsDelegation1']; ?>
                <?php 
  // else Conditional region1
  } else { ?>
                <?php echo NXT_getResource("all"); ?>
                <?php } 
  // endif Conditional region1
?>
              <?php echo NXT_getResource("records"); ?></a> &nbsp;
          &nbsp;
                <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listrsDelegation1'] == 1) {
?>
                  <a href="<?php echo $tfi_listrsDelegation1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                  <?php 
  // else Conditional region2
  } else { ?>
                  <a href="<?php echo $tfi_listrsDelegation1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                  <?php } 
  // endif Conditional region2
?>
        </div>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <thead>
            <tr class="KT_row_order">
              <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>
              </th>
              <th id="delegate_type" class="KT_sorter KT_col_delegate_type <?php echo $tso_listrsDelegation1->getSortIcon('delegate_type'); ?>"> <a href="<?php echo $tso_listrsDelegation1->getSortLink('delegate_type'); ?>">TYPE</a> </th>
              <th id="STATUS" class="KT_sorter KT_col_STATUS <?php echo $tso_listrsDelegation1->getSortIcon('STATUS'); ?>"> <a href="<?php echo $tso_listrsDelegation1->getSortLink('STATUS'); ?>">STATUS</a> </th>
              <th id="L2R_FULLNAME_NORMAL" class="KT_sorter KT_col_L2R_FULLNAME_NORMAL <?php echo $tso_listrsDelegation1->getSortIcon('L2R_FULLNAME_NORMAL'); ?>"> <a href="<?php echo $tso_listrsDelegation1->getSortLink('L2R_FULLNAME_NORMAL'); ?>">L2R_FULLNAME_NORMAL</a> </th>
              <th id="HOMEPHONE" class="KT_sorter KT_col_HOMEPHONE <?php echo $tso_listrsDelegation1->getSortIcon('HOMEPHONE'); ?>"> <a href="<?php echo $tso_listrsDelegation1->getSortLink('HOMEPHONE'); ?>">HOMEPHONE</a> </th>
              <th id="EMAIL" class="KT_sorter KT_col_EMAIL <?php echo $tso_listrsDelegation1->getSortIcon('EMAIL'); ?>"> <a href="<?php echo $tso_listrsDelegation1->getSortLink('EMAIL'); ?>">EMAIL</a> </th>
              <th>&nbsp;</th>
            </tr>
            <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listrsDelegation1'] == 1) {
?>
              <tr class="KT_row_filter">
                <td>&nbsp;</td>
                <td><input type="text" name="tfi_listrsDelegation1_delegate_type" id="tfi_listrsDelegation1_delegate_type" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsDelegation1_delegate_type']); ?>" size="10" maxlength="3" /></td>
                <td><input type="text" name="tfi_listrsDelegation1_STATUS" id="tfi_listrsDelegation1_STATUS" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsDelegation1_STATUS']); ?>" size="20" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsDelegation1_L2R_FULLNAME_NORMAL" id="tfi_listrsDelegation1_L2R_FULLNAME_NORMAL" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsDelegation1_L2R_FULLNAME_NORMAL']); ?>" size="20" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsDelegation1_HOMEPHONE" id="tfi_listrsDelegation1_HOMEPHONE" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsDelegation1_HOMEPHONE']); ?>" size="20" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsDelegation1_EMAIL" id="tfi_listrsDelegation1_EMAIL" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsDelegation1_EMAIL']); ?>" size="20" maxlength="20" /></td>
                <td><input type="submit" name="tfi_listrsDelegation1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
              </tr>
              <?php } 
  // endif Conditional region3
?>
          </thead>
          <tbody>
            <?php if ($totalRows_rsDelegation == 0) { // Show if recordset empty ?>
              <tr>
                <td colspan="7"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
              </tr>
              <?php } // Show if recordset empty ?>
            <?php if ($totalRows_rsDelegation > 0) { // Show if recordset not empty ?>
              <?php do { ?>
                <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                  <td><input type="checkbox" name="kt_pk_vw_delegations" class="id_checkbox" value="<?php echo $row_rsDelegation['delegate_id']; ?>" />
                      <input type="hidden" name="delegate_id" class="id_field" value="<?php echo $row_rsDelegation['delegate_id']; ?>" />
                  </td>
                  <td><div class="KT_col_delegate_type"><?php echo KT_FormatForList($row_rsDelegation['delegate_type'], 10); ?></div></td>
                  <td><div class="KT_col_STATUS"><?php echo KT_FormatForList($row_rsDelegation['STATUS'], 20); ?></div></td>
                  <td><div class="KT_col_L2R_FULLNAME_NORMAL"><?php echo KT_FormatForList($row_rsDelegation['L2R_FULLNAME_NORMAL'], 20); ?></div></td>
                  <td><div class="KT_col_HOMEPHONE"><?php echo KT_FormatForList($row_rsDelegation['HOMEPHONE'], 20); ?></div></td>
                  <td><div class="KT_col_EMAIL"><?php echo KT_FormatForList($row_rsDelegation['EMAIL'], 20); ?></div></td>
                  <td><a class="KT_edit_link" href="home/regional-delegation-management.php?delegate_id=<?php echo $row_rsDelegation['delegate_id']; ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
                </tr>
                <?php } while ($row_rsDelegation = mysql_fetch_assoc($rsDelegation)); ?>
              <?php } // Show if recordset not empty ?>
          </tbody>
        </table>
        <div class="KT_bottomnav">
          <div>
            <?php
            $nav_listrsDelegation1->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
          </div>
        </div>
        <div class="KT_bottombuttons">
          <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
          <span>&nbsp;</span>
          <select name="no_new" id="no_new">
            <option value="1">1</option>
            <option value="3">3</option>
            <option value="6">6</option>
          </select>
          <a class="KT_additem_op_link" href="home/regional-delegation-management.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsDelegation);

mysql_free_result($rsDelegation);
?>
