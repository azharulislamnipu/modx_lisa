<?php if ( have_posts() ) :
	global $_wp_additional_image_sizes;

	// Get the width and height of the featured image.
	// This will be used to determine if a fallback image should be used instead
	$isPostThumbnailSize = isset( $_wp_additional_image_sizes[ 'post-thumnbail' ] );
	$featuredWidth = $isPostThumbnailSize ? $_wp_additional_image_sizes[ 'post-thumnbail' ][ 'width' ] : 630;
	$featuredHeight = $isPostThumbnailSize ? $_wp_additional_image_sizes[ 'post-thumnbail' ][ 'height' ] : 180;

	// Display text and thumbnail posts
	$featured = '';
	$featuredMax = 3;
	$featuredCount = 0;
	$featuredColor = array( 'blue', 'green', 'yellow' );
	$featuredColorIdx = 0;

	$thumbnail = '';
	$thumbnailMax = 2;
	$thumbnailCount = 0;

	$text = '';

	while (have_posts()) : the_post();

		// Found a main featured image - capture it
		if (has_post_thumbnail() && $featuredCount < $featuredMax) :
			$featuredCount++;
			ob_start(); ?>

			<div class="featured">
				<a href="<?php the_permalink(); ?>" title="<?php _e( 'View post', 'archon-network' ); ?>" class="link"></a>
				<div class="text">
					<h2><?php the_title(); ?></h2>
					<p><?php the_excerpt(); ?></p>
					<!--span class="link"><?php _e( 'View', 'archon-network' ); ?></span-->
				</div>
				<?php if ($featuredCount === 1): ?><span class="corner"></span><?php endif; ?>

				<?php // Determine if the featured image is big enough or if a fallback should be used
					$image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' );
					$width = $image_data[ 1 ];
					$height = $image_data[ 2 ];
					if ( $width < $featuredWidth || $height < $featuredHeight ) {
						$color = $featuredColorIdx < count( $featuredColor ) ? $featuredColor[ $featuredColorIdx++ ] : $featuredColor[ ( $featuredColorIdx = 0 ) ]; ?>
						<img src="<?php echo get_stylesheet_directory_uri() ?>/images/bg/featured-image-<?php echo $color ?>.png" alt="Featured image">
					<?php } else {
						the_post_thumbnail();
					}

				?>
			</div><?php

			$featured .= ob_get_contents();
			ob_end_clean();

		// Output text only and thumbnail posts
		else:

			// Are we still looking for thumbnails?
			$img = false;
			if ($thumbnailCount < $thumbnailMax) {

				// Does the post have a featured image set?
				if (has_post_thumbnail()) {
					$img = get_the_post_thumbnail( $post->ID, 'medium' );

				// Look for any attachments on the post
				} else {
					$attachments = get_children( array('post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image') );
					if (!empty($attachments)) {
						$src = wp_get_attachment_image_src(array_shift($attachments)->ID, 'medium');
						$img = '<img src="' . $src[0] . '" />';
					}
				}
			}

			// Did we find a thumbnail?
			if ($img) {
				++$thumbnailCount;
				ob_start(); ?>

				<div class="thumbnail">
					<a href="<?php the_permalink(); ?>" title="<?php _e( 'View post', 'archon-network' ); ?>">
						<span class="title"><?php the_title(); ?></span>
						<?php echo $img ?>
					</a>
				</div>

				<?php $thumbnail .= ob_get_contents();
				ob_end_clean();

			} else {

				ob_start(); ?>

				<li>
					<a href="<?php the_permalink(); ?>">
						<strong><?php the_title(); ?></strong>
						<span class="exceprt"><?php the_excerpt(); ?></span>
					</a>
				</li>

				<?php $text .= ob_get_contents();
				ob_end_clean();
			}

		?>


		<?php endif; ?>
	<?php endwhile;

	$isText = !empty($text);
	$isThumbnails = !empty($thumbnail);

	echo $featured;

	if ($isText) : ?>
		<ul class="link-list home-text <?php echo ($isThumbnails ? 'golden-ratio' : '') ?>">
			<?php echo $text; ?>
		</ul>
	<?php endif;

	if ($isThumbnails) : ?>
		<div class="home-thumbnails">
			<?php echo $thumbnail; ?>
		</div>
		<div class="clear"></div>
	<?php endif;

	paginate(); ?>

	<div class="clear marginBottomLarge"></div> <?php


else : ?>

	<h2 class="color yellow"><?php _e( 'Nothing Found', 'archon-network' ); ?></h2>
	<div class="contentBlock">
		<p><?php _e( 'You must be logged in to see this page\'s content.', 'archon-network' ); ?></p>
	</div>

<?php endif; ?>