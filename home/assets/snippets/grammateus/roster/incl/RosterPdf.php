<?php

require_once 'Roster.php';
require_once 'AlphaPdf.php';

/**
 * Generates the PDF roster for a given Boule 
 * Send via email: http://www.fpdf.org/en/FAQ.php#q15
 */
class RosterPdf extends AlphaPdf {

    const PDF_INLINE = 'I';
    const PDF_STRING = 'S';

    private $roster = null;
    private $officers = null;
    private $logo = '';
    private $cover_sheet = '';
    private $pdf_name = '';
    private $cover_sheet_count = 0;
    private $is_first_page = false;
    private $angle = 0;
    
    public function __construct($args = array()) {        
	
        // Set default arguments
        $args = array_merge(array(
            'roster' => null, 
            'logo' => null,
            'cover_sheet' => null
        ), $args);        

        if(!isset($args['roster'])) {
            die('You must provide a populated Roster to generate a PDF. ');
        }

        parent::__construct('P','mm',array(216,279));          
        setlocale(LC_ALL, 'en_US');
	
        $this->roster = $args['roster'];
        $this->logo = $args['logo'];
        $this->cover_sheet = $args['cover_sheet'];
        $this->pdf_name = strtolower(str_replace(' ', '-', iconv('UTF-8','ASCII//TRANSLIT',$this->roster->chaptername))) . '-boule-membership-payment-report_' . $this->roster->year . '.pdf';
        
        // PDF meta
        $this->SetAuthor('Sigma Pi Phi');
        $this->SetCreator('Grammateus Roster Dashboard');
        $this->SetTitle(utf8_decode($this->roster->chaptername . ' Boulé Membership Payment Report ('.$this->roster->year.')'));
        $this->AliasNbPages();
        
        // Add the coversheet
        $this->addCoverSheet();

        // Add the Roster Summary
        $this->addBouleSummary();           
        
        // Add the Archons
        $this->AddPage();
        $idx = 0; 
        foreach($this->roster->archons as $archonDues) {
            if($archonDues->id !== -1) {
                $this->addArchonDues($archonDues, $idx++);
                if($idx === 14) {
                    $this->AddPage();
                    $idx = 0;
                }
            }
        }
        
        // Update the page numbers to skip cover pages
        $this->addPageNumbers(); 
    }
    
    
    /**
     * Updates the page number placeholder so it doesn't include the coversheet
     */    
    private function addPageNumbers() {
        
        $this->InFooter=true;
        $this->Footer();
        $this->InFooter=false;
        
        if(!empty($this->AliasNbPages)) {
            $nb = count($this->pages);
            for($n=1; $n <= $nb; $n++) {
                $this->pages[$n] = str_replace($this->AliasNbPages, ($nb - $this->cover_sheet_count), $this->pages[$n]);
            }    
        } 
        
        $this->_endpage();
        $this->_enddoc();         
    }
    
    /**
     * Adds the coversheet to the existing PDF 
     */
    private function addCoverSheet() {
        
        if($this->cover_sheet) {
 
            $pagecount = $this->setSourceFile($this->cover_sheet); 
            for ($i = 1; $i <= $pagecount; $i++) { 
                $tplidx = $this->ImportPage($i); 
                $s = $this->getTemplatesize($tplidx); 
                $this->AddPage($s['w'] > $s['h'] ? 'L' : 'P', array($s['w'], $s['h'])); 
                $this->useTemplate($tplidx); 
                
                // Sigma Pi Phi logo
                // $this->Image($this->logo, 12.5, 12, 36);                
                
                // Add the totals
                $this->addCoverSheetTotals();                
                $this->cover_sheet_count++;
            }                      
        }        
    }
    
    private function addCoverSheetTotals() {
        
        // Get the coversheet counts
        $count_active = isset($this->roster->status_count['Active']) ? $this->roster->status_count['Active']['requested'] : 0;
        $count_inductee = isset($this->roster->status_count['Inductee']) ? $this->roster->status_count['Inductee']['requested'] : 0;
        $count_delinquent = isset($this->roster->status_count['Delinquent']) ? $this->roster->status_count['Delinquent']['requested'] : 0;
        $count_emeritus = isset($this->roster->status_count['Emeritus']) ? $this->roster->status_count['Emeritus']['requested'] : 0;
        $count_voluntary_inactive = isset($this->roster->status_count['Voluntary Inactive']) ? $this->roster->status_count['Voluntary Inactive']['requested'] : 0;
        $count_dropped = isset($this->roster->status_count['Dropped']) ? $this->roster->status_count['Dropped']['requested'] : 0;
        $count_involuntary_inactive = isset($this->roster->status_count['Involuntary Inactive']) ? $this->roster->status_count['Involuntary Inactive']['requested'] : 0;;
        
        // Total count
        $count_total = $count_active + $count_inductee + $count_delinquent + $count_emeritus + $count_voluntary_inactive + $count_dropped + $count_involuntary_inactive;
       
        // Total payment count
        $count_payments_total = 0;
        $total_payment = 0;
        foreach($this->roster->archons as $archon) {            
            if($archon->dues_amount > 0 || $archon->dues_amount_additional > 0) { 
                $count_payments_total++;
                $total_payment += ($archon->dues_amount + $archon->dues_amount_additional);
            } 
        }        
        
        // chaptername
        $this->SetFont('Times','B',22);
        $this->SetXY(13, 74.5);
        $this->Cell(190, 6, utf8_decode($this->roster->chaptername . ' Boulé'), 0, 0, 'C');        
        
        // First column
        $this->SetFont('Times','',15);
        $this->SetXY(88, 89.5);
        $this->Cell(15, 15, $count_active + $count_inductee, 0, 0, 'C');
        
        $this->SetXY(88, 102.5);
        $this->Cell(15, 15, $count_delinquent, 0, 0, 'C');   
        
        $this->SetXY(88, 120);
        $this->Cell(15, 15, $count_voluntary_inactive, 0, 0, 'C');
        
        $this->SetXY(88, 143);
        $this->Cell(15, 15, $count_involuntary_inactive, 0, 0, 'C');  
        
        $this->SetXY(88, 162);
        $this->Cell(15, 15, $count_dropped, 0, 0, 'C');  
        
        $this->SetXY(88, 175);
        $this->Cell(15, 15, $count_emeritus, 0, 0, 'C');   
        
        $this->SetFont('Times','B',15);
        $this->SetXY(88, 184);
        $this->Cell(15, 15, $count_total, 0, 0, 'C');   		
        $this->SetXY(88, 202.5);
        $this->Cell(15, 15, $count_payments_total, 0, 0, 'C');  
        
        $this->SetFont('Times','B',12);
        $this->SetXY(85.5, 217.5);
        $this->Cell(20, 15, '$'.number_format($total_payment), 0, 0, 'C');         

        // Officers
        if($this->roster->officers){
            $this->y = 99;		
            foreach($this->roster->officers as $officer) {
                    $this->addOfficer($officer);
            }
        }
    }
    
    /**
    * Adds an officer to the coversheet
    */
    private function addOfficer($officer) {

        $name = $officer ? ($officer->first_name . ' ' . $officer->last_name . ' ' . $officer->suffix) : ''; 
        $email = $officer ? $officer->email_home : '';
        
        $this->SetX(147.3);		
	$this->SetFont('Times','B',9);
        $this->MultiCell(53, 5, $name); 
		
	$this->SetX(147);
	$this->SetFont('Times','',9);		
        $this->MultiCell(53, 5, $email); 		
		
	$this->y += 3.7;
    }
	
    /**
    * Adds the boule summary to the last page
    */
    private function addBouleSummary() {
        
        $lineHeight = 6;
        $this->is_first_page = true;
        $this->AddPage();
        $this->Ln($lineHeight);

        // Add the header		
        $this->SetFillColor(240);
        $this->Rect($this->x, $this->y, 191, $lineHeight, 'F');
        $this->SetFont('Arial','B',8);
        $this->Cell(50, $lineHeight, 'Status Requested');
        $this->Cell(50, $lineHeight, 'Archons', 0, 0, 'R');
        $this->Cell(90, $lineHeight, 'Amount', 0, 0, 'R');		

        // Add the status rows
        $this->SetFont('Arial','',8);
        $total_count = 0;
        $total_amount = 0;
        foreach($this->roster->status_count as $status => $data) {
            if($data['requested'] > 0) {
                $total_count += $data['requested'];
                $total_amount += $data['dues'];

                $this->Ln($lineHeight);
                $this->Cell(50, $lineHeight, $status);
                $this->Cell(50, $lineHeight, number_format($data['requested'],0), 0, 0, 'R');
                $this->Cell(90, $lineHeight, '$' . number_format($data['dues'], 2), 0, 0, 'R');				
            }
        }

        // Add the footer
        $this->Ln($lineHeight);		
        $this->Rect($this->x, $this->y, 191, $lineHeight, 'F');
        $this->SetFont('Arial','B',8);
        $this->Cell(50, $lineHeight, 'Grand Total');
        $this->Cell(50, $lineHeight, number_format($total_count,0), 0, 0, 'R');
        $this->Cell(90, $lineHeight, '$' . number_format($total_amount,2), 0, 0, 'R');	
        
        $this->is_first_page = false;
    }

    /*
    * Adds an Archon row to the PDF
    */
    private function addArchonDues($dues, $idx) {

            // Add the zebra striping		
            if($idx % 2 === 0) {		
                $this->SetFillColor(240);		
                $this->Rect($this->x - 1, $this->y, 191, 14, 'F');
            }

            // Save the current row's top position
            $rowTop = $this->y + 1;

            // Add the Archon's info
            $this->SetY($rowTop);
            $this->SetFont('Arial','B',7);
            $this->MultiCell(80, 4, $dues->archon->fullname);

            $this->SetFont('Arial','',7);
            $this->MultiCell(80, 4, $dues->archon->address1 . ', ' . $dues->archon->city . ', ' . $dues->archon->state . ', ' . $dues->archon->zip);
            $this->MultiCell(80, 4, 'Phone: ' . $dues->archon->phone_home . '   Email: ' . $dues->archon->email_home);

            $this->SetXY($this->x + 82, $rowTop);
            $this->MultiCell(20, 4, $dues->archon->status);

            $this->SetXY($this->x + 105, $rowTop);
            $this->MultiCell(20, 4, $dues->status_requested);	

            $this->SetXY($this->x + 132, $rowTop);
            $this->MultiCell(20, 4, '$'.number_format($dues->dues_amount, 0));	

            $this->SetTextColor(100);
            $this->SetXY($this->x + 132, $rowTop + 4);
            $this->MultiCell(56, 4, $this->truncate(str_replace('\\', '', $dues->comment), 85));	            
            $this->SetTextColor(0);
            
            $this->SetXY($this->x + 150, $rowTop);
            $this->MultiCell(20, 4, '$'.number_format($dues->dues_amount_additional, 0));	

            $this->SetXY($this->x + 172, $rowTop);
            $this->MultiCell(20, 4, '$'.number_format($dues->dues_amount + $dues->dues_amount_additional, 0));	

            
            
            // Increment the Y position for the next row
            $this->y += 11; 
    } 	

    private function truncate($str, $chars){
        if(strlen($str) + 3 > $chars) {
            $str = substr($str,0,$chars).'...';
        }
        return $str;        
    }
    
    /**
     * Adds the PDF header 
     */
    public function Header() {
        
        if($this->PageNo() > 1) {
        
            $this->SetFont('Arial','B',8);
            if($this->is_first_page) {
                    $this->Cell(82, 5, strtoupper(utf8_decode($this->roster->chaptername . ' BOULÉ TOTALS'))); 

            } else {

                    // Table header
                    $this->Cell(82, 5,'ARCHON'); 
                    $this->MultiCell(20, 5, 'CURRENT STATUS');        
                    $this->SetXY(115, 10);
                    $this->MultiCell(20, 5, 'REQUESTED STATUS');        
                    $this->SetXY(142, 10);
                    $this->MultiCell(10, 5, $this->roster->year . ' DUES');        
                    $this->SetXY(160, 10);
                    $this->MultiCell(20, 5, 'ADDL DUES/FEES');        
                    $this->SetXY(182, 10);
                    $this->MultiCell(20, 5, 'TOTAL SUBMITTED');

            }

            // Line break
            $this->Ln(4);
        }
    }

    /**
     * Adds the PDF footer 
     */
    public function Footer() { 
        
        $this->doWatermark();
        if($this->PageNo() > 1) {
            
            // Page count and Boule name
            $this->SetY(-20);
            $this->SetFont('Arial','B',8);
            $this->MultiCell(0,5, utf8_decode($this->roster->chaptername . ' Boulé Membership Payment Report ' .$this->roster->year . ' (' . $this->getStatus() . ')'));
            $this->SetFont('Arial','',8);
            $this->MultiCell(0,5,'Page '.($this->PageNo() - 1).' of {nb}');

            // Sigma Pi Phi logo
            $this->Image($this->logo, 178, 250, 25);
        
        }
    }  
	
	/**
	 * Returns the status string for a given Roster
	 */
	private function getStatus() {
		if($this->roster->is_finalized) {
			return 'Submitted';
		} else if($this->roster->is_submittable) {
			return 'Completed';
		} else {
			return 'Draft';
		}
	}
	
    /**
     * Adds a watermark to the generated PDF 
     */
    private function doWatermark() { 
	$watermark = $this->roster->is_finalized ? null : 'DRAFT';
        if($watermark !== null) {
            
            // Save the curent X/Y
            $x = $this->GetX();
            $y = $this->GetY();
            
            // Print the watermark
            $this->SetFont('Arial', 'B', 180);
            $this->SetTextColor(0);
            $this->SetXY(35, 220);            
            $this->Rotate(45);
			$this->SetAlpha(0.1);
            $this->Cell(0, 0, $watermark);
            
            // Reset everything for normal output
            $this->Rotate(0);
            $this->SetFont('Arial', '', 8);
            $this->SetTextColor(0);  
            $this->SetXY($x, $y);
            $this->SetAlpha(1);
            
        }        
    }
    
    /**
     * Rotates the PDF output by a given angle
     */
    private function Rotate($angle, $x = -1, $y = -1) { 

        if($x == -1) {
            $x=$this->x; 
        }
        if($y == -1) {
            $y = $this->y; 
        }
        if($this->angle != 0) {
            $this->_out('Q'); 
        }    
        
        $this->angle=$angle;         
        if($angle!=0) { 
            $angle *= M_PI/180; 
            $c = cos($angle); 
            $s = sin($angle); 
            $cx = $x*$this->k; 
            $cy = ($this->h-$y)*$this->k;              
            $this->_out(sprintf('q %.5f %.5f %.5f %.5f %.2f %.2f cm 1 0 0 1 %.2f %.2f cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy)); 
        } 
    }     
    
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    } 
}

?>