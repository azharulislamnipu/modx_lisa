<?php
$response = array();
$response['type'] = 'error';

$db = get_db();
$db->where ('ARCHOUSAID', $archousa_id);
$db->where ('DEGREEID', $educ_id);
if ($db->delete ('spp_educ')){
    $response['type'] = 'success';
    SPP_ARCHON_DB::update_lastupdate_date($archousa_id, 'ARCHOUSAID', 'spp_archousa');
} else{
    $response['type'] = 'error';
//  $response['html'] = $db->getLastError();
    $response['html'] = '<p>Error occurred! Please try again.</p>';
}

die(json_encode($response));