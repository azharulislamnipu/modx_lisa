<?php require_once('../Connections/limesurvey.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_limesurvey = new KT_connection($limesurvey, $database_limesurvey);

// Start trigger
$masterValidation = new tNG_FormValidation();
$tNGs->prepareValidation($masterValidation);
// End trigger

// Start trigger
$detailValidation = new tNG_FormValidation();
$tNGs->prepareValidation($detailValidation);
// End trigger

//start Trigger_LinkTransactions1 trigger
//remove this line if you want to edit the code by hand 
function Trigger_LinkTransactions1(&$tNG) {
	global $upd_lime_survey_51728;
  $linkObj = new tNG_LinkedTrans($tNG, $upd_lime_survey_51728);
  $linkObj->setLink("token");
  return $linkObj->Execute();
}
//end Trigger_LinkTransactions1 trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsTokens = "-1";
if (isset($_SESSION['BOULECD'])) {
  $colname_rsTokens = $_SESSION['BOULECD'];
}
mysql_select_db($database_limesurvey, $limesurvey);
$query_rsTokens = sprintf("SELECT lime_tokens_51728.tid AS TOKENID,   lime_survey_51728.id AS SURVEYID,   lime_tokens_51728.token,   IF(lime_survey_51728.submitdate, 'C',IF(lime_survey_51728.startdate,'I','N')) AS SURVEY_STATUS,   lime_tokens_51728.completed AS SURVEY_STATUS2,   lime_survey_51728.startdate AS DATE_STARTED,   lime_survey_51728.submitdate AS DATE_COMPLETED,   lime_tokens_51728.attribute_1,  lime_tokens_51728.firstname,   lime_tokens_51728.lastname,   lime_tokens_51728.email FROM lime_tokens_51728   LEFT OUTER JOIN lime_survey_51728 ON (lime_tokens_51728.token = lime_survey_51728.token) WHERE attribute_1 = %s", GetSQLValueString($colname_rsTokens, "text"));
$rsTokens = mysql_query($query_rsTokens, $limesurvey) or die(mysql_error());
$row_rsTokens = mysql_fetch_assoc($rsTokens);
$totalRows_rsTokens = mysql_num_rows($rsTokens);

// Make an update transaction instance
$upd_lime_tokens_51728 = new tNG_update($conn_limesurvey);
$tNGs->addTransaction($upd_lime_tokens_51728);
// Register triggers
$upd_lime_tokens_51728->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_lime_tokens_51728->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/strategic-plan-reporting.php");
$upd_lime_tokens_51728->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $masterValidation);
$upd_lime_tokens_51728->registerTrigger("AFTER", "Trigger_LinkTransactions1", 98);
$upd_lime_tokens_51728->registerTrigger("ERROR", "Trigger_LinkTransactions1", 98);
// Add columns
$upd_lime_tokens_51728->setTable("lime_tokens_51728");
$upd_lime_tokens_51728->addColumn("completed", "STRING_TYPE", "POST", "completed");
$upd_lime_tokens_51728->addColumn("token", "STRING_TYPE", "CURRVAL", "token");
$upd_lime_tokens_51728->setPrimaryKey("token", "STRING_TYPE", "VALUE", "[!strategicToken!]");

// Make an update transaction instance
$upd_lime_survey_51728 = new tNG_update($conn_limesurvey);
$tNGs->addTransaction($upd_lime_survey_51728);
// Register triggers
$upd_lime_survey_51728->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_lime_survey_51728->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/strategic-plan-reporting.php");
$upd_lime_survey_51728->registerTrigger("ERROR", "Trigger_LinkTransactions1", 98);
$upd_lime_survey_51728->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $detailValidation);
// Add columns
$upd_lime_survey_51728->setTable("lime_survey_51728");
$upd_lime_survey_51728->addColumn("submitdate", "DATE_TYPE", "POST", "submitdate");
$upd_lime_survey_51728->addColumn("token", "STRING_TYPE", "CURRVAL", "token");
$upd_lime_survey_51728->setPrimaryKey("token", "STRING_TYPE", "VALUE", "[!strategicToken!]");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rslime_tokens_51728 = $tNGs->getRecordset("lime_tokens_51728");
$row_rslime_tokens_51728 = mysql_fetch_assoc($rslime_tokens_51728);
$totalRows_rslime_tokens_51728 = mysql_num_rows($rslime_tokens_51728);

// Get the transaction recordset
$rslime_survey_51728 = $tNGs->getRecordset("lime_survey_51728");
$row_rslime_survey_51728 = mysql_fetch_assoc($rslime_survey_51728);
$totalRows_rslime_survey_51728 = mysql_num_rows($rslime_survey_51728);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?><!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>

  <table cellspacing="4" cellpadding="4">
    <tr>
      <td bgcolor="#B5D0E3">SURVEY STATUS</td>
      <td bgcolor="#B5D0E3">DATE STARTED</td>
      <td bgcolor="#B5D0E3">DATE COMPLETED</td>
<?php 
// Show IF Conditional region3 
if (@$row_rsTokens['SURVEY_STATUS'] == 'I') {
?>
    <td bgcolor="#B5D0E3">TOKEN</td>
   <?php } 
// endif Conditional region3
?>       
    
    
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" bgcolor="#C1C1C1"><?php
switch ($row_rsTokens['SURVEY_STATUS']) {
    case 'C':
        echo "<strong>You have completed your report</strong>.<br /> Click the  Reset Report button to the right<br /> if you would like to edit or update your report.";
        break;
    case 'N':
        echo "<strong>You have not started your report</strong>. <br />Click the link to the right<br /> in order to start your report.";
        break;
	case 'I':	
	    echo "<strong>Your report is incomplete</strong>.<br />Click the link to the right<br /> in order to finish your report.";
		break;
}    
?>      </td>
      <td valign="top" bgcolor="#C1C1C1"><?php echo KT_formatDate($row_rsTokens['DATE_STARTED']); ?></td>
      <td valign="top" bgcolor="#C1C1C1"><?php echo KT_formatDate($row_rsTokens['DATE_COMPLETED']); ?></td>

<?php 
// Show IF Conditional region4
if (@$row_rsTokens['SURVEY_STATUS'] == 'I') {
?>
      <td valign="top" bgcolor="#C1C1C1"><p><strong>[!strategicToken!]</strong></p>
      <p>If you are asked to enter<br /> 
      a token 
      use the value above.</p>      </td>
   <?php } 
// endif Conditional region4
?>       
      <td valign="top">      <?php
	echo $tNGs->getErrorMsg();
?>
        <?php 
// Show IF Conditional region2 
if (@$row_rsTokens['SURVEY_STATUS'] != 'C') {
?>
          <p><a href="https://www.sigmapiphi.org/reports/index.php?sid=51728&amp;lang=en&amp;token=[!strategicToken!]" target="_blank">Start/Update Your Report</a></p>
          <?php } 
// endif Conditional region2
?>
        <?php 
// Show IF Conditional region1 
if (@$row_rsTokens['SURVEY_STATUS'] == 'C') {
?>
          <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
            <input type="submit" name="KT_Update1" id="KT_Update1" value="Reset Report" />
            <input type="hidden" name="completed" id="completed" value="<?php echo KT_escapeAttribute('N'); ?>" />
            <input type="hidden" name="submitdate" id="submitdate" value="<?php $a = NULL; echo $a; ?>" />
          </form>
          <?php } 
// endif Conditional region1
?></td>
    </tr>
</table>
  <h2>The Archon listed below 
  
  <?php 
// Show IF Conditional region5
if (@$row_rsTokens['SURVEY_STATUS'] == 'C') {
?>
  received
   <?php } 
// endif Conditional region5
?>       
  
  <?php 
// Show IF Conditional region6
if (@$row_rsTokens['SURVEY_STATUS'] != 'C') {
?>
   will receive
   <?php } 
// endif Conditional region6
?>       
  
    a confirmation of the completed report. 

  <?php 
// Show IF Conditional region7
if (@$row_rsTokens['SURVEY_STATUS'] != 'C') {
?>

    Only one email address may be used.
   <?php } 
// endif Conditional region7
?>
  </h2>
  <table cellspacing="4" cellpadding="4">
<tr>
      <td colspan="2" valign="top" bgcolor="#B5D0E3">REPORT RECIPIENT</td>
      <td valign="top" bgcolor="#B5D0E3">EMAIL</td>
    </tr>
    <tr bgcolor="#C1C1C1">
      <td colspan="2" valign="top"><?php echo $row_rsTokens['firstname']; ?> <?php echo $row_rsTokens['lastname']; ?></td>
      <td valign="top"><?php echo $row_rsTokens['email']; ?></td>
    </tr>
    </table>          
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsTokens);
?>
