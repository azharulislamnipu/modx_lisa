[!userProfile? &type=`Archon`!]

<!-- #MAIN COLUMN -->



[!LDataDisplay? &table=`spp_archons` &display=`1` &tpl=`tpl_archon_prof_image` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]

<!-- [ !LDataDisplay? &table=`spp_officers` &display=`all` &tpl=`tpl_archon_prof_offices` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1` &sortBy=`START_DATE` &sortDir=`ASC`!]-->

[!LDataDisplay? &table=`spp_archousa` &display=`1` &tpl=`tpl_archon_prof_personal_info` &filter=`CUSTOMERCD,[!getPlaceholder? &name=`customercd`!],1`!]


</div></div></div>
<!-- #END MAIN COLUMN -->




<!-- #BEGIN SECOND COLUMN -->
<div class="span-4"><div class="padding">
        <div class="width padding">



            <!-- #END SECOND COLUMN -->


            <!-- #BEGIN SECOND COLUMN -->

            <h3>Career</h3>

            <div id="profile_career_box">
                [!LDataDisplay? &table=`vw_all_jobs_web_elig` &display=`all` &sortBy=`OCCUPATIONDESC` &sortDir=`ASC` &noResult=`ac_occup_no_result` &tpl=`tpl_archon_occup_info_list` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]
            </div>

            <form name="career_add" action="/services/archon-upd/profile_career_add.php" method="post">
                <input type="hidden" name="CUSTOMERID" value="[!getPlaceholder? &name=`customercd`!]">
                <input type="hidden" name="UPDATEDBY" value="[!svVars? &var=`webShortname`!]">
                <input type="hidden" name="RETURN" value="">
            </form>
            <p class="font-small"><a href="#" onclick="document.career_add.submit(); return false;" title="Add Employment" class="button font-small">Add Employment</a></p>


            <h3>Education</h3>

            [!LDataDisplay? &table=`spp_educ` &sortBy=`DEGREEYEAR` &sortDir=`ASC` &tpl=`tpl_archon_prof_educ_list` &noResult=`ac_educ_no_result` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]

            <form name="education" action="/services/archon-upd/profile_education_add.php" method="post">
                <input type="hidden" name="CUSTOMERID" value="[!getPlaceholder? &name=`customercd`!]">
                <input type="hidden" name="UPDATEDBY" value="[[svVars? &var=`webShortname`]]">
                <input type="hidden" name="RETURN" value="">
            </form>
            <p class="font-small"><a href="#" onclick="document.education.submit(); return false;" title="Add Education" class="button">Add Education</a></p>

        </div></div></div>

<!-- #begin THIRD COLUMN -->
<div class="span-4"><div class="padding">
        <div class="width padding" id="archon_bio">

            [!LDataDisplay? &table=`spp_archons` &display=`1` &tpl=`tpl_archon_prof_bio` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]

            <!-- #END THIRD COLUMN -->
        </div></div>