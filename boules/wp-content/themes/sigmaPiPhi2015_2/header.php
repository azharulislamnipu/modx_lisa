<!DOCTYPE html>

<!--[if lt IE 7]>      <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->

    <head profile="http://gmpg.org/xfn/11">

        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

        <title><?php wp_title() ?></title>

        <!-- <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_incl/css/mobile.css" type="text/css" media="handheld, only screen and (max-device-width:480px)" /> -->
			
		    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto+Slab%7CRaleway:400,700">
		    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>" type="text/css" media="all" />
			
			
        <!-- <link rel="alternate" type="application/rss+xml" title="<?php //bloginfo('name'); ?> <?php //_e( 'Blog Posts RSS Feed', 'archon-network' ) ?>" href="<?php //bloginfo('rss2_url'); ?>" />
        <link rel="alternate" type="application/atom+xml" title="<?php //bloginfo('name'); ?> <?php //_e( 'Blog Posts Atom Feed', 'archon-network' ) ?>" href="<?php //bloginfo('atom_url'); ?>" />
        <link rel="pingback" href="<?php //bloginfo('pingback_url'); ?>" /> -->
        <meta name="viewport" content="width=device-width,initial-scale=1" />
			<script src="<?php bloginfo('template_directory') ?>/assets/js/vendor/modernizr-2.8.1.min.js"></script>

        <?php wp_head(); ?>
        <?php add_thickbox(); ?>

    </head>

    <body <?php body_class( 'colocated' ) ?> >
		
	    <ul class="visuallyhidden focusable">
	        <li><a href="#nav">Skip to navigation</a></li>
	        <li><a href="#main">Skip to main content</a></li>
	    </ul>


	<header class="width">
		<!-- <div class="topbar">


			<div class="width relative">

				<ul class="jetmenu">
					<li><a href="#">Dashboard</a>

						<ul class="dropdown">
							<li><a href="#">Website Design</a></li>
							<li><a href="#">Hosting</a></li>
							<li><a href="#">Design</a>
								<ul class="dropdown">
									<li><a href="#">Graphics</a></li>
									<li><a href="#">Vectors</a></li>
									<li><a href="#">Photoshop</a>
										<ul class="dropdown">
											<li><a href="#">Photo editing</a></li>
											<li><a href="#">Business cards</a></li>

											<li><a href="#">Websites</a></li>
											<li><a href="#">Illustrations</a></li>
										</ul>
									</li>
									<li><a href="#">Fonts</a></li>
								</ul>
							</li>
							<li><a href="#">Consulting</a></li>




						</ul>
					</li>
				</ul>
				<ul class="jetmenu float-right">
					<li><a href="#">The Grand Boulé</a></li>
					<li><a href="#">Member Boulés</a>

						<ul class="dropdown">
							<li><a href="#">Photo editing</a></li>
							<li><a href="#">Business cards</a></li>
							<li><a href="#">Websites</a></li>
							<li><a href="#">Illustrations</a></li>
						</ul>
					</li>
					<li><a href="#">Archon Network</a></li>
				</ul> -->
			<!-- </div> --><!--#width relative -->
		<!-- </div> --><!--#topbar  -->
		
			<div class="row grid-12 padding-left-40">
		<div class="span-3">
			<div class="logo">
				<a href="<?php echo network_site_url(); ?>"><img src="<?php bloginfo('template_directory') ?>/assets/images/header-logo-combined.png" alt="Sigma Pi Phi Fraternity"></a>
				<!-- <a href="#"><img src="assets/images/header-logo-gamma-alpha.png" alt="Gamma Alpha Boulé – Tuscon, AZ – May 12, 1984" class="boule"></a> -->
				
				 
		
				
				<?php
				// Check that Archon plugin functions have been loaded
					if(function_exists('is_multisite_primary') && function_exists('is_regional_site')){ ?>
	            
						<?php if( !is_multisite_primary() && !is_regional_site() ) : ?>
				<?php if ("logo_url") { ?>
				<a href="<?php echo site_url(); ?>"><img src="<?php echo get_option('logo_url'); ?>" alt="<?php bloginfo('name') ?>" class="boule"/></a>
				<?php } ?>
					<em><?php bloginfo( 'description' )?></em>
					</div><!--#logo -->
					</div><!-- #span-3-->
					<div class="span-9 floatright">
					<h2><?php bloginfo('name') ?> Boulé </h2>
					<?php if ("location") { ?>
					<p><?php echo get_option('location'); ?></p>
					<?php } ?>
					
					<?php elseif (is_regional_site()) : ?>
						<em><?php bloginfo( 'description' )?></em>
						</div><!--#logo -->
						</div><!-- #span-3-->
						<div class="span-9 floatright">
								<h2><?php bloginfo('name') ?> Region</h2>
								<?php if ("location") { ?>
								<p><?php echo get_option('location'); ?></p>
								<?php } ?>
	                <!-- <div id="extraordinary"></div> -->
				<?php else : ?>
					<em><?php bloginfo( 'description' )?></em>
					</div><!--#logo -->
					</div><!-- #span-3-->
					<div class="span-9 floatright">
							<h2><?php bloginfo('name') ?></h2>
						<?php if ("location") { ?>
						<p><?php echo get_option('location'); ?></p>
						<?php } ?>
				<?php endif; ?>
				<?php } ?>
			</div><!-- #span-9-->
		
			

			<!-- <ul class="account">
				<li>Welcome <a href="#" class="profile">Archon Buress</a></li>
				<li><a href="#" class="logout">Logout</a></li>
			</ul> -->
			
            <!-- <ul id="nav">
                <li<?php //if (is_front_page() || is_home() ) : ?> class="selected"<?php //endif; ?>>
                    <a href="<?php //bloginfo('url') ?>" title="<?php _e( 'Welcome', 'archon-network' ) ?>"><?php //_e( 'Welcome', 'archon-network' ) ?></a>
                </li> -->

			<nav role="navigation">
			<!-- <?php //wp_nav_menu( array( 'theme_location' => 'primary', 'items_wrap' => '<ul class="jetmenu"><li id="item-id"><a href="#">Home</a></li>%3$s</ul>' ) ); ?> -->
		
				<?php wp_nav_menu(array(
					//'theme_location' 	=> 'primary-menu',
					'container' 		=> '',
					'items_wrap'		=> '<ul class="jetmenu"><li id="item-id"><a href="' . home_url( '/' ) . '">Home</a></li><li class="open hidemobile"><a href="#">Menu<i class="fa fa-bars"></i></a></li>%3$s</ul>',
					'walker'		=> new privacy_walker()
				)); ?>

				<?php //if(is_multisite_primary()):
					//do_action( 'sigma_nav_items' );
				//endif; ?>
</nav>


            <!-- </ul> -->
		
			<!-- <nav role="navigation">
				<h2 class="visuallyhidden">Site Menu</h2>
				<ul class="jetmenu">
					<li class="home"><a href="#">Home</a></li>
					<li><a href="#">History</a>

						<ul class="dropdown">
							<li><a href="#">Website Design</a></li>
							<li><a href="#">Hosting</a></li>
							<li><a href="#">Design</a>
								<ul class="dropdown">
									<li><a href="#">Graphics</a></li>

									<li><a href="#">Vectors</a></li>
									<li><a href="#">Photoshop</a>
										<ul class="dropdown">
											<li><a href="#">Photo editing</a></li>
											<li><a href="#">Business cards</a></li>
											<li><a href="#">Websites</a></li>
											<li><a href="#">Illustrations</a></li>
										</ul>
									</li>
									<li><a href="#">Fonts</a></li>
								</ul>
							</li>
							<li><a href="#">Consulting</a></li>


						</ul>
					</li>
					<li><a href="#">Photos</a>
						<ul class="dropdown">
							<li><a href="#">Photo editing</a></li>
							<li><a href="#">Business cards</a></li>
							<li><a href="#">Websites</a></li>
							<li><a href="#">Illustrations</a></li>




						</ul>
					</li>
					<li><a href="#">Calendar</a></li>
				</ul>
			</nav> -->
	
		
			<section>
				<!-- <h1><?php //bloginfo('name') ?>
				<?php
				//if (is_main_site()) {
				//}
				//else {
				// echo 'Boulé';
				//}
				?>
				</h1> -->
					<?php 
					//if (!is_main_site()) {
						?>
						<?php if( !is_home() || !is_front_page() ) : ?>
				
				<ul class="breadcrumbs">
					<li><a href="<?php echo get_site_url(1); ?>">Archon Network</a></li>
					<li><?php bloginfo('name') ?> 
						
					<?php 
					if (is_main_site()) { 
					}
					elseif (is_regional_site()) {
						echo 'Region';
					}
					
					else {
					 echo 'Boulé';
					}
					?>
					</li>
					<?php //}?>
					
					<?php endif; ?>

				</ul>
			</section>

				<!-- </div> --><!-- #width padding-->
		</div>		<!--#row grid-12 -->
	
			
	        <!-- <?php //do_action( 'sigma_before_header' ) ?> -->

	        <!-- <div id="header"> -->

	            <!-- <h1 id="logo"><a href="/home" title="<?php //_e( 'Home', 'archon-network' ) ?>"><?php //bloginfo('name') ?></a></h1> -->

				<!-- <?php
			//	$header_image = get_header_image();
				//if(empty($header_image)) {
					//$header_image = HEADER_IMAGE;
				//} ?>
				<img src="<?php //echo $header_image ?>" alt="Sigma Pi Phi" />

	            <?php //if(!is_multisite_primary()) : ?>
	                <h2 id="tagline"><?php //bloginfo( 'description' )?></h2>
	                <div id="extraordinary"></div>
	            <?php //endif; ?>

				<?php //do_action( 'sigma_header' ) ?> -->

	        <!-- </div> --><!-- #header -->

	        <!-- <?php //do_action( 'sigma_after_header' ) ?>
	        <?php //do_action( 'sigma_before_container' ) ?> -->
		<!-- <div id="slider"> -->
			<?php //echo do_shortcode("[URIS id=89]"); ?>
		<!-- </div> -->
	    </header>
        <?php //do_action( 'sigma_after_header' ) ?>
        <?php //do_action( 'sigma_before_container' ) ?>
		

		    <div id="main" role="main">
		        

        <div id="container">

   
