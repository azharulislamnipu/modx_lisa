/**
 * Controls the JavaScript of the theme
 */

(function($) {

	// Main menu dropdowns
	var $links = $('#nav > li').filter(function(){
			return $(this).children('.sub-menu').length !== 0;
		});

	$links.on('mouseenter', function() {
		var $link = $(this);

		$link.addClass('active');
		$link.children('.sub-menu')
			.css({opacity: 0, display: 'block'})
			.stop()
			.fadeTo(150, 1);
	}).on('mouseleave', function() {
		var $link = $(this),
			$submenu = $link.children('.sub-menu');

			$link.removeClass('active');
			$submenu
				.stop()
				.fadeTo(150, 0, function(){
					$submenu.css({display: 'none'})
				});
	});

	$(function() {
		// Fancy box
		var $fancy = $( 'a[href$=".jpg"], a[href$=".jpeg"], a[href$=".gif"], a[href$=".png"], a[href$=".JPG"], a[href$=".JPEG"], a[href$=".GIF"], a[href$=".PNG"]' );
		$fancy.attr( 'rel', 'fancybox' );
		$fancy.fancybox({
			maxHeight: '90%',
			helpers : {
				buttons : {
					skipSingle : true,
					position   : 'bottom',
					tpl        : '<div class="fancybox-buttons"><a class="btnPlay" title="Start slideshow" href="javascript:;">Start slideshow</a></div>'
				}
			},
			onPlayStart: function () {
				var $play = $('.btnPlay');
				if ($play.length) {
					$play.text('Pause slideshow').addClass('btnPlayOn');
				}
			},
			onPlayEnd: function () {
				var $play = $('.btnPlay');
				if ($play.length) {
					$play.text('Start slideshow').removeClass('btnPlayOn');
				}
			}
		});
	});

}(jQuery));
