<?php
/*
 * Name:       custom_functions.php
 * Purpose:    Addional custom functions for the Sigma Pi Phi including woocommerce
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// include woocommerce templates
//require_once('_incl/functions/actions-filters.php');

//declare woocommer support
add_action( 'after_setup_theme', 'an_declare_wc_support' );
function an_declare_wc_support() {
  add_theme_support('woocommerce');
}

add_action( 'wp_enqueue_scripts', 'an_enqueue_custom_scripts' );
function an_enqueue_custom_scripts() {		
	wp_enqueue_style( 'an_custom_styles', get_template_directory_uri() . '/assets/css/custom.css', null, null );
}

//redirect non logged-in users to login page 
add_action('template_redirect', 'an_non_loggedin_redirect');
function an_non_loggedin_redirect() {
    if ( ! is_user_logged_in() && (is_woocommerce() || is_cart() || is_checkout()) ) {
        wp_redirect( 'http://www.sigmapiphi.org/home/login.php' );
        exit;
    }
}

add_action('wp_footer', 'an_custom_footer_scripts', 99 );
function an_custom_footer_scripts(){ ?>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		//$('.yith_magnifier_thumbnail').removeAttr('rel[fancybox]');		
		//$('.yith_magnifier_thumbnail').attr('rel', '');
	});
	</script>
<?php }

//add extra user fields
add_action( 'show_user_profile', 'an_extra_user_profile_fields' );
add_action( 'edit_user_profile', 'an_extra_user_profile_fields' );

function an_extra_user_profile_fields( $user ) { ?>

	<h3>Archon User Info</h3>

	<table class="form-table">

		<tr>
			<th><label for="modxID">CUSTOMERCD</label></th>

			<td>
				<input type="text" name="CUSTOMERCD" id="CUSTOMERCD" value="<?php echo esc_attr( get_the_author_meta( 'CUSTOMERCD', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">CUSTOMERCD</span>
			</td>
		</tr>
		
	</table>
<?php }


add_action( 'personal_options_update', 'an_save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'an_save_extra_user_profile_fields' );

function an_save_extra_user_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;
	
	update_usermeta( $user_id, 'CUSTOMERCD', $_POST['CUSTOMERCD'] );
}

//add a custom link (cart) in wp_admin_bar
add_action( 'wp_before_admin_bar_render', 'an_admin_bar' );
function an_admin_bar() {
	global $wp_admin_bar;
	$wp_admin_bar->add_node(array(
		'id'    => 'an-cart',
		'title' => 'Cart',
		'href'  => 'http://dev4.sigmapiphi.org/boules/cart/',
	));
}


//add authorize.net secure seal to cart and checkout

add_action( 'woocommerce_checkout_order_review', 'an_wcchecout_secure_seal' );
add_action( 'woocommerce_proceed_to_checkout', 'an_wcchecout_secure_seal', 30 );
function an_wcchecout_secure_seal() {
    echo '<div class="aligncenter" style="text-align: center; padding-bottom: 20px;">
    <!-- (c) 2005, 2016. Authorize.Net is a registered trademark of CyberSource Corporation --> <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="c4f0fdbc-6b0b-47be-af0b-fbc7b6ca9bd4";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> <a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank">Credit Card Merchant Services</a> </div> 
    </div>';
}
