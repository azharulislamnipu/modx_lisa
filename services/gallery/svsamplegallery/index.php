<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <?php
      /* Leave this block of php code intact if customizing this page */
      error_reporting(0);
      $preferences = file_get_contents('preferences.txt');
      $preferences = unserialize($preferences);
      $title = htmlspecialchars($preferences['htmlTitle'], ENT_QUOTES, 'UTF-8');
      $galleryWidth = isset($preferences['galleryWidth']) ? "'".$preferences['galleryWidth']."'" : "'100%'";
      $galleryHeight = isset($preferences['galleryHeight']) ? "'".$preferences['galleryHeight']."'" : "'100%'";
      $useFlash = isset($preferences['useFlash']) ? strtolower($preferences['useFlash']) : 'true';
      $backgroundColor = isset($preferences['backgroundColor']) ? $preferences['backgroundColor'] : '0x222222';
      $backgroundColor = "'".ltrim(str_replace('0x', '', $backgroundColor), '#')."'";
      $flashVars = 'null';
      $params = 'null';
      $attributes = 'null';
      $useColorCorrection = isset($preferences['useColorCorrection']) ? strtolower($preferences['useColorCorrection']) : 'true';
    ?>
    <title><?php print $title; ?></title>
  </head>
  <body>
    <!--START SIMPLEVIEWER EMBED -->
    <script type="text/javascript" src="svcore/js/simpleviewer.js"></script>
    <script type="text/javascript">
      var flashvars = {};
      flashvars.galleryURL = "gallery.xml?nocache=<?php echo rand(); ?>";
      jQuery(document).ready(function () {
        SV.simpleviewer.load(
          'sv-container',
          <?php print $galleryWidth; ?>,
          <?php print $galleryHeight; ?>,
          <?php print $backgroundColor; ?>,
          <?php print $useFlash; ?>,
          <?php print $flashVars; ?>,
          <?php print $params; ?>,
          <?php print $attributes; ?>,
          <?php print $useColorCorrection; ?>
          );
      });
    </script>
    <div id="sv-container"></div>
    <!--END SIMPLEVIEWER EMBED -->
  </body>
</html>
