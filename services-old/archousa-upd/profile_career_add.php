<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("OCCUPATIONID", true, "numeric", "", "", "", "");
$formValidation->addField("COMPANYNAME", true, "text", "", "", "", "");
$formValidation->addField("COMPANYCITY", true, "text", "", "", "", "");
$formValidation->addField("COMPANYSTCD", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsJobs = "-1";
if (isset($_POST['PROFID'])) {
  $colname_rsJobs = $_POST['PROFID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsJobs = sprintf("SELECT * FROM spp_prof WHERE PROFID = %s", GetSQLValueString($colname_rsJobs, "int"));
$rsJobs = mysql_query($query_rsJobs, $sigma_modx) or die(mysql_error());
$row_rsJobs = mysql_fetch_assoc($rsJobs);
$totalRows_rsJobs = mysql_num_rows($rsJobs);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOccupType = "SELECT * FROM spp_occupation ORDER BY `DESCRIPTION` ASC";
$rsOccupType = mysql_query($query_rsOccupType, $sigma_modx) or die(mysql_error());
$row_rsOccupType = mysql_fetch_assoc($rsOccupType);
$totalRows_rsOccupType = mysql_num_rows($rsOccupType);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsTitles = "SELECT * FROM spp_skillref ORDER BY `DESCRIPTION` ASC";
$rsTitles = mysql_query($query_rsTitles, $sigma_modx) or die(mysql_error());
$row_rsTitles = mysql_fetch_assoc($rsTitles);
$totalRows_rsTitles = mysql_num_rows($rsTitles);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsState = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsState = mysql_query($query_rsState, $sigma_modx) or die(mysql_error());
$row_rsState = mysql_fetch_assoc($rsState);
$totalRows_rsState = mysql_num_rows($rsState);

// Make an insert transaction instance
$ins_spp_prof = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_prof);
// Register triggers
$ins_spp_prof->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_prof->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_prof->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-profile.php");
// Add columns
$ins_spp_prof->setTable("spp_prof");
$ins_spp_prof->addColumn("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID", "{POST.ARCHOUSAID}");
$ins_spp_prof->addColumn("OCCUPATIONID", "NUMERIC_TYPE", "POST", "OCCUPATIONID");
$ins_spp_prof->addColumn("TITLEID", "NUMERIC_TYPE", "POST", "TITLEID");
$ins_spp_prof->addColumn("COMPANYNAME", "STRING_TYPE", "POST", "COMPANYNAME");
$ins_spp_prof->addColumn("COMPANYCITY", "STRING_TYPE", "POST", "COMPANYCITY");
$ins_spp_prof->addColumn("COMPANYSTCD", "STRING_TYPE", "POST", "COMPANYSTCD");
$ins_spp_prof->addColumn("DESCRIPTION", "STRING_TYPE", "POST", "DESCRIPTION");
$ins_spp_prof->addColumn("COMPANYJOBSTART", "DATE_TYPE", "POST", "COMPANYJOBSTART");
$ins_spp_prof->addColumn("COMPANYJOBEND", "DATE_TYPE", "POST", "COMPANYJOBEND");
$ins_spp_prof->addColumn("CREATEDATE", "DATE_TYPE", "POST", "CREATEDATE", "{NOW_DT}");
$ins_spp_prof->addColumn("LASTUPDATE", "DATE_TYPE", "POST", "LASTUPDATE", "{NOW_DT}");
$ins_spp_prof->addColumn("CREATEDBY", "STRING_TYPE", "POST", "CREATEDBY", "{POST.UPDATEDBY}");
$ins_spp_prof->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY", "{POST.UPDATEDBY}");
$ins_spp_prof->setPrimaryKey("PROFID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_prof = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_prof);
// Register triggers
$upd_spp_prof->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_prof->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_prof->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-profile.php");
// Add columns
$upd_spp_prof->setTable("spp_prof");
$upd_spp_prof->addColumn("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");
$upd_spp_prof->addColumn("OCCUPATIONID", "NUMERIC_TYPE", "POST", "OCCUPATIONID");
$upd_spp_prof->addColumn("TITLEID", "NUMERIC_TYPE", "POST", "TITLEID");
$upd_spp_prof->addColumn("COMPANYNAME", "STRING_TYPE", "POST", "COMPANYNAME");
$upd_spp_prof->addColumn("COMPANYCITY", "STRING_TYPE", "POST", "COMPANYCITY");
$upd_spp_prof->addColumn("COMPANYSTCD", "STRING_TYPE", "POST", "COMPANYSTCD");
$upd_spp_prof->addColumn("DESCRIPTION", "STRING_TYPE", "POST", "DESCRIPTION");
$upd_spp_prof->addColumn("COMPANYJOBSTART", "DATE_TYPE", "POST", "COMPANYJOBSTART");
$upd_spp_prof->addColumn("COMPANYJOBEND", "DATE_TYPE", "POST", "COMPANYJOBEND");
$upd_spp_prof->addColumn("CREATEDATE", "DATE_TYPE", "POST", "CREATEDATE");
$upd_spp_prof->addColumn("LASTUPDATE", "DATE_TYPE", "POST", "{NOW_DT}");
$upd_spp_prof->addColumn("CREATEDBY", "STRING_TYPE", "POST", "CREATEDBY");
$upd_spp_prof->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "{POST.UPDATEDBY}");
$upd_spp_prof->setPrimaryKey("PROFID", "NUMERIC_TYPE", "POST", "PROFID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_prof = $tNGs->getRecordset("spp_prof");
$row_rsspp_prof = mysql_fetch_assoc($rsspp_prof);
$totalRows_rsspp_prof = mysql_num_rows($rsspp_prof);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<script>
$MXW_relPath = '../';
</script>
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/JSRecordset.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/DynamicInput.js"></script>
<?php
//begin JSRecordset
$jsObject_rsOccupType = new WDG_JsRecordset("rsOccupType");
echo $jsObject_rsOccupType->getOutput();
//end JSRecordset
?>
<?php
//begin JSRecordset
$jsObject_rsTitles = new WDG_JsRecordset("rsTitles");
echo $jsObject_rsTitles->getOutput();
//end JSRecordset
?>
<script type="text/javascript" src="../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../includes/resources/calendar.js"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->

  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">
    <h1 style="text-transform: uppercase;">
      <?php 
// Show IF Conditional region1 
	if (@$_POST['PROFID'] == "") {
?>
        <?php echo NXT_getResource("Insert_FH"); ?>
        <?php 
// else Conditional region1
} else { ?>
        <?php echo NXT_getResource("Update_FH"); ?>
        <?php } 
// endif Conditional region1
?>
      CAREER INFORMATION</h1>
    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_prof > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php } 
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <tr>
              <td class="KT_th"><label for="OCCUPATIONID_<?php echo $cnt1; ?>">OCCUPATION:</label></td>
              <td><select name="OCCUPATIONID_<?php echo $cnt1; ?>" id="OCCUPATIONID_<?php echo $cnt1; ?>" wdg:subtype="DynamicInput" wdg:type="widget" wdg:recordset="rsOccupType" wdg:displayfield="DESCRIPTION" wdg:valuefield="OCCUPATIONID" wdg:norec="50" wdg:singleclickselect="yes" wdg:defaultoptiontext="no" wdg:restrict="No" wdg:ext="php" wdg:selected="<?php echo $row_rsspp_prof['OCCUPATIONID'] ?>" wdg:fakeattribute="<?php WDG_registerRecordInsert("sigma_modx", "rsOccupType", "OCCUPATIONID", "DESCRIPTION");?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
              </select>
                <?php echo $tNGs->displayFieldError("spp_prof", "OCCUPATIONID", $cnt1); ?> </td></tr>
            <tr>
              <td class="KT_th"><label for="TITLEID_<?php echo $cnt1; ?>">TITLE:</label></td>
              <td><select name="TITLEID_<?php echo $cnt1; ?>" id="TITLEID_<?php echo $cnt1; ?>" wdg:subtype="DynamicInput" wdg:type="widget" wdg:recordset="rsTitles" wdg:displayfield="DESCRIPTION" wdg:valuefield="SKILLID" wdg:norec="50" wdg:singleclickselect="yes" wdg:defaultoptiontext="no" wdg:restrict="No" wdg:ext="php" wdg:selected="<?php echo $row_rsspp_prof['TITLEID'] ?>" wdg:fakeattribute="<?php WDG_registerRecordInsert("sigma_modx", "rsTitles", "SKILLID", "DESCRIPTION");?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
              </select>
                  <?php echo $tNGs->displayFieldError("spp_prof", "TITLEID", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMPANYNAME_<?php echo $cnt1; ?>">COMPANY NAME:</label></td>
              <td><input type="text" name="COMPANYNAME_<?php echo $cnt1; ?>" id="COMPANYNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_prof['COMPANYNAME']); ?>" size="32" maxlength="125" />
                  <?php echo $tNGs->displayFieldHint("COMPANYNAME");?> <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYNAME", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMPANYCITY_<?php echo $cnt1; ?>">COMPANY CITY:</label></td>
              <td><input type="text" name="COMPANYCITY_<?php echo $cnt1; ?>" id="COMPANYCITY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_prof['COMPANYCITY']); ?>" size="20" maxlength="20" />
                  <?php echo $tNGs->displayFieldHint("COMPANYCITY");?> <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYCITY", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMPANYSTCD_<?php echo $cnt1; ?>">COMPANY STATE:</label></td>
              <td><select name="COMPANYSTCD_<?php echo $cnt1; ?>" id="COMPANYSTCD_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                  <?php 
do {  
?>
                  <option value="<?php echo $row_rsState['st_cd']?>"<?php if (!(strcmp($row_rsState['st_cd'], $row_rsspp_prof['COMPANYSTCD']))) {echo "SELECTED";} ?>><?php echo $row_rsState['st_nm']?></option>
                  <?php
} while ($row_rsState = mysql_fetch_assoc($rsState));
  $rows = mysql_num_rows($rsState);
  if($rows > 0) {
      mysql_data_seek($rsState, 0);
	  $row_rsState = mysql_fetch_assoc($rsState);
  }
?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYSTCD", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="DESCRIPTION_<?php echo $cnt1; ?>">DESCRIPTION:</label></td>
              <td><textarea name="DESCRIPTION_<?php echo $cnt1; ?>" id="DESCRIPTION_<?php echo $cnt1; ?>" cols="50" rows="5"><?php echo KT_escapeAttribute($row_rsspp_prof['DESCRIPTION']); ?></textarea>
                  <?php echo $tNGs->displayFieldHint("DESCRIPTION");?> <?php echo $tNGs->displayFieldError("spp_prof", "DESCRIPTION", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMPANYJOBSTART_<?php echo $cnt1; ?>">COMPANY START DATE:</label></td>
              <td><input name="COMPANYJOBSTART_<?php echo $cnt1; ?>" id="COMPANYJOBSTART_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_prof['COMPANYJOBSTART']); ?>" size="10" maxlength="22" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("COMPANYJOBSTART");?> <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYJOBSTART", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMPANYJOBEND_<?php echo $cnt1; ?>">COMPANY END DATE:</label></td>
              <td><input name="COMPANYJOBEND_<?php echo $cnt1; ?>" id="COMPANYJOBEND_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_prof['COMPANYJOBEND']); ?>" size="10" maxlength="22" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("COMPANYJOBEND");?> <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYJOBEND", $cnt1); ?> </td>
            </tr>
          </table>
          <input type="hidden" name="kt_pk_spp_prof_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_prof['kt_pk_spp_prof']); ?>" />
          <input type="hidden" name="ARCHOUSAID_<?php echo $cnt1; ?>" id="ARCHOUSAID_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_prof['ARCHOUSAID']); ?>" />
          <input type="hidden" name="CREATEDATE_<?php echo $cnt1; ?>" id="CREATEDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_prof['CREATEDATE']); ?>" />
          <input type="hidden" name="LASTUPDATE_<?php echo $cnt1; ?>" id="LASTUPDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_prof['LASTUPDATE']); ?>" />
          <input type="hidden" name="CREATEDBY_<?php echo $cnt1; ?>" id="CREATEDBY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_prof['CREATEDBY']); ?>" />
          <input type="hidden" name="UPDATEDBY_<?php echo $cnt1; ?>" id="UPDATEDBY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_prof['UPDATEDBY']); ?>" />
          <?php } while ($row_rsspp_prof = mysql_fetch_assoc($rsspp_prof)); ?>
        <div class="KT_bottombuttons">
          <div>
            <?php 
      // Show IF Conditional region1
      if (@$_POST['PROFID'] == "") {
      ?>
                <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
                <?php 
      // else Conditional region1
      } else { ?>
              <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <?php }
      // endif Conditional region1
      ?>
              <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/archousa-profile.php')" />
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsJobs);

mysql_free_result($rsOccupType);

mysql_free_result($rsTitles);

mysql_free_result($rsState);
?>
