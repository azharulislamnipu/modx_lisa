<?php
#
# Snippet PPP v 0.3 beta
# By Uncle68 
# This release: 2-Jan-2007.
#
# Demo site: http://www.learningmodx.com
# Usage and examples - see documentation attached to this release
# Requires: eForm 1.4.1+
#
# Warning! This add-on might not be ready for public use
#

	//TODO: When should this (NOT) be run?
	function ppp_createTbl() {	
		
		global $modx, $table_prefix, $ppp_useTable;
	
		$sql = "CREATE TABLE IF NOT EXISTS `".$table_prefix.$ppp_useTable."` (
		  `webuser` int(10) unsigned NOT NULL,		   UNIQUE KEY `webuser` (`webuser`)
		)";

		$modx->db->query($sql);

			//Need to have at least one row for other functions to work.
			$rs = $modx->db->select('*', $table_prefix.$ppp_useTable);
			$countRows= $modx->db->getRecordCount($rs);
			
			if ($countRows < 1) {
				$webuserfield = "SET webuser='". $modx->getLoginUserID() . "'";			
				$modx->db->insert($webuserfield, $table_prefix.$ppp_useTable);
			}     
	}

	/* 	Get all columns for current user from custom table 
		- create placeholder named by field name	*/
	function ppp_makePhCustomTbl() {
		
		global $modx, $ppp_useTable, $ppp_userId, $arrayPhCustom;
		
		$table = $modx->getFullTableName($ppp_useTable);
		$results= $modx->db->select("*", $table, "webuser= $ppp_userId", "", "");
					
				if ($modx->db->getRecordCount($results) > 0) {
					foreach($row= $modx->db->getRow($results) as $key => $value) {
						$modx->setPlaceholder($key, $value);
						$arrayPhCustom[$key] = $value;
					}
				}
	}			

	function ppp_makePhStandardTbl() {
			
			/*	Get key (setting name)  and setting value from tables web_user_settings and web_user_attributes
				Create placeholder where setting_name is the placeholder name  */
		
		global $modx, $ppp_userId, $arrayPhSettings, $arrayPhAttributes;
		
			$table = $modx->getFullTableName("web_user_settings");
			$results= $modx->db->select("setting_name, setting_value", $table, "webuser= $ppp_userId", "", "");

				while ($rad = $modx->db->getRow($results)) {
					$modx->setPlaceholder($rad[setting_name] ,$rad[setting_value]);
					$arrayPhSettings[$rad[setting_name]] = $rad[setting_value];
				}

			$table = $modx->getFullTableName('web_user_attributes');
			$results= $modx->db->select('*', $table, 'id= '. $ppp_userId, "", "");

				foreach($row= $modx->db->getRow($results) as $key => $value) {
					if ($key=='sessionid' or $key=='failedlogincount') continue; //Would anyone need those?
					$modx->setPlaceholder($key, $value);
					$arrayPhAttributes[$key] = $value;
				}
	}
	
	//Adds new custom field
	//Thanks to Chuck (ProWebscape)  you can now also specify field type when creating new custom field.
	function ppp_addCustomField() {
		
		global $modx, $ppp_useTable, $table_prefix, $ppp_allowCustomColumns;

		if ($ppp_useTable=='0') {
			
			echo 'To be able to add fields you must first define a table to use in your ppp snippet call.';
			
		} else {
			
			?>

			<form method="post" id="ppp_addFieldForm" action="[~[*id*]~]">
				[+msgSuccess+]

				<p>Add a new field to your selected custom table <?php echo '"' . $ppp_useTable . '".'; ?> 
				<br />This will also create a placeholder with the same name.</p>

				<p><label accesskey="a" for="ppp_addField">Name of new field:</label>
				<input type="text" name="ppp_addField" maxlength="60" /></p>

				<?php if ($ppp_allowCustomColumns=='1') { ?>


			<em>The Custom column type setting below are only for users who are experienced in database handling.</em>
					<p><label accesskey="b" for="ppp_customColumnType">Custom column type:</label>

				<input name="ppp_customColumnType" type="text" value="" /></p>
				<?php } ?>

				<p><input type="submit" name="submit" value="Send" /></p>

			</form>
			<?php
				if (isset($_POST[ppp_customColumnType]) && $ppp_allowCustomColumns=='1') {
					$customCol = $modx->db->escape($_POST[ppp_customColumnType]);
				} else {
					$customCol = 'TINYTEXT NOT NULL';
				}

				if (isset($_POST[ppp_addField])) {
					//TODO: Escape is not good for adding a new column name. What to do? Only admin will add, but should be safe anyway.
					$addCol = $modx->db->escape($_POST[ppp_addField]);
					$table = $table_prefix.$ppp_useTable;
					$sql = "ALTER TABLE ". $table . " ADD COLUMN " . $addCol. " " . $customCol;
								
					$result = $modx->db->query($sql);
					if ($result=1) {
						$msg = '<p><em>Your new field "'.$addCol . '" was added to the "'.$ppp_useTable.'" table and is now avalible as a placeholder together with the ones listed below.</em></p>';
						$modx->setPlaceholder('msgSuccess', $msg);
					}
				}
		}	
	}	

	//This is just to be able to show a list of users - see other functions for more advanced listings
	function ppp_showListOfUsers($ppp_listUsersLimit, $ppp_listUsersDirectTo, $ppp_listUsersWithFields) {		

		global $modx;
		GLOBAL $ppp_userId, $ppp_useTable;
		
		$useTable = $ppp_useTable;
		$noOfUsers = $ppp_listUsersLimit;
		$directTo = $ppp_listUsersDirectTo;
		$withFields = $ppp_listUsersWithFields;
		

			//Determins if user should be shown in the link list, based on if required fields filled in 
			function ppp_onlyWithFields($userId, $withFields, $useTable) {
				
				GLOBAL $modx;
				
				//How many fields are set as required in the snippet call? Use later to compare  
				$arrUseFields = explode(',', str_replace(' ', '', $withFields)); //we need to be sure about spaces and count number of field names
				$wordCountWithFields = count($arrUseFields); 
				
					//Let's check how many of required fields that are set by current user
					$selectFields = implode(', ',$arrUseFields); //Format for the select				
					$where = "webuser=" .$userId;
					$table = $modx->getFullTableName($useTable);
					$results = $modx->db->select($selectFields, $table, $where);

					//Now let's do the actual count of how many field values are set
					$wordCountTblFields = 0;
					$row = $modx->db->getRow($results);
						if (is_array($row)) {
							foreach($row as $value) {if (!$value=='') ++$wordCountTblFields;}
							return ($wordCountTblFields == $wordCountWithFields) ? true : false ;
						} else {
							return false;
						}	
							
			}
							$valueCountTable = count($row = $modx->db->getRow($results));	
		
			$table = $modx->getFullTableName("web_users");		
			$rs = $modx->db->select("id, username", $table, "", "username ASC", $noOfUsers);

					//Thanks sottwell and SynthX
					if($modx->config['friendly_urls'] == 1) {
						  $arg = '?';
					} else {
						  $arg = '&';
						}
			
			$ppp_PhListUsers=''; 
		
				while ($row = $modx->db->getRow($rs)) {
					$userName = $row['username'];
					$userId = $row['id'];
						//if mandatory fields are set in ppp call: exclude some users from list
						if ($withFields) {
							if (!ppp_onlyWithFields($userId, $withFields, $useTable)) continue;
						}	
					$ppp_PhListUsers .= '<a href="[~'. $directTo .'~]'.$arg. 'WID=' . $userId . '">' . $userName . '</a><br />';
				}
					
			$modx->setPlaceHolder('ppp_ListUsers', $ppp_PhListUsers);
	}	

	//Advanced user list creator	
	function ppp_listUsersAdvanced($ppp_listUsersAdv) {
		
		GLOBAL $modx;
		
		$arrayLists = explode(';', trim($ppp_listUsersAdv));
		$noOfLists = count($arrayLists);

		$table_users = $modx->getFullTableName('web_users');	
		$table_attrib = $modx->getFullTableName('web_user_attributes');
		$table_wg = $modx->getFullTableName('web_groups');
		$table_wg_n = $modx->getFullTableName('webgroup_names');
		$table_au = $modx->getFullTableName('active_users');
		
		//Process each list	
		for ($n=0;$n<$noOfLists;$n++) { 
			
			//Set to be empty after each round
			$sql = '';
			$tableToUse = '';
			$where = '';
			$arrUseFields = '';
			$selectFields = '';
			$limit = '';
			$orderBy = '';
			$createList = '';
			$chunkTpl = '';
			$webGroups = '';
			$extra = '';
			$timeSpan = '';
			
			list($listName, $tableToUse, $chunk_tpl, $orderBy, $limit, $withFields, $webGroups, $extra) = explode(':', $arrayLists[$n]);
			
			$table_custom = $modx->getFullTableName($tableToUse);

			
			//Will rewrite so that only fields (placeholders) that are in the template chunk will be seleced in the query
			if (!EMPTY($tableToUse)) {
				$sql = 'SELECT u.username, a.*, c.*, g.*, n.*, au.lasthit FROM ' . $table_users . ' u';
				$sql .= ' INNER JOIN ' . $table_attrib . ' a ON u.id = a.id';
				$sql .= ' INNER JOIN ' . $table_wg . ' g ON u.id = g.webuser';
				$sql .= ' INNER JOIN ' . $table_wg_n . ' n ON g.webgroup = n.id';
				$sql .= ' LEFT OUTER JOIN ' . $table_au . ' au ON u.username = au.username';				
				$sql .= ' RIGHT OUTER JOIN ' . $table_custom . ' c ON u.id = c.webuser';
			} else {
				$sql = 'SELECT u.username, a.*, g.*, n.*, au.lasthit FROM ' . $table_users . ' u';
				$sql .= ' INNER JOIN ' . $table_attrib . ' a ON u.id = a.id';
				$sql .= ' INNER JOIN ' . $table_wg . ' g ON u.id = g.webuser';
				$sql .= ' INNER JOIN ' . $table_wg_n . ' n ON g.webgroup = n.id';
				$sql .= ' LEFT OUTER JOIN ' . $table_au . ' au ON u.username = au.username';
			}

			// Include / exclude users depending on if certin fields are empty or not
			if (!EMPTY($withFields)) {
		
				$arrayWithFields = explode(',', trim($withFields));
					
					$m = 0;
					for($m=0; $m < count($arrayWithFields); $m++) {
					
						// To include or exclude fields, set operator
						if ((substr(trim($arrayWithFields[$m]),0,1)) == '!') {
								$oper = '=';
							} else {
								$oper = '<>';
						}
						
						// Select fields to include / exclude
						if ($m==0) {
							$selectFields .= trim(str_replace('!', '', $arrayWithFields[$m])) . " $oper ''";
							continue;
						}						
							$selectFields .= " AND " . trim(str_replace('!', '', $arrayWithFields[$m])) . " $oper ''";				
					}
			}
			
			//Webgroups, Choose between: 
				// OFF = Lists all webusers from all groups
				// ANY = List users that are in in ANY of the listed groups
				// ALL = Lists user that are in ALL of the listed groups (not working in this version)
						
			$groupMode = substr(trim($webGroups), 0, 3);
			
			//TODO: Rewrite, off not needed
			if ($groupMode=='OFF') {

				$webGroups = ''; //Be sure it's empty if set to OFF
			
			} else {
				
				$arrayGroups = explode(',', trim(substr($webGroups, 3)));
				$webGroups = ''; //Empty for later use
				
					if ($groupMode=='ANY') {
					
							for($x=0; $x < count($arrayGroups); $x++) {
								if ($x==0) {
									$webGroups .= "'" . trim($arrayGroups[$x]) . "'";
									continue;
								}
									$webGroups .= ", '" . trim($arrayGroups[$x]) . "'";
							}
						
						 $webGroups = 'n.name IN (' . $webGroups . ')';
					}
					
					//Sorry, does not work, need to redesign the query
					if ($groupMode=='ALL') {

						for($x=0; $x < count($arrayGroups); $x++) { 
							if ($x==0) {
								$webGroups .= "n.name IN ('" . trim($arrayGroups[$x]) . "') ";
								continue;
							}
								$webGroups .= " AND n.name IN ('" . trim($arrayGroups[$x]) . "')";
						}
					}					
			}
		
			//Need to rewrite when more extra options are available
			if (!EMPTY($extra)) {
				if(substr(trim($extra), 0, 6) == 'ONLINE') {
					$timeSpan = substr(trim($extra),6)*60;
				}
			}
			
			//Put together the where clausul
			$where = '';
			if (!EMPTY($webGroups)) {
			
				if (EMPTY($where)) {$where = ' WHERE ';} else { $where .= ' AND ';}
				$where .= '(' . $webGroups . ')';
			}
			
			if (!EMPTY($selectFields)) {
			
				if (EMPTY($where)) {$where = ' WHERE ';} else { $where .= ' AND ';}
				$where .= '(' . $selectFields . ')';
			}
			
			if (!EMPTY($timeSpan)) {
			
				if (EMPTY($where)) {$where = ' WHERE ';} else { $where .= ' AND ';}
				$where .= '((au.lasthit) > (UNIX_TIMESTAMP() - ' . $timeSpan . '))' ;
			}
		
			
			$groupBy = " GROUP BY u.id, u.username";
			
			if (!EMPTY($orderBy)) {
				$orderBy = ' ORDER BY ' . $orderBy;
			}
			
			if (EMPTY($limit)) {
					$limit = ' LIMIT 50'; //Defaults to 50
				} else {
					$limit = ' LIMIT ' . $limit;	
			}	
//DEBUG - Remove the commenting to see the actual query to the database, good for trouble shooting
//echo 'Debug: <br />' . $sql.$where.$groupBy.$orderBy.$limit . '<br /><br />'; 
			
			//Let's have a chat with the database
			$rs = $modx->db->query($sql.$where.$groupBy.$orderBy.$limit);

				while($row = $modx->db->getRow($rs)) {
				$key = '';
				$value = '';
					
					$chunkTpl = $modx->getChunk($chunk_tpl);

						foreach($row as $key => $value) {
							$chunkTpl = str_replace('[+' . $key . '+]', $value, $chunkTpl);
						}	
					
					$createList .= 	$chunkTpl;
				}
				
			$modx->setPlaceHolder($listName, $createList);	
		
		} //If more than one list we'll start over again	
	return '';
	}

	function updatetable($fields) {

	global $modx, $ppp_userId, $ppp_useTable, $ppp_storeTo;
		
		//Takes value from hidden input on eForm form template
		$idToEdit = $_POST[wuid];

			//Let's handle file uploads first
			$storeTo = ''.$ppp_storeTo;
			if (!is_dir($storeTo)) {mkdir($storeTo);}

		$countKeys =  count($_FILES);
		$arrKey = array_keys($_FILES);
						
				for ($k = 0; $k < $countKeys; $k++) {
					if (!isset($_FILES[$arrKey[$k]]['name'])) continue;
					$storeFileName = $modx->db->escape($_FILES[$arrKey[$k]]['name']);
					move_uploaded_file($_FILES[$arrKey[$k]]['tmp_name'], $storeTo . $storeFileName);
				}

			//Now, let's start with the custom table
			
			$table = $modx->getFullTableName($ppp_useTable);
			$currentFields = array_keys($modx->db->getTableMetaData($table)); 
			$where = "webuser = ".$idToEdit;
			
			//If first edit (new user) the id key (webuser) does not exist: add user id to the table				
				//Saftey check: Check if the user already is in the custom table (shouldn't be)										
				if ($rs = $modx->db->select('*', $table, $where)) {
					$countRows= $modx->db->getRecordCount($rs);
					
					if ($countRows < 1) {
						$webuserfield = "SET webuser='". $idToEdit . "'";			
						$modx->db->insert($webuserfield, $table);
					}
				}	
					
			//Start collecting fields that will be updated	
			foreach($fields as $key => $value) {
		
				if (!in_array($key, $currentFields)) continue;
				if (!$value) continue; //only changes updated fields - TODO: complete erase of field content not possible
					$fields_values[$key] = $modx->db->escape($value);
				}  
				
			//Update table
			$rows_affected = $modx->db->update($fields_values, $table, $where);
	  
			// Why not working?
			If (!$rows_affected==1) echo "Something went wrong."; //If displayed -> Trouble!!!
			
			
				//Done with custom table, let's go for the attributes table
				$fields_values='';
				$table='';
				$currentFields = '';
				$where='';
				
				$table = $modx->getFullTableName('web_user_attributes');
				$currentFields = array_keys($modx->db->getTableMetaData($table));
				$where = "internalKey = ".$idToEdit;					
				
						//Start collecting fields that will be updated	
						foreach($fields as $key => $value) {
		
							if (!in_array($key, $currentFields)) continue;
								//if (!$value) continue; //only changes updated fields - TODO: complete erase of field content not possible
								$fields_values[$key] = $modx->db->escape($value);
						}	  
				
				//Update table
				$rows_affected = $modx->db->update($fields_values, $table, $where);
		  
				// Why not working?
				If (!$rows_affected==1) echo "Something went wrong."; //If displayed -> Trouble!!!
	 
		return true;
	}
	
	function populate(&$fields) {

		GLOBAL $modx, $ppp_userId, $ppp_editOtherUser;	
		GLOBAL $arrayPhCustom, $arrayPhSettings, $arrayPhAttributes;
		

			/* Safety check to ensure user is the logged in user if not an allowed editor */
			if (($ppp_userId != $modx->getLoginUserID()) && ($ppp_editOtherUser != 1)) {
				echo "<p>Something went wrong. Sorry.</p>";
				return false;
			}		
			
					if (count($arrayPhCustom) > 0) {		
							foreach($arrayPhCustom as $key => $value) {
								$fields[$key] = $value;
							}
					}

					if (count($arrayPhSettings) > 0) {	
							foreach($arrayPhSettings as $key => $value) {
								$fields[$key] = $value;
							}		
					}
					
					//No check needed, always exists some values
					foreach($arrayPhAttributes as $key => $value) {
						$fields[$key] = $value;
					}
						
		return true;
				
	}

	
	function ppp_listPlaceholders($arrayPhSettings, $arrayPhCustom, $arrayPhAttributes) {
		global $ppp_useTable;
		
		$ppp_listPh='';

		if (isset($arrayPhCustom)) {
				 $ppp_listPh .= '<p><strong>List of placeholders for selected custom table "'.$ppp_useTable.'":</strong></p>'; 
						$ppp_listPh .= '<table border="0" cellspacing="0">';
						$ppp_listPh .= "<tr><td>Placeholder name: </td><td>&nbsp;&nbsp;</td>";
						$ppp_listPh .= "<td>Value for you (if any): </td></tr>";
						
					foreach($arrayPhCustom as $placeholderCustom => $curValueCustom) {
						$ppp_listPh .= "<tr><td>" . $placeholderCustom . "</td><td>&nbsp;&nbsp;</td><td>";
						$ppp_listPh .= $curValueCustom ."</td></tr>";
					}		 
						$ppp_listPh .= "</table>";
					} else {
						$ppp_listPh .= "<p><strong>You haven't yet created any fields in your selected custom table or you haven't defined any custom table.</strong></p>";
		}	

		
		if (isset($arrayPhSettings)) {
				 $ppp_listPh .= '<p><strong>List of placeholders for standard table "web_user_settings":</strong></p>'; 
						$ppp_listPh .= '<table border="0" cellspacing="0">';
						$ppp_listPh .= "<tr><td>Placeholder name: </td><td>&nbsp;&nbsp;</td>";
						$ppp_listPh .= "<td>Value for you (if any): </td></tr>";
						
					foreach($arrayPhSettings as $placeholderSettings => $curValueSettings) {
						$ppp_listPh .= "<tr><td>" . $placeholderSettings . "</td><td>&nbsp;&nbsp;</td><td>";
						$ppp_listPh .= $curValueSettings ."</td></tr>";
					}		 
						$ppp_listPh .= "</table>";
					} else {
						$ppp_listPh .= "<p><strong>You haven't created any fields in the web_user_settings table yet.</strong><br />This will be possible in later version of ppp.</p>";
		}
		

		if (isset($arrayPhAttributes)) {
				 $ppp_listPh .= '<p><strong>List of placeholders for standard table "web_user_attributes":</strong></p>'; 
						$ppp_listPh .= '<table border="0" cellspacing="0">';
						$ppp_listPh .= "<tr><td>Placeholder name: </td><td>&nbsp;&nbsp;</td>";
						$ppp_listPh .= "<td>Value for you (if any): </td></tr>";
						
					foreach($arrayPhAttributes as $placeholderCustom => $curValueCustom) {
						$ppp_listPh .= "<tr><td>" . $placeholderCustom . "</td><td>&nbsp;&nbsp;</td><td>";
						$ppp_listPh .= $curValueCustom ."</td></tr>";
					}		 
						$ppp_listPh .= "</table>";
					} else {
						$ppp_listPh .= "<p><strong>You haven't created any fields in the web_user_attributes table yet.</strong></p>";
		}	
				
		echo $ppp_listPh;

	} 
	

?>	