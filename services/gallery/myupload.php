<?php
/**
 * SimpleViewer admin package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

/** Callback function for myUploader
*
*/
require 'includes/constants.php';
$validImageTypes = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
$absImageDirectory = $_GET['path'];
$POST_MAX_SIZE = ini_get('post_max_size');
$mul = substr($POST_MAX_SIZE, -1);
$mul = ($mul == 'M' ? 1048576 : ($mul == 'K' ? 1024 : ($mul == 'G' ? 1073741824 : 1)));
if ($_SERVER['CONTENT_LENGTH'] > $mul*(int)$POST_MAX_SIZE && $POST_MAX_SIZE)
{
  header("HTTP/1.0 500 Internal Server Error");
  exit;
}
foreach ($_FILES["userfile"]["error"] as $key => $error)
{
  if ($error != UPLOAD_ERR_OK)
  {
    error_log($error);
    header("HTTP/1.0 500 Internal Server Error");
    exit;
  }
  $tmp_name = $_FILES["userfile"]["tmp_name"][$key];
  $name = $_FILES["userfile"]["name"][$key];
  $file = basename(urldecode($name));
  $imageSize = getimagesize($tmp_name);
  if ($imageSize === false ) continue;
  if (!in_array($imageSize[2], $validImageTypes)) continue;
  if (!move_uploaded_file($tmp_name, $absImageDirectory.$file))
  {
    header("HTTP/1.0 500 Internal Server Error");
    exit;
  }
  @chmod($absImageDirectory.$file, UPLOAD_MODE);  
}
?>
