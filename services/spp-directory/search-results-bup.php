<?php setlocale(LC_ALL, 'en_EN'); ?>
<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
    global $sigma_modx;
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($sigma_modx, $theValue) : mysqli_escape_string($sigma_modx, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchons = "-1";
if (isset($_GET['id'])) {
$colname_rsArchons = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsArchons = sprintf("SELECT * FROM vw_elig WHERE WEB_ID = %s", GetSQLValueString($colname_rsArchons, "int"));
$rsArchons = mysqli_query($sigma_modx, $query_rsArchons) or die(mysqli_error($sigma_modx));
$row_rsArchons = mysqli_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysqli_num_rows($rsArchons);

$colname_rsProfessional = "-1";
if (isset($_GET['id'])) {
  $colname_rsProfessional = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsProfessional = sprintf("SELECT * FROM vw_web_elig_educ WHERE WEB_ID = %s ORDER BY DEGREEYEAR ASC", GetSQLValueString($colname_rsProfessional, "int"));
$rsProfessional = mysqli_query($sigma_modx, $query_rsProfessional) or die(mysqli_error($sigma_modx));
$row_rsProfessional = mysqli_fetch_assoc($rsProfessional);
$totalRows_rsProfessional = mysqli_num_rows($rsProfessional);

$colname_rsOffices = "-1";
if (isset($row_rsArchons['CUSTOMERID'])) {
  $colname_rsOffices = (get_magic_quotes_gpc()) ? $row_rsArchons['CUSTOMERID'] : addslashes($row_rsArchons['CUSTOMERID']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsOffices = sprintf("SELECT * FROM spp_officers WHERE CUSTOMERCD = %s ORDER BY JOINDATE DESC", GetSQLValueString($colname_rsOffices, "int"));
$rsOffices = mysqli_query($sigma_modx, $query_rsOffices) or die(mysqli_error($sigma_modx));
$row_rsOffices = mysqli_fetch_assoc($rsOffices);
$totalRows_rsOffices = mysqli_num_rows($rsOffices);

$colname_rsBoule = "-1";
if (isset($_SESSION['BOULENAME'])) {
  $colname_rsBoule = (get_magic_quotes_gpc()) ? $_SESSION['BOULENAME'] : addslashes($_SESSION['BOULENAME']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsBoule = sprintf("SELECT * FROM vw_web_elig_no_officers WHERE BOULENAME = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsBoule, "text"));
$rsBoule = mysqli_query($sigma_modx, $query_rsBoule) or die(mysqli_error($sigma_modx));
$row_rsBoule = mysqli_fetch_assoc($rsBoule);
$totalRows_rsBoule = mysqli_num_rows($rsBoule);

$colname_rsJobs = "-1";
if (isset($_GET['id'])) {
  $colname_rsJobs = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsJobs = sprintf("SELECT    spp_prof.PROFID,   spp_prof.TITLEID,   spp_skillref.DESCRIPTION AS JOBTITLE,   spp_prof.CUSTOMERID,   spp_prof.COMPANYNAME,   spp_prof.COMPANYCITY,   spp_prof.COMPANYSTCD,   spp_prof.DESCRIPTION,   spp_prof.COMPANYJOBSTART,   web_user_attributes.id AS WEB_ID FROM   spp_prof   INNER JOIN spp_skillref ON (spp_prof.TITLEID = spp_skillref.SKILLID)   INNER JOIN web_user_attributes ON (spp_prof.CUSTOMERID = web_user_attributes.internalKey) WHERE web_user_attributes.id = %s ORDER BY COMPANYJOBSTART DESC", GetSQLValueString($colname_rsJobs, "int"));
$rsJobs = mysqli_query($sigma_modx, $query_rsJobs) or die(mysqli_error($sigma_modx));
$row_rsJobs = mysqli_fetch_assoc($rsJobs);
$totalRows_rsJobs = mysqli_num_rows($rsJobs);

$colname_rsArchonOcc = "-1";
if (isset($_GET['id'])) {
  $colname_rsArchonOcc = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsArchonOcc = sprintf("SELECT    spp_prof.OCCUPATIONID,   spp_occupation.DESCRIPTION,   spp_prof.CUSTOMERID,   web_user_attributes.id AS WEB_ID FROM   spp_prof   INNER JOIN spp_occupation ON (spp_prof.OCCUPATIONID = spp_occupation.OCCUPATIONID)   INNER JOIN web_user_attributes ON (spp_prof.CUSTOMERID = web_user_attributes.internalKey) WHERE web_user_attributes.id = %s GROUP BY OCCUPATIONID", GetSQLValueString($colname_rsArchonOcc, "int"));
$rsArchonOcc = mysqli_query($sigma_modx, $query_rsArchonOcc) or die(mysqli_error($sigma_modx));
$row_rsArchonOcc = mysqli_fetch_assoc($rsArchonOcc);
$totalRows_rsArchonOcc = mysqli_num_rows($rsArchonOcc);

$colname_rsArchonTitles = "-1";
if (isset($_GET['id'])) {
  $colname_rsArchonTitles = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsArchonTitles = sprintf("SELECT    spp_prof.CUSTOMERID,   spp_prof.TITLEID,   spp_skillref.DESCRIPTION AS JOBTITLE,   web_user_attributes.id AS WEB_ID FROM   spp_prof   INNER JOIN spp_skillref ON (spp_prof.TITLEID = spp_skillref.SKILLID)   INNER JOIN web_user_attributes ON (spp_prof.CUSTOMERID = web_user_attributes.internalKey) WHERE web_user_attributes.id = %s GROUP BY TITLEID", GetSQLValueString($colname_rsArchonTitles, "int"));
$rsArchonTitles = mysqli_query($sigma_modx, $query_rsArchonTitles) or die(mysqli_error($sigma_modx));
$row_rsArchonTitles = mysqli_fetch_assoc($rsArchonTitles);
$totalRows_rsArchonTitles = mysqli_num_rows($rsArchonTitles);

$colname_rsChildren = "-1";
if (isset($_GET['id'])) {
  $colname_rsChildren = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsChildren = sprintf("SELECT web_user_attributes.id,   spp_children.FIRSTNAME,   spp_children.MIDDLEINITIAL,   spp_children.LASTNAME,   spp_children.SUFFIX FROM web_user_attributes   INNER JOIN spp_children ON (web_user_attributes.internalKey = spp_children.CUSTOMERID) WHERE web_user_attributes.id = %s", GetSQLValueString($colname_rsChildren, "int"));
$rsChildren = mysqli_query($sigma_modx, $query_rsChildren) or die(mysqli_error($sigma_modx));
$row_rsChildren = mysqli_fetch_assoc($rsChildren);
$totalRows_rsChildren = mysqli_num_rows($rsChildren);
?>
<!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --><!DOCTYPE html>
<!--
 ____________________________________________________________
|                                                            |
|      DATE + 2006.11.14                                     |
| COPYRIGHT + Sigma Pi Phi 2006.  All Rights Reserved.       |
|____________________________________________________________|

-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-meta.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-css.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-javascript.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-links.php"); ?>

    <!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body class="home blue-bar color-override-yellow-orange">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/main-nav.php"); ?>
			<div class="width">
			    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/global-logo-tagline.php"); ?>
			</div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
	<!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- InstanceBeginEditable name="pagetitle" -->
            <div class="row grid-12">

                        <section class="margin-bottom-medium">
                        <div class="span-12">
                            <div class="inner">
                            <h1>Viewing Archon Profile</h1>
                            <ol class="breadcrumbs" style="margin-top:-18px;"><li><a class="B_homeCrumb" href="/home/archon-home.php" title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li><li><a class="B_crumb" href="/home/archon-and-member-boule-management.php" title="Archon &amp;amp; Member Boul� Management">Administration</a></li><li><a class="B_crumb" href="/home/archon-services.php" title="Archon Services">Archons</a></li><li><span class="B_currentCrumb">Viewing Archon</span></li></ol>
                            </div>
                        </div>
                        </section>
             <!-- InstanceEndEditable -->
             
             <!-- InstanceBeginEditable name="content" -->
<section class="margin-bottom-medium">
        <div class="span-12">

		<div class="inner">

	
	


<div class="span-4"><div class="margin-top-none">
<div class="width padding">
  <style>
table.lines {
	width: 100%;
	margin: 0;
}

table.lines th:first-child,
table.lines td:first-child {
	padding-left: 0px;
}

table.lines th:last-child,
table.lines td:last-child {
	padding-right: 0px;
}

table.lines th,
table.lines td {
	border: 0;
	vertical-align: top;
}

table.lines td {
	border-top: 1px solid #ccc;
}

img {
	vertical-align: middle;
}
  </style>
  <!-- #MAIN COLUMN -->

<h1 ><?php echo $row_rsArchons['L2R_FULLNAME2']; ?></h1>


	  <?php 
// Show IF Conditional region9 
if (@$row_rsArchons['IMAGE'] != NULL) {
?>
	      <img src="/home/assets/images/archons/<?php echo $row_rsArchons['IMAGE']; ?>" width="190"/>
	      <?php 
// else Conditional region9
} else { ?>
	    <img src="../includes/tng/styles/img_not_found.gif" alt="Add your image." width="100" height="100"/>
  <?php } 
// endif Conditional region9
?>	

<strong><?php echo $row_rsArchons['BOULENAME']; ?> Boul&eacute;</strong><br />
<?php echo $row_rsArchons['BOULECITY']; ?>, <?php echo $row_rsArchons['BOULESTATECD']; ?><br />
<?php echo $row_rsArchons['REGIONNAME']; ?> Region 
  <?php 
// Show IF Conditional region5 
if (@$row_rsArchons['JOINDATE'] != NULL) {
?>
    <p><strong>Member Since</strong><br />
	    <?php 
// Show IF Conditional region10 
if (@$row_rsArchons['JOINDATE'] != NULL) {
?>
	        <?php echo date("F Y",strtotime($row_rsArchons['JOINDATE'])); ?>            </p>
	        	      
    <?php } 
// endif Conditional region10
?>	  
    <?php } 
// endif Conditional region5
?>

    <?php if ($totalRows_rsOffices > 0) { // Show if recordset not empty ?>
      <p><strong>Offices Held</strong><br />
	  <?php do { ?>
          <?php echo $row_rsOffices['CPOSITION']; ?>, <?php echo date('Y',strtotime($row_rsOffices['JOINDATE'])); ?><br />
        <?php } while ($row_rsOffices = mysqli_fetch_assoc($rsOffices)); ?>
      <?php } // Show if recordset not empty ?>
      </p>


<p><strong>Mailing Address</strong><br />
          <?php echo $row_rsArchons['ADDRESS1']; ?><br />
        <?php echo $row_rsArchons['CITY']; ?>, <?php echo $row_rsArchons['STATECD']; ?> <?php echo $row_rsArchons['ZIP']; ?> </p>
<p><strong>Email</strong><br />
      <a href="mailto:<?php echo $row_rsArchons['EMAIL']; ?>"><?php echo $row_rsArchons['EMAIL']; ?></a></p>
      
        <?php 
// Show IF Conditional region8 
if (@$row_rsArchons['HOMEPHONE'] != NULL) {
?>
          <p><strong>Home Phone</strong><br />
          <a href="tel:<?php echo $row_rsArchons['HOMEPHONE']; ?>"> <?php echo $row_rsArchons['HOMEPHONE']; ?></a></p>
          <?php } 
// endif Conditional region8
?>

          <?php 
// Show IF Conditional region15
if (@$row_rsArchons['MOBILEPHONE'] != NULL) {
?>
            <p><strong>Mobile</strong><br />
              <a href="tel:<?php echo $row_rsArchons['MOBILEPHONE']; ?>"><?php echo $row_rsArchons['MOBILEPHONE']; ?></a></p>
            <?php } 
// endif Conditional region15
?>


          <?php 
// Show IF Conditional region11 
if (@$row_rsArchons['BIRTHDATE'] != NULL) {
?>
              <p><strong>Birthday</strong><br />
                <?php echo date("F j", strtotime($row_rsArchons['BIRTHDATE'])); ?></p>
          <?php } 
// endif Conditional region11
?>


              <?php 
// Show IF Conditional region12 
if (@$row_rsArchons['SPOUSENAME'] != NULL) {
?>
                <p><strong>Archousa</strong><br/>
                    <?php echo $row_rsArchons['SPOUSENAME']; ?></p>
                <?php } 
// endif Conditional region12
?>
                <?php if ($totalRows_rsChildren > 0) { // Show if recordset not empty ?>
                
                  <p><strong>Children</strong><br>
                <?php do { ?>
                <?php echo $row_rsChildren['FIRSTNAME']; ?>  <?php echo $row_rsChildren['MIDDLEINITIAL']; ?> <?php echo $row_rsChildren['LASTNAME']; ?>  <?php echo $row_rsChildren['SUFFIX']; ?><br />
                <?php } while ($row_rsChildren = mysqli_fetch_assoc($rsChildren)); ?></p>
                
                  <?php } // Show if recordset not empty ?>

</div></div></div>
<!-- #END MAIN COLUMN -->


<!-- #BEGIN SECOND COLUMN -->
<div class="span-4"><div class="padding">
<div class="width padding">



<!-- #END SECOND COLUMN -->


<!-- #BEGIN SECOND COLUMN -->

<h3>Career</h3>


<?php 
// Show IF Conditional region10 
if ($totalRows_rsJobs > 0) {
?>

        <?php do { ?>
        
        <p><strong><?php echo $row_rsJobs['JOBTITLE']; ?></strong>, <?php echo $row_rsJobs['COMPANYNAME']; ?>,  <?php echo $row_rsJobs['COMPANYCITY']; ?> <?php echo $row_rsJobs['COMPANYSTCD']; ?>, <?php echo date('Y',strtotime($row_rsJobs['COMPANYJOBSTART'])); ?></p>
          <?php } while ($row_rsJobs = mysqli_fetch_assoc($rsJobs)); ?>
          <?php if ($totalRows_rsJobs == 0) { // Show if recordset empty ?>
            <p>No career information is available for this Archon.</p>
            <?php } // Show if recordset empty ?>
          <?php } 
// endif Conditional region10
?>
















<h3>Education</h3>

<?php 
// Show IF Conditional region11 
if (@$row_rsProfessional['DEGREE'] != NULL) {
?>
<?php do { ?>
<p><strong><?php echo $row_rsProfessional['INSTITUTION']; ?></strong>, <?php echo $row_rsProfessional['DEGREEYEAR']; ?><br/>
    <?php echo $row_rsProfessional['DEGREE']; ?>, <?php echo $row_rsProfessional['MAJOR']; ?><br/>
    <?php 
// Show IF Conditional region1 
if (@$row_rsProfessional['MINOR1'] != NULL) {
?>
    <span class="font-small"><?php echo @$row_rsProfessional['MINOR']; ?></span><br/>
    <?php echo $row_rsProfessional['MINOR2']; ?></p>
<?php } 
// endif Conditional region1
?>
<?php } while ($row_rsProfessional = mysqli_fetch_assoc($rsProfessional)); ?>
<?php if ($totalRows_rsProfessional == 0) { // Show if recordset empty ?>
  <p>Education information is not available for this Archon.</p>
  <?php } // Show if recordset empty ?>
<?php } 
// endif Conditional region11
?>
</div>
</div></div>

<!-- #begin THIRD COLUMN -->
<div class="span-4"><div class="padding">
<div class="width padding">

	<h3>Biography</h3>
<?php 
// Show IF Conditional region7 
if (@$row_rsArchons['BIO'] != NULL) {
?>
       <p><?php echo nl2br($row_rsArchons['BIO']); ?></p>
        <?php 
// else Conditional region7
} else { ?>
<p>A personal biography is not available for this Archon.</p>
<?php } 
// endif Conditional region7
?>


 <!-- #END THIRD COLUMN -->
</div></div>
        

	
	</div>
</div>
</section>

					
</div>			
<!-- resource content row end -->

			<!-- end showing the content, pagetitle, breadcrumb and sidebar -->
					
			<!-- 3 round images toggle start -->
            <!-- don't display spotlight row section -->
 			<!-- 3 round images toggle end -->
       </div>
    </div>
<!-- InstanceEndEditable -->
            <!-- main content body end -->	
	
	<!-- third section event and articles start -->
  	 <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
	<!-- third section event and articles end -->
	
    <footer class="width padding" style="border-top: 10px #52B609 solid;">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-content-default.php"); ?>
      </div>
    </footer>
	
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-scripts.php"); ?>
</body>
<!-- InstanceEnd --></html>
<?php
mysqli_free_result($rsArchons);

mysqli_free_result($rsProfessional);

mysqli_free_result($rsOffices);

mysqli_free_result($rsBoule);

mysqli_free_result($rsJobs);

mysqli_free_result($rsArchonOcc);

mysqli_free_result($rsArchonTitles);

mysqli_free_result($rsChildren);
?>
