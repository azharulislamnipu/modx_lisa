<?php
/**
 * SimpleViewer admin package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

/**
 * Authentication class
 *
 * @access public
 * @package svManager
 */
Class Auth
{
 /**
  * @var array in form user=>password
  */
  var $authData = array();
  
 /**
  * Constructs Auth
  * Test to see if the password file contains just one key=>value combination
  * The script dies straight away if errors are not trapped (e.g. error_reporting set to 0)
  */
  function Auth()
  {
    session_start();
  }
  
 /**
  * Generate hash
  *
  * @access private
  * @return string hash
  * @param string plain text password
  * @param string stored hash
  */
  function generateHash($plainText, $hash = null)
  {
    if ($hash === null)
    {
      $salt = substr(md5(uniqid(mt_rand(), true)), 0, SALT_LENGTH);
    }
    else
    {
      $salt = substr($hash, 0, SALT_LENGTH);
    }
    return $salt . sha1($salt.$plainText);
  }


 /**
  * Sets-up the session variables for valid users
  *
  * @access public
  * @return boolean logged-in
  */
  function login()
  {
    if (isset($_SESSION[SESSION_HASH]))
    {
      return $this->confirmAuth();
    }
    if (!isset($_POST['user']) || !isset($_POST['password']))
    {
      return false;
    }
    @$authData = file_get_contents(AUTH_FILE);
    if ($authData === false)
    {
      die('Error: cannot read the stored user names and passwords. See the documentation for how to fix password problems.');
    }
    $authData = substr($authData, 16);
    @$authData = unserialize($authData);
    if (!is_array($authData) || (count($authData) != 1))
    {
      die('Error: the password file is corrupted. See the documentation for how to fix password problems.');
    }
    sleep(SLEEP_TIME);
    $user = $_POST['user'];
    $password = $_POST['password'];
    unset($_POST['user']);
    unset($_POST['password']);
    $userCount = 0;
    foreach($authData as $validUser => $validPasswordHash)
    {
      $passwordHash = $this->generateHash($password, $validPasswordHash);
      if ($user == $validUser && $passwordHash == $validPasswordHash) $userCount++;
    }
    if ($userCount != 1)
    {
      return false;
    }
    $_SESSION[SESSION_DEFAULT_PASS] = ($password == 'admin');
    $_SESSION[SESSION_USER] = $user;
    $_SESSION[SESSION_PASS] = $passwordHash;
    $_SESSION[SESSION_HASH] = md5(SALT.$user.$passwordHash);
    return true;  
  }

 /**
  * Checks for valid session variable
  *
  * @access private
  * @return boolean session variable contains valid user name and password
  */
  function confirmAuth()
  {
    if ( !isset ( $_SESSION[SESSION_USER]) || !isset($_SESSION[SESSION_PASS]) || !isset($_SESSION[SESSION_HASH]))
    {
      return false;
    }
    $user = $_SESSION[SESSION_USER];
    $passwordHash = $_SESSION[SESSION_PASS];
    $loginHash = $_SESSION[SESSION_HASH];
    return (md5(SALT.$user.$passwordHash) == $loginHash);     
  }

 /**
  * Deletes user, password and loginHash session variables.
  * Note that galleryRef session variable is not deleted because it has
  * no security implications and user can log-in again to the same gallery.
  *
  * @access public
  * @return void
  */
  function logout()
  {
    unset($_SESSION[SESSION_USER]);
    unset($_SESSION[SESSION_PASS]);
    unset($_SESSION[SESSION_HASH]);
    return;
  }
  
 /* Confirm password only, irrespective of user name
  * Used when changing passwords and on galleries screen
  * @access public
  * @return boolean
  */
  function confirmPass($plainPass)
  {
    @$authData = file_get_contents(AUTH_FILE);
    if ($authData === false)
    {
      trigger_error('cannot read the stored user names and passwords. See the documentation for how to fix password problems', E_USER_NOTICE);
      return false;
    }
    $authData = substr($authData, 16);
    @$authData = unserialize($authData);
    if (!is_array($authData) || (count($authData) != 1))
    {
      trigger_error('the password file is corrupted. See the documentation for how to fix password problems', E_USER_NOTICE);
      return false;
    }
    sleep(SLEEP_TIME);
    $userCount = 0;
    foreach($authData as $validUser => $validPasswordHash)
    {
      $passwordHash = $this->generateHash($plainPass, $validPasswordHash);
      if ($passwordHash == $validPasswordHash) $userCount++;
    }
    return ($userCount == 1);
  }
  
 /*
  * Change user name and password
  *
  * @access public
  * @return boolean true on success
  * @param string user name
  * @param string password
  */
  function changeLogin($user, $password)
  {
    $passwordHash = $this->generateHash($password);
    $authData = '<?php exit(); ?>'.serialize(array($user => $passwordHash));
    if(!file_put_contents(AUTH_FILE, $authData, FPC_LOCK))
    {
      trigger_error('Unable to store new user name and password in '.AUTH_FILE, E_USER_WARNING);
      return false;
    }
    $_SESSION[SESSION_DEFAULT_PASS] = ($password == 'admin');
    $_SESSION[SESSION_USER] = $user;
    $_SESSION[SESSION_PASS] = $passwordHash;
    $_SESSION[SESSION_HASH] = md5(SALT.$user.$passwordHash);  
    return true;
  }   
}

?>