/**
 * SimpleViewer admin package.
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
window.onload = externalLinks;
 
 /**
 * Sets target for external links
 * @link http://www.sitepoint.com/article/standards-compliant-world
 * @return void
 */
function externalLinks()
{
 if (!document.getElementsByTagName) return;
 var anchors = document.getElementsByTagName("a");
 for (var i=0; i<anchors.length; i++)
 {
   var anchor = anchors[i];
   if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external")
     anchor.target = "gallery";
   if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external_blank")
     anchor.target = "_blank";
 }
}

/**
 * Confirm for user actions such as delete
 * Called from the html
 * @param string message
 * @return
 */
function allowSubmit(prompt)
{
  return window.confirm(prompt);
}