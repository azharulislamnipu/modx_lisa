<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listspp_prof1 = new TFI_TableFilter($conn_sigma_modx, "tfi_listspp_prof1");
$tfi_listspp_prof1->addColumn("spp_prof.CUSTOMERID", "NUMERIC_TYPE", "CUSTOMERID", "=");
$tfi_listspp_prof1->addColumn("spp_prof.ARCHOUSAID", "NUMERIC_TYPE", "ARCHOUSAID", "=");
$tfi_listspp_prof1->addColumn("spp_prof.OCCUPATIONID", "NUMERIC_TYPE", "OCCUPATIONID", "=");
$tfi_listspp_prof1->addColumn("spp_prof.TITLEID", "NUMERIC_TYPE", "TITLEID", "=");
$tfi_listspp_prof1->addColumn("spp_prof.COMPANYNAME", "STRING_TYPE", "COMPANYNAME", "%");
$tfi_listspp_prof1->addColumn("spp_prof.COMPANYCITY", "STRING_TYPE", "COMPANYCITY", "%");
$tfi_listspp_prof1->addColumn("spp_prof.COMPANYSTCD", "STRING_TYPE", "COMPANYSTCD", "%");
$tfi_listspp_prof1->addColumn("spp_prof.DESCRIPTION", "STRING_TYPE", "DESCRIPTION", "%");
$tfi_listspp_prof1->addColumn("spp_prof.COMPANYJOBSTART", "DATE_TYPE", "COMPANYJOBSTART", "=");
$tfi_listspp_prof1->addColumn("spp_prof.COMPANYJOBEND", "DATE_TYPE", "COMPANYJOBEND", "=");
$tfi_listspp_prof1->addColumn("spp_prof.CREATEDATE", "DATE_TYPE", "CREATEDATE", "=");
$tfi_listspp_prof1->addColumn("spp_prof.LASTUPDATE", "DATE_TYPE", "LASTUPDATE", "=");
$tfi_listspp_prof1->addColumn("spp_prof.CREATEDBY", "STRING_TYPE", "CREATEDBY", "%");
$tfi_listspp_prof1->addColumn("spp_prof.UPDATEDBY", "STRING_TYPE", "UPDATEDBY", "%");
$tfi_listspp_prof1->Execute();

// Sorter
$tso_listspp_prof1 = new TSO_TableSorter("rsspp_prof1", "tso_listspp_prof1");
$tso_listspp_prof1->addColumn("spp_prof.CUSTOMERID");
$tso_listspp_prof1->addColumn("spp_prof.ARCHOUSAID");
$tso_listspp_prof1->addColumn("spp_prof.OCCUPATIONID");
$tso_listspp_prof1->addColumn("spp_prof.TITLEID");
$tso_listspp_prof1->addColumn("spp_prof.COMPANYNAME");
$tso_listspp_prof1->addColumn("spp_prof.COMPANYCITY");
$tso_listspp_prof1->addColumn("spp_prof.COMPANYSTCD");
$tso_listspp_prof1->addColumn("spp_prof.DESCRIPTION");
$tso_listspp_prof1->addColumn("spp_prof.COMPANYJOBSTART");
$tso_listspp_prof1->addColumn("spp_prof.COMPANYJOBEND");
$tso_listspp_prof1->addColumn("spp_prof.CREATEDATE");
$tso_listspp_prof1->addColumn("spp_prof.LASTUPDATE");
$tso_listspp_prof1->addColumn("spp_prof.CREATEDBY");
$tso_listspp_prof1->addColumn("spp_prof.UPDATEDBY");
$tso_listspp_prof1->setDefault("spp_prof.CUSTOMERID");
$tso_listspp_prof1->Execute();

// Navigation
$nav_listspp_prof1 = new NAV_Regular("nav_listspp_prof1", "rsspp_prof1", "../", $_SERVER['PHP_SELF'], 25);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOccupType = "SELECT * FROM spp_occupation ORDER BY `DESCRIPTION` ASC";
$rsOccupType = mysql_query($query_rsOccupType, $sigma_modx) or die(mysql_error());
$row_rsOccupType = mysql_fetch_assoc($rsOccupType);
$totalRows_rsOccupType = mysql_num_rows($rsOccupType);

//NeXTenesio3 Special List Recordset
$maxRows_rsspp_prof1 = $_SESSION['max_rows_nav_listspp_prof1'];
$pageNum_rsspp_prof1 = 0;
if (isset($_GET['pageNum_rsspp_prof1'])) {
  $pageNum_rsspp_prof1 = $_GET['pageNum_rsspp_prof1'];
}
$startRow_rsspp_prof1 = $pageNum_rsspp_prof1 * $maxRows_rsspp_prof1;

// Defining List Recordset variable
$NXTFilter_rsspp_prof1 = "1=1";
if (isset($_SESSION['filter_tfi_listspp_prof1'])) {
  $NXTFilter_rsspp_prof1 = $_SESSION['filter_tfi_listspp_prof1'];
}
// Defining List Recordset variable
$NXTSort_rsspp_prof1 = "spp_prof.CUSTOMERID";
if (isset($_SESSION['sorter_tso_listspp_prof1'])) {
  $NXTSort_rsspp_prof1 = $_SESSION['sorter_tso_listspp_prof1'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_rsspp_prof1 = "SELECT spp_prof.CUSTOMERID, spp_prof.ARCHOUSAID, spp_prof.OCCUPATIONID, spp_prof.TITLEID, spp_prof.COMPANYNAME, spp_prof.COMPANYCITY, spp_prof.COMPANYSTCD, spp_prof.DESCRIPTION, spp_prof.COMPANYJOBSTART, spp_prof.COMPANYJOBEND, spp_prof.CREATEDATE, spp_prof.LASTUPDATE, spp_prof.CREATEDBY, spp_prof.UPDATEDBY, spp_prof.PROFID FROM spp_prof WHERE {$NXTFilter_rsspp_prof1} ORDER BY {$NXTSort_rsspp_prof1}";
$query_limit_rsspp_prof1 = sprintf("%s LIMIT %d, %d", $query_rsspp_prof1, $startRow_rsspp_prof1, $maxRows_rsspp_prof1);
$rsspp_prof1 = mysql_query($query_limit_rsspp_prof1, $sigma_modx) or die(mysql_error());
$row_rsspp_prof1 = mysql_fetch_assoc($rsspp_prof1);

if (isset($_GET['totalRows_rsspp_prof1'])) {
  $totalRows_rsspp_prof1 = $_GET['totalRows_rsspp_prof1'];
} else {
  $all_rsspp_prof1 = mysql_query($query_rsspp_prof1);
  $totalRows_rsspp_prof1 = mysql_num_rows($all_rsspp_prof1);
}
$totalPages_rsspp_prof1 = ceil($totalRows_rsspp_prof1/$maxRows_rsspp_prof1)-1;
//End NeXTenesio3 Special List Recordset

$nav_listspp_prof1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: false,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_CUSTOMERID {width:140px; overflow:hidden;}
  .KT_col_ARCHOUSAID {width:140px; overflow:hidden;}
  .KT_col_OCCUPATIONID {width:140px; overflow:hidden;}
  .KT_col_TITLEID {width:140px; overflow:hidden;}
  .KT_col_COMPANYNAME {width:140px; overflow:hidden;}
  .KT_col_COMPANYCITY {width:140px; overflow:hidden;}
  .KT_col_COMPANYSTCD {width:140px; overflow:hidden;}
  .KT_col_DESCRIPTION {width:140px; overflow:hidden;}
  .KT_col_COMPANYJOBSTART {width:140px; overflow:hidden;}
  .KT_col_COMPANYJOBEND {width:140px; overflow:hidden;}
  .KT_col_CREATEDATE {width:140px; overflow:hidden;}
  .KT_col_LASTUPDATE {width:140px; overflow:hidden;}
  .KT_col_CREATEDBY {width:140px; overflow:hidden;}
  .KT_col_UPDATEDBY {width:140px; overflow:hidden;}
</style>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">Page Name</h1>
<div class="contentBlock">

  <div class="KT_tng" id="listspp_prof1">
    <h1> Spp_prof
      <?php
  $nav_listspp_prof1->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
    </h1>
    <div class="KT_tnglist">
      <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
        <div class="KT_options"> <a href="<?php echo $nav_listspp_prof1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
          <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listspp_prof1'] == 1) {
?>
            <?php echo $_SESSION['default_max_rows_nav_listspp_prof1']; ?>
            <?php 
  // else Conditional region1
  } else { ?>
            <?php echo NXT_getResource("all"); ?>
            <?php } 
  // endif Conditional region1
?>
              <?php echo NXT_getResource("records"); ?></a> &nbsp;
          &nbsp;
                <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listspp_prof1'] == 1) {
?>
                  <a href="<?php echo $tfi_listspp_prof1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                  <?php 
  // else Conditional region2
  } else { ?>
                  <a href="<?php echo $tfi_listspp_prof1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                  <?php } 
  // endif Conditional region2
?>
        </div>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <thead>
            <tr class="KT_row_order">
              <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>
              </th>
              <th id="CUSTOMERID" class="KT_sorter KT_col_CUSTOMERID <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.CUSTOMERID'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.CUSTOMERID'); ?>">CUSTOMERID</a> </th>
              <th id="ARCHOUSAID" class="KT_sorter KT_col_ARCHOUSAID <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.ARCHOUSAID'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.ARCHOUSAID'); ?>">ARCHOUSAID</a> </th>
              <th id="OCCUPATIONID" class="KT_sorter KT_col_OCCUPATIONID <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.OCCUPATIONID'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.OCCUPATIONID'); ?>">OCCUPATION</a> </th>
              <th id="TITLEID" class="KT_sorter KT_col_TITLEID <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.TITLEID'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.TITLEID'); ?>">TITLE</a> </th>
              <th id="COMPANYNAME" class="KT_sorter KT_col_COMPANYNAME <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.COMPANYNAME'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.COMPANYNAME'); ?>">COMPANY NAME</a> </th>
              <th id="COMPANYCITY" class="KT_sorter KT_col_COMPANYCITY <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.COMPANYCITY'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.COMPANYCITY'); ?>">COMPANY CITY</a> </th>
              <th id="COMPANYSTCD" class="KT_sorter KT_col_COMPANYSTCD <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.COMPANYSTCD'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.COMPANYSTCD'); ?>">COMPANY STate</a> </th>
              <th id="DESCRIPTION" class="KT_sorter KT_col_DESCRIPTION <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.DESCRIPTION'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.DESCRIPTION'); ?>">DESCRIPTION</a> </th>
              <th id="COMPANYJOBSTART" class="KT_sorter KT_col_COMPANYJOBSTART <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.COMPANYJOBSTART'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.COMPANYJOBSTART'); ?>">COMPANY START DATE</a> </th>
              <th id="COMPANYJOBEND" class="KT_sorter KT_col_COMPANYJOBEND <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.COMPANYJOBEND'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.COMPANYJOBEND'); ?>">COMPANY END DATE</a> </th>
              <th id="CREATEDATE" class="KT_sorter KT_col_CREATEDATE <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.CREATEDATE'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.CREATEDATE'); ?>">CREATEDATE</a> </th>
              <th id="LASTUPDATE" class="KT_sorter KT_col_LASTUPDATE <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.LASTUPDATE'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.LASTUPDATE'); ?>">LASTUPDATE</a> </th>
              <th id="CREATEDBY" class="KT_sorter KT_col_CREATEDBY <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.CREATEDBY'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.CREATEDBY'); ?>">CREATEDBY</a> </th>
              <th id="UPDATEDBY" class="KT_sorter KT_col_UPDATEDBY <?php echo $tso_listspp_prof1->getSortIcon('spp_prof.UPDATEDBY'); ?>"> <a href="<?php echo $tso_listspp_prof1->getSortLink('spp_prof.UPDATEDBY'); ?>">UPDATEDBY</a> </th>
              <th>&nbsp;</th>
            </tr>
            <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listspp_prof1'] == 1) {
?>
              <tr class="KT_row_filter">
                <td>&nbsp;</td>
                <td><input type="text" name="tfi_listspp_prof1_CUSTOMERID" id="tfi_listspp_prof1_CUSTOMERID" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_CUSTOMERID']); ?>" size="20" maxlength="100" /></td>
                <td><input type="text" name="tfi_listspp_prof1_ARCHOUSAID" id="tfi_listspp_prof1_ARCHOUSAID" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_ARCHOUSAID']); ?>" size="20" maxlength="100" /></td>
                <td><select name="tfi_listspp_prof1_OCCUPATIONID" id="tfi_listspp_prof1_OCCUPATIONID">
                    <option value="" <?php if (!(strcmp("", @$_SESSION['tfi_listspp_prof1_OCCUPATIONID']))) {echo "SELECTED";} ?>><?php echo NXT_getResource("None"); ?></option>
                    <?php
do {  
?>
                    <option value="<?php echo $row_rsOccupType['OCCUPATIONID']?>"<?php if (!(strcmp($row_rsOccupType['OCCUPATIONID'], @$_SESSION['tfi_listspp_prof1_OCCUPATIONID']))) {echo "SELECTED";} ?>><?php echo $row_rsOccupType['DESCRIPTION']?></option>
                    <?php
} while ($row_rsOccupType = mysql_fetch_assoc($rsOccupType));
  $rows = mysql_num_rows($rsOccupType);
  if($rows > 0) {
      mysql_data_seek($rsOccupType, 0);
	  $row_rsOccupType = mysql_fetch_assoc($rsOccupType);
  }
?>
                  </select>
                </td>
                <td><select name="tfi_listspp_prof1_TITLEID" id="tfi_listspp_prof1_TITLEID">
                    <option value="menuitem1" <?php if (!(strcmp("menuitem1", KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_TITLEID'])))) {echo "SELECTED";} ?>>[ Label ]</option>
                    <option value="menuitem2" <?php if (!(strcmp("menuitem2", KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_TITLEID'])))) {echo "SELECTED";} ?>>[ Label ]</option>
                  </select>
                </td>
                <td><input type="text" name="tfi_listspp_prof1_COMPANYNAME" id="tfi_listspp_prof1_COMPANYNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_COMPANYNAME']); ?>" size="20" maxlength="125" /></td>
                <td><input type="text" name="tfi_listspp_prof1_COMPANYCITY" id="tfi_listspp_prof1_COMPANYCITY" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_COMPANYCITY']); ?>" size="20" maxlength="20" /></td>
                <td><input type="text" name="tfi_listspp_prof1_COMPANYSTCD" id="tfi_listspp_prof1_COMPANYSTCD" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_COMPANYSTCD']); ?>" size="20" maxlength="2" /></td>
                <td><input type="text" name="tfi_listspp_prof1_DESCRIPTION" id="tfi_listspp_prof1_DESCRIPTION" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_DESCRIPTION']); ?>" size="20" maxlength="100" /></td>
                <td><input type="text" name="tfi_listspp_prof1_COMPANYJOBSTART" id="tfi_listspp_prof1_COMPANYJOBSTART" value="<?php echo @$_SESSION['tfi_listspp_prof1_COMPANYJOBSTART']; ?>" size="10" maxlength="22" /></td>
                <td><input type="text" name="tfi_listspp_prof1_COMPANYJOBEND" id="tfi_listspp_prof1_COMPANYJOBEND" value="<?php echo @$_SESSION['tfi_listspp_prof1_COMPANYJOBEND']; ?>" size="10" maxlength="22" /></td>
                <td><input type="text" name="tfi_listspp_prof1_CREATEDATE" id="tfi_listspp_prof1_CREATEDATE" value="<?php echo @$_SESSION['tfi_listspp_prof1_CREATEDATE']; ?>" size="10" maxlength="22" /></td>
                <td><input type="text" name="tfi_listspp_prof1_LASTUPDATE" id="tfi_listspp_prof1_LASTUPDATE" value="<?php echo @$_SESSION['tfi_listspp_prof1_LASTUPDATE']; ?>" size="10" maxlength="22" /></td>
                <td><input type="text" name="tfi_listspp_prof1_CREATEDBY" id="tfi_listspp_prof1_CREATEDBY" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_CREATEDBY']); ?>" size="20" maxlength="20" /></td>
                <td><input type="text" name="tfi_listspp_prof1_UPDATEDBY" id="tfi_listspp_prof1_UPDATEDBY" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listspp_prof1_UPDATEDBY']); ?>" size="20" maxlength="20" /></td>
                <td><input type="submit" name="tfi_listspp_prof1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
              </tr>
              <?php } 
  // endif Conditional region3
?>
          </thead>
          <tbody>
            <?php if ($totalRows_rsspp_prof1 == 0) { // Show if recordset empty ?>
              <tr>
                <td colspan="16"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
              </tr>
              <?php } // Show if recordset empty ?>
            <?php if ($totalRows_rsspp_prof1 > 0) { // Show if recordset not empty ?>
              <?php do { ?>
                <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                  <td><input type="checkbox" name="kt_pk_spp_prof" class="id_checkbox" value="<?php echo $row_rsspp_prof1['PROFID']; ?>" />
                      <input type="hidden" name="PROFID" class="id_field" value="<?php echo $row_rsspp_prof1['PROFID']; ?>" />
                  </td>
                  <td><div class="KT_col_CUSTOMERID"><?php echo KT_FormatForList($row_rsspp_prof1['CUSTOMERID'], 20); ?></div></td>
                  <td><div class="KT_col_ARCHOUSAID"><?php echo KT_FormatForList($row_rsspp_prof1['ARCHOUSAID'], 20); ?></div></td>
                  <td><div class="KT_col_OCCUPATIONID"><?php echo KT_FormatForList($row_rsspp_prof1['OCCUPATIONID'], 20); ?></div></td>
                  <td><div class="KT_col_TITLEID"><?php echo KT_FormatForList($row_rsspp_prof1['TITLEID'], 20); ?></div></td>
                  <td><div class="KT_col_COMPANYNAME"><?php echo KT_FormatForList($row_rsspp_prof1['COMPANYNAME'], 20); ?></div></td>
                  <td><div class="KT_col_COMPANYCITY"><?php echo KT_FormatForList($row_rsspp_prof1['COMPANYCITY'], 20); ?></div></td>
                  <td><div class="KT_col_COMPANYSTCD"><?php echo KT_FormatForList($row_rsspp_prof1['COMPANYSTCD'], 20); ?></div></td>
                  <td><div class="KT_col_DESCRIPTION"><?php echo KT_FormatForList($row_rsspp_prof1['DESCRIPTION'], 20); ?></div></td>
                  <td><div class="KT_col_COMPANYJOBSTART"><?php echo KT_formatDate($row_rsspp_prof1['COMPANYJOBSTART']); ?></div></td>
                  <td><div class="KT_col_COMPANYJOBEND"><?php echo KT_formatDate($row_rsspp_prof1['COMPANYJOBEND']); ?></div></td>
                  <td><div class="KT_col_CREATEDATE"><?php echo KT_formatDate($row_rsspp_prof1['CREATEDATE']); ?></div></td>
                  <td><div class="KT_col_LASTUPDATE"><?php echo KT_formatDate($row_rsspp_prof1['LASTUPDATE']); ?></div></td>
                  <td><div class="KT_col_CREATEDBY"><?php echo KT_FormatForList($row_rsspp_prof1['CREATEDBY'], 20); ?></div></td>
                  <td><div class="KT_col_UPDATEDBY"><?php echo KT_FormatForList($row_rsspp_prof1['UPDATEDBY'], 20); ?></div></td>
                  <td><a class="KT_edit_link" href="profile_career_add.php?PROFID=<?php echo $row_rsspp_prof1['PROFID']; ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
                </tr>
                <?php } while ($row_rsspp_prof1 = mysql_fetch_assoc($rsspp_prof1)); ?>
              <?php } // Show if recordset not empty ?>
          </tbody>
        </table>
        <div class="KT_bottomnav">
          <div>
            <?php
            $nav_listspp_prof1->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
          </div>
        </div>
        <div class="KT_bottombuttons">
          <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
<span>&nbsp;</span>
          <select name="no_new" id="no_new">
            <option value="1">1</option>
            <option value="3">3</option>
            <option value="6">6</option>
          </select>
          <a class="KT_additem_op_link" href="profile_career_add.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsOccupType);

mysql_free_result($rsspp_prof1);
?>
