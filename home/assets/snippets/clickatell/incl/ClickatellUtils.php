<?php

/**
 * Utility classes to support the Clickatell actions
 * @author heardp {http://fullahead.org} 
 */
class ClickatellUtils {
    
    /** Formats a phone number */
    public static function format($number){
        $n = self::cleanPhoneNumber($number);
        $n = self::addLongDistanceInd($n);
        if(self::isValidNumber($n)){
            if(strlen($n) == 11){
                return '(' . substr($n, 1, 3) . ') ' . substr($n, 4, 3) . '-' . substr($n, 7, 4);
            }
        }
        return $number;
    } 
    
    /** Cleans everything but numeric chars out of a phone number */
    public static function cleanPhoneNumber($n=''){
        return preg_replace('/[^0-9]/', '', $n);
    }	

    /** Adds the long distance "1" prefix to phone numbers that don't have it */
    public static function addLongDistanceInd($n=''){
        return strpos($n, '1') === 0 ? $n : '1' . $n;
    }

    /** Valid phone number? */
    public static function isValidNumber($n=''){
        return preg_match('/[0-9]{11,16}/', $n);
    }    
    
}

?>
