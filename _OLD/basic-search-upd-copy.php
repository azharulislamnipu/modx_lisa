<h3>Basic Search</h3>
<form action="/services/spp-directory/lastname_results.php?LASTNAME=<?php echo $_POST['LASTNAME']; ?>" method="get" name="LASTNAME">

    <p><strong>Search by Last Name</strong><br />
        <input id="LASTNAME" name="LASTNAME" type="text" size="20" maxlength="20" />  <input type="submit" class="macButton" name="Submit1" value="Submit"></p>

</form>

<form id="CITY" name="CITY" method="get" action="/services/spp-directory/city_results.php?CITY=<?php echo $_POST['CITY']; ?>">
    <p><strong>Search by City</strong><br>
        <input name="CITY" type="text" id="CITY" size="20" maxlength="20"> <input type="submit" class="macButton" name="Submit2" value="Submit"></p>
</form>


<form class="searchForm" action="/services/spp-directory/boule_results.php?BOULENAME=<?php echo $_POST['BOULENAME']; ?>" method="get" name="BOULENAME">
    <p><strong>Search by Boul&eacute;</strong><br>
        <select name="BOULENAME" id="BOULENAME">
            <option value="">Select a boul&eacute;.</option>
            <option value="Alpha">Alpha (Philadelphia, PA) </option>
            <option value="Beta">Beta (Chicago, IL) </option>
            <option value="Gamma">Gamma (Baltimore, MD) </option>
            <option value="Delta">Delta (Memphis, TN) </option>
            <option value="Epsilon">Epsilon (Washington, DC) </option>
            <option value="Zeta">Zeta (New York, NY) </option>
            <option value="Eta">Eta (St. Louis, MO) </option>
            <option value="Theta">Theta (Kansas City, MO) </option>
            <option value="Iota">Iota (Detroit, MI) </option>
            <option value="Kappa">Kappa (Atlanta, GA) </option>
            <option value="Lambda">Lambda (Columbus, OH) </option>
            <option value="Mu">Mu (Northern NJ, NJ) </option>
            <option value="Nu">Nu (Houston, TX) </option>
            <option value="Xi">Xi (Los Angeles, CA) </option>
            <option value="Omicron">Omicron (Minneapolis/St. Paul, MN) </option>
            <option value="Pi">Pi (Little Rock, AR) </option>
            <option value="Rho">Rho (Pittsburgh, PA) </option>
            <option value="Sigma">Sigma (Dayton, OH) </option>
            <option value="Tau">Tau (Cleveland, OH) </option>
            <option value="Upsilon">Upsilon (Charleston, WV) </option>
            <option value="Phi">Phi (Montgomery, AL) </option>
            <option value="Chi">Chi (Nashville, TN) </option>
            <option value="Psi">Psi (Louisville, KY) </option>
            <option value="Alpha Alpha">Alpha Alpha (New Orleans, LA) </option>
            <option value="Alpha Beta">Alpha Beta (Richmond, VA) </option>
            <option value="Alpha Gamma">Alpha Gamma (Oakland/Berkeley, CA) </option>
            <option value="Alpha Delta">Alpha Delta (Cincinnati, OH) </option>
            <option value="Alpha Epsilon">Alpha Epsilon (Dallas/Ft. Worth, TX) </option>
            <option value="Alpha Zeta">Alpha Zeta (Tallahassee, FL) </option>
            <option value="Alpha Eta">Alpha Eta (Indianapolis, IN) </option>
            <option value="Alpha Theta">Alpha Theta (State of OK, OK) </option>
            <option value="Alpha Iota">Alpha Iota (Columbia, SC) </option>
            <option value="Alpha Kappa">Alpha Kappa (Buffalo, NY) </option>
            <option value="Alpha Lambda">Alpha Lambda (Savannah, GA) </option>
            <option value="Alpha Mu">Alpha Mu (Augusta, GA) </option>
            <option value="Alpha Nu">Alpha Nu (Wichita, KS) </option>
            <option value="Alpha Xi">Alpha Xi (Baton Rouge, LA) </option>
            <option value="Alpha Omicron">Alpha Omicron (Seattle, WA) </option>
            <option value="Alpha Pi">Alpha Pi (San Diego, CA) </option>
            <option value="Alpha Rho">Alpha Rho (Miami, FL) </option>
            <option value="Alpha Sigma">Alpha Sigma (Brooklyn/Long Island, NY) </option>
            <option value="Alpha Tau">Alpha Tau (Durham, NC) </option>
            <option value="Alpha Upsilon">Alpha Upsilon (Bluefield, WV) </option>
            <option value="Alpha Phi">Alpha Phi (Toledo, OH) </option>
            <option value="Alpha Chi">Alpha Chi (Lansing, MI) </option>
            <option value="Alpha Psi">Alpha Psi (Hartford, CT) </option>
            <option value="Beta Alpha">Beta Alpha (Milwaukee, WI) </option>
            <option value="Beta Beta">Beta Beta (Boston, MA) </option>
            <option value="Beta Gamma">Beta Gamma (Jackson, MS) </option>
            <option value="Beta Delta">Beta Delta (Charlotte, NC) </option>
            <option value="Beta Epsilon">Beta Epsilon (Greensboro, NC) </option>
            <option value="Beta Zeta">Beta Zeta (Westchester Co., NY) </option>
            <option value="Beta Eta">Beta Eta (State of DE, DE) </option>
            <option value="Beta Theta">Beta Theta (Knoxville, TN) </option>
            <option value="Beta Iota">Beta Iota (Farmington Valley, CT) </option>
            <option value="Beta Kappa">Beta Kappa (Birmingham, AL) </option>
            <option value="Beta Lambda">Beta Lambda (Hampton/Norfolk, VA) </option>
            <option value="Beta Mu">Beta Mu (Suburban MD, MD) </option>
            <option value="Beta Nu">Beta Nu (Northern VA, VA) </option>
            <option value="Beta Xi">Beta Xi (Central Florida, FL) </option>
            <option value="Beta Omicron">Beta Omicron (Grand Rapids, MI) </option>
            <option value="Beta Pi">Beta Pi (Harrisburg, PA) </option>
            <option value="Beta Rho">Beta Rho (Akron/Kent, OH) </option>
            <option value="Beta Sigma">Beta Sigma (Springfield, MA) </option>
            <option value="Beta Tau">Beta Tau (New Haven, CT) </option>
            <option value="Beta Upsilon">Beta Upsilon (San Francisco, CA) </option>
            <option value="Beta Phi">Beta Phi (Roanoke/Lynchburg, VA) </option>
            <option value="Beta Chi">Beta Chi (Fort Valley, GA) </option>
            <option value="Beta Psi">Beta Psi (Albany, NY) </option>
            <option value="Gamma Alpha">Gamma Alpha (Tucson, AZ) </option>
            <option value="Gamma Beta">Gamma Beta (Jacksonville, FL) </option>
            <option value="Gamma Gamma">Gamma Gamma (Austin, TX) </option>
            <option value="Gamma Delta">Gamma Delta (Flint, MI) </option>
            <option value="Gamma Epsilon">Gamma Epsilon (Sacramento, CA) </option>
            <option value="Gamma Zeta">Gamma Zeta (Pasadena, CA) </option>
            <option value="Gamma Eta">Gamma Eta (Des Moines, IA) </option>
            <option value="Gamma Theta">Gamma Theta (Princess Anne, MD) </option>
            <option value="Gamma Iota">Gamma Iota (Rochester, NY) </option>
            <option value="Gamma Kappa">Gamma Kappa (Winson-Salem, NC) </option>
            <option value="Gamma Lambda">Gamma Lambda (Charleston, SC) </option>
            <option value="Gamma Mu">Gamma Mu (Phoenix, AZ) </option>
            <option value="Gamma Nu">Gamma Nu (Northern Mississippi, MS) </option>
            <option value="Gamma Xi">Gamma Xi (Sarasota, FL) </option>
            <option value="Gamma Omicron">Gamma Omicron (Tampa Bay, FL) </option>
            <option value="Gamma Pi">Gamma Pi (Chattanooga, TN) </option>
            <option value="Gamma Rho">Gamma Rho (Ann Arbor, MI) </option>
            <option value="Gamma Sigma">Gamma Sigma (Raleigh, NC) </option>
            <option value="Gamma Tau">Gamma Tau (Racine/Kenosha, WI) </option>
            <option value="Gamma Upsilon">Gamma Upsilon (Asheville, NC) </option>
            <option value="Gamma Phi">Gamma Phi (San Antonio, TX) </option>
            <option value="Gamma Chi">Gamma Chi (San Jose, CA) </option>
            <option value="Gamma Psi">Gamma Psi (Columbus, GA) </option>
            <option value="Delta Alpha">Delta Alpha (Northern Illinois, IL) </option>
            <option value="Delta Beta">Delta Beta (Greenville, SC) </option>
            <option value="Delta Gamma">Delta Gamma (Tyler, TX) </option>
            <option value="Delta Delta">Delta Delta (Albany, GA) </option>
            <option value="Delta Epsilon">Delta Epsilon (Southern NJ, NJ) </option>
            <option value="Delta Zeta">Delta Zeta (Beaumont, TX) </option>
            <option value="Delta Eta">Delta Eta (Denver, CO) </option>
            <option value="Delta Theta">Delta Theta (Las Vegas, NV) </option>
            <option value="Delta Iota">Delta Iota (Mobile, AL) </option>
            <option value="Delta Kappa">Delta Kappa (Shreveport, LA) </option>
            <option value="Delta Lambda">Delta Lambda (Nassau, BS) </option>
            <option value="Delta Mu">Delta Mu (Fort Worth, TX) </option>
            <option value="Delta Nu">Delta Nu (Fairfield, CT) </option>
            <option value="Delta Xi">Delta Xi (Diamond Bar, CA) </option>
            <option value="Delta Omicron">Delta Omicron (Gulfport, MS) </option>
            <option value="Delta Pi">Delta Pi (Pensacola, FL) </option>
            <option value="Delta Rho">Delta Rho (Anchorage, AK) </option>
            <option value="Delta Sigma">Delta Sigma (S. Suburban Chicago, IL) </option>
            <option value="Delta Tau">Delta Tau (Frankfort, KY) </option>
            <option value="Delta Upsilon">Delta Upsilon (North Atlanta, GA) </option>
            <option value="Delta Phi">Delta Phi (Portland, OR) </option>
            <option value="Delta Chi">Delta Chi (Orangeburg, SC) </option>
            <option value="Delta Psi">Delta Psi (Athens, GA) </option>
            <option value="Epsilon Alpha">Epsilon Alpha (Fayetteville, NC) </option>
            <option value="Epsilon Beta">Epsilon Beta (Contra Costa, CA) </option>
            <option value="Epsilon Gamma">Epsilon Gamma (Chapel Hill, NC) </option>
            <option value="Epsilon Delta">Epsilon Delta (Williamsburg, VA) </option>
            <option value="Epsilon Epsilon">Epsilon Epsilon (Irvine, CA) </option>
            <option value="Epsilon Zeta">Epsilon Zeta (Loudon County, VA) </option>
            <option value="Epsilon Eta">Epsilon Eta (Stockton/Central Valley, CA) </option>
            <option value="Epsilon Theta">Epsilon Theta (Madison, WI) </option>
            <option value="Epsilon Iota">Epsilon Iota (Tulsa, OK) </option>
            <option value="Epsilon Kappa">Epsilon Kappa (Huntsville, AL) </option>
            <option value="Epsilon Lambda">Epsilon Lambda (Palm Beach/Martin/St. Lucie , FL) </option>
            <option value="Epsilon Mu">Epsilon Mu (Hilton Head Island, SC) </option>
            <option value="Epsilon Nu">Epsilon Nu (SW Florida, FL) </option>
            <option value="Epsilon Xi">Epsilon Xi (London, UK) </option>
            <option value="Epsilon Omicron">Epsilon Omicron (North Houston, TX) </option>
            <option value="Epsilon Pi">Epsilon Pi (Southlake, TX) </option>
            <option value="Epsilon Rho">Epsilon Rho (Fredricksburg Region, VA) </option>
            <option value="Epsilon Sigma">Epsilon Sigma (Prince William County, VA) </option>
        </select>
        <input type="submit" class="macButton" name="Submit3" value="Submit"></p>
</form>


<form id="STATECD" name="STATECD" method="get" action="/services/spp-directory/state_results.php?STATECD=<?php echo $_POST['STATECD']; ?>" class="searchForm">
    <p><strong>Search by State </strong><br />
        <select name="STATECD">
            <option value="">Select a state.</option>
            <option value="AL">Alabama</option>
            <option value="AK">Alaska</option>
            <option value="AZ">Arizona</option>
            <option value="AR">Arkansas</option>
            <option value="BS">Bahamas</option>
            <option value="CA">California</option>
            <option value="CO">Colorado</option>
            <option value="CT">Connecticut</option>
            <option value="DE">Delaware</option>
            <option value="DC">District Of Columbia</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="GU">Guam</option>
            <option value="HI">Hawaii</option>
            <option value="ID">Idaho</option>
            <option value="IL">Illinois</option>
            <option value="IN">Indiana</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NV">Nevada</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NM">New Mexico</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="ND">North Dakota</option>
            <option value="OH">Ohio</option>
            <option value="OK">Oklahoma</option>
            <option value="OR">Oregon</option>
            <option value="PW">Palau</option>
            <option value="PA">Pennsylvania</option>
            <option value="PR">Puerto Rico</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="SD">South Dakota</option>
            <option value="TN">Tennessee</option>
            <option value="TX">Texas</option>
            <option value="UT">Utah</option>
            <option value="VT">Vermont</option>
            <option value="VI">Virgin Islands</option>
            <option value="VA">Virginia</option>
            <option value="WA">Washington</option>
            <option value="WV">West Virginia</option>
            <option value="WI">Wisconsin</option>
            <option value="WY">Wyoming</option>
        </select>
        <input type="submit" class="macButton" name="Submit4" value="Submit">
    </p>
</form>
