<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Filter
$tfi_listrsArchonInfo1 = new TFI_TableFilter($sigma_modx, "tfi_listrsArchonInfo1");
$tfi_listrsArchonInfo1->addColumn("STATUS", "STRING_TYPE", "STATUS", "%");
$tfi_listrsArchonInfo1->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listrsArchonInfo1->addColumn("JOINDATE", "DATE_TYPE", "JOINDATE", "=");
$tfi_listrsArchonInfo1->addColumn("LASTUPDATED", "DATE_TYPE", "LASTUPDATED", "=");
$tfi_listrsArchonInfo1->Execute();

// Sorter
$tso_listrsArchonInfo1 = new TSO_TableSorter("rsArchonInfo", "tso_listrsArchonInfo1");
$tso_listrsArchonInfo1->addColumn("STATUS");
$tso_listrsArchonInfo1->addColumn("FULLNAME");
$tso_listrsArchonInfo1->addColumn("JOINDATE");
$tso_listrsArchonInfo1->addColumn("LASTUPDATED");
$tso_listrsArchonInfo1->setDefault("FULLNAME");
$tso_listrsArchonInfo1->Execute();

// Navigation
$nav_listrsArchonInfo1 = new NAV_Regular("nav_listrsArchonInfo1", "rsArchonInfo", "../../", $_SERVER['PHP_SELF'], 20);

// Begin List Recordset
$maxRows_rsArchonInfo = $_SESSION['max_rows_nav_listrsArchonInfo1'];
$pageNum_rsArchonInfo = 0;
if (isset($_GET['pageNum_rsArchonInfo'])) {
  $pageNum_rsArchonInfo = $_GET['pageNum_rsArchonInfo'];
}
$startRow_rsArchonInfo = $pageNum_rsArchonInfo * $maxRows_rsArchonInfo;
$colname__rsArchonInfo = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsArchonInfo = $_SESSION['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter__rsArchonInfo = "1=1";
if (isset($_SESSION['filter_tfi_listrsArchonInfo1'])) {
  $NXTFilter__rsArchonInfo = $_SESSION['filter_tfi_listrsArchonInfo1'];
}
// Defining List Recordset variable
$NXTSort__rsArchonInfo = "FULLNAME";
if (isset($_SESSION['sorter_tso_listrsArchonInfo1'])) {
  $NXTSort__rsArchonInfo = $_SESSION['sorter_tso_listrsArchonInfo1'];
}
$query_rsArchonInfo = sprintf("SELECT * FROM vw_gramm_archons WHERE BOULENAME = %s AND  {$NXTFilter__rsArchonInfo}  ORDER BY  {$NXTSort__rsArchonInfo} ASC ", GetSQLValueString($colname__rsArchonInfo, "text"));
$rsArchonInfo = $sigma_modx->SelectLimit($query_rsArchonInfo, $maxRows_rsArchonInfo, $startRow_rsArchonInfo) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsArchonInfo'])) {
  $totalRows_rsArchonInfo = $_GET['totalRows_rsArchonInfo'];
} else {
  $all_rsArchonInfo = $sigma_modx->SelectLimit($query_rsArchonInfo) or die($sigma_modx->ErrorMsg());
  $totalRows_rsArchonInfo = $all_rsArchonInfo->RecordCount();
}
$totalPages_rsArchonInfo = (int)(($totalRows_rsArchonInfo-1)/$maxRows_rsArchonInfo);
// End List Recordset

$nav_listrsArchonInfo1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_STATUS {width:126px; overflow:hidden;}
  .KT_col_FULLNAME {width:260px; overflow:hidden;}
  .KT_col_JOINDATE {width:70px; overflow:hidden;}
  .KT_col_LASTUPDATED {width:70px; overflow:hidden;}
.style1 {
	font-size: 14px;
	margin: 4px 0;
}
.style2 {
	font-size: 12px;
}
</style>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listrsArchonInfo1">
  <h1> Quick Roster  | Displaying 
    <?php
  $nav_listrsArchonInfo1->Prepare();
  require("../../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listrsArchonInfo1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
            <?php 
  // Show IF Conditional region1 
  if (@$_GET['show_all_nav_listrsArchonInfo1'] == 1) {
?>
              <?php echo $_SESSION['default_max_rows_nav_listrsArchonInfo1']; ?>
              <?php 
  // else Conditional region1
  } else { ?>
              <?php echo NXT_getResource("all"); ?>
              <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                            <?php 
  // Show IF Conditional region2 
  if (@$_SESSION['has_filter_tfi_listrsArchonInfo1'] == 1) {
?>
                              <a href="<?php echo $tfi_listrsArchonInfo1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                              <?php 
  // else Conditional region2
  } else { ?>
                              <a href="<?php echo $tfi_listrsArchonInfo1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                              <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>
            </th>
            <th id="STATUS" class="KT_sorter KT_col_STATUS <?php echo $tso_listrsArchonInfo1->getSortIcon('STATUS'); ?>"> <a href="<?php echo $tso_listrsArchonInfo1->getSortLink('STATUS'); ?>">STATUS</a> </th>
            <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listrsArchonInfo1->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listrsArchonInfo1->getSortLink('FULLNAME'); ?>">FULLNAME</a> </th>
            <th id="JOINDATE" class="KT_sorter KT_col_JOINDATE <?php echo $tso_listrsArchonInfo1->getSortIcon('JOINDATE'); ?>"> <a href="<?php echo $tso_listrsArchonInfo1->getSortLink('JOINDATE'); ?>">JOIN DATE</a> </th>
            <th id="LASTUPDATED" class="KT_sorter KT_col_LASTUPDATED <?php echo $tso_listrsArchonInfo1->getSortIcon('LASTUPDATED'); ?>"> <a href="<?php echo $tso_listrsArchonInfo1->getSortLink('LASTUPDATED'); ?>">LAST UPDATED</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3 
  if (@$_SESSION['has_filter_tfi_listrsArchonInfo1'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listrsArchonInfo1_STATUS" id="tfi_listrsArchonInfo1_STATUS" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchonInfo1_STATUS']); ?>" size="18" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsArchonInfo1_FULLNAME" id="tfi_listrsArchonInfo1_FULLNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchonInfo1_FULLNAME']); ?>" size="30" maxlength="20" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="submit" name="tfi_listrsArchonInfo1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsArchonInfo == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="6"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsArchonInfo > 0) { // Show if recordset not empty ?>
            <?php
  while (!$rsArchonInfo->EOF) { 
?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_vw_gramm_archons" class="id_checkbox" value="<?php echo $rsArchonInfo->Fields('CUSTOMERID'); ?>" />
                    <input type="hidden" name="CUSTOMERID" class="id_field" value="<?php echo $rsArchonInfo->Fields('CUSTOMERID'); ?>" />
                </td>
                <td><div class="style2"><?php echo KT_FormatForList($rsArchonInfo->Fields('STATUS'), 18); ?></div></td>
                <td><div class="style1"><?php echo $rsArchonInfo->Fields('L2R_FULLNAME'); ?></div></td>
                <td><div class="KT_col_JOINDATE"><?php echo KT_formatDate($rsArchonInfo->Fields('JOINDATE')); ?></div></td>
                <td><div class="KT_col_LASTUPDATED"><?php echo KT_formatDate($rsArchonInfo->Fields('LASTUPDATED')); ?></div></td>
                <td><a class="KT_edit_link" href="home/edit_archon.php?CUSTOMERID=<?php echo $rsArchonInfo->Fields('CUSTOMERID'); ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
              </tr>
              <?php
    $rsArchonInfo->MoveNext(); 
  }
?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsArchonInfo1->Prepare();
            require("../../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
        <span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
          <option value="3">3</option>
          <option value="6">6</option>
        </select>
        <a class="KT_additem_op_link" href="home/edit_archon.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
  <!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchonInfo->Close();
?>
