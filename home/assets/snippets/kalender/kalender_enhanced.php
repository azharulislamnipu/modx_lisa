<?php
/*
*
* "KALENDER"
* ----------
* 
* A tiny calendar app for MODx.
*  
* Created by: Danny van Ommen (dannyvanommen@gmail.com)
* Created on: November 27, 2006
*
* Modified by: Wim Beerens (info@webesite.nl)
* Modified on: April 5, 2007
*
* Enhanced by: Joey Livingston (tiltedsymmetry.com)
* Enhanced on: December 8, 2007
*
* Added some parameters for more flexible use
* Added internationalisation
*
* Version: 0.2
* 
* 
*
* Credits
* ------- 
*  
* As with all calendar apps, this one isn't the most original so credit goes to 
* where credit is due.
* 
* 1) The idea for this snippet came out of EasyPHPCalendar.
*    (http://www.easyphpcalendar.com)
*    
* 2) Also, Kalender extensively uses Erik Bosrup's "overLib" Javascript library
*   (http://www.bosrup.com/web/overlib) 
* 
* License
* -------
* 
* GNU GPU
* 
*/

// snippet parameters

// $parent [ folder id ]
(isset($parent)) ? $parent : $parent = "-1";

// $theme [ gold (default) ]
(isset($theme)) ? $theme : $theme = "gold_enhanced";

// $mm [ month ]
(isset($mm)) ? $mm : $mm = date("n");

// $yyyy [ year ]
(isset($yyyy)) ? $yyyy : $yyyy = date("Y");

// $lang [ language ]
(isset($lang)) ? $lang : $lang = "en";

// $dateFormat [ mdy (default) | dmy ]
(isset($dateFormat)) ? $dateFormat : $dateFormat = "mdy";

// $dayLen [ 1 (default) | 2 | 3 ]
(isset($dayLen)) ? $dayLen: $dayLen = "1";

// $showInfo [ intro | content | both (default) ]
(isset($showInfo)) ? $showInfo : $showInfo = "both";

// config

$base_path = $modx->config['base_path'];
$rb_base_dir = $modx->config['rb_base_dir'];
$assets_path = substr($rb_base_dir, (strlen($base_path)));

// if Kalender is installed in a different directory, change it here
$pathPrefix = $assets_path."snippets/kalender";

$themePath = $pathPrefix."/themes/".$theme."/";
$includePath = $pathPrefix."/themes/".$theme."/";

$cssTheme = $includePath."theme.css";

$jsOverLib = $pathPrefix."/js/overlib.js";

$url_prefix = $_SERVER['REDIRECT_URL']."?";

$dbase = $modx->dbConfig['dbase'];

$tbl_prefix = $modx->dbConfig['table_prefix'];	

$dbase_tbl = $dbase.".".$tbl_prefix;

$site_content = $dbase_tbl."site_content";
$site_tvs = $dbase_tbl."site_tmplvars";
$site_tv_content = $dbase_tbl."site_tmplvar_contentvalues";

// include theme settings
	
include($includePath."theme.php");

// internationalization
$locale = $lang.'_'.strtoupper($lang);
setlocale (LC_TIME, $locale);

// number of characters of daynames more than 3: default to 1
if($dayLen > 3) {
	$dayLen = 1;
}

// default month and year as of today

if(is_numeric($_GET["mo"]) && $_GET["mo"]!=""){
	$mm = $_GET["mo"];
}

if(is_numeric($_GET["yr"]) && $_GET["yr"]!=""){
	$yyyy = $_GET["yr"];
}

//if(!is_numeric($mm)) $mm = date("n");
//if(!is_numeric($yyyy)) $yyyy = date("Y");
	
$month = strftime("%B",strtotime($mm."/1/".$yyyy));
$maxdays = date("t",strtotime($mm."/1/".$yyyy));
	
$prevMonth_mm = $mm - 1;
if($prevMonth_mm==0) $prevMonth_mm = 12;
	
$prevMonth_yyyy = $yyyy;
if($prevMonth_mm==12) $prevMonth_yyyy = $yyyy-1;
			
$prevMonth_maxdays = date("t",strtotime($prevMonth_mm."/1/".$yyyy));
	
$prevMonth_d = 0;
	
$nextMonth_mm = $mm + 1;
if($nextMonth_mm==13) $nextMonth_mm = 1;
	
$nextMonth_yyyy = $yyyy;
if($nextMonth_mm==1) $nextMonth_yyyy = $yyyy+1;
	
$nextMonth_maxdays = date("t",strtotime($nextMonth_mm."/1/".$yyyy));
	
$nextMonth_d = 0;

// get calendar events from modx content database for the selected month

$strtotime_month_start = strtotime($mm."/1/".$yyyy." 00:00:00");
$strtotime_month_end = strtotime($mm."/".$maxdays."/".$yyyy." 23:59:59");

// get parent where clause to dig and get all non-folder documents within sub-folders of the parent folder
$parentWhere[] = $parent;
$parentWhere_tmp[] = $parent;
$parent_count = 0;

do{
	if(is_array($childParentWhere_tmp)){
		$parentWhere_tmp = $childParentWhere_tmp;
		unset($childParentWhere_tmp);
	}
	else{
		$parent_count = 0;		
	}
	
	foreach($parentWhere_tmp as $p){
		$sql = "SELECT ".$site_content.".id"
			." FROM ".$site_content
			." WHERE ".$site_content.".parent=".$p
			." AND ".$site_content.".isfolder=1;";
		
		$rs = $modx->dbQuery($sql);
		$parent_count = $modx->recordCount($rs);
		
		for($i=0 ; $i<$parent_count ; $i++){
			$row = $modx->fetchRow($rs);
			$parentWhere[] = $row["id"];
			$childParentWhere_tmp[] = $row["id"];
		}
	}
} while ($parent_count > 0);

$parentWhere_str = "";
foreach($parentWhere as $p){
	if($parentWhere_str!="") $parentWhere_str .= " OR";
	$parentWhere_str .= " ".$site_content.".parent=".$p;
}

$sql = "SELECT "
	.$site_content.".id"
	.",".$site_content.".pagetitle"
	.",".$site_content.".longtitle"	
	.",".$site_content.".menutitle"
	.",".$site_content.".description";
if(($showInfo=="intro") or ($showInfo=="both")){
	$sql = $sql.",".$site_content.".introtext";
        };
if(($showInfo=="content") or ($showInfo=="both")){
	$sql = $sql.",".$site_content.".content";
        };
        $sql = $sql
	.",".$site_content.".menuindex"		
	." FROM "
	.$site_content
	." WHERE"
	." (".$parentWhere_str.")"		
	
	." ORDER BY ".$site_content.".menuindex"	
	." ASC";
	
$rs = $modx->dbQuery($sql);	
$rs_count = $modx->recordCount($rs);
$snippetNameArray = array();

for ($i = 0; $i < $rs_count; $i++){
	$row = $modx->fetchRow($rs);
	
	$tvs = $modx->getTemplateVarOutput('*',$row['id']);
	
	$row['StartTime'] = $tvs['StartTime'];
	$row['EndTime'] = $tvs['EndTime'];
	
	if(($row['StartTime'] >= $strtotime_month_start && $row['StartTime'] <= $strtotime_month_end) || ($row['EndTime'] >= $strtotime_month_start && $row['EndTime'] <= $strtotime_month_end)) {
		if($row["StartTime"]>=$strtotime_month_start && $row["StartTime"]<=$strtotime_month_end){
			$d = date("j",$row["StartTime"]);
			$monthEventsArray[$d][] = $row;		
		}

		if($row["EndTime"]>0){
			$unpub_strtotime_difference = $row["EndTime"] - $row["StartTime"];
			if($unpub_strtotime_difference>0){
				if($row["StartTime"]>=$strtotime_month_start && $row["StartTime"]<=$strtotime_month_end){
					$unpub_days_difference = round($unpub_strtotime_difference/(60*60*24));				
					$unpub_last_day = intval($d + $unpub_days_difference);
					$unpub_j = $d+1;
				}
				else if($row["StartTime"]<$strtotime_month_start){
					$unpub_j = 1;
					$unpub_last_day = date("j",$row["EndTime"]);				
				}

				if($unpub_last_day > $maxdays){
					$unpub_last_day = $maxdays;
				}

				for($j=$unpub_j ; $j<=$unpub_last_day ; $j++){
					$monthEventsArray[$j][] = $row;
				}			
			}
		}
	}	
}

?>

<link type="text/css" rel="stylesheet" href="<? echo $cssTheme; ?>" />

<script type="text/javascript" src="<? echo $jsOverLib; ?>"><!-- overLIB (c) Erik Bosrup --></script>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<table border="0" cellpadding="2" cellspacing="1" class="kalender-menu">
<tr>
	<td align="right">
		<table border="0" cellpadding="0" cellspacing="0" align="right">
		<tr>
			<td align="right">
				<div class="kalender-previous"><a href="<? echo $url_prefix; ?>mo=<? echo $prevMonth_mm; ?>&yr=<? echo $prevMonth_yyyy; ?>"><img src="<? echo $themePath; ?>images/previous.gif" border="0" width="15" height="14" onMouseOver="this.src='<? echo $themePath; ?>images/previous_over.gif';" onMouseOut="this.src='<? echo $themePath; ?>images/previous.gif';" style="cursor:pointer;"></a></div>
			</td>
			<td align="left">
				<div class="kalender-next"><a href="<? echo $url_prefix; ?>mo=<? echo $nextMonth_mm; ?>&yr=<? echo $nextMonth_yyyy; ?>"><img src="<? echo $themePath; ?>images/next.gif" border="0" width="15" height="14" onMouseOver="this.src='<? echo $themePath; ?>images/next_over.gif';" onMouseOut="this.src='<? echo $themePath; ?>images/next.gif';" style="cursor:pointer;"></a></div>
			</td>				
		</tr>
		</table>
	</td>
</tr>
</table>	
<table border="0" cellpadding="2" cellspacing="1" class="kalender">
	<tr>
		<td colspan="7" align="center" class="kalender-month">
			<? echo utf8_encode($month).' '.$yyyy; ?>
                </td>
	</tr>
        <?
	$maxdays = date("t",strtotime($mm."/1/".$yyyy));
	$firstday = date("w",strtotime($mm."/1/".$yyyy));
        $difdays = 8 - $firstday;
        ?>
	<tr>
                <?
		for($count = 0;$count<=6;$count++){
			$tmpday = $difdays + $count;
	        	$showday = strftime("%a",strtotime($mm."/".$tmpday."/".$yyyy));
                ?>
			<td nowrap align="center" class="kalender-week">
                        	<? echo substr($showday, 0, $dayLen); ?>
			</td>
		<?
		} // end for $count
		?>
	</tr>
	<?
		if($firstday==6 && $maxdays>=30){
			// six rows when first day is saturday and max days is 30 or 31		
			$weekrows = 6;
		}
		else if($firstday==0 && $maxdays==28){
			// four rows when first day is sunday and max days is 28
			$weekrows = 4;
		}
		else{
			// all other months have 5 calendar rows
			$weekrows = 5;
		}
		
		$d = 0;		
		$highlightGroup = 1;
		$changeHighlight = true;
		
		for($row=0 ; $row<$weekrows ; $row++){
			?>
			<tr>
			<?
				// calendar details
				
				// week
				for($w=0 ; $w<=6 ; $w++){
					$event = "";
					$html = "";					
					
					if($row==0 && $d==0 && $w==$firstday) $d = 1;					
					else if($d>0) $d++;
					
					if($d>0 && $d<=$maxdays){
						$dd = $d;												
					}
					else{
						if($d==0){
							// calculate previous month							
							if($prevMonth_d==0){
								$prevMonth_d = ($prevMonth_maxdays - $firstday) + 1;
							}
							else{
								$prevMonth_d++;
							}
							$dd = $prevMonth_d;
						}
						else if($d>$maxdays){
							// calculate next month
							$nextMonth_d++;
							$dd = $nextMonth_d;
						}
						else{
							$dd = "";							
						}
					}
					

					if($d>0 && $d<=$maxdays){
						if(is_array($monthEventsArray) && array_key_exists($d,$monthEventsArray)){
							$changeHighlight = true;
							if(is_array($yesterdayIDArray)){
								foreach($monthEventsArray[$d] as $monthEvent){
									if(in_array($monthEvent["id"],$yesterdayIDArray)){
										$changeHighlight = false;
									}									
								}
								unset($yesterdayIDArray);
							}
							if($changeHighlight==true){
								$highlightGroup++;
								if($highlightGroup>3) $highlightGroup = 1;
							}
							
							$eventHighlightClass = "kalender-event-highlight_".$highlightGroup;
							
							$thisMonthsEventsArray = $monthEventsArray[$d];
							/* sort by hour							
							$thisMonthsEventsArray = array();
							$e = 0;
							foreach($monthEventsArray[$d] as $monthEvent){
								$thisMonthsEventsArray[$monthEvent["StartTime"]."_".$e] = $monthEvent;
								$e++;
							}
							ksort($thisMonthsEventsArray);
							*/
							
							$html .= "<ul>";
							
							foreach($thisMonthsEventsArray as $monthEvent){
								$yesterdayIDArray[] = $monthEvent["id"];
								
								$eventRowClass = "kalender-event-row_1";
								
								
								
								$tooltipTable = "<table cellpadding=0 cellspacing=0 width=100%>";
								$tooltipTable.= "<tr><td align=\'center\' valign=\'middle\' class=\'kalender-tooltip-day\'>";
								                                    if($dateFormat == "mdy") {
				                $tooltipTable.= strftime("%A, %b. %d",strtotime($mm."/".$d."/".$yyyy)).", ".$yyyy;
				                                    } else {
				                $tooltipTable.= strftime("%A %d %b.",strtotime($mm."/".$d."/".$yyyy))." ".$yyyy;
				                                    }

								$yesterdayIDArray[] = $monthEvent["id"];

								$toolTipRowClass = "kalender-tooltip-row_1";

								$tooltipTable.= "<tr><td align=\'left\' valign=\'top\' class=\'".$eventHighlightClass."\'>";
								$tooltipTable.= addslashes(str_replace('"',"'",$monthEvent["longtitle"]));
								$tooltipTable.= "</td></tr>";

								$eventTime = "";								
								if(date("H:i",$monthEvent["StartTime"])!="00:00"){
									$eventTime = date("h:ia",$monthEvent["StartTime"]);
									if($monthEvent["EndTime"]!=0){
										if(date("m-d-Y",$monthEvent["StartTime"])==date("m-d-Y",$monthEvent["EndTime"])){
											if($monthEvent["EndTime"]>$monthEvent["StartTime"]){
												if(date("H:i",$monthEvent["EndTime"])!="00:00"){
													$eventTime .= " to ";
													$eventTime .= date("h:ia",$monthEvent["EndTime"]);												
												}
											}
										}
									}									
									$tooltipTable.= "<tr><td align=\'left\' valign=\'top\' class=\'".$toolTipRowClass."\'>";
									$tooltipTable.= addslashes(str_replace('"',"'",$eventTime));
									$tooltipTable.= "</td></tr>";	

									if($toolTipRowClass=="kalender-tooltip-row_1") $toolTipRowClass = "kalender-tooltip-row_2";
									else $toolTipRowClass = "kalender-tooltip-row_1";
								}

								if($monthEvent["description"]!=""){
									$tooltipTable.= "<tr><td align=\'left\' valign=\'top\' class=\'".$toolTipRowClass."\'>";
									$tooltipTable.= addslashes(str_replace('"',"'",preg_replace('/\r|\n/','',$monthEvent["description"])));
									$tooltipTable.= "</td></tr>";

									if($toolTipRowClass=="kalender-tooltip-row_1") $toolTipRowClass = "kalender-tooltip-row_2";
									else $toolTipRowClass = "kalender-tooltip-row_1";									
								}
								
								if($monthEvent["introtext"]!=""){
									$tooltipTable.= "<tr><td align=\'left\' valign=\'top\' class=\'".$toolTipRowClass."\'>";
									$tooltipTable.= addslashes(str_replace('"',"'",preg_replace('/\r|\n/','',$monthEvent["introtext"])));
									$tooltipTable.= "</td></tr>";
									
									if($toolTipRowClass=="kalender-tooltip-row_1") $toolTipRowClass = "kalender-tooltip-row_2";
									else $toolTipRowClass = "kalender-tooltip-row_1";									
								}		
								
								if($monthEvent["content"]!=""){
									$tooltipTable.= "<tr><td align=\'left\' valign=\'top\' class=\'".$toolTipRowClass."\'>";
									$tooltipTable.= addslashes(str_replace('"',"'",preg_replace('/\r|\n/','',$monthEvent["content"])));
									$tooltipTable.= "</td></tr>";
									
									if($toolTipRowClass=="kalender-tooltip-row_1") $toolTipRowClass = "kalender-tooltip-row_2";
									else $toolTipRowClass = "kalender-tooltip-row_1";									
								}						

								$tooltipTable.= "<tr><td class=\'kalender-tooltip-row_shadow\'></td></tr>";

								$tooltipTable.= "</td></tr></table>";
								$overlib_over = "return overlib('".$tooltipTable."', CENTER, OFFSETY, 18);";
								$overlib_out = "return nd();";
								$overlib_click = "";

								// current month
								if($w==0 || $w==6){
									// weekends
									$cssClass = "kalender-dayWeekendHighlight_".$highlightGroup;
									$td_click = "";
								}
								else{
									// weekday
									$cssClass = "kalender-dayHighlight_".$highlightGroup;

									$td_click = "";								
								}

								$tooltip = "onMouseOver=\"".$td_over." ".$overlib_over."\"";
								$tooltip.= " onMouseOut=\"".$td_out." ".$overlib_out."\"";
								$tooltip.= " onMouseClick=\"".$td_click." ".$overlib_click."\"";
								
								
								$html.= '<li '.$tooltip.'>';								
								
								if($monthEvent["menutitle"]!=""){
									$html.= "<a href='[~{$monthEvent['id']}~]'>".addslashes(str_replace('"',"'",$monthEvent["menutitle"])).'</a>';
									
									if($eventRowClass=="kalender-event-row_1") $eventTipRowClass = "kalender-event-row_2";
									else $eventRowClass = "kalender-event-row_1";									
								}										
								
								$html.= "</li>";
							}
							
							$html.= "</ul>";
							
							// current month
							if($w==0 || $w==6){
								// weekends
								$cssClass = "kalender-dayWeekendHighlight_".$highlightGroup;
							}
							else{
								// weekday
								$cssClass = "kalender-dayHighlight_".$highlightGroup;							
							}
							
							$event = $html;
						}
						else{
							// current month
							if($w==0 || $w==6){
								// weekends
								$cssClass = "kalender-dayWeekend";
							}
							else{
								// default day
								$cssClass = "kalender-dayDefault";
							}
							$event = "";
						}
					}
					else{
						// previous months
						$cssClass = "kalender-dayDim";
					}
					
					?>
						<td nowrap align="left" class="<? echo $cssClass; ?>">
							<? echo $dd; ?><? echo $event; ?></td>								
					<?					

				}
			?>
			</tr>
			<?
		}
		?>
	</table>
	<?
		$html_jump = "";
		$html_jump .= "<table border=\'0\' cellpadding=\'2\' cellspacing=\'1\' width=\'100%\'>";

		$jump_rows = 7;

		for($i=($jump_rows-1) ; $i>=0 ; $i--){
			$minus = $jump_rows - round($jump_rows/2) - $i;
			
			$jump_rows_yyyy = $yyyy;			
			$jump_rows_mm = $mm + $minus;
			if($minus > 0 && $jump_rows_mm>=13){
				$jump_rows_mm = $jump_rows_mm - 12;
				$jump_rows_yyyy = $yyyy + 1;				
			}
			if($minus < 0 && $jump_rows_mm<=0){
				$jump_rows_mm = 12 + $jump_rows_mm;
				$jump_rows_yyyy = $yyyy-1;								
			}
			
			if($minus==0){
				$td_class = "kalender-jump-current-row";
			}
			else{
				$td_class = "kalender-jump-row";				
			}
			
			$html_jump .= "<tr>";
			$html_jump .= "<td class=\'".$td_class."\' align=\'left\' onMouseOver=".$td_over." onMouseOut=".$td_out.">";
			$html_jump .= "<a class=\'kalender-jump-row-text\' href=\'".$url_prefix."mo=".$jump_rows_mm."&yr=".$jump_rows_yyyy."\'>";
			$html_jump .= utf8_encode(strftime("%B %Y",strtotime($jump_rows_mm."/1/".$jump_rows_yyyy)));
			$html_jump .= "</a>";
			$html_jump .= "</td>";
			$html_jump .= "</tr>";			
		}

		$html_jump .= "</table>";
		$tooltip_jump = "onClick=\"return overlib('".$html_jump."', STICKY, NOCLOSE, ABOVE, HEIGHT, 112, WIDTH, 120, CENTER);\"";
		$tooltip_jump.= " onMouseOut=\"nd();\"";
	?>
	<table border="0" cellpadding="2" cellspacing="1" class="kalender-menu">
	<tr>
		<td align="left">
			<table border="0" cellpadding="0" cellspacing="0" align="left">
			<tr>
				<td align="right">
					<div class="kalender-previous"><a href="<? echo $url_prefix; ?>mo=<? echo $prevMonth_mm; ?>&yr=<? echo $prevMonth_yyyy; ?>"><img src="<? echo $themePath; ?>images/previous.gif" border="0" width="15" height="14" onMouseOver="this.src='<? echo $themePath; ?>images/previous_over.gif';" onMouseOut="this.src='<? echo $themePath; ?>images/previous.gif';" style="cursor:pointer;"></a></div>
				</td>
				<td align="left">
					<div class="kalender-next"><a href="<? echo $url_prefix; ?>mo=<? echo $nextMonth_mm; ?>&yr=<? echo $nextMonth_yyyy; ?>"><img src="<? echo $themePath; ?>images/next.gif" border="0" width="15" height="14" onMouseOver="this.src='<? echo $themePath; ?>images/next_over.gif';" onMouseOut="this.src='<? echo $themePath; ?>images/next.gif';" style="cursor:pointer;"></a></div>
				</td>				
			</tr>
			</table>		
		</td>
		<td align="center">
			<a href="<? echo $url_prefix; ?>mo=<? echo date("n"); ?>&yr=<? echo date("Y"); ?>" class="kalender-jump-link"><? echo strftime("%B %Y"); ?></a></td>
		<td align="right">
			<img src="<? echo $themePath."images/jump.gif"; ?>" border="0" width="15" height="14" align="right" onMouseOver="this.src='<? echo $themePath."images/jump_over.gif"; ?>';" onMouseOut="this.src='<? echo $themePath."images/jump.gif"; ?>';" <? echo $tooltip_jump; ?>></td>			
	</tr>
	</table>

<?
?>