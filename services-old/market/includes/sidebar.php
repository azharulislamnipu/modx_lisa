<?php
////////////////////////////////////////////////////////////
//Sidebar generator
////////////////////////////////////////////////////////////

function getSideBar($beg,$end){//generates the sidebar reading from the config.php
	$widgets=explode(",",SIDEBAR);
	foreach ($widgets as $widget){
		$widget="sb_".$widget;
		echo $widget($beg,$end);
	}
}

//////////////////////////////////////////////////////
//Side bar functions. ALL OF THEM MUST START ON sb_FUNCTION_NAME, to add them in the config file just write FUNCTION_NAME,
/////////////////////////////////////////////////////

function sb_new($beg,$end){//add new
	return $beg.'<b><a title="'.T_NEW_ITEM.'" href="'.SITE_URL.newURL().'">'.T_NEW_ITEM.'</a></b>'.$end;
}
////////////////////////////////////////////////////////////
function sb_search($beg,$end){//serach form
	global $categoryName,$idCategory,$currentCategory,$type;
		if (cG("s")=="") $ws=T_SEARCH."...";
		else $ws=cG("s");
		$search= "<form method=\"get\" action=\"".SITE_URL."\">
			<input name=\"s\" id=\"s\" maxlength=\"15\" title=\"".T_SEARCH."\"
				onblur=\"this.value=(this.value=='') ? '$ws' : this.value;\" 
				onfocus=\"this.value=(this.value=='$ws') ? '' : this.value;\" 
				value=\"$ws\" type=\"text\" />";
		
		if(isset($categoryName)) $search.='<input type="hidden" name="category" value="'.$currentCategory.'" />';
			
		$search.='</form>';
		
		if(FRIENDLY_URL) $search.='<a href="'.SITE_URL.'/'.u(T_ADV_SEARCH).'.htm">'.T_ADV_SEARCH.'</a>';
		else $search.='<a href="'.SITE_URL.'/content/search.php">'.T_ADV_SEARCH.'</a>';
		      		
	return $beg.$search.$end;
}
////////////////////////////////////////////////////////////
function sb_infolinks($beg,$end){//site stats info and tools linsk rss map..
    $info.= '<b>'.T_TOTAL_ADS.':</b> '.totalAds($idCategory).SEPARATOR
		.' <b>'.T_VIEWS.':</b> '.totalViews($idCategory).SEPARATOR
		.' <b><a href="'.SITE_URL.'/rss/?category='.$currentCategory.'&amp;type='.$type.'">RSS</a></b>';
		 if (MAP_KEY!="") $info.=SEPARATOR.'<b><a href="'.SITE_URL.'/'.mapURL().'?category='.$currentCategory.'&amp;type='.$type.'">'.T_MAP.'</a></b>';
   return $beg.$info.$end;
}
////////////////////////////////////////////////////////////
function sb_categories($beg,$end){// popular categories
	global $categoryName;
	if(!isset($categoryName)){ 
		echo $beg."<b>".T_CATEGORY_CLOUD."</b><br />";
		generateTagPopularCategories();
		echo $end;
	}
}
////////////////////////////////////////////////////////////
function sb_donate($beg,$end){//donation
	return $beg.'<b>'.T_RECOMMENDED.'</b><br />Please donate to help developing this software. No matter how much, even small amounts are very welcome.
<form action="https://www.paypal.com/cgi-bin/webscr" method="post"> 
<input type="hidden" name="cmd" value="_s-xclick"> 
<input type="hidden" name="hosted_button_id" value="6070590"> 
<input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online."> 
<img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1"> 
</form> Thanks. <br /><br /> To erase this please go to: Admin->Settings->Look and Feel->Widget Sidebar.'.$end;
}
////////////////////////////////////////////////////////////
function sb_advertisement($beg,$end){//advertisement
	return $beg.ADVERT.$end;
}
////////////////////////////////////////////////////////////
function sb_popular($beg,$end){//popular items
	if (COUNT_POSTS){
		global $categoryName,$idCategory;
		$ret=$beg."<b>".T_POPULAR_ITEMS." $categoryName:</b>";
		$ret.=generatePopularItems(7,5,$idCategory);
		$ret.="*".T_LAST_WEEK.$end;
		return $ret;
	}
}
////////////////////////////////////////////////////////////
function sb_item_tools($beg,$end){//utils for admin
	global $idItem,$itemPassword;
	if(isset($idItem)&&isset($_SESSION['admin'])){
		echo $beg;?>
		<b><?php echo T_ITEM_UTILS;?>:</b>
		<ul>
			<li><a href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idItem;?>&amp;pwd=<?php echo $itemPassword;?>&amp;action=edit">
				<?php echo T_EDIT;?></a>
			</li>
			<li><a onClick="return confirm('<?php echo T_DEACTIVATE;?>?');" 
				href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idItem;?>&amp;pwd=<?php echo $itemPassword;?>&amp;action=deactivate">
				<?php echo T_DEACTIVATE;?></a>
			</li>
			<li>	<a onClick="return confirm('<?php echo T_SPAM;?>?');"
					href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idItem;?>&amp;pwd=<?php echo $itemPassword;?>&amp;action=spam">
						<?php echo T_SPAM;?></a>
			</li>
			<li><a onClick="return confirm('<?php echo T_DELETE;?>?');"
				href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idItem;?>&amp;pwd=<?php echo $itemPassword;?>&amp;action=delete">
				<?php echo T_DELETE;?></a>
			</li>
			<li><a href="<?php echo SITE_URL;?>/admin/logout.php">Logout</a>
			</li>
		</ul>
	<?php 
		echo $end;
	}
}
////////////////////////////////////////////////////////////
function sb_links($beg,$end){//links sitemap
		echo $beg;
		
	?>
		<b><?php echo T_MENU;?>:</b>
		<ul>
		    <?php if(FRIENDLY_URL) {?>
			    <li><a href="<?php echo SITE_URL."/".u(T_ADV_SEARCH);?>.htm"><?php echo T_ADV_SEARCH;?></a></li>
			    <li><a href="<?php echo SITE_URL."/".u(T_SITEMAP);?>.htm"><?php echo T_SITEMAP;?></a></li>   
			    <li><a href="<?php echo SITE_URL."/".u(T_PRIVACY);?>.htm"><?php echo T_PRIVACY;?></a></li>
		    <?php }else { ?>
		        <li><a href="<?php echo SITE_URL;?>/content/search.php"><?php echo T_ADV_SEARCH;?></a></li>
		        <li><a href="<?php echo SITE_URL;?>/content/site-map.php"><?php echo T_SITEMAP;?></a></li>
			    <li><a href="<?php echo SITE_URL;?>/content/privacy.php"><?php echo T_PRIVACY;?></a></li>
		    <?php } ?>
		    <li><a href="<?php echo SITE_URL."/".contactURL();?>"><?php echo T_CONTACT;?></a></li>
		    <li><a href="<?php echo SITE_URL;?>/admin/"><?php echo T_ADMIN;?></a></li>
		</ul>
	<?php 
	echo $end;
}

////////////////////////////////////////////////////////////
function sb_comments($beg,$end){//disqus comments
	if (DISQUS!=""){
		return $beg .'<script type="text/javascript" src="http://disqus.com/forums/'.DISQUS.'/combination_widget.js?num_items=5&hide_mods=0&color=blue&default_tab=recent&excerpt_length=200"></script>'.$end;
	}
}

////////////////////////////////////////////////////////////
function sb_translator($beg,$end){//google translate
	return $beg.'<div id="google_translate_element"></div><script>
	function googleTranslateElementInit() {
	new google.translate.TranslateElement({pageLanguage: \'en\'}, \'google_translate_element\');
	}</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>'.$end;
}

///////////////////////////////////////////////////////////
function sb_theme($beg,$end){//theme selector
	if (THEME_SELECTOR){
	    echo $beg;?>
	    <b onclick="openClose('theme_sel');" style="cursor:pointer;"><?php echo THEME;?></b>
	    <div id="theme_sel" style="display:none;"><ul>
		<?php
		$themes = scandir(SITE_ROOT."/themes");
		foreach ($themes as $theme) {
			if($theme!="" && $theme!=THEME && $theme!="." && $theme!=".." && $theme!="wordcloud.css"){
				echo '<li><a href="'.SITE_URL.'/?theme='.$theme.'">'.$theme.'</a></li>';
			}
		}
	    echo "</ul></div>" . $end;
	}
}
?>
