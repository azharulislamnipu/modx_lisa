[!userProfile? &type=`Archousa`!]
<style>
    table.lines {
        width: 100%;
        margin: 0;
    }

    table.lines th:first-child,
    table.lines td:first-child {
        padding-left: 0px;
    }

    table.lines th:last-child,
    table.lines td:last-child {
        padding-right: 0px;
    }

    table.lines th,
    table.lines td {
        border: 0;
        vertical-align: top;
    }

    table.lines td {
        border-top: 1px solid #ccc;
    }

    img {
        vertical-align: middle;
    }
</style>

<div class="span-4"><div class="margin-top-none">
        <div class="width padding">

            [!LDataDisplay? &table=`spp_archons,spp_archousa` &display=`1` &tpl=`tpl_ac_upd_image` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1|ARCHOUSAID,[!getPlaceholder? &name=`archousaid`!],1`!]

            <!--[!LDataDisplay? &table=`vw_elig` &display=`1` &tpl=`ac_upd_archon_info` &filter=`CUSTOMERID,7018,1|STATUSSTT,Delinquent,2|STATUSSTT,Inactive,2`!]-->

            <!--[ !LDataDisplay? &table=`vw_elig` &display=`1` &tpl=`ac_upd_archon_info` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1|STATUSSTT,Delinquent,2|STATUSSTT,Inactive,2`!]-->

            <h3>Children</h3>

            <table class="lines font-small" cellspacing="0">
                [!LDataDisplay? &table=`spp_children` &display=`all` &sortBy=`FIRSTNAME` &sortDir=`ASC` &tpl=`ac_upd_children_info` &noResult=`ac_children_none` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]
            </table>

            <form name="child_add" action="/services/archousa-upd/profile_child_add.php" method="post">
                <input type="hidden" name="ARCHOUSAID" value="[!getPlaceholder? &name=`archousaid`!]">
                <input type="hidden" name="CUSTOMERID" value="[!getPlaceholder? &name=`customercd`!]">
                <input type="hidden" name="SPOUSEID" value="[!getPlaceholder? &name=`customercd`!]">
                <input type="hidden" name="UPDATEDBY" value="[[svVars? &var=`webShortname`]]">
                <input type="hidden" name="RETURN" value="[!getPlaceholder? &name=`return`!]">
            </form>

            <p class="font-small"><a href="#" onclick="document.child_add.submit(); return false;" title="Add child" class="button">Add Child</a>

        </div></div></div>
<!-- #END MAIN COLUMN -->




<!-- #BEGIN SECOND COLUMN -->
<div class="span-4"><div class="padding">
        <div class="width padding">

            <h3>Education</h3>

            [!LDataDisplay? &table=`spp_educ` &sortBy=`DEGREEYEAR` &sortDir=`ASC` &tpl=`tpl_ac_upd_educ_list` &noResult=`ac_educ_no_result` &filter=`ARCHOUSAID,[!getPlaceholder? &name=`archousaid`!],1`!]

            <form name="education" action="/services/archousa-upd/profile_education_add.php" method="post">
                <input type="hidden" name="ARCHOUSAID" value="[!getPlaceholder? &name=`archousaid`!]">
                <input type="hidden" name="CUSTOMERID" value="[!getPlaceholder? &name=`customercd`!]">
                <input type="hidden" name="UPDATEDBY" value="[[svVars? &var=`webShortname`]]">
                <input type="hidden" name="RETURN" value="[!getPlaceholder? &name=`return`!]">
            </form>

            <p class="font-small"><a href="#" onclick="document.education.submit(); return false;" title="Add Education" class="button">Add Education</a></p>


            <h3>Career</h3>

            [!LDataDisplay? &table=`vw_all_jobs_web_elig` &display=`all` &sortBy=`COMPANYJOBSTART` &sortDir=`DESC` &tpl=`tpl_ac_upd_job_list` &noResult=`ac_job_no_result` &filter=`ARCHOUSAID,[!getPlaceholder? &name=`archousaid`!],1`!]

            <form name="career_add" action="/services/archousa-upd/profile_career_add.php" method="post">
                <input type="hidden" name="ARCHOUSAID" value="[!getPlaceholder? &name=`archousaid`!]">
                <input type="hidden" name="CUSTOMERID" value="[!getPlaceholder? &name=`customercd`!]">
                <input type="hidden" name="UPDATEDBY" value="[!svVars? &var=`webShortname`!]">
                <input type="hidden" name="RETURN" value="[!getPlaceholder? &name=`return`!]">
            </form>
            <p class="font-small"><a href="#" onclick="document.career_add.submit(); return false;" title="Add employment" class="button">Add Employment</a></p>

        </div></div></div>
<!-- #END MAIN COLUMN -->




<!-- #BEGIN SECOND COLUMN -->
<div class="span-4"><div class="padding">
        <div class="width padding">

            [!LDataDisplay? &table=`spp_archousa` &display=`1` &tpl=`tpl_ac_upd_bio` &filter=`ARCHOUSAID,[!getPlaceholder? &name=`archousaid`!],1`!]

        </div></div>












    <div style="display:none;">[!userProfile? &type='Archousa'!]

        [ !LDataDisplay? &table=`spp_archons,spp_archousa` &display=`1` &tpl=`tpl_ac_image` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1|ARCHOUSAID,[!getPlaceholder? &name=`archousaid`!],1`!]

        { {tpl_ac_educ}}

        [ !LDataDisplay? &table=`spp_archousa` &display=`1` &tpl=`tpl_ac_jobs` &filter=`ARCHOUSAID,[!getPlaceholder? &name=`archousaid`!],1`!]

        [ !LDataDisplay? &table=`spp_archousa` &display=`1` &tpl=`tpl_ac_bio` &filter=`ARCHOUSAID,[!getPlaceholder? &name=`archousaid`!],1`!]

        [ !LDataDisplay? &table=`vw_elig` &display=`1` &tpl=`ac_archon_info` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1|STATUSSTT,Delinquent,2|STATUSSTT,Inactive,2`!]

        [ !LDataDisplay? &table=`spp_archousa` &display=`1` &tpl=`ac_personal_info` &filter=`ARCHOUSAID,[!getPlaceholder? &name=`archousaid`!],1`!]

        { {ac_occup_info}}
    </div>

