<?php
require_once('../Connections/sigma_modx.php');

//$_GET = array('firstname'=>'aa', 'state'=>'BA');
//print_r($_GET);
$selectDefaultValue = 'all';

if (!function_exists("GetSQLValueString"))
{
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

        $theValue = urldecode($theValue);
        $theValue = str_replace('*', '%', $theValue);
        
        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType)
        {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;    
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}

// --------------------------------------------------------------
// Start of building up the extra conditional sql from form input
// to be used inside MX Site Search
// --------------------------------------------------------------

// fields definitions (form field name => table field name)
$fields = array
(
    'firstname' => 'FIRSTNAME',
    'lastname'  => 'LASTNAME',
    'boule'     => 'CHAPTERID',
    'city'      => 'CITY',
    'state'     => 'STATECD',
    'region'    => 'REGIONCD',
    'title'     => 'JOBTITLE',
    'company'   => array('COMPANYNAME', 'ORGNAME'),
    'school'    => 'INSTITUTION',
    'industry'  => 'OCCUPATIONID',
);

// establishing sql statement expression verification type in WHERE statement
// eg: field LIKE value, field = value
$sqlFieldsEquiv = array
(
    'like' => array
    (
        'FIRSTNAME', 'LASTNAME', 'CITY', 'JOBTITLE', 'COMPANYNAME', 'INSTITUTION', 'OCCUPATIONID'
    ),
    
    'equal' => array
    (
        'CHAPTERID', 'STATECD', 'REGIONCD'
    )
);

// adding error validation (no "all results returned")
$errorMsg = null;
$hasErrors = true;
if (empty($_GET['q']))
{
    foreach ($fields as $formFld => $tblFld)
    {
        if (isset($_GET[$formFld]) && !empty($_GET[$formFld])
            && $_GET[$formFld] != $selectDefaultValue
            && $_GET[$formFld] != '*')
        {
            $hasErrors = false;
            break;
        }
    }
}
else
{
    $hasErrors = false;
}

if ($hasErrors) $errorMsg = 'You must specify at least one field!';

// converts same name keys from query strings into $_GET array values
// (used for multiple-type select values sent via GET requests)
function createArrayFromDuplicateQueryString($queryStr, $selectName)
{
    if (empty($queryStr))
    {
        return false;
    }
    
    $parts = explode('&', $queryStr);
    
    $selectNameArray = array();
    foreach ($parts as $part)
    {
        // if select name is found, add all of them in an array
        if (strpos($part, $selectName) === 0)
        {
            list($key, $value) = explode('=', $part);
            $selectNameArray[] = $value;
        }
    }
    
    return $selectNameArray;
}

$selectName = 'industry';
$queryStr = $_SERVER['QUERY_STRING'];
if (($data =& createArrayFromDuplicateQueryString($queryStr, $selectName)) !== false)
{
    $_GET[$selectName] = $data;
}

// assigning values to the array containing form fields values
// builds up the formValues array containing data being sent by http request
$formValues = array();
foreach ($fields as $formFld => $tblFld)
{
    if (isset($_GET[$formFld]))
    {
        $formValue = $_GET[$formFld];
        
        // for text input fields
        if (!is_array($_GET[$formFld]))
        {
            $value  = GetSQLValueString($formValue, "text");
            $formValues[$formFld] = (get_magic_quotes_gpc()) ? $value : addslashes($value);
        }
        else if (!empty($_GET[$formFld])) // for multiple-type select input fields
        {
            foreach ($formValue as $value)
            {
                $value  = GetSQLValueString($value, "text");
                $formValues[$formFld][] = (get_magic_quotes_gpc()) ? $value : addslashes($value);
            }
        }
    }
}

// building up the conditional sql based on selected form input
$whereConditions = array();
foreach ($fields as $formFld => $tblFld)
{
    $value = $formValues[$formFld];
    $sqlEquiv = in_array($tblFld, $sqlFieldsEquiv['equal']) ? '=' : 'LIKE';
    
    if (!empty($value) && $value !== 'NULL')
    {
        if (!is_array($value))
        {
            if ($value != '\''.addslashes($selectDefaultValue).'\'') // default select value
            {   
                if (is_array($tblFld))
                {
                    $extraWhereConditions = array();
                    foreach ($tblFld as $fld)
                    {
                        $extraWhereConditions[] = '`'.$fld.'` '.$sqlEquiv.' '.$value;
                    }
                    $whereConditions[] = '('.implode(' OR ', $extraWhereConditions).')';
                }
                else
                {
                    $whereConditions[] = '`'.$tblFld.'` '.$sqlEquiv.' '.$value;
                }
            }
        }
        else
        {
            // in case the form values are sent from a multiple-type select (array)
            $values = array();
            foreach ($value as $val)
            {
                $values[] = substr($val, 1, -1);
            }
            $value = '\''.implode(' <~> ', $values).'\'';
            $whereConditions[] = '`'.$tblFld.'` '.$sqlEquiv.' '.$value;
        }
    }
}

// added for extra filtering options (to support extra search form fields)
$sqlExtraCond = implode(' AND ', $whereConditions);
//exit;

// ------------------------------------------------------------
// End of building up the extra conditional sql from form input
// ------------------------------------------------------------

// ----------------------------------------
// Begin of MX Site Search default behavior
// ----------------------------------------

if (isset($_GET['q']))
{
    //Search functions include
    require_once('../includes/MXSearch/MX_Search.inc.php');

    //Start Search Configuration
    $KT_MxSearchTables = new KT_MXSearchConfig();

    // Start vw_search
    $KT_MxSearchTables->addTable("vw_search", 5, "L2R_FULLNAME", "LOCATION", "/services/directory/results.php?id=", "WEB_ID", $sqlExtraCond);
    $KT_MxSearchTables->addField("vw_search", "REGIONNAME", 3);
    $KT_MxSearchTables->addField("vw_search", "REGIONCD", 3);
    $KT_MxSearchTables->addField("vw_search", "BOULENAME", 3);
    $KT_MxSearchTables->addField("vw_search", "CHAPTERID", 3);
    $KT_MxSearchTables->addField("vw_search", "BOULECITY", 3);
    $KT_MxSearchTables->addField("vw_search", "BOULESTATECD", 3);
    $KT_MxSearchTables->addField("vw_search", "LOCATION", 3);
    $KT_MxSearchTables->addField("vw_search", "STATUS", 3);
    $KT_MxSearchTables->addField("vw_search", "FULLNAME", 3);
    $KT_MxSearchTables->addField("vw_search", "L2R_FULLNAME", 5);
    $KT_MxSearchTables->addField("vw_search", "PREFIX", 3);
    $KT_MxSearchTables->addField("vw_search", "FIRSTNAME", 3);
    $KT_MxSearchTables->addField("vw_search", "MIDDLEINITIAL", 3);
    $KT_MxSearchTables->addField("vw_search", "LASTNAME", 4);
    $KT_MxSearchTables->addField("vw_search", "HOMEPHONE", 3);
    $KT_MxSearchTables->addField("vw_search", "SUFFIX", 3);
    $KT_MxSearchTables->addField("vw_search", "DESIGNATIONLST", 3);
    $KT_MxSearchTables->addField("vw_search", "EMAIL", 3);
    $KT_MxSearchTables->addField("vw_search", "WORK_EMAIL", 3);
    $KT_MxSearchTables->addField("vw_search", "WEB_EMAIL", 3);
    $KT_MxSearchTables->addField("vw_search", "UPDATEDBY", 3);
    $KT_MxSearchTables->addField("vw_search", "SKILLCDLST", 3);
    $KT_MxSearchTables->addField("vw_search", "OCCUPATIONCD", 3);
    $KT_MxSearchTables->addField("vw_search", "JOBTITLE", 5);
    $KT_MxSearchTables->addField("vw_search", "ORGNAME", 5);
    $KT_MxSearchTables->addField("vw_search", "CITY", 3);
    $KT_MxSearchTables->addField("vw_search", "STATECD", 3);
    $KT_MxSearchTables->addField("vw_search", "ZIP", 3);
    $KT_MxSearchTables->addField("vw_search", "BIO", 5);
    $KT_MxSearchTables->addField("vw_search", "SPOUSENAME", 3);
    $KT_MxSearchTables->addField("vw_search", "OCCUPATION_TYPE", 4);
    $KT_MxSearchTables->addField("vw_search", "JOB_TITLE", 4);
    $KT_MxSearchTables->addField("vw_search", "COMPANYNAME", 5);
    $KT_MxSearchTables->addField("vw_search", "COMPANYCITY", 3);
    $KT_MxSearchTables->addField("vw_search", "COMPANYSTCD", 3);
    $KT_MxSearchTables->addField("vw_search", "JOB_DESCRIPTION", 3);
    $KT_MxSearchTables->addField("vw_search", "DEGREE", 3);
    $KT_MxSearchTables->addField("vw_search", "INSTITUTION", 5);
    $KT_MxSearchTables->addField("vw_search", "MAJOR", 4);
    $KT_MxSearchTables->addField("vw_search", "MINOR1", 3);
    $KT_MxSearchTables->addField("vw_search", "MINOR2", 3);
    $KT_MxSearchTables->addField("vw_search", "COMMENT", 3);
    $KT_MxSearchTables->addField("vw_search", "CPOSITION", 5);
    // End vw_search

    $currentPage = $_SERVER["PHP_SELF"];

    // Begin Search Recordset

    mysql_select_db($database_sigma_modx, $sigma_modx);

    $KTSE_rsSearch = new KT_MXSearch();
    $KTSE_rsSearch->setSearchType("boolean fulltext");
    $KTSE_rsSearch->setConnection($sigma_modx, "MySQL");
    $KTSE_rsSearch->setCache("src_cache_cah", 0);
    $KTSE_rsSearch->setTempTable("src_temp_tmp");
    $KTSE_rsSearch->setTables($KT_MxSearchTables);
    $KTSE_rsSearch->computeAll("q");

    $maxRows_rsSearch = 10;
    $pageNum_rsSearch = 0;
    if (isset($HTTP_GET_VARS['pageNum_rsSearch'])) {
        $pageNum_rsSearch = $HTTP_GET_VARS['pageNum_rsSearch'];
    }
    $startRow_rsSearch = $pageNum_rsSearch * $maxRows_rsSearch;
    $rsSearch = $KTSE_rsSearch->getRecordset($startRow_rsSearch, $maxRows_rsSearch);
    $row_rsSearch = mysql_fetch_assoc($rsSearch);

    if (isset($HTTP_GET_VARS['totalRows_rsSearch'])) {
        $totalRows_rsSearch = $HTTP_GET_VARS['totalRows_rsSearch'];
    } else {
        $totalRows_rsSearch = $KTSE_rsSearch->getTotalRows();
    }
    $totalPages_rsSearch = (int)(($totalRows_rsSearch-1)/$maxRows_rsSearch);
    // End Search Recordset
    ?>
    <?php $KT_MXSearchKeywords = $KTSE_rsSearch->getKeywords(); ?>
    <?php
    $queryString_rsSearch = "";
    if (!empty($_SERVER['QUERY_STRING'])) {
      $params = explode("&", $_SERVER['QUERY_STRING']);
      $newParams = array();
      foreach ($params as $param) {
        if (stristr($param, "pageNum_rsSearch") == false && 
            stristr($param, "totalRows_rsSearch") == false) {
          array_push($newParams, $param);
        }
      }
      if (count($newParams) != 0) {
        $queryString_rsSearch = "&" . htmlentities(implode("&", $newParams));
      }
    }
    $queryString_rsSearch = sprintf("&totalRows_rsSearch=%d%s", $totalRows_rsSearch,$queryString_rsSearch);
}

// --------------------------------------
// End of MX Site Search default behavior
// --------------------------------------

// --------------------------------------------------
// Begin of getting data to fill search form's fields
// --------------------------------------------------

//mysql_select_db($database_sigma_modx, $sigma_modx);
//$query_rsIndustry = "SELECT * FROM vw_search WHERE OCCUPATIONID is not null";
//$rsIndustry = mysql_query($query_rsIndustry, $sigma_modx) or die(mysql_error());
//$row_rsIndustry = mysql_fetch_assoc($rsIndustry);
//$totalRows_rsIndustry = mysql_num_rows($rsIndustry);
//do
//{
//    echo '<pre>';
//    print_r($row_rsIndustry);
//    echo '</pre>';
//}
//while ($row_rsIndustry = mysql_fetch_assoc($rsIndustry));
//exit;

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsIndustry = "SELECT * FROM spp_occupation ORDER BY `DESCRIPTION` ASC";
$rsIndustry = mysql_query($query_rsIndustry, $sigma_modx) or die(mysql_error());
$totalRows_rsIndustry = mysql_num_rows($rsIndustry);

// built to allow key -> values relations for industries in the "You searched for" text
$industries = array();
if ($totalRows_rsIndustry)
{
    while ($row_rsIndustry = mysql_fetch_assoc($rsIndustry))
    {
      $industry = mysql_fetch_assoc($rsIndustry);
      $industries[$industry['OCCUPATIONID']] = $industry['DESCRIPTION'];
    }
    mysql_data_seek($rsIndustry, 0);
}

$row_rsIndustry = mysql_fetch_assoc($rsIndustry);


$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = mysql_query($query_rsStates, $sigma_modx) or die(mysql_error());
$row_rsStates = mysql_fetch_assoc($rsStates);
$totalRows_rsStates = mysql_num_rows($rsStates);

$query_rsBoule = "SELECT * FROM spp_boule ORDER BY BOULENAME ASC";
$rsBoule = mysql_query($query_rsBoule, $sigma_modx) or die(mysql_error());
$row_rsBoule = mysql_fetch_assoc($rsBoule);
$totalRows_rsBoule = mysql_num_rows($rsBoule);

$query_rsRegions = "SELECT DISTINCT REGIONCD, REGIONNAME FROM spp_boule ORDER BY REGIONNAME ASC";
$rsRegions = mysql_query($query_rsRegions, $sigma_modx) or die(mysql_error());
$row_rsRegions = mysql_fetch_assoc($rsRegions);
$totalRows_rsRegions = mysql_num_rows($rsRegions);

$flds = array
(
    'q' => 'keywords',
    'firstname' => 'first name',
    'lastname'  => 'last name',
    'state' => 'state',
    'city'  => 'city',
    'boule'  => 'boul&eacute;',
    'city'  => 'city',
    'state' => 'state',
    'region'    => 'region',
    'title'     => 'title',
    'company'   => 'company',
    'school'    => 'school',
    'industry'  => 'industries',
);

$parts = array();
foreach ($flds as $fld => $desc)
{
    if (isset($_GET[$fld]) && !empty($_GET[$fld]))
    {
        if (!is_array($_GET[$fld]))
        {
            $parts[] = $desc.': <b>'.stripslashes(htmlentities($_GET[$fld])).'</b>';
        }
        else
        {
            $values = array();
            foreach ($_GET[$fld] as $val)
            {
                $values[] = $industries[$val];
            }
            
            $values = implode(',', $values);
            $parts[] = $desc.': <b>'.stripslashes(htmlentities($values)).'</b>';
        }
    }
}

$searchTerms = 'for '.implode(', ', $parts);
$searchString = 'Your search '.$searchTerms.' returned the following results:';

// ------------------------------------------------
// End of getting data to fill search form's fields
// ------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity |</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
  <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">ADVANCED SEARCH</h1>
<div class="contentBlock">
  <table width="100%" cellpadding="2">
    <tr valign="top">
      <td width="50%">
      <form name="MX_Search_Form" method="GET" action="">
      <table width="100%" cellpadding="2">
          <tr>
            <td><div align="right">Keywords:              </div></td>
            <td><input name="q" type="text" id="q" size="45" value="<?=@$_GET['q']?>" /></td>
          </tr>
          <tr>
            <td><div align="right">State:</div></td>
            <td><select name="state" id="state"><option value="all">Anywhere</option>
                <?php
do {  
    $extra = ((isset($_GET['state']) && $row_rsStates['st_cd'] == $_GET['state'])?'selected="selected"':'');
?>
                <option value="<?php echo $row_rsStates['st_cd']?>" <?=$extra?>><?php echo $row_rsStates['st_nm']?></option>
                <?php
} while ($row_rsStates = mysql_fetch_assoc($rsStates));
  $rows = mysql_num_rows($rsStates);
  if($rows > 0) {
      mysql_data_seek($rsStates, 0);
      $row_rsStates = mysql_fetch_assoc($rsStates);
  }
?>
            </select></td>
          </tr>
          <tr>
            <td><div align="right">City:</div></td>
            <td><input name="city" type="text" id="city" size="45" value="<?=@$_GET['city']?>" /></td>
          </tr>
        </table></td>
      <td><table width="100%" cellpadding="2">
        <tr>
          <td><div align="right">First Name:</div></td>
          <td><input name="firstname" type="text" id="firstname" size="45" value="<?=@$_GET['firstname']?>" /></td>
        </tr>
        <tr>
          <td><div align="right">Last Name:</div></td>
          <td><input name="lastname" type="text" id="lastname" size="45" value="<?=@$_GET['lastname']?>" /></td>
        </tr>
        <tr>
          <td><div align="right">Boul&eacute;:</div></td>
          <td><select name="boule" id="boule">
            <option value="all">Anywhere</option>
            <?php
do {
    $extra = ((isset($_GET['boule']) && $row_rsBoule['CHAPTERID'] == $_GET['boule'])?'selected="selected"':'');  
?>
            <option value="<?php echo $row_rsBoule['CHAPTERID']?>" <?=$extra?>><?php echo $row_rsBoule['BOULENAME']?></option>
            <?php
} while ($row_rsBoule = mysql_fetch_assoc($rsBoule));
  $rows = mysql_num_rows($rsBoule);
  if($rows > 0) {
      mysql_data_seek($rsBoule, 0);
      $row_rsBoule = mysql_fetch_assoc($rsBoule);
  }
?>
          </select></td>
        </tr>
        <tr>
          <td><div align="right">Region:</div></td>
          <td><select name="region" id="region">
            <option value="all">Anywhere</option>
            <?php
do {  
    $extra = ((isset($_GET['region']) && $row_rsRegions['REGIONCD'] == $_GET['region'])?'selected="selected"':'');
?>
            <option value="<?php echo $row_rsRegions['REGIONCD']?>" <?=$extra?>><?php echo $row_rsRegions['REGIONNAME']?></option>
            <?php
} while ($row_rsRegions = mysql_fetch_assoc($rsRegions));
  $rows = mysql_num_rows($rsRegions);
  if($rows > 0) {
      mysql_data_seek($rsRegions, 0);
      $row_rsRegions = mysql_fetch_assoc($rsRegions);
  }
?>
          </select></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="2"><hr noshade /></td>
      </tr>
    <tr valign="top">
      <td width="50%"><table width="100%" cellpadding="2">
        <tr>
          <td><div align="right">Title:</div></td>
          <td><input name="title" type="text" id="title" size="45" value="<?=@$_GET['title']?>" /></td>
        </tr>
        <tr>
          <td><div align="right">Company:</div></td>
          <td><input name="company" type="text" id="company" size="45" value="<?=@$_GET['company']?>" /></td>
        </tr>
        <tr>
          <td><div align="right">School:</div></td>
          <td><input name="school" type="text" id="school" size="45" value="<?=@$_GET['school']?>" /></td>
        </tr>
      </table></td>
      <td><table width="100%" cellpadding="2">
        <tr>
          <td valign="top"><div align="right">Industry:</div></td>
          <td><select name="industry" size="5" multiple="multiple" id="industry">
            <?php
do {  
    //$extra = ((isset($_GET['industry']) && in_array($row_rsIndustry['OCCUPATIONID'], $_GET['industry']))?'selected="selected"':'');
    $extra = '';
?>
            <option value="<?php echo $row_rsIndustry['OCCUPATIONID']?>" <?=$extra?>><?php echo $row_rsIndustry['DESCRIPTION']?></option>
            <?php
} while ($row_rsIndustry = mysql_fetch_assoc($rsIndustry));
  $rows = mysql_num_rows($rsIndustry);
  if($rows > 0) {
      mysql_data_seek($rsIndustry, 0);
      $row_rsIndustry = mysql_fetch_assoc($rsIndustry);
  }
?>
          </select></td>
        </tr>
        <tr>
          <td valign="top"><div align="right">:</div></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="2"><div align="center">
        <input type="submit" name="search" id="search" value="Search" />
</div></td>
      </tr>
  </table>
  
  <!-- added -->
  <table width="90%" border="0">
  <?php if ($totalRows_rsSearch > 0 && !$hasErrors) { // Show if recordset not empty ?>
    <tr>
      <td><p><?=$searchString?></p>
        <table width="100%"  border="0" align="left" cellpadding="2" cellspacing="0">
            <?php $offset = 0; ?>
            <?php do { ?>
              <tr>
                <td><div style="margin: 0 0 0 18px;">
                    <p><?php //echo (min($startRow_rsSearch + 1, $totalRows_rsSearch)) + $offset?><a href="<?php echo $row_rsSearch['url_cah']; ?>" target="_self"><?php echo $row_rsSearch['title_cah']; ?></a><br>
                          <font color="#339900"><?php echo $KTSE_rsSearch->formatDescription($row_rsSearch['shortdesc_cah']); ?></font> </p>
                </div></td>
              </tr>
              <?php $offset++; ?>
              <?php } while ($row_rsSearch = mysql_fetch_assoc($rsSearch)); ?>
        </table></td>
    </tr>
    <tr>
      <td><p>Showing matches <?php echo (min($startRow_rsSearch + 1, $totalRows_rsSearch)) ?> - <?php echo min($startRow_rsSearch + $maxRows_rsSearch, $totalRows_rsSearch) ?> from <?php echo $totalRows_rsSearch ?></p></td>
    </tr>
    <tr>
      <td><?php if ($totalRows_rsSearch > 10) { // Show if recordset not empty ?>
          <table border="0" align="left">
              <tr>
                <td align="center"><?php if ($pageNum_rsSearch > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_rsSearch=%d%s", $currentPage, 0, $queryString_rsSearch); ?>">First</a>
                      <?php } // Show if not first page ?>
                  <?php if ($pageNum_rsSearch == 0) { // Show if first page ?>
                    First
                    <?php } // Show if first page ?>                </td>
                <td align="center"><?php if ($pageNum_rsSearch > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_rsSearch=%d%s", $currentPage, max(0, $pageNum_rsSearch - 1), $queryString_rsSearch); ?>">Prev</a>
                      <?php } // Show if not first page ?>
                  <?php if ($pageNum_rsSearch == 0) { // Show if first page ?>
                    Prev
                    <?php } // Show if first page ?>                </td>
                <td align="center"><?php if ($pageNum_rsSearch < $totalPages_rsSearch) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_rsSearch=%d%s", $currentPage, min($totalPages_rsSearch, $pageNum_rsSearch + 1), $queryString_rsSearch); ?>">Next</a>
                      <?php } // Show if not last page ?>
                  <?php if ($pageNum_rsSearch >= $totalPages_rsSearch) { // Show if last page ?>
                    Next
                    <?php } // Show if last page ?>                </td>
                <td align="center"><?php if ($pageNum_rsSearch < $totalPages_rsSearch) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_rsSearch=%d%s", $currentPage, $totalPages_rsSearch, $queryString_rsSearch); ?>">Last</a>
                      <?php } // Show if not last page ?>
                  <?php if ($pageNum_rsSearch >= $totalPages_rsSearch) { // Show if last page ?>
                    Last
                    <?php } // Show if last page ?>                </td>
              </tr>
                </table>
          <?php } // Show if recordset not empty ?></td>
    </tr>
    <?php } // Show if recordset not empty ?>
      <?php 
// Show IF Conditional region1 
if (@$_GET['q'] == NULL) {
?>
<tr>
        <td>&nbsp;</td>
</tr>
  <?php } 
// endif Conditional region1

if (isset($_GET['q']))
{
    if ($hasErrors && !is_null($errorMsg)) echo '<tr><td>'.$errorMsg.'</td>';
    else if (($totalRows_rsSearch == 0)){ // Show if recordset empty
?>
<tr>
          <td>No records found matching your criteria! </td>
</tr>
<?php }} // Show if recordset empty ?>
</table>
<!-- added -->
</div>
<!-- InstanceEndEditable --></div>
<!-- #END MAIN COLUMN -->
</div>
<!-- #PAGE CONTENT ENDS -->
</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsIndustry);

mysql_free_result($rsStates);

mysql_free_result($rsBoule);

mysql_free_result($rsRegions);
?>