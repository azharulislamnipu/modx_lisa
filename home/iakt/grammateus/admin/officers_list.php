<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Filter
$tfi_listrsOfficers2 = new TFI_TableFilter($sigma_modx, "tfi_listrsOfficers2");
$tfi_listrsOfficers2->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listrsOfficers2->addColumn("CPOSITION", "STRING_TYPE", "CPOSITION", "%");
$tfi_listrsOfficers2->addColumn("STARTDATE", "DATE_TYPE", "STARTDATE", "=");
$tfi_listrsOfficers2->Execute();

// Sorter
$tso_listrsOfficers2 = new TSO_TableSorter("rsOfficers", "tso_listrsOfficers2");
$tso_listrsOfficers2->addColumn("FULLNAME");
$tso_listrsOfficers2->addColumn("CPOSITION");
$tso_listrsOfficers2->addColumn("STARTDATE");
$tso_listrsOfficers2->setDefault("FULLNAME");
$tso_listrsOfficers2->Execute();

// Navigation
$nav_listrsOfficers2 = new NAV_Regular("nav_listrsOfficers2", "rsOfficers", "../../", $_SERVER['PHP_SELF'], 30);

// Begin List Recordset
$maxRows_rsOfficers = $_SESSION['max_rows_nav_listrsOfficers2'];
$pageNum_rsOfficers = 0;
if (isset($_GET['pageNum_rsOfficers'])) {
  $pageNum_rsOfficers = $_GET['pageNum_rsOfficers'];
}
$startRow_rsOfficers = $pageNum_rsOfficers * $maxRows_rsOfficers;
$colname__rsOfficers = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsOfficers = $_SESSION['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter__rsOfficers = "1=1";
if (isset($_SESSION['filter_tfi_listrsOfficers2'])) {
  $NXTFilter__rsOfficers = $_SESSION['filter_tfi_listrsOfficers2'];
}
// Defining List Recordset variable
$NXTSort__rsOfficers = "FULLNAME";
if (isset($_SESSION['sorter_tso_listrsOfficers2'])) {
  $NXTSort__rsOfficers = $_SESSION['sorter_tso_listrsOfficers2'];
}
$query_rsOfficers = sprintf("SELECT * FROM vw_boule_officers WHERE COMMITTEESTATUSSTT = 'Active' AND BOULENAME = %s  AND  {$NXTFilter__rsOfficers}  ORDER BY  {$NXTSort__rsOfficers} ", GetSQLValueString($colname__rsOfficers, "text"));
$rsOfficers = $sigma_modx->SelectLimit($query_rsOfficers, $maxRows_rsOfficers, $startRow_rsOfficers) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsOfficers'])) {
  $totalRows_rsOfficers = $_GET['totalRows_rsOfficers'];
} else {
  $all_rsOfficers = $sigma_modx->SelectLimit($query_rsOfficers) or die($sigma_modx->ErrorMsg());
  $totalRows_rsOfficers = $all_rsOfficers->RecordCount();
}
$totalPages_rsOfficers = (int)(($totalRows_rsOfficers-1)/$maxRows_rsOfficers);
// End List Recordset

$nav_listrsOfficers2->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: false,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_FULLNAME {width:200px; overflow:hidden;}
  .KT_col_CPOSITION {width:140px; overflow:hidden;}
  .KT_col_STARTDATE {width:90px; overflow:hidden;}
.style2 {
	font-size: 12px;
	font-weight: bold;
}
</style><!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listrsOfficers2">
  <h1> MANAGE <?php echo strtoupper($_SESSION['BOULENAME']); ?> OFFICERS </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listrsOfficers2->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
            <?php 
  // Show IF Conditional region1 
  if (@$_GET['show_all_nav_listrsOfficers2'] == 1) {
?>
              <?php echo $_SESSION['default_max_rows_nav_listrsOfficers2']; ?>
              <?php 
  // else Conditional region1
  } else { ?>
              <?php echo NXT_getResource("all"); ?>
              <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
      &nbsp; </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>            </th>
            <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listrsOfficers2->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listrsOfficers2->getSortLink('FULLNAME'); ?>">NAME</a> </th>
            <th id="CPOSITION" class="KT_sorter KT_col_CPOSITION <?php echo $tso_listrsOfficers2->getSortIcon('CPOSITION'); ?>"> <a href="<?php echo $tso_listrsOfficers2->getSortLink('CPOSITION'); ?>">OFFICE</a> </th>
            <th id="STARTDATE" class="KT_sorter KT_col_STARTDATE <?php echo $tso_listrsOfficers2->getSortIcon('STARTDATE'); ?>"> <a href="<?php echo $tso_listrsOfficers2->getSortLink('STARTDATE'); ?>">START DATE</a> </th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php if ($totalRows_rsOfficers == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="5"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsOfficers > 0) { // Show if recordset not empty ?>
            <?php
  while (!$rsOfficers->EOF) { 
?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_vw_boule_officers" class="id_checkbox" value="<?php echo $rsOfficers->Fields('OFFICERID'); ?>" />
                    <input type="hidden" name="OFFICERID" class="id_field" value="<?php echo $rsOfficers->Fields('OFFICERID'); ?>" />                </td>
                <td><div class="KT_col_FULLNAME"><span class="style2"><?php echo $rsOfficers->Fields('L2R_FULLNAME'); ?></span></div></td>
                <td><div class="KT_col_CPOSITION"><?php echo KT_FormatForList($rsOfficers->Fields('CPOSITION'), 30); ?></div></td>
                <td><div class="KT_col_STARTDATE"><?php echo KT_formatDate($rsOfficers->Fields('STARTDATE')); ?></div></td>
                <td><a href="/home/officers_start.php?OFFICERID=<?php echo $rsOfficers->Fields('OFFICERID'); ?>&amp;CUSTOMERID=<?php echo $rsOfficers->Fields('CUSTOMERCD'); ?>&amp;KT_back=1">Change Start Date</a><br>
<a class="KT_edit_link" href="/home/officers_detail.php?OFFICERID=<?php echo $rsOfficers->Fields('OFFICERID'); ?>&amp;CUSTOMERID=<?php echo $rsOfficers->Fields('CUSTOMERCD'); ?>&amp;KT_back=1">Remove from Office</a></td>
              </tr>
              <?php
    $rsOfficers->MoveNext(); 
  }
?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsOfficers2->Prepare();
            require("../../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
        <span>&nbsp;</span>
		<input name="no_new" id="no_new" type="hidden" value="1" />
           <a class="KT_additem_op_link" href="/home/officer_add_new.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsOfficers->Close();
?>
