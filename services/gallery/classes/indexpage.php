<?php
/**
 * SimpleViewer admin package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

/**
 * IndexPage class
 *
 * @access public
 * @package svManager
 */
Class IndexPage
{
 /**
  * @var string relative path to svManager from index file with trailing separator or empty string
  */
  var $pathToManager = '';

 /**
  * Generate html for table of links
  *
  * @access public
  * @return string html
  * @param integer number of columns in table
  */
  function getHtml($columns, $pathToManager = '')
  {
    $this->thumbWidthHtml = 'width="'.INDEX_THUMB_WIDTH.'"';
    $this->thumbHeightHtml = 'height="'.INDEX_THUMB_HEIGHT.'"';
    $this->pathToManager = $pathToManager;
    $columns = ($columns < 0) ? 0 : $columns;
    $columns = floor($columns);
    if (!file_exists($pathToManager.GALLERY_DATA_FILE))
    {
      return '<p class="nogalleries">You have no galleries</p>';
    }
    $galleriesData = @file_get_contents($pathToManager.GALLERY_DATA_FILE);
    if ($galleriesData === false)
    {
      trigger_error('cannot read '.$pathToManager.GALLERY_DATA_FILE, E_USER_ERROR);
    }
    $galleriesData = @unserialize($galleriesData);
    if ($galleriesData === false)
    {
      trigger_error($pathToManager.GALLERY_DATA_FILE.' file is corrupted', E_USER_ERROR);
    }
    if (count($galleriesData) == 0)
    {
      return '<p class="nogalleries">You have no galleries</p>';
    }
    if ($columns > 0)
    {
      return $this->makeTable($galleriesData, $columns);
    }
    else
    {
      return $this->makeDivs($galleriesData);
    }
  }
 /**
  * Make table html
  *
  * @access private
  * @return string html
  * @param array gallery data
  * @param integer number of columns in table
  */
  function makeTable($galleriesData, $columns)
  {
    $nGalleries = count($galleriesData);
    $rows = ceil($nGalleries/$columns);
    $keys = array_keys($galleriesData);
    $html = '      <table class="svlinks" cellspacing="0">
';
    $n = 0;
    for ($r=0; $r<$rows; $r++)
    {
      $html .= '        <tr class="r'.(1+$r).'">
';
      for ($c=0; $c<$columns; $c++)
      {
        $class = '"c'.(1+$c).'"';
        if ($n < $nGalleries)
        {
          $key = $keys[$n];    
          $thumbPath = $this->pathToManager.THUMB_CACHE.INDEX_THUMB_PREFIX.$galleriesData[$key]['galleryRef'].'.jpg';
          $customThumbPath = $this->pathToManager.CUSTOM_THUMB_DIR.INDEX_THUMB_PREFIX.$galleriesData[$key]['galleryRef'].'.jpg';
          $thumb = file_exists($thumbPath) ? $thumbPath : $this->pathToManager.IMAGE_SRC.'nothumb.gif';
          $thumb = file_exists($customThumbPath) ? $customThumbPath : $thumb; 
          $src = '"'.str_replace('\\', '/', $thumb).'"';
          $alt = '""';
          $title = htmlspecialchars(strip_tags($galleriesData[$key]['title']), ENT_QUOTES, 'UTF-8');
          $id = '"g'.$key.'"';
          $href = '"'.str_replace('\\', '/', $this->pathToManager.$galleriesData[$key]['path']).'"';
          
          $td = "          <td id={$id} class={$class}><a href={$href}><img src={$src} {$this->thumbWidthHtml} {$this->thumbHeightHtml} alt={$alt}/></a><p><a href={$href}>{$title}</a></p></td>";
        }
        else
        {
          $td = "          <td class={$class}>&nbsp;</td>";
        }
        $html .= $td.'
';
        $n++;
      }
      $html .= '        </tr>
';
    }
    $html .= '      </table>
';
    return $html;
  }

 /**
  * Make html unordered list
  *
  * @access private
  * @return string html
  * @param array gallery data
  */
  function makeDivs($galleriesData)
  {
    $count = 0;
    $html = '      <div class="svlinks">
';
    foreach ($galleriesData as $key=>$gallery)
    {
      $count ++;
      $thumbPath = $this->pathToManager.THUMB_CACHE.INDEX_THUMB_PREFIX.$galleriesData[$key]['galleryRef'].'.jpg';
      $customThumbPath = $this->pathToManager.CUSTOM_THUMB_DIR.INDEX_THUMB_PREFIX.$galleriesData[$key]['galleryRef'].'.jpg';
      $thumb = file_exists($thumbPath) ? $thumbPath : $this->pathToManager.IMAGE_SRC.'nothumb.gif';
      $thumb = file_exists($customThumbPath) ? $customThumbPath : $thumb;
      $src = '"'.str_replace('\\', '/', $thumb).'"';
      $alt = '""';
      $title = htmlspecialchars(strip_tags($gallery['title']), ENT_QUOTES, 'UTF-8');
      $id = '"g'.$key.'"';
      $class = '"n'.$count.'"';
      $href = '"'.str_replace('\\', '/', $this->pathToManager.$gallery['path']).'"';
      $html .= "        <div id={$id} class={$class}><a href={$href}><img src={$src} {$this->thumbWidthHtml} {$this->thumbHeightHtml} alt={$alt} /></a><p><a href={$href}>{$title}</a></p></div>
";
    }
    $html .= '        <br class="clearboth" />
      </div>
';
    return $html;
  }
}
?>