<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("JOINDATE", false, "date", "date", "", "", "Please enter the date the archon was installed.");
$formValidation->addField("PREFIX", true, "text", "", "", "", "");
$formValidation->addField("FIRSTNAME", true, "text", "", "", "", "");
$formValidation->addField("LASTNAME", true, "text", "", "", "", "");
$formValidation->addField("ADDRESS1", true, "text", "", "", "", "");
$formValidation->addField("CITY", true, "text", "", "", "", "");
$formValidation->addField("STATECD", true, "text", "", "", "", "");
$formValidation->addField("ZIP", true, "text", "", "", "", "");
$formValidation->addField("BIRTHDATE", false, "date", "date", "", "", "");
$formValidation->addField("DECEASEDDATE", false, "date", "date", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// begin Recordset
$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = $sigma_modx->SelectLimit($query_rsStates) or die($sigma_modx->ErrorMsg());
$totalRows_rsStates = $rsStates->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_archons = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_archons);
// Register triggers
$ins_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$ins_spp_archons->setTable("spp_archons");
$ins_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_archons = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$upd_spp_archons->addColumn("PREFIX", "STRING_TYPE", "POST", "PREFIX");
$upd_spp_archons->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$upd_spp_archons->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$upd_spp_archons->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$upd_spp_archons->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$upd_spp_archons->addColumn("DESIGNATIONLST", "STRING_TYPE", "POST", "DESIGNATIONLST");
$upd_spp_archons->addColumn("ADDRESS1", "STRING_TYPE", "POST", "ADDRESS1");
$upd_spp_archons->addColumn("ADDRESS2", "STRING_TYPE", "POST", "ADDRESS2");
$upd_spp_archons->addColumn("CITY", "STRING_TYPE", "POST", "CITY");
$upd_spp_archons->addColumn("STATECD", "STRING_TYPE", "POST", "STATECD");
$upd_spp_archons->addColumn("ZIP", "STRING_TYPE", "POST", "ZIP");
$upd_spp_archons->addColumn("HOMEPHONE", "STRING_TYPE", "POST", "HOMEPHONE");
$upd_spp_archons->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$upd_spp_archons->addColumn("BIRTHDATE", "DATE_TYPE", "POST", "BIRTHDATE");
$upd_spp_archons->addColumn("DECEASEDDATE", "DATE_TYPE", "POST", "DECEASEDDATE");
$upd_spp_archons->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$upd_spp_archons->addColumn("GRAMM", "STRING_TYPE", "VALUE", "{SESSION.webFullname}");
$upd_spp_archons->addColumn("GRAMMUPDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "GET", "CUSTOMERID");

// Make an instance of the transaction object
$del_spp_archons = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_archons);
// Register triggers
$del_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$del_spp_archons->setTable("spp_archons");
$del_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "GET", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$totalRows_rsspp_archons = $rsspp_archons->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wdg="http://www.interaktonline.com/MXWidgets" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    Edit Archon Information </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_archons->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_archons > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <?php 
// Show IF Conditional show_JOINDATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><a name="joindate"></a><label for="JOINDATE_<?php echo $cnt1; ?>">JOIN DATE:</label></td>
              <td><input name="JOINDATE_<?php echo $cnt1; ?>" id="JOINDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_archons->Fields('JOINDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                <?php echo $tNGs->displayFieldHint("JOINDATE");?> <?php echo $tNGs->displayFieldError("spp_archons", "JOINDATE", $cnt1); ?> </td></tr>
            <?php } 
// endif Conditional show_JOINDATE_on_update_only
?>
          <?php 
// Show IF Conditional show_PREFIX_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="PREFIX_<?php echo $cnt1; ?>">PREFIX:</label></td>
              <td><input type="text" name="PREFIX_<?php echo $cnt1; ?>" id="PREFIX_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('PREFIX')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("PREFIX");?> <?php echo $tNGs->displayFieldError("spp_archons", "PREFIX", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_PREFIX_on_update_only
?>
          <?php 
// Show IF Conditional show_FIRSTNAME_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="FIRSTNAME_<?php echo $cnt1; ?>">FIRST NAME:</label></td>
              <td><input type="text" name="FIRSTNAME_<?php echo $cnt1; ?>" id="FIRSTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('FIRSTNAME')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "FIRSTNAME", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_FIRSTNAME_on_update_only
?>
          <?php 
// Show IF Conditional show_MIDDLEINITIAL_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="MIDDLEINITIAL_<?php echo $cnt1; ?>">MIDDLE INITIAL:</label></td>
              <td><input type="text" name="MIDDLEINITIAL_<?php echo $cnt1; ?>" id="MIDDLEINITIAL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('MIDDLEINITIAL')); ?>" size="1" maxlength="1" />
                  <?php echo $tNGs->displayFieldHint("MIDDLEINITIAL");?> <?php echo $tNGs->displayFieldError("spp_archons", "MIDDLEINITIAL", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_MIDDLEINITIAL_on_update_only
?>
          <?php 
// Show IF Conditional show_LASTNAME_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="LASTNAME_<?php echo $cnt1; ?>">LAST NAME:</label></td>
              <td><input type="text" name="LASTNAME_<?php echo $cnt1; ?>" id="LASTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('LASTNAME')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "LASTNAME", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_LASTNAME_on_update_only
?>
          <?php 
// Show IF Conditional show_SUFFIX_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="SUFFIX_<?php echo $cnt1; ?>">SUFFIX:</label></td>
              <td><input type="text" name="SUFFIX_<?php echo $cnt1; ?>" id="SUFFIX_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('SUFFIX')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("SUFFIX");?> <?php echo $tNGs->displayFieldError("spp_archons", "SUFFIX", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_SUFFIX_on_update_only
?>
          <?php 
// Show IF Conditional show_DESIGNATIONLST_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="DESIGNATIONLST_<?php echo $cnt1; ?>">HONORIFIC:</label></td>
              <td><input type="text" name="DESIGNATIONLST_<?php echo $cnt1; ?>" id="DESIGNATIONLST_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('DESIGNATIONLST')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("DESIGNATIONLST");?> <?php echo $tNGs->displayFieldError("spp_archons", "DESIGNATIONLST", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_DESIGNATIONLST_on_update_only
?>
          <?php 
// Show IF Conditional show_ADDRESS1_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ADDRESS1_<?php echo $cnt1; ?>">ADDRESS 1:</label></td>
              <td><input type="text" name="ADDRESS1_<?php echo $cnt1; ?>" id="ADDRESS1_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ADDRESS1')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ADDRESS1");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS1", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ADDRESS1_on_update_only
?>
          <?php 
// Show IF Conditional show_ADDRESS2_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ADDRESS2_<?php echo $cnt1; ?>">ADDRESS 2:</label></td>
              <td><input type="text" name="ADDRESS2_<?php echo $cnt1; ?>" id="ADDRESS2_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ADDRESS2')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ADDRESS2");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS2", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ADDRESS2_on_update_only
?>
          <?php 
// Show IF Conditional show_CITY_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="CITY_<?php echo $cnt1; ?>">CITY:</label></td>
              <td><input type="text" name="CITY_<?php echo $cnt1; ?>" id="CITY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('CITY')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("CITY");?> <?php echo $tNGs->displayFieldError("spp_archons", "CITY", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_CITY_on_update_only
?>
          <?php 
// Show IF Conditional show_STATECD_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="STATECD_<?php echo $cnt1; ?>">STATE:</label></td>
              <td><select name="STATECD_<?php echo $cnt1; ?>" id="STATECD_<?php echo $cnt1; ?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                <?php
  while(!$rsStates->EOF){
?>
                <option value="<?php echo $rsStates->Fields('st_cd')?>"<?php if (!(strcmp($rsStates->Fields('st_cd'), $rsspp_archons->Fields('STATECD')))) {echo "SELECTED";} ?>><?php echo $rsStates->Fields('st_nm')?></option>
                <?php
    $rsStates->MoveNext();
  }
  $rsStates->MoveFirst();
?>
              </select>
                  <?php echo $tNGs->displayFieldError("spp_archons", "STATECD", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_STATECD_on_update_only
?>
          <?php 
// Show IF Conditional show_ZIP_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ZIP_<?php echo $cnt1; ?>">ZIP:</label></td>
              <td><input type="text" name="ZIP_<?php echo $cnt1; ?>" id="ZIP_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ZIP')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ZIP");?> <?php echo $tNGs->displayFieldError("spp_archons", "ZIP", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ZIP_on_update_only
?>
          <?php 
// Show IF Conditional show_HOMEPHONE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><a name="birthdate" id="birthdate"></a><label for="HOMEPHONE_<?php echo $cnt1; ?>">HOME PHONE:</label></td>
              <td><input type="text" name="HOMEPHONE_<?php echo $cnt1; ?>" id="HOMEPHONE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('HOMEPHONE')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("HOMEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "HOMEPHONE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_HOMEPHONE_on_update_only
?>
          <?php 
// Show IF Conditional show_EMAIL_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="EMAIL_<?php echo $cnt1; ?>">EMAIL:</label></td>
              <td><input type="text" name="EMAIL_<?php echo $cnt1; ?>" id="EMAIL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('EMAIL')); ?>" size="32" maxlength="125" />
                  <?php echo $tNGs->displayFieldHint("EMAIL");?> <?php echo $tNGs->displayFieldError("spp_archons", "EMAIL", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_EMAIL_on_update_only
?>
          <?php 
// Show IF Conditional show_BIRTHDATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><a name="birthdate"></a><label for="BIRTHDATE_<?php echo $cnt1; ?>">BIRTH DATE:</label></td>
              <td><input name="BIRTHDATE_<?php echo $cnt1; ?>" id="BIRTHDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_archons->Fields('BIRTHDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("BIRTHDATE");?> <?php echo $tNGs->displayFieldError("spp_archons", "BIRTHDATE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_BIRTHDATE_on_update_only
?>
          <?php 
// Show IF Conditional show_DECEASEDDATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><a name="deceaseddate"></a><label for="DECEASEDDATE_<?php echo $cnt1; ?>">DECEASED DATE:</label></td>
              <td><input name="DECEASEDDATE_<?php echo $cnt1; ?>" id="DECEASEDDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_archons->Fields('DECEASEDDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("DECEASEDDATE");?> <?php echo $tNGs->displayFieldError("spp_archons", "DECEASEDDATE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_DECEASEDDATE_on_update_only
?>
          <?php 
// Show IF Conditional show_COMMENT_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="COMMENT_<?php echo $cnt1; ?>">COMMENT:</label></td>
              <td><textarea name="COMMENT_<?php echo $cnt1; ?>" id="COMMENT_<?php echo $cnt1; ?>" cols="80" rows="5"><?php echo KT_escapeAttribute($rsspp_archons->Fields('COMMENT')); ?></textarea>
                  <?php echo $tNGs->displayFieldHint("COMMENT");?> <?php echo $tNGs->displayFieldError("spp_archons", "COMMENT", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_COMMENT_on_update_only
?>
          <?php 
// Show IF Conditional show_GRAMM_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">GRAMMATEUS:</td>
              <td><?php echo KT_escapeAttribute($rsspp_archons->Fields('GRAMM')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_GRAMM_on_update_only
?>
          <?php 
// Show IF Conditional show_GRAMMUPDATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">DATE UPDATED:</td>
              <td><?php echo KT_formatDate($rsspp_archons->Fields('GRAMMUPDATE')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_GRAMMUPDATE_on_update_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_archons_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('kt_pk_spp_archons')); ?>" />
        <?php
    $rsspp_archons->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['CUSTOMERID'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
  <!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsStates->Close();
?>
