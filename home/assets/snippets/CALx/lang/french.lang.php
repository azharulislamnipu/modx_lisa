<?php
		/* ------------------------------------------------------------
		:: Snippet: CALx
		---------------------------------------------------------------
		
		---------------------------------------------------------------
		:: Fichier de langue : FRANCAIS
		--------------------------------------------------------------*/
  	// Liste des mois de l'ann�e
		$month[0] = "janvier" ;
  	$month[1] = "f�vrier" ;
  	$month[2] = "mars" ;
  	$month[3] = "avril" ;
  	$month[4] = "mai" ;
  	$month[5] = "juin" ;
  	$month[6] = "juillet" ;
  	$month[7] = "ao�t" ;
  	$month[8] = "septembre" ;
  	$month[9] = "octobre" ;
  	$month[10] = "novembre" ;
  	$month[11] = "d�cembre" ;

  	// Premi�re lettre des jours de la semaine
  	$day[0] = "lu" ;
  	$day[1] = "ma" ;
  	$day[2] = "me" ;
  	$day[3] = "je" ;
  	$day[4] = "ve" ;
  	$day[5] = "sa" ;
  	$day[6] = "di" ;
 
   	// Noms des jours de la semaine en entier
  	$dayName[0] = "lundi" ;
  	$dayName[1] = "mardi" ;
  	$dayName[2] = "mercredi" ;
  	$dayName[3] = "jeudi" ;
  	$dayName[4] = "vendredi" ;
  	$dayName[5] = "samedi" ;
  	$dayName[6] = "dimanche" ;
		 	
  	// Message d'erreur en cas de date invalide
  	$error01 = "Erreur : date invalide";
  	
  	// Message d'erreur lorsque le calendrier ne peut pas �tre cr�er
  	$msgCannotCreateCALx = "Probl�me lors de la cr�ation du calendrier";
  	
  	// Message pour les info bulles : overlib
  	$closeText="Fermer";
		$titleText="Informations";
		$startStringUniqEvent = "Il y a ";
		$endStringUniqEvent = " �v�nements pr�vus dans la journ�e.";
  	$stringDetails = "Cliquez ici pour plus de d�tails";
  	// Configuration des informations de la page de liens vers le multievent
  	$pagetitle="Listing des �v�nements";
  	$longtitle="Listing des �v�nements";
  	$description="Listing des �v�nements";
  	$introtext="Listing des �v�nements";
  	$eventOf = "Ev�nements du ";
  	$noEvent = "Pas d'�v�nements pour cette journ�e.";
  	$noEventMonth = "Pas d'�v�nements durant ce mois.";
  	
  	// Message pour les info bulles des liens de changements de mois
  	$previousYear="Afficher l'ann�e pr�c�dente";
  	$previousMonth="Afficher le mois pr�c�dent";
  	$nextMonth="Afficher le mois suivant";
  	$nextYear="Afficher l'ann�e suivante";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowAYear="Afficher l'ann�e compl�te";
    
		//Message du lien vers l'ann�e compl�te
  	$msgShowMonthEvents="Afficher tous les �v�nements du mois";
?>