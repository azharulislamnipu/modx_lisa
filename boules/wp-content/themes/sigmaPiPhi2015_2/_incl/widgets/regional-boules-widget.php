<?php
/**
 * Plugin Name: Regional Boules
 * Description: Displays a listing of Boules by region.
 * Version: 1.0
 * Author: Pat Heard
 * Author URI: http://fullahead.org
 */

require_once(dirname(__FILE__) . '/../functions/template-functions.php');

class RegionalBoules_Widget extends WP_Widget {

	function RegionalBoules_Widget() {
		$widget_ops = array( 'classname' => 'regional-boules', 'description' => __('Displays the Boulés, grouped by region', 'archon-network') );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'regional-boules-widget' );
		$this->WP_Widget( 'regional-boules-widget', __('Regional Boules', 'archon-network'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		//Our variables from the widget settings.
		$title = isset($instance['title']) ? apply_filters('widget_title', $instance['title'] ) : 'The Member Boulés';
		$region = isset($instance['region']) ? $instance['region'] : '';
		$showRegional = isset($instance['show_regional']) ? $instance['show_regional'] === 'yes' : false;

		wp_enqueue_script('jquery-ui-accordion');
		wp_enqueue_script('jquery-ui-autocomplete');
		wp_enqueue_style('jquery-ui-css', get_stylesheet_directory_uri() . '/_incl/css/jquery-ui/jquery-ui-1.10.3.custom.min.css');

		echo $before_widget;

		// Display the widget title
		if ($title) {
			echo $before_title . $title . $after_title;
		}

		?><div class="contentBlock"><?php

		//Get the boules by region
		$sites_by_region = group_sites_by_region($region);

		// Accordion view of boules
		if (!empty($sites_by_region)) {
			$site_url = network_site_url();

			?>
			<script>var data = [];</script>
			<div id="accordion">
			<?php
			foreach ($sites_by_region as $region => $boules) {
				// Add the accordion panel for the region ?>
				<h3><?php echo $region ?></h3>
				<ul>
					<?php if ($showRegional) : ?>
						<li class="bold"><a href="<?php echo $site_url . '/' . strtolower($region) ?>" title="<?php _e('Visit regional site', 'archon-network') ?>"><?php echo $region ?> <?php _e('Region Boulé Website', 'archon-network') ?></a></li>
					<?php endif;

				foreach ($boules as $boule) {
					// Add the link to the Boule and push its details into the autocomplete data array ?>
					<li><a href="<?php echo $boule->siteurl ?>" title="<?php _e('Visit Boul&eacute; site', 'archon-network') ?>"><b><?php echo $boule->blogname ?></b> &ndash; <?php echo $boule->city ?>, <?php echo $boule->statecd ?></a></li>
					<script>data.push({label: "<?php echo $boule->blogname ?> - <?php echo $boule->city ?>, <?php echo $boule->statecd ?>", category: "<?php echo $region ?>", url: "<?php echo $boule->siteurl ?>"});</script>	<?php
				}
				?> </ul> <?php
			}
			?> </div>

			<p><input id="autocomplete" title="Search for Boulés" placeholder="Search for Boulés"></p>

			<script>
				(function($) {
					$(function() {

						// Add categories for autocomplete results
						$.widget( "custom.catcomplete", $.ui.autocomplete, {
						_renderMenu: function( ul, items ) {
						  var that = this,
							currentCategory = "";
						  $.each( items, function( index, item ) {
							if ( item.category != currentCategory ) {
							  ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
							  currentCategory = item.category;
							}
							that._renderItemData( ul, item );
						  });
						}
						});

						// Initialize the accordtion and autocomplete
						$("#accordion").accordion({
							collapsible: true,
							active: false,
							heightStyle: 'content',
							create: function( event, ui ) {
								$('#accordion').css({top: 0});
							}
						});
						$( "#autocomplete" ).catcomplete({
							delay: 0,
							source: data,
							select: function (event, ui) {
								if (ui.item && ui.item.url) {
									document.location.href = ui.item.url;
								}
							}
						});

					});
				}(jQuery));
			</script>

			<?php
		} else {
			_e('No Boulé\'s found...', 'archon-network');
		}

		// Autocomplete search of boules

		?></div><?php

		echo $after_widget;
	}

	//Update the widget
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['region'] = strip_tags( $new_instance['region'] );
		$instance['show_regional'] = strip_tags( $new_instance['show_regional'] );
		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'title' => __('Archon Network', 'archon-network'), 'region' => '' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'archon-network'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'region' ); ?>"><?php _e('Show Boulés from:', 'archon-network'); ?></label>
			<select style="width:100%;" id="<?php echo $this->get_field_id( 'region' ); ?>" name="<?php echo $this->get_field_name( 'region' ); ?>">
				<option value="" <?php echo ($instance['region'] === '' ? 'selected="selected"' : ''); ?>><?php _e('All regions', 'archon-network'); ?></option>
				<option value="Central" <?php echo ($instance['region'] === 'Central' ? 'selected="selected"' : ''); ?>><?php _e('Central', 'archon-network'); ?></option>
				<option value="Northeast" <?php echo ($instance['region'] === 'Northeast' ? 'selected="selected"' : ''); ?>><?php _e('Northeast', 'archon-network'); ?></option>
				<option value="Pacific" <?php echo ($instance['region'] === 'Pacific' ? 'selected="selected"' : ''); ?>><?php _e('Pacific', 'archon-network'); ?></option>
				<option value="Southeast" <?php echo ($instance['region'] === 'Southeast' ? 'selected="selected"' : ''); ?>><?php _e('Southeast', 'archon-network'); ?></option>
				<option value="West" <?php echo ($instance['region'] === 'West' ? 'selected="selected"' : ''); ?>><?php _e('West', 'archon-network'); ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'show_regional' ); ?>"><?php _e('Show Regional Boulé sites:', 'archon-network'); ?></label>
			<select style="width:100%;" id="<?php echo $this->get_field_id( 'show_regional' ); ?>" name="<?php echo $this->get_field_name( 'show_regional' ); ?>">
				<option value="yes" <?php echo ($instance['show_regional'] === 'yes' ? 'selected="selected"' : ''); ?>><?php _e('Yes', 'archon-network'); ?></option>
				<option value="no" <?php echo ($instance['show_regional'] === 'no' ? 'selected="selected"' : ''); ?>><?php _e('No', 'archon-network'); ?></option>
			</select>
		</p>


	<?php
	}
}

?>