<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
// Set DEBUG to true for detailed error messages
define('DEBUG', false);
// Max upload size in Megabyte, 0 for auto
define('POST_MAX_SIZE', 0);
// Max size for expanded jpeg image in MByte, 0 for auto
define('MEMORY_LIMIT', 0);
// If image directory needs to be created, attempts to set these permissions
define('IMAGE_DIR_MODE', 0775);
// If thumb directory needs to be created, attempts to set these permissions
define('THUMB_DIR_MODE', 0775);
// Attempt to set permissions on new gallery folders (default is false)
define('NEW_GALLERY_CHMOD', false);
// Permissions for new gallery folders (only if NEW_GALLERY_CHMOD is true)
define('NEW_GALLERY_DIR_MODE', 0755);
// Permissions for files inside new galleries (only if NEW_GALLERY_CHMOD is true)
define('NEW_GALLERY_FILE_MODE', 0644);
// Unlist option attempts to set these permissions
define('DIR_MODE', 0777);
define('FILE_MODE', 0777);
// Uploader attempts to set these permissions for uploaded images
define('UPLOAD_MODE', 0664);
// Sets upload image resize on or off
define('UPLOAD_RESIZE_DEFAULT', false);
// URL for gallery index file can be relative to svManager directory or absolute (default is index.php)
define('INDEX_URL', 'index.php');
// Rebuild overwrites thumbnail files
define('OVERWRITE_THUMBNAILS', true);
// Width of thumbnails in galleries index (px)
define('INDEX_THUMB_WIDTH', 65);
// Height of thumbnails in galleries index (px)
define('INDEX_THUMB_HEIGHT', 65);
// Jpg image quality for resize, max 100
define('IMAGE_QUALITY', 100);
// Override auto html title with fixed text
define('USE_HTML_TITLE', false);
define('HTML_TITLE', 'Gallery');
define('CLONE_TITLE_PREFIX', 'Clone of ');
define('VERSION', '1.7.7, build 110429');
define('MEMORY_SAFETY_FACTOR', 1.9);
define('POST_MAX_SIZE_FALLBACK', '2M');
define('MEMORY_LIMIT_FALLBACK', '8M');
define('UPLOAD_MAX_FILESIZE_FALLBACK', '2M');
define('GALLERY_PREFIX', 'g');
define('PREFERENCES_PATH', 'preferences.txt');
define('GALLERY_DATA_FILE', 'data'.DIRECTORY_SEPARATOR.'gallerydata.txt');
define('GALLERIES_PREFS_FILE', 'data'.DIRECTORY_SEPARATOR.'galleriesprefs.txt');
define('AUTH_FILE', 'data'.DIRECTORY_SEPARATOR.'user.php');
define('SALT', 'osformo');
define('SALT_LENGTH', 9);
define('SESSION_USER', 'svmuser');
define('SESSION_PASS', 'svmpass');
define('SESSION_DEFAULT_PASS', 'svmdefaultpass');
define('SESSION_HASH', 'svmhash');
define('SLEEP_TIME', 1);
define('IMAGE_SRC', 'img'.DIRECTORY_SEPARATOR);
define('CUSTOM_THUMB_DIR', 'customthumbs'.DIRECTORY_SEPARATOR);
define('THUMB_CACHE', 'thumbcache'.DIRECTORY_SEPARATOR);
define('INDEX_THUMB_PREFIX', 't');
define('THUMB_DISPLAY_SIZE', 65);
define('JAVA_CONSOLE_LOG', false);
// Early versions of php5 may not define LOCK_EX.
if (!defined('LOCK_EX')) {define('LOCK_EX', 2);}
// turn-on file locking for file_put_contents(), default is LOCK_EX, to turn-off set to zero
define('FPC_LOCK', LOCK_EX);
?>