<?php
/**
 * Flexible Posts Widget: Old Default widget template
 * 
 * @since 1.0.0
 *
 * This is the ORIGINAL default template used by the plugin.
 * There is a new default template (default.php) that will be 
 * used by default if no template was specified in a widget.
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

echo $before_widget;

if ( !empty($title) )
	echo $before_title . $title . $after_title;



if( $flexible_posts->have_posts() ):
?>
	<ul class="dpe-flexible-posts">
	<?php while( $flexible_posts->have_posts() ) : $flexible_posts->the_post(); global $post; ?>
		<section class="span-4 align-center"><div class="inner"><div class="rounded">
			<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<a href="<?php echo the_permalink(); ?>">
				<?php
					if( $thumbnail == true ) {
						// If the post has a feature image, show it
						if( has_post_thumbnail() ) {
							the_post_thumbnail( $thumbsize );
						// Else if the post has a mime type that starts with "image/" then show the image directly.
						} elseif( 'image/' == substr( $post->post_mime_type, 0, 6 ) ) {
							echo wp_get_attachment_image( $post->ID, $thumbsize );
						}
					}
				?>
				</a>
				<h2><?php the_title(); ?></h2>
				<p><?php the_excerpt(); ?></p>
				<?php $key_1_value = get_post_meta( get_the_ID(), 'teasertext', true ); ?>
					<p><strong><a href="<?php echo the_permalink(); ?>"><?php
if (!empty($key_1_value)) {
	echo $key_1_value;
} else {
echo 'Read More';	
}
?></a></strong></p>
					<?php //echo get_post_meta( get_the_ID(), 'teasertext', true ); ?>
					<?php //echo the_meta( get_the_ID(), 'teasertext', true ); ?>
			</a>
		</li>
		</div></div></section>
	<?php endwhile; ?>
	</ul><!-- .dpe-flexible-posts -->
<?php else: // We have no posts ?>
	<div class="dpe-flexible-posts no-posts">
		<p><?php _e( 'No post found', 'flexible-posts-widget' ); ?></p>
	</div>
<?php	
endif; // End have_posts()
	
echo $after_widget;
