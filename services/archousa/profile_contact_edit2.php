<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("ADDRESS1", true, "text", "", "", "", "");
$formValidation->addField("CITY", true, "text", "", "", "", "");
$formValidation->addField("STATECD", true, "text", "", "", "", "");
$formValidation->addField("ZIP", true, "text", "", "", "", "");
$formValidation->addField("HOMEPHONE", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsState = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsState = mysql_query($query_rsState, $sigma_modx) or die(mysql_error());
$row_rsState = mysql_fetch_assoc($rsState);
$totalRows_rsState = mysql_num_rows($rsState);

// Make an update transaction instance
$upd_spp_archons = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-profile.php");
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("ADDRESS1", "STRING_TYPE", "POST", "ADDRESS1");
$upd_spp_archons->addColumn("ADDRESS2", "STRING_TYPE", "POST", "ADDRESS2");
$upd_spp_archons->addColumn("CITY", "STRING_TYPE", "POST", "CITY");
$upd_spp_archons->addColumn("STATECD", "STRING_TYPE", "POST", "STATECD");
$upd_spp_archons->addColumn("ZIP", "STRING_TYPE", "POST", "ZIP");
$upd_spp_archons->addColumn("HOMEPHONE", "STRING_TYPE", "POST", "HOMEPHONE");
$upd_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "{NOW_DT");
$upd_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_archons->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$row_rsspp_archons = mysql_fetch_assoc($rsspp_archons);
$totalRows_rsspp_archons = mysql_num_rows($rsspp_archons);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow" style="text-transform:uppercase;">Page Name</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="ADDRESS1">ADDRESS1:</label></td>
        <td><input type="text" name="ADDRESS1" id="ADDRESS1" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ADDRESS1']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ADDRESS1");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS1"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ADDRESS2">ADDRESS2:</label></td>
        <td><input type="text" name="ADDRESS2" id="ADDRESS2" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ADDRESS2']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ADDRESS2");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS2"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="CITY">CITY:</label></td>
        <td><input type="text" name="CITY" id="CITY" value="<?php echo KT_escapeAttribute($row_rsspp_archons['CITY']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("CITY");?> <?php echo $tNGs->displayFieldError("spp_archons", "CITY"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="STATECD">STATE:</label></td>
        <td><select name="STATECD" id="STATECD">
            <?php 
do {  
?>
            <option value="<?php echo $row_rsState['st_cd']?>"<?php if (!(strcmp($row_rsState['st_cd'], $row_rsspp_archons['STATECD']))) {echo "SELECTED";} ?>><?php echo $row_rsState['st_nm']?></option>
            <?php
} while ($row_rsState = mysql_fetch_assoc($rsState));
  $rows = mysql_num_rows($rsState);
  if($rows > 0) {
      mysql_data_seek($rsState, 0);
	  $row_rsState = mysql_fetch_assoc($rsState);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archons", "STATECD"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ZIP">ZIP:</label></td>
        <td><input type="text" name="ZIP" id="ZIP" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ZIP']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ZIP");?> <?php echo $tNGs->displayFieldError("spp_archons", "ZIP"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="HOMEPHONE">HOME PHONE:</label></td>
        <td><input type="text" name="HOMEPHONE" id="HOMEPHONE" value="<?php echo KT_escapeAttribute($row_rsspp_archons['HOMEPHONE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("HOMEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "HOMEPHONE"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Update Address" /> <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/archousa-profile.php')" />
        </td>
      </tr>
    </table>
    <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo KT_formatDate($row_rsspp_archons['LASTUPDATED']); ?>" />
    <input name="CUSTOMERID" type="hidden" id="CUSTOMERID" value="<?php echo KT_escapeAttribute($_POST['CUSTOMERID']); ?>" />
    <input type="hidden" name="UPDATEDBY" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($row_rsspp_archons['UPDATEDBY']); ?>" />
  </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsState);
?>
