/**************************************************************
   AUTHOR:  Pat Heard (fullahead.org)
   DATE:    2008.08.12
   PURPOSE: Generic functions
 **************************************************************/

// Set the width on all caption containers to their contained image's width
function initCaptions(){
  var captions = getElementsByClass("caption");
  for(var i = 0; i < captions.length; i++) {
    var img = captions[i].getElementsByTagName('IMG');
    if(img && img.length > 0)
      captions[i].style.width = img[0].offsetWidth + 'px';
  }
}


// Get elements by className
function getElementsByClass(searchClass,node,tag) {
  var classElements = new Array();
  if ( node == null )
    node = document;
  if ( tag == null )
    tag = '*';
  var els = node.getElementsByTagName(tag);
  var elsLen = els.length;
  var pattern = new RegExp("(^|\\\\s)"+searchClass+"(\\\\s|$)");
  for (i = 0, j = 0; i < elsLen; i++) {
    if ( pattern.test(els[i].className) ) {
      classElements[j] = els[i];
      j++;
    }
  }
  return classElements;
}