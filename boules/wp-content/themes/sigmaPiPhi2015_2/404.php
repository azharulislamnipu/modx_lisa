<?php get_header(); ?>

	<div id="content">
		<div class="padder one-column width padding-top-none-20">
			<?php do_action( 'sigma_before_404' ); ?>
			
			<div class="row grid-12">
				
		   	 <div class="span-12 float-right">
		   		 <div class="inner">
			<div id="post-0" class="post page-404 error404 not-found" role="main">
				<h2 class="purple color"><?php _e( "Page not found", 'archon-network' ); ?></h2>
				<div class="contentBlock">
					<p><?php _e( "We're sorry, but we can't find the page that you're looking for. Perhaps searching will help.", 'archon-network' ); ?></p>
					<?php get_search_form(); ?>
				</div>

				<?php do_action( 'sigma_404' ); ?>
			</div>

			<?php do_action( 'sigma_after_404' ); ?>
		</div><!-- .padder -->
	</div><!-- #content -->
	
	</div><!-- .inner -->
	</div><!-- .span-12 -->
	</div><!-- #row grid 12 round images  -->
	

<?php get_footer(); ?>