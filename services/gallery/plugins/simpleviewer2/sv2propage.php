<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from <tr>
      <td class="label">'.@link http://www.airtightinteractive.com/viewers/->label().'</td><td>'.@link http://www.airtightinteractive.com/viewers/->text($tab).'</td>
    </tr>
      
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie <tr>
      <td class="label">'.@link http://www.jhardie.com->label().'</td><td>'.@link http://www.jhardie.com->text($tab).'</td>
    </tr>
      
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Customize page
 *
 * @package svManager
 */
class Sv2ProPage extends Page
{
 /**
  * @var string overrides some of head content in Page
  */
  var $customHeadHtml = '
       <link href="colorjack/plugin.css" rel="stylesheet" type="text/css" />
       <style type="text/css">
         #plugin {
           color: #FFFFFF;
           background-color: #999999;
         }
         #H {
           border-color: #999999;
         }
         #plugCLOSE {
           color: #FFFFFF;
           background-color: #999999;
         }
       </style>
       <script src="scripts/mootools-core.js" type="text/javascript"></script>
       <script src="scripts/mootools-more.js" type="text/javascript"></script>
       <script src="colorjack/plugin.js" type="text/javaScript"></script>
       <script src="scripts/customize.js" type="text/javascript"></script>
       <script src="scripts/mooformcheck.js" type="text/javascript"></script>
';
 
 /**
  * constructs Sv2ProPage class
  * @param string contains text for html <title></title> tags
  * @param string id for html body tag
  * @param string class for html body tag
  */
  function Sv2ProPage()
  {
    parent::Page('svManager Pro &ndash; settings', 'sv2pro', 'navpro simpleviewer2 pro');
  }

 /**
  * get html for pro customize page
  *
  * Note hack in conditional comments from
  * @link http://www.hedgerwow.com/360/bugs/css-select-free.html
  * to prevent select elements showing above the colorpicker in IE6Win
  * @access public
  * @return string html
  * @param object gallery set
  * @param array gallery attributes
  */
  function getProHtml(&$gallerySet, &$gallery)
  {
    $prefs = $gallery->getPreferences();
    $title = htmlspecialchars($prefs['title'], ENT_QUOTES, 'UTF-8');
    $tab = 0;
    $k = 'stageBorder';
    $$k = new FormField('Stage border', $k, $prefs[$k]);
    $k = 'stageHPadding';
    $$k = new FormField('Stage horizontal padding', $k, $prefs[$k]);
    $k = 'stageVPadding';
    $$k = new FormField('Stage vertical padding', $k, $prefs[$k]);
    $k = 'stageVAlign';
    $$k = new FormField('Stage vertical align', $k, $prefs[$k]);
    $k = 'stageHAlign';
    $$k = new FormField('Stage horizontal align', $k, $prefs[$k]);
    $k = 'lockTopStack';
    $$k = new FormField('Lock top stack', $k, $prefs[$k]);
    $k = 'lockBottomStack';
    $$k = new FormField('Lock bottom stack', $k, $prefs[$k]);
    $k = 'enableKeyboardControls';
    $$k = new FormField('Enable keyboard controls', $k, $prefs[$k]);
    $k = 'enableMouseWheel';
    $$k = new FormField('Enable mouse wheel', $k, $prefs[$k]);
    $k = 'firstImageIndex';
    $$k = new FormField('First image index', $k, $prefs[$k]);
    $k = 'randomizeImages';
    $$k = new FormField('Randomize images', $k, $prefs[$k]);
    $k = 'enableAPI';
    $$k = new FormField('Enable API', $k, $prefs[$k]);
    $k = 'dropShadowStrength';
    $$k = new FormField('Drop shadow strength (0–1)', $k, $prefs[$k]);
    $k = 'useColorCorrection';
    $$k = new FormField('Use color correction', $k, $prefs[$k]);
    
// Main image
    $k = 'frameColor';
    $$k = new FormField('Frame color', $k, $prefs[$k]);
    $k = 'frameWidth';
    $$k = new FormField('Frame width', $k, $prefs[$k]);
    $k = 'maxImageWidth';
    $$k = new FormField('Max image width', $k, $prefs[$k]);
    $k = 'maxImageHeight';
    $$k = new FormField('Max image height', $k, $prefs[$k]);
    $k = 'imageClickMode';
    $$k = new FormField('Action on image click', $k, $prefs[$k]);
    $k = 'imageScaleMode';
    $$k = new FormField('Image scaling', $k, $prefs[$k]);
    $k = 'imagePreloading';
    $$k = new FormField('Image preload', $k, $prefs[$k]);
    $k = 'imageSmoothing';
    $$k = new FormField('Image smoothing', $k, $prefs[$k]);
    $k = 'imageFrameStyle';
    $$k = new FormField('Image frame style', $k, $prefs[$k]);
    $k = 'imageCornerRadius';
    $$k = new FormField('Image corner radius', $k, $prefs[$k]);
    $k = 'imageDropShadow';
    $$k = new FormField('Drop shadow', $k, $prefs[$k]);
    $k = 'imageTransitionType';
    $$k = new FormField('Transition', $k, $prefs[$k]);
    $k = 'imageTransitionTime';
    $$k = new FormField('Transition time (secs)', $k, $prefs[$k]);
    $k = 'flashBrightness';
    $$k = new FormField('Flash brightness (0–2)', $k, $prefs[$k]);
    $k = 'imageHAlign';
    $$k = new FormField('Horizontal align', $k, $prefs[$k]); 
    $k = 'imageVAlign';
    $$k = new FormField('Vertical align', $k, $prefs[$k]);
    $k = 'imageBackColor';
    $$k = new FormField('Image back color', $k, $prefs[$k]);
    $k = 'imageBackOpacity';
    $$k = new FormField('Image back opacity (0–1)', $k, $prefs[$k]);
    
// Image overlay
    $k = 'showOverlay';
    $$k = new FormField('Show overlay', $k, $prefs[$k]);
    $k = 'showImageNav';
    $$k = new FormField('Show image nav', $k, $prefs[$k]);
    $k = 'imageNavStyle';
    $$k = new FormField('Image nav style', $k, $prefs[$k]);
    $k = 'imageNavScale';
    $$k = new FormField('Image nav scale', $k, $prefs[$k]);
    $k = 'imageNavColor';
    $$k = new FormField('Image nav color', $k, $prefs[$k]);
    $k = 'imageNavBackColor';
    $$k = new FormField('Image nav back color', $k, $prefs[$k]);
    $k = 'imageNavBackOpacity';
    $$k = new FormField('Image nav back opacity', $k, $prefs[$k]);
    $k = 'imageNavPadding';
    $$k = new FormField('Image nav padding', $k, $prefs[$k]);
    $k = 'showBigPlayButton';
    $$k = new FormField('Show big play button', $k, $prefs[$k]);
    $k = 'hideOverlayOnTransition';
    $$k = new FormField('Hide overlay on transition', $k, $prefs[$k]);
    $k = 'overlayFadeTime';
    $$k = new FormField('Overlay fade time', $k, $prefs[$k]);
    
    // Thumbnails
    $k = 'thumbPosition';
    $p = 'Thumb position';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbColumns';
    $p = 'Thumb columns';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbRows';
    $p = 'Thumb Rows';
    $$k = new FormField($p, $k, $prefs[$k]);

    $k = 'thumbWidth';
    $p = 'Width of thumbnail images (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbHeight';
    $p = 'Height of thumbnail images (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbPadding';
    $p = 'Space between thumbs (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbFrameColor';
    $p = 'Color of thumbnail frames';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbFrameHoverColor';
    $p = 'Color of thumbnail frames on hover';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbFrameStyle';
    $p = 'Thumbnail frame style';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbCornerRadius';
    $p = 'Rounded corner radius';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbFrameWidth';
    $p = 'Width of frame (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbHoverFrameWidth';
    $p = 'Width of thumbnail frame on hover (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showThumbVisited';
    $p = 'Show thumb visited';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showThumbLoadBar';
    $p = 'Show thumb load bar';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbLoadBarHeight';
    $p = 'height of load bar (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbClickShift';
    $p = 'Thumb click shift (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbDropShadow';
    $p = 'Thumb drop shadow';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbHAlign';
    $p = 'Thumb H align';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbVAlign';
    $p = 'Thumb V align';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbPreloading';
    $p = 'Thumb preloading';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbLoadBrightness';
    $p = 'Thumb load brightness (0–2)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'doThumbPopOut';
    $p = 'Animate thumb load';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'changeCaptionOnHover';
    $p = 'Change caption on hover';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'changeImageOnHover';
    $p = 'Change main image on hover';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Thumbnail Navigation
    
    $k = 'thumbNavStyle';
    $p = 'Thumb nav style';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbNavPosition';
    $p = 'Thumb nav position';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbNavColor';
    $p = 'Thumb nav color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbNavHoverColor';
    $p = 'Thumb nav hover color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbNavBackColor';
    $p = 'Thumb nav back color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbNavBackOpacity';
    $p = 'Thumb nav back opacity (0–1)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbNavPadding';
    $p = 'Thumb nav padding';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbNavSlideTime';
    $p = 'Thumb nav slide time (secs)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    
    // Button bar
    $k = 'showOpenButton';
    $p = 'Show open button';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showFullscreenButton';
    $p = 'Show fullscreen button';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    
    $k = 'buttonBarPosition';
    $p = 'Button bar position ';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showDownloadButton';
    $p = 'Show download button';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showNavButtons';
    $p = 'Show nav buttons';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showAutoPlayButton';
    $p = 'Show autoplay button.';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showAudioButton';
    $p = 'Show audio toggle button.';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showToolTips';
    $p = 'Show button tooltips';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarHAlign';
    $p = 'Button bar H align';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarVAlign';
    $p = 'Button bar V align';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarPadding';
    $p = 'Button bar padding';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarColor';
    $p = 'Button bar color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarHoverColor';
    $p = 'Button bar hover color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarBackColor';
    $p = 'Button bar back color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarBackOpacity';
    $p = 'Button bar back opacity 0–1';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarScale';
    $p = 'Button bar scale factor';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'floatButtonBar';
    $p = 'Float button bar';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Title
    
    $k = 'titlePosition';
    $p = 'Title position';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleFontName';
    $p = 'Title font name(s)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleFontColor';
    $p = 'Title font color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleFontSize';
    $p = 'Title font size';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titlePadding';
    $p = 'Title padding (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleBackColor';
    $p = 'Title background color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleBackOpacity';
    $p = 'Title background opacity (0–1)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleWidth';
    $p = 'Title width';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleHeight';
    $p = 'Title height';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleDropShadow';
    $p = 'Title drop shadow';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'floatTitle';
    $p = 'Float title';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleTextAlignment';
    $p = 'Title text alignment';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Caption
    
    $k = 'captionPosition';
    $p = 'Caption position';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionFontName';
    $p = 'Caption font name(s)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionFontColor';
    $p = 'Caption font color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionFontSize';
    $p = 'Caption font size';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showImageNumber';
    $p = 'Show image number';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'imageNumberFontSize';
    $p = 'Image number font size';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionPadding';
    $p = 'Caption padding (px)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionTextAlignment';
    $p = 'Caption text alignment';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionBackColor';
    $p = 'Caption background color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionBackOpacity';
    $p = 'Caption back opacity (0–1)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionWidth';
    $p = 'Caption width inc padding';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionHeight';
    $p = 'Caption height inc padding';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionDropShadow';
    $p = 'Caption drop shadow';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'enableCaptionLinks';
    $p = 'Enable caption links';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'floatCaption';
    $p = 'Float caption';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // AutoPlay
    
    $k = 'autoPlayOnLoad';
    $p = 'Autoplay on load';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'displayTime';
    $p = 'Display time (secs)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'showAutoPlayStatus';
    $p = 'Show Autoplay on/off';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'goNextOnAutoPlay';
    $p = 'Go next on auto play';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'autoPlayThumbs';
    $p = 'Autoplay navigate thumbs';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'enableLooping';
    $p = 'Enable looping';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    
    // Audio
    
    $k = 'audioURL';
    $p = 'Audio url';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'loopAudio';
    $p = 'Loop audio';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'playAudioOnLoad';
    $p = 'Play audio on load';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'audioVolume';
    $p = 'Audio volume';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Background Image
    
    $k = 'backgroundURL';
    $p = 'Background image url';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backgroundScale';
    $p = 'Scale background image ';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Watermark
    
    $k = 'watermarkURL';
    $p = 'Watermark url';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'watermarkHAlign';
    $p = 'Watermark H align';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'watermarkVAlign';
    $p = 'Watermark V align';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'watermarkPadding';
    $p = 'Watermark padding';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'watermarkLinkURL';
    $p = 'Watermark link url';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'watermarkLinkTarget';
    $p = 'Watermark link Target';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Flickr
    $k = 'useFlickr';
    $p = 'Use Flickr';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrUserName';
    $p = 'Flickr user name';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrTags';
    $p = 'Flickr tags';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrUserId';
    $p = 'Flickr user ID';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrSetId';
    $p = 'Flickr photoset ID';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrGroupId';
    $p = 'Flickr group ID';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrTagMode';
    $p = 'Flickr tag mode ';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrSort';
    $p = 'Flickr sort';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrImageSize';
    $p = 'Flickr image size';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrImageCount';
    $p = 'Flickr image count (1–500)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrExtraParams';
    $p = 'Flickr extra parameters';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrShowTitle';
    $p = 'Flickr show title';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrShowDescription';
    $p = 'Flickr show description';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrShowPageLink';
    $p = 'Flickr show page link';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrPageLinkText';
    $p = 'Flickr page link text';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrTitleFontName';
    $p = 'Flickr title font(s)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrTitleFontColor';
    $p = 'Flickr title font color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'flickrTitleFontSize';
    $p = 'Flickr Title font size';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Back button
    
    $k = 'showBackButton';
    $p = 'Show back button';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonText';
    $p = 'Back button text';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonURL';
    $p = 'Back button url';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonFontColor';
    $p = 'Back button font color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonFontSize';
    $p = 'Back button font size';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonFontName';
    $p = 'Back button font name';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonBackColor';
    $p = 'Back button back color';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonBackOpacity';
    $p = 'Back button back opacity (0–1)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonX';
    $p = 'Back button X (left)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'backButtonY';
    $p = 'Back button Y (top)';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Fixed layout
    
    $k = 'useFixedLayout';
    $p = 'Use fixed layout';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'imageAreaX';
    $p = 'Image area X';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'imageAreaY';
    $p = 'Image area Y';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'imageAreaWidth';
    $p = 'Image area width';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'imageAreaHeight';
    $p = 'Image area height';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbAreaX';
    $p = 'Thumb area X';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'thumbAreaY';
    $p = 'Thumb area Y';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionX';
    $p = 'Caption X';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'captionY';
    $p = 'Caption Y';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleX';
    $p = 'Title X';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'titleY';
    $p = 'Title Y';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarX';
    $p = 'Button bar X';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'buttonBarY';
    $p = 'Button Bar Y';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    // Mobile
    
    $k = 'mobileShowNav';
    $p = 'Mobile Show Nav';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'mobileShowThumbs';
    $p = 'Mobile Show Thumbs';
    $$k = new FormField($p, $k, $prefs[$k]);
    
    $k = 'mobileShowCaption';
    $p = 'Mobile Show Caption';
    $$k = new FormField($p, $k, $prefs[$k]);

$html = $this->getColorjackHtml();
$html .= '
<form class="public" action = "'.$_SERVER['PHP_SELF'].'" id="customizeform" method="post">
  <div class="elementwrap">
  <h3 class="togglers">SimpleViewer Pro Settings</h3>
  <div class="accordionElements">
    <p>SimpleViewer <em>Pro</em> galleries only. Compatible with svBuilder.</p>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">General</h3>
  <div class="accordionElements">
  <table cellspacing="0">
    <tr>
      <td class="label">'.$stageBorder->label().'</td><td>'.$stageBorder->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$stageHPadding->label().'</td><td>'.$stageHPadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$stageVPadding->label().'</td><td>'.$stageVPadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$stageHAlign->label().'</td><td>'.$stageHAlign->select($tab, array('LEFT', 'CENTER', 'RIGHT')).'</td>
    </tr>
    <tr>
      <td class="label">'.$stageVAlign->label().'</td><td>'.$stageVAlign->select($tab, array('TOP', 'CENTER', 'BOTTOM')).'</td>
    </tr>
    <tr>
      <td class="label">'.$lockTopStack->label().'</td><td>'.$lockTopStack->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$lockBottomStack->label().'</td><td>'.$lockBottomStack->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$enableLooping->label().'</td><td>'.$enableLooping->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$enableKeyboardControls->label().'</td><td>'.$enableKeyboardControls->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$enableMouseWheel->label().'</td><td>'.$enableMouseWheel->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$firstImageIndex->label().'</td><td>'.$firstImageIndex->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$randomizeImages->label().'</td><td>'.$randomizeImages->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$enableAPI->label().'</td><td>'.$enableAPI->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$dropShadowStrength->label().'</td><td>'.$dropShadowStrength->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$useColorCorrection->label().'</td><td>'.$useColorCorrection->checkbox($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Main Image</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$frameColor->label().'</td><td>'.$frameColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$frameWidth->label().'</td><td>'.$frameWidth->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$maxImageWidth->label().'</td><td>'.$maxImageWidth->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$maxImageHeight->label().'</td><td>'.$maxImageHeight->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageFrameStyle->label().'</td><td>'.$imageFrameStyle->select($tab, array('SQUARE', 'ROUNDED', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageCornerRadius->label().'</td><td>'.$imageCornerRadius->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageClickMode->label().'</td><td>'.$imageClickMode->select($tab, array('NAVIGATE', 'OPEN_URL', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageScaleMode->label().'</td><td>'.$imageScaleMode->select($tab, array('SCALE', 'SCALE_UP', 'FILL', 'STRETCH', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageDropShadow->label().'</td><td>'.$imageDropShadow->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageTransitionType->label().'</td><td>'.$imageTransitionType->select($tab, array('FADE', 'CROSS_FADE', 'SLIDE', 'FLASH', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageTransitionTime->label().'</td><td>'.$imageTransitionTime->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flashBrightness->label().'</td><td>'.$flashBrightness->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageHAlign->label().'</td><td>'.$imageHAlign->select($tab, array('CENTER', 'LEFT', 'RIGHT')).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageVAlign->label().'</td><td>'.$imageVAlign->select($tab, array('CENTER', 'TOP', 'BOTTOM')).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageBackColor->label().'</td><td>'.$imageBackColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageBackOpacity->label().'</td><td>'.$imageBackOpacity->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageSmoothing->label().'</td><td>'.$imageSmoothing->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imagePreloading->label().'</td><td>'.$imagePreloading->select($tab, array('PAGE', 'NEXT', 'ALL', 'NONE')).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Image overlay</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$showOverlay->label().'</td><td>'.$showOverlay->select($tab, array('HOVER', 'ALWAYS', 'NEVER')).'</td>
    </tr>
    <tr>
      <td class="label">'.$showImageNav->label().'</td><td>'.$showImageNav->select($tab, array('HOVER', 'ALWAYS', 'NEVER')).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageNavStyle->label().'</td><td>'.$imageNavStyle->select($tab, array('CURSOR', 'BIG', 'CIRCLE', 'SQUARE', 'CLASSIC', 'TRIANGLE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageNavScale->label().'</td><td>'.$imageNavScale->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageNavColor->label().'</td><td>'.$imageNavColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageNavBackColor->label().'</td><td>'.$imageNavBackColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageNavBackOpacity->label().'</td><td>'.$imageNavBackOpacity->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageNavPadding->label().'</td><td>'.$imageNavPadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showBigPlayButton->label().'</td><td>'.$showBigPlayButton->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$hideOverlayOnTransition->label().'</td><td>'.$hideOverlayOnTransition->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$overlayFadeTime->label().'</td><td>'.$overlayFadeTime->text($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Thumbnails</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$thumbPosition->label().'</td><td>'.$thumbPosition->select($tab, array('STAGE_TOP', 'TOP', 'BOTTOM', 'LEFT', 'RIGHT', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbColumns->label().'</td><td>'.$thumbColumns->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbRows->label().'</td><td>'.$thumbRows->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbWidth->label().'</td><td>'.$thumbWidth->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbHeight->label().'</td><td>'.$thumbHeight->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbPadding->label().'</td><td>'.$thumbPadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbFrameColor->label().'</td><td>'.$thumbFrameColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbFrameHoverColor->label().'</td><td>'.$thumbFrameHoverColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbHAlign->label().'</td><td>'.$thumbHAlign->select($tab, array('CENTER', 'LEFT', 'RIGHT')).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbVAlign->label().'</td><td>'.$thumbVAlign->select($tab, array('CENTER', 'TOP', 'BOTTOM')).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbDropShadow->label().'</td><td>'.$thumbDropShadow->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showThumbVisited->label().'</td><td>'.$showThumbVisited->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showThumbLoadBar->label().'</td><td>'.$showThumbLoadBar->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbLoadBarHeight->label().'</td><td>'.$thumbLoadBarHeight->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbFrameWidth->label().'</td><td>'.$thumbFrameWidth->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbHoverFrameWidth->label().'</td><td>'.$thumbHoverFrameWidth->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbClickShift->label().'</td><td>'.$thumbClickShift->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbFrameStyle->label().'</td><td>'.$thumbFrameStyle->select($tab, array('SQUARE', 'ROUNDED', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbCornerRadius->label().'</td><td>'.$thumbCornerRadius->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbLoadBrightness->label().'</td><td>'.$thumbLoadBrightness->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$doThumbPopOut->label().'</td><td>'.$doThumbPopOut->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$changeCaptionOnHover->label().'</td><td>'.$changeCaptionOnHover->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$changeImageOnHover->label().'</td><td>'.$changeImageOnHover->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbPreloading->label().'</td><td>'.$thumbPreloading->select($tab, array('PAGE', 'ALL')).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Thumbnail navigation</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$thumbNavStyle->label().'</td><td>'.$thumbNavStyle->select($tab, array('CIRCLE', 'CLASSIC', 'BIG', 'SQUARE', 'TRIANGLE', 'NUMERIC', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbNavPosition->label().'</td><td>'.$thumbNavPosition->select($tab, array('CENTER', 'BOTTOM')).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbNavColor->label().'</td><td>'.$thumbNavColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbNavHoverColor->label().'</td><td>'.$thumbNavHoverColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbNavBackColor->label().'</td><td>'.$thumbNavBackColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbNavBackOpacity->label().'</td><td>'.$thumbNavBackOpacity->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbNavPadding->label().'</td><td>'.$thumbNavPadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$thumbNavSlideTime->label().'</td><td>'.$thumbNavSlideTime->text($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Button bar</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$showOpenButton->label().'</td><td>'.$showOpenButton->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showFullscreenButton->label().'</td><td>'.$showFullscreenButton->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarPosition->label().'</td><td>'.$buttonBarPosition->select($tab, array('STAGE_TOP', 'TOP', 'BOTTOM', 'LEFT', 'RIGHT', 'OVERLAY', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarHAlign->label().'</td><td>'.$buttonBarHAlign->select($tab, array('CENTER', 'LEFT', 'RIGHT')).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarVAlign->label().'</td><td>'.$buttonBarVAlign->select($tab, array('CENTER', 'TOP', 'BOTTOM')).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarPadding->label().'</td><td>'.$buttonBarPadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showDownloadButton->label().'</td><td>'.$showDownloadButton->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showNavButtons->label().'</td><td>'.$showNavButtons->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showAutoPlayButton->label().'</td><td>'.$showAutoPlayButton->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showAudioButton->label().'</td><td>'.$showAudioButton->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showToolTips->label().'</td><td>'.$showToolTips->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarColor->label().'</td><td>'.$buttonBarColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarHoverColor->label().'</td><td>'.$buttonBarHoverColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarBackColor->label().'</td><td>'.$buttonBarBackColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarBackOpacity->label().'</td><td>'.$buttonBarBackOpacity->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$buttonBarScale->label().'</td><td>'.$buttonBarScale->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$floatButtonBar->label().'</td><td>'.$floatButtonBar->checkbox($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Title</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$titlePosition->label().'</td><td>'.$titlePosition->select($tab, array('TOP', 'BOTTOM', 'RIGHT', 'LEFT', 'OVERLAY_TOP', 'OVERLAY_BOTTOM', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$floatTitle->label().'</td><td>'.$floatTitle->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$titleFontName->label().'</td><td>'.$titleFontName->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$titleFontColor->label().'</td><td>'.$titleFontColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$titleFontSize->label().'</td><td>'.$titleFontSize->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$titlePadding->label().'</td><td>'.$titlePadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$titleTextAlignment->label().'</td><td>'.$titleTextAlignment->select($tab, array('LEFT', 'RIGHT', 'CENTER')).'</td>
    </tr>
    <tr>
      <td class="label">'.$titleBackColor->label().'</td><td>'.$titleBackColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$titleBackOpacity->label().'</td><td>'.$titleBackOpacity->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$titleWidth->label().'</td><td>'.$titleWidth->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$titleDropShadow->label().'</td><td>'.$titleDropShadow->checkbox($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Captions</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$captionPosition->label().'</td><td>'.$captionPosition->select($tab, array('TOP', 'BOTTOM', 'RIGHT', 'LEFT', 'OVERLAY_TOP', 'OVERLAY_BOTTOM', 'NONE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionFontName->label().'</td><td>'.$captionFontName->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionFontSize->label().'</td><td>'.$captionFontSize->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showImageNumber->label().'</td><td>'.$showImageNumber->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$imageNumberFontSize->label().'</td><td>'.$imageNumberFontSize->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionPadding->label().'</td><td>'.$captionPadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionTextAlignment->label().'</td><td>'.$captionTextAlignment->select($tab, array('LEFT', 'RIGHT', 'CENTER')).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionFontColor->label().'</td><td>'.$captionFontColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionBackColor->label().'</td><td>'.$captionBackColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionBackOpacity->label().'</td><td>'.$captionBackOpacity->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionWidth->label().'</td><td>'.$captionWidth->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionHeight->label().'</td><td>'.$captionHeight->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$captionDropShadow->label().'</td><td>'.$captionDropShadow->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$enableCaptionLinks->label().'</td><td>'.$enableCaptionLinks->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$floatCaption->label().'</td><td>'.$floatCaption->checkbox($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Autoplay</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$autoPlayOnLoad->label().'</td><td>'.$autoPlayOnLoad->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$displayTime->label().'</td><td>'.$displayTime->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$goNextOnAutoPlay->label().'</td><td>'.$goNextOnAutoPlay->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$showAutoPlayStatus->label().'</td><td>'.$showAutoPlayStatus->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$autoPlayThumbs->label().'</td><td>'.$autoPlayThumbs->checkbox($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Audio</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$audioURL->label().'</td><td>'.$audioURL->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$loopAudio->label().'</td><td>'.$loopAudio->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$playAudioOnLoad->label().'</td><td>'.$playAudioOnLoad->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$audioVolume->label().'</td><td>'.$audioVolume->text($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Background image</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$backgroundURL->label().'</td><td>'.$backgroundURL->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backgroundScale->label().'</td><td>'.$backgroundScale->select($tab, array('FILL', 'STRETCH', 'NONE')).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Watermark</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$watermarkURL->label().'</td><td>'.$watermarkURL->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$watermarkHAlign->label().'</td><td>'.$watermarkHAlign->select($tab, array('CENTER', 'LEFT', 'RIGHT')).'</td>
    </tr>
    <tr>
      <td class="label">'.$watermarkVAlign->label().'</td><td>'.$watermarkVAlign->select($tab, array('CENTER', 'TOP', 'BOTTOM')).'</td>
    </tr>
    <tr>
      <td class="label">'.$watermarkPadding->label().'</td><td>'.$watermarkPadding->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$watermarkLinkURL->label().'</td><td>'.$watermarkLinkURL->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$watermarkLinkTarget->label().'</td><td>'.$watermarkLinkTarget->text($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Flickr</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$useFlickr->label().'</td><td>'.$useFlickr->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrUserName->label().'</td><td>'.$flickrUserName->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrTags->label().'</td><td>'.$flickrTags->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrUserId->label().'</td><td>'.$flickrUserId->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrSetId->label().'</td><td>'.$flickrSetId->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrGroupId->label().'</td><td>'.$flickrGroupId->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrTagMode->label().'</td><td>'.$flickrTagMode->select($tab, array('ALL', 'ANY')).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrSort->label().'</td><td>'.$flickrSort->select($tab, array('DATE-POSTED-DESC', 'DATE-POSTED-DESC', 'DATE-POSTED-ASC', 'DATE-TAKEN-ASC', 'DATE-TAKEN-DESC', 'INTERESTINGNESS-DESC', 'INTERESTINGNESS-ASC', 'RELEVANCE')).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrImageCount->label().'</td><td>'.$flickrImageCount->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrImageSize->label().'</td><td>'.$flickrImageSize->select($tab, array('MEDIUM', 'LARGE', 'ORIGINAL')).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrShowTitle->label().'</td><td>'.$flickrShowTitle->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrShowDescription->label().'</td><td>'.$flickrShowDescription->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrShowPageLink->label().'</td><td>'.$flickrShowPageLink->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrPageLinkText->label().'</td><td>'.$flickrPageLinkText->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrTitleFontColor->label().'</td><td>'.$flickrTitleFontColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrTitleFontName->label().'</td><td>'.$flickrTitleFontName->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrTitleFontSize->label().'</td><td>'.$flickrTitleFontSize->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$flickrExtraParams->label().'</td><td>'.$flickrExtraParams->text($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Back button</h3>
  <div class="accordionElements">
    <table cellspacing="0">
      <tr>
      <td class="label">'.$showBackButton->label().'</td><td>'.$showBackButton->checkbox($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonText->label().'</td><td>'.$backButtonText->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonURL->label().'</td><td>'.$backButtonURL->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonFontColor->label().'</td><td>'.$backButtonFontColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonFontSize->label().'</td><td>'.$backButtonFontSize->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonFontName->label().'</td><td>'.$backButtonFontName->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonBackColor->label().'</td><td>'.$backButtonBackColor->color($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonBackOpacity->label().'</td><td>'.$backButtonBackOpacity->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonX->label().'</td><td>'.$backButtonX->text($tab).'</td>
    </tr>
    <tr>
      <td class="label">'.$backButtonY->label().'</td><td>'.$backButtonY->text($tab).'</td>
    </tr>
    <tr>
        <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
      </tr>
    </table>
  </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Fixed layout</h3>
    <div class="accordionElements">
      <table cellspacing="0">
        <tr>
        <td class="label">'.$useFixedLayout->label().'</td><td>'.$useFixedLayout->checkbox($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$imageAreaX->label().'</td><td>'.$imageAreaX->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$imageAreaY->label().'</td><td>'.$imageAreaY->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$imageAreaWidth->label().'</td><td>'.$imageAreaWidth->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$imageAreaHeight->label().'</td><td>'.$imageAreaHeight->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$thumbAreaX->label().'</td><td>'.$thumbAreaX->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$thumbAreaY->label().'</td><td>'.$thumbAreaY->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$captionX->label().'</td><td>'.$captionX->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$captionY->label().'</td><td>'.$captionY->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$titleX->label().'</td><td>'.$titleX->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$titleY->label().'</td><td>'.$titleY->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$buttonBarX->label().'</td><td>'.$buttonBarX->text($tab).'</td>
      </tr>
      <tr>
        <td class="label">'.$buttonBarY->label().'</td><td>'.$buttonBarY->text($tab).'</td>
      </tr>
      <tr>
          <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="elementwrap">
  <h3 class="togglers">Mobile</h3>
    <div class="accordionElements">
      <table cellspacing="0">
        <tr>
          <td class="label">'.$mobileShowNav->label().'</td><td>'.$mobileShowNav->checkbox($tab).'</td>
        </tr>
        <tr>
          <td class="label">'.$mobileShowThumbs->label().'</td><td>'.$mobileShowThumbs->checkbox($tab).'</td>
        </tr>
        <tr>
          <td class="label">'.$mobileShowCaption->label().'</td><td>'.$mobileShowCaption->checkbox($tab).'</td>
        </tr>
        <tr>
          <td><input type="hidden" name="customizesubmitted" value="true" /></td><td><input type="submit" name="submit" class="formbutton" value="Update all" /></td>
        </tr>
      </table>
    </div>
  </div>
</form>';
    return $html;
  }

}
