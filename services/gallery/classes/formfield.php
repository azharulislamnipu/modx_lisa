<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Html form input
 *
 * @package svManager
 */
class FormField
{
 /**
  * Constructs FormField
  * @access public
  * @return void
  * @param string label for form element
  * @param string form element name
  * @param string form element value
  * @param string form element tab index
  * @param array of options for select element
  */
  function FormField($label, $name, $value)
  {
    $this->id = strtolower($name);
    $this->label = $label;
    $this->name = $name;
    $this->value = $value;
    $this->tab = &$tab;
  }
 /**
  * Returns html label string
  * @access public
  * @return string
  */
  function label()
  {
    return '<label for="'.$this->id.'">'.$this->label.'</label>';
  }
  
 /**
  * Returns html text input
  * @access public
  * @param integer tab index
  * @return string
  */
  function text(&$tab)
  {
    $tab ++;
    return '<input type="text" class="text" name="textFields['.$this->name.']" id="'.$this->id.'" tabindex="'.$tab.'" value="'.htmlspecialchars($this->value, ENT_QUOTES, 'UTF-8').'" />';
  }
  
 /**
  * Returns html color input
  * @access public
  * @param integer tab index
  * @return string
  */
  function color(&$tab)
  {
    $tab++;
    $this->value = strtoupper(str_replace(array('#', '0x'), '', $this->value));
    return '<input type="text" class="colorpicker" name="colorFields['.$this->name.']" id="'.$this->id.'" tabindex="'.$tab.'" value="'.$this->value.'" />';
  }
  
 /**
  * Returns html checkbox input
  * @access public
  * @param integer tab index
  * @return string
  */
  function checkbox(&$tab)
  {
    $tab++;
    $checkString = (strtoupper($this->value) == 'TRUE') ? 'checked = "checked"' : '';
    return  '<input type="hidden" name="clearCheckboxes['.$this->name.']" value="FALSE" /><input type="checkbox" class="checkbox" name="checkboxes['.$this->name.']" value="TRUE" id="'.$this->id.'" tabindex="'.$tab.'" '.$checkString.' />';
  }
  
 /**
  * Returns html radio input, sets id and label
  * Must be called before getting label
  * @access public
  * @param integer tab index
  * @return string
  */
  function radio(&$tab, $checkedValue)
  {
    $tab++;
    $this->id = $this->id.strtolower($checkedValue);
    $checkString = (strtoupper($this->value) == $checkedValue) ? 'checked = "checked"' : '';
    return  '<input type="radio" class="radio" name="radio['.$this->name.']" value="'.$checkedValue.'" id="'.$this->id.'" tabindex="'.$tab.'" '.$checkString.' />';
  }

  
 /**
  * Returns html checkbox input
  * @access public
  * @param integer tab index
  * @return string
  */
  function select(&$tab, $options)
  {
    $tab++;
    $html = '<select name="selectFields['.$this->name.']" id="'.$this->id.'" tabindex="'.$tab.'">';
    foreach ($options as $option)
    {
      if ($option == $this->value)
      {
        $selectString = 'selected="selected"';
      }
      else
      {
        $selectString = '';
      }
      $html .= '<option value="'.$option.'" '.$selectString.'>'.$option.'</option>';
    }
    $html .= '</select>';
    return $html;
  }
  
}