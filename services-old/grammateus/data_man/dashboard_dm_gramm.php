<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsOfficers = "-1";
if (isset($_SESSION['BOULENAME'])) {
  $colname_rsOfficers = $_SESSION['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOfficers = sprintf("SELECT CPOSITION, BOULENAME, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, SUFFIX, DESIGNATIONLST, ADDRESS1, CITY, STATECD, ZIP, HOMEPHONE, EMAIL, WORKPHONE, ALTEMAIL,L2R_FULLNAME,CPOSITIONID FROM vw_boule_officers WHERE BOULENAME = %s AND COMMITTEESTATUSSTT = 'Active' ORDER BY CPOSITIONID ASC", GetSQLValueString($colname_rsOfficers, "text"));
$rsOfficers = mysql_query($query_rsOfficers, $sigma_modx) or die(mysql_error());
$row_rsOfficers = mysql_fetch_assoc($rsOfficers);
$totalRows_rsOfficers = mysql_num_rows($rsOfficers);

$colname_rsBoules = "-1";
if (isset($_SESSION['BOULENAME'])) {
  $colname_rsBoules = $_SESSION['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoules = sprintf("SELECT * FROM spp_boule WHERE BOULENAME = %s", GetSQLValueString($colname_rsBoules, "text"));
$rsBoules = mysql_query($query_rsBoules, $sigma_modx) or die(mysql_error());
$row_rsBoules = mysql_fetch_assoc($rsBoules);
$totalRows_rsBoules = mysql_num_rows($rsBoules);

$colname_rsBoulesByRegion = "-1";
if (isset($_SESSION['BOULENAME'])) {
  $colname_rsBoulesByRegion = $_SESSION['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoulesByRegion = sprintf("SELECT * FROM vw_gramm_archons WHERE BOULENAME = %s AND vw_gramm_archons.STATUS <> 'Deceased' ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsBoulesByRegion, "text"));
$rsBoulesByRegion = mysql_query($query_rsBoulesByRegion, $sigma_modx) or die(mysql_error());
$row_rsBoulesByRegion = mysql_fetch_assoc($rsBoulesByRegion);
$totalRows_rsBoulesByRegion = mysql_num_rows($rsBoulesByRegion);

$colname_rsExportBoule = "-1";
if (isset($_SESSION['BOULENAME'])) {
  $colname_rsExportBoule = $_SESSION['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsExportBoule = sprintf("SELECT  vw_web_elig.PREFIX,   vw_web_elig.FIRSTNAME,   vw_web_elig.MIDDLEINITIAL,   vw_web_elig.LASTNAME,   vw_web_elig.SUFFIX,   vw_web_elig.DESIGNATIONLST as HONORIFIC,   vw_web_elig.ADDRESS1,   vw_web_elig.CITY,   vw_web_elig.STATECD AS STATE,   vw_web_elig.ZIP,   vw_web_elig.WEB_EMAIL AS HOME_EMAIL,   vw_web_elig.HOMEPHONE AS HOME_PHONE,   vw_web_elig.WORKPHONE AS WORK_PHONE,   vw_web_elig.ALTEMAIL AS WORK_EMAIL FROM vw_web_elig WHERE BOULENAME = %s ORDER BY LASTNAME, FIRSTNAME ASC", GetSQLValueString($colname_rsExportBoule, "text"));
$rsExportBoule = mysql_query($query_rsExportBoule, $sigma_modx) or die(mysql_error());
$row_rsExportBoule = mysql_fetch_assoc($rsExportBoule);
$totalRows_rsExportBoule = mysql_num_rows($rsExportBoule);

//Export to Excel Server Behavior
if (isset($_POST['boule_export'])&&($_POST['boule_export']=="boule")){
	$delim="";
	$delim_replace="";
	if($delim==""){
		$lang=(strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],",")===false)?$_SERVER['HTTP_ACCEPT_LANGUAGE']:substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],","));
		$semi_array=array("af","zh-hk","zh-mo","zh-cn","zh-sg","zh-tw","fr-ch","de-li","de-ch","it-ch","ja","ko","es-do","es-sv","es-gt","es-hn","es-mx","es-ni","es-pa","es-pe","es-pr","sw");
		$delim=(in_array($lang,$semi_array) || substr_count($lang,"en")>0)?",":";";
	}
	$output="";
	$include_hdr="1";
	if($include_hdr=="1"){
		$totalColumns_rsExportBoule=mysql_num_fields($rsExportBoule);
		for ($x=0; $x<$totalColumns_rsExportBoule; $x++) {
			if($x==$totalColumns_rsExportBoule-1){$comma="";}else{$comma=$delim;}
			$output = $output.(ereg_replace("_", " ",mysql_field_name($rsExportBoule, $x))).$comma;
		}
		$output = $output."\r\n";
	}

	do{$fixcomma=array();
    		foreach($row_rsExportBoule as $r){array_push($fixcomma,ereg_replace($delim,$delim_replace,$r));}
		$line = join($delim,$fixcomma);
    		$line=ereg_replace("\r\n", " ",$line);
    		$line = "$line\n";
    		$output=$output.$line;}while($row_rsExportBoule = mysql_fetch_assoc($rsExportBoule));
	header("Content-Type: application/xls");
	header("Content-Disposition: attachment; filename=Boule.csv");
	header("Content-Type: application/force-download");
	header("Cache-Control: post-check=0, pre-check=0", false);
	echo $output;
	die();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Regional Grammateus Management</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="/home/assets/css/balloontip.css" media="all" />
<!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" -->
  <!-- #MAIN COLUMN -->
<div class="floatLeft width460">

  <h1 class="yellow">Manage <?php echo $row_rsBoules['BOULENAME']; ?> Boul&#233; Archons</h1>
  <div class="contentBlock">
    <table width="100%" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
<tr>
    <td colspan="2" bgcolor="F4F4F4"><p>Placing the cursor over an Archon's name will cause his contact information to appear. Click the link in order to edit the informatuion.</p>
      <p>You may request, but not change an archon's status on the website. Click the &quot;Request Status Change&quot; link in order to submit an email to the Office of the Grand Boul&#233; detailing your request.</p></td>
    </tr>
    <tr>
    <td bgcolor="F4F4F4"><p><strong>ARCHON</strong></p></td>
     <td bgcolor="F4F4F4"><p><strong>STATUS</strong></p></td>
   </tr>
    
  <?php do { ?>
    <tr valign="top">
      <td bgcolor="F4F4F4">
  <script language="JavaScript">
    function edit_<?php echo $row_rsBoulesByRegion['CUSTOMERID'];?>(){
        document.forms.form_<?php echo $row_rsBoulesByRegion['CUSTOMERID']; ?>.submit();
    }
  </script>
  <form name="form_<?php echo $row_rsBoulesByRegion['CUSTOMERID']; ?>" action="/services/grammateus/data_man/archons_edit.php" method="get">
    <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsBoulesByRegion['CUSTOMERID']); ?>">
    <input type="hidden" name="GRAMMID" value="<?php echo KT_escapeAttribute($_SESSION['WEB_ID']); ?>">
    <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
    <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
    <input type="hidden" name="REGIONNAME" value="<?php echo KT_escapeAttribute($_SESSION['REGIONNAME']); ?>">
  </form>
  <a href="#" rel="<?php echo $row_rsBoulesByRegion['WEB_ID']; ?>" onclick="edit_<?php echo $row_rsBoulesByRegion['CUSTOMERID'];?>();return(FALSE);"><?php echo $row_rsBoulesByRegion['FULLNAME']; ?></a>
  <div id="<?php echo $row_rsBoulesByRegion['WEB_ID']; ?>" class="balloonstyle">
            <?php echo $row_rsBoulesByRegion['ADDRESS1']; ?><br />
            <?php echo $row_rsBoulesByRegion['CITY']; ?>, <?php echo $row_rsBoulesByRegion['STATECD']; ?> <?php echo $row_rsBoulesByRegion['ZIP']; ?><br />
            HOME PHONE: <?php echo $row_rsBoulesByRegion['HOMEPHONE']; ?><br />
            EMAIL: <?php echo $row_rsBoulesByRegion['EMAIL']; ?><br />
            BUSINESS PHONE: <?php echo $row_rsBoulesByRegion['WORKPHONE']; ?><br />
            BUSINESS EMAIL: <?php echo $row_rsBoulesByRegion['ALTEMAIL']; ?>    </div></td>
      <td bgcolor="F4F4F4"><strong><?php echo $row_rsBoulesByRegion['STATUS']; ?></strong><br />
        <span style="font-size: 10px;"><a href="/services/grammateus/data_man/status_request.php?CUSTOMERID=<?php echo $row_rsBoulesByRegion['CUSTOMERID'];?>&amp;UPDATEDBY=<?php echo $_SESSION['webInternalKey'];?>&amp;DM_EMAIL=<?php echo $_SESSION['webEmail']; ?>&amp;KT_back=1">Request Status Change</a></span></td>
    </tr>
    <?php } while ($row_rsBoulesByRegion = mysql_fetch_assoc($rsBoulesByRegion)); ?>
</table>
  </div>

  

</div>
<!-- #END MAIN COLUMN -->
<!-- #BEGIN SECOND COLUMN -->
<div class="floatRight width305">
  <h1 class="yellow">Manage <?php echo $row_rsBoules['BOULENAME']; ?> Boul&#233; Officers</h1>
  <div class="contentBlock">
  <table>
   <?php do { ?><tr>
   <td><strong><?php echo $row_rsOfficers['L2R_FULLNAME']; ?>, </strong><?php echo $row_rsOfficers['CPOSITION']; ?></td>
    </tr><?php } while ($row_rsOfficers = mysql_fetch_assoc($rsOfficers)); ?>
    <tr>
    <td><p><a href="/services/grammateus/data_man/officers_mgmt.php?BOULENAME=<?php echo $_SESSION['BOULENAME'];?>&amp;CHAPTERCD=<?php echo $_SESSION['BOULECD']; ?>&amp;REGIONCD=<?php echo $_SESSION['REGIONCD'];?>&amp;UPDATEDBY=<?php echo $_SESSION['webShortname']; ?>&amp;GRAMMID=<?php echo $_SESSION['webInternalKey']; ?>">Update  Officers' Listing</a></p>
      <p><a href="/services/grammateus/data_man/mail_officers.php?BOULENAME=<?php echo $_SESSION['BOULENAME'];?>&amp;UPDATEDBY=<?php echo $_SESSION['webFullname']; ?>&amp;DM_EMAIL=<?php echo $_SESSION['webEmail']; ?>">Submit Officers' Listing to the Office of the Grand Boul&eacute;.</a></p></td>
</table>
   </div>
   
  <h1 class="red">Export Data for <?php echo $row_rsBoules['BOULENAME']; ?> Boul&#233; </h1>
  <div class="contentBlock">
  <form id="boule" name="boule" method="post" action="">
    
    <input name="boule_export" type="hidden" id="boule" value="boule" />
    <input name="export" type="submit" value="Click to Export <?php echo $row_rsBoulesByRegion['BOULENAME']; ?> Data" />
    </form>  
</div>

<div class="floatRight width305">
  <h1 class="yellow">Manage  Status Changes and Transfers for <?php echo $row_rsBoules['BOULENAME']; ?> Boul&#233; </h1>
  <div class="contentBlock">
<table width="100%" cellspacing="4" cellpadding="4">
  <tr valign="top">
    <td><p><strong>Transfer Request</strong><br />
      Click the link on the right if you wish to receive a transfer form for an archon. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format.</p></td>
    <td valign="bottom"><a href="/home/iakt/grammateus/forms/TRANSFER_FORM.zip">Download Transfer Form</a></td>
  </tr>
  <tr valign="top">
    <td><p><strong>Voluntary Inactive Request</strong><br />
      Click the link on the right if you wish to receive a Voluntary Inactive form. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format.</p></td>
    <td valign="bottom"><a href="/home/iakt/grammateus/forms/VOLUNTARY_INACTIVE_FORM.zip">Download Voluntary Inactive Form</a></td>
  </tr>
  <tr>
    <td><p><strong>Involuntary Inactive Request</strong><br />
      Click the link on the right if you wish to receive an Involuntary Inactive form. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format.</p></td>
    <td valign="bottom"><a href="/home/iakt/grammateus/forms/INVOLUNTARY_INACTIVE_FORM.zip">Download Involuntary Inactive Form</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

  </div>

</div><!-- #END SECOND COLUMN -->
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsOfficers);

mysql_free_result($rsBoules);

mysql_free_result($rsBoulesByRegion);

mysql_free_result($rsExportBoule);
?>
