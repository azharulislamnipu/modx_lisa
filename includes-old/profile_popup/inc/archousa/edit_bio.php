<div id="archon_edit_bio" class="accordion parent-elem edit_bio" data-accordion>
    <div data-control><h3 class="text-center">Edit Biography</h3></div>
    <div class="ymp_form_input" data-content>
        <div class="data-content-inner edit_bio_inner">
            <label class="span-12">Biography <span class="required_ind">*</span></label>
            <textarea class="input-required input-validated tiny-editable" name="BIO" id="BIO" cols="90" rows="10"><?php echo htmlspecialchars_decode($user_data['BIO'], ENT_NOQUOTES);?></textarea>
        </div>

        <?php echo $submit_dv;?>
    </div>

</div>