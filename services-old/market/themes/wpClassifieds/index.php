<?php if ($advs){
 echo '<div>';
    advancedSearchForm();
 echo '</div>';}?>
   
<?php 
if (isset($idItem)||isset($currentCategory)||isset($type)||cG("s")!=""){?>
<div id="listings">
   
   <?php if(isset($categoryName)){ ?><h2><?php echo $categoryName;?></h2><?php }?>
        
<?php 
	if ($resultSearch){

	foreach ( $resultSearch as $row ){
		$idPost=$row['idPost'];
		$postType=$row['type'];
		$postTypeName=getTypeName($postType);
		$postTitle=$row['title'];
		$postPrice=$row['price'];
		$postDesc= substr(strip_tags(html_entity_decode($row['description'], ENT_QUOTES, CHARSET)), 0, 200)."...";
		$category=$row['category'];//real category name
		$fcategory=$row['fcategory'];//frienfly name category
		$idCategoryParent=$row['idCategoryParent'];
		$fCategoryParent=$row['parent'];
		$postImage=$row['image'];
		$postPassword=$row['password'];
		$insertDate=setDate($row['insertDate']);
		if ($postImage=="") $postImage=SITE_URL."/images/no_pic.png";
		else $postImage=SITE_URL.IMG_UPLOAD.$idPost."/thumb_".$postImage;
		$postUrl=itemURL($idPost,$fcategory,$postTypeName,$postTitle,$fCategoryParent);
		
		?>
		<div class="post">
		
		    <?php if (MAX_IMG_NUM>0){?>	
					<img  title="<?php echo $postTitle." ".$postTypeName." ".$category;?>"  alt="<?php echo $postTitle." ".$postTypeName." ".$category;?>"  src="<?php echo $postImage;?>" class="post-img" />
			<?php }?>
			
		    <h2><a title="<?php echo $postTitle." ".$postTypeName." ".$category;?>" href="<?php echo SITE_URL.$postUrl;?>"  rel="bookmark" >
					<?php echo $postTitle;?></a></h2>
		    
		     <div class="post-detail">
                <p><?php if ($postPrice!=0) echo '<span class="post-price">'.getPrice($postPrice).'</span> — ';?><span class="post-cat"><?php echo '<a href="'.SITE_URL.catURL($fcategory,$fCategoryParent).'" title="'.$category.' '.$fCategoryParent.'">'.$category.'</a>';?></span> — <span class="post-date"><?php echo $insertDate;?></span></p>
             </div>
             
          <p class="post-desc"><?php echo $postDesc;?></p>
          
          <?php if(isset($_SESSION['admin'])){?>
				<br />
				<a href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idPost;?>&amp;pwd=<?php echo $postPassword;?>&amp;action=edit">
						<?php echo T_EDIT;?></a><?php echo SEPARATOR;?>
				<a onClick="return confirm('<?php echo T_DEACTIVATE;?>?');" 
					href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idPost;?>&amp;pwd=<?php echo $postPassword;?>&amp;action=deactivate">
						<?php echo T_DEACTIVATE;?></a><?php echo SEPARATOR;?>
				<a onClick="return confirm('<?php echo T_SPAM;?>?');"
					href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idPost;?>&amp;pwd=<?php echo $postPassword;?>&amp;action=spam">
						<?php echo T_SPAM;?></a><?php echo SEPARATOR;?>
				<a onClick="return confirm('<?php echo T_DELETE;?>?');"
					href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idPost;?>&amp;pwd=<?php echo $postPassword;?>&amp;action=delete">
						<?php echo T_DELETE;?></a>
			<?php }?>
          <div class="clear"></div>
        </div>    
		    		
		<?php 
	}
}//end if check there's results
else echo "<h2>".T_NOTHING_FOUND."</h2>";
?>
</div>
	

	<div class="pagination">
	 <div class="wp-pagenavi">
	<?php //page numbers echo $_SERVER["REQUEST_URI"];
		if ($total_pages>1){
			
			//if is a search
			if (strlen(cG("s"))>=MIN_SEARCH_CHAR) $search="&s=".cG("s");
			
			$pag_title=$html_title." ".T_PAGE." ";

			//getting the url
			if(strlen(cG("s"))>=MIN_SEARCH_CHAR){//home with search
				$pag_url='?s='.cG("s").'&category='.$currentCategory.'&page=';
			}
			elseif ($advs){//advanced search
				$pag_url="?category=$currentCategory&title=".cG("title")."&desc=".cG("desc")."&price=".cG("price")."&place=".cG("place")."&sort=".cG("sort")."&page=";
			}
			elseif (isset($type)){ //only set type in the home
				$pag_url=typeURL($type,$currentCategory).'&page=';
			}
			elseif (isset($currentCategory)){//category
				$pag_url=catURL($currentCategory,$selectedCategory);//only category
				if(!FRIENDLY_URL) $pag_url.='&page=';
			}
			else {
			    $pag_url="/";//home
			    if(!FRIENDLY_URL) $pag_url.='?page=';
			}
			//////////////////////////////////
		
			if ($page>1){
				echo "<a title='$pag_title' href='".SITE_URL.$pag_url."1'>&lt;&lt;</a>";//First
				echo "<a title='".T_PREVIOUS." $pag_title".($page-1)."' href='".SITE_URL.$pag_url.($page-1)."'>&lt;</a>";//previous
			}
			//pages loop
			for ($i = $page; $i <= $total_pages && $i<=($page+DISPLAY_PAGES); $i++) {//for ($i = 1; $i <= $total_pages; $i++) {
		        if ($i == $page) echo "<span class='current'>$i</span>";//not printing link current page
		        else echo "<a class='page' title='$pag_title$i' href='".SITE_URL."$pag_url$i'>$i</a>";//print the link
		    }
		    
		    if ($page<$total_pages){
		    	echo "<a href='".SITE_URL.$pag_url.($page+1)."' title='".T_NEXT." $pag_title".($page+1)."' >&gt;</a>";//next
		    	echo  "<a title='$pag_title$total_pages' href='".SITE_URL."$pag_url$total_pages'>&gt;&gt;</a>";//End
		    }
		}	
	?>
	</div>
	</div>
   
<?php    
}//if not home
else {//home page carousel and categories

     ?>
     <h4 class="carousel"><?php echo T_RSS_ADS; ?></h4>
      <div id="carousel">
        <div class="prev"><img src="<?php echo SITE_URL;?>/themes/wpClassifieds/prev.jpg" alt="prev" width="19" height="19" /></div>
        <div class="slider">
          <ul>
            <?php foreach ( $resultSearch as $row ){
                    $idPost=$row['idPost'];
		            $postType=$row['type'];
		            $postTypeName=getTypeName($postType);
		            $postTitle=$row['title'];
		            $category=$row['category'];//real category name
		            $fcategory=$row['fcategory'];//frienfly name category
		            $idCategoryParent=$row['idCategoryParent'];
		            $fCategoryParent=$row['parent'];
		            $postImage=$row['image'];
		            if ($postImage=="") $postImage=SITE_URL."/images/no_pic.png";
		            else $postImage=SITE_URL.IMG_UPLOAD.$idPost."/thumb_".$postImage;
		            $postUrl=itemURL($idPost,$fcategory,$postTypeName,$postTitle,$fCategoryParent);?>
                <li>
			
		      	  <?php if (MAX_IMG_NUM>0){?>	
		      	        <a title="<?php echo $postTitle." ".$postTypeName." ".$category;?>" href="<?php echo SITE_URL.$postUrl;?>" >
					    <img  title="<?php echo $postTitle." ".$postTypeName." ".$category;?>"  alt="<?php echo $postTitle." ".$postTypeName." ".$category;?>"  src="<?php echo $postImage;?>"  />
					    </a>
			       <?php }?>
                        <a title="<?php echo $postTitle." ".$postTypeName." ".$category;?>" href="<?php echo SITE_URL.$postUrl;?>" >
					    <?php echo $postTitle;?></a>
                </li>
            <?php }?>
          </ul>
        </div>
        <div class="next"><img src="<?php echo SITE_URL;?>/themes/wpClassifieds/next.jpg" alt="next" width="19" height="19" /></div>
        <div class="clear"></div>
      </div>
    
 
      <div id="frontpage_cats">
        <?php echo getCategoriesList();?>
        <div class="clear"></div>
      </div>

     <?php
     }
?>
  


        
        
	    
	  


	
