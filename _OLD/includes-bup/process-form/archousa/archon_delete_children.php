<?php
$response = array();
$response['type'] = 'error';

$db = get_db();
$db->where ('CUSTOMERID', $user_id);
$db->where ('CHILDID', $prof_id);
if ($db->delete ('spp_children')){
    $response['type'] = 'success';
    SPP_ARCHON_DB::update_lastupdate_date($user_id);
} else{
    $response['type'] = 'error';
//  $response['html'] = $db->getLastError();
    $response['html'] = '<p>Error occurred! Please try again.</p>';
}

die(json_encode($response));