<?php 
	# PHP ADODB document - made with PHAkt
	# FileName="Connection_php_adodb.htm"
	# Type="ADODB"
	# HTTP="true"
	# DBTYPE="mysql"
	
	$MM_sigma_modx_HOSTNAME = 'localhost';
	$MM_sigma_modx_DATABASE = 'mysql:modx_dev';
	$MM_sigma_modx_DBTYPE   = preg_replace('/:.*$/', '', $MM_sigma_modx_DATABASE);
	$MM_sigma_modx_DATABASE = preg_replace('/^[^:]*?:/', '', $MM_sigma_modx_DATABASE);
	$MM_sigma_modx_USERNAME = 'sigma-admin';
	$MM_sigma_modx_PASSWORD = 'beaure';
	$MM_sigma_modx_LOCALE = 'En';
	$MM_sigma_modx_MSGLOCALE = 'En';
	$MM_sigma_modx_CTYPE = 'P';
	$KT_locale = $MM_sigma_modx_MSGLOCALE;
	$KT_dlocale = $MM_sigma_modx_LOCALE;
	$KT_serverFormat = '%Y-%m-%d %H:%M:%S';
	$QUB_Caching = 'false';

	$KT_localFormat = $KT_serverFormat;
	
	if (!defined('CONN_DIR')) define('CONN_DIR',dirname(__FILE__));
	require_once(CONN_DIR.'/../adodb/adodb.inc.php');
	$sigma_modx=&KTNewConnection($MM_sigma_modx_DBTYPE);

	if($MM_sigma_modx_DBTYPE == 'access' || $MM_sigma_modx_DBTYPE == 'odbc'){
		if($MM_sigma_modx_CTYPE == 'P'){
			$sigma_modx->PConnect($MM_sigma_modx_DATABASE, $MM_sigma_modx_USERNAME,$MM_sigma_modx_PASSWORD);
		} else $sigma_modx->Connect($MM_sigma_modx_DATABASE, $MM_sigma_modx_USERNAME,$MM_sigma_modx_PASSWORD);
	} else if (($MM_sigma_modx_DBTYPE == 'ibase') or ($MM_sigma_modx_DBTYPE == 'firebird')) {
		if($MM_sigma_modx_CTYPE == 'P'){
			$sigma_modx->PConnect($MM_sigma_modx_HOSTNAME.':'.$MM_sigma_modx_DATABASE,$MM_sigma_modx_USERNAME,$MM_sigma_modx_PASSWORD);
		} else $sigma_modx->Connect($MM_sigma_modx_HOSTNAME.':'.$MM_sigma_modx_DATABASE,$MM_sigma_modx_USERNAME,$MM_sigma_modx_PASSWORD);
	}else {
		if($MM_sigma_modx_CTYPE == 'P'){
			$sigma_modx->PConnect($MM_sigma_modx_HOSTNAME,$MM_sigma_modx_USERNAME,$MM_sigma_modx_PASSWORD, $MM_sigma_modx_DATABASE);
		} else $sigma_modx->Connect($MM_sigma_modx_HOSTNAME,$MM_sigma_modx_USERNAME,$MM_sigma_modx_PASSWORD, $MM_sigma_modx_DATABASE);
   }

	if (!function_exists('updateMagicQuotes')) {
		function updateMagicQuotes($HTTP_VARS){
			if (is_array($HTTP_VARS)) {
				foreach ($HTTP_VARS as $name=>$value) {
					if (!is_array($value)) {
						$HTTP_VARS[$name] = addslashes($value);
					} else {
						foreach ($value as $name1=>$value1) {
							if (!is_array($value1)) {
								$HTTP_VARS[$name1][$value1] = addslashes($value1);
							}
						}
					}
				}
			}
			return $HTTP_VARS;
		}
		
		if (!get_magic_quotes_gpc()) {
			$_GET = updateMagicQuotes($_GET);
			$_POST = updateMagicQuotes($_POST);
			$_COOKIE = updateMagicQuotes($_COOKIE);
		}
	}
	if (!isset($_SERVER['REQUEST_URI']) && isset($_ENV['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = $_ENV['REQUEST_URI'];
	}
	if (!isset($_SERVER['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = $_SERVER['PHP_SELF'].(isset($_SERVER['QUERY_STRING'])?"?".$_SERVER['QUERY_STRING']:"");
	}
?>