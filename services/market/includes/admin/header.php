<?php
////////////////////////////////////////////////////////////
//Common header for admin
////////////////////////////////////////////////////////////
require_once('../includes/functions.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANGUAGE;?>" lang="<?php echo LANGUAGE;?>">

<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET;?>" />
		
		<title><?php echo T_ADMIN?> <?php echo $html_title;?></title>
		<meta name="generator" content="Open Classifieds <?php echo VERSION;?>" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>/admin/style.css" media="screen" />
		<link rel="shortcut icon" href="<?php echo SITE_URL;?>/favicon.ico" />
		<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/js/common.js"></script>		
</head>
<body>

<div id="header">
<h1><?php echo T_ADMIN?> <?php echo SITE_NAME; ?></h1>
<?php
if(strpos($_SERVER["REQUEST_URI"], "login.php")<=0){//don't display for login?>
<div id="menu">
  <ul id="nav">
  	<li class="nav"><a href="<?php echo SITE_URL;?>/admin/">Admin</a></li>
  	<li class="nav"><a href="settings.php">Settings</a></li>
  	<li class="nav"><a href="optimize.php">Optimize</a></li>
	<li class="nav"><a href="categories.php"><?php echo T_CATEGORIES; ?></a></li>
	<li class="nav"><a href="admin_sitemap.php">Sitemap</a></li>
	<li class="nav"><a href="<?php echo SITE_URL;?>"><?php echo T_HOME; ?></a></li>
	<li class="nav"><a href="logout.php"><?php echo T_LOGOUT;?></a></li>
	</ul>
</div>
<?php } ?>
</div>

<div id="content">
<div id="left">
