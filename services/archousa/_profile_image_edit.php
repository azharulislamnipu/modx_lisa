<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_ImageUpload trigger
//remove this line if you want to edit the code by hand 
function Trigger_ImageUpload(&$tNG) {
  $uploadObj = new tNG_ImageUpload($tNG);
  $uploadObj->setFormFieldName("IMAGE");
  $uploadObj->setDbFieldName("IMAGE");
  $uploadObj->setFolder("../archousa/images/");
  $uploadObj->setResize("true", 200, 0);
  $uploadObj->setMaxSize(1500);
  $uploadObj->setAllowedExtensions("gif, jpg, jpe, jpeg, png");
  $uploadObj->setRename("custom");
  $uploadObj->setRenameRule("{ARCHOUSAID}.{KT_name}.{KT_ext}");
  return $uploadObj->Execute();
}
//end Trigger_ImageUpload trigger

// Make an update transaction instance
$upd_spp_archousa = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archousa);
// Register triggers
$upd_spp_archousa->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archousa->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archousa->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-profile.php");
$upd_spp_archousa->registerTrigger("AFTER", "Trigger_ImageUpload", 97);
// Add columns
$upd_spp_archousa->setTable("spp_archousa");
$upd_spp_archousa->addColumn("IMAGE", "FILE_TYPE", "FILES", "IMAGE");
$upd_spp_archousa->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_archousa->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "LASTUPDATED");
$upd_spp_archousa->addColumn("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");
$upd_spp_archousa->setPrimaryKey("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archousa = $tNGs->getRecordset("spp_archousa");
$row_rsspp_archousa = mysql_fetch_assoc($rsspp_archousa);
$totalRows_rsspp_archousa = mysql_num_rows($rsspp_archousa);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">Page Name</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" enctype="multipart/form-data">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="IMAGE">IMAGE:</label></td>
        <td><input type="file" name="IMAGE" id="IMAGE" size="32" />
            <?php echo $tNGs->displayFieldError("spp_archousa", "IMAGE"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Upload Image" /> 
        <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/archousa-profile.php')" />
        </td>
      </tr>
    </table>
    <input type="hidden" name="UPDATEDBY" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['UPDATEDBY']); ?>" />
    <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo KT_formatDate($row_rsspp_archousa['LASTUPDATED']); ?>" />
    <input type="hidden" name="ARCHOUSAID" id="ARCHOUSAID" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ARCHOUSAID']); ?>" />
  </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
