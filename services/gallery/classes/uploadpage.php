<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Upload page
 *
 * @package svManager
 */
class UploadPage extends Page
{  
 /**
  * constructs UploadPage class
  * @param string gallery type
  */
  function UploadPage($galleryType)
  {
    parent::Page('svManager &ndash; upload images', 'upload', $galleryType);
  }

 /**
  * get html for upload page
  *
  * @access public
  * @return string html
  * @param array subset of gallery attributes
  * @param integer max Mbytes for user prompt
  * @param boolean resize images is on
  */
  function getUploadHtml(&$gallery, $galleryType, $resize)
  {
    $preferences = $gallery->getPreferences();
    $maxMByte = $gallery->getMaxUploadMBytes();
    if ($galleryType == 'simpleviewer')
    {
      $maxImageWidth = $preferences['maxImageWidth'] - (2 * $preferences['frameWidth']);
      $maxImageHeight = $preferences['maxImageHeight'] - (2 * $preferences['frameWidth']); 
    }
    if ($galleryType == 'simpleviewer2')
    {
      $maxImageWidth = $preferences['maxImageWidth'] - (2 * $preferences['frameWidth']);
      $maxImageHeight = $preferences['maxImageHeight'] - (2 * $preferences['frameWidth']); 
    }
    if ($galleryType == 'tiltviewer')
    {
      $frameWidth = max(0, $preferences['frameWidth']);
      $maxImageWidth = $preferences['maxJPGSize'];
      $maxImageHeight = $preferences['maxJPGSize'];
    }
    if (($galleryType == 'autoviewer') || ($galleryType == 'postcardviewer'))
    {
      $preferences = $gallery->getPreferences();
      $maxImageWidth = $preferences['maxImageWidth'];
      $maxImageHeight = $preferences['maxImageHeight'];
    }
    if ($resize)
    {
      $maxImageWidthParam = '<param name="maxImageWidth" value="'.$maxImageWidth.'"/>';
      $maxImageHeightParam = '<param name="maxImageHeight" value="'.$maxImageHeight.'"/>';
      $resizeHtml = 'Image resize is <span class="on">on</span>: <a href="'.$_SERVER['PHP_SELF'].'?resize=0">turn off</a> for this session';                         
    }
    else
    {
      $maxImageWidthParam = '';
      $maxImageHeightParam = '';
      $resizeHtml = 'Image resize is <span class="off">off</span>: <a href="'.$_SERVER['PHP_SELF'].'?resize=1">turn on</a> for this session';
    }
	  $absImagePath = rawurlencode(realpath(getcwd().DIRECTORY_SEPARATOR.$gallery->getImageDirPathRelSvm()).DIRECTORY_SEPARATOR);
    // Source: http://joliclic.free.fr/html/object-tag/en/object-java.html
    $compressionQuality = IMAGE_QUALITY;
    $showHTTPResponse = JAVA_CONSOLE_LOG;
$html = <<<EOD
<div id="applet">
  <applet name="uploadApplet" 
    code="javaatwork.myuploader.UploadApplet.class" 
    codebase="uploader/" 
    archive="myuploader-source-signed-1.14.jar, labels.jar" 
    width="400" height="250">
    <param name="codebase_lookup" value="false"/>
    <param name="uploadURL" value="../myupload.php?path={$absImagePath}"/>
    <param name="successURL" value="../svmupload.php?uploaded=true"/>
    <param name="showThumbNailsInApplet" value="true"/>
    <param name="showThumbNailsInFileDialog" value="true"/>
    <param name="fileFilter" value="Image files (*.jpg, *.jpeg, *.png, *.gif): jpg, jpeg, png, gif"/>
    <param name="maxByteSize" value="{$maxMByte}"/>
    <param name="rejectFileFilter" value="exe, bat, php, htm, html"/>
    <param name="compressionQuality" value="{$compressionQuality}"/>
    <param name="showHTTPResponse" value="{showHTTPResponse}" />
    {$maxImageWidthParam}
    {$maxImageHeightParam}
    <strong>
      This browser does not have a Java Plug-in.
      <br />
      <a href="http://java.com">
        Get the latest Java Plug-in here.
      </a>
    </strong>
  </applet>
  <form action="POST">
    <p id="presizeinput">{$resizeHtml}</p>
  </form>
  <br class="clearboth" />
</div>
<div id="applettext">
<p>Drag your image files to the file box or use the &lsquo;Add&rsquo; button to browse. The maximum size for one upload is set by your server at {$maxMByte}MByte. You can repeat this as many times as you wish.</p>
</div>
EOD;
    return $html;
  }
}
?>
