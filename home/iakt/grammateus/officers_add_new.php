<?php
//Connection statement
require_once('../Connections/sigma_modx.php');

//Aditional Functions
require_once('../includes/functions.inc.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("CUSTOMERCD", true, "numeric", "", "", "", "Please choose an archon.");
$formValidation->addField("CPOSITION", true, "text", "", "", "", "Please enter choose the archon's office.");
$formValidation->addField("JOINDATE", true, "date", "date", "", "", "Please enter the date the archon was installed.");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("{KT_defaultSender}");
  $emailObj->setTo("info@sigmapiphi.org");
  $emailObj->setCC("{SESSION.webEmail}");
  $emailObj->setBCC("");
  $emailObj->setSubject(" New {SESSION.BOULENAME} Boule Officer Installed");
  //WriteContent method
  $emailObj->setContent("Archon {SESSION.webFullname}, Grammateus of {SESSION.BOULENAME} has made the following update: \n\nArchon {rsBouleOfficerDetail.FIRSTNAME} {rsBouleOfficerDetail.LASTNAME} of {rsBouleOfficerDetail.BOULENAME} now holds the office of {CPOSITION}.\n\nThis installation is effective as of {JOINDATE}.\n\nDate Processed: {NOW_DT}");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

// begin Recordset
$query_rsBouleOffices = "SELECT * FROM spp_cposition WHERE CPOSITION not like '%grand%' and CPOSITION not like '%regional%' ORDER BY CPOSITION ASC";
$rsBouleOffices = $sigma_modx->SelectLimit($query_rsBouleOffices) or die($sigma_modx->ErrorMsg());
$totalRows_rsBouleOffices = $rsBouleOffices->RecordCount();
// end Recordset

// begin Recordset
$colname__rsBouleMembers = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsBouleMembers = $_SESSION['BOULENAME'];
}
$query_rsBouleMembers = sprintf("SELECT * FROM vw_gramm_archons WHERE STATUSSTT = 'Active' AND BOULENAME = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname__rsBouleMembers, "text"));
$rsBouleMembers = $sigma_modx->SelectLimit($query_rsBouleMembers) or die($sigma_modx->ErrorMsg());
$totalRows_rsBouleMembers = $rsBouleMembers->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_officers = new tNG_insert($sigma_modx);
$tNGs->addTransaction($ins_spp_officers);
// Register triggers
$ins_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "officers_list.php");
$ins_spp_officers->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_officers->setTable("spp_officers");
$ins_spp_officers->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$ins_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "POST", "CPOSITION");
$ins_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$ins_spp_officers->addColumn("REGIONCD", "STRING_TYPE", "VALUE", "{rsBouleMembers.REGIONCD}");
$ins_spp_officers->addColumn("CHAPTERCD", "STRING_TYPE", "VALUE", "{rsBouleMembers.CHAPTERID}");
$ins_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "VALUE", "Active");
$ins_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "VALUE", "{SESSION.webFullname}");
$ins_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_officers = $tNGs->getRecordset("spp_officers");
$totalRows_rsspp_officers = $rsspp_officers->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<?php
	echo $tNGs->getErrorMsg();
?>
<form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
  <table cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr>
      <td class="KT_th"><label for="CUSTOMERCD">ARCHON:</label></td>
      <td><select name="CUSTOMERCD" id="CUSTOMERCD">
        <?php
  while(!$rsBouleMembers->EOF){
?>
        <option value="<?php echo $rsBouleMembers->Fields('CUSTOMERID')?>"<?php if (!(strcmp($rsBouleMembers->Fields('CUSTOMERID'), $rsspp_officers->Fields('CUSTOMERCD')))) {echo "SELECTED";} ?>><?php echo $rsBouleMembers->Fields('FULLNAME')?></option>
        <?php
    $rsBouleMembers->MoveNext();
  }
  $rsBouleMembers->MoveFirst();
?>
      </select>
          <?php echo $tNGs->displayFieldError("spp_officers", "CUSTOMERCD"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="CPOSITION">OFFICE:</label></td>
      <td><select name="CPOSITION" id="CPOSITION">
        <?php
  while(!$rsBouleOffices->EOF){
?>
        <option value="<?php echo $rsBouleOffices->Fields('CPOSITION')?>"<?php if (!(strcmp($rsBouleOffices->Fields('CPOSITION'), $rsspp_officers->Fields('CPOSITION')))) {echo "SELECTED";} ?>><?php echo $rsBouleOffices->Fields('CPOSITION')?></option>
        <?php
    $rsBouleOffices->MoveNext();
  }
  $rsBouleOffices->MoveFirst();
?>
      </select>
          <?php echo $tNGs->displayFieldError("spp_officers", "CPOSITION"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="JOINDATE">START DATE:</label></td>
      <td><input type="text" name="JOINDATE" id="JOINDATE" value="<?php echo KT_formatDate($rsspp_officers->Fields('JOINDATE')); ?>" size="32" />
          <?php echo $tNGs->displayFieldHint("JOINDATE");?> <?php echo $tNGs->displayFieldError("spp_officers", "JOINDATE"); ?> </td>
    </tr>
    <tr class="KT_buttons">
      <td colspan="2"><input type="submit" name="KT_Insert1" id="KT_Insert1" value="Insert record" />
      </td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?><!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsBouleOffices->Close();

$rsBouleMembers->Close();
?>
