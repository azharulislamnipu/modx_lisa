<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("event_date", true, "date", "", "", "", "");
$formValidation->addField("dept_cd", true, "text", "", "", "", "");
$formValidation->addField("art_title", true, "text", "", "", "", "");
$formValidation->addField("art_file", true, "", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// Start trigger
$formValidation1 = new tNG_FormValidation();
$formValidation1->addField("photo_file", true, "", "", "", "", "");
$formValidation1->addField("ph_caption", true, "text", "", "", "", "");
$formValidation1->addField("dept_cd", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation1);
// End trigger

// Start trigger
$formValidation2 = new tNG_FormValidation();
$formValidation2->addField("archon_sub_stat", true, "text", "", "", "", "");
$formValidation2->addField("grapter_sub_stat", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation2);
// End trigger

//start Trigger_FileDelete1 trigger
//remove this line if you want to edit the code by hand 
function Trigger_FileDelete1(&$tNG) {
  $deleteObj = new tNG_FileDelete($tNG);
  $deleteObj->setFolder("../grapter/images/{rsArticle.iss_cd}/{rsArticle.CHAPTERID}/");
  $deleteObj->setDbFieldName("art_title");
  return $deleteObj->Execute();
}
//end Trigger_FileDelete1 trigger

//start Trigger_ImageUpload trigger
//remove this line if you want to edit the code by hand 
function Trigger_ImageUpload(&$tNG) {
  $uploadObj = new tNG_ImageUpload($tNG);
  $uploadObj->setFormFieldName("photo_file");
  $uploadObj->setDbFieldName("photo_file");
  $uploadObj->setFolder("../grapter/images/{rsArticle.iss_cd}/{rsArticle.CHAPTERID}/");
  $uploadObj->setMaxSize(51200);
  $uploadObj->setAllowedExtensions("jpg, jpeg");
  $uploadObj->setRename("custom");
  $uploadObj->setRenameRule("{rsArticle.art_id}-{rsArticle.iss_cd}-{rsArticle.dept_cd}-{rsArticle.ORGCD}-{KT_name}.{KT_ext}");
  return $uploadObj->Execute();
}
//end Trigger_ImageUpload trigger

//start Trigger_FileDelete trigger
//remove this line if you want to edit the code by hand 
function Trigger_FileDelete(&$tNG) {
  $deleteObj = new tNG_FileDelete($tNG);
  $deleteObj->setFolder("../grapter/images/{iss_cd}/{CHAPTERID}/");
  $deleteObj->setDbFieldName("art_file");
  return $deleteObj->Execute();
}
//end Trigger_FileDelete trigger

//start Trigger_FileUpload trigger
//remove this line if you want to edit the code by hand 
function Trigger_FileUpload(&$tNG) {
  $uploadObj = new tNG_FileUpload($tNG);
  $uploadObj->setFormFieldName("art_file");
  $uploadObj->setDbFieldName("art_file");
  $uploadObj->setFolder("../grapter/images/{iss_cd}/{CHAPTERID}/");
  $uploadObj->setMaxSize(51200);
  $uploadObj->setAllowedExtensions("txt, doc, docx");
  $uploadObj->setRename("custom");
  $uploadObj->setRenameRule("{art_id}-{iss_cd}-{dept_cd}-{ORGCD}-{KT_name}.{KT_ext}");
  return $uploadObj->Execute();
}
//end Trigger_FileUpload trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsDept = "SELECT dept_id, dept_cd, dept_order, dept_nm FROM grapter_departments ORDER BY dept_nm ASC";
$rsDept = mysql_query($query_rsDept, $sigma_modx) or die(mysql_error());
$row_rsDept = mysql_fetch_assoc($rsDept);
$totalRows_rsDept = mysql_num_rows($rsDept);

$colname_rsArticle = "-1";
if (isset($_GET['art_id'])) {
  $colname_rsArticle = $_GET['art_id'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArticle = sprintf("SELECT art_id, CUSTOMERID, event_date, event_loc, art_title, art_file, art_content, art_cmt, iss_cd, dept_cd, CHAPTERID, ORGCD, date_submit, submit_by FROM grapter_articles WHERE art_id = %s", GetSQLValueString($colname_rsArticle, "int"));
$rsArticle = mysql_query($query_rsArticle, $sigma_modx) or die(mysql_error());
$row_rsArticle = mysql_fetch_assoc($rsArticle);
$totalRows_rsArticle = mysql_num_rows($rsArticle);

$colname_rsBoule = "-1";
if (isset($_GET['CHAPTERID'])) {
  $colname_rsBoule = $_GET['CHAPTERID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoule = sprintf("SELECT CHAPTERID, BOULENAME, REGIONCD, REGIONNAME, ORGCD FROM spp_boule WHERE CHAPTERID = %s ORDER BY BOULENAME ASC", GetSQLValueString($colname_rsBoule, "text"));
$rsBoule = mysql_query($query_rsBoule, $sigma_modx) or die(mysql_error());
$row_rsBoule = mysql_fetch_assoc($rsBoule);
$totalRows_rsBoule = mysql_num_rows($rsBoule);

$colname_rsDeptFullName = "-1";
if (isset($_GET['dept_cd'])) {
  $colname_rsDeptFullName = $_GET['dept_cd'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsDeptFullName = sprintf("SELECT * FROM grapter_departments WHERE dept_cd = %s", GetSQLValueString($colname_rsDeptFullName, "text"));
$rsDeptFullName = mysql_query($query_rsDeptFullName, $sigma_modx) or die(mysql_error());
$row_rsDeptFullName = mysql_fetch_assoc($rsDeptFullName);
$totalRows_rsDeptFullName = mysql_num_rows($rsDeptFullName);

$colname_rsPhotos = "-1";
if (isset($_GET['art_id'])) {
  $colname_rsPhotos = $_GET['art_id'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsPhotos = sprintf("SELECT photo_id, photo_file, ph_caption, art_id FROM grapter_photos WHERE art_id = %s ORDER BY photo_id ASC", GetSQLValueString($colname_rsPhotos, "int"));
$rsPhotos = mysql_query($query_rsPhotos, $sigma_modx) or die(mysql_error());
$row_rsPhotos = mysql_fetch_assoc($rsPhotos);
$totalRows_rsPhotos = mysql_num_rows($rsPhotos);

// Make an insert transaction instance
$ins_grapter_articles = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_grapter_articles);
// Register triggers
$ins_grapter_articles->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_grapter_articles->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_grapter_articles->registerTrigger("END", "Trigger_Default_Redirect", 99, "submit_article.php?art_id={art_id}&ORGCD={ORGCD}&dept_cd={dept_cd}");
$ins_grapter_articles->registerTrigger("AFTER", "Trigger_FileUpload", 97);
// Add columns
$ins_grapter_articles->setTable("grapter_articles");
$ins_grapter_articles->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID", "{GET.CUSTOMERCD}");
$ins_grapter_articles->addColumn("event_loc", "STRING_TYPE", "POST", "event_loc");
$ins_grapter_articles->addColumn("event_date", "DATE_TYPE", "POST", "event_date");
$ins_grapter_articles->addColumn("dept_cd", "STRING_TYPE", "POST", "dept_cd");
$ins_grapter_articles->addColumn("art_title", "STRING_TYPE", "POST", "art_title");
$ins_grapter_articles->addColumn("art_file", "FILE_TYPE", "FILES", "art_file");
$ins_grapter_articles->addColumn("art_cmt", "STRING_TYPE", "POST", "art_cmt");
$ins_grapter_articles->addColumn("date_submit", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_grapter_articles->addColumn("submit_by", "STRING_TYPE", "VALUE", "{GET.webShortname}");
$ins_grapter_articles->addColumn("iss_cd", "STRING_TYPE", "POST", "iss_cd", "{GET.iss_cd}");
$ins_grapter_articles->addColumn("CHAPTERID", "STRING_TYPE", "POST", "CHAPTERID", "{GET.CHAPTERID}");
$ins_grapter_articles->addColumn("ORGCD", "NUMERIC_TYPE", "POST", "ORGCD", "{rsBoule.ORGCD}");
$ins_grapter_articles->setPrimaryKey("art_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_grapter_articles = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_grapter_articles);
// Register triggers
$upd_grapter_articles->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_grapter_articles->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_grapter_articles->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
$upd_grapter_articles->registerTrigger("AFTER", "Trigger_FileUpload", 97);
// Add columns
$upd_grapter_articles->setTable("grapter_articles");
$upd_grapter_articles->addColumn("event_date", "DATE_TYPE", "POST", "event_date");
$upd_grapter_articles->addColumn("event_loc", "STRING_TYPE", "POST", "event_loc");
$upd_grapter_articles->addColumn("dept_cd", "STRING_TYPE", "POST", "dept_cd");
$upd_grapter_articles->addColumn("art_title", "STRING_TYPE", "POST", "art_title");
$upd_grapter_articles->addColumn("art_file", "FILE_TYPE", "POST", "art_file");
$upd_grapter_articles->addColumn("art_cmt", "STRING_TYPE", "POST", "art_cmt");
$upd_grapter_articles->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");
$upd_grapter_articles->addColumn("date_submit", "DATE_TYPE", "CURRVAL", "");
$upd_grapter_articles->addColumn("submit_by", "STRING_TYPE", "CURRVAL", "");
$upd_grapter_articles->addColumn("iss_cd", "STRING_TYPE", "POST", "iss_cd");
$upd_grapter_articles->addColumn("CHAPTERID", "STRING_TYPE", "POST", "CHAPTERID");
$upd_grapter_articles->addColumn("ORGCD", "NUMERIC_TYPE", "POST", "ORGCD");
$upd_grapter_articles->setPrimaryKey("art_id", "NUMERIC_TYPE", "GET", "art_id");

// Make an instance of the transaction object
$del_grapter_articles = new tNG_multipleDelete($conn_sigma_modx);
$tNGs->addTransaction($del_grapter_articles);
// Register triggers
$del_grapter_articles->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_grapter_articles->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
$del_grapter_articles->registerTrigger("AFTER", "Trigger_FileDelete", 98);
$del_grapter_articles->registerTrigger("AFTER", "Trigger_FileDelete1", 98);
// Add columns
$del_grapter_articles->setTable("grapter_articles");
$del_grapter_articles->setPrimaryKey("art_id", "NUMERIC_TYPE", "GET", "art_id");

// Make an update transaction instance
$upd_grapter_articles1 = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_grapter_articles1);
// Register triggers
$upd_grapter_articles1->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update5");
$upd_grapter_articles1->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation2);
$upd_grapter_articles1->registerTrigger("END", "Trigger_Default_Redirect", 99, "submission_thanks.php?art_id={art_id}");
// Add columns
$upd_grapter_articles1->setTable("grapter_articles");
$upd_grapter_articles1->addColumn("archon_sub_stat", "STRING_TYPE", "POST", "archon_sub_stat");
$upd_grapter_articles1->addColumn("grapter_sub_stat", "STRING_TYPE", "POST", "grapter_sub_stat");
$upd_grapter_articles1->addColumn("art_id", "NUMERIC_TYPE", "POST", "art_id");
$upd_grapter_articles1->setPrimaryKey("art_id", "NUMERIC_TYPE", "GET", "art_id");

// Make an insert transaction instance
$ins_grapter_photos = new tNG_insert($conn_sigma_modx);
$tNGs->addTransaction($ins_grapter_photos);
// Register triggers
$ins_grapter_photos->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert2");
$ins_grapter_photos->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation1);
$ins_grapter_photos->registerTrigger("END", "Trigger_Default_Redirect", 99, "submit_article.php?photo_id={photo_id}&art_id={art_id}&ORGCD={ORGCD}&dept_cd={dept_cd}");
$ins_grapter_photos->registerTrigger("AFTER", "Trigger_ImageUpload", 97);
// Add columns
$ins_grapter_photos->setTable("grapter_photos");
$ins_grapter_photos->addColumn("photo_file", "FILE_TYPE", "FILES", "photo_file");
$ins_grapter_photos->addColumn("ph_caption", "STRING_TYPE", "POST", "ph_caption");
$ins_grapter_photos->addColumn("iss_cd", "STRING_TYPE", "POST", "iss_cd", "{rsArticle.iss_cd}");
$ins_grapter_photos->addColumn("ORGCD", "NUMERIC_TYPE", "POST", "ORGCD", "{rsArticle.ORGCD}");
$ins_grapter_photos->addColumn("dept_cd", "STRING_TYPE", "POST", "dept_cd", "{rsArticle.dept_cd}");
$ins_grapter_photos->addColumn("CHAPTERID", "STRING_TYPE", "POST", "CHAPTERID", "{rsArticle.CHAPTERID}");
$ins_grapter_photos->addColumn("art_id", "NUMERIC_TYPE", "POST", "art_id", "{GET.art_id}");
$ins_grapter_photos->addColumn("date_submit", "DATE_TYPE", "POST", "date_submit", "{NOW_DT}");
$ins_grapter_photos->addColumn("archon_id", "NUMERIC_TYPE", "POST", "archon_id", "{rsArticle.CUSTOMERID}");
$ins_grapter_photos->setPrimaryKey("photo_id", "NUMERIC_TYPE");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsgrapter_articles = $tNGs->getRecordset("grapter_articles");
$row_rsgrapter_articles = mysql_fetch_assoc($rsgrapter_articles);
$totalRows_rsgrapter_articles = mysql_num_rows($rsgrapter_articles);

// Get the transaction recordset
$rsgrapter_photos = $tNGs->getRecordset("grapter_photos");
$row_rsgrapter_photos = mysql_fetch_assoc($rsgrapter_photos);
$totalRows_rsgrapter_photos = mysql_num_rows($rsgrapter_photos);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../includes/resources/calendar.js"></script>
<style type="text/css">
<!--
.style1 {
	font-size: 18px;
	font-weight: bold;
}
.style2 {font-size: 36px}
-->
</style>
<?php echo $tNGs->displayValidationRules();?>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">
  <!-- #HEADER: Holds title, and logo -->
  <div id="header">
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
  </div>
  <!--  #END HEADER -->
  <!-- #PAGE CONTENT BEGINS -->
  <div id="page">
    <!-- #SIDENAV: side navigation, logo and search box -->
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
    <!-- #END SIDENAV -->
    <!-- #MAIN COLUMN -->
    <div class="floatLeft width804"> <!-- InstanceBeginEditable name="content" --> <?php echo $tNGs->displayValidationRules();?>
      <script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
      <script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
      <script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
    </script>
          <?php 
// Show IF Conditional region4 
if (@$_GET['art_id'] != "") {
?>
            <h1 class="yellow">ARTICLE</h1>
        <div class="contentBlock">
              <table cellspacing="4" cellpadding="4">
                <tr>
                  <td valign="top">DEPARTMENT</td>
                  <td valign="top"><?php echo $row_rsDeptFullName['dept_nm']; ?></td>
                </tr>
                <tr>
                  <td valign="top">TITLE</td>
                  <td valign="top"><p><span class="style1"><?php echo $row_rsArticle['art_title']; ?></span><br />
                    (<?php echo $row_rsArticle['art_file']; ?>)</p>
                    <p><a href="edit_article.php?art_id=<?php echo $row_rsArticle['art_id']; ?>">Change</a></p></td>
                </tr>
                <tr>
                  <td valign="top">EVENT DATE</td>
                  <td valign="top"><?php echo KT_formatDate($row_rsArticle['event_date']); ?></td>
                </tr>
                <tr>
                  <td valign="top">EVENT LOCATION</td>
                  <td valign="top"><?php echo $row_rsArticle['event_loc']; ?></td>
                </tr>
                <tr>
                  <td valign="top">SUBMITTED BY</td>
                  <td valign="top"><?php echo $row_rsArticle['submit_by']; ?></td>
                </tr>
                <tr>
                  <td valign="top">SUBMIT DATE</td>
                  <td valign="top"><?php echo KT_formatDate($row_rsArticle['date_submit']); ?></td>
                </tr>
              </table>
        </div>
        <?php } 
// endif Conditional region4
?>
          <?php 
// Show IF Conditional region5 
if (@$_GET['art_id'] == "") {
?>
            <?php
	echo $tNGs->getErrorMsg();
?>
            <div class="KT_tng">
              <h1>
                <?php 
// Show IF Conditional region1 
if (@$_GET['art_id'] == "") {
?>
                  <?php echo NXT_getResource("Insert_FH"); ?>
                  <?php 
// else Conditional region1
} else { ?>
                  <?php echo NXT_getResource("Update_FH"); ?>
                  <?php } 
// endif Conditional region1
?>
                Grapter_articles </h1>
              <div class="KT_tngform">
                <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" enctype="multipart/form-data">
                  <?php $cnt1 = 0; ?>
                  <?php do { ?>
                    <?php $cnt1++; ?>
                    <?php 
// Show IF Conditional region1 
if (@$totalRows_rsgrapter_articles > 1) {
?>
                      <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
                      <?php } 
// endif Conditional region1
?>
                    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
                      <tr>
                        <td class="KT_th"><label for="event_date_<?php echo $cnt1; ?>">Event_date:</label></td>
                        <td><input name="event_date_<?php echo $cnt1; ?>" id="event_date_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsgrapter_articles['event_date']); ?>" size="10" maxlength="22" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                            <?php echo $tNGs->displayFieldHint("event_date");?> <?php echo $tNGs->displayFieldError("grapter_articles", "event_date", $cnt1); ?> </td>
                      </tr>
                      <tr>
                        <td class="KT_th"><label for="event_loc_<?php echo $cnt1; ?>">EVENT LOCATION:</label></td>
                        <td><input type="text" name="event_loc_<?php echo $cnt1; ?>" id="event_loc_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['event_loc']); ?>" size="32" maxlength="40" />
                            <?php echo $tNGs->displayFieldHint("event_loc");?> <?php echo $tNGs->displayFieldError("grapter_articles", "event_loc", $cnt1); ?> </td>
                      </tr>
                      <tr>
                        <td class="KT_th"><label for="dept_cd_<?php echo $cnt1; ?>">DEPARTMENT:</label></td>
                        <td><select name="dept_cd_<?php echo $cnt1; ?>" id="dept_cd_<?php echo $cnt1; ?>">
                            <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                            <?php 
do {  
?>
                            <option value="<?php echo $row_rsDept['dept_cd']?>"<?php if (!(strcmp($row_rsDept['dept_cd'], $row_rsgrapter_articles['dept_cd']))) {echo "SELECTED";} ?>><?php echo $row_rsDept['dept_nm']?></option>
                            <?php
} while ($row_rsDept = mysql_fetch_assoc($rsDept));
  $rows = mysql_num_rows($rsDept);
  if($rows > 0) {
      mysql_data_seek($rsDept, 0);
	  $row_rsDept = mysql_fetch_assoc($rsDept);
  }
?>
                          </select>
                            <?php echo $tNGs->displayFieldError("grapter_articles", "dept_cd", $cnt1); ?> </td>
                      </tr>
                      <tr>
                        <td class="KT_th"><label for="art_title_<?php echo $cnt1; ?>">ARTICLE TITLE:</label></td>
                        <td><input type="text" name="art_title_<?php echo $cnt1; ?>" id="art_title_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['art_title']); ?>" size="82" maxlength="125" />
                            <?php echo $tNGs->displayFieldHint("art_title");?> <?php echo $tNGs->displayFieldError("grapter_articles", "art_title", $cnt1); ?> </td>
                      </tr>
                      <tr>
                        <td class="KT_th"><label for="art_file_<?php echo $cnt1; ?>">ARTICLE FILE:</label></td>
                        <td><input type="file" name="art_file_<?php echo $cnt1; ?>" id="art_file_<?php echo $cnt1; ?>" size="67" />
                            <?php echo $tNGs->displayFieldError("grapter_articles", "art_file", $cnt1); ?> </td>
                      </tr>
                      <tr>
                        <td class="KT_th"><label for="art_cmt_<?php echo $cnt1; ?>">COMMENT:</label></td>
                        <td><textarea name="art_cmt_<?php echo $cnt1; ?>" id="art_cmt_<?php echo $cnt1; ?>" cols="80" rows="5"><?php echo KT_escapeAttribute($row_rsgrapter_articles['art_cmt']); ?></textarea>
                            <?php echo $tNGs->displayFieldHint("art_cmt");?> <?php echo $tNGs->displayFieldError("grapter_articles", "art_cmt", $cnt1); ?> </td>
                      </tr>
                      <tr>
                        <td class="KT_th">DATE SUBMIT:</td>
                        <td><?php echo KT_formatDate($row_rsgrapter_articles['date_submit']); ?></td>
                      </tr>
                      <tr>
                        <td class="KT_th">Submit_by:</td>
                        <td><?php echo KT_escapeAttribute($row_rsgrapter_articles['submit_by']); ?></td>
                      </tr>
                    </table>
                    <input type="hidden" name="kt_pk_grapter_articles_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['kt_pk_grapter_articles']); ?>" />
                    <input type="hidden" name="CUSTOMERID_<?php echo $cnt1; ?>" id="CUSTOMERID_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_GET['CUSTOMERID']); ?>" />
                    <input type="hidden" name="iss_cd_<?php echo $cnt1; ?>" id="iss_cd_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_GET['iss_cd']); ?>" />
                    <input type="hidden" name="CHAPTERID_<?php echo $cnt1; ?>" id="CHAPTERID_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_GET['CHAPTERID']); ?>" />
                    <input type="hidden" name="ORGCD_<?php echo $cnt1; ?>" id="ORGCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsBoule['ORGCD']); ?>" />
                    <?php } while ($row_rsgrapter_articles = mysql_fetch_assoc($rsgrapter_articles)); ?>
                  <div class="KT_bottombuttons">
                    <div>
                      <?php 
      // Show IF Conditional region1
      if (@$_GET['art_id'] == "") {
      ?>
                        <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
                        <?php 
      // else Conditional region1
      } else { ?>
                        <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
                        <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
                        <?php }
      // endif Conditional region1
      ?>
                      <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
                    </div>
                  </div>
                </form>
              </div>
              <br class="clearfixplain" />
            </div>
        <?php } 
// endif Conditional region5
?>
          <p>&nbsp;</p>
      <h1 class="yellow">IMAGES</h1>
      
      <p style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%">You may upload a maxium of four (4) photos.</p>
      <p style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%">JPG format only. Maximum file size is 25 MB.</p>
      <p style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%">You are not required to include photos.</p>
      <?php if ($totalRows_rsPhotos > 0) { // Show if recordset not empty ?>
<div class="contentBlock"><br />
  <br />
<table cellspacing="4" cellpadding="4">
                <?php do { ?>
                  <tr valign="top">
                    <td><p><a href="edit_image.php?photo_id=<?php echo $row_rsPhotos['photo_id']; ?>&amp;art_id=<?php echo $row_rsPhotos['art_id']; ?>">Change Image</a></p>
                    </td>
                    <td><img src="images/<?php echo $row_rsArticle['iss_cd']; ?>/<?php echo $row_rsArticle['CHAPTERID']; ?>/<?php echo $row_rsPhotos['photo_file']; ?>"  width="200"/></td>
                    <td><p><strong><?php echo $row_rsPhotos['photo_file']; ?></strong></p>
                        <p><?php echo $row_rsPhotos['ph_caption']; ?></p>                      <p><a href="edit_caption.php?photo_id=<?php echo $row_rsPhotos['photo_id']; ?>&amp;art_id=<?php echo $row_rsPhotos['art_id']; ?>">Edit Caption</a></p></td>
                  </tr>
                  <?php } while ($row_rsPhotos = mysql_fetch_assoc($rsPhotos)); ?>
          </table>
        </div>
        <?php } // Show if recordset not empty ?>
      <?php 
// Show IF Conditional region6 
if (@$totalRows_rsPhotos < 4) {
?>
            <div class="contentBlock style2 style2">
              <form method="post" id="form2" NAME="form2" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" enctype="multipart/form-data">
                <table cellpadding="2" cellspacing="0" class="KT_tngtable">
                  <tr>
                    <td class="KT_th"><label for="photo_file">IMAGE:</label></td>
                    <td><input type="file" name="photo_file" id="photo_file" size="67" />
                        <?php echo $tNGs->displayFieldError("grapter_photos", "photo_file"); ?> </td>
                  </tr>
                  <tr>
                    <td class="KT_th"><label for="ph_caption">CAPTION:</label></td>
                    <td><textarea name="ph_caption" id="ph_caption" cols="80" rows="5"><?php echo KT_escapeAttribute($row_rsgrapter_photos['ph_caption']); ?></textarea>
                        <?php echo $tNGs->displayFieldHint("ph_caption");?> <?php echo $tNGs->displayFieldError("grapter_photos", "ph_caption"); ?> </td>
                  </tr>
                  <tr class="KT_buttons">
                    <td colspan="2"><input type="submit" name="KT_Insert2" id="KT_Insert2" value="UPLOAD" />                    </td>
                  </tr>
                </table>
                <input type="hidden" name="iss_cd" id="iss_cd" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['iss_cd']); ?>" />
                <input type="hidden" name="ORGCD" id="ORGCD" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['ORGCD']); ?>" />
                <input type="hidden" name="dept_cd" id="dept_cd" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['dept_cd']); ?>" />
                <input type="hidden" name="CHAPTERID" id="CHAPTERID" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['CHAPTERID']); ?>" />
                <input type="hidden" name="art_id" id="art_id" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['art_id']); ?>" />
                <input type="hidden" name="date_submit" id="date_submit" value="<?php echo KT_formatDate($row_rsgrapter_photos['date_submit']); ?>" />
                <input type="hidden" name="archon_id" id="archon_id" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['archon_id']); ?>" />
              </form>
              <p>&nbsp;</p>
            </div>
      <?php } 
// endif Conditional region6
?>
      <?php 
// Show IF Conditional region7 
if (@$_GET['art_id'] != "") {
?>
        <div style="text-align:center; background-color:#CC3300">
          <form method="post" id="form3" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
            <p style="color: white; font-size: 36px; margin: 0 0 6px 0; line-height:125%">Please review all of your content carefully. </p>
            <p style="color: white;  font-size: 36px; margin: 0 0 6px 0; line-height:125%">Once you click &quot;SUBMIT ARTICLE&quot; neither the article nor the images (if any) may be modified or revised.</p>
            <table width="100%" cellpadding="2" cellspacing="0" class="KT_tngtable">
              <tr>
                <td colspan="2" align="center"><input type="submit" name="KT_Update5" id="KT_Update5" value="SUBMIT ARTICLE"  style="font-size: 36px;" /> 
                </td>
              </tr>
            </table>
            <input type="hidden" name="archon_sub_stat" id="archon_sub_stat" value="Final" />
            <input type="hidden" name="grapter_sub_stat" id="grapter_sub_stat" value="Final" />
            <input type="hidden" name="art_id" id="art_id" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['art_id']); ?>" />
          </form>
          <p>&nbsp;</p>
        </div>
        <?php } 
// endif Conditional region7
?><!-- InstanceEndEditable --> </div>
    <!-- #END MAIN COLUMN -->
  </div>
  <!-- #PAGE CONTENT ENDS -->
</div>
<!-- #CONTENT ENDS -->
<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsDept);

mysql_free_result($rsArticle);

mysql_free_result($rsBoule);

mysql_free_result($rsDeptFullName);

mysql_free_result($rsPhotos);
?>
