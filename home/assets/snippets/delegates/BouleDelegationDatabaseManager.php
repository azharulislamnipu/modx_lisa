<?php

/**
 * Handles the database interactions for the Boule delegation administration 
 * @author Pat Heard {http://fullahead.org}
 */

require_once 'DelegateBean.php';
require_once '/opt/lampp/htdocs/modx_lisa/home/index.php';
define('MODX_API_MODE', true); // Allows this class to use the MODx API

class BouleDelegationDatabaseManager {
        
    private $modx = null;

    /** Initialize the database manager */
    public function __construct(&$modx = null){
        if($modx == null){
            die('You must pass a valid $modx object reference to the BouleDelegationDatabaseManager.');
        }
        $this->modx = $modx; 
    } 
    
    /** Load the delegates for a given Boule */
    public function getDelegation($bouleCode = ''){
        
        $delegateBeans = array();
        
        if(!empty($bouleCode)){
        
            // Get the delegates/alterates for the boule
            $results = $this->modx->db->query('
                SELECT 
                    D.delegate_id,
                    D.delegate_type, 
                    D.APPOINTED, 
                    D.CERTIFIED, 
                    D.CONFIRMCOMMENT,
                    concat_ws(_latin1" ",concat(ucase(substr(A.FIRSTNAME,1,1)),lcase(substr(A.FIRSTNAME,2))),concat(ucase(substr(A.MIDDLEINITIAL,1,1)),lcase(substr(A.MIDDLEINITIAL,2))),concat(ucase(substr(A.LASTNAME,1,1)),lcase(substr(A.LASTNAME,2))),A.SUFFIX) AS L2R_FULLNAME2,
                    A.CUSTOMERALTCD,
                    CASE
                        WHEN A.CUSTOMERTYPE = "Member" AND A.STATUSSTT = "Active" AND ISNULL(A.CUSTOMERCLASSSTT) THEN "1"
                        ELSE "0"
                    END AS MEMBER_ELIGIBLE
                FROM 
                    spp_delegates D 
                INNER JOIN 
                    spp_archons A
                ON 
                    D.CUSTOMERCD = A.CUSTOMERID 
                WHERE 
                    D.BOULECD = "' . $this->modx->db->escape($bouleCode) . '" 
                    AND D.delegate_type IN("D", "A")
                    AND D.delegate_status = "A"
                ORDER BY
                    D.delegate_type DESC
            ');

            if($this->modx->db->getRecordCount($results) >= 1) {
                while($row = $this->modx->db->getRow($results)){
                    $delegateBeans[$row['delegate_id']] = new DelegateBean(array(
                        'id'                    => $row['delegate_id'],
                        'name'                  => $row['L2R_FULLNAME2'],
                        'memberNumber'          => $row['CUSTOMERALTCD'],
                        'type'                  => $row['delegate_type'],
                        'appointed'             => $row['APPOINTED'] === '1',
                        'certified'             => $row['CERTIFIED'] === '1',
                        'certifiedComment'      => $row['CONFIRMCOMMENT'],
                        'financiallyEligible'   => $row['MEMBER_ELIGIBLE'] === '1',                         
                        'ineligible'            => $row['APPOINTED'] === '2'
                    ));                   
                }
            } 
        }        
        return $delegateBeans;
        
    }
    
    /** Gets the delegate and alternates count for a given boule in a region */
    public function getDelegationCounts($regionName = '', $bouleName = ''){
        
        // Get the delegate and alternate counts
        $countDelegates = 0;
        $countAlternates = 0;
        
        if(!empty($regionName) && !empty($bouleName)){
            $results = $this->modx->db->select("DELEGATES, ALTERNATES", $this->modx->getFullTableName('vw_delegate_count'), "REGIONNAME='".$this->modx->db->escape($regionName)."' AND BOULENAME='".$this->modx->db->escape($bouleName)."'");
            if($this->modx->db->getRecordCount($results) >= 1) {
                $row = $this->modx->db->getRow($results);
                $countDelegates = intval($row['DELEGATES']);
                $countAlternates = intval($row['ALTERNATES']);
            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR, 'Failed to get delegate/alternate count: ' . mysql_error()); 
            }
        }
        
        return array(
            'count_delegates'   => $countDelegates,
            'count_alternates'   => $countAlternates
        );
    }
    
    /** Updates a given Boule's delegation's appointed status 
     *  @param $bouleCode String Boule code of the delegates
     *  @param $appointedBy String web userid of the person performing the appointment
     *  @param $appointed String "1" for appointed, "0" for not
     */
    public function setDelegationAppointed($bouleCode = '', $appointedBy = '', $appointed = ''){
        
        $success = false;        
        if(!empty($bouleCode) && !empty($appointedBy)){
            
            $fields = array(
                'APPOINTED'	=> ($appointed === '1' ? '1' : '0'),
                'APPTDATE'	=> date('Y-m-d H:i:s'),
                'APPTBY'	=> substr($appointedBy, 0, 20),
                
                // Clean out previous certifications
                'CERTIFIED'     => '0',
                'CERTIFYDATE'   => null,
                'CERTIFYBY'     => null,
                'CONFIRMCOMMENT'=> null                
            );

            // Update the delegation
            $result = $this->modx->db->update( $fields, $this->modx->getFullTableName('spp_delegates'), 'BOULECD = "' . $this->modx->db->escape($bouleCode) . '" AND delegate_status = "A"' );

            // Rousing success!
            if($result){
                $success = true;

            // Pitiful error....
            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR, 'Failed to update Boule delegates APPOINTED.spp_delegates field: ' . mysql_error());           
            }
        }        
        return $success;
    }  
    
    
    /** Updates a given Boule's delegation's certified status 
     *  @param $bouleCode String Boule code of the delegates
     *  @param $certifyBy String web userid of the person performing the certification
     *  @param $delegates array Array of DelegateBeans to certify or decertify
     *  @return $updateCount int A count of the number of successful updates
     */
    public function setDelegationCertified($bouleCode, $certifiedBy, $delegates){
        
        $success = false;      
        if(!empty($bouleCode) && !empty($certifiedBy) && !empty($delegates)){
        
            $certifyDate =  date('Y-m-d H:i:s');
            
            // Loop through the delegates and certify the ones who have been modified since load
            foreach ($delegates as $d){                     
                if($d->isModified){
                    $fields = array(
                        'CERTIFIED'         => ($d->certified ? '1' : '0'),
                        'CERTIFYDATE'       => $certifyDate,
                        'CERTIFYBY'         => substr($certifiedBy, 0, 20),
                        'CONFIRMCOMMENT'    => substr($this->modx->db->escape($d->certifiedComment), 0, 65535)
                    );

                    // Update the delegate
                    $result = $this->modx->db->update( $fields, $this->modx->getFullTableName('spp_delegates'), 'BOULECD = "' . $this->modx->db->escape($bouleCode) . '" AND delegate_id = '. $this->modx->db->escape($d->id) .' AND delegate_status = "A" AND APPOINTED = "1"' );                
                    if($result){
                        $success = true; 

                    // Pitiful error....
                    } else {
                        $this->modx->log(modX::LOG_LEVEL_ERROR, 'Failed to certify Boule delegate with spp.delegates.delegate_id ' . $d->id . ': ' . mysql_error());           
                    } 
                }
            } 
        }        
        return $success;
    }    
    
    /** Returns a boule that matches the give boule code
     *  @param $bouleCode String The boule code to retrieve a boule for
     *  @return $boule mixed Associative array of Boule details on success, false on fail
     */
    public function getBoule($bouleCode = ''){
        
        $boule = false;        
        
        if(!empty($bouleCode)){
            
            // Get the boule details
            $results = $this->modx->db->query('
                SELECT DISTINCT
                    B.CHAPTERID,
                    B.BOULENAME,
                    B.REGIONCD,
                    B.REGIONNAME
                FROM
                    spp_boule B
                WHERE
                    B.CHAPTERID = "'.$this->modx->db->escape($bouleCode).'"
                ORDER BY 
                    B.BOULENAME ASC
                LIMIT 1
            ');

            if($this->modx->db->getRecordCount($results) >= 1) {
                while($row = $this->modx->db->getRow($results)){
                    $boule = array(
                        'BOULECD'     => htmlspecialchars($row['CHAPTERID']),
                        'BOULENAME'   => htmlspecialchars($row['BOULENAME']),
                        'REGIONCD'   => htmlspecialchars($row['REGIONCD']),
                        'REGIONNAME'   => htmlspecialchars($row['REGIONNAME'])
                    );
                }
            }  
        }
        return $boule;
    }    
    
    /** Returns a list of Boules with finalized delegations 
     *  @param $regionCode String optionally filter boules returned by region
     *  @param $appointed String optionally only retrieve boules with delegates with a given APPOINTED value
     */
    public function getBoules($regionCode = '', $appointed = ''){
        
        $boules = false;        
        
        // Determine if a region JOIN and WHERE clause are required
        $regionCode = !empty($regionCode) ? ' B.REGIONCD = "'.$this->modx->db->escape($regionCode).'"' : '';
        $appointed = !empty($appointed) ? ' EXISTS (SELECT 1 FROM spp_delegates D WHERE D.BOULECD = B.CHAPTERID AND D.APPOINTED = "'.$appointed.'")' : '';
        $where = !empty($regionCode) || !empty($appointed) ? ' WHERE ' : '';
        $and = !empty($regionCode) && !empty($appointed) ? ' AND ' : '';
        
        // Get the delegates/alterates for the boule
        $results = $this->modx->db->query('
            SELECT DISTINCT
                B.CHAPTERID,
                B.BOULENAME
            FROM
                spp_boule B '
                . $where
                . $regionCode  
                . $and 
                . $appointed . '
            ORDER BY 
                B.BOULENAME ASC
        ');

        $boules = array();
        if($this->modx->db->getRecordCount($results) >= 1) {
            while($row = $this->modx->db->getRow($results)){
                $boules[] = array(
                    'BOULECD'     => htmlspecialchars($row['CHAPTERID']),
                    'BOULENAME'   => htmlspecialchars($row['BOULENAME'])
                );
            }
        }  
        return $boules;
    }
    
    /** Convenience method for legacy code */
    public function getFinalizedBoules($regionCode){
        return getBoules($regionCode, '1');
    }        
        
}
?>
