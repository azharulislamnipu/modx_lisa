<?php
//setlocale(LC_ALL, 'en_EN');
$main_path = str_replace('services/spp-directory', '',__DIR__);
/**
 * Local
 */
$pos = strrpos(__DIR__, "htdocs");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'htdocs/';
}


/**
 * Server
 */

/*$pos = strrpos(__DIR__, "apache");
if($pos !== false){
    $new_path = substr(__DIR__, 0, $pos) . 'apache/';
}*/


require $new_path . "includes/functions.php";
if(!is_ajax()){
    die('Nor permitted');
}
?>
	<div id="main" role="main">
        <a href="#" class="view_popup_close">X</a>
             <!-- InstanceBeginEditable name="content" -->
<section class="row grid-12">
        <div class="span-12">

<?php
$valid_id = $archousa_info = false;
if (isset($_GET['id']) && (int)$_GET['id']) {
	$user_id    = test_input( $_GET['id'] );

	$archousa_info = SPP_ARCHON_DB::get_archousa_profile_data( $user_id )['data'];
	if ( $archousa_info ) {
		$valid_id = true;
	}
}
if($valid_id){
    $fullname = "{$archousa_info['FIRSTNAME']} {$archousa_info['MIDDLEINITIAL']} {$archousa_info['LASTNAME']} {$archousa_info['SUFFIX']}";
    
?>

<div class="inner">


    <!-- #MAIN COLUMN -->

    <div class="profile_title_header clearfix">
        <h1 id="profile_title">
            <?php echo $fullname; ?>
        </h1>
    </div>

   <!-- <div class="profile_editn_vn clearfix">
        <a href="#" class="view_next_archon button">View Next Archon</a>
    </div>-->

    <div class="span-4">
        <div class="margin-top-none">
            <div class="width padding">

                <div class="prof_img">
                    <?php
                    // Show IF Conditional region9
                    if (@$archousa_info['IMAGE'] != NULL) {
                        ?>
                        <img id="profile_img" src="/home/assets/images/archousa/<?php echo $archousa_info['IMAGE']; ?>" alt="Image of <?php echo $fullname; ?>" />
                        <?php
// else Conditional region9
                    } else { ?>
                        <img src="/services/includes/tng/styles/img_not_found.gif" alt="Add your image." width="100" height="100"/>
                    <?php }
                    // endif Conditional region9
                    ?>
                    <br><br>
                </div>
                <?php
                    $archon = SPP_ARCHON_DB::get_elig_user_profile_data($archousa_info['SPOUSEID']);
                    if($archon['count'] > 0){
	                    $archon_info = $archon['data'];
//	                    $archon_fullname = "{$archon_info['FIRSTNAME']} {$archon_info['MIDDLEINITIAL']} {$archon_info['LASTNAME']} {$archon_info['SUFFIX']}";
                ?>

                        <div class="archon-archousa-info clearfix" id="archon_block" style="">
                            <div class="span-12 archons-info">
                                <h3>Archon Snapshot</h3>

                                <div class="span-4 width float-left  archon_profile_img">
	                                <?php
	                                // Show IF Conditional region9
	                                if (@$archon_info['IMAGE'] != NULL) {
		                                ?>
                                        <img src="/home/assets/images/archons/<?php echo $archon_info['IMAGE']; ?>" alt="Image of <?php echo $archon_info['L2R_FULLNAME']; ?>" />
		                                <?php
// else Conditional region9
	                                } else { ?>
                                        <img src="/services/includes/tng/styles/img_not_found.gif" alt="Add your image." />
	                                <?php }
	                                // endif Conditional region9
	                                ?>
                                </div>


                                <div class="span-8 inner">
                                    <p><?php echo $archon_info['L2R_FULLNAME'];?></p>
                                    <p class="font-small">
                                        <?php echo "<strong>{$archon_info['BOULENAME']} Boul&eacute;</strong><br />" .
                                                   "{$archon_info['BOULECITY']}, {$archon_info['BOULESTATECD']}<br />" .
                                                   "{$archon_info['REGIONNAME']} Region";
                                        if(!empty($archon_info['JOINDATE'])) {
                                            $date = strtotime($archon_info['JOINDATE']);
                                            echo "<br/>Member since " . date('F j, Y', $date);
                                        }
                                        ?>
                                    </p>

                                    <p class="btn_p"><a class="button small_button" id="view_archon_json" href="/services/spp-directory/search-results-noedit-json.php?id=<?php echo $archon_info['CUSTOMERID'];?>">View</a>
                                    </p>

                                </div></div>
                        </div>

                    <?php }?>


                <div class="contact-box" id="contact_wrap">

                    <p><strong>Mailing Address</strong><br>
                        <?php echo "{$archon_info['ADDRESS1']} {$archon_info['ADDRESS2']}<br />
                    {$archon_info['CITY']}, {$archon_info['STATECD']} {$archon_info['ZIP']}";?> </p>

                    <p><strong>Email</strong><br>
                        <a href="mailto:<?php echo $archon_info['EMAIL']; ?>">
			                <?php echo $archon_info['EMAIL']; ?>
                        </a></p>

                    <p><strong>Home Phone</strong><br>
                        <a href="tel:<?php echo $archon_info['HOMEPHONE']; ?>">
			                <?php echo $archon_info['HOMEPHONE']; ?>
                        </a></p>

                    <p><strong>Mobile</strong><br>
                        <a href="tel:<?php echo $archon_info['MOBILEPHONE']; ?>">
			                <?php echo $archon_info['MOBILEPHONE']; ?>
                        </a></p>
                    <p></p>

                    <p><strong>Birthdate</strong><br>
                        <?php echo date("F j", strtotime($archousa_info['BIRTHDATE'])); ?>
                    </p>

                </div>

                <!-- [ !LDataDisplay? &table=`spp_officers` &display=`all` &tpl=`tpl_archon_prof_offices` &filter=`CUSTOMERID,2,1` &sortBy=`START_DATE` &sortDir=`ASC`]]-->

                <div id="children_block">
                    <strong>Children</strong>

                    <?php
                    $children = SPP_ARCHON_DB::get_all_user_children($archon_info['CUSTOMERID'])['data'];
                    ?>

                    <ul class="children_list">
                        <?php
                        foreach ($children as $child){
                            echo "<li>{$child['FIRSTNAME']} {$child['MIDDLEINITIAL']} {$child['LASTNAME']} {$child['SUFFIX']}</li>";
                        }
                        ?>
                    </ul>
                </div>


            </div>
        </div>
    </div>
    <!-- #END MAIN COLUMN -->


    <!-- #BEGIN SECOND COLUMN -->
    <div class="span-4">
        <div class="padding">
            <div class="width padding">


                <!-- #END SECOND COLUMN -->


                <!-- #BEGIN SECOND COLUMN -->

                <h3>Career</h3>

                <?php
                $all_profs = SPP_ARCHON_DB::get_all_archousa_prof($user_id);
                ?>
                <div id="profile_career_box">

                    <?php if($all_profs['count'] > 0){
	                    $prof_datas = $all_profs['data'];
	                    foreach ($prof_datas as $prof_data) {
		                    $prof_title = SPP_ARCHON_DB::get_prof_title($prof_data['TITLEID'])['data'];
		                    $skill_title = '';
		                    if(is_array($prof_title)){
			                    $prof_title = reset($prof_title);
		                    }
		                    if(isset($prof_title['DESCRIPTION'])){
			                    $skill_title = $prof_title['DESCRIPTION'];
                            }
		                    echo <<<DDD
                        <p id="prof_id_{$prof_data['PROFID']}">
                        <strong>{$prof_data['COMPANYTITLE']}</strong>, {$prof_data['COMPANYNAME']}, {$prof_data['COMPANYCITY']}, {$prof_data['COMPANYSTCD']}<br/>
                        {$skill_title}
                        </p>
DDD;
                            }
                    } else { ?>
                        <p>No Career data.</p>
                    <?php } ?>

                </div>

                <h3>Education</h3>

	            <?php
	            $all_educs = SPP_ARCHON_DB::get_all_archousa_edu($user_id);
	            ?>
                <div id="profile_educ_box">

		            <?php if($all_educs['count'] > 0) {
			            $edus_data = $all_educs['data'];
			            foreach ( $edus_data as $edu_data ) {
				            $edu_minor_data = empty( $edu_data['MINOR1'] ) ? '' : "<span class=\"font-small\">{$edu_data['MINOR1']}</span><br/>";
				            echo <<<DDD
				            <p id="educ_id_{$edu_data['DEGREEID']}">
                            <strong>{$edu_data['INSTITUTION']}</strong>, {$edu_data['DEGREEYEAR']}<br/>
                            {$edu_data['DEGREE']}, {$edu_data['MAJOR']}<br/>
                            {$edu_minor_data}
                        </p>                        
DDD;
			            }
		            } else { ?>
                        <p>No Education data.</p>
		            <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <!-- #begin THIRD COLUMN -->
    <div class="span-4">
        <div class="padding">
            <div class="width padding" id="archon_bio">

                <h3>Biography</h3>


                <div id="bio_text"><?php
	                echo nl2br($archousa_info['BIO']);
	                ?>
                    </p></div>


                <!-- #END THIRD COLUMN -->
            </div>
        </div>


    </div>
</div>

<?php
} else {
    echo "<p>Wrong ID! Please search again.</p>";
}
?>

</section>

    </div>