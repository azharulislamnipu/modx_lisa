<?php
		/* ------------------------------------------------------------
		:: Snippet: CALx
		---------------------------------------------------------------
		
		---------------------------------------------------------------
		:: Archivo de la idioma  : ESPA�OL
		---------------------------------------------------------------
		
		---------------------------------------------------------------
		:: Translate by : islander
		--------------------------------------------------------------*/
		
		// Name of the month
  	$month[0] = "enero" ;
  	$month[1] = "febrero" ;
  	$month[2] = "marzo" ;
  	$month[3] = "abril" ;
  	$month[4] = "mayo" ;
  	$month[5] = "junio" ;
  	$month[6] = "julio" ;
  	$month[7] = "agosto" ;
  	$month[8] = "septiembre" ;
  	$month[9] = "octubre" ;
  	$month[10] = "noviembre" ;
  	$month[11] = "diciembre" ;

  	// First letter of the month
  	$day[0] = "lun" ;
  	$day[1] = "mar" ;
  	$day[2] = "mie" ;
  	$day[3] = "jue" ;
  	$day[4] = "vie" ;
  	$day[5] = "sab" ;
  	$day[6] = "dom" ;

   	// Noms des jours de la semaine en entier
  	$dayName[0] = "lunes" ;
  	$dayName[1] = "martes" ;
  	$dayName[2] = "mi�rcoles" ;
  	$dayName[3] = "jueves" ;
  	$dayName[4] = "viernes" ;
  	$dayName[5] = "s�bado" ;
  	$dayName[6] = "domingo" ;
		   	
  	// Message d'erreur en cas de date invalide
  	$error01 = "Error : fecha invalida";
  	
  	// Message d'erreur lorsque le calendrier ne peut pas �tre cr�er
  	$msgCannotCreateCALx = "No puedes hacer calendario";
  	
  	// Message pour les info bulles : overlib
  	$closeText="Cerrar";
		$titleText="Informaci�n";
		$startStringUniqEvent = "Hay ";
		$endStringUniqEvent = " eventos planeado para esta fecha.";
  	$stringDetails = "Pinchar aqu� para mas detalles";
  	// Configuration des informations de la page de liens vers le multievent
  	$pagetitle="Listado de eventos";
  	$longtitle="Listado de eventos";
  	$description="Listado de eventos";
  	$introtext="Listado de eventos";
  	$eventOf = "Eventos de ";
  	$noEvent = "No hay eventos en esta fecha.";
  	$noEventMonth = "No hay eventos en este mes.";
		  	
  	// Message pour les info bulles des liens de changements de mois
  	$previousYear="Ense�ar el ultimo a�o";
  	$previousMonth="Ense�ar el ultimo mes";
  	$nextMonth="Ense�ar el proximo mes";
  	$nextYear="Ense�ar el proximo a�o";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowAYear="Indicar el a�o completo";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowMonthEvents="Ense�ar todos los eventos de este mes.";
?>