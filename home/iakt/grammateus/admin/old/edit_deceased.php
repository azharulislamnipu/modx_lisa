<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("DECEASEDDATE", true, "date", "date", "", "", "You must enter a deceased date.");
$tNGs->prepareValidation($formValidation);
// End trigger

// begin Recordset
$colname__rsArchonInfo = '-1';
if (isset($_GET['CUSTOMERID'])) {
  $colname__rsArchonInfo = $_GET['CUSTOMERID'];
}
$query_rsArchonInfo = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname__rsArchonInfo, "int"));
$rsArchonInfo = $sigma_modx->SelectLimit($query_rsArchonInfo) or die($sigma_modx->ErrorMsg());
$totalRows_rsArchonInfo = $rsArchonInfo->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_archons = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_archons);
// Register triggers
$ins_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "../../includes/nxt/back.php");
// Add columns
$ins_spp_archons->setTable("spp_archons");
$ins_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_archons = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "../../includes/nxt/back.php");
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("DECEASEDDATE", "DATE_TYPE", "POST", "DECEASEDDATE");
$upd_spp_archons->addColumn("STATUSSTT", "STRING_TYPE", "POST", "STATUSSTT");
$upd_spp_archons->addColumn("GRAMM", "STRING_TYPE", "CURRVAL", "");
$upd_spp_archons->addColumn("GRAMMUPDATE", "DATE_TYPE", "CURRVAL", "");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "GET", "CUSTOMERID");

// Make an instance of the transaction object
$del_spp_archons = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_archons);
// Register triggers
$del_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "../../includes/nxt/back.php");
// Add columns
$del_spp_archons->setTable("spp_archons");
$del_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "GET", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$totalRows_rsspp_archons = $rsspp_archons->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wdg="http://www.interaktonline.com/MXWidgets" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<style type="text/css">
<!--
.style2 {font-size: 16px}
-->
</style>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" /><!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    Mark Archon Deceased </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_archons->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_archons > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <?php 
// Show IF Conditional show_DECEASEDDATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th">&nbsp;</td>
                <td><span class="style2"><?php echo $rsArchonInfo->Fields('L2R_FULLNAME'); ?></span></td>
              </tr>
            <tr>
              <td class="KT_th"><label for="DECEASEDDATE_<?php echo $cnt1; ?>">DECEASED DATE:</label></td>
              <td><input name="DECEASEDDATE_<?php echo $cnt1; ?>" id="DECEASEDDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_archons->Fields('DECEASEDDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("DECEASEDDATE");?> <?php echo $tNGs->displayFieldError("spp_archons", "DECEASEDDATE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_DECEASEDDATE_on_update_only
?>
          <?php 
// Show IF Conditional show_GRAMM_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">GRAMMATEUS:</td>
              <td><?php echo KT_escapeAttribute($rsspp_archons->Fields('GRAMM')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_GRAMM_on_update_only
?>
          <?php 
// Show IF Conditional show_GRAMMUPDATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">DATE UPDATED:</td>
              <td><?php echo KT_formatDate($rsspp_archons->Fields('GRAMMUPDATE')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_GRAMMUPDATE_on_update_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_archons_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('kt_pk_spp_archons')); ?>" />
        <input type="hidden" name="STATUSSTT_<?php echo $cnt1; ?>" id="STATUSSTT_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('STATUSSTT')); ?>" />
        <?php
    $rsspp_archons->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['CUSTOMERID'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchonInfo->Close();
?>
