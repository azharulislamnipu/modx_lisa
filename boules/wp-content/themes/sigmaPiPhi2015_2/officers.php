<?php
/*
Template Name: Officers
*/
?>

<?php get_header(); ?>

	<div id="content">
		<div class="padder width padding-top-none-20">
<div class="width padding-top-none-20 divider">
		<?php do_action( 'sigma_before_blog_links' ); ?>
		
		<div class="row grid-12">

		<div class="page" id="blog-latest" role="main">
			
	   	 <div class="span-9 float-right">
	   		 <div class="inner">
				 
	 		    <?php 
	 			// Check that Archon plugin functions have been loaded
	 				if(function_exists('is_multisite_primary') && function_exists('is_regional_site')){ ?>

		    <?php if( !is_multisite_primary() && !is_regional_site() ) :

			//	the_widget('RegionalBoules_Widget', array('show_regional' => 'no'), array('before_title' => '<h3 class="widgettitle">', 'after_title' => '</h3>'));

		    	$chapter_name = get_bloginfo('name'); ?>
		    	<!-- <div class="widget"> -->
			        <h2 style="clear: both"><?php echo $chapter_name . ' ' . __('Officers', 'archon-network');?></h2>
			        <div class="contentBlock">
			            <?php get_boule_leadership( $chapter_name ) ?>
			        </div>
		        <!-- </div> -->
		    <?php endif; ?>
			<?php } ?>

			</div><!--inner-->
		</div><!--span-9-->

			 <div class="span-3 float-left">
				 <div class="inner">
					 <?php get_sidebar(); ?>
				</div><!-- .inner -->
			</div><!-- .span-3 -->
	
		</div><!-- .page -->

			</div><!-- .grid-12 -->

		<?php do_action( 'sigma_after_blog_links' ); ?>

		</div>
	</div>
	
    <!-- 3 round images -->
    <div class="row grid-12 margin-top-medium padding">
			<div class="width padding">
		<div class="round">
		
	<?php if ( is_active_sidebar( 'three_round_images' ) ) : ?>
		
		<?php dynamic_sidebar( 'three_round_images' ); ?>
		 
		<?php endif; ?>
		</div><!-- #width padding -->
	</div><!-- #round -->
		</div><!-- #row grid 12 round images  -->

    </div><!-- #content -->
	
	

<?php get_footer(); ?>
