<?php

/*
 * Custom template functions
 */

define('IS_DEV', strpos($_SERVER['HTTP_HOST'], 'dev3') !== false);

/**
 * Group the Boule's by their region
 */
function group_sites_by_region( $region = '' ) {

	$sites_by_region = get_transient('archon_network_sites_by_region_' . $region);

	if (false === $sites_by_region) {

		$sites_by_region = array();

		// Get all the visible sites.  The "sigma_get_multisites" function is defined by the Archon Network
		// plugin and should always be available, but hey, I'm a cyncial kinda guy and this might help the
		// future version of me if the the Sigma site lists suddenly stop populating.
		$sites = function_exists( 'sigma_get_multisites' ) ? sigma_get_multisites( 1, false, false ) : array();
		if (!empty($sites)) {

			// Get the link between each region and Boule
			$modx = get_modx_database();

			//$modx = new wpdb($username_sigma_modx, $password_sigma_modx, $database_sigma_modx, $hostname_sigma_modx);
			$query = !empty($region) ?
				$modx->prepare("select BOULENAME, REGIONNAME, CITY, STATECD from spp_boule WHERE REGIONNAME = %s ORDER BY ORGCD", $region) :
				"select BOULENAME, REGIONNAME, CITY, STATECD from spp_boule ORDER BY REGIONNAME, ORGCD";

			$boules = $modx->get_results($query);
			if (!empty($boules)) {
				foreach ($boules as $boule) {

					// Do we have a matching WP Boule site?
					if (isset($sites[$boule->BOULENAME])) {

						// Check if the Boule's region needs to be added
						if (!isset($sites_by_region[$boule->REGIONNAME])) {
							$sites_by_region[$boule->REGIONNAME] = array();
						}

						// Store the WP site object by its region
						$sites[$boule->BOULENAME]->city = $boule->CITY;
						$sites[$boule->BOULENAME]->statecd = $boule->STATECD;
						$sites_by_region[$boule->REGIONNAME][$boule->BOULENAME] = $sites[$boule->BOULENAME];
					}
				}
			}
			if (!empty($sites_by_region)) {
				set_transient('archon_network_sites_by_region_' . $region, $sites_by_region, (60 * 60));
			}
		}
	}
	return $sites_by_region;
}

/**
 * Clears the cached site lists when a new blog is created
 */
function clear_cache ( $blog_id = '' ) {
	delete_transient('archon_network_sites');
	delete_transient('archon_network_sites_by_region');
}
add_action('wpmu_new_blog', 'clear_cache');
add_action('wpmueditblogaction', 'clear_cache');


/*
 * Sorts blogs by name in alphabetical ascending order
 */
function sigma_sort_by_blogname( $a, $b ){
    return strcmp($a->blogname, $b->blogname);
}

/*
 * Sorts blogs by ORGCD in alphabetical ascending order
 */
function sigma_sort_by_orgcd( $a, $b ){
    return ($a->ORGCD > $b->ORGCD ? 1 : ($a->ORGCD < $b->ORGCD ? -1 : 0));
}

/*
 * Gets a page's content given a page slug
 */
function get_page_content_by_slug( $slug ) {

    $content = '<p>'.__('No '. str_replace('-', ' ', $slug) .' found.', 'buddypress').'</p>';
    query_posts( 'post_type=page&orderby=date&order=DESC$posts_per_page=1&pagename=' . $slug );

	// Found something
    if ( have_posts() ) {
    	the_post();

    	// Can the user view it?
		if ( can_view_content() ) {
        	$content = get_the_content();
		} else {
			$content = '<p>' . __('The information on this page is available for members only. Please log in to view this content.', 'archon-network') . '</p>';
		}
	}
    wp_reset_query();
    echo $content;
}

/*
 * Returns a set of content
 */
function get_content( $post_type, $cat, $posts_per_page, $posts_exclude = array() ){

	// Determine what content this archon is allowed to view
	$can_view_all_archons = can_view_all_archons();
	$post_status = 'publish' . ( $can_view_all_archons ? ',private' : '' );

	// Everyone can view posts with _visible_to meta set
	$meta_query =
		array (
			array(
				'key' => '_visible_to',
				'value' => 'foo',
				'compare' => 'NOT EXISTS'
			)
		);

	// If this is a logged in Archon, they can also view "All Archons" content and
	// potentially "Boule only" as well.
	if ( $can_view_all_archons ) {
		$meta_query[ 'relation' ] = 'OR';
		$meta_query[] =
			array(
				'key' => '_visible_to',
				'value' => can_view_boule_only() ? array( 'archons', 'boule' ) : array ( 'archons' ),
				'compare' => 'IN'
			);
	}

	return
		new WP_Query(
			array(
				'post_type' => $post_type,
				'post_status' => $post_status,
				'cat' => $cat,
				'posts_per_page' => $posts_per_page,
				'post__not_in' => $posts_exclude,
				'meta_query' => $meta_query
			)
		);
}


/**
 * Determines if a given page has content
 * @param Mixed $page_id_or_slug Page numeric ID or slug
 * @return boolean
 */
function is_page_content( $page_id_or_slug ){
    $is_content = false;

    $page = is_numeric($page_id_or_slug) ? get_page($page_id_or_slug) : get_page_by_path($page_id_or_slug);
    if($page !== null){
        $is_content = strlen($page->post_content) > 0;
    }
    return $is_content;
}


/**
 * Determines if a category has content based on its slug
 * @param Mixed $slug $cat_id_or_slug Category numeric ID or slug
 * @return boolean
 */
function is_category_content( $cat_id_or_slug ){
    global $wpdb;

    $is_content = false;
	$can_view_all_archons = can_view_all_archons();

    $cat = is_numeric($cat_id_or_slug) ? get_category($cat_id_or_slug) : get_category_by_slug($cat_id_or_slug);
    if(!empty($cat)){

		// Must be an archon to view private content
		$poststatus = "'publish'";
		if ( $can_view_all_archons ) {
			$poststatus .= ",'private'";
		}

		// Must be a member of the given Boule to view private, Boule only content
		$postmeta_OR = "";
		if ( $can_view_all_archons ) {
			$postmeta_OR = "OR $wpdb->postmeta.meta_value IN ('archons'" . ( can_view_boule_only() ? ", 'boule'" : "" ) . ")";
		}

		$query = "
			SELECT COUNT(*) FROM
			$wpdb->posts

			JOIN $wpdb->term_relationships
			ON $wpdb->posts.ID = $wpdb->term_relationships.object_id

			JOIN $wpdb->term_taxonomy
			ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id

			LEFT JOIN $wpdb->postmeta
			ON $wpdb->posts.ID = $wpdb->postmeta.post_id
			AND $wpdb->postmeta.meta_key = '_visible_to'

			WHERE
			$wpdb->term_taxonomy.term_id = $cat->cat_ID
			AND $wpdb->posts.post_status IN ($poststatus)
			AND (
				$wpdb->postmeta.meta_key IS NULL
				$postmeta_OR
			)
			LIMIT 1";

        $count = $wpdb->get_var($query);
        $is_content = $count > 0;
    }
    return $is_content;
}


/*
 * Retrieves a set of content by a given category slug
 */
function get_content_by_category( $cat, $post_type = array('page','post'), $count = 5, $show_excerpt = true ){

	$is_content = false;
	if(!empty($cat)){

		// Gets the content
		$content = get_content($post_type, $cat->cat_ID, $count);
		if($content->have_posts()){

			$is_content = true;
			$currPage = get_query_var('paged'); ?>

			<ul class="<?php echo $currPage?> link-list">
			<?php while($content->have_posts()) : $content->the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>">
						<strong><?php the_title(); ?></strong>
						<?php if($show_excerpt):?><span class="exceprt"><?php the_excerpt(); ?></span><?php endif;?>
					</a>
				</li>
			<?php endwhile; ?>
			</ul> <?php

			wp_reset_query();
		}
	}

	// Issue an error if no content returned
	if(!$is_content):
		if(!empty($cat)):?>
			<p>No <?php echo strtolower($cat->name) ?> found.</p>
		<?php else: ?>
			<p>No content found.</p>
		<?php endif;
	endif;
}


/*
 * Gets a list of a page's children's excerpts given a page ID or slug
 */
function get_page_children_excerpts( $page_id_or_slug, $count = 5, $show_excerpt = true, $sort = 'DESC' ){

    $excerpts = '';

    if($page_id_or_slug){
        $parentId = is_numeric($page_id_or_slug) ? $page_id_or_slug : get_page_id($page_id_or_slug);
        if($parentId){
            $currPage = get_query_var( 'paged' );
            query_posts('post_type=page&orderby=date&order=' . $sort . '$posts_per_page=' . $count . '&post_parent=' . $parentId . ($currPage != 0 ? '&paged=' . $currPage  : ''));
            if (have_posts()) {
                echo "<ul class='".$currPage." link-list'>\n";
                while (have_posts()) : the_post();
                	if ( can_view_content() ): ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <strong><?php the_title(); ?></strong>
                            <?php if($show_excerpt):?><span class="exceprt"><?php the_excerpt(); ?></span><?php endif;?>
                        </a>
                    </li>
                    <?php endif;
                endwhile;
                echo "</ul>\n";
            } else {
                $page_title = get_the_title($parentId);
                $excerpts = '<p>'.__('No '. ($page_title ? strtolower($page_title) : 'entries') .' found.', 'buddypress').'</p>';
            }
        }
    }
    echo $excerpts;
}


/*
 * Gets the first image from given content
 */
function get_first_image(){
    global $post;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/(<img.+src=[\'"][^\'"]+[\'"].*>)/i', $post->post_content, $matches);
    $first_img = $matches[1][0];

    return !empty($first_img) ? $first_img : '';
}


/*
 * Retrieves a list of the Boul� chapters leadership
 */
function get_boule_leadership( $chapter_name ){

    // Pull the officers from the MODx tables
    $modxdb = get_modx_database();
    $modxdb->suppress_errors(false);
    $modxdb->show_errors(true);

    $officers = $modxdb->get_results(
        "SELECT vw_boule_officers.L2R_FULLNAME, vw_boule_officers.WEB_ID, vw_boule_officers.CPOSITION, vw_boule_officers.CPOSITIONID FROM vw_boule_officers WHERE vw_boule_officers.COMMITTEESTATUSSTT = 'Active' AND vw_boule_officers.BOULENAME = '" . $chapter_name . "' ORDER BY  vw_boule_officers.CPOSITIONID"
    );

    // Loop through and display the officers for the given Boul�
    if($officers) :
		$isLoggedIn = is_user_logged_in();
        echo "<ul id='chapter-officers'>\n";
        foreach($officers as $officer){
        	if ( $isLoggedIn ) {
        		echo '<li><strong>' . $officer->CPOSITION . '</strong><br><a href="/services/directory/results.php?id=' . $officer->WEB_ID . '">' . $officer->L2R_FULLNAME . '</a></li>'."\n";
        	} else {
        		echo '<li><strong>' . $officer->CPOSITION . '</strong><br>' . $officer->L2R_FULLNAME . '</li>'."\n";
        	}
        }
        echo "</ul>\n";
    else :
        echo '<p>No chapter officers found.</p>';
    endif;
}

/**
 * Outputs featured image posts and returns the IDs of the posts displayed
 */
function get_featured_image_posts( $limit = 1 ) {


	$count = 0;
	// $cat_id = get_cat_ID('exclude');
	$featuredImgs = new WP_Query(array('posts_per_page' => -1, 'meta_key' => '_thumbnail_id'));
	while ( $featuredImgs->have_posts() ) :
		$posts_displayed[] = $post->ID;
		$featuredImgs->the_post(); ?>

		
			<div class="featured span-12 float-left">
			<a href="<?php the_permalink(); ?>">
				<div class="inner">
					<div class="image">
				<?php the_post_thumbnail('blog-large'); ?>
			</div>
			<!-- <a href="<?php //the_permalink(); ?>" title="<?php //_e( 'View post', 'archon-network' ); ?>"></a> -->
			<div class="text">
				<h2><?php the_title(); ?></h2>
				<span class="updated"><?php //echo date('F j, Y', strtotime($featuredImgs->post_modified_gmt)) ?><?php the_time('F j, Y'); ?></span>
				<span class="blogname"><?php bloginfo('name'); ?></span>
				<!-- <p><?php //the_excerpt(); ?></p> -->
				<!-- <span class="link"><?php //_e( 'View', 'archon-network' ); ?></span> -->
			</div>
			<?php if ($count++ === 0): ?><span class="corner"></span><?php endif; ?>
		
		</div></a></div>

		<?php if ($count++ >= $limit) {
			break;
		}

	endwhile;
	wp_reset_postdata();

	return $posts_displayed;
}

function get_featured_image_posts_secondary( $limit = 4 ) {


	$count = 0;
	$cat_id = get_cat_ID('exclude');
	$featuredImgs = new WP_Query(array('meta_key' => '_thumbnail_id', 'cat' => '-14', 'offset' => 1));
	while ( $featuredImgs->have_posts() ) :
		$posts_displayed[] = $post->ID;
		$featuredImgs->the_post(); ?>

		<div class="featured span-6 float-left">
			<a href="<?php the_permalink(); ?>">
				<div class="inner">
					<div class="image">
					<?php the_post_thumbnail('blog-med'); ?>
				</div>
			<!-- <a href="<?php //the_permalink(); ?>" title="<?php //_e( 'View post', 'archon-network' ); ?>"></a> -->
			<div class="text">
				<h2><?php the_title(); ?></h2>
				<span class="updated"><?php //echo date('F j, Y', strtotime($featuredImgs->post_modified_gmt)) ?><?php the_time('F j, Y'); ?></span>
				<span class="blogname"><?php bloginfo('name'); ?></span>
				<!-- <p><?php //the_excerpt(); ?></p> -->
				<!-- <span class="link"><?php //_e( 'View', 'archon-network' ); ?></span> -->
			</div>
			<!-- <?php //if ($count++ === 0): ?><span class="corner"></span><?php //endif; ?> -->
		</div></a></div>

		<?php if ($count++ >= $limit) {
			break;
		}

	endwhile;
	wp_reset_postdata();

	return $posts_displayed;
}

/**
 * Creates pagination links
 */
function paginate() {
	global $wp_query, $wp_rewrite;
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

	$pagination = array(
		'base' => @add_query_arg('page','%#%'),
		'format' => '',
		'total' => $wp_query->max_num_pages,
		'current' => $current,
		'show_all' => true,
		'type' => 'list',
		'prev_text'    => __('Previous'),
		'next_text'    => __('Next'),
		);

	if( $wp_rewrite->using_permalinks() )
		$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

	if( !empty($wp_query->query_vars['s']) )
		$pagination['add_args'] = array( 's' => get_query_var( 's' ) );

	echo paginate_links( $pagination );
}

/** Checks if a given post has a gallery */
function has_gallery( $post_id = false ) {
    if ( !$post_id ) {
        global $post;
    } else {
        $post = get_post($post_id);
    }
    return strpos( $post->post_content,'[gallery') !== false;
}

/**
 * Returns a list of gallery links
 * [galleries page="slugs"] */
function galleries_shortcode( $atts ) {
	global $post;

	extract( shortcode_atts( array(
		'page' => $post->post_name
	), $atts ) );

	return $page;
}
add_shortcode( 'galleries', 'galleries_shortcode' );

if ( function_exists('register_sidebar') )
register_sidebar(array(
		'name'=> 'Three Round Images',
		'id' => 'three_round_images',
		//'before_widget' => '<section class="span-4 align-center"><div class="inner"><div class="rounded">',
		//'after_widget' => '</div></div></section>',
	));
	
	register_sidebar(array(
			'name'=> 'Home Page Large Content Block ("History")',
			'id' => 'home_large_block',
		));
		
		register_sidebar(array(
				'name'=> 'Events & Highlighted: Gray block area',
				'id' => 'event_block',
				'before_widget' => '<section class="span-4 align-left"><div class="inner"><div class="rounded">',
				'after_widget' => '</div></div></section>',
			));
			register_sidebar(array(
					'name'=> 'Footer Block',
					'id' => 'footer_block',
				));
				
				register_sidebar(array(
						'name'=> 'Recent Updates: Regional Newsletter Page',
						'id' => 'newsletter_block',
					));
					
					register_sidebar(array(
							'name'=> 'Home Page Right Side Block',
							'id' => 'home_right_block',
						));
					
		function wpse_allowedtags() {
		    // Add custom tags to this string
		        return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>'; 
		    }

		if ( ! function_exists( 'wpse_custom_wp_trim_excerpt' ) ) : 

		    function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
		    global $post;
		    $raw_excerpt = $wpse_excerpt;
		        if ( '' == $wpse_excerpt ) {

		            $wpse_excerpt = get_the_content('');
		            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
		            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
		            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
		            $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

		            //Set the excerpt word count and only break after sentence is complete.
		                $excerpt_word_count = 200;
		                $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
		                $tokens = array();
		                $excerptOutput = '';
		                $count = 0;

		                // Divide the string into tokens; HTML tags, or words, followed by any whitespace
		                preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

		                foreach ($tokens[0] as $token) { 

		                    if ($count >= $excerpt_word_count && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
		                    // Limit reached, continue until , ; ? . or ! occur at the end
		                        $excerptOutput .= trim($token);
		                        break;
		                    }

		                    // Add words to complete sentence
		                    $count++;

		                    // Append what's left of the token
		                    $excerptOutput .= $token;
		                }

		            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

		               // $excerpt_end = ' <a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf(__( 'Read more about: %s &nbsp;&raquo;', 'wpse' ), get_the_title()) . '</a>'; 
		                $excerpt_more = apply_filters('excerpt_more', ' '); 

		                //$pos = strrpos($wpse_excerpt, '</');
		                //if ($pos !== false)
		                // Inside last HTML tag
		                //$wpse_excerpt = substr_replace($wpse_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word */
		                //else
		                // After the content
		               // $wpse_excerpt .= $excerpt_end; /*Add read more in new paragraph */

		            return $wpse_excerpt;   

		        }
		        return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
		    }

		endif; 

		remove_filter('get_the_excerpt', 'wp_trim_excerpt');
		add_filter('get_the_excerpt', 'wpse_custom_wp_trim_excerpt'); 
		
		function wpse_custom_excerpts($limit) {
		    //return wp_trim_words(get_the_excerpt(), $limit, '<a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&hellip;' . __( 'Read more &nbsp;&raquo;', 'wpse' ) . '</a>');
			return wp_trim_words(get_the_excerpt(), $limit);
		}
		
		function link_to_stylesheet() {
		?>
		<style type="text/css">
		#wpadminbar *{font-family: Raleway, arial, sans-serif; font-size: 14px;}

	/*	#wpadminbar ul li:before,#wpadminbar ul li:after{
		content:normal;}

		#wpadminbar a,#wpadminbar a:hover,#wpadminbar a img,#wpadminbar a img:hover{
		outline:0;:0;text-decoration:none;background:0}

		#wpadminbar{
		direction:ltr;height:40px;position:fixed;top:0;left:0;width:100%;min-width:600px;z-index:99999;background-image:url(../../wp-content/themes/buddyboss-child/gfx/nav_bg.png)}

		#wpadminbar .ab-sub-wrapper,#wpadminbar ul,#wpadminbar ul li{
		background:0;clear:none;list-style:none;margin:0;padding:0;position:relative;z-index:99999}

		#wpadminbar .quicklinks{
		height:37px;
		border-left:1px solid transparent}

		#wpadminbar .quicklinks ul{
		text-align:left}

		#wpadminbar li{
		float:left}

		#wpadminbar .ab-empty-item{
		outline:0}

		#wpadminbar .quicklinks>ul>li{
		height:37px;
		border-right:1px solid #E7E7E7}

		#wpadminbar .quicklinks>ul>li>a,#wpadminbar .quicklinks>ul>li>.ab-empty-item{
		height:37px;
		border-right:1px solid #E7E7E}

		#wpadminbar .quicklinks .ab-top-secondary>li{
		height:37px;
		border-left:1px solid #E7E7E7;border-right:0;float:right}*/
		<!-- Border right site-->
		/*#wpadminbar .quicklinks .ab-top-secondary>li>a,#wpadminbar .quicklinks .ab-top-secondary>li>.ab-empty-item{
		height:37px;
		border-left:1px solid #E7E7E;border-right:0}

		#wpadminbar .quicklinks a,#wpadminbar .quicklinks .ab-empty-item,#wpadminbar .shortlink-input{
		height:37px;display:block;padding:0 12px;margin:0}*/

/*		#wpadminbar .menupop .ab-sub-wrapper,#wpadminbar .shortlink-input{
		margin:0 0 0 -1px;padding:0;-moz-box-shadow:0 4px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 4px 4px rgba(0,0,0,0.2);box-shadow:0 4px 4px rgba(0,0,0,0.2);background:#fff;display:none;position:absolute;float:none;border-width:0 1px 1px 1px;border-style:solid;border-color:#dfdfdf}*/

	/*	#wpadminbar.ie7 .menupop .ab-sub-wrapper,#wpadminbar.ie7 .shortlink-input{
		top:40px;left:0}

		#wpadminbar .ab-top-menu>.menupop>.ab-sub-wrapper{
		min-width:100%}

		#wpadminbar .ab-top-secondary .menupop .ab-sub-wrapper{
		right:0;left:auto;margin:0 -1px 0 0}

		#wpadminbar .ab-sub-wrapper>.ab-submenu:first-child{
		border-top:0}

		#wpadminbar .ab-submenu{
		padding:6px 0;border-top:1px solid #dfdfdf}

		#wpadminbar .selected .shortlink-input{
		display:block}

		#wpadminbar .quicklinks .menupop ul li{
		float:none}

		#wpadminbar .quicklinks .menupop ul li a strong{
		font-weight:bold}

		#wpadminbar .quicklinks .menupop ul li .ab-item,#wpadminbar .quicklinks .menupop ul li a strong,#wpadminbar .quicklinks .menupop.hover ul li .ab-item,#wpadminbar.nojs .quicklinks .menupop:hover ul li .ab-item,#wpadminbar .shortlink-input{
		line-height:26px;height:26px;text-shadow:none;white-space:nowrap;min-width:140px}

		#wpadminbar .shortlink-input{
		width:200px}

		#wpadminbar.nojs li:hover>.ab-sub-wrapper,#wpadminbar li.hover>.ab-sub-wrapper{
		display:block}

		#wpadminbar .menupop li:hover>.ab-sub-wrapper,#wpadminbar .menupop li.hover>.ab-sub-wrapper{
		margin-left:100%;margin-top:-33px;border-width:1px}

		#wpadminbar .ab-top-secondary .menupop li:hover>.ab-sub-wrapper,#wpadminbar .ab-top-secondary .menupop li.hover>.ab-sub-wrapper{
		margin-left:0;left:inherit;right:100%}*/

		<!-- Change the hover effect here -->
		
		#wpadminbar.nojs .ab-top-menu>li.menupop:hover>.ab-item,#wpadminbar .ab-top-menu>li.menupop.hover>.ab-item{
		background-color:#cccccc;}
		
		#wpadminbar .quicklinks .menupop ul li .ab-item:hover, #wpadminbar .quicklinks .menupop ul li a:hover strong, #wpadminbar .quicklinks .menupop.hover ul li .ab-item:hover , #wpadminbar .shortlink-input, #wpadminbar.nojs .quicklinks .menupop:hover ul li .ab-item a:hover {color: #000000;}

		<!--Text color in right side -->
		/*#wpadminbar .hover .ab-label,#wpadminbar.nojq .ab-item:focus .ab-label{color:#fafafa}*/

		#wpadminbar .menupop li:hover,#wpadminbar .menupop li.hover,#wpadminbar .quicklinks .menupop .ab-item:focus,#wpadminbar .quicklinks .ab-top-menu .menupop .ab-item:focus, #wpadminbar .quicklinks .ab-top-menu .menupop{background-color:#cccccc !important}

		#wpadminbar .quicklinks .menupop ul li a, #wpadminbar .quicklinks .menupop ul li a:hover,#wpadminbar .quicklinks .menupop ul li a strong,#wpadminbar .quicklinks .menupop.hover ul li a,#wpadminbar.nojs .quicklinks .menupop:hover ul li a{color:#6D6D6D}

	/*	#wpadminbar .quicklinks .menupop ul.ab-sub-secondary{display:block;position:relative;right:auto;margin:0;background:#eee;-moz-box-shadow:none;-webkit-box-shadow:none;box-shadow:none}
*/
		#wpadminbar .quicklinks .menupop .ab-sub-secondary>li:hover,#wpadminbar .quicklinks .menupop .ab-sub-secondary>li.hover,#wpadminbar .quicklinks .menupop .ab-sub-secondary>li .ab-item:focus{background-color:#cccccc; color:#6D6D6D}
		#wpadminbar {background: #20488a; position:absolute !important;}
		.logged-in #wpadminbar {    background: #cea02e !important;}
		/*#wpadminbar .quicklinks a span#ab-updates{background:#eee;color:#333;text-shadow:none;display:inline;padding:2px 5px;font-size:10px;font-weight:bold;-webkit-border-radius:10px;border-radius:10px}*/
		/*#wpadminbar .quicklinks a:hover span#ab-updates{background:#fff;color:#000}

		#wpadminbar .ab-top-secondary{float:right;text-shadow:none;}*/

	/*	#wpadminbar ul li:last-child,#wpadminbar ul li:last-child .ab-item{border-right:0;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none}
		#wp-admin-bar-my-account>ul{min-width:198px}#wp-admin-bar-my-account.with-avatar>ul{min-width:270px}
		#wpadminbar #wp-admin-bar-user-actions>li{margin-left:16px;margin-right:16px}
		#wpadminbar #wp-admin-bar-my-account.with-avatar #wp-admin-bar-user-actions>li{margin-left:88px}
		#wp-admin-bar-user-actions>li>.ab-item{padding-left:8px}
		#wpadminbar #wp-admin-bar-user-info{margin-top:6px;margin-bottom:15px;height:auto;background:0}
		#wp-admin-bar-user-info .avatar{position:absolute;left:-72px;top:4px;width:64px;height:64px}
		#wpadminbar #wp-admin-bar-user-info a{background:0;height:auto}#wpadminbar #wp-admin-bar-user-info span{background:0;padding:0;height:18px}
		#wpadminbar #wp-admin-bar-user-info .display-name,#wpadminbar #wp-admin-bar-user-info .username{text-shadow:none;display:block}
		#wpadminbar #wp-admin-bar-user-info .display-name{color:#333}#wpadminbar #wp-admin-bar-user-info .username{color:#999;font-size:11px}
		#wpadminbar .quicklinks li#wp-admin-bar-my-account.with-avatar>a img
		{width:30px;height:30px;padding:0;vertical-align:middle;margin:-7px 0 0 6px;float:none;display:inline; padding-top: 5px;}

*/
		#wpadminbar .menupop .ab-sub-wrapper, #wpadminbar .shortlink-input {box-shadow:none;}
		/*#wpadminbar {background: #E4E4E4;}*/
		#wpadminbar .ab-sub-wrapper, #wpadminbar .ab-sub-wrapper .ab-submenu li a, #wpadminbar .menupop .ab-sub-wrapper,#wpadminbar .shortlink-input {background: #20488a;color: #ffffff;}
		.logged-in #wpadminbar .ab-sub-wrapper, .logged-in #wpadminbar .ab-sub-wrapper .ab-submenu li a, .logged-in #wpadminbar .menupop .ab-sub-wrapper, .logged-in #wpadminbar .shortlink-input {/*background: #cea02e;*/}
		#wpadminbar .ab-top-menu>li.menupop.hover>.ab-item,#wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus,#wpadminbar.nojs .ab-top-menu>li.menupop:hover>.ab-item,#wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item,#wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus, #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-sub-wrapper, #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-submenu, #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-submenu li{text-decoration:underline; color: #ffffff;}
		
		 .logged-in #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item, .logged-in #wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus,.logged-in #wpadminbar.nojs .ab-top-menu>li.menupop:hover>.ab-item, .logged-in #wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item,.logged-in #wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus, .logged-in #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-sub-wrapper, .logged-in #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-submenu, .logged-in #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-submenu li {/*background: #cea02e;*/text-decoration:underline;}
		 #wpadminbar .ab-submenu { margin: 6px 0;padding:0;}
		 .logged-in #wpadminbar .quicklinks .menupop ul.ab-sub-secondary, .logged-in #wpadminbar .quicklinks .menupop ul.ab-sub-secondary .ab-submenu {/*background: #cea02e;*/}
		#wpadminbar .ab-top-menu>li.menupop.hover>.ab-item,#wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus,#wpadminbar.nojs .ab-top-menu>li.menupop:hover>.ab-item,#wpadminbar:not(.mobile) .ab-top-menu>li:hover>.ab-item,#wpadminbar:not(.mobile) .ab-top-menu>li>.ab-item:focus, #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-sub-wrapper, #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-submenu, #wpadminbar .ab-top-menu>li.menupop.hover>.ab-item .ab-submenu li {background: #20488a ;}
		
		#wpadminbar a:link, #wpadminbar a:visited, #wpadminbar a:hover, #wpadminbar a.ab-item, #wpadminbar>#wp-toolbar span.ab-label, #wpadminbar>#wp-toolbar span.noticon, #wpadminbar #wp-admin-bar-my-sites>.ab-item:before:hover, #wpadminbar #wp-admin-bar-site-name>.ab-item:before:hover {color: #ffffff !important;background: transparent !important;}
		#wpadminbar, #wpadminbar #adminbarsearch:before, #wpadminbar .ab-icon:before, #wpadminbar .ab-item:before, #wpadminbar, #wpadminbar #adminbarsearch:before {color: #ffffff;}
		#wpadminbar a.ab-item, #wpadminbar .ab-empty-item, #wpadminbar .ab-icon:hover:before, #wpadminbar .ab-item:hover:before, #wpadminbar a.ab-item:hover, #wpadminbar a.ab-item:hover:before, #wpadminbar .ab-item:visited:before, #wpadminbar a.ab-item:visited, #wpadminbar a.ab-item:visited:before {color: #ffffff !important;}
		
		#wpadminbar a:hover, #wpadminbar .quicklinks a:hover, #wpadminbar .ab-sub-wrapper .ab-submenu li a:hover, #wpadminbar .quicklinks .menupop ul li .ab-item:hover {text-decoration:underline !important;}
		#wp-admin-bar-member-boules .ab-sub-wrapper { overflow-y: auto; overflow-x: hidden; max-height: 20em; }
		</style>
		<?php
		}
		add_action('wp_head', 'link_to_stylesheet');
	
?>

<?php
/*
 * Alters event's archive titles
 */
function tribe_alter_event_archive_titles ( $original_recipe_title, $depth ) {
	// Modify the titles here
	// Some of these include %1$s and %2$s, these will be replaced with relevant dates
	$title_upcoming =   'Upcoming Events'; // List View: Upcoming events
	$title_past =       'Past Events'; // List view: Past events
	$title_range =      'Events for %1$s - %2$s'; // List view: range of dates being viewed
	$title_month =      '%1$s'; // Month View, %1$s = the name of the month
	$title_day =        'Events for %1$s'; // Day View, %1$s = the day
	$title_all =        'All events for %s'; // showing all recurrences of an event, %s = event title
	$title_week =       'Events for week of %s'; // Week view
	// Don't modify anything below this unless you know what it does
	global $wp_query;
	$tribe_ecp = Tribe__Events__Main::instance();
	$date_format = apply_filters( 'tribe_events_pro_page_title_date_format', tribe_get_date_format( true ) );
	// Default Title
	$title = $title_upcoming;
	// If there's a date selected in the tribe bar, show the date range of the currently showing events
	if ( isset( $_REQUEST['tribe-bar-date'] ) && $wp_query->have_posts() ) {
		if ( $wp_query->get( 'paged' ) > 1 ) {
			// if we're on page 1, show the selected tribe-bar-date as the first date in the range
			$first_event_date = tribe_get_start_date( $wp_query->posts[0], false );
		} else {
			//otherwise show the start date of the first event in the results
			$first_event_date = tribe_event_format_date( $_REQUEST['tribe-bar-date'], false );
		}
		$last_event_date = tribe_get_end_date( $wp_query->posts[ count( $wp_query->posts ) - 1 ], false );
		$title = sprintf( $title_range, $first_event_date, $last_event_date );
	} elseif ( tribe_is_past() ) {
		$title = $title_past;
	}
	// Month view title
	if ( tribe_is_month() ) {
		$title = sprintf(
			$title_month,
			date_i18n( tribe_get_option( 'monthAndYearFormat', 'F Y' ), strtotime( tribe_get_month_view_date() ) )
		);
	}
	// Day view title
	if ( tribe_is_day() ) {
		$title = sprintf(
			$title_day,
			date_i18n( tribe_get_date_format( true ), strtotime( $wp_query->get( 'start_date' ) ) )
		);
	}
	// All recurrences of an event
	if ( function_exists('tribe_is_showing_all') && tribe_is_showing_all() ) {
		$title = sprintf( $title_all, get_the_title() );
	}
	// Week view title
	if ( function_exists('tribe_is_week') && tribe_is_week() ) {
		$title = sprintf(
			$title_week,
			date_i18n( $date_format, strtotime( tribe_get_first_week_day( $wp_query->get( 'start_date' ) ) ) )
		);
	}
	if ( is_tax( $tribe_ecp->get_event_taxonomy() ) && $depth ) {
		$cat = get_queried_object();
		$title = '<a href="' . esc_url( tribe_get_events_link() ) . '">' . $title . '</a>';
		$title .= ' &#8250; ' . $cat->name;
	}
	return $title;
}
add_filter( 'tribe_get_events_title', 'tribe_alter_event_archive_titles', 11, 2 );

add_filter( 'wp_nav_menu_objects', 'submenu_limit', 10, 2 );

function submenu_limit( $items, $args ) {

    if ( empty($args->submenu) )
        return $items;

    $parent_id = array_pop( wp_filter_object_list( $items, array( 'title' => $args->submenu ), 'and', 'ID' ) );
    $children  = submenu_get_children_ids( $parent_id, $items );

    foreach ( $items as $key => $item ) {

        if ( ! in_array( $item->ID, $children ) )
            unset($items[$key]);
    }

    return $items;
}

function submenu_get_children_ids( $id, $items ) {

    $ids = wp_filter_object_list( $items, array( 'menu_item_parent' => $id ), 'and', 'ID' );

    foreach ( $ids as $id ) {

        $ids = array_merge( $ids, submenu_get_children_ids( $id, $items ) );
    }

    return $ids;
}

function widget($atts) {
    
    global $wp_widget_factory;
    
    extract(shortcode_atts(array(
        'widget_name' => FALSE
    ), $atts));
    
    $widget_name = wp_specialchars($widget_name);
    
    if (!is_a($wp_widget_factory->widgets[$widget_name], 'WP_Widget')):
        $wp_class = 'WP_Widget_'.ucwords(strtolower($class));
        
        if (!is_a($wp_widget_factory->widgets[$wp_class], 'WP_Widget')):
            return '<p>'.sprintf(__("%s: Widget class not found. Make sure this widget exists and the class name is correct"),'<strong>'.$class.'</strong>').'</p>';
        else:
            $class = $wp_class;
        endif;
    endif;
    
    ob_start();
    the_widget($widget_name, $instance, array('widget_id'=>'arbitrary-instance-'.$id,
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
    
}
add_shortcode('widget','widget'); 

add_image_size( 'blog-large', 760, 450, true ); // Hard Crop Mode
add_image_size( 'blog-med', 350, 220, true ); // Hard Crop Mode

//Custom Theme Settings
add_action('admin_menu', 'add_gcf_interface');

function add_gcf_interface() {
	add_options_page('Global Custom Fields', 'Global Custom Fields', '8', 'functions', 'editglobalcustomfields');
}

function editglobalcustomfields() {
	?>
	<div class='wrap'>
	<h2>Global Custom Fields</h2>
	<form method="post" action="options.php">
	<?php wp_nonce_field('update-options') ?>

	<p><strong>Chapter city and state:</strong><br />
	<input type="text" name="location" size="45" value="<?php echo get_option('location'); ?>" /></p>
	
	<p><strong>Chapter logo url:</strong><br />
		Upload logo to media library, then copy url and enter here. </br>
	<input type="text" name="logo_url" size="45" value="<?php echo get_option('logo_url'); ?>" /></p>

	<p><input type="submit" name="Submit" value="Update Options" /></p>

	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="page_options" value="location,logo_url" />

	</form>
	</div>
	<?php
}

?>

