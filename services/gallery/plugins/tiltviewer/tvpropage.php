<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Customize page
 *
 * @package svManager
 */
class TvProPage extends Page
{
 /**
  * @var string overrides some of head content in Page
  */
  var $customHeadHtml = '
       <link href="colorjack/plugin.css" rel="stylesheet" type="text/css" />
       <style type="text/css">
         #plugin {
           color: #FFFFFF;
           background-color: #999999;
         }
         #H {
           border-color: #999999;
         }
         #plugCLOSE {
           color: #FFFFFF;
           background-color: #999999;
         }
       </style>
       <script src="scripts/mootools-core.js" type="text/javascript"></script>
       <script src="scripts/mootools-more.js" type="text/javascript"></script>
       <script src="colorjack/plugin.js" type="text/javaScript"></script>
       <script src="scripts/customize.js" type="text/javascript"></script>
       <script src="scripts/mooformcheck.js" type="text/javascript"></script>
';
 
 /**
  * constructs TvCustomizePage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function TvProPage()
  {
    parent::Page('svManager &ndash; TiltViewer Pro', 'tvpro', 'navpro tiltviewer customize');
  }

 /**
  * get html for customize page
  *
  * Note hack in conditional comments from
  * @link http://www.hedgerwow.com/360/bugs/css-select-free.html
  * to prevent select elements showing above the colorpicker in IE6Win
  * @access public
  * @return string html
  * @param object gallery set
  * @param array gallery attributes
  */
  function getProHtml(&$gallerySet, &$gallery)
  {
    $preferences = $gallery->getPreferences();
    $galleriesPrefs = $gallerySet->getGalleriesPrefs();
    $textColor = strtoupper(substr($preferences['textColor'], 2, strlen($preferences['textColor']) - 2));
    $linkTextColor = strtoupper(substr($preferences['linkTextColor'], 2, strlen($preferences['linkTextColor']) - 2));
    $linkBkgndColor = strtoupper(substr($preferences['linkBkgndColor'], 2, strlen($preferences['linkBkgndColor']) - 2));
    $navButtonColor = strtoupper(substr($preferences['navButtonColor'], 2, strlen($preferences['navButtonColor']) - 2));
    $flipButtonColor = strtoupper(substr($preferences['flipButtonColor'], 2, strlen($preferences['flipButtonColor']) - 2));
    $showFlipButtonChecked = ($preferences['showFlipButton'] == true) ? 'checked="checked"' : '';
    $bkgndTransparentChecked = ($preferences['bkgndTransparent'] == true) ? 'checked="checked"' : '';
    $showFullscreenOptionChecked = ($preferences['showFullscreenOption'] == true) ? 'checked="checked"' : '';
    $enableSoundsChecked = ($preferences['enableSounds'] == 'true') ? 'checked="checked"' : '';
$html = <<<EOD

<div id="plugin" class="selectfree">
 <div id="plugHEX" onmousedown="stop=0; setTimeout('stop=1',100);">F1FFCC</div>
 <!-- Note change of function name from toggle to avoid namespace conflict -->
 <div id="plugCLOSE" onmousedown="toggleDisplay('plugin')">X</div><br />
 <div id="SV" onmousedown="HSVslide('SVslide','plugin',event)" title="Saturation + Value">
  <div id="SVslide" style="TOP: -4px; LEFT: -4px;"><br /></div>
 </div>
 <form id="H" action="" onmousedown="HSVslide('Hslide','plugin',event)" title="Hue">
  <div id="Hslide" style="TOP: -7px; LEFT: -8px;"><br /></div>
  <div id="Hmodel"></div>
 </form>
 <!--[if lte IE 6.5]><iframe></iframe><![endif]-->
</div>
<form class="public" action = "{$_SERVER['PHP_SELF']}" id="customizeform" method="post">
    <table id="settings2" cellspacing="0">
      <tr id="textcolorentry">
        <td class="label"><label for="textcolor">Flipside text color</label></td><td><input type="text" class="colorpicker" id="textcolor" tabindex="20" name="textColor" value="{$textColor}" /></td>
      </tr>
      <tr id="linktextcolorentry">
        <td class="label"><label for="linktextcolor">Link text color</label></td><td><input type="text" class="colorpicker" id="linktextcolor" tabindex="21" name="linkTextColor" value="{$linkTextColor}" /></td>
      </tr>
      <tr id="linkbkgndcolorentry">
        <td class="label"><label for="linkbkgndcolor">Link background</label></td><td><input type="text" class="colorpicker" id="linkbkgndcolor" tabindex="22" name="linkBkgndColor" value="{$linkBkgndColor}" /></td>
      </tr>
      <tr id="navbuttoncolorentry">
        <td class="label"><label for="navbuttoncolor">Nav. button color</label></td><td><input type="text" class="colorpicker" id="navbuttoncolor" tabindex="29" name="navButtonColor" value="{$navButtonColor}" /></td>
      </tr>
      <tr id="flipbuttoncolorentry">
        <td class="label"><label for="flipbuttoncolor">Flip button color</label></td><td><input type="text" class="colorpicker" id="flipbuttoncolor" tabindex="30" name="flipButtonColor" value="{$flipButtonColor}" /></td>
      </tr>
      <tr id="framewidthentry">
        <td class="label"><label for="framewidth">Frame width, px</label></td><td><input type="text" id="framewidth" tabindex="23" class="text" name="frameWidth" value="{$preferences['frameWidth']}" /></td>
      </tr>
      <tr id="linklinktargeteentry">
        <td class="label"><label for="linktarget">Link target</label></td><td><input type="text" id="linktarget" tabindex="28" class="text" name="linkTarget" value="{$preferences['linkTarget']}" /></td>
      </tr>
      <tr id="zoomedindistanceentry">
        <td class="label"><label for="zoomedindistance">Zoomed-in distance</label></td><td><input type="text" id="zoomedindistance" tabindex="24" class="text" name="zoomedInDistance" value="{$preferences['zoomedInDistance']}" /></td>
      </tr>
      <tr id="zoomedoutdistanceentry">
        <td class="label"><label for="zoomedoutdistance">Zoomed-out distance</label></td><td><input type="text" id="zoomedoutdistance" tabindex="25" class="text" name="zoomedOutDistance" value="{$preferences['zoomedOutDistance']}" /></td>
      </tr>
    </table>
    <table id="settings3" cellspacing="0">
      <tr id="tiltamountinentry">
        <td class="label"><label for="tiltamountin">Tilt amount in</label></td><td><input type="text" id="tiltamountin" tabindex="26" class="text" name="tiltAmountIn" value="{$preferences['tiltAmountIn']}" /></td>
      </tr>
      <tr id="tiltamountoutentry">
        <td class="label"><label for="tiltamountout">Tilt amount out</label></td><td><input type="text" id="tiltamountout" tabindex="27" class="text" name="tiltAmountOut" value="{$preferences['tiltAmountOut']}" /></td>
      </tr>
      <tr id="fontnameentry">
        <td class="label"><label for="fontname">Font name</label></td><td><input type="text" id="fontname" tabindex="31" class="text" name="fontName" value="{$preferences['fontName']}" /></td>
      </tr>
      <tr id="titlefontsizeentry">
        <td class="label"><label for="titlefontsize">Title font size</label></td><td><input type="text" id="titlefontsize" tabindex="32" class="text" name="titleFontSize" value="{$preferences['titleFontSize']}" /></td>
      </tr>
      <tr id="descriptionfontsizeentry">
        <td class="label"><label for="descriptionfontsize">Description font size</label></td><td><input type="text" id="descriptionfontsize" tabindex="33" class="text" name="descriptionFontSize" value="{$preferences['descriptionFontSize']}" /></td>
      </tr>
      <tr id="linkfontsizeentry">
        <td class="label"><label for="linkfontsize">Link font size</label></td><td><input type="text" id="linkfontsize" tabindex="34" class="text" name="linkFontSize" value="{$preferences['linkFontSize']}" /></td>
      </tr>
      <tr id="enablesoundsentry">
        <td class="label"><label for="enablesounds">Enable sounds</label></td><td><input type="checkbox" class="checkbox" id="enablesounds" tabindex="35" {$enableSoundsChecked} name="enableSounds" /></td>
      </tr>
      <tr id="bkgndtransparententry">
        <td class="label"><label for="bkgndtransparent">Backgnd transparent</label></td><td><input type="checkbox" class="checkbox" id="bkgndtransparent" tabindex="36" {$bkgndTransparentChecked} name="bkgndTransparent" /></td>
      </tr>
      <tr id="showfullscreenoptionentry">
        <td class="label"><label for="showfullscreenoption">Full screen option</label></td><td><input type="checkbox" class="checkbox" id="showfullscreenoption" tabindex="37" {$showFullscreenOptionChecked} name="showFullscreenOption" /></td>
      </tr>
    </table>
    <div id="submitinputs">
    <input type="hidden" name="customizesubmitted" value="true" /><input type="submit" name="submit" class="formbutton" value="Update" />
  </div>
</form>
EOD;
    return $html;
  }

}
