<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Customize page
 *
 * @package svManager
 */
class SvCustomizePage extends Page
{
 /**
  * @var string overrides some of head content in Page
  */
  var $customHeadHtml = '
       <link href="colorjack/plugin.css" rel="stylesheet" type="text/css" />
       <style type="text/css">
         #plugin {
           color: #FFFFFF;
           background-color: #999999;
         }
         #H {
           border-color: #999999;
         }
         #plugCLOSE {
           color: #FFFFFF;
           background-color: #999999;
         }
       </style>
       <script src="scripts/mootools-core.js" type="text/javascript"></script>
       <script src="scripts/mootools-more.js" type="text/javascript"></script>
       <script src="colorjack/plugin.js" type="text/javaScript"></script>
       <script src="scripts/customize.js" type="text/javascript"></script>
       <script src="scripts/mooformcheck.js" type="text/javascript"></script>
';
 
 /**
  * constructs CusomizePage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function SvCustomizePage()
  {
    parent::Page('svManager &ndash; customize', 'svcustomize', 'navcustomize customize simpleviewer');
  }

 /**
  * get html for customize page
  *
  * Note hack in conditional comments from
  * @link http://www.hedgerwow.com/360/bugs/css-select-free.html
  * to prevent select elements showing above the colorpicker in IE6Win
  * @access public
  * @return string html
  * @param object gallery set
  * @param array gallery attributes
  */
  function getCustomizeHtml(&$gallerySet, &$gallery)
  {
    $preferences = $gallery->getPreferences();
    $rootUrl = $gallery->getRootUrl();
    $title = htmlspecialchars($preferences['title'], ENT_QUOTES, 'UTF-8');
    $serverName = 'http://'.$_SERVER['SERVER_NAME'].'/';
    $svManagerUrl = substr(dirname($_SERVER['PHP_SELF']), 1);
    $textColor = strtoupper(substr($preferences['textColor'], 2, strlen($preferences['textColor']) - 2));
    $frameColor = strtoupper(substr($preferences['frameColor'], 2, strlen($preferences['frameColor']) - 2));
    $backgroundColor = strtoupper(substr($preferences['backgroundColor'], 2, strlen($preferences['backgroundColor']) - 2));
    $vAlign = isset($preferences['vAlign']) ? strtolower($preferences['vAlign']) : V_ALIGN;
    $hAlign = isset($preferences['hAlign']) ? strtolower($preferences['hAlign']) : H_ALIGN;
    $navPadding = isset($preferences['navPadding']) ? $preferences['navPadding'] : NAV_PADDING;
    $addLinksChecked = $preferences['addLinks'] ? 'checked="checked"' : '';
    $enableRightClickOpenChecked = ($preferences['enableRightClickOpen'] == 'true') ? 'checked="checked"' : '';
    
    $options = array('top_left'=>'top left', 'top_center'=>'top center', 'top_right'=>'top right', 'center_left'=>'center left', 'center_center'=>'centered', 'center_right'=>'center right', 'bottom_left'=>'bottom left', 'bottom_center'=>'bottom center', 'bottom_right'=>'bottom right');
    $alignmentHtml = $this->htmlSelect('alignment', 'alignment', '6', $options, $vAlign.'_'.$hAlign);
    $options = array('left'=>'left', 'right'=>'right', 'top'=>'top', 'bottom'=>'bottom');
    $navPositionHtml = $this->htmlSelect('navPosition', 'navposition', '7', $options, $preferences['navPosition']);
    $options = array('0'=>'0', '1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10');
    $thumbnailRowsHtml = $this->htmlSelect('thumbnailRows', 'thumbnailrows', '9', $options, $preferences['thumbnailRows']);
    $options = array('0'=>'0', '1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10');
    $thumbnailColumnsHtml = $this->htmlSelect('thumbnailColumns', 'thumbnailcolumns', '10', $options, $preferences['thumbnailColumns']);
    $relativePathChecked = '';
    $absolutePathChecked = '';
    $httpPathChecked = '';
    switch (strtoupper($preferences['urlType']))
    {
      case 'ABSOLUTE' :
      $absolutePathChecked = 'checked="checked"';
      break;
      case 'HTTP' :
      $httpPathChecked = 'checked="checked"';
      break;
      default :
      $relativePathChecked = 'checked="checked"';
    }


$html = <<<EOD

<div id="plugin" class="selectfree">
 <div id="plugHEX" onmousedown="stop=0; setTimeout('stop=1',100);">F1FFCC</div>
 <!-- Note change of function name from toggle to avoid namespace conflict -->
 <div id="plugCLOSE" onmousedown="toggleDisplay('plugin')">X</div><br />
 <div id="SV" onmousedown="HSVslide('SVslide','plugin',event)" title="Saturation + Value">
  <div id="SVslide" style="TOP: -4px; LEFT: -4px;"><br /></div>
 </div>
 <form id="H" action="" onmousedown="HSVslide('Hslide','plugin',event)" title="Hue">
  <div id="Hslide" style="TOP: -7px; LEFT: -8px;"><br /></div>
  <div id="Hmodel"></div>
 </form>
 <!--[if lte IE 6.5]><iframe></iframe><![endif]-->
</div>
<form class="public" action = "{$_SERVER['PHP_SELF']}" id="customizeform" method="post">
  <div class="elementwrap">
    <h3 class="togglers">Basic settings</h3>
    <div class="accordionElements">
      <table id="settings1" cellspacing="0">
        <tr id="titleentry">
          <td class="label"><label for="title">Gallery title</label></td><td><input type="text" id="title" tabindex="1" class="text" name="title" value="{$title}" /></td>
        </tr>
      </table>
    
      <table id="settings2" cellspacing="0">
        <tr id="textcolorentry">
          <td class="label"><label for="textcolor">Text color</label></td><td><input type="text" id="textcolor" tabindex="3" class="colorpicker"  name="textColor" value="{$textColor}" /></td>
        </tr>
        <tr id="framecolorentry">
          <td class="label"><label for="framecolor">Frame color</label></td><td><input type="text" id="framecolor" tabindex="4" class="colorpicker" name="frameColor" value="{$frameColor}" /></td>
        </tr>
        <tr id="backgroundcolorentry">
          <td class="label"><label for="backgroundcolor">Background color</label></td><td><input type="text" class="colorpicker" id="backgroundcolor" tabindex="5" name="backgroundColor" value="{$backgroundColor}" /></td>
        </tr>
        <tr id="backgroundimagepathentry">
          <td class="label"><label for="backgroundimagepath">Background image</label></td><td><input type="text" id="backgroundimagepath" tabindex="6" class="text" name="backgroundImagePath" value="{$preferences['backgroundImagePath']}" /></td>
        </tr>
        <tr id="alignmententry">
          <td class="label"><label for="alignment">Gallery alignment</label></td><td>{$alignmentHtml}</td>
        </tr>
        <tr id="navpositionentry">
          <td class="label"><label for="navposition">Navigate position</label></td><td>{$navPositionHtml}</td>
        </tr>
        <tr id="thumbnailrowsentry">
          <td class="label"><label for="thumbnailrows">Thumbnail rows</label></td><td>{$thumbnailRowsHtml}</td>
        </tr>
        <tr id="thumbnailcolumnsentry">
          <td class="label"><label for="thumbnailcolumns">Thumbnail col&rsquo;s</label></td><td>{$thumbnailColumnsHtml}</td>
        </tr>
      </table>
    
      <table id="settings3" cellspacing="0">
        <tr id="framewidthentry">
          <td class="label"><label for="framewidth">Frame width, px</label></td><td><input type="text" id="framewidth" tabindex="11" class="text" name="frameWidth" value="{$preferences['frameWidth']}" /></td>
        </tr>
        <tr id="stagepaddingentry">
          <td class="label"><label for="stagepadding">Stage padding, px</label></td><td><input type="text" id="stagepadding" tabindex="12" class="text" name="stagePadding" value="{$preferences['stagePadding']}" /></td>
        </tr>
        <tr id="navpaddingentry">
          <td class="label"><label for="navpadding">Nav padding, px</label></td><td><input type="text" id="navpadding" tabindex="13" class="text" name="navPadding" value="{$preferences['navPadding']}" /></td>
        </tr>
        <tr id="maximagewidthentry">
          <td class="label"><label for="maximagewidth">Max image width, px</label></td><td><input type="text" id="maximagewidth" tabindex="14" class="text" name="maxImageWidth" value="{$preferences['maxImageWidth']}" /></td>
        </tr>
        <tr id="maximageheightentry">
          <td class="label"><label for="maximageheight">Max image height, px</label></td><td><input type="text" id="maximageheight" tabindex="15" class="text" name="maxImageHeight" value="{$preferences['maxImageHeight']}" /></td>
        </tr>
        <tr id="addlinksentry">
          <td class="label"><label for="addlinks">Auto caption links</label></td><td><input type="checkbox" class="checkbox" id="addlinks" tabindex="16" {$addLinksChecked} name="addLinks" /></td>
        </tr>
        <tr id="enablerightclickopenentry">
          <td class="label"><label for="enablerightclickopen">Right-click download</label></td><td><input type="checkbox" class="checkbox" id="enablerightclickopen" tabindex="17" {$enableRightClickOpenChecked} name="enableRightClickOpen" /></td>
        </tr>
        <tr id="submitreset">
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
    </div>
  </div>
  <div class="elementwrap" id="advanced">
    <h3 class="togglers">Advanced settings</h3>
    <div class="accordionElements">
      <table id="settingspaths" cellspacing="0">
        <tr id="absurlentry">
          <td class="label"><label for="absurl">Path from web root</label></td><td><input type="text" id="absurl" tabindex="2" class="text" name="absurl" value="{$rootUrl}" /></td>
        </tr>
        <tr id="urltypeentry">
            <td class="label">Save paths as</td>
            <td><input id="relativepath" tabindex="12" type="radio" name="urlType" value="RELATIVE" {$relativePathChecked} /><label for="relativepath">Relative</label>
            <input id="absolutepath" tabindex="13" type="radio" name="urlType" value="ABSOLUTE" {$absolutePathChecked} /><label for="absolutepath">Absolute</label>
            <input id="httppath" tabindex="13" type="radio" name="urlType" value="HTTP" {$httpPathChecked} /><label for="httppath">http</label></td>
          </tr>
      </table>
    </div>
  </div>
  <div id="submitinputs">
    <input type="hidden" name="customizesubmitted" value="true" /><input type="submit" name="submit" class="formbutton" value="Update" />
  </div>
</form>
EOD;
    return $html;
  }

}
