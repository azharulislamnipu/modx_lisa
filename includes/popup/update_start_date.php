<?php
//error_reporting(0);
include_once "../../home/modxapi.php";
$modx = new MODxAPI();
//$modx->connect();
//$modx->startSession();
//var_dump($_SESSION['CUSTOMERCD']);
//die();

if(empty($_SESSION['CUSTOMERCD'])){
	header('Location:  '.  $_SERVER['HTTP_REFERER']);
	exit;
}
$new_path = '';
/*
 * Local
 */
$pos = strrpos(__DIR__, "htdocs");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'htdocs/';
}

/**
 * Server
 */
/*$pos = strrpos(__DIR__, "apache");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'apache/';
}*/

include $new_path . "includes/functions.php";
if(!is_ajax()){
	die('No direct access!');
}
	define('LoggedIn', true);
	ob_start();
	include $new_path . "includes/popup-templates/update_start_date_template.php";
	ob_end_flush();
	die();