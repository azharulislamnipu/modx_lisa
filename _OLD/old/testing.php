<div class="inner">


    <!-- #MAIN COLUMN -->

    <div class="profile_title_header clearfix">
        <h1 id="profile_title">
            Member Tes Aecs
            <!---->
        </h1>
        <div class="edit_btn_wrap">
            <button class="button open-popup">Edit Profile</button>
            <span class="last-updated" style="display: inline;">Last Updated: <span>05/27/2017</span></span>
        </div>
    </div>

    <div class="span-4">
        <div class="margin-top-none">
            <div class="width padding">

                <div class="prof_img">

                    <img id="profile_img" src="/home/assets/images/archons/2.member.jpeg"
                         alt="Image of Member Tes Aecs ">
                    <br><br>
                </div>
                <div class="archon-archousa-info clearfix" id="archousa_block" style="

">
                    <div class="span-12 archons-info">
                        <h3>Archousa Snapshot</h3>

                        <div class="span-4 width float-left archousa_profile_img">

                            <img id="archousa_profile_img" src="/home/assets/images/archousa/10000002.guala.jpeg"
                                 alt="Image of Guala Cr Reza III">

                        </div>


                        <div class="span-8 inner">
                            <p><span id="ar_fname">Guala</span> Cr Reza III</p>
                            <p class="font-small">
                                <strong>Kal</strong>,
                                Ral
                            </p>
                            <p class="btn_p"><a class="button small_button" id="view_archousa"
                                                href="/home/archousa-profile.php">View</a>
                            </p>
                        </div>
                    </div>
                </div>


                <div class="contact-box" id="contact_wrap">

                    <p><strong>Mailing Address</strong><br>
                        Aja <br>
                        Gajaa, AK 165465 </p>

                    <p><strong>Email</strong><br>
                        jodu@modu.com</p>

                    <p><strong>Home Phone</strong><br>
                        12356454</p>

                    <p><strong>Mobile</strong><br>
                        +8801912948179</p>
                    <p></p>

                    <p><strong>Birthdate</strong><br>
                        April 18
                    </p>

                </div>

                <!-- [ !LDataDisplay? &table=`spp_officers` &display=`all` &tpl=`tpl_archon_prof_offices` &filter=`CUSTOMERID,2,1` &sortBy=`START_DATE` &sortDir=`ASC`]]-->

                <div id="children_block">
                    <strong>Children</strong>

                    <ul class="children_list">
                        <li id="child_id_61">
                            Karen Alease Archon
                        </li>
                        <li id="child_id_51">
                            Lauren Selena Archon
                        </li>
                        <li id="child_id_31">
                            Bryan Armand Archon
                        </li>
                        <li id="child_id_11846">
                            James Baloon Bond
                        </li>
                    </ul>
                </div>


            </div>
        </div>
    </div>
    <!-- #END MAIN COLUMN -->


    <!-- #BEGIN SECOND COLUMN -->
    <div class="span-4">
        <div class="padding">
            <div class="width padding">


                <!-- #END SECOND COLUMN -->


                <!-- #BEGIN SECOND COLUMN -->

                <h3>Career</h3>

                <div id="profile_career_box">
                    <p id="prof_id_11">
                        <strong>Goods</strong>, Morphogen Pharmaceuticals, San Diego, CA<br>
                        <!--<span class="font-small"></span><br/>
						<span class="font-small"></span><br/>
						-->
                    </p>
                    <p id="prof_id_10706">
                        <strong>Jet</strong>, Jambo, ALoha, AZ<br>
                        <!--<span class="font-small"></span><br/>
						<span class="font-small"></span><br/>
						-->
                    </p>
                    <p id="prof_id_1">
                        <strong>Egora</strong>, Manhattan Sports Medicine, New York, NY<br>
                        <!--<span class="font-small"></span><br/>
						<span class="font-small"></span><br/>
						-->
                    </p>
                    <p id="prof_id_10707">
                        <strong>Xhasdfodu</strong>, Jodu, Lodu, BS<br>
                        <!--<span class="font-small"></span><br/>
						<span class="font-small"></span><br/>
						-->
                    </p>
                    <p id="prof_id_10708">
                        <strong>Taro</strong>, Lado, kahot, CT<br>
                        <!--<span class="font-small"></span><br/>
						<span class="font-small"></span><br/>
						-->
                    </p>
                    <p id="prof_id_10710">
                        <strong>Ruatre</strong>, Ruaj, Koare, AZ<br>
                        <!--<span class="font-small"></span><br/>
						<span class="font-small"></span><br/>
						-->
                    </p>

                </div>

                <h3>Education</h3>
                <div id="profile_educ_box">
                    <p id="educ_id_34541">
                        <strong>ma</strong>, 2016<br>
                        ba, me<br>
                        <span class="font-small">me</span><br>
                    </p>
                    <p id="educ_id_34544">
                        <strong>Lu</strong>, 2016<br>
                        Ja, Tu<br>
                        <span class="font-small">Mu</span><br>
                    </p>
                    <p id="educ_id_34545">
                        <strong>Gajaj</strong>, 2016<br>
                        Raja, Rajl<br>
                        <span class="font-small">Tor</span><br>
                    </p>
                    <p id="educ_id_34540">
                        <strong>sadf</strong>, 2133<br>
                        asfsa, sadf<br>
                        <span class="font-small">asdf</span><br>
                    </p>

                </div>

            </div>
        </div>
    </div>

    <!-- #begin THIRD COLUMN -->
    <div class="span-4">
        <div class="padding">
            <div class="width padding" id="archon_bio">

                <h3>Biography</h3>


                <div id="bio_text" style="display: block;"><p>My Bio is <strong>bold. </strong>fasdfas &nbsp;<strong>sadf&nbsp;<em>afsdf&nbsp;</em>sdf</strong>
                    </p></div>


                <!-- #END THIRD COLUMN -->
            </div>
        </div>


    </div>
</div>