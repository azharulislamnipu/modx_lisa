<?php

class SPP_ARCHON_DB {

    /**
     * Get General User Info
     * From SPP Archons DB
     * @param $user_id
     * @return array
     */
    public static function get_user_profile_data($user_id){
        $db = get_db();
        $db->where ("CUSTOMERID", $user_id);
        return array(
            'data' => $db->getOne('spp_archons'),
            'count' => $db->count
        );
    }

    public static function get_elig_user_profile_data($user_id){
	    return self::get_profile_data($user_id, 'vw_elig');
    }

    private static function get_profile_data($user_id, $table){
	    $db = get_db();
	    $db->where ("CUSTOMERID", $user_id);
	    return array(
		    'data' => $db->getOne('vw_elig'),
		    'count' => $db->count
	    );
    }

    public static function get_elig_user_profile_ID($webid){
        $db = get_db();
        $db->where ('WEB_ID', $webid);
        return array(
            'data' => $db->getValue('vw_elig', 'CUSTOMERID'),
            'count' => $db->count
        );
    }

    public static function get_archousa_profile_data($user_id){
        $db = get_db();
        $db->where ("ARCHOUSAID", $user_id);
        return array(
            'data' => $db->getOne('spp_archousa'),
            'count' => $db->count
        );
    }

	public static function get_archousa_prof_by_archon($archon_id){
		$db = get_db();
		$db->where ("CUSTOMERCD", $archon_id);
		$db->where ("MARITALSTATUS", 'CUR');
		return array(
			'data' => $db->getOne('spp_archousa'),
			'count' => $db->count
		);
	}

    /**
     * Get all prefix
     * @return array
     */
    public static function get_all_prefix(){
        $db= get_db();
        $db->orderBy('PREFIX', 'asc');
        return array(
            'data' => $db->get('spp_sal'),
            'count' => $db->count
        );
    }

    /**
     * Get all Suffix
     * @return array
     */
    public static function get_all_suffix(){
        $db= get_db();
        $db->orderBy('SUFFIX', 'asc');
        return array(
            'data' => $db->get('spp_suffix'),
            'count' => $db->count
        );
    }

    /**
     * Get All States
     * @return array
     */
    public static function get_all_state(){
        $db= get_db();
        $db->orderBy('st_nm', 'asc');
        return array(
            'data' => $db->get('spp_state'),
            'count' => $db->count
        );
    }

    /**
     * Get All Designation
     * @return array
     */
    public static function get_all_hos_desig(){
        $db= get_db();
        $db->orderBy('DESIGNATIONLST', 'asc');
        $cols = array("hon_id", "DESIGNATIONLST");
        return array(
            'data' => $db->get('spp_hon', null, $cols),
            'count' => $db->count
        );
    }

    /**
     * Career Elements
     */

    /**
     * Get All Profession Name
     * @return array
     */
    public static function get_all_profession(){
        $db= get_db();
        $db->orderBy('DESCRIPTION', 'asc');
        $cols = array("OCCUPATIONID", "DESCRIPTION");
        return array(
            'data' => $db->get('spp_occupation', null, $cols),
            'count' => $db->count
        );
    }

    /**
     * Get All Profession Name Title
     * @return array
     */
    public static function get_all_prof_title(){
        $db= get_db();
        $db->orderBy('DESCRIPTION', 'asc');
        $cols = array("SKILLID", "DESCRIPTION");
        return array(
            'data' => $db->get('spp_skillref', null, $cols),
            'count' => $db->count
        );
    }

    public static function get_prof_title($ids){
        $db= get_db();
        $db->orderBy('DESCRIPTION', 'asc');
        $cols = array("SKILLID", "DESCRIPTION");
        if($ids){
	        $db->where ("SKILLID", $ids);
        }
        return array(
            'data' => $db->get('spp_skillref', null, $cols),
            'count' => $db->count
        );
    }

    /**
     * Get All Profession Name Title
     * @param $user_id
     * @return array
     */
    public static function get_all_user_prof($user_id){
        $db = get_db();
        $db->orderBy('OCCUPATIONID', 'asc');
        $db->where ("CUSTOMERID", $user_id);
        return array(
            'data' => $db->get('spp_prof'),
            'count' => $db->count
        );
    }
    public static function get_all_archousa_prof($user_id){
        $db = get_db();
        $db->orderBy('OCCUPATIONID', 'asc');
        $db->where ("ARCHOUSAID", $user_id);
        return array(
            'data' => $db->get('spp_prof'),
            'count' => $db->count
        );
    }

    /**
     * Get User Education Information
     * @param $user_id
     * @return array
     */
    public static function get_all_user_edu($user_id){
        $db = get_db();
        $db->where ("CUSTOMERID", $user_id);
        return array(
            'data' => $db->get('spp_educ'),
            'count' => $db->count
        );
    }

    public static function get_all_archousa_edu($user_id){
        $db = get_db();
        $db->where ("ARCHOUSAID", $user_id);
        return array(
            'data' => $db->get('spp_educ'),
            'count' => $db->count
        );
    }

    /**
     * Get All Children of user
     * @param $user_id
     * @return array
     */
    public static function get_all_user_children($user_id){
        $db = get_db();
        $db->orderBy("BIRTHDATE","Desc");
        $db->where ("CUSTOMERID", $user_id);
        return array(
            'data' => $db->get('spp_children'),
            'count' => $db->count
        );
    }
    public static function get_all_archousa_children($user_id){
        $db = get_db();
        $db->orderBy("BIRTHDATE","Desc");
        $db->where ("ARCHOUSAID", $user_id);
        return array(
            'data' => $db->get('spp_children'),
            'count' => $db->count
        );
    }

    /**
     * Get All Spouses of user
     * @param $user_id
     * @return array
     */
    public static function get_all_user_spouse($user_id){
        $db = get_db();
        $db->where ("CUSTOMERCD", $user_id);
        return array(
            'data' => $db->get('spp_archousa'),
            'count' => $db->count
        );
    }

    public static function update_lastupdate_date($id, $where='CUSTOMERID', $table='spp_archons'){
        $db = get_db();
        $data = array(
            'LASTUPDATED'       => date("Y-m-d H:i:s")
        );
        $db->where ($where, $id);
        $db->update ($table, $data);
    }

    public static function get_active_boule_archons($boulename){
	    $db = get_db();
//	    $db->setTrace (true);
	    $db->join("vw_boule_officers o", "g.CUSTOMERID=o.CUSTOMERCD", "LEFT");
	    $db->orderBy("g.FULLNAME","ASC");
	    $db->where ("g.BOULENAME", $boulename);
	    $db->where ("g.STATUS", 'Deceased', "!=");
	    $return =  array(
		    'data' => $db->get('vw_gramm_archons g', null, "g.*, o.CPOSITION, o.COMMITTEESTATUSSTT"),
		    'count' => $db->count
	    );
//	    print_r ($db->trace);
	    return $return;
    }

    public static function get_active_boule_archons_new($boulename, $where_cond='CHAPTERID'){
        if($where_cond == 'BOULENAME'){
            $where_cond = 'CHAPTERID';
        }
	    $db = get_db();
//	    $db->setTrace (true);
	    $db->orderBy("LASTNAME","ASC");
	    if($where_cond == 'CHAPTERID'){
            $db->where ($where_cond, $boulename);
        } else {
            $db->where ($where_cond, $boulename, 'LIKE');
        }
	    $db->where ("STATUS", 'Deceased', "!=");
        $return =  array(
//		    'data' => $db->get('vw_gramm_archons'),
		    'data' => $db->get('vw_search_y2'),
		    'count' => $db->count
	    );
//        var_dump($return['count']);
//	    print_r ($db->trace);
//	    die();
	    return $return;
    }
    public static function get_active_boule_officers($boulename, $where_cond='BOULENAME'){
	    $db = get_db();
//	    $db->setTrace (true);
	    $db->where ($where_cond, $boulename, 'LIKE');
	    $db->where ("COMMITTEESTATUSSTT", 'Active');
	    $return = array(
		    'data' => $db->get('vw_boule_officers'),
		    'count' => $db->count
	    );
//	    print_r ($db->trace);
	    return $return;
    }

}