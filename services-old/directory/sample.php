<?php
//Connection statement
require_once('../Connections/sigma_modx.php');

//Aditional Functions
require_once('../includes/functions.inc.php');

// begin Recordset
$query_rsNortheast = "SELECT * FROM spp_boule WHERE REGIONCD = 'NE' ORDER BY BOULENAME ASC";
$rsNortheast = $modx->SelectLimit($query_rsNortheast) or die($modx->ErrorMsg());
$totalRows_rsNortheast = $rsNortheast->RecordCount();
// end Recordset

// begin Recordset
$query_rsSoutheast = "SELECT * FROM spp_boule WHERE REGIONCD = 'SE' ORDER BY BOULENAME ASC";
$rsSoutheast = $modx->SelectLimit($query_rsSoutheast) or die($modx->ErrorMsg());
$totalRows_rsSoutheast = $rsSoutheast->RecordCount();
// end Recordset

// begin Recordset
$query_rsCentral = "SELECT * FROM spp_boule WHERE REGIONCD = 'CE' ORDER BY BOULENAME ASC";
$rsCentral = $modx->SelectLimit($query_rsCentral) or die($modx->ErrorMsg());
$totalRows_rsCentral = $rsCentral->RecordCount();
// end Recordset

// begin Recordset
$query_rsWest = "SELECT * FROM spp_boule WHERE REGIONCD = 'WE' ORDER BY BOULENAME ASC";
$rsWest = $modx->SelectLimit($query_rsWest) or die($modx->ErrorMsg());
$totalRows_rsWest = $rsWest->RecordCount();
// end Recordset

// begin Recordset
$query_rsPacific = "SELECT * FROM spp_boule WHERE REGIONCD = 'PA' ORDER BY BOULENAME ASC";
$rsPacific = $modx->SelectLimit($query_rsPacific) or die($modx->ErrorMsg());
$totalRows_rsPacific = $rsPacific->RecordCount();
// end Recordset

// begin Recordset
$colname__rsByLastname = '-1';
if (isset($_GET['LASTNAME'])) {
  $colname__rsByLastname = $_GET['LASTNAME'];
}
$query_rsByLastname = sprintf("SELECT spp_archons.CHAPTERID,   spp_boule.REGIONCD,   spp_boule.REGIONNAME,   spp_boule.BOULENAME,   spp_boule.CITY,   spp_boule.STATECD,   spp_archons.STATUSSTT,   spp_archons.CUSTOMERCLASSSTT,   spp_archons.CUSTOMERTYPE,   spp_archons.CUSTOMERID,   spp_archons.JOINDATE,   spp_archons.PREFIX,   spp_archons.FIRSTNAME,   spp_archons.MIDDLEINITIAL,   spp_archons.LASTNAME,   spp_archons.SUFFIX,   spp_archons.DESIGNATIONLST,   spp_archons.ADDRESS1,   spp_archons.ADDRESS2,   spp_archons.CITY,   spp_archons.STATECD,   spp_archons.ZIP,   spp_archons.HOMEPHONE,   spp_archons.EMAIL,   spp_archons.CPOSITION,   spp_archons.JOBTITLE,   spp_archons.OCCUPATIONCD,   spp_archons.SKILLCDLST,   spp_archons.ORGNAME,   spp_archons.IMAGE,   spp_archons.BIRTHDATE,   spp_archons.BIO FROM spp_boule   INNER JOIN spp_archons ON (spp_boule.BOULENAME = spp_archons.CHAPTERID) WHERE ((spp_archons.CUSTOMERTYPE = 'Member') OR    (spp_archons.CUSTOMERTYPE = 'Emeritus')) AND    ((spp_archons.STATUSSTT = 'Active') OR    ((spp_archons.STATUSSTT = 'Inactive') AND    (spp_archons.CUSTOMERCLASSSTT = 'Voluntary'))) AND   LASTNAME = %s ORDER BY FIRSTNAME, MIDDLEINITIAL ASC", GetSQLValueString($colname__rsByLastname, "text"));
$rsByLastname = $modx->SelectLimit($query_rsByLastname) or die($modx->ErrorMsg());
$totalRows_rsByLastname = $rsByLastname->RecordCount();
// end Recordset

// begin Recordset
$colname__rsByBoule = '-1';
if (isset($_GET['BOULENAME'])) {
  $colname__rsByBoule = $_GET['BOULENAME'];
}
$query_rsByBoule = sprintf("SELECT spp_archons.CHAPTERID,   spp_boule.REGIONCD,   spp_boule.REGIONNAME,   spp_boule.BOULENAME,   spp_boule.CITY,   spp_boule.STATECD,   spp_archons.STATUSSTT,   spp_archons.CUSTOMERCLASSSTT,   spp_archons.CUSTOMERTYPE,   spp_archons.CUSTOMERID,   spp_archons.JOINDATE,   spp_archons.PREFIX,   spp_archons.FIRSTNAME,   spp_archons.MIDDLEINITIAL,   spp_archons.LASTNAME,   spp_archons.SUFFIX,   spp_archons.DESIGNATIONLST,   spp_archons.ADDRESS1,   spp_archons.ADDRESS2,   spp_archons.CITY,   spp_archons.STATECD,   spp_archons.ZIP,   spp_archons.HOMEPHONE,   spp_archons.EMAIL,   spp_archons.CPOSITION,   spp_archons.JOBTITLE,   spp_archons.OCCUPATIONCD,   spp_archons.SKILLCDLST,   spp_archons.ORGNAME,   spp_archons.IMAGE,   spp_archons.BIRTHDATE,   spp_archons.BIO FROM spp_boule   INNER JOIN spp_archons ON (spp_boule.BOULENAME = spp_archons.CHAPTERID) WHERE ((spp_archons.CUSTOMERTYPE = 'Member') OR    (spp_archons.CUSTOMERTYPE = 'Emeritus')) AND    ((spp_archons.STATUSSTT = 'Active') OR    ((spp_archons.STATUSSTT = 'Inactive') AND    (spp_archons.CUSTOMERCLASSSTT = 'Voluntary'))) AND   BOULENAME = %s ORDER BY LASTNAME, FIRSTNAME, MIDDLEINITIAL ASC", GetSQLValueString($colname__rsByBoule, "text"));
$rsByBoule = $modx->SelectLimit($query_rsByBoule) or die($modx->ErrorMsg());
$totalRows_rsByBoule = $rsByBoule->RecordCount();
// end Recordset

// begin Recordset
$colname__rsByCity = '-1';
if (isset($_GET['CITY'])) {
  $colname__rsByCity = $_GET['CITY'];
}
$query_rsByCity = sprintf("SELECT spp_archons.CHAPTERID,   spp_boule.REGIONCD,   spp_boule.REGIONNAME,   spp_boule.BOULENAME,   spp_boule.CITY,   spp_boule.STATECD,   spp_archons.STATUSSTT,   spp_archons.CUSTOMERCLASSSTT,   spp_archons.CUSTOMERTYPE,   spp_archons.CUSTOMERID,   spp_archons.JOINDATE,   spp_archons.PREFIX,   spp_archons.FIRSTNAME,   spp_archons.MIDDLEINITIAL,   spp_archons.LASTNAME,   spp_archons.SUFFIX,   spp_archons.DESIGNATIONLST,   spp_archons.ADDRESS1,   spp_archons.ADDRESS2,   spp_archons.CITY,   spp_archons.STATECD,   spp_archons.ZIP,   spp_archons.HOMEPHONE,   spp_archons.EMAIL,   spp_archons.CPOSITION,   spp_archons.JOBTITLE,   spp_archons.OCCUPATIONCD,   spp_archons.SKILLCDLST,   spp_archons.ORGNAME,   spp_archons.IMAGE,   spp_archons.BIRTHDATE,   spp_archons.BIO FROM spp_boule   INNER JOIN spp_archons ON (spp_boule.BOULENAME = spp_archons.CHAPTERID) WHERE ((spp_archons.CUSTOMERTYPE = 'Member') OR    (spp_archons.CUSTOMERTYPE = 'Emeritus')) AND    ((spp_archons.STATUSSTT = 'Active') OR    ((spp_archons.STATUSSTT = 'Inactive') AND    (spp_archons.CUSTOMERCLASSSTT = 'Voluntary'))) AND spp_archons.CITY = %s ORDER BY spp_archons.STATECD, LASTNAME, FIRSTNAME, MIDDLEINITIAL ASC", GetSQLValueString($colname__rsByCity, "text"));
$rsByCity = $modx->SelectLimit($query_rsByCity) or die($modx->ErrorMsg());
$totalRows_rsByCity = $rsByCity->RecordCount();
// end Recordset

// begin Recordset
$colname__rsBouleOfficers = '-1';
if (isset($_GET['BOULENAME'])) {
  $colname__rsBouleOfficers = $_GET['BOULENAME'];
}
$query_rsBouleOfficers = sprintf("SELECT spp_archons.CHAPTERID,   spp_boule.REGIONCD,   spp_boule.REGIONNAME,   spp_boule.BOULENAME,   spp_boule.CITY,   spp_boule.STATECD,   spp_archons.STATUSSTT,   spp_archons.CUSTOMERCLASSSTT,   spp_archons.CUSTOMERTYPE,   spp_archons.CUSTOMERID,   spp_archons.JOINDATE,   spp_archons.PREFIX,   spp_archons.FIRSTNAME,   spp_archons.MIDDLEINITIAL,   spp_archons.LASTNAME,   spp_archons.SUFFIX,   spp_archons.DESIGNATIONLST,   spp_archons.ADDRESS1,   spp_archons.ADDRESS2,   spp_archons.CITY,   spp_archons.STATECD,   spp_archons.ZIP,   spp_archons.HOMEPHONE,   spp_archons.EMAIL,   spp_archons.CPOSITION,   spp_archons.JOBTITLE,   spp_archons.OCCUPATIONCD,   spp_archons.SKILLCDLST,   spp_archons.ORGNAME,   spp_archons.IMAGE,   spp_archons.BIRTHDATE,   spp_archons.BIO FROM spp_boule   INNER JOIN spp_archons ON (spp_boule.BOULENAME = spp_archons.CHAPTERID) WHERE ((spp_archons.CUSTOMERTYPE = 'Member') OR    (spp_archons.CUSTOMERTYPE = 'Emeritus')) AND    ((spp_archons.STATUSSTT = 'Active') OR    ((spp_archons.STATUSSTT = 'Inactive') AND    (spp_archons.CUSTOMERCLASSSTT = 'Voluntary')))  AND    (spp_archons.CPOSITION IS NOT NULL) AND   spp_boule.BOULENAME = %s ORDER BY LASTNAME, FIRSTNAME, MIDDLEINITIAL ASC", GetSQLValueString($colname__rsBouleOfficers, "text"));
$rsBouleOfficers = $modx->SelectLimit($query_rsBouleOfficers) or die($modx->ErrorMsg());
$totalRows_rsBouleOfficers = $rsBouleOfficers->RecordCount();
// end Recordset

// begin Recordset
$query_rsBoule = "SELECT * FROM spp_boule ORDER BY BOULENAME ASC";
$rsBoule = $modx->SelectLimit($query_rsBoule) or die($modx->ErrorMsg());
$totalRows_rsBoule = $rsBoule->RecordCount();
// end Recordset

// begin Recordset
$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = $modx->SelectLimit($query_rsStates) or die($modx->ErrorMsg());
$totalRows_rsStates = $rsStates->RecordCount();
// end Recordset
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sigma Pi Phi Fraternity | Membership Directory</title>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="author" content="fullahead.org" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index, follow, noarchive" />
<meta name="googlebot" content="noarchive" />
<link rel="stylesheet" type="text/css" href="/home/assets/css/html-iframe.css" media="all" />
<link rel="stylesheet" type="text/css" href="/home/assets/css/layout.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/home/assets/css/print.css" media="print" />
<link rel="stylesheet" type="text/css" href="/home/assets/css/text.css" media="all" />

<link rel="stylesheet" type="text/css" href="/home/assets/css/map.css" media="all" />
<link rel="stylesheet" type="text/css" href="/home/assets/css/yearbook.css" media="all" />

<!--[if IE]><link rel="stylesheet" type="text/css" href="/home/assets/css/ie.css" media="all" /><![endif]-->
<script type="text/javascript" src="/home/assets/js/map/map.js"></script>
</head>

<body>
      <h1 class="yellow">
        Member Boul&eacute;s
      </h1>
<div class="contentBlock">

        <p align="center">
          Move your mouse over the map below to see an outline of each region.<br>
Click the region to see a list of boul&eacute;s for that region.<br>
Click a boul&eacute; name to list the members of the boul&eacute;.</p>

        <div id="map"
          onmouseover="document.getElementById('img').src='/directory/images/chapterMap/usa_on.gif'"
          onmouseout="document.getElementById('img').src='/directory/images/chapterMap/usa_off.gif'">

        <img src="/directory/images/chapterMap/usa_off.gif" alt="Boul&eacute; Map" name="img" width="550" height="320" id="img" title="Boul&eacute; Map"/>

        <ul>

          <li id="pacific"><a href="#nogo" class="tl" onclick="map.showPopup('pPacific')" title="View Pacific Region Boul&eacute;s">Pacific Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pPacific" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Pacific Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=PA" class="close">Pacific Region Officers</a></p>
                <p><a href="http://www.agboule07.com/" class="close" target="_blank">Pacific Region Meeting Website</a></p>
				<p><a href="javascript:map.hidePopup('pPacific')" title="Close Popup" class="close">Close Pacific Popup</a></p>
                <div>
                  <?php
  while (!$rsPacific->EOF) { 
?>
                  <a href="boule_results.php?BOULENAME=<?php echo $rsPacific->Fields('BOULENAME'); ?>"><?php echo $rsPacific->Fields('BOULENAME'); ?> (<?php echo $rsPacific->Fields('CITY'); ?>, <?php echo $rsPacific->Fields('STATECD'); ?>)</a>
                    <?php
    $rsPacific->MoveNext(); 
  }
?>
                </div>
              </div>
            <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="western"><a href="#nogo" class="tl" onclick="map.showPopup('pWestern')" title="View Western Region Boul&eacute;s">Western Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pWestern" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Western Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=WE" class="close">Western Region Officers</a></p>
                <p><a href="http://www.deltamuboule.org/" class="close" target="_blank">Western Region Meeting Website</a></p>
                <p><a href="javascript:map.hidePopup('pWestern')" title="Close Popup" class="close">Close Western Popup</a></p>
              <div>
<?php
  while (!$rsWest->EOF) { 
?>
                  <a href="boule_results.php?BOULENAME=<?php echo $rsWest->Fields('BOULENAME'); ?>"><?php echo $rsWest->Fields('BOULENAME'); ?> (<?php echo $rsWest->Fields('CITY'); ?>, <?php echo $rsWest->Fields('STATECD'); ?>)</a>
                    <?php
    $rsWest->MoveNext(); 
  }
?>
				</div>
              </div>
              <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="central"><a href="#nogo" class="tl" onclick="map.showPopup('pCentral')" title="View Central Region Boul&eacute;s">Central Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pCentral" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Central Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=CE" class="close">Central Region Officers</a></p>
                <p><a href="javascript:map.hidePopup('pCentral')" title="Close Popup" class="close">Close Central Popup</a></p>
                <div>
<?php
  while (!$rsCentral->EOF) { 
?>
                  <a href="boule_results.php?BOULENAME=<?php echo $rsCentral->Fields('BOULENAME'); ?>"><?php echo $rsCentral->Fields('BOULENAME'); ?> (<?php echo $rsCentral->Fields('CITY'); ?>, <?php echo $rsCentral->Fields('STATECD'); ?>)</a>
                    <?php
    $rsCentral->MoveNext(); 
  }
?>
                </div>
              </div>
            <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="northeast"><a href="#nogo" class="tl" onclick="map.showPopup('pNortheast')" title="View Northeast Region Boul&eacute;s">Northeast Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pNortheast" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Northeast Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=NE" class="close">Northeast Region Officers</a></p>
                <p><a href="http://www.rochesterboule.org/" class="close" target="_blank">Northeast Region Meeting Website</a></p>
                <p><a href="javascript:map.hidePopup('pNortheast')" title="Close Popup" class="close">Close Northeast Popup</a></p>
              <div>
<?php
  while (!$rsNortheast->EOF) { 
?>
                  <a href="boule_results.php?BOULENAME=<?php echo $rsNortheast->Fields('BOULENAME'); ?>"><?php echo $rsNortheast->Fields('BOULENAME'); ?> (<?php echo $rsNortheast->Fields('CITY'); ?>, <?php echo $rsNortheast->Fields('STATECD'); ?>)</a>
                    <?php
    $rsNortheast->MoveNext(); 
  }
?>
                </div>
              </div>
            <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="southeast"><a href="#nogo" class="tl" onclick="map.showPopup('pSoutheast')" title="View Southeast Region Boul&eacute;s">Southeast Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pSoutheast" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Southeast Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=SE" class="close">Southeast Region Officers</a></p>
                <p><a href="http://www.gammakappaboule.org/" class="close" target="_blank">Southeast Region Meeting Website</a></p>
                <p><a href="javascript:map.hidePopup('pSoutheast')" title="Close Popup" class="close">Close Southeast Popup</a></p>
                <div>
<?php
  while (!$rsSoutheast->EOF) { 
?>
                  <a href="boule_results.php?BOULENAME=<?php echo $rsSoutheast->Fields('BOULENAME'); ?>"><?php echo $rsSoutheast->Fields('BOULENAME'); ?> (<?php echo $rsSoutheast->Fields('CITY'); ?>, <?php echo $rsSoutheast->Fields('STATECD'); ?>)</a>
                    <?php
    $rsSoutheast->MoveNext(); 
  }
?>
                </div>
              </div>
            <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>
        </ul>
  </div>
<table align="center" cellpadding="2" cellspacing="4">
  <form action="lastname_results.php?LASTNAME=<?php echo $_POST['LASTNAME']; ?>" method="get" name="LASTNAME">
    <tr>
    <td>Search by Last Name</td>
    <td><input id="LASTNAME" name="LASTNAME" type="text" size="20" maxlength="20" /> 
	<input type="submit" class="macButton" name="Submit1" value="Submit"></td>
    <td>&nbsp;</td>
  </tr></form>
<form id="CITY" name="CITY" method="get" action="city_results.php?CITY=<?php echo $rsByCity->Fields('CITY'); ?>">
  <tr class="searchForm">
    <td>Search by City </td>
    <td><input name="CITY" type="text" id="CITY" size="20" maxlength="20">
      <input type="submit" class="macButton" name="Submit2" value="Submit"></td>
    <td>&nbsp;</td>
  </tr>
</form>
<form class="searchForm" action="boule_results.php?BOULENAME=<?php echo $_POST['BOULENAME']; ?>" method="get" name="BOULENAME">  <tr>
    <td>Search by Boul&eacute; </td>
    <td><select name="BOULENAME" id="BOULENAME">
      <option value="">Select a boul&eacute;.</option>
      <?php
  while(!$rsBoule->EOF){
?>
      <option value="<?php echo $rsBoule->Fields('BOULENAME')?>"><?php echo $rsBoule->Fields('BOULENAME'). " (" .$rsBoule->Fields('CITY'). ", ". $rsBoule->Fields('STATECD').")"  ?> </option>
      <?php
    $rsBoule->MoveNext();
  }
  $rsBoule->MoveFirst();
?>
    </select>
      <input type="submit" class="macButton" name="Submit3" value="Submit"></td>
    <td>&nbsp;</td>
  </tr>
</form>
<form id="STATECD" name="STATECD" method="get" action="state_results.php?STATECD=<?php echo $_POST['STATECD']; ?>" class="searchForm">
  <tr>
    <td>Search by State </td>
    <td><select name="STATECD">
      <option value="">Select a state.</option>
      <?php
  while(!$rsStates->EOF){
?>
      <option value="<?php echo $rsStates->Fields('st_cd')?>"><?php echo $rsStates->Fields('st_nm')?></option>
      <?php
    $rsStates->MoveNext();
  }
  $rsStates->MoveFirst();
?>
    </select>
      <input type="submit" class="macButton" name="Submit4" value="Submit"></td>
    <td>&nbsp;</td>
  </tr>
</form>
</table>

 
</div>
</body>
</html>
<?php
$rsNortheast->Close();

$rsSoutheast->Close();

$rsCentral->Close();

$rsWest->Close();

$rsPacific->Close();

$rsByLastname->Close();

$rsByBoule->Close();

$rsByCity->Close();

$rsBouleOfficers->Close();

$rsBoule->Close();

$rsStates->Close();
?>
