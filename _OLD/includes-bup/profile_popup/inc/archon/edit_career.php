<div id="archon_edit_career" class="accordion parent-elem edit_career" data-accordion>
    <div data-control><h3>Edit Career</h3></div>
    <div data-content>
        <div class="data-content-inner data-content-inner-tabs ymp_form_input">
            <?php

            $i = 0;

            if(!empty($user_prof_data)) {

                foreach ($user_prof_data as $index => $user_prof_single) {


                    $user_prof_name = $all_prof_name[$user_prof_single['OCCUPATIONID']]['DESCRIPTION'];
                    $user_prof_title = $all_prof_title[$user_prof_single['TITLEID']]['DESCRIPTION'];

                    ?>
                    <div id="archon_edit_carrer_<?php echo $index; ?>"
                         class="accordion parent-elem edit_career_inner" data-accordion>
                        <div data-control>
                            <h3><?php echo $user_prof_single['COMPANYTITLE'] . ', ' . $user_prof_single['COMPANYNAME']; ?></h3></div>
                        <div data-content>
                            <div class="data-content-inner clearfix ymp_wrapper grid-12">
                                <div class="ymp-row">

                                    <div class="span-6">
                                        <label for="COMPANYNAME_<?php echo $index; ?>">Company Name
                                            <span class="required_ind">*</span></label>
                                        <input class="input-required input-validated" type="text"
                                               name="COMPANYNAME" id="COMPANYNAME_<?php echo $index; ?>"
                                               value="<?php echo $user_prof_single['COMPANYNAME']; ?>"
                                               placeholder="Enter Company Name">
                                    </div><!--End Item-->

                                    <div class="span-6">
                                        <label for="COMPANYTITLE_<?php echo $index; ?>">Company Title
                                            <span class="required_ind">*</span></label>
                                        <input class="input-required input-validated" type="text"
                                               name="COMPANYTITLE" id="COMPANYTITLE_<?php echo $index; ?>"
                                               value="<?php echo $user_prof_single['COMPANYTITLE']; ?>"
                                               placeholder="Enter Company Title">
                                    </div><!--End Item-->

                                    <div class="clearfix"></div>

                                    <div class="span-6">
                                        <label for="COMPANYCITY_<?php echo $index; ?>">Company City
                                            <span class="required_ind">*</span></label>
                                        <input class="input-required input-validated" type="text"
                                               name="COMPANYCITY" id="COMPANYCITY_<?php echo $index; ?>"
                                               value="<?php echo $user_prof_single['COMPANYCITY']; ?>"
                                               placeholder="Enter Company City">
                                    </div><!--End Item-->

                                    <div class="span-6">
                                        <label for="COMPANYSTCD_<?php echo $index; ?>">Company State
                                            <span class="required_ind">*</span></label>
                                        <select class="input-required input-validated chosen-select"
                                                name="COMPANYSTCD"
                                                id="COMPANYSTCD_<?php echo $index; ?>" data-placeholder="Choose a State...">
                                            <?php foreach ($all_states['data'] as $state) {
                                                echo "<option value='{$state['st_cd']}'" . selected($state['st_cd'], $user_prof_single['COMPANYSTCD'], false) . ">{$state['st_nm']}</option>";
                                            } ?>
                                        </select>
                                    </div><!--End Item-->

                                    <div class="clearfix"></div>

                                    <div class="span-6">
                                        <label for="OCCUPATIONID_<?php echo $index; ?>">Industry <span
                                                    class="required_ind">*</span></label>
                                        <select class="input-required input-validated chosen-select"
                                                name="OCCUPATIONID"
                                                id="OCCUPATIONID_<?php echo $index; ?>"  data-placeholder="Choose a Profession...">
                                            <?php foreach ($all_prof_name as $prof_single_name) {
                                                echo "<option value='{$prof_single_name['OCCUPATIONID']}'" . selected($prof_single_name['OCCUPATIONID'], $user_prof_single['OCCUPATIONID'], false) . ">{$prof_single_name['DESCRIPTION']}</option>";
                                            } ?>
                                        </select>
                                    </div><!--End Item-->

                                    <div class="span-6">
                                        <label for="TITLEID_<?php echo $index; ?>">Occupation <span
                                                    class="required_ind">*</span></label>
                                        <select class="input-required input-validated chosen-select" name="TITLEID"
                                                id="TITLEID_<?php echo $index; ?>"   data-placeholder="Choose a Title...">
                                            <?php foreach ($all_prof_title as $prof_single_title) {
                                                echo "<option value='{$prof_single_title['SKILLID']}'" . selected($prof_single_title['SKILLID'], $user_prof_single['TITLEID'], false) . ">{$prof_single_title['DESCRIPTION']}</option>";
                                            } ?>
                                        </select>
                                    </div><!--End Item-->

                                    <div class="clearfix"></div>

                                    <div class="span-12">
                                        <label for="DESCRIPTION_<?php echo $index; ?>">Highlights</label>
                                        <textarea class="tiny-editable" type="text"
                                                  name="DESCRIPTION"
                                                  id="DESCRIPTION_<?php echo $index; ?>"><?php echo $user_prof_single['DESCRIPTION']; ?></textarea>
                                    </div><!--End Item-->

                                    <div class="clearfix"></div>
                                    <div class="clearfix" style="padding: 6px;"></div>
                                    
                                    <div class="span-6">
                                        <label for="COMPANYJOBSTART_<?php echo $index; ?>">Company Start
                                            Date</label>
                                        <div class="clearfix birth_date_input_wrap">
                                            <a class="birth_date_btn"
                                               id="COMPANYJOBSTART_<?php echo $index; ?>_btn"
                                               href="#"><img
                                                    src="<?php echo $base_url; ?>/assets/templates/archon/images/calendar-icon.png"
                                                    alt="Select Date"></a>
                                            <div class="bdinput">
                                                <input type="text" data-monthdate="" name="COMPANYJOBSTART"
                                                       size="20" maxlength="40"
                                                       id="COMPANYJOBSTART_<?php echo $index; ?>"
                                                       value="<?php echo $user_prof_single['COMPANYJOBSTART']; ?>"
                                                       placeholder="">
                                            </div>
                                        </div>
                                    </div><!--End Item-->

                                    <div class="span-6">
                                        <label for="COMPANYJOBEND_<?php echo $index; ?>">Company End
                                            Date</label>
                                        <div class="clearfix birth_date_input_wrap">
                                            <a class="birth_date_btn"
                                               id="COMPANYJOBEND_<?php echo $index; ?>_btn"
                                               href="#"><img
                                                    src="<?php echo $base_url; ?>/assets/templates/archon/images/calendar-icon.png"
                                                    alt="Select Date"></a>
                                            <div class="bdinput">
                                                <input type="text" data-monthdate="" name="COMPANYJOBEND"
                                                       size="20" maxlength="40"
                                                       id="COMPANYJOBEND_<?php echo $index; ?>"
                                                       value="<?php echo $user_prof_single['COMPANYJOBEND']; ?>"
                                                       placeholder="">
                                            </div>
                                        </div>
                                    </div><!--End Item-->

                                    <div class="clearfix"></div>
                                </div>

                                <input type="hidden" name="PROFID"
                                       value="<?php echo $user_prof_single['PROFID']; ?>">

                                <?php echo $submit_dv; ?>
                            </div>
                        </div>
                    </div>
                <?php }

                $i = count($user_prof_data);
            }

            while ($i < 10){?>

                <div id="archon_edit_carrer_<?php echo $i; ?>"
                     class="accordion parent-elem edit_career_inner add_career_inner" data-accordion>
                    <div data-control>
                        <h3>New Career</h3></div>
                    <div data-content>
                        <div class="data-content-inner clearfix ymp_wrapper grid-12">

                            <div class="ymp-row">

                                <div class="span-6">
                                    <label for="COMPANYNAME_<?php echo $i; ?>">Company Name
                                        <span class="required_ind">*</span></label>
                                    <input class="input-required " type="text"
                                           name="COMPANYNAME" id="COMPANYNAME_<?php echo $i; ?>"
                                           value=""
                                           placeholder="Enter Company Name">
                                </div><!--End Item-->

                                <div class="span-6">
                                    <label for="COMPANYTITLE_<?php echo $i; ?>">Company Title
                                        <span class="required_ind">*</span></label>
                                    <input class="input-required " type="text"
                                           name="COMPANYTITLE" id="COMPANYTITLE_<?php echo $i; ?>"
                                           value=""
                                           placeholder="Enter Company Title">
                                </div><!--End Item-->

                                <div class="clearfix"></div>

                                <div class="span-6">
                                    <label for="COMPANYCITY_<?php echo $i; ?>">Company City
                                        <span class="required_ind">*</span></label>
                                    <input class="input-required " type="text"
                                           name="COMPANYCITY" id="COMPANYCITY_<?php echo $i; ?>"
                                           value=""
                                           placeholder="Enter Company City">
                                </div><!--End Item-->

                                <div class="span-6">
                                    <label for="COMPANYSTCD_<?php echo $i; ?>">Company State
                                        <span class="required_ind">*</span></label>

                                    <select class="input-required chosen-select"
                                            name="COMPANYSTCD"
                                            id="COMPANYSTCD_<?php echo $i; ?>">
                                        <option value="">Select One...</option>
                                        <?php foreach ($all_states['data'] as $state) {
                                            echo "<option value='{$state['st_cd']}'" . ">{$state['st_nm']}</option>";
                                        } ?>
                                    </select>
                                </div><!--End Item-->

                                <div class="clearfix"></div>

                                <div class="span-6">
                                    <label for="OCCUPATIONID_<?php echo $i; ?>">Industry <span
                                                class="required_ind">*</span></label>
                                    <select class="input-required chosen-select"
                                            name="OCCUPATIONID"
                                            id="OCCUPATIONID_<?php echo $i; ?>">
                                        <?php foreach ($all_prof_name as $prof_single_name) {
                                            echo "<option value='{$prof_single_name['OCCUPATIONID']}'" . ">{$prof_single_name['DESCRIPTION']}</option>";
                                        } ?>
                                    </select>
                                </div><!--End Item-->

                                <div class="span-6">
                                    <label for="TITLEID_<?php echo $i; ?>">Occupation <span
                                                class="required_ind">*</span></label>
                                    <select class="input-required chosen-select" name="TITLEID"
                                            id="TITLEID_<?php echo $i; ?>">
                                        <?php foreach ($all_prof_title as $prof_single_title) {
                                            echo "<option value='{$prof_single_title['SKILLID']}'" . ">{$prof_single_title['DESCRIPTION']}</option>";
                                        } ?>
                                    </select>
                                </div><!--End Item-->

                                <div class="clearfix"></div>

                                <div class="span-12">
                                    <label for="DESCRIPTION_<?php echo $i; ?>">Highlights</label>
                                    <textarea class="tiny-editable" type="text"
                                              name="DESCRIPTION"
                                              id="DESCRIPTION_<?php echo $i; ?>"></textarea>
                                </div><!--End Item-->

                                <div class="clearfix"></div>
                                <div class="clearfix" style="padding: 6px;"></div>

                                <div class="span-6">
                                    <label for="COMPANYJOBSTART_<?php echo $i; ?>">Company Start
                                        Date</label>
                                    <div class="clearfix birth_date_input_wrap">
                                        <a class="birth_date_btn"
                                           id="COMPANYJOBSTART_<?php echo $i; ?>_btn"
                                           href="#"><img
                                                    src="<?php echo $base_url; ?>/assets/templates/archon/images/calendar-icon.png"
                                                    alt="Select Date"></a>
                                        <div class="bdinput">
                                            <input type="text" data-monthdate="" name="COMPANYJOBSTART"
                                                   size="20" maxlength="40"
                                                   id="COMPANYJOBSTART_<?php echo $i; ?>"
                                                   value=""
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div><!--End Item-->

                                <div class="span-6">
                                    <label for="COMPANYJOBEND_<?php echo $i; ?>">Company End
                                        Date</label>
                                    <div class="clearfix birth_date_input_wrap">
                                        <a class="birth_date_btn"
                                           id="COMPANYJOBEND_<?php echo $i; ?>_btn"
                                           href="#"><img
                                                    src="<?php echo $base_url; ?>/assets/templates/archon/images/calendar-icon.png"
                                                    alt="Select Date"></a>
                                        <div class="bdinput">
                                            <input type="text" data-monthdate="" name="COMPANYJOBEND"
                                                   size="20" maxlength="40"
                                                   id="COMPANYJOBEND_<?php echo $i; ?>"
                                                   value=""
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div><!--End Item-->

                                <div class="clearfix"></div>
                            </div>

                            <input type="hidden" name="PROFID"
                                   value="">

                            <?php echo $submit_dv; ?>
                        </div>
                    </div>
                </div>
                <?php
                ++$i;
            }


            ?>

        </div>
        <div class="sb_div">
            <button class="button add_new_career">Add New Profession</button>
        </div>

    </div>
</div>