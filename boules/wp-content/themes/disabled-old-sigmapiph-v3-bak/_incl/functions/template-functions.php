<?php

/*
 * Custom template functions
 */

define('IS_DEV', strpos($_SERVER['HTTP_HOST'], 'dev3') !== false);

/**
 * Group the Boule's by their region
 */
function group_sites_by_region( $region = '' ) {

	$sites_by_region = get_transient('archon_network_sites_by_region_' . $region);

	if (false === $sites_by_region) {

		$sites_by_region = array();

		// Get all the visible sites.  The "sigma_get_multisites" function is defined by the Archon Network
		// plugin and should always be available, but hey, I'm a cyncial kinda guy and this might help the
		// future version of me if the the Sigma site lists suddenly stop populating.
		$sites = function_exists( 'sigma_get_multisites' ) ? sigma_get_multisites( 1, false, false ) : array();
		if (!empty($sites)) {

			// Get the link between each region and Boule
			$modx = get_modx_database();

			//$modx = new wpdb($username_sigma_modx, $password_sigma_modx, $database_sigma_modx, $hostname_sigma_modx);
			$query = !empty($region) ?
				$modx->prepare("select BOULENAME, REGIONNAME, CITY, STATECD from spp_boule WHERE REGIONNAME = %s ORDER BY ORGCD", $region) :
				"select BOULENAME, REGIONNAME, CITY, STATECD from spp_boule ORDER BY REGIONNAME, ORGCD";

			$boules = $modx->get_results($query);
			if (!empty($boules)) {
				foreach ($boules as $boule) {

					// Do we have a matching WP Boule site?
					if (isset($sites[$boule->BOULENAME])) {

						// Check if the Boule's region needs to be added
						if (!isset($sites_by_region[$boule->REGIONNAME])) {
							$sites_by_region[$boule->REGIONNAME] = array();
						}

						// Store the WP site object by its region
						$sites[$boule->BOULENAME]->city = $boule->CITY;
						$sites[$boule->BOULENAME]->statecd = $boule->STATECD;
						$sites_by_region[$boule->REGIONNAME][$boule->BOULENAME] = $sites[$boule->BOULENAME];
					}
				}
			}
			if (!empty($sites_by_region)) {
				set_transient('archon_network_sites_by_region_' . $region, $sites_by_region, (60 * 60));
			}
		}
	}
	return $sites_by_region;
}

/**
 * Clears the cached site lists when a new blog is created
 */
function clear_cache ( $blog_id = '' ) {
	delete_transient('archon_network_sites');
	delete_transient('archon_network_sites_by_region');
}
add_action('wpmu_new_blog', 'clear_cache');
add_action('wpmueditblogaction', 'clear_cache');


/*
 * Sorts blogs by name in alphabetical ascending order
 */
function sigma_sort_by_blogname( $a, $b ){
    return strcmp($a->blogname, $b->blogname);
}

/*
 * Sorts blogs by ORGCD in alphabetical ascending order
 */
function sigma_sort_by_orgcd( $a, $b ){
    return ($a->ORGCD > $b->ORGCD ? 1 : ($a->ORGCD < $b->ORGCD ? -1 : 0));
}

/*
 * Gets a page's content given a page slug
 */
function get_page_content_by_slug( $slug ) {

    $content = '<p>'.__('No '. str_replace('-', ' ', $slug) .' found.', 'buddypress').'</p>';
    query_posts( 'post_type=page&orderby=date&order=DESC$posts_per_page=1&pagename=' . $slug );

	// Found something
    if ( have_posts() ) {
    	the_post();

    	// Can the user view it?
		if ( can_view_content() ) {
        	$content = get_the_content();
		} else {
			$content = '<p>' . __('You are not authorized to view this content.', 'archon-network') . '</p>';
		}
	}
    wp_reset_query();
    echo $content;
}

/*
 * Returns a set of content
 */
function get_content( $post_type, $cat, $posts_per_page, $posts_exclude = array() ){

	// Determine what content this archon is allowed to view
	$can_view_all_archons = can_view_all_archons();
	$post_status = 'publish' . ( $can_view_all_archons ? ',private' : '' );

	// Everyone can view posts with _visible_to meta set
	$meta_query =
		array (
			array(
				'key' => '_visible_to',
				'value' => 'foo',
				'compare' => 'NOT EXISTS'
			)
		);

	// If this is a logged in Archon, they can also view "All Archons" content and
	// potentially "Boule only" as well.
	if ( $can_view_all_archons ) {
		$meta_query[ 'relation' ] = 'OR';
		$meta_query[] =
			array(
				'key' => '_visible_to',
				'value' => can_view_boule_only() ? array( 'archons', 'boule' ) : array ( 'archons' ),
				'compare' => 'IN'
			);
	}

	return
		new WP_Query(
			array(
				'post_type' => $post_type,
				'post_status' => $post_status,
				'cat' => $cat,
				'posts_per_page' => $posts_per_page,
				'post__not_in' => $posts_exclude,
				'meta_query' => $meta_query
			)
		);
}


/**
 * Determines if a given page has content
 * @param Mixed $page_id_or_slug Page numeric ID or slug
 * @return boolean
 */
function is_page_content( $page_id_or_slug ){
    $is_content = false;

    $page = is_numeric($page_id_or_slug) ? get_page($page_id_or_slug) : get_page_by_path($page_id_or_slug);
    if($page !== null){
        $is_content = strlen($page->post_content) > 0;
    }
    return $is_content;
}


/**
 * Determines if a category has content based on its slug
 * @param Mixed $slug $cat_id_or_slug Category numeric ID or slug
 * @return boolean
 */
function is_category_content( $cat_id_or_slug ){
    global $wpdb;

    $is_content = false;
	$can_view_all_archons = can_view_all_archons();

    $cat = is_numeric($cat_id_or_slug) ? get_category($cat_id_or_slug) : get_category_by_slug($cat_id_or_slug);
    if(!empty($cat)){

		// Must be an archon to view private content
		$poststatus = "'publish'";
		if ( $can_view_all_archons ) {
			$poststatus .= ",'private'";
		}

		// Must be a member of the given Boule to view private, Boule only content
		$postmeta_OR = "";
		if ( $can_view_all_archons ) {
			$postmeta_OR = "OR $wpdb->postmeta.meta_value IN ('archons'" . ( can_view_boule_only() ? ", 'boule'" : "" ) . ")";
		}

		$query = "
			SELECT COUNT(*) FROM
			$wpdb->posts

			JOIN $wpdb->term_relationships
			ON $wpdb->posts.ID = $wpdb->term_relationships.object_id

			JOIN $wpdb->term_taxonomy
			ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id

			LEFT JOIN $wpdb->postmeta
			ON $wpdb->posts.ID = $wpdb->postmeta.post_id
			AND $wpdb->postmeta.meta_key = '_visible_to'

			WHERE
			$wpdb->term_taxonomy.term_id = $cat->cat_ID
			AND $wpdb->posts.post_status IN ($poststatus)
			AND (
				$wpdb->postmeta.meta_key IS NULL
				$postmeta_OR
			)
			LIMIT 1";

        $count = $wpdb->get_var($query);
        $is_content = $count > 0;
    }
    return $is_content;
}


/*
 * Retrieves a set of content by a given category slug
 */
function get_content_by_category( $cat, $post_type = array('page','post'), $count = 5, $show_excerpt = true ){

	$is_content = false;
	if(!empty($cat)){

		// Gets the content
		$content = get_content($post_type, $cat->cat_ID, $count);
		if($content->have_posts()){

			$is_content = true;
			$currPage = get_query_var('paged'); ?>

			<ul class="<?php echo $currPage?> link-list">
			<?php while($content->have_posts()) : $content->the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>">
						<strong><?php the_title(); ?></strong>
						<?php if($show_excerpt):?><span class="exceprt"><?php the_excerpt(); ?></span><?php endif;?>
					</a>
				</li>
			<?php endwhile; ?>
			</ul> <?php

			wp_reset_query();
		}
	}

	// Issue an error if no content returned
	if(!$is_content):
		if(!empty($cat)):?>
			<p>No <?php echo strtolower($cat->name) ?> found.</p>
		<?php else: ?>
			<p>No content found.</p>
		<?php endif;
	endif;
}


/*
 * Gets a list of a page's children's excerpts given a page ID or slug
 */
function get_page_children_excerpts( $page_id_or_slug, $count = 5, $show_excerpt = true, $sort = 'DESC' ){

    $excerpts = '';

    if($page_id_or_slug){
        $parentId = is_numeric($page_id_or_slug) ? $page_id_or_slug : get_page_id($page_id_or_slug);
        if($parentId){
            $currPage = get_query_var( 'paged' );
            query_posts('post_type=page&orderby=date&order=' . $sort . '$posts_per_page=' . $count . '&post_parent=' . $parentId . ($currPage != 0 ? '&paged=' . $currPage  : ''));
            if (have_posts()) {
                echo "<ul class='".$currPage." link-list'>\n";
                while (have_posts()) : the_post();
                	if ( can_view_content() ): ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <strong><?php the_title(); ?></strong>
                            <?php if($show_excerpt):?><span class="exceprt"><?php the_excerpt(); ?></span><?php endif;?>
                        </a>
                    </li>
                    <?php endif;
                endwhile;
                echo "</ul>\n";
            } else {
                $page_title = get_the_title($parentId);
                $excerpts = '<p>'.__('No '. ($page_title ? strtolower($page_title) : 'entries') .' found.', 'buddypress').'</p>';
            }
        }
    }
    echo $excerpts;
}


/*
 * Gets the first image from given content
 */
function get_first_image(){
    global $post;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/(<img.+src=[\'"][^\'"]+[\'"].*>)/i', $post->post_content, $matches);
    $first_img = $matches[1][0];

    return !empty($first_img) ? $first_img : '';
}


/*
 * Retrieves a list of the Boul� chapters leadership
 */
function get_boule_leadership( $chapter_name ){

    // Pull the officers from the MODx tables
    $modxdb = get_modx_database();
    $modxdb->suppress_errors(false);
    $modxdb->show_errors(true);

    $officers = $modxdb->get_results(
        "SELECT vw_boule_officers.L2R_FULLNAME, vw_boule_officers.WEB_ID, vw_boule_officers.CPOSITION, vw_boule_officers.CPOSITIONID FROM vw_boule_officers WHERE vw_boule_officers.COMMITTEESTATUSSTT = 'Active' AND vw_boule_officers.BOULENAME = '" . $chapter_name . "' ORDER BY  vw_boule_officers.CPOSITIONID"
    );

    // Loop through and display the officers for the given Boul�
    if($officers) :
		$isLoggedIn = is_user_logged_in();
        echo "<ul id='chapter-officers'>\n";
        foreach($officers as $officer){
        	if ( $isLoggedIn ) {
        		echo '<li><strong>' . $officer->CPOSITION . '</strong><a href="/services/directory/results.php?id=' . $officer->WEB_ID . '">' . $officer->L2R_FULLNAME . '</a></li>'."\n";
        	} else {
        		echo '<li><strong>' . $officer->CPOSITION . '</strong>' . $officer->L2R_FULLNAME . '</li>'."\n";
        	}
        }
        echo "</ul>\n";
    else :
        echo '<p>No chapter officers found.</p>';
    endif;
}

/**
 * Outputs featured image posts and returns the IDs of the posts displayed
 */
function get_featured_image_posts( $limit = 3 ) {


	$count = 0;
	$featuredImgs = new WP_Query(array('posts_per_page' => -1, 'meta_key' => '_thumbnail_id'));
	while ( $featuredImgs->have_posts() ) :
		$posts_displayed[] = $post->ID;
		$featuredImgs->the_post(); ?>

		<div class="featured">
			<a href="<?php the_permalink(); ?>" title="<?php _e( 'View post', 'archon-network' ); ?>"></a>
			<div class="text">
				<h2><?php the_title(); ?></h2>
				<p><?php the_excerpt(); ?></p>
				<span class="link"><?php _e( 'View', 'archon-network' ); ?></span>
			</div>
			<?php if ($count++ === 0): ?><span class="corner"></span><?php endif; ?>
			<?php the_post_thumbnail(); ?>
		</div>

		<?php if ($count++ >= $limit) {
			break;
		}

	endwhile;
	wp_reset_postdata();

	return $posts_displayed;
}

/**
 * Creates pagination links
 */
function paginate() {
	global $wp_query, $wp_rewrite;
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

	$pagination = array(
		'base' => @add_query_arg('page','%#%'),
		'format' => '',
		'total' => $wp_query->max_num_pages,
		'current' => $current,
		'show_all' => true,
		'type' => 'list',
		'prev_text'    => __('Previous'),
		'next_text'    => __('Next'),
		);

	if( $wp_rewrite->using_permalinks() )
		$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

	if( !empty($wp_query->query_vars['s']) )
		$pagination['add_args'] = array( 's' => get_query_var( 's' ) );

	echo paginate_links( $pagination );
}

/** Checks if a given post has a gallery */
function has_gallery( $post_id = false ) {
    if ( !$post_id ) {
        global $post;
    } else {
        $post = get_post($post_id);
    }
    return strpos( $post->post_content,'[gallery') !== false;
}

/**
 * Returns a list of gallery links
 * [galleries page="slugs"] */
function galleries_shortcode( $atts ) {
	global $post;

	extract( shortcode_atts( array(
		'page' => $post->post_name
	), $atts ) );

	return $page;
}
add_shortcode( 'galleries', 'galleries_shortcode' );


?>