<?php
include "functions.php";
echo "<pre>";
//$csv = array_map('str_getcsv', file('prof.csv'));
//$csv = str_getcsv(file_get_contents('prof.csv'), ",", '"');

/** Set default timezone (will throw a notice otherwise) */
date_default_timezone_set('America/New_York');

// include PHPExcel
include_once 'phpexcel/PHPExcel.php';

// create new PHPExcel object
$objPHPExcel = new PHPExcel;
$inputFileName = 'prof.xlsx';
$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

echo '<hr />';

$sheetData = $objPHPExcel->getActiveSheet()->toArray();

$res = $desc_arr = $id_arr = [];

$i = 0;

foreach ($sheetData as $index=>$val) {
    $nval = trim(reset($val));
    if(!empty($nval)){
//        $desc[] = $nval;
//        $nval = trim(substr($nval, 0, 5));
//        $nval = str_replace(' ', '', $nval);
//        $id[] = $nval;

        $desc = $nval;
        $desc2 = trim(substr($nval, 0, 30));;
        $nval = trim(substr($nval, 0, 5));
        $nval = str_replace(' ', '', $nval);
        $id = $nval;


        if(!in_array($desc2, $desc_arr)){

            if(in_array($id, $id_arr)){
                $id = $nval.'_'.++$i;
            }

            $desc_arr[] = $desc2;
            $id_arr[] = $id;
            $res[] = array(
                'DESCRIPTION' => $desc,
                'OCCUPATIONCD' => $id,
            );
        }
    }
}
//$res['DESCRIPTION'] = array_unique($desc);
//$res['OCCUPATIONCD'] = array_unique($id);

// function defination to convert array to xml

function array_to_xml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
        if( is_numeric($key) ){
            $key = 'item'; //dealing with <0/>..<n/> issues
        }
        if( is_array($value) ) {
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
    }
}

$xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');

array_to_xml($res,$xml_data);

$result = $xml_data->asXML('prof.xml');



//$db= get_db();
//$db_success = $db->insert ('spp_children', $ndata);