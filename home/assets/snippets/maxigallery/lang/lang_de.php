<?php
// German language file
// If you are going to make your own language file and are usign the lighbox, 
// make a language file for it too, f.ex. /maxigallery/slidebox/slidebox_lang_en.js
$strings['previous']="&laquo; vorheriges";
$strings['next']="n&auml;chstes &raquo;";
$strings['click_to_zoom']=" &raquo; Klick zum vergr&ouml;ssern ->";
$strings['click_to_go_back']="<- zur&uuml;ck zur &Uuml;bersicht";
$strings['click_to_open_original']= "in Originalgr&ouml;sse &ouml;ffnen";
$strings['back_to_normal_view'] = "zur&uuml;ck zur normalen Ansicht";
$strings['index']="&Uuml;bersicht";
$strings['go_to_gallery']="zur Galerie ->";
$strings['pictures_successfully_uploaded']="<b>Bilder erfolgreich hochgeladen.</b>";
$strings['changes_have_been_saved']="<b>&Auml;nderungen erfolgreich gespeichert.</b>";
$strings['delete']="L&ouml;schen";
$strings['title']="Titel";
$strings['save_changes']="&Auml;nderungen speichern";
$strings['upload_pictures']="Bilder hochladen";
$strings['check_to_delete_this_picture']="zum L&ouml;schen markieren";
$strings['manage_pictures']="Bilder verwalten";
$strings['date']="Datum";
$strings['description']="Beschreibung";
$strings['position']="Position";
$strings['gallery_description']="Galerie-Beschreibung";
$strings['db_no_descr_support']="<em>Warnung: Galerie-Datenbank unterst&uuml;tzt keine Beschreibungen!</em><br />";
$strings['db_no_pos_support']="<em>Warnung: Galerie-Datenbank unterst&uuml;tzt keine Positionen f&uuml;r Bilder!</em><br />";
$strings['picture'] = "Bild:";
$strings['max_pics_reached'] = "Maximum an Bildern f&uuml;r diese Galerie erreicht.";
$strings['supported_types'] = "<b>falscher Datei-Typ!</b> Diese Galerie unterst&uuml;tzt folgende Datei-Typen: jpg/jpeg, png, gif and zip (welches erlaubte Datei-Typen enth&auml;lt)";
$strings['gif_not_supported'] = "<b>Die GD-Bibliothek auf diesem Server unterst&uuml;tzt keine Gif-Bilder.</b> Nur JPEG und PNG m&ouml;glich.";
$strings['supported_types_inzip'] = "Fehler: Das ZIP-Archiv enthielt ung&uuml;ltige Datei-Typen. Diese wurden ignoriert.";
$strings['max_pics_reached_some_discarded'] = "<b>Maximum an m&ouml;glichen Bildern wurde &uuml;berschritten!</b> M&ouml;glicherweise wurden einige der gesendeten Bilder ignoriert.";
$strings['zip_not_supported'] = "<b>Diese PHP-Installation unterst&uuml;tzt keine ZIP-Archive!</b> Mehr Informationen: <a href=\"http://www.php.net/manual/ref.zip.php\">PHP ZIP-Modul und ZZIPlib-Bibliothek.</a>";
$strings['database_error'] = "Fehler: Galerie-Datenbank konnte nicht erstellt werden!";
$strings['database_error_ownid'] = "Fehler: Das Feld own_id zur Benutzer-Zuordnung konnte nicht erstellt werden!";
$strings['invalid_class'] = "Error: class is invalid. Please check it.! ";
$strings['invalid_name_class'] = "Error: name class is invalid. ";
?>