<?php
/*
	Copyright (c) InterAKT Online 2000-2006. All rights reserved.
*/

class XML_Recordset {
	var $connection;
	var $source;
	var $timeout;
	var $tempFolder;
	var $xmlFormat;
	var $xmlEncoding;
	var $pageEncoding;
	var $internalEncoding;
	var $startRow;
	var $maxRows;
	var $totalRows;
	
	function XML_Recordset(&$connection) {
		$this->connection = $connection;
		$this->source = '';
		$this->timeout = 10;
		$this->tempFolder = dirname(realpath(__FILE__)) . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR;
		$this->xmlFormat = 'NODES';
		$this->xmlEncoding = 'UTF-8';
		$this->pageEncoding = 'UTF-8';
		$this->internalEncoding = 'UTF-8';
		$this->startRow = 0;
		$this->maxRows = 0;
	}

	function setSource($source) {
		$this->source = trim($source);
	}
	
	function setTimeout($timeout) {
		$this->timeout = abs(intval(trim($timeout)));
	}
	
	function setXMLFormat($xmlFormat) {
		$this->xmlFormat = strtoupper(trim($xmlFormat));
		if ($this->xmlFormat != 'ATTRIBUTES') {
			$this->xmlFormat = 'NODES';
		}
	}
	
	function setPageEncoding($pageEncoding) {
		$this->pageEncoding = strtoupper(trim($pageEncoding));
	}
	
	function setStartRow($startRow) {
		$this->startRow = abs(intval(trim($startRow)));
	}
	
	function setMaxRows($maxRows) {
		$this->maxRows = abs(intval(trim($maxRows)));
	}
	
	function getTotalRows() {
		return $this->totalRows;
	}
	
	function getRecordset() {
		$srcFile = $this->getCacheFileName();
		$this->checkTempFolder();
		$this->clearTempFolder();
		$content = &$this->getContent($srcFile);
		$dataArr = $this->parseXML($content);
		$fakeRS = new KT_FakeRecordset($this->connection);
		$rs = $fakeRS->getFakeRecordset($dataArr);
		if ($fakeRS->hasError) {
			die('Error creating XML Recordset <br />' . $fakeRS->getError());
		}
		return $rs;
	}
	
		function getCacheFileName() {
			return $this->tempFolder . md5($this->source) . '.cache';
		}
		
		function checkTempFolder() {
			$folder = new KT_folder();
			$folder->createFolder($this->tempFolder);
			if ($folder->hasError()) {
				$errObj = $folder->getError();
				die('Error writing in cache folder: ' . $errObj[1]);
			}
		}
		
		function clearTempFolder() {
			$folder = new KT_folder();
			$folderContent = $folder->readFolder($this->tempFolder);
			if ($folder->hasError()) {
				$errObj = $folder->getError();
				die('Error reading cache folder: ' . $errObj[1]);
			}
			clearstatcache();
			foreach($folderContent['files'] as $k => $file) {
				$props = @stat($this->tempFolder . $file['name']);
				if ( $props !== false && $props['size'] > 0 && $props['mtime'] + 60 * $this->timeout < time() ) {
					@unlink($this->tempFolder . $file['name']);
				}
			}
		}
		
		function &getContent($cacheFile) {
			$content = '';
			if (file_exists($cacheFile)) {
				$f = @fopen($cacheFile, 'rb');
				if (is_resource($f)) {
					if (flock($f, LOCK_EX)) {
						while (!feof($f)) {
							$content .= fread($f, 8192);
						}
						flock($f, LOCK_UN);
						fclose($f);
					} else {
						die('Error obtaining a lock on the cache file: ' . $cacheFile);
					}
				} else {
					die('Error opening cache file: ' . $cacheFile);
				}
				return $content;
			}
			
			if ( strpos($this->source, '://') !== false ) {
				$content = &$this->getContentFromURI();
			} else {
				$content = &$this->getContentFromFile();
			}
			
			// TODO : caching only for remote files ?
			if (trim($content) !== '' && $this->timeout > 0) {
				$f = @fopen($cacheFile, 'wb');
				if (is_resource($f)) {
					if (flock($f, LOCK_EX)) {
						fwrite($f, $content);
						flock($f, LOCK_UN);
						fclose($f);
					} else {
						die('Error obtaining a lock on the cache file: ' . $cacheFile);
					}
				} else {
					die('Error writing cache file: ' . $cacheFile);
				}
			}
			
			return $content;
		}
		
			function &getContentFromFile() {
				$buf = '';
				$f = @fopen(KT_realpath($this->source, false), 'rb');
				if (is_resource($f)) {
					if (flock($f, LOCK_EX)) {
						$buf = fread($f, filesize(KT_realpath($this->source, false)));
						flock($f, LOCK_UN);
						fclose($f);
					} else {
						die('Error obtaining a lock on the file: ' . $this->source);
					}
				} else {
					die('Error opening file: ' . $this->source);
				}
				return $buf;
			}
		
			function &getContentFromURI() {
				$buf = '';
				if (ini_get('allow_url_fopen')) {
					$f = @fopen($this->source, 'rb');
					if (is_resource($f)) {
						while (!feof($f)) {
							$buf .= fread($f, 8192);
						}
						fclose($f);
					} else {
						die('Error opening: ' . $this->source);
					}
				} else {
					die('Please enable <strong>allow_url_fopen</strong> in php.ini.');
				}
				return $buf;
			}
	
		function &parseXML(&$xmlContent) {
			$this->depth = 0;
			$this->depthArr = array();
			$data = array();
			
			if (preg_match("/<\?xml.*?encoding\s*=\s*[\"'](.*?)[\"'].*?\?>/is", $xmlContent, $matches)) {
				$this->xmlEncoding = strtoupper($matches[1]);
			}
			$xmlContent = preg_replace('/<\?xml.*\?>/is', '', $xmlContent);
			
			$expatEncodings = array('ISO-8859-1', 'UTF-8');
			if (!in_array($this->xmlEncoding, $expatEncodings) || !in_array($this->pageEncoding, $expatEncodings)) {
				// we need mbstring enabled
				$this->internalEncoding = 'UTF-8';
				$ret = $this->checkMBstring();
				$xmlContent = $this->convertEncodings($xmlContent, $this->xmlEncoding, $this->internalEncoding);
			} else {
				if ($this->xmlEncoding == 'ISO-8859-1' && $this->pageEncoding == 'ISO-8859-1') {
					$this->internalEncoding = 'ISO-8859-1';
				} else {
					$this->internalEncoding = 'UTF-8';
					$xmlContent = $this->convertEncodings($xmlContent, $this->xmlEncoding, $this->internalEncoding);
				}
			}
			
			$xmlParser = xml_parser_create($this->internalEncoding);
			xml_set_object($xmlParser, $this);
			xml_parser_set_option($xmlParser, XML_OPTION_CASE_FOLDING, 0);
			xml_set_element_handler($xmlParser, "tag_open", "tag_close");
			xml_set_character_data_handler($xmlParser, "cdata");
			$error = xml_parse($xmlParser, $xmlContent, true);
			if ($error == false) {
				die(xml_error_string(xml_get_error_code($xmlParser)) . xml_get_current_line_number($xmlParser));
			}
			xml_parser_free($xmlParser);
			
			if ($this->xmlFormat == 'NODES') {
				$multiple = (count($this->depthArr[0]['child'])>1?true:false);
				foreach ($this->depthArr[0]['child'] as $i => $row) {
					foreach ($row['child'] as $j => $header) {
						if ($multiple === true) {
							$data[$header['name']][$i] = $this->convertEncodings($header['text'], $this->internalEncoding, $this->pageEncoding);
						} else {
							$data[$header['name']] = $this->convertEncodings($header['text'], $this->internalEncoding, $this->pageEncoding);
						}
					}
				}
			} else {
				foreach ($this->depthArr[0]['child'] as $i => $row) {
					$multiple = (count($this->depthArr[0]['child'])>1?true:false);
					foreach ($row['attrs'] as $header => $value) {
						if ($multiple === true) {
							$data[$header][$i] = $this->convertEncodings($value, $this->internalEncoding, $this->pageEncoding);
						} else {
							$data[$header] = $this->convertEncodings($value, $this->internalEncoding, $this->pageEncoding);
						}
					}
				}
			}
			
			unset($this->depth);
			unset($this->depthArr);
			
			foreach ($data as $k => $dataArr) {
				if (!isset($this->totalRows)) {
					$this->totalRows = count($dataArr);
				}
				if ($this->startRow > 0) {
					$data[$k] = array_slice($dataArr, $this->startRow, $this->maxRows);
					if (count($data[$k]) == 0) {
						$data[$k] = '';
					}
				}
			}
			
			return $data;
		}
		
			function tag_open($parser, $node, $attributes) {
				$this->depth++;
				if ($this->depth > 3) {
					return;
				}
				array_push($this->depthArr, array('name' => $node, 'attrs' => $attributes , 'text' => '', 'child' => array()));
			}
			
			function cdata($parser, $cdata) {
				if ($this->depth > 3) {
					return;
				}
				$this->depthArr[count($this->depthArr) - 1]['text'] .= $cdata;
			}
			
			function tag_close($parser, $node) {
				if ($this->depth > 3 || $this->depth == 1) {
					return;
				}
				$tmpNode = array_pop($this->depthArr);
				$this->depthArr[count($this->depthArr) - 1]['child'][] = $tmpNode;
				$this->depth--;
			}
			
			function checkMBstring() {
				if (!function_exists('mb_convert_encoding')) {
					die('<strong>XML_Recordset Error.</strong><br/>Could not perform characters conversion. Function <strong>mb_convert_encoding</strong> is not available! Please enable the <strong>mbstring</strong> extension!');
				}
			}
			
			function convertEncodings($value, $inEncoding, $outEncoding) {
				if ($inEncoding != $outEncoding) {
					if ($inEncoding == 'ISO-8859-1' && $outEncoding == 'UTF-8') {
						$value = utf8_encode($value);
					} elseif ($inEncoding == 'UTF-8' && $outEncoding == 'ISO-8859-1') {
						$value = utf8_decode($value);
					} else {
						$value = mb_convert_encoding($value, $outEncoding, $inEncoding);
					}
				}
				return $value;
			}
			
}

?>