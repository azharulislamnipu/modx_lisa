<?php

require_once 'ClickatellText.php';
require_once 'ClickatellUtils.php';
require_once 'ClickatellService.php';
require_once 'ClickatellMessageBean.php';
require_once '/opt/lampp/htdocs/modx_lisa/home/index.php';
define('MODX_API_MODE', true); // Allows this class to use the MODx API

/**
 * Handles the database interaction for the Clickatell screens.
 * Author: Pat Heard {http://fullahead.org}
 */
 
class ClickatellDatabaseManager {
	
        // Keys for the distribution lists
        const DIST_LIST_BOULES = 'boules';
        const DIST_LIST_REGIONS = 'regions';
        const DIST_LIST_OFFICERS = 'officers';
    
	private $modx = null;
	
	/** Initialize the database manager */
	public function __construct(&$modx = null){
		if($modx == null){
			die('You must pass a valid $modx object reference to the ClickatellDatabaseManager.');
		}
		$this->modx = $modx; 
	}
	
	public function getMessages(array $args = array()){
	
		// Default search args
		$args = array_merge(array(
			'from' 			=> '',
			'to' 			=> '',
			'text' 			=> '',
			'sent' 			=> '',
			'sent_operator'	=> '=',
			'date_format'	=> 'M j, Y H:i'
		), $args);

		// Build WHERE clause
		$where = 
			(!empty($args['from']) 	? 'from LIKE "' . $this->modx->db->escape($args['from']) . '%" AND ' : '') .
			(!empty($args['to']) 	? 'to LIKE "' . $this->modx->db->escape($args['to']) . '%" AND ' : '') .
			(!empty($args['text']) 	? 'text LIKE "%' . $this->modx->db->escape($args['text']) . '%" AND ' : '') .
			(!empty($args['sent']) 	? 'sent ' . $this->modx->db->escape($args['sent_operator']) . ' "' . date( 'Y-m-d H:i:s', $this->modx->db->escape($args['sent'])) . '" AND ' : '') .
			'active_ind = "1"';
		
		// Select all messages from the database
		$res = $this->modx->db->select('*', $this->modx->getFullTableName('spp_txt_msg'),  $where, 'udh ASC');
		
		// Load the results into ClickatellMessageBeans
		if($this->modx->db->getRecordCount($res) >= 1) {
			$messages = array();
			while($row = $this->modx->db->getRow($res)){
				$messages[] = new ClickatellMessageBean(array(
					'id' 		=> intval($row['id']),
					'active_ind'	=> $row['active_ind'],	
					'date_format' 	=> $args['date_format'],	
					'from_post'	=> false,	
					'api_id'	=> $row['api_id'],	
					'moMsgId'	=> $row['msg_id'],	
					'moMsgId_parent'=> $row['msg_id_parent'],	
					'to'		=> $row['number_to'],	
					'from'		=> $row['number_from'],	
					'text'		=> $row['text'],	
					'charset'	=> $row['char_encoding'],	
					'udh'		=> $row['udh'],	
					'timestamp'	=> $row['sent']	
				));
			}
			
                        $service = new ClickatellService();
			$service->joinExtendedMessages($messages);
                        
                        // Lookup name based on "from" phone number
                        foreach($messages as $m){
                            $m->setName($this->getNameFromNumber($m->from, 'MOBILEPHONE'));
                        }                        
			return $messages;
		}
		return false;
		
	}
        
        /** 
         * Returns all members that are eligible to recieve a text message, grouped by their
         * Boule and region
         */
        public function getNameFromNumber($number, $numberField){

            $name = '';
            
            $number = $this->modx->db->escape($number);
            $numberFormatted = ClickatellUtils::format($number);
            if(!empty($number) && !empty($numberFormatted)){
            
                // Try to find a name based on the formatted and clean phone number
                $res = $this->modx->db->query("
                    SELECT 
                        FULLNAME
                    FROM 
                        spp_archons 
                    WHERE "
                        . $numberField . " IN ('" . $number . "', '" . $numberFormatted . "') " .
                    " LIMIT 1
                ");

                if($this->modx->db->getRecordCount($res) >= 1) {
                    $row = $this->modx->db->getRow($res);
                    $name = $row['FULLNAME'];
                }
            }
            return $name;             
        }
        
        /** Gets a count of active messages */
        public function getMessageCount(){
            return $this->modx->db->getValue( 
                $this->modx->db->select('count(*)', $this->modx->getFullTableName('spp_txt_msg'), 'active_ind = "1"')
            );
        }
	
	/** Deletes a message by marking it as inactive */
	public function deleteMessage($ids = null){
		if(isset($ids) && preg_match('/[0-9]+(,[0-9]+)*/', $ids)){
			return $this->modx->db->update( 'active_ind = "0"', $this->modx->getFullTableName('spp_txt_msg'), 'id IN (' . $this->modx->db->escape($ids) . ')' );
		}
		return false;
	}
	
	/** Inserts a message to the database from a $_POST request */
	public function insertMessageFromPost(){
	
		// Do we have the required data?
		if(isset($_POST['moMsgId'])){
		
			// Check the message doesn't already exist
			$res = $this->modx->db->select('msg_id', $this->modx->getFullTableName('spp_txt_msg'),  'msg_id = "' . $this->modx->db->escape($_POST['moMsgId']) . '"');
			if($this->modx->db->getRecordCount($res) == 0) {		
				$message = new ClickatellMessageBean(array(
					'active_ind' 	=> 1,
					'from_post'		=> true
				));
				return $this->insertMessage($message);			
			} else {
				return true; // no insert required - message already in the database
			}
		}
		return false;
	
	}
	
        /** 
         * Returns all members that are eligible to recieve a text message, grouped by their
         * Boule and region
         */
        public function getDistListBoules(){

            // Select Boule names that have at least one valid member for a text message
            $res = $this->modx->db->query("
                SELECT 
                    a.CHAPTERID,
                    a.MOBILEPHONE,
                    b.REGIONNAME
                FROM 
                    spp_archons a 
                JOIN
                    spp_boule b
                ON
                    a.CHAPTERID = b.BOULENAME
                WHERE "
                    . $this->getWhereDistListEligible() .
                "ORDER BY 
                    CHAPTERID DESC
            ");

            // Load the Boule names
            if($this->modx->db->getRecordCount($res) >= 1) {
                $numbers = array(
                    self::DIST_LIST_BOULES  => array(),
                    self::DIST_LIST_REGIONS => array()
                );
                while($row = $this->modx->db->getRow($res)){
                                       
                    // Clean and validate phone number.  If good, add to Boule and region maps
                    $mobile = ClickatellUtils::cleanPhoneNumber($row['MOBILEPHONE']);
                    $mobile = ClickatellUtils::addLongDistanceInd($mobile); 
                    if(ClickatellUtils::isValidNumber($mobile)){
                        
                        // Boule map of phone numbers
                        $chapterId = $row['CHAPTERID'];                    
                        if(!isset($numbers[self::DIST_LIST_BOULES][$chapterId])){
                            $numbers[self::DIST_LIST_BOULES][$chapterId] = array();
                        }

                        // Region map of phone numbers
                        $regionName = $row['REGIONNAME'];
                        if(!isset($numbers[self::DIST_LIST_REGIONS][$regionName])){
                            $numbers[self::DIST_LIST_REGIONS][$regionName] = array();
                        }                        
                        
                        $numbers[self::DIST_LIST_BOULES][$chapterId][] = $mobile;
                        $numbers[self::DIST_LIST_REGIONS][$regionName][] = $mobile;
                    }
                }
                return $numbers;
            }
            return false;             
        }
        
        
        /** Returns a distribution list for officers */
        public function getDistListOfficers(){                   
                
            $res = $this->modx->db->query("
                SELECT
                    o.CPOSITION,
                    a.MOBILEPHONE
                FROM 
                    spp_archons a
                JOIN
                    spp_officers o
                ON
                    a.CUSTOMERID = o.CUSTOMERCD
                WHERE " .
                    $this->getWhereDistListEligible() . "
                    AND COMMITTEESTATUSSTT = 'Active' 
                    AND o.CPOSITION IN ('" . implode("','", Officer::cposition()) . "') " .                         
                "ORDER BY
                    CPOSITION"
            );

            // Load the numbers by position type
            if($this->modx->db->getRecordCount($res) >= 1) {

                $numbers = array();
                $curr_cposition = '';
                $curr_type = '';

                while($row = $this->modx->db->getRow($res)){

                    // Clean and validate phone number.  If good, add to officer type map
                    $mobile = ClickatellUtils::cleanPhoneNumber($row['MOBILEPHONE']);
                    $mobile = ClickatellUtils::addLongDistanceInd($mobile);                    
                    if(ClickatellUtils::isValidNumber($mobile)){

                        // Track the currently view CPOSITION and officer type    
                        if($curr_cposition !== $row['CPOSITION']){
                            $curr_cposition = $row['CPOSITION'];
                            $curr_type = Officer::typeFromCposition($curr_cposition);
                        }

                        // If we've got a valid officer type, add the phone number
                        if(!empty($curr_type)){
                            if(!isset($numbers[$curr_type])){
                                $numbers[$curr_type] = array();
                            }                                
                            $numbers[$curr_type][] = $mobile;
                        }
                    }
                }
                return $numbers;
            }
            return false;
        } 

        
        /** Where clause condition that marks an Archon as eligible for being on a distribution list */
        private function getWhereDistListEligible(){
            return "MOBILEPHONE IS NOT NULL " .
                   "AND ( " . 
                   "  CUSTOMERTYPE IN ('Member', 'Emeritus') " .
                   "  AND STATUSSTT = 'Active' " .
                   "  OR ( " .
                   "     STATUSSTT = 'Inactive' " .
                   "     AND CUSTOMERCLASSSTT = 'Voluntary' " .
                   "  ) " .
                   ")"; 
        }        
        	
	/** Inserts a message to the database */
	private function insertMessage($message = null){

		if(isset($message) && $message->isInsertable()){			
			$fields = array(
				'api_id'	=> $this->modx->db->escape($message->api_id),
				'msg_id'	=> $this->modx->db->escape($message->moMsgId),
				'msg_id_parent'	=> $this->modx->db->escape($message->moMsgId_parent),
				'number_to'	=> $this->modx->db->escape($message->to),
				'number_from'	=> $this->modx->db->escape($message->from),
				'text'		=> $this->modx->db->escape($message->text),
				'char_encoding'	=> $this->modx->db->escape($message->charset),
				'udh'		=> $this->modx->db->escape($message->udh),
				'sent'		=> $this->modx->db->escape($message->timestamp),
				'active_ind'	=> $this->modx->db->escape($message->active_ind),
			);
			return $this->modx->db->insert($fields, $this->modx->getFullTableName('spp_txt_msg'));
		}
		return false;
	}	        

};


/**
 * Constant officer types 
 */
final class Officer {    
    
    const SireArchons = 'sire archons';
    const Grammati = 'grammati';
    const Thesauristes = 'thesauristes';
    const Grapters = 'grapters';
    
    // CPOSITION values in spp_officers for the officer types
    public static function cposition(){
        return array(      
            'Sire Archon','Regional Sire Archon',       
            'Grammateus','Regional Grammateus',
            'Thesauristes','Regional Thesauristes',
            'Grapter','Regional Grapter'
        );
    }
    
    public static function typeFromCposition($cposition){
        
        switch($cposition){
            case 'Sire Archon':
            case 'Regional Sire Archon':
                return Officer::SireArchons;
                
            case 'Grammateus':
            case 'Regional Grammateus':
                return Officer::Grammati;
                
            case 'Thesauristes':
            case 'Regional Thesauristes':
                return Officer::Thesauristes;
                
            case 'Grapter':
            case 'Regional Grapter':
                return Officer::Grapters;                

        }
        return '';
    }
    
    private function __construct(){}

};


?>