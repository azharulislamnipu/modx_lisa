<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_sigma_remote = new KT_connection($sigma_remote, $database_sigma_remote);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listrsArchons3 = new TFI_TableFilter($conn_sigma_remote, "tfi_listrsArchons3");
$tfi_listrsArchons3->addColumn("FIRSTNAME", "STRING_TYPE", "FIRSTNAME", "%");
$tfi_listrsArchons3->addColumn("LASTNAME", "STRING_TYPE", "LASTNAME", "%");
$tfi_listrsArchons3->addColumn("BOULENAME", "STRING_TYPE", "BOULENAME", "%");
$tfi_listrsArchons3->addColumn("CITY", "STRING_TYPE", "CITY", "%");
$tfi_listrsArchons3->addColumn("STATECD", "STRING_TYPE", "STATECD", "%");
$tfi_listrsArchons3->addColumn("EMAIL", "STRING_TYPE", "EMAIL", "%");
$tfi_listrsArchons3->addColumn("HOMEPHONE", "STRING_TYPE", "HOMEPHONE", "%");
$tfi_listrsArchons3->addColumn("JOBTITLE", "STRING_TYPE", "JOBTITLE", "%");
$tfi_listrsArchons3->addColumn("CPOSITION", "STRING_TYPE", "CPOSITION", "%");
$tfi_listrsArchons3->addColumn("WEB_ID", "NUMERIC_TYPE", "WEB_ID", "=");
$tfi_listrsArchons3->Execute();

// Sorter
$tso_listrsArchons3 = new TSO_TableSorter("rsArchons", "tso_listrsArchons3");
$tso_listrsArchons3->addColumn("FIRSTNAME");
$tso_listrsArchons3->addColumn("LASTNAME");
$tso_listrsArchons3->addColumn("BOULENAME");
$tso_listrsArchons3->addColumn("CITY");
$tso_listrsArchons3->addColumn("STATECD");
$tso_listrsArchons3->addColumn("EMAIL");
$tso_listrsArchons3->addColumn("HOMEPHONE");
$tso_listrsArchons3->addColumn("JOBTITLE");
$tso_listrsArchons3->addColumn("CPOSITION");
$tso_listrsArchons3->addColumn("WEB_ID");
$tso_listrsArchons3->setDefault("FIRSTNAME");
$tso_listrsArchons3->Execute();

// Navigation
$nav_listrsArchons3 = new NAV_Regular("nav_listrsArchons3", "rsArchons", "../", $_SERVER['PHP_SELF'], 25);

//NeXTenesio3 Special List Recordset
$maxRows_rsArchons = $_SESSION['max_rows_nav_listrsArchons3'];
$pageNum_rsArchons = 0;
if (isset($_GET['pageNum_rsArchons'])) {
  $pageNum_rsArchons = $_GET['pageNum_rsArchons'];
}
$startRow_rsArchons = $pageNum_rsArchons * $maxRows_rsArchons;

$colname_rsArchons = "-1";
if (isset($_GET['LASTNAME'])) {
  $colname_rsArchons = $_GET['LASTNAME'];
}
// Defining List Recordset variable
$NXTFilter_rsArchons = "1=1";
if (isset($_SESSION['filter_tfi_listrsArchons3'])) {
  $NXTFilter_rsArchons = $_SESSION['filter_tfi_listrsArchons3'];
}
// Defining List Recordset variable
$NXTSort_rsArchons = "LASTNAME";
if (isset($_SESSION['sorter_tso_listrsArchons3'])) {
  $NXTSort_rsArchons = $_SESSION['sorter_tso_listrsArchons3'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_rsArchons = sprintf("SELECT * FROM vw_web_elig WHERE LASTNAME = %s AND  {$NXTFilter_rsArchons} ORDER BY {$NXTSort_rsArchons} ", GetSQLValueString($colname_rsArchons, "text"));
$query_limit_rsArchons = sprintf("%s LIMIT %d, %d", $query_rsArchons, $startRow_rsArchons, $maxRows_rsArchons);
$rsArchons = mysql_query($query_limit_rsArchons, $sigma_modx) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);

if (isset($_GET['totalRows_rsArchons'])) {
  $totalRows_rsArchons = $_GET['totalRows_rsArchons'];
} else {
  $all_rsArchons = mysql_query($query_rsArchons);
  $totalRows_rsArchons = mysql_num_rows($all_rsArchons);
}
$totalPages_rsArchons = ceil($totalRows_rsArchons/$maxRows_rsArchons)-1;
//End NeXTenesio3 Special List Recordset

$nav_listrsArchons3->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_FIRSTNAME {width:70px; overflow:hidden;}
  .KT_col_LASTNAME {width:70px; overflow:hidden;}
  .KT_col_BOULENAME {width:105px; overflow:hidden;}
  /*.KT_col_CITY {width:56px; overflow:hidden;}*/
  .KT_col_STATECD {width:28px; overflow:hidden;}
  .KT_col_EMAIL {width:155px;; overflow:hidden;}
  .KT_col_HOMEPHONE {width:100px; overflow:hidden;}
  .KT_col_JOBTITLE {width:105px; overflow:hidden;}
  .KT_col_CPOSITION {width:70px; overflow:hidden;}
  .KT_col_WEB_ID {width:35px; overflow:hidden;}
</style><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="pageWide">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width982">
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listrsArchons3">
  <h1>SEARCH BY LASTNAME =  <?php echo $_GET['CITY']; ?> | 
     DISPLAYING  <?php
  $nav_listrsArchons3->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
    </h1>
    <div class="KT_tnglist">
      <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
        <div class="KT_options"> <a href="<?php echo $nav_listrsArchons3->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
          <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listrsArchons3'] == 1) {
?>
            <?php echo $_SESSION['default_max_rows_nav_listrsArchons3']; ?>
            <?php 
  // else Conditional region1
  } else { ?>
            <?php echo NXT_getResource("all"); ?>
            <?php } 
  // endif Conditional region1
?>
              <?php echo NXT_getResource("records"); ?></a> &nbsp;
          &nbsp;
                <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listrsArchons3'] == 1) {
?>
                  <a href="<?php echo $tfi_listrsArchons3->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                  <?php 
  // else Conditional region2
  } else { ?>
                  <a href="<?php echo $tfi_listrsArchons3->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                  <?php } 
  // endif Conditional region2
?>
        </div>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <thead>
            <tr class="KT_row_order">
              <th id="FIRSTNAME" class="KT_sorter KT_col_FIRSTNAME <?php echo $tso_listrsArchons3->getSortIcon('FIRSTNAME'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('FIRSTNAME'); ?>">FIRST NAME</a> </th>
              <th id="LASTNAME" class="KT_sorter KT_col_LASTNAME <?php echo $tso_listrsArchons3->getSortIcon('LASTNAME'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('LASTNAME'); ?>">LAST NAME</a> </th>
              <th id="BOULENAME" class="KT_sorter KT_col_BOULENAME <?php echo $tso_listrsArchons3->getSortIcon('BOULENAME'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('BOULENAME'); ?>">BOUL&Eacute;</a> </th>
              <th id="CITY" class="KT_sorter KT_col_CITY <?php echo $tso_listrsArchons3->getSortIcon('CITY'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('CITY'); ?>">CITY</a> </th>
              <th id="STATECD" class="KT_sorter KT_col_STATECD <?php echo $tso_listrsArchons3->getSortIcon('STATECD'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('STATECD'); ?>">STATE</a> </th>
              <th id="EMAIL" class="KT_sorter KT_col_EMAIL <?php echo $tso_listrsArchons3->getSortIcon('EMAIL'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('EMAIL'); ?>">EMAIL</a> </th>
              <th id="HOMEPHONE" class="KT_sorter KT_col_HOMEPHONE <?php echo $tso_listrsArchons3->getSortIcon('HOMEPHONE'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('HOMEPHONE'); ?>">HOME PHONE</a> </th>
              <th id="JOBTITLE" class="KT_sorter KT_col_JOBTITLE <?php echo $tso_listrsArchons3->getSortIcon('JOBTITLE'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('JOBTITLE'); ?>">TITLE</a> </th>
              <th id="CPOSITION" class="KT_sorter KT_col_CPOSITION <?php echo $tso_listrsArchons3->getSortIcon('CPOSITION'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('CPOSITION'); ?>">OFFICE</a> </th>
              <th id="WEB_ID" class="KT_sorter KT_col_WEB_ID <?php echo $tso_listrsArchons3->getSortIcon('WEB_ID'); ?>"> <a href="<?php echo $tso_listrsArchons3->getSortLink('WEB_ID'); ?>">PROFILE</a> </th>
            </tr>
            <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listrsArchons3'] == 1) {
?>
              <tr class="KT_row_filter">
                <td><input type="text" name="tfi_listrsArchons3_FIRSTNAME" id="tfi_listrsArchons3_FIRSTNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_FIRSTNAME']); ?>" size="10" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_LASTNAME" id="tfi_listrsArchons3_LASTNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_LASTNAME']); ?>" size="10" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_BOULENAME" id="tfi_listrsArchons3_BOULENAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_BOULENAME']); ?>" size="10" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_CITY" id="tfi_listrsArchons3_CITY" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_CITY']); ?>" size="10" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_STATECD" id="tfi_listrsArchons3_STATECD" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_STATECD']); ?>" size="2" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_EMAIL" id="tfi_listrsArchons3_EMAIL" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_EMAIL']); ?>" size="15" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_HOMEPHONE" id="tfi_listrsArchons3_HOMEPHONE" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_HOMEPHONE']); ?>" size="15" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_JOBTITLE" id="tfi_listrsArchons3_JOBTITLE" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_JOBTITLE']); ?>" size="15" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_CPOSITION" id="tfi_listrsArchons3_CPOSITION" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_CPOSITION']); ?>" size="10" maxlength="20" /></td>
                <td><input type="submit" name="tfi_listrsArchons3" value="<?php echo NXT_getResource("Filter"); ?>" />&nbsp;</td>
              </tr>
              <?php } 
  // endif Conditional region3
?>
          </thead>
          <tbody>
            <?php if ($totalRows_rsArchons == 0) { // Show if recordset empty ?>
              <tr>
                <td colspan="12"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
              </tr>
              <?php } // Show if recordset empty ?>
            <?php if ($totalRows_rsArchons > 0) { // Show if recordset not empty ?>
              <?php do { ?>
                <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                  <td style="white-space:normal;"><div class="KT_col_FIRSTNAME"><?php echo KT_FormatForList($row_rsArchons['FIRSTNAME'], 15); ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_LASTNAME"><?php echo KT_FormatForList($row_rsArchons['LASTNAME'], 15); ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_BOULENAME"><?php echo KT_FormatForList($row_rsArchons['BOULENAME'], 25); ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_CITY"><?php echo $row_rsArchons['CITY']; ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_STATECD"><?php echo $row_rsArchons['STATECD']; ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_EMAIL"><?php echo $row_rsArchons['EMAIL']; ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_HOMEPHONE"><?php echo KT_FormatForList($row_rsArchons['HOMEPHONE'], 20); ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_JOBTITLE"><?php echo $row_rsArchons['JOBTITLE']; ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_CPOSITION"><?php echo KT_FormatForList($row_rsArchons['CPOSITION'], 100); ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_WEB_ID"><a href="/services/directory/results.php?id=<?php echo $row_rsArchons['WEB_ID']; ?>">VIEW</a></div></td>
                <?php } while ($row_rsArchons = mysql_fetch_assoc($rsArchons)); ?>
              <?php } // Show if recordset not empty ?>
          </tbody>
        </table>
        <div class="KT_bottomnav">
          <div>
            <?php
            $nav_listrsArchons3->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
          </div>
        </div>
        </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchons);
?>
