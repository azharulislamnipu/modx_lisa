<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * TiltViewer Image
 *
 * @package svManager
 */
class TvImage extends Image
{  
 /**
  * @var string thumb path relative to gallery
  */
  var $thumbPathRelGallery = '';
  
 /**
  * @var string link url
  */
  var $linkUrl = TV_LINK_URL;
  
 /**
  * @var string link html target attribute
  */
  var $linkTarget = '_self';
  
 /**
  * @var string imageTitle'=>'Image 1', 'imageDescription'=>'First image', 'showFlipButton'=>true
  */
  var $imageTitle = TV_IMAGE_TITLE;
  
 /**
  * @var string image description
  */
  var $imageDescription = TV_IMAGE_DESCRIPTION;
  
 /**
  * @var boolean show flip button
  */
  var $showFlipButton = TV_SHOW_FLIP_BUTTON;
  
 /**
  * constructs TvImage class
  *
  * @param string  gallery path rel svm passed by reference in case it changes
  * @param string image path rel gallery
  * @param string thumb path rel gallery
  * @param string image title
  * @param string image description
  * @param string link url
  * @param boolean show flip button
  */
  function TvImage(&$galleryPathRelSvm, $imagePath, $thumbPath, $imageTitle='', $imageDescription='', $linkUrl='', $showFlipButton='default')
  {
    $this->galleryPathRelSvm = &$galleryPathRelSvm;
    $this->thumbPathRelGallery = $thumbPath;
    $this->imagePathRelGallery = $imagePath;
    if ($imageTitle != '') $this->imageTitle = $imageTitle;
    if ($imageDescription != '') $this->imageDescription = $imageDescription;
    if ($linkUrl != '') $this->linkUrl = $linkUrl;
    if ($showFlipButton != 'default') $this->showFlipButton = $showFlipButton;
  }
  
 /**
  * get title
  *
  * @access public
  * @return string
  */
  function getImageTitle()
  {
    return $this->imageTitle;
  }
  
 /**
  * get description
  *
  * @access public
  * @return string
  */
  function getImageDescription()
  {
    return $this->imageDescription;
  }

 /**
  * get image link
  *
  * @access public
  * @return string
  */
  function getLinkUrl()
  {
    return $this->linkUrl;
  }
  
 /**
  * get show flip button
  *
  * @access public
  * @return boolean
  */
  function getShowFlipButton()
  {
    return $this->showFlipButton;
  }


  
 /**
  * Set image caption
  *
  * @access public
  * @return void
  * @param string caption text
  * @param string acceptable html tags
  */
  function setCaption($title, $description, $linkUrl, $showFlipButton, $i, $okTags)
  {
    $title = strip_tags($title);
    $title = str_replace('%', '%%', $title);
    $title = str_replace('%%1$', '%1$', $title);
    $title = str_replace('%%2$', '%2$', $title);
    $title = str_replace('{count}', '%1$s', $title);
    $title = str_replace('{file}', '%2$s', $title);
    $this->imageTitle = sprintf($title, $i+1, $this->getCleanImageFileName());
    $description = strip_tags($description, $okTags);
    $description = str_replace('%', '%%', $description);
    $description = str_replace('%%1$', '%1$', $description);
    $description = str_replace('%%2$', '%2$', $description);
    $description = str_replace('{count}', '%1$s', $description);
    $description = str_replace('{file}', '%2$s', $description);
    $this->imageDescription = sprintf($description, $i+1, $this->getCleanImageFileName());
    $linkUrl = strip_tags($linkUrl);
    $linkUrl = str_replace('%', '%%', $linkUrl);
    $linkUrl = str_replace('%%1$', '%1$', $linkUrl);
    $linkUrl = str_replace('%%2$', '%2$', $linkUrl);
    $linkUrl = str_replace('{count}', '%1$s', $linkUrl);
    $linkUrl = str_replace('{file}', '%2$s', $linkUrl);
    $this->linkUrl = sprintf($linkUrl, $i+1, $this->getImageFileName());
    $this->showFlipButton = $showFlipButton;
  }
}
?>
