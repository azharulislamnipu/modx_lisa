<?php

/**
 * Submits a transaction to Authorize.net for processing
 * Author: Pat Heard {http://fullahead.org}
 */

//require_once '/opt/lampp/htdocs/modx_lisa/home/assets/snippets/anet/AuthorizeNet.php';
require_once 'anet_php_sdk/AuthorizeNet.php';
require_once 'AuthorizeNetECheckInfo.php';

define("AUTHORIZENET_API_LOGIN_ID", "8bfd81d0339a");  		// Your API LOGIN ID
define("AUTHORIZENET_TRANSACTION_KEY","626XNSc8Hg49aYdq");	// Your API transaction key
define("AUTHORIZENET_SANDBOX", false);       			// Set to false to test against production
define("TEST_REQUEST", "TRUE");           			// Set to true if testing against production

/**
 * Process an Authorize.net Payment
 */
class AuthorizeNetProcessECheckPayment{

        const TRANSACTION_LIMIT = 60000.00;

	public $eCheckInfo = null;
        public $transaction_limit;

	public function __construct($eCheckInfo, $transaction_limit = AuthorizeNetProcessECheckPayment::TRANSACTION_LIMIT) {
		if (AUTHORIZENET_API_LOGIN_ID === '') {
			die('Enter your merchant credentials in AuthorizeNetPayment before running the app.');
		}
		$this->eCheckInfo = new AuthorizeNetECheckInfo($eCheckInfo);
                $this->transaction_limit = $transaction_limit;
	}

	public function getECheckInfo(){
		return $this->eCheckInfo;
	}

	/** Validates that the info is present and correct for an Authorize.net transaction */
	public function validate(){
		return $this->eCheckInfo->validate();
	}

	/** Process the payment */
	public function process($database, $update_data){

                $response = array();
                $amount = $this->eCheckInfo->x_amount;
                $is_multiple = $amount > $this->transaction_limit;

                do {
                    $response[] = $this->createTransaction($amount > $this->transaction_limit ? $this->transaction_limit : $amount, $is_multiple);
                    $amount -= $this->transaction_limit;
                } while($amount > 0);


                // Determine the outcome of the transaction processing and update the database
                $msg = count($response) > 1 ? '<li class="no-icon"><strong>To process your payment, it was split into the following ' . count($response) .  ' transactions:</strong></li>' : '';
                foreach($response as $r) {
                    $status = $this->getResponseStatus($r);
                    $msg .= '<li class="'. $status['css'] . '">'.$status['msg'].'</li>';

                    // Record success and warning transactions to spp_dues
                    if($status['css'] !== 'error') {
                        $update_data['transaction_id'] = $status['transaction_id'];
                        $database->updateTransactionId($update_data);
                    }
                }

                // Let the user know what happened
                return $msg;
	}

        /**
         * Returns the status for a given transaction
         * @param type $response
         * @return boolean
         */
        private function getResponseStatus($response) {

            $status = array();
            if($response->approved) {
                $status = array (
                    'css' => 'success',
                    'msg' => 'Your payment of $' . number_format($response->amount,0) . ' was successful.  You will receive an email receipt shortly.',
                    'transaction_id' => $response->transaction_id
                );
            } else if ($response->held) {
                $status = array (
                    'css' => 'warning',
                    'msg' => 'Your payment of $' . number_format($response->amount,0) . ' has been held for review.  The Office of the Grand Boul&eacute; will contact you if there are any problems.'
                );
            } else {
                $status = array (
                    'css' => 'error',
                    'msg' => 'There was a problem processing your payment: ' . htmlentities($response->response_reason_text) . ' (' . htmlentities($response->response_reason_code) . '-' . htmlentities($response->response_code) . ')'
                );
            }
            return $status;
        }

        /**
         * Creates the Authorize.net AIM transaction
         * @param type $amount Amount to charge
         * @param type $is_multiple Is this part of a series of multiple transactions?
         * @return type
         */
        private function createTransaction($amount, $is_multiple = false){
                $transaction = new AuthorizeNetAIM();
                $transaction->setSandbox(AUTHORIZENET_SANDBOX);

                $this->eCheckInfo->setAmount($amount);
                $transaction->setFields($this->eCheckInfo->getTransactionFields());
                $transaction->setField('duplicate_window', $is_multiple ? 0 : 120);

                return $transaction->authorizeAndCapture();
        }
}

?>