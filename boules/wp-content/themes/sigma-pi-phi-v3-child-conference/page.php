<?php get_header() ?>

    <div id="content">
        <div class="padder">

        <?php do_action( 'sigma_before_blog_page' ) ?>

        <div class="page" id="blog-page">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <?php // Check that the user is allowed to view the content
                      $can_view_content = can_view_content(); ?>
                <?php if ( $can_view_content ) : ?>
                    <h2 class="pagetitle"><?php the_title(); ?></h2>
                    <div class="post" id="post-<?php the_ID(); ?>">
                        <div class="contentBlock">
                            <div class="entry">
                                <?php the_content( __( '<p class="serif">Read the rest of this page &rarr;</p>', 'archon-network' ) ); ?>
                                <?php wp_link_pages( array( 'before' => __( '<p><strong>Pages:</strong> ', 'archon-network' ), 'after' => '</p>', 'next_or_number' => 'number')); ?>
                                <?php edit_post_link( __( 'Edit this entry.', 'archon-network' ), '<p>', '</p>'); ?>
                            </div>
                        </div>
                    </div>

                <?php else: ?>
                    <h2 class="pagetitle"><?php _e( 'Unauthorized', 'archon-network' ); ?></h2>
                    <div class="post" id="post-<?php the_ID(); ?>">
                        <div class="contentBlock">
                            <div class="entry">
                                <p><?php _e( 'You are not allowed to view this content.', 'archon-network' ); ?></p>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>


            <?php endwhile; endif; ?>

            <?php if ( $can_view_content ) : ?>
                 <?php the_content(); ?>
            <?php endif; ?>

        </div><!-- .page -->

        <?php do_action( 'sigma_after_blog_page' ) ?>

        </div><!-- .padder -->
    </div><!-- #content -->

    <?php locate_template( array( 'sidebar.php' ), true ) ?>

<?php get_footer(); ?>
