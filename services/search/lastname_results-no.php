<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listrsLastname4 = new TFI_TableFilter($conn_sigma_modx, "tfi_listrsLastname4");
$tfi_listrsLastname4->addColumn("FIRSTNAME", "STRING_TYPE", "FIRSTNAME", "%");
$tfi_listrsLastname4->addColumn("LASTNAME", "STRING_TYPE", "LASTNAME", "%");
$tfi_listrsLastname4->addColumn("BOULENAME", "STRING_TYPE", "BOULENAME", "%");
$tfi_listrsLastname4->addColumn("CITY", "STRING_TYPE", "CITY", "%");
$tfi_listrsLastname4->addColumn("STATECD", "STRING_TYPE", "STATECD", "%");
$tfi_listrsLastname4->addColumn("EMAIL", "STRING_TYPE", "EMAIL", "%");
$tfi_listrsLastname4->addColumn("HOMEPHONE", "STRING_TYPE", "HOMEPHONE", "%");
$tfi_listrsLastname4->addColumn("JOBTITLE", "STRING_TYPE", "JOBTITLE", "%");
$tfi_listrsLastname4->addColumn("CPOSITION", "STRING_TYPE", "CPOSITION", "%");
$tfi_listrsLastname4->addColumn("WEB_ID", "NUMERIC_TYPE", "WEB_ID", "=");
$tfi_listrsLastname4->Execute();

// Sorter
$tso_listrsLastname4 = new TSO_TableSorter("rsLastname", "tso_listrsLastname4");
$tso_listrsLastname4->addColumn("FIRSTNAME");
$tso_listrsLastname4->addColumn("LASTNAME");
$tso_listrsLastname4->addColumn("BOULENAME");
$tso_listrsLastname4->addColumn("CITY");
$tso_listrsLastname4->addColumn("STATECD");
$tso_listrsLastname4->addColumn("EMAIL");
$tso_listrsLastname4->addColumn("HOMEPHONE");
$tso_listrsLastname4->addColumn("JOBTITLE");
$tso_listrsLastname4->addColumn("CPOSITION");
$tso_listrsLastname4->addColumn("WEB_ID");
$tso_listrsLastname4->setDefault("FIRSTNAME");
$tso_listrsLastname4->Execute();

// Navigation
$nav_listrsLastname4 = new NAV_Regular("nav_listrsLastname4", "rsLastname", "../", $_SERVER['PHP_SELF'], 25);

//NeXTenesio3 Special List Recordset
$maxRows_rsLastname = $_SESSION['max_rows_nav_listrsLastname4'];
$pageNum_rsLastname = 0;
if (isset($_GET['pageNum_rsLastname'])) {
  $pageNum_rsLastname = $_GET['pageNum_rsLastname'];
}
$startRow_rsLastname = $pageNum_rsLastname * $maxRows_rsLastname;

// Defining List Recordset variable
$NXTFilter_rsLastname = "1=1";
if (isset($_SESSION['filter_tfi_listrsLastname4'])) {
  $NXTFilter_rsLastname = $_SESSION['filter_tfi_listrsLastname4'];
}
// Defining List Recordset variable
$NXTSort_rsLastname = "FIRSTNAME";
if (isset($_SESSION['sorter_tso_listrsLastname4'])) {
  $NXTSort_rsLastname = $_SESSION['sorter_tso_listrsLastname4'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_rsLastname = "SELECT * FROM vw_web_elig WHERE {$NXTFilter_rsLastname} AND LASTNAME = 'Brown' ORDER BY {$NXTSort_rsLastname} ";
$query_limit_rsLastname = sprintf("%s LIMIT %d, %d", $query_rsLastname, $startRow_rsLastname, $maxRows_rsLastname);
$rsLastname = mysql_query($query_limit_rsLastname, $sigma_modx) or die(mysql_error());
$row_rsLastname = mysql_fetch_assoc($rsLastname);

if (isset($_GET['totalRows_rsLastname'])) {
  $totalRows_rsLastname = $_GET['totalRows_rsLastname'];
} else {
  $all_rsLastname = mysql_query($query_rsLastname);
  $totalRows_rsLastname = mysql_num_rows($all_rsLastname);
}
$totalPages_rsLastname = ceil($totalRows_rsLastname/$maxRows_rsLastname)-1;
//End NeXTenesio3 Special List Recordset

$nav_listrsLastname4->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | LAST NAME RESULTS</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --> 
<script type="text/javascript" src="/services/directory/js/sortable/sorttable.js"></script>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_FIRSTNAME {width:140px; overflow:hidden;}
  .KT_col_LASTNAME {width:140px; overflow:hidden;}
  .KT_col_BOULENAME {width:140px; overflow:hidden;}
  .KT_col_CITY {width:140px; overflow:hidden;}
  .KT_col_STATECD {width:140px; overflow:hidden;}
  .KT_col_EMAIL {width:140px; overflow:hidden;}
  .KT_col_HOMEPHONE {width:140px; overflow:hidden;}
  .KT_col_JOBTITLE {width:140px; overflow:hidden;}
  .KT_col_CPOSITION {width:140px; overflow:hidden;}
  .KT_col_WEB_ID {width:140px; overflow:hidden;}
</style>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">Page Name</h1>
<div class="contentBlock">

<table cellspacing="4" cellpadding="4" class="sortable">
<thead>
  <tr>
    <td>FIRST NAME</td>
    <td>LAST NAME</td>
    <td>BOUL&Eacute;</td>
    <td>CITY</td>
    <td>STATE</td>
    <td>EMAIL</td>
    <td>PHONE</td>
    <td>OFFICE</td>
    <td>JOB TITLE</td>
    <td>PROFILE</td>
  </tr>
  </thead>
<tfoot>
  <tr>
    <td>FIRST NAME</td>
    <td>LAST NAME</td>
    <td>BOUL&Eacute;</td>
    <td>CITY</td>
    <td>STATE</td>
    <td>EMAIL</td>
    <td>PHONE</td>
    <td>OFFICE</td>
    <td>JOB TITLE</td>
    <td>PROFILE</td>
  </tr>
  </tfoot>
  <tbody>
  <?php do { ?>
    <tr>
      <td><?php echo $row_rsLastname['FIRSTNAME']; ?> <?php echo $row_rsLastname['MIDDLEINITIAL']; ?></td>
      <td><?php echo $row_rsLastname['LASTNAME']; ?></td>
      <td><?php echo $row_rsLastname['BOULENAME']; ?></td>
      <td><?php echo $row_rsLastname['CITY']; ?></td>
      <td><?php echo $row_rsLastname['STATECD']; ?></td>
      <td><a href="mailto:<?php echo $row_rsLastname['EMAIL']; ?>"><?php echo $row_rsLastname['EMAIL']; ?></a></td>
      <td><?php echo $row_rsLastname['HOMEPHONE']; ?></td>
      <td><?php echo $row_rsLastname['CPOSITION']; ?></td>
      <td><?php echo $row_rsLastname['JOBTITLE']; ?></td>
      <td><a href="results.php?id=<?php echo $row_rsLastname['WEB_ID']; ?>">VIEW</a></td>
    </tr>
      <?php } while ($row_rsLastname = mysql_fetch_assoc($rsLastname)); ?>
  </tbody>
</table>






<div class="KT_tng" id="listrsLastname4">
  <h1> Vw_web_elig
    <?php
  $nav_listrsLastname4->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listrsLastname4->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
        <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listrsLastname4'] == 1) {
?>
          <?php echo $_SESSION['default_max_rows_nav_listrsLastname4']; ?>
          <?php 
  // else Conditional region1
  } else { ?>
          <?php echo NXT_getResource("all"); ?>
          <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listrsLastname4'] == 1) {
?>
                  <a href="<?php echo $tfi_listrsLastname4->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                  <?php 
  // else Conditional region2
  } else { ?>
                  <a href="<?php echo $tfi_listrsLastname4->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                  <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>
            </th>
            <th id="FIRSTNAME" class="KT_sorter KT_col_FIRSTNAME <?php echo $tso_listrsLastname4->getSortIcon('FIRSTNAME'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('FIRSTNAME'); ?>">FIRSTNAME</a> </th>
            <th id="LASTNAME" class="KT_sorter KT_col_LASTNAME <?php echo $tso_listrsLastname4->getSortIcon('LASTNAME'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('LASTNAME'); ?>">LASTNAME</a> </th>
            <th id="BOULENAME" class="KT_sorter KT_col_BOULENAME <?php echo $tso_listrsLastname4->getSortIcon('BOULENAME'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('BOULENAME'); ?>">BOULENAME</a> </th>
            <th id="CITY" class="KT_sorter KT_col_CITY <?php echo $tso_listrsLastname4->getSortIcon('CITY'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('CITY'); ?>">CITY</a> </th>
            <th id="STATECD" class="KT_sorter KT_col_STATECD <?php echo $tso_listrsLastname4->getSortIcon('STATECD'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('STATECD'); ?>">STATECD</a> </th>
            <th id="EMAIL" class="KT_sorter KT_col_EMAIL <?php echo $tso_listrsLastname4->getSortIcon('EMAIL'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('EMAIL'); ?>">EMAIL</a> </th>
            <th id="HOMEPHONE" class="KT_sorter KT_col_HOMEPHONE <?php echo $tso_listrsLastname4->getSortIcon('HOMEPHONE'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('HOMEPHONE'); ?>">HOMEPHONE</a> </th>
            <th id="JOBTITLE" class="KT_sorter KT_col_JOBTITLE <?php echo $tso_listrsLastname4->getSortIcon('JOBTITLE'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('JOBTITLE'); ?>">JOBTITLE</a> </th>
            <th id="CPOSITION" class="KT_sorter KT_col_CPOSITION <?php echo $tso_listrsLastname4->getSortIcon('CPOSITION'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('CPOSITION'); ?>">CPOSITION</a> </th>
            <th id="WEB_ID" class="KT_sorter KT_col_WEB_ID <?php echo $tso_listrsLastname4->getSortIcon('WEB_ID'); ?>"> <a href="<?php echo $tso_listrsLastname4->getSortLink('WEB_ID'); ?>">WEB_ID</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listrsLastname4'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listrsLastname4_FIRSTNAME" id="tfi_listrsLastname4_FIRSTNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_FIRSTNAME']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_LASTNAME" id="tfi_listrsLastname4_LASTNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_LASTNAME']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_BOULENAME" id="tfi_listrsLastname4_BOULENAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_BOULENAME']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_CITY" id="tfi_listrsLastname4_CITY" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_CITY']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_STATECD" id="tfi_listrsLastname4_STATECD" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_STATECD']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_EMAIL" id="tfi_listrsLastname4_EMAIL" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_EMAIL']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_HOMEPHONE" id="tfi_listrsLastname4_HOMEPHONE" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_HOMEPHONE']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_JOBTITLE" id="tfi_listrsLastname4_JOBTITLE" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_JOBTITLE']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_CPOSITION" id="tfi_listrsLastname4_CPOSITION" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_CPOSITION']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listrsLastname4_WEB_ID" id="tfi_listrsLastname4_WEB_ID" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsLastname4_WEB_ID']); ?>" size="20" maxlength="100" /></td>
              <td><input type="submit" name="tfi_listrsLastname4" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsLastname == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="12"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsLastname > 0) { // Show if recordset not empty ?>
            <?php do { ?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_vw_web_elig" class="id_checkbox" value="<?php echo $row_rsLastname['CUSTOMERID']; ?>" />
                    <input type="hidden" name="CUSTOMERID" class="id_field" value="<?php echo $row_rsLastname['CUSTOMERID']; ?>" />
                </td>
                <td><div class="KT_col_FIRSTNAME"><?php echo KT_FormatForList($row_rsLastname['FIRSTNAME'], 20); ?></div></td>
                <td><div class="KT_col_LASTNAME"><?php echo KT_FormatForList($row_rsLastname['LASTNAME'], 20); ?></div></td>
                <td><div class="KT_col_BOULENAME"><?php echo KT_FormatForList($row_rsLastname['BOULENAME'], 20); ?></div></td>
                <td><div class="KT_col_CITY"><?php echo KT_FormatForList($row_rsLastname['CITY'], 20); ?></div></td>
                <td><div class="KT_col_STATECD"><?php echo KT_FormatForList($row_rsLastname['STATECD'], 20); ?></div></td>
                <td><div class="KT_col_EMAIL"><?php echo KT_FormatForList($row_rsLastname['EMAIL'], 20); ?></div></td>
                <td><div class="KT_col_HOMEPHONE"><?php echo KT_FormatForList($row_rsLastname['HOMEPHONE'], 20); ?></div></td>
                <td><div class="KT_col_JOBTITLE"><?php echo KT_FormatForList($row_rsLastname['JOBTITLE'], 20); ?></div></td>
                <td><div class="KT_col_CPOSITION"><?php echo KT_FormatForList($row_rsLastname['CPOSITION'], 20); ?></div></td>
                <td><div class="KT_col_WEB_ID"><?php echo KT_FormatForList($row_rsLastname['WEB_ID'], 20); ?></div></td>
                <td><a class="KT_edit_link" href="../committee/edit_committee.php?CUSTOMERID=<?php echo $row_rsLastname['CUSTOMERID']; ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
              </tr>
              <?php } while ($row_rsLastname = mysql_fetch_assoc($rsLastname)); ?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsLastname4->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
<span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
          <option value="3">3</option>
          <option value="6">6</option>
        </select>
        <a class="KT_additem_op_link" href="../committee/edit_committee.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</div>


<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsLastname);
?>
