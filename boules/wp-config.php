<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress4');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '4wU58idZ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aDkxnWmbA8wf4R7r~<Tbiq>nyMyWqt3>mj2JCShqDj@HK+b45nAz$R+7Ev74hSRm');
define('SECURE_AUTH_KEY',  'Tn=yY4GGq5YemNyfAGMm)LCuYSm(LqYxD@DjVA4kN#Xb6d~HcBVnETXRjK*+(Rw<');
define('LOGGED_IN_KEY',    '9#DhVL=(@2J~iK(ChgX%twtV7%LNrr<qdtQ3)Z6y-Q72c7D-&)Uge&9G$uRUxaf~');
define('NONCE_KEY',        'Xv~EQ5nq=Vr-Dh$3Dm*sEJpBcj2j&eiwmk~~hfipGmMqD6$m(Lct82~GjgrK+B7Y');
define('AUTH_SALT',        'H=Y7(9=58%xqMUEX-q-2WWd)KNwvgVhdMw*KrnRUvfzk(=q-c64yR7&Z<)(7HDxy');
define('SECURE_AUTH_SALT', 'mwQH#GMJRYHXa7Lj$G%AJiXhcxJj6ZtD82SGr6TZRkUyMA8BrrkSVmTVvue#Zbmt');
define('LOGGED_IN_SALT',   'A3@%8p9upiUuD+87)hmr4tw9q3L~&jh5uu2CYcTMkJcZ4Db$n5mbC%MR-#$)25H@');
define('NONCE_SALT',       'kq+$44Xw%bPU3kf7=v>hzgCRdc*MWHU6@ichHQ2Yrx&6z-E5xccSKxvjgfLJ72+Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define('WPLANG', '');

/* Define the cookie paths so logging in from MODx /home URL correctly sets the /boule path */
define( 'COOKIEPATH', '/boules/' );
define( 'SITECOOKIEPATH', COOKIEPATH );
define( 'COOKIEHASH', md5( 'http://dev4.sigmapiphi.org/boules') );
//define( 'ADMIN_COOKIE_PATH', SITECOOKIEPATH . 'wp-admin' );

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
$base = '/boules/';
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', true);
define('WP_DEBUG_LOG', true);
//define( 'WP_ALLOW_MULTISITE', true );
define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', false );
define( 'DOMAIN_CURRENT_SITE', 'dev4.sigmapiphi.org' );
define( 'PATH_CURRENT_SITE', '/boules/' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );
define( 'AUTOMATIC_UPDATER_DISABLED', true );
// try to use mysqli
//define( 'WP_USE_EXT_MYSQL', true );
//define( 'WPDB_DRIVER', 'wpdb_driver_mysqli' );
//define('RELOCATE',true);
//define( 'ABSPATH', '/var/lib/apache/htdocs/flippingwrong/');

/** Removes Events from WP Admin Bar  */
define('TRIBE_DISABLE_TOOLBAR_ITEMS', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
