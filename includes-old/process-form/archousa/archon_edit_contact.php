<?php
$data = $response = array();
parse_str($_POST['data'], $data);
array_walk($data, 'test_in_array');
$response['type'] = 'success';

$date = date_create($data['BIRTHDATE']);
$data['BIRTHDATE'] = date_format($date,"Y-m-d");

if (filter_var($data['EMAIL'], FILTER_VALIDATE_EMAIL) === false) {
    $response['type'] = 'error';
    $response['html'] = '<p>Email is not valid</p>';
}

$archousa_values = array(
    'PREFIX', 'SUFFIX', 'FIRSTNAME', 'MIDDLEINITIAL', 'LASTNAME',
    'DESIGNATIONLST', 'MOBILEPHONE', 'EMAIL', 'BIRTHDATE',
);
$archon_values = array(
    'ADDRESS1', 'ADDRESS2', 'CITY', 'STATECD',
    'ZIP', 'HOMEPHONE',
);

if($response['type'] !== 'error'){
    $db = get_db();

    $archousa_data = array();
    $archon_data = array();

    foreach ($archousa_values as $key) {
        if(empty($data[$key])){
            $archousa_data[$key] = '';
        } else {
            $archousa_data[$key] = $data[$key];
        }
    }

    $archousa_data['LASTUPDATED'] = date("Y-m-d H:i:s");
    $archousa_data['UPDATEDBY'] = $updated_by;

    $db->where ('ARCHOUSAID', $archousa_id);
    if ($db->update ('spp_archousa', $archousa_data)){

        $ardb = get_db();

        foreach ($archon_values as $key) {
            if(empty($data[$key])){
                $archon_data[$key] = '';
            } else {
                $archon_data[$key] = $data[$key];
            }
        }
        $archon_data['SPOUSENAME'] = $archousa_data['FIRSTNAME'];
        
        $ardb->where ('CUSTOMERID', $user_id);

        $ardb->update ('spp_archons', $archon_data);

        SPP_ARCHON_DB::update_lastupdate_date($archousa_id, 'ARCHOUSAID', 'spp_archousa');
        $response['type'] = 'success';
        $response['html'] = "
<p><strong>Mailing Address</strong><br>
    {$data['ADDRESS1']} {$data['ADDRESS2']} <br>
    {$data['CITY']}, {$data['STATECD']} {$data['ZIP']} </p>

<p><strong>Email</strong><br>
    {$data['EMAIL']}</p>

<p><strong>Home Phone</strong><br>
    {$data['HOMEPHONE']}</p>

<p><strong>Mobile</strong><br>
{$data['MOBILEPHONE']}
    </p>

<p><strong>Birthdate</strong><br>
    " . date('F d', strtotime($data['BIRTHDATE'])) ."
</p>
";
        $response['profile_title'] = "{$data['FIRSTNAME']}' {$data['MIDDLEINITIAL']}' {$data['LASTNAME']}' {$data['SUFFIX']}'";

    } else{
        $response['type'] = 'error';
        $response['html'] = '<p>Error occurred! Please try again.</p>';
    }
}

die(json_encode($response));