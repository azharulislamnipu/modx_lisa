<?php
$data = $response = array();
parse_str($_POST['data'], $data);
array_walk($data, 'test_in_array');
$response['type'] = 'success';

$response['html'] = '';

if (empty($data['DEGREE'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Degree is not valid</p>';
}
if (empty($data['INSTITUTION'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Institution is not valid</p>';
}
if (empty($data['DEGREEYEAR']) || !(@preg_match('/^\d{4}$/', $data['DEGREEYEAR']))) {
    $response['type'] = 'error';
    $response['html'] .= "<p>Degree Year {$data['DEGREEYEAR']} is not valid</p>";
}
if (empty($data['MAJOR'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Major Subject is not valid</p>';
}

$same_values = array(
    'DEGREE', 'INSTITUTION', 'DEGREEYEAR',
    'MAJOR', 'MINOR1', 'MINOR2', 'COMMENT'
);

if($response['type'] !== 'error'){
    $db = get_db();
    $ndata = array();

    foreach ($same_values as $key) {
        if(array_key_exists($key, $data)){
            $ndata[$key] = $data[$key];
        }
    }
    $ndata['UPDATEDBY'] = $updated_by;
    $ndata['LASTUPDATED'] = date("Y-m-d H:i:s");
    $ndata['IPADDRESS'] = $ip;
    /*$ndata = array (
        'DEGREE'        => $data['DEGREE'],
        'INSTITUTION'   => $data['INSTITUTION'],
        'DEGREEYEAR'    => $data['DEGREEYEAR'],
        'MAJOR'         => $data['MAJOR'],
        'MINOR1'        => $data['MINOR1'],
        'MINOR2'        => $data['MINOR2'],
        'COMMENT'       => $data['COMMENT'],
        'UPDATEDBY'     => $updated_by,
        'LASTUPDATED'   => date("Y-m-d H:i:s"),
        'IPADDRESS'     => $ip,
    );*/

//    print_r($extra_val);
    $db_success = false;

    if(empty($data['DEGREEID'])){
        $ndata['CUSTOMERID'] = $user_id;
        $db_success = $db->insert ('spp_educ', $ndata);
        $response['action'] = 'INSERT';
        $response['DEGREEID'] = $db_success;
    } else {
        $db->where ('CUSTOMERID', $user_id);
        $db->where ('DEGREEID', $data['DEGREEID']);
        $db_success = $db->update ('spp_educ', $ndata);
        $response['action'] = 'UPDATE';
        $response['DEGREEID'] = $data['DEGREEID'];
    }

    if ($db_success){
        $response['type'] = 'success';
        $response['TITLE'] = $data['INSTITUTION'] . ', ' . $data['DEGREEYEAR'];
        $response['html'] = "
    <strong>{$data['INSTITUTION']}</strong>, {$data['DEGREEYEAR']}<br>
    {$data['DEGREE']}, {$data['MAJOR']}<br>
";
        if(!empty($data['MINOR1'])){
            $response['html'] .= "<span class='font-small'>{$data['MINOR1']}</span><br>";
        }

        SPP_ARCHON_DB::update_lastupdate_date($user_id);
    } else{
        $response['type'] = 'error';
//        $response['html'] = $db->getLastError();
        $response['html'] = '<p>Error occurred! Please try again.</p>';
    }
}

die(json_encode($response));