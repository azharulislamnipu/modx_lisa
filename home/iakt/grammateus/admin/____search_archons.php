<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Filter
$tfi_listrsArchonList2 = new TFI_TableFilter($sigma_modx, "tfi_listrsArchonList2");
$tfi_listrsArchonList2->addColumn("STATUS", "STRING_TYPE", "STATUS", "%");
$tfi_listrsArchonList2->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listrsArchonList2->addColumn("JOINDATE", "DATE_TYPE", "JOINDATE", "=");
$tfi_listrsArchonList2->addColumn("LASTUPDATED", "DATE_TYPE", "LASTUPDATED", "=");
$tfi_listrsArchonList2->Execute();

// Sorter
$tso_listrsArchonList2 = new TSO_TableSorter("rsArchonList", "tso_listrsArchonList2");
$tso_listrsArchonList2->addColumn("STATUS");
$tso_listrsArchonList2->addColumn("FULLNAME");
$tso_listrsArchonList2->addColumn("JOINDATE");
$tso_listrsArchonList2->addColumn("LASTUPDATED");
$tso_listrsArchonList2->setDefault("FULLNAME");
$tso_listrsArchonList2->Execute();

// Navigation
$nav_listrsArchonList2 = new NAV_Regular("nav_listrsArchonList2", "rsArchonList", "../../", $_SERVER['PHP_SELF'], 300);

// Begin List Recordset
$maxRows_rsArchonList = $_SESSION['max_rows_nav_listrsArchonList2'];
$pageNum_rsArchonList = 0;
if (isset($_GET['pageNum_rsArchonList'])) {
  $pageNum_rsArchonList = $_GET['pageNum_rsArchonList'];
}
$startRow_rsArchonList = $pageNum_rsArchonList * $maxRows_rsArchonList;
$colname__rsArchonList = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsArchonList = $_SESSION['BOULENAME'];
}
$colname1__rsArchonList = '%';
if (isset($_GET['ID'])) {
  $colname1__rsArchonList = $_GET['ID'];
}
// Defining List Recordset variable
$NXTFilter__rsArchonList = "1=1";
if (isset($_SESSION['filter_tfi_listrsArchonList2'])) {
  $NXTFilter__rsArchonList = $_SESSION['filter_tfi_listrsArchonList2'];
}
// Defining List Recordset variable
$NXTSort__rsArchonList = "FULLNAME";
if (isset($_SESSION['sorter_tso_listrsArchonList2'])) {
  $NXTSort__rsArchonList = $_SESSION['sorter_tso_listrsArchonList2'];
}
$query_rsArchonList = sprintf("SELECT * FROM vw_gramm_archons WHERE vw_gramm_archons.BOULENAME = %s AND vw_gramm_archons.`L2R_FULLNAME`LIKE %s  AND  {$NXTFilter__rsArchonList} ORDER BY {$NXTSort__rsArchonList} ", GetSQLValueString($colname__rsArchonList, "text"),GetSQLValueString("%" . $colname1__rsArchonList . "%", "text"));
$rsArchonList = $sigma_modx->SelectLimit($query_rsArchonList, $maxRows_rsArchonList, $startRow_rsArchonList) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsArchonList'])) {
  $totalRows_rsArchonList = $_GET['totalRows_rsArchonList'];
} else {
  $all_rsArchonList = $sigma_modx->SelectLimit($query_rsArchonList) or die($sigma_modx->ErrorMsg());
  $totalRows_rsArchonList = $all_rsArchonList->RecordCount();
}
$totalPages_rsArchonList = (int)(($totalRows_rsArchonList-1)/$maxRows_rsArchonList);
// End List Recordset

// begin Recordset
$query_rsStatusTypes = "SELECT    vw_gramm_archons.`STATUS` FROM   vw_gramm_archons GROUP BY   vw_gramm_archons.`STATUS`";
$rsStatusTypes = $sigma_modx->SelectLimit($query_rsStatusTypes) or die($sigma_modx->ErrorMsg());
$totalRows_rsStatusTypes = $rsStatusTypes->RecordCount();
// end Recordset

// begin Recordset
$colname__rsArchons = '-1';
if (isset($_GET['ID'])) {
  $colname__rsArchons = $_GET['ID'];
}
$colname1__rsArchons = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname1__rsArchons = $_SESSION['BOULENAME'];
}
$query_rsArchons = sprintf("SELECT * FROM vw_gramm_archons WHERE BOULENAME = %s AND L2R_FULLNAME LIKE %s ORDER BY FULLNAME ASC", GetSQLValueString($colname1__rsArchons, "text"),GetSQLValueString("%" . $colname__rsArchons . "%", "text"));
$rsArchons = $sigma_modx->SelectLimit($query_rsArchons) or die($sigma_modx->ErrorMsg());
$totalRows_rsArchons = $rsArchons->RecordCount();
// end Recordset

$nav_listrsArchonList2->checkBoundries();

//PHP ADODB document - made with PHAkt 3.7.1
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_STATUS {width:126px; overflow:hidden;}
  .KT_col_FULLNAME {width:290px; overflow:hidden;}
  .KT_col_JOINDATE {width:70px; overflow:hidden;}
  .KT_col_LASTUPDATED {width:70px; overflow:hidden;}
.style4 {
	font-size: 18px;
	margin-bottom: 4px;
	margin-top: -4px;
}
.style6 {font-size: 12px}
.style8 {
	font-size: 11px;
	margin-top: -2px;
}
</style>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng">
<h1>[*pagetitle*]</h1>
  <div class="KT_tngform">
 <form action="/home/search_archons.php" method="get"  name="search">
 Search for Archons by name in your boul&eacute;: 
 <input name="ID" type="text" size="35" /> <input name="Search" type="submit" value="Search" />
 </form>
 </div>
 
<?php if ($totalRows_rsArchons > 0) { // Show if recordset not empty ?>
  <div class="KT_tng" id="listrsArchonList2" style="margin-top: 24px;">
    <h1>SEARCH RESULTS | Displaying -  <?php echo (min($startRow_rsArchonList + 1, $totalRows_rsArchonList)) ?> to <?php echo min($startRow_rsArchonList + $maxRows_rsArchonList, $totalRows_rsArchonList) ?> of <?php echo $totalRows_rsArchonList ?></h1>
    <div class="KT_tnglist">
      <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <thead>
            <tr class="KT_row_order">
              <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>            </th>
              <th id="STATUS" class="KT_sorter KT_col_STATUS <?php echo $tso_listrsArchonList2->getSortIcon('STATUS'); ?>"> <a href="<?php echo $tso_listrsArchonList2->getSortLink('STATUS'); ?>">STATUS</a> </th>
              <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listrsArchonList2->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listrsArchonList2->getSortLink('FULLNAME'); ?>">FULL NAME</a> </th>
              <th id="JOINDATE" class="KT_sorter KT_col_JOINDATE <?php echo $tso_listrsArchonList2->getSortIcon('JOINDATE'); ?>"> <a href="<?php echo $tso_listrsArchonList2->getSortLink('JOINDATE'); ?>">JOIN DATE</a> </th>
              <th id="LASTUPDATED" class="KT_sorter KT_col_LASTUPDATED <?php echo $tso_listrsArchonList2->getSortIcon('LASTUPDATED'); ?>"> <a href="<?php echo $tso_listrsArchonList2->getSortLink('LASTUPDATED'); ?>">LAST UPDATED</a> </th>
              <th>&nbsp;</th>
            </tr>
            <?php 
  // Show IF Conditional region3 
  if (@$_SESSION['has_filter_tfi_listrsArchonList2'] == 1) {
?>
              <tr class="KT_row_filter">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><input type="text" name="tfi_listrsArchonList2_FULLNAME" id="tfi_listrsArchonList2_FULLNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchonList2_FULLNAME']); ?>" size="35" maxlength="20" /></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><input type="submit" name="tfi_listrsArchonList2" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
              </tr>
              <?php } 
  // endif Conditional region3
?>
          </thead>
          <tbody>
            <?php if ($totalRows_rsArchonList == 0) { // Show if recordset empty ?>
              <tr>
                <td colspan="6"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
              </tr>
              <?php } // Show if recordset empty ?>
            <?php if ($totalRows_rsArchonList > 0) { // Show if recordset not empty ?>
              <?php
  while (!$rsArchonList->EOF) { 
?>
                <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                  <td><input type="checkbox" name="kt_pk_vw_gramm_archons" class="id_checkbox" value="<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>" />
                      <input type="hidden" name="CUSTOMERID" class="id_field" value="<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>" />                </td>
                  <td><div class="KT_col_STATUS"><span class="style6"><?php echo KT_FormatForList($rsArchonList->Fields('STATUS'), 18); ?></span><br />
                    <br />
                    <?php 
// Show IF Conditional region9 
if (@$rsArchonList->Fields('STATUS') != "Deceased") {
?>
                      <a href="/home/edit_status.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Request Status Change</a><br />
                      <a href="/home/request_transfer.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Transfer Form</a> <br />
                      <a href="/home/request_vol_inactive.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Voluntary Inactive Form</a><br>
                      <a href="/home/request_invol_inactive.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Involuntary Inactive Form</a>
                      <?php } 
// endif Conditional region9
?>
                    </div></td>
                  <td><div class="KT_col_FULLNAME">
                    <p class="style4"><?php echo $rsArchonList->Fields('L2R_FULLNAME'); ?></p>
                    <p class="style6"><?php echo $rsArchonList->Fields('ADDRESS1'); ?><?php echo $rsArchonList->Fields('ADDRESS2'); ?>, <?php echo $rsArchonList->Fields('CITY'); ?>, <?php echo $rsArchonList->Fields('STATECD'); ?> <?php echo $rsArchonList->Fields('ZIP'); ?></p>
                    <p class="style8">
                      <?php 
// Show IF Conditional region7 
if (@$rsArchonList->Fields('HOMEPHONE') != NULL) {
?>
                        HOME PHONE: <span class="style6"><?php echo $rsArchonList->Fields('HOMEPHONE'); ?></span>
                        <?php 
// else Conditional region7
} else { ?>
                        <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1#homephone">Add Home Phone</a>
                          <?php } 
// endif Conditional region7
?>
                      <br>
                      EMAIL: <span class="style6"><a href="mailto:<?php echo $rsArchonList->Fields('WEB_EMAIL'); ?>"><?php echo $rsArchonList->Fields('WEB_EMAIL'); ?></a></span></p>
                    <p class="style8"> 
                      <?php 
// Show IF Conditional region8 
if (@$rsArchonList->Fields('SPOUSENAME') != NULL) {
?>
                        ARCHOUSA: <span class="style6"><a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1#spousename"><?php echo $rsArchonList->Fields('SPOUSENAME'); ?></a></span>
                        <?php 
// else Conditional region8
} else { ?>
                        <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1#spousename">Add Archousa</a>
                          <?php } 
// endif Conditional region8
?>
                    </p>
                  </div></td>
                  <td><div class="KT_col_JOINDATE">
                    <p class="style8">JOIN DATE<br>
                      <?php 
// Show IF Conditional region11 
if (@$rsArchonList->Fields('JOINDATE') != NULL) {
?>
                        <span class="style6"><?php echo KT_formatDate($rsArchonList->Fields('JOINDATE')); ?></span>
                        <?php 
// else Conditional region11
} else { ?>
                        <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1#joindate">Add Date</a>
                          <?php } 
// endif Conditional region11
?>
                    </p>
                    <p class="style8">BIRTH DATE<br>
                      <?php 
// Show IF Conditional region6 
if (@$rsArchonList->Fields('BIRTHDATE') != NULL) {
?>
                        <span class="style6"><?php echo KT_formatDate($rsArchonList->Fields('BIRTHDATE')); ?></span>
                        <?php 
// else Conditional region6
} else { ?>
                        <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1#birthdate">Add Date</a>
                          <?php } 
// endif Conditional region6
?>
                      </p>
                    <?php  // Show IF Conditional region4 
if (@$rsArchonList->Fields('DECEASEDDATE') != "Deceased") {
?>
                      <p class="style8"> DECEASED<br>
                        <?php 
// Show IF Conditional region5 
if (@$rsArchonList->Fields('DECEASEDDATE') != NULL) {
?>
                          <span class="style6"><?php echo $rsArchonList->Fields('DECEASEDDATE'); ?></span>
                          <?php 
// else Conditional region5
} else { ?>
                          <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1#deceaseddate">Add Date</a>
                          <?php } 
// endif Conditional region5
?>
                      </p>
                      <?php } 
// endif Conditional region4
?>
                      </div></td>
                  <td><div class="KT_col_LASTUPDATED"><p class="style8">BY ARCHON<br><span class="style6"><?php echo KT_formatDate($rsArchonList->Fields('LASTUPDATED')); ?></span></p>
                  <p class="style8">BY GRAMM<br><span class="style6"><?php echo KT_formatDate($rsArchonList->Fields('GRAMMUPDATE')); ?></span></p></div></td>
                  <td><a class="KT_edit_link" href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Modify All Data</a><br />
                    <?php 
// Show IF Conditional region3 
if (@$rsArchonList->Fields('STATUS') != "Deceased") {
?>
                      <a href="/home/edit_deceased.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Mark Deceased</a>
                    <?php } 
// endif Conditional region3
?></td>
                </tr>
                <?php
    $rsArchonList->MoveNext(); 
  }
?>
              <?php } // Show if recordset not empty ?>
          </tbody>
        </table>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <?php } // Show if recordset not empty ?><p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchonList->Close();

$rsStatusTypes->Close();

$rsArchons->Close();
?>
