<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsBoulesByRegion = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_rsBoulesByRegion = $_COOKIE['REGIONNAME'];
}
$colname2_rsBoulesByRegion = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname2_rsBoulesByRegion = $_GET['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoulesByRegion = sprintf("SELECT * FROM vw_web_elig WHERE REGIONNAME = %s AND BOULENAME = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsBoulesByRegion, "text"),GetSQLValueString($colname2_rsBoulesByRegion, "text"));
$rsBoulesByRegion = mysql_query($query_rsBoulesByRegion, $sigma_modx) or die(mysql_error());
$row_rsBoulesByRegion = mysql_fetch_assoc($rsBoulesByRegion);
$totalRows_rsBoulesByRegion = mysql_num_rows($rsBoulesByRegion);

$colname_result = "-1";
if (isset($_COOKIE['GRAMMID'])) {
  $colname_result = $_COOKIE['GRAMMID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_result = sprintf("SELECT REGIONNAME, BOULENAME, CHAPTERID FROM vw_web_elig_no_officers WHERE CUSTOMERID = %s", GetSQLValueString($colname_result, "int"));
$result = mysql_query($query_result, $sigma_modx) or die(mysql_error());
$row_result = mysql_fetch_assoc($result);
$totalRows_result = mysql_num_rows($result);

$colname_exportBoules = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_exportBoules = $_COOKIE['REGIONNAME'];
}
$colname2_exportBoules = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname2_exportBoules = $_GET['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_exportBoules = sprintf("SELECT    vw_web_elig.BOULENAME,   vw_web_elig.PREFIX,   vw_web_elig.FIRSTNAME,   vw_web_elig.MIDDLEINITIAL,   vw_web_elig.LASTNAME,   vw_web_elig.SUFFIX,   vw_web_elig.DESIGNATIONLST as HONORIFIC,   vw_web_elig.ADDRESS1,   vw_web_elig.CITY,   vw_web_elig.STATECD AS STATE,   vw_web_elig.ZIP,   vw_web_elig.WEB_EMAIL AS HOME_EMAIL,   vw_web_elig.HOMEPHONE AS HOME_PHONE,   vw_web_elig.WORKPHONE AS WORK_PHONE,   vw_web_elig.ALTEMAIL AS WORK_EMAIL FROM   vw_web_elig WHERE REGIONNAME = %s AND BOULENAME = %s ORDER BY LASTNAME, FIRSTNAME ASC", GetSQLValueString($colname_exportBoules, "text"),GetSQLValueString($colname2_exportBoules, "text"));
$exportBoules = mysql_query($query_exportBoules, $sigma_modx) or die(mysql_error());
$row_exportBoules = mysql_fetch_assoc($exportBoules);
$totalRows_exportBoules = mysql_num_rows($exportBoules);

//Export to Excel Server Behavior
if (isset($_POST['boule_export'])&&($_POST['boule_export']=="boule")){
	$delim="";
	$delim_replace="";
	if($delim==""){
		$lang=(strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],",")===false)?$_SERVER['HTTP_ACCEPT_LANGUAGE']:substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],","));
		$semi_array=array("af","zh-hk","zh-mo","zh-cn","zh-sg","zh-tw","fr-ch","de-li","de-ch","it-ch","ja","ko","es-do","es-sv","es-gt","es-hn","es-mx","es-ni","es-pa","es-pe","es-pr","sw");
		$delim=(in_array($lang,$semi_array) || substr_count($lang,"en")>0)?",":";";
	}
	$output="";
	$include_hdr="1";
	if($include_hdr=="1"){
		$totalColumns_exportBoules=mysql_num_fields($exportBoules);
		for ($x=0; $x<$totalColumns_exportBoules; $x++) {
			if($x==$totalColumns_exportBoules-1){$comma="";}else{$comma=$delim;}
			$output = $output.(ereg_replace("_", " ",mysql_field_name($exportBoules, $x))).$comma;
		}
		$output = $output."\r\n";
	}

	do{$fixcomma=array();
    		foreach($row_exportBoules as $r){array_push($fixcomma,ereg_replace($delim,$delim_replace,$r));}
		$line = join($delim,$fixcomma);
    		$line=ereg_replace("\r\n", " ",$line);
    		$line = "$line\n";
    		$output=$output.$line;}while($row_exportBoules = mysql_fetch_assoc($exportBoules));
	header("Content-Type: application/xls");
	header("Content-Disposition: attachment; filename=Boule.csv");
	header("Content-Type: application/force-download");
	header("Cache-Control: post-check=0, pre-check=0", false);
	echo $output;
	die();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_regional_grammateus.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow"> <?php echo $row_rsBoulesByRegion['BOULENAME']; ?> Archons</h1>
<div class="contentBlock">
<table width="100%" cellpadding="2">
<tr>
  <td><a href="/home/reg_gramm_mgmt.php">Back to Management Screen</a></td>
  <td><form id="boule" name="boule" method="post" action="">
    
    <input name="boule_export" type="hidden" id="boule" value="boule" />
    <input name="export" type="submit" value="Click to Export <?php echo $row_rsBoulesByRegion['BOULENAME']; ?> Data" />
    </form>  </td>
  </tr>
  <tr>
    <td><strong>ARCHONS</strong></td>
    <td>&nbsp;</td>
    </tr>
  <?php do { ?>
    <tr valign="top">
      <td colspan="2">
  <script language="JavaScript">
    function edit_<?php echo $row_rsBoulesByRegion['CUSTOMERID'];?>(){
        document.forms.form_<?php echo $row_rsBoulesByRegion['CUSTOMERID']; ?>.submit();
    }
  </script>
  <form name="form_<?php echo $row_rsBoulesByRegion['CUSTOMERID']; ?>" action="/services/grammateus/regional/archons_edit.php" method="get">
    <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsBoulesByRegion['CUSTOMERID']); ?>">
    <input type="hidden" name="GRAMMID" value="<?php echo KT_escapeAttribute($_COOKIE['GRAMMID']); ?>">
    <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_COOKIE['UPDATEDBY']); ?>">
    <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_COOKIE['BOULENAME']); ?>">
    <input type="hidden" name="REGIONNAME" value="<?php echo KT_escapeAttribute($_COOKIE['REGIONNAME']); ?>">
  </form>
  <a href="#" rel="<?php echo $row_rsBoulesByRegion['WEB_ID']; ?>" onclick="edit_<?php echo $row_rsBoulesByRegion['CUSTOMERID'];?>();return(FALSE);"><?php echo $row_rsBoulesByRegion['FULLNAME']; ?></a>
          <div id="<?php echo $row_rsBoulesByRegion['WEB_ID']; ?>" class="balloonstyle">
            <?php echo $row_rsBoulesByRegion['ADDRESS1']; ?><br />
            <?php echo $row_rsBoulesByRegion['CITY']; ?>, <?php echo $row_rsBoulesByRegion['STATECD']; ?> <?php echo $row_rsBoulesByRegion['ZIP']; ?><br />
            HOME PHONE: <?php echo $row_rsBoulesByRegion['HOMEPHONE']; ?><br />
            EMAIL: <?php echo $row_rsBoulesByRegion['EMAIL']; ?><br />
            BUSINESS PHONE: <?php echo $row_rsBoulesByRegion['WORKPHONE']; ?><br />
            BUSINESS EMAIL: <?php echo $row_rsBoulesByRegion['ALTEMAIL']; ?>    </div></td>
      </tr>
    <?php } while ($row_rsBoulesByRegion = mysql_fetch_assoc($rsBoulesByRegion)); ?>
<tr>
  <td><a href="/home/reg_gramm_mgmt.php">Back to Management Screen</a></td>
  <td>&nbsp;</td>
  </tr>
</table>
</div>
<!-- InstanceEndEditable --></div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsBoulesByRegion);

mysql_free_result($result);

mysql_free_result($exportBoules);
?>
