[!userProfile? &type=`Archon`!]

<!-- #MAIN COLUMN -->



[!LDataDisplay? &table=`spp_archons` &display=`1` &tpl=`tpl_archon_prof_image` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]

<!-- [ !LDataDisplay? &table=`spp_officers` &display=`all` &tpl=`tpl_archon_prof_offices` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1` &sortBy=`START_DATE` &sortDir=`ASC`!]-->

[!LDataDisplay? &table=`spp_archons` &display=`1` &tpl=`tpl_archon_prof_personal_info` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]


</div></div></div>
<!-- #END MAIN COLUMN -->




<!-- #BEGIN SECOND COLUMN -->
<div class="span-4"><div class="padding">
        <div class="width padding">



            <!-- #END SECOND COLUMN -->


            <!-- #BEGIN SECOND COLUMN -->

            <h3>Career</h3>

            <div id="profile_career_box">
                [!LDataDisplay? &table=`spp_prof` &display=`all` &sortBy=`OCCUPATIONID` &sortDir=`ASC` &noResult=`ac_prof_occup_no_result` &tpl=`tpl_archon_prof_occup_info_list` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]
            </div>

            <h3>Education</h3>
            <div id="profile_educ_box">
                [!LDataDisplay? &table=`spp_educ` &sortBy=`DEGREEYEAR` &sortDir=`ASC` &tpl=`tpl_archon_prof_educ_list` &noResult=`ac_prof_educ_no_result` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]
            </div>

        </div></div></div>

<!-- #begin THIRD COLUMN -->
<div class="span-4"><div class="padding">
        <div class="width padding" id="archon_bio">

            [!LDataDisplay? &table=`spp_archons` &display=`1` &tpl=`tpl_archon_prof_bio` &filter=`CUSTOMERID,[!getPlaceholder? &name=`customercd`!],1`!]

            <!-- #END THIRD COLUMN -->
        </div></div>