<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listrsOfficers1 = new TFI_TableFilter($conn_sigma_modx, "tfi_listrsOfficers1");
$tfi_listrsOfficers1->addColumn("L2R_FULLNAME", "STRING_TYPE", "L2R_FULLNAME", "%");
$tfi_listrsOfficers1->addColumn("CPOSITION", "STRING_TYPE", "CPOSITION", "%");
$tfi_listrsOfficers1->Execute();

// Sorter
$tso_listrsOfficers1 = new TSO_TableSorter("rsOfficers", "tso_listrsOfficers1");
$tso_listrsOfficers1->addColumn("L2R_FULLNAME");
$tso_listrsOfficers1->addColumn("CPOSITION");
$tso_listrsOfficers1->setDefault("L2R_FULLNAME");
$tso_listrsOfficers1->Execute();

// Navigation
$nav_listrsOfficers1 = new NAV_Regular("nav_listrsOfficers1", "rsOfficers", "../../", $_SERVER['PHP_SELF'], 150);

//NeXTenesio3 Special List Recordset
$maxRows_rsOfficers = $_SESSION['max_rows_nav_listrsOfficers1'];
$pageNum_rsOfficers = 0;
if (isset($_GET['pageNum_rsOfficers'])) {
  $pageNum_rsOfficers = $_GET['pageNum_rsOfficers'];
}
$startRow_rsOfficers = $pageNum_rsOfficers * $maxRows_rsOfficers;

$colname_rsOfficers = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname_rsOfficers = $_GET['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter_rsOfficers = "1=1";
if (isset($_SESSION['filter_tfi_listrsOfficers1'])) {
  $NXTFilter_rsOfficers = $_SESSION['filter_tfi_listrsOfficers1'];
}
// Defining List Recordset variable
$NXTSort_rsOfficers = "L2R_FULLNAME";
if (isset($_SESSION['sorter_tso_listrsOfficers1'])) {
  $NXTSort_rsOfficers = $_SESSION['sorter_tso_listrsOfficers1'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_rsOfficers = sprintf("SELECT OFFICERID, CPOSITION, BOULENAME, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, SUFFIX, DESIGNATIONLST, ADDRESS1, CITY, STATECD, ZIP, HOMEPHONE, EMAIL, WORKPHONE, ALTEMAIL,L2R_FULLNAME,CPOSITIONID, REGIONCD, CHAPTERID, CUSTOMERCD FROM vw_boule_officers WHERE BOULENAME = %s AND COMMITTEESTATUSSTT = 'Active' AND  {$NXTFilter_rsOfficers} ORDER BY CPOSITIONID, {$NXTSort_rsOfficers}", GetSQLValueString($colname_rsOfficers, "text"));
$query_limit_rsOfficers = sprintf("%s LIMIT %d, %d", $query_rsOfficers, $startRow_rsOfficers, $maxRows_rsOfficers);
$rsOfficers = mysql_query($query_limit_rsOfficers, $sigma_modx) or die(mysql_error());
$row_rsOfficers = mysql_fetch_assoc($rsOfficers);

if (isset($_GET['totalRows_rsOfficers'])) {
  $totalRows_rsOfficers = $_GET['totalRows_rsOfficers'];
} else {
  $all_rsOfficers = mysql_query($query_rsOfficers);
  $totalRows_rsOfficers = mysql_num_rows($all_rsOfficers);
}
$totalPages_rsOfficers = ceil($totalRows_rsOfficers/$maxRows_rsOfficers)-1;
//End NeXTenesio3 Special List Recordset

$nav_listrsOfficers1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  dir="ltr" lang="en-EN"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi | Manage Boul&#233; Officers</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: false,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_L2R_FULLNAME {width:140px; overflow:hidden;}
  .KT_col_CPOSITION {width:140px; overflow:hidden;}
</style>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listrsOfficers1">
  <h1> OFFICER MANAGEMENT
    <?php
  $nav_listrsOfficers1->Prepare();
  require("../../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listrsOfficers1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
            <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listrsOfficers1'] == 1) {
?>
              <?php echo $_SESSION['default_max_rows_nav_listrsOfficers1']; ?>
              <?php 
  // else Conditional region1
  } else { ?>
              <?php echo NXT_getResource("all"); ?>
              <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp; </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th>
            </th>
            <th id="L2R_FULLNAME" class="KT_sorter KT_col_L2R_FULLNAME <?php echo $tso_listrsOfficers1->getSortIcon('L2R_FULLNAME'); ?>"> <a href="<?php echo $tso_listrsOfficers1->getSortLink('L2R_FULLNAME'); ?>">L2R_FULLNAME</a> </th>
            <th id="CPOSITION" class="KT_sorter KT_col_CPOSITION <?php echo $tso_listrsOfficers1->getSortIcon('CPOSITION'); ?>"> <a href="<?php echo $tso_listrsOfficers1->getSortLink('CPOSITION'); ?>">CPOSITION</a> </th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php if ($totalRows_rsOfficers == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="4"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsOfficers > 0) { // Show if recordset not empty ?>
            <?php do { ?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td>
                    <input type="hidden" name="OFFICERID" class="id_field" value="<?php echo $row_rsOfficers['OFFICERID']; ?>" />
                </td>
                <td><div class="KT_col_L2R_FULLNAME"><strong><?php echo KT_FormatForList($row_rsOfficers['L2R_FULLNAME'], 20); ?></strong></div></td>
                <td><div class="KT_col_CPOSITION"><?php echo KT_FormatForList($row_rsOfficers['CPOSITION'], 20); ?></div></td>
                <td><a class="KT_edit_link" href="officer_remove.php?OFFICERID=<?php echo $row_rsOfficers['OFFICERID']; ?>&amp;BOULENAME=<?php echo $row_rsOfficers['BOULENAME']; ?>&amp;CPOSITION=<?php echo $row_rsOfficers['CPOSITION']; ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> </td>
              </tr>
              <?php } while ($row_rsOfficers = mysql_fetch_assoc($rsOfficers)); ?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsOfficers1->Prepare();
            require("../../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"></div>
        <span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
        </select>
        <a class="KT_additem_op_link" href="officer_add.php?REGIONCD=<?php echo $_GET['REGIONCD']; ?>&amp;CHAPTERCD=<?php echo $_GET['CHAPTERCD']; ?>&amp;UPDATEDBY=<?php echo $_GET['UPDATEDBY']; ?>&amp;KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    <p>Return to Boul&#233; Listing</p>
    </form>
  </div>
  <br class="clearfixplain" />

<p>&nbsp;</p>

<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsOfficers);
?>
