<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listvw_feedback1 = new TFI_TableFilter($conn_sigma_modx, "tfi_listvw_feedback1");
$tfi_listvw_feedback1->addColumn("vw_feedback.req_date", "DATE_TYPE", "req_date", "=");
$tfi_listvw_feedback1->addColumn("vw_feedback.req_cmt", "STRING_TYPE", "req_cmt", "%");
$tfi_listvw_feedback1->addColumn("vw_feedback.FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listvw_feedback1->Execute();

// Sorter
$tso_listvw_feedback1 = new TSO_TableSorter("rsvw_feedback1", "tso_listvw_feedback1");
$tso_listvw_feedback1->addColumn("vw_feedback.req_date");
$tso_listvw_feedback1->addColumn("vw_feedback.req_cmt");
$tso_listvw_feedback1->addColumn("vw_feedback.FULLNAME");
$tso_listvw_feedback1->setDefault("vw_feedback.req_date DESC");
$tso_listvw_feedback1->Execute();

// Navigation
$nav_listvw_feedback1 = new NAV_Regular("nav_listvw_feedback1", "rsvw_feedback1", "../", $_SERVER['PHP_SELF'], 10);

//NeXTenesio3 Special List Recordset
$maxRows_rsvw_feedback1 = $_SESSION['max_rows_nav_listvw_feedback1'];
$pageNum_rsvw_feedback1 = 0;
if (isset($_GET['pageNum_rsvw_feedback1'])) {
  $pageNum_rsvw_feedback1 = $_GET['pageNum_rsvw_feedback1'];
}
$startRow_rsvw_feedback1 = $pageNum_rsvw_feedback1 * $maxRows_rsvw_feedback1;

// Defining List Recordset variable
$NXTFilter_rsvw_feedback1 = "1=1";
if (isset($_SESSION['filter_tfi_listvw_feedback1'])) {
  $NXTFilter_rsvw_feedback1 = $_SESSION['filter_tfi_listvw_feedback1'];
}
// Defining List Recordset variable
$NXTSort_rsvw_feedback1 = "vw_feedback.req_date DESC";
if (isset($_SESSION['sorter_tso_listvw_feedback1'])) {
  $NXTSort_rsvw_feedback1 = $_SESSION['sorter_tso_listvw_feedback1'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_rsvw_feedback1 = "SELECT vw_feedback.req_date, vw_feedback.req_cmt, vw_feedback.FULLNAME, vw_feedback.req_id, vw_feedback.CHAPTERID, vw_feedback.feedback_page FROM vw_feedback WHERE {$NXTFilter_rsvw_feedback1} ORDER BY {$NXTSort_rsvw_feedback1}";
$query_limit_rsvw_feedback1 = sprintf("%s LIMIT %d, %d", $query_rsvw_feedback1, $startRow_rsvw_feedback1, $maxRows_rsvw_feedback1);
$rsvw_feedback1 = mysql_query($query_limit_rsvw_feedback1, $sigma_modx) or die(mysql_error());
$row_rsvw_feedback1 = mysql_fetch_assoc($rsvw_feedback1);

if (isset($_GET['totalRows_rsvw_feedback1'])) {
  $totalRows_rsvw_feedback1 = $_GET['totalRows_rsvw_feedback1'];
} else {
  $all_rsvw_feedback1 = mysql_query($query_rsvw_feedback1);
  $totalRows_rsvw_feedback1 = mysql_num_rows($all_rsvw_feedback1);
}
$totalPages_rsvw_feedback1 = ceil($totalRows_rsvw_feedback1/$maxRows_rsvw_feedback1)-1;
//End NeXTenesio3 Special List Recordset

$nav_listvw_feedback1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: false,
  row_effects: false,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_req_date {width:84px; overflow:hidden;}
  .KT_col_req_cmt {width:280px; overflow:hidden;}
  .KT_col_FULLNAME {width:140px; overflow:hidden;}
</style>
<!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" -->

  <div class="KT_tng" id="listvw_feedback1">
    <h1>ARCHON FEEDBACK</h1>
<div class="contentBlock">
    <div class="KT_tnglist">
      <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
        <div class="KT_options"> <a href="<?php echo $nav_listvw_feedback1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
              <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listvw_feedback1'] == 1) {
?>
                <?php echo $_SESSION['default_max_rows_nav_listvw_feedback1']; ?>
                <?php 
  // else Conditional region1
  } else { ?>
                <?php echo NXT_getResource("all"); ?>
                <?php } 
  // endif Conditional region1
?>
              <?php echo NXT_getResource("records"); ?></a> &nbsp;
          &nbsp; </div>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <thead>
            <tr class="KT_row_order">
              <th>&nbsp;</th>
              <th id="req_date" class="KT_sorter KT_col_req_date <?php echo $tso_listvw_feedback1->getSortIcon('vw_feedback.req_date'); ?>"> <a href="<?php echo $tso_listvw_feedback1->getSortLink('vw_feedback.req_date'); ?>">DATE MADE</a> </th>
              <th id="req_cmt" class="KT_sorter KT_col_req_cmt <?php echo $tso_listvw_feedback1->getSortIcon('vw_feedback.req_cmt'); ?>"> <a href="<?php echo $tso_listvw_feedback1->getSortLink('vw_feedback.req_cmt'); ?>">COMMENT</a> </th>
              <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listvw_feedback1->getSortIcon('vw_feedback.FULLNAME'); ?>"> <a href="<?php echo $tso_listvw_feedback1->getSortLink('vw_feedback.FULLNAME'); ?>">MADE BY</a> </th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($totalRows_rsvw_feedback1 == 0) { // Show if recordset empty ?>
              <tr>
                <td colspan="5"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
              </tr>
              <?php } // Show if recordset empty ?>
            <?php if ($totalRows_rsvw_feedback1 > 0) { // Show if recordset not empty ?>
              <?php do { ?>
                <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                  <td><input type="hidden" name="req_id" class="id_field" value="<?php echo $row_rsvw_feedback1['req_id']; ?>" />
                  </td>
                  <td><div class="KT_col_req_date"><?php echo KT_formatDate($row_rsvw_feedback1['req_date']); ?></div></td>
                  <td><div class="KT_col_req_cmt"><a href="/home/display_feedback.php?req_id=<?php echo $row_rsvw_feedback1['req_id']; ?>&amp;KT_back=1"><?php echo KT_FormatForList($row_rsvw_feedback1['req_cmt'], 40); ?></a></div></td>
                  <td><div class="KT_col_FULLNAME"><?php echo KT_FormatForList($row_rsvw_feedback1['FULLNAME'], 20); ?></div></td>
                  <td></td>
                </tr>
                <?php } while ($row_rsvw_feedback1 = mysql_fetch_assoc($rsvw_feedback1)); ?>
              <?php } // Show if recordset not empty ?>
          </tbody>
        </table>
        <div class="KT_bottomnav">
          <div>
            <?php
            $nav_listvw_feedback1->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsvw_feedback1);
?>
