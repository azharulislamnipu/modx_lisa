<?php

/**
 * Handles the database interactions for the Boule delegation administration 
 * @author Pat Heard {http://fullahead.org}
 */

require_once 'DelegateBean.php';
require_once '/opt/lampp/htdocs/modx_lisa/home/index.php';
define('MODX_API_MODE', true); // Allows this class to use the MODx API

class BouleDelegationDatabaseManager {
        
    private $modx = null;

    /** Initialize the database manager */
    public function __construct(&$modx = null){
        if($modx == null){
            die('You must pass a valid $modx object reference to the BouleDelegationDatabaseManager.');
        }
        $this->modx = $modx; 
    } 
    
    /** Load the delegates for a given Boule */
    public function getDelegation($bouleCode = ''){
        
        $delegateBeans = array();
        
        if(!empty($bouleCode)){
        
            // Get the delegates/alterates for the boule
            $results = $this->modx->db->query('
                SELECT 
                    D.delegate_type, 
                    D.APPOINTED, 
                    concat_ws(_latin1" ",concat(ucase(substr(A.FIRSTNAME,1,1)),lcase(substr(A.FIRSTNAME,2))),concat(ucase(substr(A.MIDDLEINITIAL,1,1)),lcase(substr(A.MIDDLEINITIAL,2))),concat(ucase(substr(A.LASTNAME,1,1)),lcase(substr(A.LASTNAME,2))),A.SUFFIX) AS L2R_FULLNAME2,
                    A.CUSTOMERALTCD
                FROM 
                    spp_delegates D 
                INNER JOIN 
                    spp_archons A
                ON 
                    D.CUSTOMERCD = A.CUSTOMERID 
                WHERE 
                    D.BOULECD = "' . $this->modx->db->escape($bouleCode) . '" 
                    AND D.delegate_type IN("D", "A")
                    AND D.delegate_status = "A"
                ORDER BY
                    D.delegate_type DESC
            ');

            if($this->modx->db->getRecordCount($results) >= 1) {
                while($row = $this->modx->db->getRow($results)){
                    $delegateBeans[] = new DelegateBean(array(
                        'name'          => $row['L2R_FULLNAME2'],
                        'memberNumber'  => $row['CUSTOMERALTCD'],
                        'type'          => $row['delegate_type'],
                        'appointed'     => $row['APPOINTED'] === '1',
                        'ineligible'    => $row['APPOINTED'] === '2'
                    ));                   
                }
            } 
        }
        return $delegateBeans;
        
    }
    
    /** Gets the delegate and alternates count for a given boule in a region */
    public function getDelegationCounts($regionName = '', $bouleName = ''){
        
        // Get the delegate and alternate counts
        $countDelegates = 0;
        $countAlternates = 0;
        
        if(!empty($regionName) && !empty($bouleName)){
            $results = $this->modx->db->select("DELEGATES, ALTERNATES", $this->modx->getFullTableName('vw_delegate_count'), "REGIONNAME='".$this->modx->db->escape($regionName)."' AND BOULENAME='".$this->modx->db->escape($bouleName)."'");
            if($this->modx->db->getRecordCount($results) >= 1) {
                $row = $this->modx->db->getRow($results);
                $countDelegates = intval($row['DELEGATES']);
                $countAlternates = intval($row['ALTERNATES']);
            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR, 'Failed to get delegate/alternate count: ' . mysql_error()); 
            }
        }
        
        return array(
            'count_delegates'   => $countDelegates,
            'count_alternates'   => $countAlternates
        );
    }
    
    /** Updates a given Boule's delegation's appointed status */
    public function setDelegationAppointed($bouleCode = '', $appointedBy = '', $appointed = ''){
        
        $success = false;        
        if(!empty($bouleCode) && !empty($appointedBy)){
        
            $fields = array(
                'APPOINTED'	=> ($appointed === '1' ? '1' : '0'),
                'APPTDATE'	=> date('Y-m-d H:i:s'),
                'APPTBY'	=> substr($appointedBy, 0, 20)
            );

            // Update the delegation
            $result = $this->modx->db->update( $fields, $this->modx->getFullTableName('spp_delegates'), 'BOULECD = "' . $this->modx->db->escape($bouleCode) . '" AND delegate_status = "A"' );

            // Rousing success!
            if($result){
                $success = true;

            // Pitiful error....
            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR, 'Failed to update Boule delegates APPOINTED.spp_delegates field: ' . mysql_error());           
            }
        }        
        return $success;
    }   
    
    /** Returns a list of Boules with finalized delegations */
    public function getFinalizedBoules($regionCode){
        
        $finalizedBoules = false;
        
        if(!empty($regionCode)) {        
            // Get the delegates/alterates for the boule
            $results = $this->modx->db->query('
                SELECT DISTINCT
                    D.BOULECD,
                    B.BOULENAME
                FROM
                    spp_delegates D
                JOIN
                    spp_boule B
                ON
                    D.BOULECD = B.CHAPTERID
                    AND B.REGIONCD = "'.$this->modx->db->escape($regionCode).'"
                WHERE
                    D.APPOINTED = "1"
                ORDER BY 
                    B.BOULENAME ASC
            ');

            $finalizedBoules = array();
            if($this->modx->db->getRecordCount($results) >= 1) {
                while($row = $this->modx->db->getRow($results)){
                    $finalizedBoules[] = array(
                        'BOULECD'     => htmlspecialchars($row['BOULECD']),
                        'BOULENAME'   => htmlspecialchars($row['BOULENAME'])
                    );
                }
            }
        }
        
        return $finalizedBoules;
    }
  
        
}
?>
