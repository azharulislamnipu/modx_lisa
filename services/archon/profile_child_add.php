<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("GENDER", true, "text", "", "", "", "");
$formValidation->addField("BIRTHDATE", true, "date", "", "", "", "");
$formValidation->addField("FIRSTNAME", true, "text", "", "", "", "");
$formValidation->addField("LASTNAME", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an insert transaction instance
$ins_spp_children = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_children);
// Register triggers
$return = !empty( $_POST['RETURN'] ) ? $_POST['RETURN'] : '/home/personal-profile.php';
$ins_spp_children->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_children->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_children->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$ins_spp_children->setTable("spp_children");
$ins_spp_children->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID", "{POST.CUSTOMERID}");
$ins_spp_children->addColumn("SPOUSEID", "NUMERIC_TYPE", "POST", "SPOUSEID", "{POST.SPOUSEID}");
$ins_spp_children->addColumn("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID", "{POST.ARCHOUSAID}");
$ins_spp_children->addColumn("GENDER", "STRING_TYPE", "POST", "GENDER");
$ins_spp_children->addColumn("BIRTHDATE", "DATE_TYPE", "POST", "BIRTHDATE");
$ins_spp_children->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$ins_spp_children->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$ins_spp_children->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$ins_spp_children->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$ins_spp_children->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY", "{SESSION.webShortname}");
$ins_spp_children->addColumn("LASTUPDATE", "DATE_TYPE", "POST", "LASTUPDATE", "{NOW_DT}");
$ins_spp_children->setPrimaryKey("CHILDID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_children = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_children);
// Register triggers
$upd_spp_children->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_children->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_children->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$upd_spp_children->setTable("spp_children");
$upd_spp_children->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");
$upd_spp_children->addColumn("SPOUSEID", "NUMERIC_TYPE", "POST", "SPOUSEID");
$upd_spp_children->addColumn("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");
$upd_spp_children->addColumn("GENDER", "STRING_TYPE", "POST", "GENDER");
$upd_spp_children->addColumn("BIRTHDATE", "DATE_TYPE", "POST", "BIRTHDATE");
$upd_spp_children->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$upd_spp_children->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$upd_spp_children->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$upd_spp_children->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$upd_spp_children->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_children->addColumn("LASTUPDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_children->setPrimaryKey("CHILDID", "NUMERIC_TYPE", "POST", "CHILDID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_children = $tNGs->getRecordset("spp_children");
$row_rsspp_children = mysql_fetch_assoc($rsspp_children);
$totalRows_rsspp_children = mysql_num_rows($rsspp_children);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../includes/resources/calendar.js"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>

  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">
    <h1 style="text-transform:uppercase;">
      <?php
// Show IF Conditional region1
if (@$_POST['CHILDID'] == "") {
?>
        <?php echo NXT_getResource("Add"); ?>
        <?php
// else Conditional region1
} else { ?>
        <?php echo NXT_getResource("Update"); ?>
        <?php }
// endif Conditional region1
?>
       Child</h1>
    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php
// Show IF Conditional region1
if (@$totalRows_rsspp_children > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php }
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <tr>
              <td class="KT_th"><label for="GENDER_<?php echo $cnt1; ?>_1">GENDER:</label></td>
              <td><div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_children['GENDER']),"M"))) {echo "CHECKED";} ?> type="radio" name="GENDER_<?php echo $cnt1; ?>" id="GENDER_<?php echo $cnt1; ?>_1" value="M" />
                  <label for="GENDER_<?php echo $cnt1; ?>_1">Male</label>
                </div>
                  <div>
                    <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_children['GENDER']),"F"))) {echo "CHECKED";} ?> type="radio" name="GENDER_<?php echo $cnt1; ?>" id="GENDER_<?php echo $cnt1; ?>_2" value="F" />
                    <label for="GENDER_<?php echo $cnt1; ?>_2">Female</label>
                  </div>
                <?php echo $tNGs->displayFieldError("spp_children", "GENDER", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="BIRTHDATE_<?php echo $cnt1; ?>">BIRTH DATE:</label></td>
              <td><input name="BIRTHDATE_<?php echo $cnt1; ?>" id="BIRTHDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_children['BIRTHDATE']); ?>" size="10" maxlength="22" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("BIRTHDATE");?> <?php echo $tNGs->displayFieldError("spp_children", "BIRTHDATE", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="FIRSTNAME_<?php echo $cnt1; ?>">FIRST NAME:</label></td>
              <td><input type="text" name="FIRSTNAME_<?php echo $cnt1; ?>" id="FIRSTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_children['FIRSTNAME']); ?>" size="20" maxlength="20" />
                  <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("spp_children", "FIRSTNAME", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="MIDDLEINITIAL_<?php echo $cnt1; ?>">MIDDLE NAME:</label></td>
              <td><input type="text" name="MIDDLEINITIAL_<?php echo $cnt1; ?>" id="MIDDLEINITIAL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_children['MIDDLEINITIAL']); ?>" size="20" maxlength="20" />
                  <?php echo $tNGs->displayFieldHint("MIDDLEINITIAL");?> <?php echo $tNGs->displayFieldError("spp_children", "MIDDLEINITIAL", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="LASTNAME_<?php echo $cnt1; ?>">LAST NAME:</label></td>
              <td><input type="text" name="LASTNAME_<?php echo $cnt1; ?>" id="LASTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_children['LASTNAME']); ?>" size="20" maxlength="20" />
                  <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("spp_children", "LASTNAME", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="SUFFIX_<?php echo $cnt1; ?>">SUFFIX:</label></td>
              <td><input type="text" name="SUFFIX_<?php echo $cnt1; ?>" id="SUFFIX_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_children['SUFFIX']); ?>" size="20" maxlength="20" />
                  <?php echo $tNGs->displayFieldHint("SUFFIX");?> <?php echo $tNGs->displayFieldError("spp_children", "SUFFIX", $cnt1); ?> </td>
            </tr>
          </table>
          <input type="hidden" name="kt_pk_spp_children_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_children['kt_pk_spp_children']); ?>" />
          <input type="hidden" name="CUSTOMERID_<?php echo $cnt1; ?>" id="CUSTOMERID_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['CUSTOMERID']); ?>" />
          <input type="hidden" name="SPOUSEID_<?php echo $cnt1; ?>" id="SPOUSEID_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['SPOUSEID']); ?>" />
          <input type="hidden" name="ARCHOUSAID_<?php echo $cnt1; ?>" id="ARCHOUSAID_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['ARCHOUSAID']); ?>" />
          <input type="hidden" name="UPDATEDBY_<?php echo $cnt1; ?>" id="UPDATEDBY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['UPDATEDBY']); ?>" />
          <input type="hidden" name="SPOUSEID<?php echo $cnt1; ?>" id="SPOUSEID<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['SPOUSEID']); ?>" />
          <input type="hidden" name="LASTUPDATE_<?php echo $cnt1; ?>" id="LASTUPDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_children['LASTUPDATE']); ?>" />
          <input type="hidden" name="RETURN" id="RETURN" value="<?php echo KT_escapeAttribute($_POST['RETURN']); ?>" />
          <?php } while ($row_rsspp_children = mysql_fetch_assoc($rsspp_children)); ?>
        <div class="KT_bottombuttons">
          <div>
            <?php
      // Show IF Conditional region1
      if (@$_POST['CHILDID'] == "") {
      ?>
              <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Add"); ?>" />
              <?php
      // else Conditional region1
      } else { ?>
              <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <?php }
      // endif Conditional region1
      ?>
            <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '<?php echo $return ?>')" />
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>

<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
