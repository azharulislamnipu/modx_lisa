<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("CUSTOMERCD", true, "numeric", "", "", "", "");
$formValidation->addField("CPOSITION", true, "text", "", "", "", "");
$formValidation->addField("JOINDATE", true, "date", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("tonny.joseph@sigmapiphi.org");
  $emailObj->setCC("{rsDataManager.WEB_EMAIL}");
  $emailObj->setBCC("webmaster@sigmapiphi.org");
  $emailObj->setSubject("NEW OFFICER | {CPOSITION}, {CHAPTERCD} Boul�");
  //WriteContent method
  $emailObj->setContent("<table>\n<tr>\n<td>{CHAPTERCD} Boul� has a new {CPOSITION}. His CUSTOMERCD is {CUSTOMERCD}.\n</td>\n</tr>\n<tr>\n<td>Submitted by {rsDataManager.L2R_FULLNAME} Grammateus.\n</td>\n</tr>\n</table>");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchons = "-1";
if (isset($_GET['CHAPTERCD'])) {
  $colname_rsArchons = $_GET['CHAPTERCD'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchons = sprintf("SELECT * FROM vw_gramm_archons WHERE CHAPTERID = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsArchons, "text"));
$rsArchons = mysql_query($query_rsArchons, $sigma_modx) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysql_num_rows($rsArchons);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOffice = "SELECT * FROM spp_cposition WHERE CPOSITION NOT LIKE '%regio%' AND CPOSITION NOT LIKE '%rand%' ORDER BY CPOSITIONID ASC";
$rsOffice = mysql_query($query_rsOffice, $sigma_modx) or die(mysql_error());
$row_rsOffice = mysql_fetch_assoc($rsOffice);
$totalRows_rsOffice = mysql_num_rows($rsOffice);

$colname_rsDataManager = "-1";
if (isset($_GET['GRAMMID'])) {
  $colname_rsDataManager = $_GET['GRAMMID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsDataManager = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname_rsDataManager, "int"));
$rsDataManager = mysql_query($query_rsDataManager, $sigma_modx) or die(mysql_error());
$row_rsDataManager = mysql_fetch_assoc($rsDataManager);
$totalRows_rsDataManager = mysql_num_rows($rsDataManager);

// Make an insert transaction instance
$ins_spp_officers = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_officers);
// Register triggers
$ins_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archon-management-update.php");
$ins_spp_officers->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_officers->setTable("spp_officers");
$ins_spp_officers->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$ins_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "POST", "CPOSITION");
$ins_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$ins_spp_officers->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$ins_spp_officers->addColumn("CHAPTERCD", "STRING_TYPE", "POST", "CHAPTERCD", "{GET.CHAPTERCD}");
$ins_spp_officers->addColumn("REGIONCD", "STRING_TYPE", "POST", "REGIONCD", "{GET.REGIONCD}");
$ins_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "POST", "COMMITTEESTATUSSTT", "Active");
$ins_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "POST", "UPDATEUSERCD", "{GET.UPDATEDBY}");
$ins_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "POST", "UPDATETMS", "{NOW_DT}");
$ins_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_officers = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_officers);
// Register triggers
$upd_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archon-management-update.php");
$upd_spp_officers->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$upd_spp_officers->setTable("spp_officers");
$upd_spp_officers->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$upd_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "POST", "CPOSITION");
$upd_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$upd_spp_officers->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$upd_spp_officers->addColumn("CHAPTERCD", "STRING_TYPE", "POST", "CHAPTERCD");
$upd_spp_officers->addColumn("REGIONCD", "STRING_TYPE", "POST", "REGIONCD");
$upd_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "POST", "COMMITTEESTATUSSTT");
$upd_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "POST", "UPDATEUSERCD");
$upd_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "POST", "UPDATETMS");
$upd_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE", "GET", "OFFICERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_officers = $tNGs->getRecordset("spp_officers");
$row_rsspp_officers = mysql_fetch_assoc($rsspp_officers);
$totalRows_rsspp_officers = mysql_num_rows($rsspp_officers);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_data_man.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">ADD OFFICER</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">
    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_officers > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php } 
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <tr>
              <td class="KT_th"><label for="CUSTOMERCD_<?php echo $cnt1; ?>">CUSTOMERCD:</label></td>
              <td><select name="CUSTOMERCD_<?php echo $cnt1; ?>" id="CUSTOMERCD_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                  <?php 
do {  
?>
                  <option value="<?php echo $row_rsArchons['CUSTOMERID']?>"<?php if (!(strcmp($row_rsArchons['CUSTOMERID'], $row_rsspp_officers['CUSTOMERCD']))) {echo "SELECTED";} ?>><?php echo $row_rsArchons['FULLNAME']?></option>
                  <?php
} while ($row_rsArchons = mysql_fetch_assoc($rsArchons));
  $rows = mysql_num_rows($rsArchons);
  if($rows > 0) {
      mysql_data_seek($rsArchons, 0);
	  $row_rsArchons = mysql_fetch_assoc($rsArchons);
  }
?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_officers", "CUSTOMERCD", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="CPOSITION_<?php echo $cnt1; ?>">OFFICE:</label></td>
              <td><select name="CPOSITION_<?php echo $cnt1; ?>" id="CPOSITION_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                  <?php 
do {  
?>
                  <option value="<?php echo $row_rsOffice['CPOSITION']?>"<?php if (!(strcmp($row_rsOffice['CPOSITION'], $row_rsspp_officers['CPOSITION']))) {echo "SELECTED";} ?>><?php echo $row_rsOffice['CPOSITION']?></option>
                  <?php
} while ($row_rsOffice = mysql_fetch_assoc($rsOffice));
  $rows = mysql_num_rows($rsOffice);
  if($rows > 0) {
      mysql_data_seek($rsOffice, 0);
	  $row_rsOffice = mysql_fetch_assoc($rsOffice);
  }
?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_officers", "CPOSITION", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="JOINDATE_<?php echo $cnt1; ?>">INSTALLATION DATE:</label></td>
              <td><input name="JOINDATE_<?php echo $cnt1; ?>" id="JOINDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_officers['JOINDATE']); ?>" size="10" maxlength="22" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("JOINDATE");?> <?php echo $tNGs->displayFieldError("spp_officers", "JOINDATE", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMMENT_<?php echo $cnt1; ?>">COMMENT:</label></td>
              <td><textarea name="COMMENT_<?php echo $cnt1; ?>" id="COMMENT_<?php echo $cnt1; ?>" cols="80" rows="8"><?php echo KT_escapeAttribute($row_rsspp_officers['COMMENT']); ?></textarea>
                  <?php echo $tNGs->displayFieldHint("COMMENT");?> <?php echo $tNGs->displayFieldError("spp_officers", "COMMENT", $cnt1); ?> </td>
            </tr>
          </table>
          <input type="hidden" name="kt_pk_spp_officers_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_officers['kt_pk_spp_officers']); ?>" />
          <input type="hidden" name="CHAPTERCD_<?php echo $cnt1; ?>" id="CHAPTERCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_GET['CHAPTERCD']); ?>" />
          <input type="hidden" name="REGIONCD_<?php echo $cnt1; ?>" id="REGIONCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_GET['REGIONCD']); ?>" />
          <input type="hidden" name="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" id="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_officers['COMMITTEESTATUSSTT']); ?>" />
          <input type="hidden" name="UPDATEUSERCD_<?php echo $cnt1; ?>" id="UPDATEUSERCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_GET['UPDATEDBY']); ?>" />
          <input type="hidden" name="UPDATETMS_<?php echo $cnt1; ?>" id="UPDATETMS_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_officers['UPDATETMS']); ?>" />
          <?php } while ($row_rsspp_officers = mysql_fetch_assoc($rsspp_officers)); ?>
        <div class="KT_bottombuttons">
          <div>
            <?php 
      // Show IF Conditional region1
      if (@$_GET['OFFICERID'] == "") {
      ?>
              <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
              <?php 
      // else Conditional region1
      } else { ?>
              <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <?php }
      // endif Conditional region1
      ?>
            <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../../includes/nxt/back.php')" />
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchons);

mysql_free_result($rsOffice);

mysql_free_result($rsDataManager);
?>
