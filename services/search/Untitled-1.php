<?php require_once('../Connections/sigma_remote.php'); ?>
<?php
// Load the XML classes
require_once('../includes/XMLExport/XMLExport.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_remote, $sigma_remote);
$query_rsLastname = "SELECT * FROM vw_web_elig WHERE LASTNAME = 'Brown' ORDER BY L2R_FULLNAME2 ASC";
$rsLastname = mysql_query($query_rsLastname, $sigma_remote) or die(mysql_error());
$row_rsLastname = mysql_fetch_assoc($rsLastname);
$totalRows_rsLastname = mysql_num_rows($rsLastname);

// Begin XMLExport rsLastname
$xmlExportObj = new XMLExport();
$xmlExportObj->setRecordset($rsLastname);
$xmlExportObj->addColumn("CUSTOMERID", "CUSTOMERID");
$xmlExportObj->addColumn("REGIONNAME", "REGIONNAME");
$xmlExportObj->addColumn("REGIONCD", "REGIONCD");
$xmlExportObj->addColumn("BOULENAME", "BOULENAME");
$xmlExportObj->addColumn("CHAPTERID", "CHAPTERID");
$xmlExportObj->addColumn("BOULECITY", "BOULECITY");
$xmlExportObj->addColumn("BOULESTATECD", "BOULESTATECD");
$xmlExportObj->addColumn("STATUS", "STATUS");
$xmlExportObj->addColumn("STATUSSTT", "STATUSSTT");
$xmlExportObj->addColumn("CUSTOMERCLASSSTT", "CUSTOMERCLASSSTT");
$xmlExportObj->addColumn("CUSTOMERTYPE", "CUSTOMERTYPE");
$xmlExportObj->addColumn("JOINDATE", "JOINDATE");
$xmlExportObj->addColumn("PREFIX", "PREFIX");
$xmlExportObj->addColumn("FIRSTNAME", "FIRSTNAME");
$xmlExportObj->addColumn("MIDDLEINITIAL", "MIDDLEINITIAL");
$xmlExportObj->addColumn("LASTNAME", "LASTNAME");
$xmlExportObj->addColumn("SUFFIX", "SUFFIX");
$xmlExportObj->addColumn("DESIGNATIONLST", "DESIGNATIONLST");
$xmlExportObj->addColumn("L2R_FULLNAME", "L2R_FULLNAME");
$xmlExportObj->addColumn("FULLNAME", "FULLNAME");
$xmlExportObj->addColumn("FULLNAME2", "FULLNAME2");
$xmlExportObj->addColumn("L2R_FULLNAME2", "L2R_FULLNAME2");
$xmlExportObj->addColumn("ADDRESS1", "ADDRESS1");
$xmlExportObj->addColumn("ADDRESS2", "ADDRESS2");
$xmlExportObj->addColumn("CITY", "CITY");
$xmlExportObj->addColumn("STATECD", "STATECD");
$xmlExportObj->addColumn("ZIP", "ZIP");
$xmlExportObj->addColumn("HOMEPHONE", "HOMEPHONE");
$xmlExportObj->addColumn("EMAIL", "EMAIL");
$xmlExportObj->addColumn("JOBTITLE", "JOBTITLE");
$xmlExportObj->addColumn("OCCUPATIONCD", "OCCUPATIONCD");
$xmlExportObj->addColumn("SKILLCDLST", "SKILLCDLST");
$xmlExportObj->addColumn("ORGNAME", "ORGNAME");
$xmlExportObj->addColumn("ALTADDRESS1", "ALTADDRESS1");
$xmlExportObj->addColumn("ALTADDRESS2", "ALTADDRESS2");
$xmlExportObj->addColumn("ALTCITY", "ALTCITY");
$xmlExportObj->addColumn("ALTSTATE", "ALTSTATE");
$xmlExportObj->addColumn("ALTZIP", "ALTZIP");
$xmlExportObj->addColumn("WORKPHONE", "WORKPHONE");
$xmlExportObj->addColumn("ALTEMAIL", "ALTEMAIL");
$xmlExportObj->addColumn("BIRTHDATE", "BIRTHDATE");
$xmlExportObj->addColumn("SPOUSENAME", "SPOUSENAME");
$xmlExportObj->addColumn("LASTUPDATED", "LASTUPDATED");
$xmlExportObj->addColumn("UPDATEDBY", "UPDATEDBY");
$xmlExportObj->addColumn("GRAMMUPDATE", "GRAMMUPDATE");
$xmlExportObj->addColumn("GRAMM", "GRAMM");
$xmlExportObj->addColumn("IMAGE", "IMAGE");
$xmlExportObj->addColumn("DECEASEDDATE", "DECEASEDDATE");
$xmlExportObj->addColumn("WEB_EMAIL", "WEB_EMAIL");
$xmlExportObj->addColumn("CPOSITION", "CPOSITION");
$xmlExportObj->addColumn("COMMITTEESTATUSSTT", "COMMITTEESTATUSSTT");
$xmlExportObj->addColumn("WEB_ID", "WEB_ID");
$xmlExportObj->setMaxRecords("ALL");
$xmlExportObj->setDBEncoding("ISO-8859-1");
$xmlExportObj->setXMLEncoding("ISO-8859-1");
$xmlExportObj->setXMLFormat("NODES");
$xmlExportObj->Execute();
// End XMLExport rsLastname
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

</body>
</html>
<?php
mysql_free_result($rsLastname);
?>
