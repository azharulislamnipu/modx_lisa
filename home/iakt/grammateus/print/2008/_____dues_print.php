<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// begin Recordset
$colname__rsRoster = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsRoster = $_SESSION['BOULENAME'];
}
$query_rsRoster = sprintf("SELECT vw_gramm_archons.CUSTOMERID,   vw_gramm_archons.BOULENAME,   vw_gramm_archons.`STATUS` AS `CURRENT_STATUS`,   vw_gramm_archons.JOINDATE,   vw_gramm_archons.L2R_FULLNAME,   vw_gramm_archons.FULLNAME, vw_gramm_archons.ADDRESS1,   vw_gramm_archons.CITY,   vw_gramm_archons.STATECD,   vw_gramm_archons.ZIP,   vw_gramm_archons.HOMEPHONE,   vw_gramm_archons.WEB_EMAIL,   spp_dues.status_req AS REQUESTED_STATUS,   spp_dues.dues_amt,   spp_dues.addl_amt,   spp_dues.dues_amt + spp_dues.addl_amt AS TOTAL_AMT,   spp_dues.dues_cmt FROM   vw_gramm_archons   LEFT OUTER JOIN spp_dues ON (vw_gramm_archons.CUSTOMERID = spp_dues.CUSTOMERCD) WHERE BOULENAME = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname__rsRoster, "text"));
$rsRoster = $sigma_modx->SelectLimit($query_rsRoster) or die($sigma_modx->ErrorMsg());
$totalRows_rsRoster = $rsRoster->RecordCount();
// end Recordset
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng">
<h1>2008 Membership Report Archon Listing</h1>
<div style="padding: 2px 12px; font-size: 16px; border-bottom: black solid 1px; margin-bottom: 8px;">
<p>Print this document and return it with your 2008 Membership Reporting documents.</p>
<p>If you have not added any payments, Archons will have no payment listed. </p>
<p>[*print_doc*]</p>
</div>
<!-- p>Print this document: [*print_doc*]</p-->
<table cellspacing="0" cellpadding="2" class="KT_tngtable">
  <tr valign="bottom">
    <td width="350">ARCHON</td>
    <td><p>CURRENT<br />
    STATUS</p>      </td>
    <td>REQUESTED<br />
      STATUS</td>
    <td>2008<br />
      DUES </td>
    <td>ADD'L<br />
      DUES </td>
    <td>TOTAL<br />
      SUBMITTED </td>
  </tr>
  <?php
  while (!$rsRoster->EOF) { 
?>
    <tr valign="top">
      <td width="350"><strong><?php echo $rsRoster->Fields('FULLNAME'); ?></strong><br>
          <?php echo $rsRoster->Fields('ADDRESS1'); ?>, <?php echo $rsRoster->Fields('CITY'); ?>, <?php echo $rsRoster->Fields('STATECD'); ?> <?php echo $rsRoster->Fields('ZIP'); ?><br>
        Phone: <?php echo $rsRoster->Fields('HOMEPHONE'); ?> Email: <?php echo $rsRoster->Fields('WEB_EMAIL'); ?></td>
      <td><?php echo $rsRoster->Fields('CURRENT_STATUS'); ?></td>
      <td><?php 
// Show IF Conditional region1 
if (@$rsRoster->Fields('REQUESTED_STATUS') != NULL) {
?>
          <?php echo $rsRoster->Fields('REQUESTED_STATUS'); ?>
          <?php 
// else Conditional region1
} else { ?>
          --
  <?php } 
// endif Conditional region1
?></td>
      <td><?php 
// Show IF Conditional region2 
if (@$rsRoster->Fields('dues_amt') != NULL) {
?>
          <?php echo $rsRoster->Fields('dues_amt'); ?>
          <?php 
// else Conditional region2
} else { ?>
          -0-
  <?php } 
// endif Conditional region2
?></td>
      <td><?php 
// Show IF Conditional region3 
if (@$rsRoster->Fields('addl_amt') != NULL) {
?>
          <?php echo $rsRoster->Fields('addl_amt'); ?>
          <?php 
// else Conditional region3
} else { ?>
          -0-
  <?php } 
// endif Conditional region3
?></td>
      <td><?php 
		  $dues =  $rsRoster->Fields('dues_amt');
		  $more =  ($rsRoster->Fields('addl_amt') == NULL ) ? 0  : $rsRoster->Fields('addl_amt');
		  $all =  $dues + $more;
		  echo $all; 
		  ?></td>
    </tr>
    <?php
    $rsRoster->MoveNext(); 
  }
?>
</table>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsRoster->Close();
?>
