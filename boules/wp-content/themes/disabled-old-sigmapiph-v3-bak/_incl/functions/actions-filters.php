<?php


/*
 * Define Sigma theme's custom actions and filters
 */

 require_once(dirname(__FILE__) . '/../widgets/regional-boules-widget.php');
 require_once(dirname(__FILE__) . '/../widgets/recent-updates-widget.php');
 require_once('template-functions.php');

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function sigma_theme_setup() {

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size(630, 180, true);

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'sigma-pi-phi' ) );

	// Add a way for the custom header to be styled in the admin panel that controls custom headers.
	define( 'HEADER_IMAGE', get_stylesheet_directory_uri() . '/images/bg/header-black.jpg' );
	define( 'HEADER_TEXTCOLOR', 'FFFFFF' );

	add_theme_support( 'custom-header', $custom_header_args );

}
add_action( 'after_setup_theme', 'sigma_theme_setup' );

/**
 * Register widget areas
 */
function sigma_theme_widgets_init() {

	// Add sidebar widget area
	register_sidebar( array(
		'name'          => 'Sidebar',
		'id'            => 'sidebar-1',
		'description'   => __( 'The sidebar widget area', 'archon-network' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>'
	) );

	// Add Sigma Post Aggregate and Boule list widgets
	if(is_multisite_primary() || is_regional_site()) {
		register_widget('RecentUpdates_Widget');
	}
	register_widget('RegionalBoules_Widget');

	// Remove unused widgets
	unregister_widget('WP_Widget_Pages');
	unregister_widget('WP_Widget_Calendar');
	unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Widget_Meta');
	unregister_widget('WP_Widget_Categories');
	unregister_widget('WP_Widget_Recent_Posts');
	unregister_widget('WP_Widget_Recent_Comments');
	unregister_widget('WP_Widget_RSS');
	unregister_widget('WP_Widget_Tag_Cloud');
	unregister_widget('WP_Nav_Menu_Widget');

}
add_action( 'widgets_init', 'sigma_theme_widgets_init', 11 );


/**
 * Add custom styles
 */
function sigma_enqueue() {
	wp_enqueue_script('site', get_stylesheet_directory_uri() . '/_incl/js/site.js', array(), '1.0', true);
	wp_enqueue_script('fancybox', get_stylesheet_directory_uri() . '/_incl/js/fancybox/source/jquery.fancybox.min.js', array('jquery'), '2.1.5', true);
	wp_enqueue_script('fancybox', get_stylesheet_directory_uri() . '/_incl/js/fancybox/source/jquery.fancybox.min.js', array('jquery'), '2.1.5', true);
	wp_enqueue_script('fancybox-buttons', get_stylesheet_directory_uri() . '/_incl/js/fancybox/source/helpers/jquery.fancybox-buttons.js', array('fancybox'), '2.1.5', true);
	wp_enqueue_script('fancybox-media', get_stylesheet_directory_uri() . '/_incl/js/fancybox/source/helpers/jquery.fancybox-media.js', array('fancybox'), '2.1.5', true);

	wp_enqueue_style('fancybox', get_stylesheet_directory_uri() . '/_incl/js/fancybox/source/jquery.fancybox.css', array(), '2.1.5');
}
add_action( 'wp_enqueue_scripts', 'sigma_enqueue');


/**
 * Creates the coloured heading bars for post and page titles
 */
function sigma_before_post(){
    global $post;

    $color = get_post_meta( $post->ID, '_title_bar_color', true);
    $color = !empty($color) ? $color : 'red';

    echo '<div class="'.$color.'">';
}
add_action('sigma_before_blog_page', 'sigma_before_post');
add_action('sigma_before_blog_post', 'sigma_before_post');
add_action('sigma_before_blog_single_post', 'sigma_before_post');


function sigma_after_post(){
    echo '</div>';
}
add_action('sigma_after_blog_page', 'sigma_after_post');
add_action('sigma_after_blog_post', 'sigma_after_post');
add_action('sigma_after_blog_single_post', 'sigma_after_post');



/*
 * Sets a post or page as private if it is assigned a private category
 */
function set_privacy_from_categories($postid) {
	global $wpdb;

	// Get true post ID
	if ($parent_id = wp_is_post_revision($postid)){
		$postid = $parent_id;
	}

	// Get categories for post
	$categories = wp_get_post_categories($postid);
	foreach($categories as $c){
		$category = get_category( $c );

		// Found a private category?  Set the post to "private"
		if( is_private_category( $category ) ){
			$wpdb->query( $wpdb->prepare("UPDATE $wpdb->posts SET `post_status` = %s WHERE ID = %d AND `post_status` = %s", 'private', (int)$postid, 'publish') );
			break;
		}
	}

}
add_action('save_post', 'set_privacy_from_categories');


function sigma_link_pages_args($args){
    $args['before'] = __( '<p id="pages"><strong>Pages</strong> ', 'archon-network' );
    $args['link_before'] = '<span class="link">';
    $args['link_after'] = '</span>';
    return $args;
}
add_filter('wp_link_pages_args', 'sigma_link_pages_args');


/*
 * Fixes the page title
 */
function sigma_page_title($title){

    global $post, $wp_query, $current_blog;

    if ( is_front_page() || is_home() ) {
        $title = __( 'Home', 'archon-network' );
	} else if ( is_single() ) {
        $title = __( 'Blog &#124; ' . $post->post_title, 'archon-network' );
    } else if ( is_category() ) {
        $title = __( 'Blog &#124; Categories &#124; ' . ucwords( str_replace('-', ' ', $wp_query->query_vars['category_name'] ) ), 'archon-network' );
    } else if ( is_tag() ) {
        $title = __( 'Blog &#124; Tags &#124; ' . ucwords( $wp_query->query_vars['tag'] ), 'archon-network' );
    } else if ( is_page() ){
        $title = $post->post_title;
    } else {
        $title = __( 'Blog', 'archon-network' );
	}

	$title = preg_replace('/Blog(\s&#124;)?/', '', esc_attr( $title ));
    echo get_bloginfo('name') . (strlen($title) > 0 ? ' &#124; ' : '') . $title;

}
add_filter('wp_title', 'sigma_page_title');




/**
 * Change the excerpt length
 */
function sigma_excerpt_length($length) {
    return 20;
}
add_filter('excerpt_length', 'sigma_excerpt_length');



/**
 * Change the characters after the end of an excerpt
 */
function sigma_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'sigma_excerpt_more');


/**
 * Custom comment avatar
 */
function custom_avatar ($avatar_defaults) {
	$src = get_stylesheet_directory_uri() .'/images/default-avatar.png';
	$avatar_defaults[$src] = "Sigma Pi Phi Avatar";
	return $avatar_defaults;
}
add_filter( 'avatar_defaults', 'custom_avatar' );


/**
 * Dynamically removes the no-js class from the <body> element.
 */
function sigma_theme_remove_nojs_class() {
?><script type="text/javascript">//<![CDATA[
(function(){
	var doc = document.documentElement;
	doc.className = doc.className.replace(/(^|\s)no-js(\s|$)/, '$1js$2');
})();
//]]></script>
<?php
}
add_action( 'wp_head', 'sigma_theme_remove_nojs_class' );


/**
 * Prepopulate the user's Boule and username for the submit page of the business directory
 */
function wpbdp_prepopulate_listing_fields( $html, $field = null, $value = null, $render_context = null ) {

	global $current_user;

	// Only prepopulate if the form hasn't been submitted
	if ( !isset( $_POST['listingfields'] ) && $field != null && $value === '' && $render_context === 'submit' ) {

		get_currentuserinfo();

		switch ( $field->get_label() ) {
			case "Boulé":
				// Loop through the user's blogs and find theirs
				$blogs = get_blogs_of_user( $current_user->ID );
				foreach( $blogs as $blog ){
					if( $blog->userblog_id !== 1 ){
						$html = str_replace( 'value=""', 'value="' . $blog->blogname . '"', $html );
						break;
					}
				}
				break;
			case "Username":
				$html = str_replace( 'value=""', 'value="' . $current_user->user_login . '"', $html );
				break;
		}
	}
	return $html;
}
add_filter('wpbdp_render_field_inner', 'wpbdp_prepopulate_listing_fields', 10, 5 );


?>