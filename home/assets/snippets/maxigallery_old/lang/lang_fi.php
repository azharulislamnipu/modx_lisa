<?php
// Finnish language file
// If you are going to make your own language file and are usign the lighbox, 
// make a language file for it too, f.ex. /maxigallery/slidebox/slidebox_lang_fi.js
$strings['previous']="&laquo; edellinen";
$strings['next']="seuraava &raquo;";
$strings['click_to_zoom']=" &raquo; Klikkaa avataksesi suurempana ->";
$strings['click_to_go_back']="<- Klikkaa menn&auml;ksesi takaisin";
$strings['click_to_open_original']= "Klikkaa avataksesi ison kuvan";
$strings['back_to_normal_view'] = "Takaisin";
$strings['index']="takaisin";
$strings['go_to_gallery']="Mene kuvagalleriaan ->";
$strings['pictures_successfully_uploaded']="<b>Kuvat ladattu onnistuneesti.</b>";
$strings['changes_have_been_saved']="<b>Muutokset talletettu.</b>";
$strings['delete']="Poista";
$strings['title']="Otsikko";
$strings['save_changes']="Tallenna muutokset";
$strings['upload_pictures']="L&auml;het&auml; kuvat";
$strings['check_to_delete_this_picture']="Valitse poistaaksesi kuvan";
$strings['manage_pictures']="Hallitse kuvia";
$strings['date']="P&auml;iv&auml;ys";
$strings['description']="Kuvaus";
$strings['position']="Sijainti";
$strings['gallery_description']="Kuvagallerian kuvaus";
$strings['db_no_descr_support']="<em>Varoitus: Gallerian tietokannan taulu ei salli kuvauksien k&auml;ytt&ouml;&auml;!</em><br />";
$strings['db_no_pos_support']="<em>Varoitus: Gallerian tietokannan taulu ei salli kuvien j&auml;rjest&auml;mist&auml;!</em><br />";
$strings['picture'] = "Kuva:";
$strings['max_pics_reached'] = "Olet saavuttanut gallerialle asetetun kuvien maksimim&auml;&auml;r&auml;n, joten et voi l&auml;hett&auml;&auml; enemp&auml;&auml; kuvia.";
$strings['supported_types'] = "<b>Tiedostotyyppi virheellinen!</b> Galleria tukee vain seuraavia tiedostomuotoja: jpg/jpeg/png/gif ja zip (joiden sis&auml;ll&auml; em. kuvatiedostot)";
$strings['gif_not_supported'] = "<b>Palvelimen GD-kirjasto ei tue gif-tiedostotyyppi&auml;.</b> K&auml;yt&auml; vain jpg/png kuvia.";
$strings['supported_types_inzip'] = "Virhe: Pakatun tiedoston sis&auml;ll&auml; oli virheellisi&auml; tiedostotyyppej&auml;, n&auml;it&auml; tiedostoja ei kopioitu.";
$strings['max_pics_reached_some_discarded'] = "<b>Maksimi m&auml;&auml;r&auml; kuvia saavutettiin!</b> Kaikkia kuvia ei v&auml;ltt&auml;m&auml;tt&auml; ladattu.";
$strings['zip_not_supported'] = "<b>Sinun PHP-asennus ei tue .zip tiedostoja!</b> Ota yhteytt&auml; paveluntarjoajaasi ja pyyd&auml; heit&auml; asentamaan <a href=\"http://www.php.net/manual/ref.zip.php\">PHP:n zip laajennukset.</a>";
$strings['database_error'] = "Virhe: Gallerian tarvitsemaa tietokantaa ei voitu luoda!";
$strings['database_error_ownid'] = "Virhe: Ei voitu luoda own_id saraketta tietokanta tauluun!";
$strings['invalid_class'] = "Virhe: Maxigallery luokan tiedostoa ei löydy, ole hyvä ja tarkasta että kaikki tiedostot ovat oikeassa paikassa!";
$strings['invalid_name_class'] = "Virhe: maxigallery luokan nimi virheellinen. ";
?>