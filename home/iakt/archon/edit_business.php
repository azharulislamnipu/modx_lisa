<?php
//Connection statement
require_once('../Connections/sigma_modx.php');

//Aditional Functions
require_once('../includes/functions.inc.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("JOBTITLE", true, "text", "", "2", "", "");
$formValidation->addField("ORGNAME", true, "text", "", "5", "", "");
$formValidation->addField("ALTADDRESS1", true, "text", "", "5", "", "");
$formValidation->addField("ALTCITY", true, "text", "", "2", "", "");
$formValidation->addField("ALTSTATE", true, "text", "", "", "", "");
$formValidation->addField("ALTZIP", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// begin Recordset
$query_rsState = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsState = $sigma_modx->SelectLimit($query_rsState) or die($sigma_modx->ErrorMsg());
$totalRows_rsState = $rsState->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_archons = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_archons);
// Register triggers
$ins_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_spp_archons->setTable("spp_archons");
$ins_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_archons = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("JOBTITLE", "STRING_TYPE", "POST", "JOBTITLE");
$upd_spp_archons->addColumn("ORGNAME", "STRING_TYPE", "POST", "ORGNAME");
$upd_spp_archons->addColumn("ALTADDRESS1", "STRING_TYPE", "POST", "ALTADDRESS1");
$upd_spp_archons->addColumn("ALTADDRESS2", "STRING_TYPE", "POST", "ALTADDRESS2");
$upd_spp_archons->addColumn("ALTCITY", "STRING_TYPE", "POST", "ALTCITY");
$upd_spp_archons->addColumn("ALTSTATE", "STRING_TYPE", "POST", "ALTSTATE");
$upd_spp_archons->addColumn("ALTZIP", "STRING_TYPE", "POST", "ALTZIP");
$upd_spp_archons->addColumn("WORKPHONE", "STRING_TYPE", "POST", "WORKPHONE");
$upd_spp_archons->addColumn("ALTEMAIL", "STRING_TYPE", "POST", "ALTEMAIL");
$upd_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "VALUE", "{SESSION.webShortname}");
$upd_spp_archons->addColumn("GRAMMUPDATE", "DATE_TYPE", "CURRVAL", "");
$upd_spp_archons->addColumn("GRAMM", "STRING_TYPE", "CURRVAL", "");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "GET", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$totalRows_rsspp_archons = $rsspp_archons->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<!-- InstanceEditableHeadTag -->
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['CUSTOMERID'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    Spp_archons </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_archons->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_archons > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <?php 
// Show IF Conditional show_JOBTITLE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="JOBTITLE_<?php echo $cnt1; ?>">BUSINESS TITLE:</label></td>
              <td><input type="text" name="JOBTITLE_<?php echo $cnt1; ?>" id="JOBTITLE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('JOBTITLE')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("JOBTITLE");?> <?php echo $tNGs->displayFieldError("spp_archons", "JOBTITLE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_JOBTITLE_on_update_only
?>
          <?php 
// Show IF Conditional show_ORGNAME_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ORGNAME_<?php echo $cnt1; ?>">BUSINESS NAME:</label></td>
              <td><input type="text" name="ORGNAME_<?php echo $cnt1; ?>" id="ORGNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ORGNAME')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ORGNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "ORGNAME", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ORGNAME_on_update_only
?>
          <?php 
// Show IF Conditional show_ALTADDRESS1_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ALTADDRESS1_<?php echo $cnt1; ?>">BUSINESS ADDRESS:</label></td>
              <td><input type="text" name="ALTADDRESS1_<?php echo $cnt1; ?>" id="ALTADDRESS1_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTADDRESS1')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ALTADDRESS1");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTADDRESS1", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ALTADDRESS1_on_update_only
?>
          <?php 
// Show IF Conditional show_ALTADDRESS2_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ALTADDRESS2_<?php echo $cnt1; ?>">BUSINESS ADDRESS 2:</label></td>
              <td><input type="text" name="ALTADDRESS2_<?php echo $cnt1; ?>" id="ALTADDRESS2_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTADDRESS2')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ALTADDRESS2");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTADDRESS2", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ALTADDRESS2_on_update_only
?>
          <?php 
// Show IF Conditional show_ALTCITY_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ALTCITY_<?php echo $cnt1; ?>">BUSINESS CITY:</label></td>
              <td><input type="text" name="ALTCITY_<?php echo $cnt1; ?>" id="ALTCITY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTCITY')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ALTCITY");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTCITY", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ALTCITY_on_update_only
?>
          <?php 
// Show IF Conditional show_ALTSTATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ALTSTATE_<?php echo $cnt1; ?>">BUSINESS STATE:</label></td>
              <td><select name="ALTSTATE_<?php echo $cnt1; ?>" id="ALTSTATE_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                  <?php
  while(!$rsState->EOF){
?>
                  <option value="<?php echo $rsState->Fields('st_cd')?>"<?php if (!(strcmp($rsState->Fields('st_cd'), $rsspp_archons->Fields('ALTSTATE')))) {echo "SELECTED";} ?>><?php echo $rsState->Fields('st_nm')?></option>
                  <?php
    $rsState->MoveNext();
  }
  $rsState->MoveFirst();
?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_archons", "ALTSTATE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ALTSTATE_on_update_only
?>
          <?php 
// Show IF Conditional show_ALTZIP_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ALTZIP_<?php echo $cnt1; ?>">BUSINESS ZIP:</label></td>
              <td><input type="text" name="ALTZIP_<?php echo $cnt1; ?>" id="ALTZIP_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTZIP')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ALTZIP");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTZIP", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ALTZIP_on_update_only
?>
          <?php 
// Show IF Conditional show_WORKPHONE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="WORKPHONE_<?php echo $cnt1; ?>">BUSINESS PHONE:</label></td>
              <td><input type="text" name="WORKPHONE_<?php echo $cnt1; ?>" id="WORKPHONE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('WORKPHONE')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("WORKPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "WORKPHONE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_WORKPHONE_on_update_only
?>
          <?php 
// Show IF Conditional show_ALTEMAIL_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="ALTEMAIL_<?php echo $cnt1; ?>">BUSINESS EMAIL:</label></td>
              <td><input type="text" name="ALTEMAIL_<?php echo $cnt1; ?>" id="ALTEMAIL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('ALTEMAIL')); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ALTEMAIL");?> <?php echo $tNGs->displayFieldError("spp_archons", "ALTEMAIL", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_ALTEMAIL_on_update_only
?>
          <?php 
// Show IF Conditional show_LASTUPDATED_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">LAST UPDATE:</td>
              <td><?php echo KT_formatDate($rsspp_archons->Fields('LASTUPDATED')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_LASTUPDATED_on_update_only
?>
          <?php 
// Show IF Conditional show_UPDATEDBY_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">UPDATED BY:</td>
              <td><?php echo KT_escapeAttribute($rsspp_archons->Fields('UPDATEDBY')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_UPDATEDBY_on_update_only
?>
          <?php 
// Show IF Conditional show_GRAMMUPDATE_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">LAST GRAMMATEUS UPDATE:</td>
              <td><?php echo KT_formatDate($rsspp_archons->Fields('GRAMMUPDATE')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_GRAMMUPDATE_on_update_only
?>
          <?php 
// Show IF Conditional show_GRAMM_on_update_only 
if (@$_GET['CUSTOMERID'] != "") {
?>
            <tr>
              <td class="KT_th">GRAMMATEUS:</td>
              <td><?php echo KT_escapeAttribute($rsspp_archons->Fields('GRAMM')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_GRAMM_on_update_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_archons_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_archons->Fields('kt_pk_spp_archons')); ?>" />
        <?php
    $rsspp_archons->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['CUSTOMERID'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsState->Close();
?>
