<?php
////////////////////////////////////////////////////////////
//SEO generator
////////////////////////////////////////////////////////////
//metas implemented in header.php

//item
if (isset($idItem)) {
	//title
	    if (isset($itemType)) $html_title=ucwords(getTypeName($itemType))." ";
		$html_title.=$itemTitle;
		if ($itemPlace!="") $html_title.=" ".$itemPlace;
		if ($itemPrice!=0) $html_title.="  ".getPrice($itemPrice);
		
		//to display the category in the title uncomment this:
		/*$html_title.=SEPARATOR;
		if ($categoryParent!=0) $html_title.=$selectedCategoryName. " ";
		$html_title.=$categoryName.SEPARATOR;*/
		
	//end title
	$html_description=$itemDescription;	
}
//Smart 404
elseif(strpos($_SERVER["SCRIPT_NAME"], "404.php")>0){
	$html_title.=u($_SERVER["REQUEST_URI"]).SEPARATOR.SITE_NAME;
	$html_description=$html_title;	
}
//for new item
elseif(strpos($_SERVER["SCRIPT_NAME"], "item-new.php")>0){
	 //title
		$html_title=T_NEW_ITEM.SEPARATOR;
		if (isset($categoryName)){//new item with category selected
			if ($categoryParent!=0) $html_title.=$selectedCategoryName. " ";
			$html_title.=$categoryName.SEPARATOR;	
		}
		//else $html_title.=getCategories().SEPARATOR;
		$html_title.=SITE_NAME;
	//end title
	$html_description=$html_title;	
}
//categories
elseif (isset($categoryName)) {
	//title
		//$html_title=T_CLASSIFIEDS." ";
		if (isset($type)) $html_title.=ucwords(getTypeName($type)).SEPARATOR;
		if ($categoryParent!=0) $html_title.=$selectedCategoryName.SEPARATOR;
		$html_title.=$categoryName.SEPARATOR.SITE_NAME;//.SEPARATOR;	
		
	//end title
	$html_description=$categoryDescription;	

}
elseif ((strlen(cG("title"))>=MIN_SEARCH_CHAR) || strlen(cG("s"))>=MIN_SEARCH_CHAR){//search
    if (cG("title")!="")$html_title.=cG("title").' ';
    if (cG("s")!="")$html_title.=cG("s").' ';
    if (cG("place")!="")$html_title.=cG("place").' ';
    //$html_title.=SITE_NAME;
	$html_description=$html_title;	
}
//home
elseif ($_SERVER["REQUEST_URI"]=="/"||(isset($type)&&!isset($categoryName))||isset($page)) {
	//title
		//$html_title=T_CLASSIFIEDS." ";
		if (isset($type)) $html_title.=ucwords(getTypeName($type)).SEPARATOR;
		//$html_title.=getCategories().SEPARATOR;
		$html_title.=SITE_NAME;
	//end title
	$html_description=$html_title;	
}
//search form
elseif(strpos($_SERVER["SCRIPT_NAME"], "search.php")>0){
	$html_title.=T_ADV_SEARCH.SEPARATOR.SITE_NAME;
	$html_description=$html_title;	
}
//contact form
elseif(strpos($_SERVER["SCRIPT_NAME"], "contact.php")>0){
	$html_title.=T_CONTACT.SEPARATOR.SITE_NAME;
	$html_description=$html_title;	
}
//sitemap
elseif(strpos($_SERVER["SCRIPT_NAME"], "site-map.php")>0){
	$html_title.=T_SITEMAP.SEPARATOR.SITE_NAME;
	$html_description=$html_title;	
}
//privacy
elseif(strpos($_SERVER["SCRIPT_NAME"], "privacy.php")>0){
	$html_title.=T_PRIVACY.SEPARATOR.SITE_NAME;
	$html_description=$html_title;	
}

// common in all
if (isset($page)&&$page>1) $html_title.=SEPARATOR.T_PAGE." ".$page;

//better SEO with phpSEO class http:/neo22s.com/phpseo
$seo = new phpSEO($html_title);
$html_keywords= $seo->getKeyWords(5);

$seo = new phpSEO($html_description);
$html_keywords.=", ". $seo->getKeyWords(15);
$html_description= $seo->getMetaDescription(160);
unset($seo);
?>
