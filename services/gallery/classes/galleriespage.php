<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Import page
 *
 * @package svManager
 */
class GalleriesPage extends Page
{
 /**
  * constructs GalleriesPage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function GalleriesPage()
  {
    parent::Page('svManager &ndash; galleries', 'galleries');
  }

  /**
  * Returns list of galleries as html table
  * @access public
  * @return string html table
  * @param object gallery set
  */
  function getGalleriesHtml(&$gallerySet)
  {
    $galleriesData = $gallerySet->getGalleriesData();
    $galleriesPrefs = $gallerySet->getGalleriesPrefs();
    $newGalleryPref = isset($galleriesPrefs['newGalleryPref']) ? $galleriesPrefs['newGalleryPref'] : '';
    $newGalleryOptions = '';
    $submitDisabled = '';
    if (file_exists('plugins'.DIRECTORY_SEPARATOR.'autoviewer'))
    {
      if (AV_CREATE_NEW)
      {
        $selectString = ($newGalleryPref == 'autoviewer') ? 'selected="selected"' : '';
        $newGalleryOptions .= '<option value="autoviewer" '.$selectString.'>AutoViewer&nbsp;</option>
          ';
      }
    }
    if (file_exists('plugins'.DIRECTORY_SEPARATOR.'postcardviewer'))
    {
      if (PV_CREATE_NEW)
      {
        $selectString = ($newGalleryPref == 'postcardviewer') ? 'selected="selected"' : '';
        $newGalleryOptions .= '<option value="postcardviewer" '.$selectString.'>PostcardViewer&nbsp;</option>
          ';
      }
    }
    if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer'))
    {
      if (SV_CREATE_NEW)
      {
        $selectString = ($newGalleryPref == 'simpleviewer') ? 'selected="selected"' : '';
        $newGalleryOptions .= '<option value="simpleviewer" '.$selectString.'>SimpleViewer&nbsp;v1</option>
          ';
      }
    }
    if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'))
    {
      if (SV2_CREATE_NEW)
      {
        $selectString = ($newGalleryPref == 'simpleviewer2') ? 'selected="selected"' : '';
        $newGalleryOptions .= '<option value="simpleviewer2" '.$selectString.'>SimpleViewer</option>
          ';
      }
    }
    if (file_exists('plugins'.DIRECTORY_SEPARATOR.'tiltviewer'))
    {
      if (TV_CREATE_NEW)
      {
        $selectString = ($newGalleryPref == 'tiltviewer') ? 'selected="selected"' : '';
        $newGalleryOptions .= '<option value="tiltviewer" '.$selectString.'>TiltViewer&nbsp;</option>';
      }
    }
    if ($newGalleryOptions == '')
    {
      $newGalleryOptions = '<option value="">No plugins available&nbsp;</option>';
      $submitDisabled = 'disabled="disabled"';
    }
    $sortRefImage = 'img/sortoff.gif';
    $sortTitleImage = 'img/sortoff.gif';
    $sortCTimeImage = 'img/sortoff.gif';
    $sortTypeImage = 'img/sortoff.gif';
    switch ($galleriesPrefs['sortColumn'])
    {
      case 'galleryRef' :
        $sortRefImage = ($galleriesPrefs['sortAscending']) ? 'img/sortdown.gif' : 'img/sortup.gif';
        break;
      case 'galleryTitle' :
        $sortTitleImage = ($galleriesPrefs['sortAscending']) ? 'img/sortdown.gif' : 'img/sortup.gif';
        break;
      case 'galleryCTime' :
        $sortCTimeImage = ($galleriesPrefs['sortAscending']) ? 'img/sortdown.gif' : 'img/sortup.gif';
        break;
      case 'galleryType' :
        $sortTypeImage = ($galleriesPrefs['sortAscending']) ? 'img/sortdown.gif' : 'img/sortup.gif';
    }
    $html = '<form id="newgallery" method="post" action="svmcustomize.php">
      <div id="submitinputs">
        <label for="newgallerytype">New gallery type: </label>
        <select id="newgallerytype" name="newGalleryType">
          '.$newGalleryOptions.'
        </select>
        <input type="submit" id="submit" name="createNewGallery" value="Create New Gallery" '.$submitDisabled.' />
      </div>
    </form>
    <br class="clearboth" />  
    <h2>Galleries</h2>
    
';
    if (count($galleriesData) == 0)
    {
      $html .= '<p class="message">You have no galleries</p>';
      return $html;
    }
    $html .= '
    <table cellspacing="0" id="galleryset">
      <thead>
        <tr>
          <th id="refheader" scope="col"><a href="'.$_SERVER['PHP_SELF'].'?newSort=galleryRef" title="Change list order">No.<img src="'.$sortRefImage.'" alt="" width="10" height="10" /></a></th><th id="titleheader" scope="col"><a href="'.$_SERVER['PHP_SELF'].'?newSort=galleryTitle" title="Change list order">Gallery title<img src="'.$sortTitleImage.'" alt="" width="10" height="10" /></a></th>
          <th id="ageheader"><a href="'.$_SERVER['PHP_SELF'].'?newSort=galleryCTime" title="Change list order">List by age<img src="'.$sortCTimeImage.'" alt="" width="10" height="10" /></a></th>
          <th id="typeheader"><a href="'.$_SERVER['PHP_SELF'].'?newSort=galleryType" title="Change list order">Type<img src="'.$sortTypeImage.'" alt="" width="10" height="10" /></a></th>
          <th scope="col" colspan="5" >Actions</th>
        </tr>
      </thead>
      <tbody>';
          $localGalleriesData = $galleriesData;
    foreach ($localGalleriesData as $key=>$gallery)
    {
      $galleryRef = $gallery['galleryRef'];
      $galleryUrl = str_replace('\\', '/', $gallery['path']);
      $title = $this->stripHtml($gallery['title']);
      $titleWrapped = wordwrap($title, 30, "<br />\n", true);
      $titleWrapped = str_replace('&', '&amp;', $titleWrapped);
      $titleEscaped = str_replace('&', '&amp;', $title);
      $titleEscaped = str_replace("'", "\'", $titleEscaped);
      $titleEscaped = str_replace('"', "'+String.fromCharCode(34)+'", $titleEscaped);
      switch ($gallery['galleryType'])
      {
        case 'simpleviewer' :
          $iconSrc = 'img/svicon18.gif';
          break;
        case 'simpleviewer2' :
          $iconSrc = 'img/sv2icon18.gif';
          break;
        case 'tiltviewer' :
          $iconSrc = 'img/tvicon18.gif';
          break;
        case 'autoviewer' :
          $iconSrc = 'img/avicon18.gif';
          break;
        case 'postcardviewer' :
          $iconSrc = 'img/pvicon18.gif';
          break;
        default :
          $iconSrc = 'img/defaulticon18.gif';
      }
      $galleryClass = strtolower($gallery['galleryType']);
      $clonePrompt = "'Clone ".$titleEscaped."?'";
      $remPrompt = "'Remove ".$titleEscaped." from list?'";
      $rebuildPrompt = "'Rebuild ".$titleEscaped."?'";
      $html .= '
      <tr>
        <td class="ref">'.$galleryRef.'</td>
        <td class="title" colspan="2"><a href="svmcustomize.php?galleryindex='.$key.'" title="change gallery title &amp; appearance">'.$titleWrapped.'</a></td>
        <td class="'.$galleryClass.'"><a href="svmcustomize.php?galleryindex='.$key.'"><img src="'.$iconSrc.'" width="18" height="18" title="gallery type" alt="gallery type" /></a></td>
        <td class="actionedit"><a href="svmcustomize.php?galleryindex='.$key.'" title="change gallery title &amp; appearance">Edit</a></td>
        <td class="actionview"><a rel="external" href="'.$galleryUrl.'" title="view gallery in new window">View</a></td>
        <td class="actionclone"><a href="svmcustomize.php?clone='.$key.'&amp;gallerytype='.$gallery['galleryType'].'" onclick="return allowSubmit('.$clonePrompt.')" title="clone this gallery">Clone</a></td>
        <td class="actionrebuild"><a href="'.$_SERVER['PHP_SELF'].'?rebuild='.$key.'" onclick="return allowSubmit('.$rebuildPrompt.')" title="rebuild gallery data files">Rebuild</a></td>
        <td class="actionunlist"><a href="'.$_SERVER['PHP_SELF'].'?remove='.$key.'" onclick="return allowSubmit('.$remPrompt.')" title="remove gallery from list">Unlist</a></td>
      </tr>';
    }
    $html .= '
    </tbody>
    </table>'; 
    return $html;    
  }
}
?>