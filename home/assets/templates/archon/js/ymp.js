function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function htmlspecialchars_decode (string, quoteStyle) {
    var optTemp = 0;
    var i = 0;
    var noquotes = false;
    if (typeof quoteStyle === 'undefined') {
        quoteStyle = 2;
    }
    string = string.toString()
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>');
    var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
    };
    if (quoteStyle === 0) {
        noquotes = true;
    }
    if (typeof quoteStyle !== 'number') {
        quoteStyle = [].concat(quoteStyle);
        for (i = 0; i < quoteStyle.length; i++) {

            if (OPTS[quoteStyle[i]] === 0) {
                noquotes = true;
            } else if (OPTS[quoteStyle[i]]) {
                optTemp = optTemp | OPTS[quoteStyle[i]];
            }
        }
        quoteStyle = optTemp;
    }
    if (quoteStyle && OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/&#0*39;/g, "'");
    }
    if (!noquotes) {
        string = string.replace(/&quot;/g, '"');
    }
    string = string.replace(/&amp;/g, '&');
    return string
}

function toDate(dateStr) {
    var parts = dateStr.split("-");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function scrollTo($top) {
    if($top == 'top' || !$top){
        $top = 0;
    }
    $('html, body').animate({
        scrollTop: $top
    }, 1000);
}

var $body,
    delete_this_pp,
    IamAdding = false,
    post_url = '/includes/process-form/archon_process_form.php',
    $cropper_img = jQuery(""),
    $total_rq_time_show_txt = 0,
    $profile_img,
    show_success_msg,
    hide_success_msg,
    reset_profile_img,
    CropAvatar_init,
    update_lastupdate_profile,
    $success_loader,
    showText,
    showTextMsg,
    process_img = false,
    $profile_type = false,
    show_output_pp = true,
    accordion_var,
    $current_profile_tr;

(function($) {
    $body = $('body');


    var $pp_wrap = $("#popup_ymp"),
        $profile_contact = $('#contact_wrap'),
        $profile_career = $('#profile_career_box'),
        $profile_educ = $('#profile_educ_box'),
        $profile_children = $('#children_block'),
        $profile_bio = $('#bio_text').show(),
        $archousa_block = $("#archousa_block");
    $success_loader = $body.find("#success_loader");

    $profile_img = $("#profile_img");
    $profile_type = $('[name="PROFILETYPE"]').val();

    if('ARCHOUSA' == $profile_type){
        post_url = '/includes/process-form/archousa_process_form.php';
    }

    $body.find('input[value="0000-00-00"]').val('');

    showText = function  (target, message, index, interval) {
        if($total_rq_time_show_txt == 0){
            $total_rq_time_show_txt = message.length * interval + 500;
        }
        if (index < message.length) {
            target.append(message[index++]);
            showTextMsg = setTimeout(function () { showText(target, message, index, interval); }, interval);
        }
    }

    show_success_msg = function ($msg) {
        if($msg == undefined){
            $msg = 'Success!';
        }
        $body.addClass('show-success');
        $body.removeClass('loader-open');
        $success_loader.text('');
        $total_rq_time_show_txt = 0;
        clearInterval(showTextMsg);
        showText ($success_loader, $msg, 0, 100);
    };

    hide_success_msg = function () {
        $body.removeClass('show-success loader-open');

        // accorion_init();
        var $open = $body.find('.open').last();
        if($open.closest('.open').length){
            $open = $open.closest('.open');
        }
        $open.find('[data-control]').trigger('click');
    };
    reset_profile_img = function () {
        var $src = $profile_img.attr('src');
        $body.find(".avatar-preview, .avatar-btns, .preview_title").hide();
        // $body.find(".avatar-btns").hide();
        $body.find(".avatar-preview img").attr('src', $src);
        $body.find(".avatar-wrapper img").attr('src', $src);
    };
    /**
     * popup form start
     */

    function open_popup(id) {
        $body.addClass('popup-open');
        CropAvatar_init();
        // $("#" + id).addClass('active').siblings().removeClass('active');
    }

    function popup_close() {
        $body.removeClass('popup-open  view-popup');
        // $pp_wrap.find('.active').removeClass('active');
        $cropper_img.cropper('destroy');
        reset_profile_img();
        // $pp_wrap.find('form')[0].reset();
    }

    function change_class_on_condition_input($item, $condition) {
        if($condition){
            $item.addClass('input-validated').removeClass('input-invalid');
        } else {
            $item.addClass('input-invalid').removeClass('input-validated');
        }
    }


    function check_if_validate_pp_inputs($elems) {
        var validation = true;
        $elems.each(function () {
            var $this = $(this);
            if(!$this.hasClass('input-validated')){
                $this.addClass('input-invalid');
                validation = false;
            }
        });
        return validation;
    }

    function process_popup_form($form, id) {

        $body.find('input[value="00/00/0000"]').val('');
        $body.find('input[value="00-00-0000"]').val('');
        $body.find('input[value="01/01/1960"]').val('');
        $body.find(".message").html("");

        var $id = $("#"+id),
            nid = id.replace(/\d+$/, ""),
            $tinymce = $id.find(".tiny-editable");;

        IamAdding = $id.hasClass('add_career_inner') || $id.hasClass('add_children_inner');

        switch (nid) {
            case "archon_edit_img":
                // process_archon_image_upload($form, $id);
                process_img = true;
                $body.find(".avatar-save").trigger('click');
                break;
            case "archon_edit_contact":
                process_archon_form_general($form, $id, $profile_contact);
                break;
            case "archon_edit_bio":
                // $tinymce = $id.find("#BIO");
                $tinymce.val(tinymce.get($tinymce.attr('id')).getContent());
                process_archon_form_general($form, $id, $profile_bio);
                break;
            case "archon_edit_carrer_":
                $tinymce.val(tinymce.get($tinymce.attr('id')).getContent());
                process_archon_form_general($form, $id, $profile_career.find('#prof_id_'+$id.find('input[name="PROFID"]').val()));
                break;
            case "archon_edit_educ_":
                process_archon_form_general($form, $id, $profile_educ.find('#educ_id_'+$id.find('input[name="DEGREEID"]').val()));
                break;
            case "archon_edit_children_":
                process_archon_form_general($form, $id, $profile_children.find('#child_id_'+$id.find('input[name="CHILDID"]').val()));
                break;
            case "archon_request_status_change":
                $tinymce.val(tinymce.get($tinymce.attr('id')).getContent());
                process_archon_form_general($form, $id, $("#archon_request_status_change"));
                break;
            case "archon_remove_officer":
                show_output_pp = false;
                process_archon_form_general($form, $id, $());
                break;
            case "archon_add_officer":
                show_output_pp = false;
                process_archon_form_general($form, $id, $());
                break;
            case "archon_edit_officer":
                show_output_pp = false;
                process_archon_form_general($form, $id, $());
                break;
            default :
                return false;
        }
    }

    /**
     * Process Image upload element
     * @param $form
     * @param $elem
     */
    function process_archon_image_upload($form, $elem) {
        //configuration
        var max_file_size 		= 2048576; //allowed file size. (1 MB = 1048576)
        var allowed_file_types 		= ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg']; //allowed file types

        var proceed = true,
            error = [],
            total_files_size = 0,
            $file = $elem.find('[type="file"]'),
            file = $file[0].files[0],
            $message_box = $form.find('.message'),
            img_url;

        if(!window.File && window.FileReader && window.FileList && window.Blob){ //if browser doesn't supports File API
            error.push("Your browser does not support new File API! Please upgrade."); //push error text
        }else{
            if($file.val() != ""){
                if(allowed_file_types.indexOf(file.type) === -1){ //check unsupported file
                    error.push( "<b>"+ file.name + "</b> is unsupported file type!"); //push error text
                    proceed = false; //set proceed flag to false
                }

                total_files_size = total_files_size + file.size;
            }

            if(total_files_size > max_file_size){
                error.push( "Total size "+total_files_size+", Allowed size is " + max_file_size +", Try smaller file!"); //push error text
                proceed = false; //set proceed flag to false
            }

            //if everything looks good, proceed with jQuery Ajax
            if(proceed){
                $body.addClass('loader-open');
                var form_data = new FormData();

                form_data.append("user_id", $form.find('[name="CUSTOMERID"]').val());
                form_data.append("__files[]", file);
                form_data.append("UPDATEDBY", $form.find('[name="UPDATEDBY"]').val());
                if('ARCHOUSA' == $profile_type){
                    form_data.append("ARCHOUSAID", $form.find('[name="ARCHOUSAID"]').val());
                    form_data.append("SPOUSEID", $form.find('[name="SPOUSEID"]').val());
                }
                form_data.append("action", 'archon_image_upload');
                //jQuery Ajax to Post form data
                $.ajax({
                    url : post_url,
                    type: "POST",
                    data : form_data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    mimeType:"multipart/form-data"
                }).done(function(response){
                    var result = jQuery.parseJSON( response );
                    if(result.type == 'success'){
                        if(show_output_pp) {
                            img_url = result.img_url + "?t=" + Date.now();
                            $profile_img.attr('src', img_url);
                            $('.img_box').find('img').attr('src', img_url);
                            update_lastupdate_profile(new Date());
                        }
                        show_success_msg();
                    } else {
                        show_success_msg('Error!');
                        error.push( result.err_msg);
                    }
                    setTimeout(function () { hide_success_msg(); }, $total_rq_time_show_txt);

                    $body.removeClass('loader-open');
                    // $form[0].reset(); //reset form
                    // popup_close();
                });
            }
        }

        if(error.length){
            $message_box.html(""); //reset output
            $(error).each(function(i){ //output any error to output element
                $(result_output).append('<div class="error">'+error[i]+"</div>");
            });
        }

    }

    /**
     * Process the output from archon form
     * @param result
     * @param $output
     */
    function process_output_archon_general(result, $output, $elem) {
        var $output_clone = $output.clone(),
            $parent_elem = '';

        if($output.length) {
            $output.html(result.html.replace(/\n|\\/gi, ""));
            if(result.TITLE){
                $elem.find('h3').first().html(result.TITLE);
            }
            $parent_elem = $output.closest('div');

            if(result.archousa_fname) {
                $archousa_block.show().find("#ar_fname").text(result.archousa_fname);
            } else if(result.archousa_fname == 0){
                $archousa_block.hide();
            }

        } else { /*New Entry*/
            var $new_elem;
            if(result.PROFID && result.action == 'INSERT'){
                $new_elem = $("<p id='prof_id_"+result.PROFID+"'></p>");
                $new_elem.html(result.html.replace(/\n|\\/gi, ""));
                $parent_elem = $profile_career;
                $profile_career.append($new_elem);
                $elem.removeClass('add_career_inner');
                $elem.find('h3').first().html(result.TITLE);
                $elem.find('input[name="PROFID"]').val(result.PROFID);
            } else if(result.DEGREEID && result.action == 'INSERT'){
                $new_elem = $("<p id='educ_id_"+result.DEGREEID+"'></p>");
                $new_elem.html(result.html.replace(/\n|\\/gi, ""));
                $parent_elem = $profile_educ;
                $profile_educ.append($new_elem);
                $elem.removeClass('add_educ_inner');
                $elem.find('h3').first().html(result.TITLE);
                $elem.find('input[name="DEGREEID"]').val(result.DEGREEID);
            }
            else if(result.CHILDID && result.action == 'INSERT'){
                $new_elem = $("<li id='child_id_"+result.CHILDID+"'></li>");
                $new_elem.html(result.html.replace(/\n|\\/gi, ""));
                $parent_elem = $profile_children;
                $profile_children.find('ul').append($new_elem);
                $elem.removeClass('add_children_inner');
                $elem.find('h3').first().html(result.TITLE);
                $elem.find('input[name="CHILDID"]').val(result.CHILDID);
            }
        }
        $parent_elem.find('.no-result').remove();


        if($output.attr('id') == 'contact_wrap'){
            $("#profile_title").text(result.profile_title.replace(/\n|\\|'/gi, ""));
            // $output.html(result.html.replace(/\n|\\/gi, ""));
            $output.prepend($output_clone.find('h3').first());
        }


        /*if($output.attr('id') == 'archon_bio'){
         $output.html(result.html.replace(/\n|\\/gi, ""));
         }*/

        /*Updates the last update time*/
        update_lastupdate_profile(new Date());
        show_hide_spouse_children_name_frontend();
    }

    /**
     * Process General Archon Form
     * @param $form
     * @param $elem
     * @param $output
     */
    function process_archon_form_general($form, $elem, $output) {
        var proceed = true,
            error = [],
            $message_box = $form.find('.message'),
            nurl;
        $message_box.html("");

        proceed = check_if_validate_pp_inputs($elem.find('.input-required'));


        //if everything looks good, proceed with jQuery Ajax
        if(proceed){
            $body.addClass('loader-open');
            var form_data = new FormData(),
                $profile_contact_clone = $profile_contact.clone(),
                extra_val = {};

            $elem.find('select').each(function (e) {
                var $this = $(this);
                extra_val[$this.val()] = $.trim($this.find('option[value="'+$this.val()+'"]').text());
            });


            form_data.append("user_id", $form.find('[name="CUSTOMERID"]').val());
            form_data.append("data", $elem.find('input, select, textarea').serialize());
            form_data.append("UPDATEDBY", $form.find('[name="UPDATEDBY"]').val());
            form_data.append("SPOUSEID", $form.find('[name="SPOUSEID"]').val());
            form_data.append("ARCHOUSAID", $form.find('[name="ARCHOUSAID"]').val());
            form_data.append("extra_val", JSON.stringify(extra_val));
            form_data.append("action", $elem.attr('id'));

            if($elem.find('[name="KT_Insert1"]').length){
                nurl = '/services/grammateus/member/status_request_ajax.php';
            } else if($elem.hasClass('remove-officer')){
                nurl = '/includes/process-form/process_officer_ajax.php';
            } else if($elem.hasClass('edit-officer')){
                nurl = '/includes/process-form/edit_officer_ajax.php';
            } else if($elem.hasClass('arc-add-officer')){
                nurl = '/includes/process-form/add_officer_ajax.php';
            } else {
                nurl = post_url;
            }
            //jQuery Ajax to Post form data
            $.ajax({
                url : nurl,
                type: "POST",
                data : form_data,
                contentType: false,
                cache: false,
                processData:false,
            }).done(function(response){
                var result = jQuery.parseJSON( response );
                if(result.type == 'success'){
                    if(show_output_pp){
                        process_output_archon_general(result, $output, $elem);
                    }
                    show_success_msg();
                } else {
                    show_success_msg('Error!');
                    error.push( result.html);
                }
                setTimeout(function () {
                    hide_success_msg();
                    if($elem.hasClass('remove-officer') || $elem.hasClass('arc-add-officer') || $elem.hasClass('edit-officer')){
                        location.reload();
                    }
                    }, $total_rq_time_show_txt);

                $body.removeClass('loader-open');
                // popup_close();
            });
        } else {
            error.push("<p>Please check required inputs.</p>");
        }

        if(error.length){
            $message_box.html(""); //reset output
            $(error).each(function(i){ //output any error to output element
                $message_box.append('<div class="error">'+error[i]+"</div>");
            });
        }
    }

    $body.on('click', '.open-popup', function (e) {
        e.preventDefault();
        open_popup();
        accorion_init();
        $("#" + $(this).attr('data-id')).find('[data-control]').trigger('click');
        setTimeout(function () {
            $(window).trigger('resize');
            bio_tinymce_init();
        }, 500);
    }).on('click', '.popup_close, .pp_cancel', function (e) {
        var $this = $(this);
        e.preventDefault();
        popup_close();
        $this.closest('.add_educ_inner').remove();
        $this.closest('.add_career_inner').remove();
        $this.closest('.add_children_inner').remove();
    });

    /*$body.on('click',function (e) {
     // console.log($(this).closest('#popup_ymp').length);
     // popup_close();
     });*/

    $body.on('submit', '#pupup_form', function (e) {
        e.preventDefault();
    }).on('blur change', '.input-required', function (e) {
        var $this = $(this);
        change_class_on_condition_input($this, ($this.val().length > 0));
        if($this.attr('type') == 'email'){
            change_class_on_condition_input($this, (validateEmail($this.val())));
        }
    }).on('click', '.pp_submit', function (e) {
        e.preventDefault();
        process_popup_form($pp_wrap.find('form'), $(this).closest('.parent-elem').attr('id'));
    }).on('click', '.add_new_career', function (e) {
        e.preventDefault();
        new_element_open(jQuery(".add_career_inner").first());
    }).on('click', '.add_new_child', function (e) {
        e.preventDefault();
        new_element_open(jQuery(".add_children_inner").first());
    }).on('click', '.add_new_educ', function (e) {
        e.preventDefault();
        new_element_open(jQuery(".add_educ_inner").first());
    });

    function new_element_open($new_elem) {
        $new_elem.show()
            .siblings('.open').trigger('click');
        $new_elem.find('[data-control]').trigger('click');
        // console.log($new_elem.find('[data-control]'));
        jQuery(window).trigger('resize');
    }

    $(window).load(function () {
        $body.find('input[value="0000-00-00"]').val('');
        if(typeof popup_open != 'undefined' && popup_open == true){
            $(".open-popup").trigger('click');
        }
    });

    /**
     * popup form end
     */

    $(document).on('click', '.birth_date_btn', function (e) {
        e.preventDefault();
    });

    var currentTime = new Date(),
        year = currentTime.getFullYear()+5,
        date_picker = null,
        date_picker_cont = $("");

    function date_picker_init() {
        $("input[data-date]").each(function (e) {
            var id = $(this).attr('id'),
                ndate = Date.parse($(this).val());

            ndate = moment(ndate).format('MM/DD/YYYY');
            $(this).val(ndate);

            date_picker = null;
            date_picker = new Pikaday(
                {
                    field: document.getElementById(id),
                    trigger: document.getElementById(id + '_btn'),
                    minDate: new Date(1900, 0, 1),
                    format: 'MM/DD/YYYY',
                    maxDate: year,
                    editable: true,
                    yearRange: [1900,year],
                    theme: 'dark-theme',
                    position: 'bottom right'
                });

            date_picker.setMoment(new Date($(this).val()));
        });

        date_picker = {};
        $("input[data-monthdate]").each(function (e) {
            var id = $(this).attr('id'),
                ndate = Date.parse($(this).val());

            ndate = moment(ndate).format('MM/YYYY');
            $(this).val(ndate);

            date_picker[id] = new Pikaday(
                {
                    field: document.getElementById(id),
                    trigger: document.getElementById(id + '_btn'),
                    minDate: new Date(1900, 0, 1),
                    format: 'MM/YYYY',
                    maxDate: year,
                    editable: true,
                    yearRange: [1900,year],
                    theme: 'dark-theme',
                    position: 'bottom right',
                    onOpen: function () {
                        date_picker_cont = $body.find(".pika-single:not(.is-hidden)");
                        date_picker_cont.addClass('month-only');
                        date_picker_cont.attr('data-id', id);
                    },

                    onClose: function () {
                        /*var $oll_opened_month = $(".month-only");
                         console.log($oll_opened_month.not(date_picker_cont));
                         console.log(date_picker_cont);
                         if($oll_opened_month.length > 1){
                         $oll_opened_month.not(date_picker_cont).removeClass('month-only');
                         } else {
                         date_picker_cont.removeClass('month-only');
                         }*/
                    },
                    onChange: function () {
                        // console.log($(this));
                    }
                });

            date_picker[id].setMoment(new Date($(this).val()));
        });
    }
    // date_picker_init();

    $(document).on('change', '.pika-select', function (e) {
        var $this = $(this),
            $parent = $('.month-only:not(.is-hidden)'),
            id = $parent.attr('data-id'),
            mnth, year, ndate;
        if($parent.has($this[0])){
            mnth = 1*$parent.find('.pika-select-month').val() + 1;
            year = $parent.find('.pika-select-year').val();

            ndate = Date.parse(year+'-'+mnth+'-1');
            ndate = moment(ndate).format('MM/YYYY');
            $("#"+id).val(ndate);
        }

    });
    /*$(document).on('click', '.pika-title', function (e) {
     var $this = $(this),
     $parent = $('.month-only:not(.is-hidden)'),
     id = $parent.attr('data-id'),
     mnth, year, ndate;
     console.log($parent);
     if($parent.has($this[0])){
     mnth = 1*$parent.find('.pika-select-month').val() + 1;
     year = $parent.find('.pika-select-year').val();

     ndate = Date.parse(year+'-'+mnth+'-1');
     ndate = moment(ndate).format('MM/YYYY');
     $("#"+id).val(ndate);
     }

     });*/

    function accorion_init() {
        var $accordion = $('.accordion');
        if($accordion.length){
            accordion_var = $accordion.accordion({
                "transitionSpeed": 400
            });
        }
    }
    // accorion_init();

    function bio_tinymce_init() {

    }

    function all_tinymce_init() {
        tinymce.remove('textarea');
        if(typeof tinymce == 'object'){
            tinymce.init({
                selector: 'textarea.tiny-editable',
                theme: 'modern',
                height: 300,
                plugins: [
                    'advlist autolink link lists charmap preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking',
                    'save table contextmenu directionality template paste textcolor'
                ],
                toolbar: 'undo redo | styleselect | bold italic | link | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fullpage',
                menubar: 'file edit view'
            });
        }
    }

    $profile_bio.html(htmlspecialchars_decode($profile_bio.text(), 'ENT_HTML_QUOTE_DOUBLE'));

    /*Delete Career*/
    delete_this_pp = function ($elem) {
        var $parent_elem = $elem.closest('.parent-elem'),
            delete_id;
        if($parent_elem.closest('#archon_edit_career').length){
            delete_id = $parent_elem.find('input[name="PROFID"]').val();
            profile_archon_delete_career(
                $parent_elem,
                $parent_elem.closest('form'),
                delete_id,
                'delete_career',
                $("#prof_id_"+delete_id)
            );
        } else if($parent_elem.closest('#archon_edit_education').length){
            delete_id = $parent_elem.find('input[name="DEGREEID"]').val();
            profile_archon_delete_career(
                $parent_elem,
                $parent_elem.closest('form'),
                delete_id,
                'delete_educ',
                $("#educ_id_"+delete_id)
            );
        }
        else if($parent_elem.closest('#archon_edit_children').length){
            delete_id = $parent_elem.find('input[name="CHILDID"]').val();
            profile_archon_delete_career(
                $parent_elem,
                $parent_elem.closest('form'),
                delete_id,
                'delete_children',
                $("#child_id_"+delete_id)
            );
        }
    };

    function profile_archon_delete_career($parent, $form, $delete_id, action, $target) {
        $body.addClass('loader-open');
        var form_data = new FormData();

        form_data.append("user_id", $form.find('[name="CUSTOMERID"]').val());
        form_data.append("ARCHOUSAID", $form.find('[name="ARCHOUSAID"]').val());
        form_data.append("delete_id", $delete_id);
        form_data.append("action", action);

        //jQuery Ajax to Post form data
        $.ajax({
            url : post_url,
            type: "POST",
            data : form_data,
            contentType: false,
            cache: false,
            processData:false,
        }).done(function(response){
            var result = jQuery.parseJSON( response );
            if(result.type == 'success'){
                if(!$target.siblings().length){

                }
                $target.remove();
                $parent.remove();
                show_success_msg();
            } else {
                show_success_msg('Error!');
            }
            setTimeout(function () { hide_success_msg(); }, $total_rq_time_show_txt);

            $body.removeClass('loader-open');
        });

    }

    $body.on('change', '.radio_wrap input[type="radio"]', function (e) {
        var $this = $(this);
        if($this.is(':checked')){
            $this.closest('.checkbox_radio_wrap').find('input[type="hidden"]').val($this.val()).addClass('input-validated');
            $this.siblings().removeAttr('checked');
        }
    });

    $body.find("input[checked]").trigger('click');


    /*Date new format*/
    update_lastupdate_profile = function ($date) {
        var $date_parent = $body.find('.last-updated'),
            $date_span = $date_parent.find('span'),
            ndate = Date.parse($date_span.text());
        if($date != undefined){
            ndate = $date;
        }
        if(isNaN(ndate)){
            return false;
        }
        ndate = moment(ndate).format('MM/DD/YYYY');
        $date_span.text(ndate);
        $date_parent.show();
    };
    update_lastupdate_profile();

    function check_and_hide_elem($cond, $elem) {
        if($cond){
            $elem.show();
        } else {
            $elem.hide();
        }
    }

    function show_hide_spouse_name() {
        var $spouse_div = $body.find("#archon_spouse");
        check_and_hide_elem(
            ($body.find('[name="MARITALSTT"]:checked').val() == 1),
            $spouse_div);
    }
    show_hide_spouse_name();
    $body.on('change', '[name="MARITALSTT"]', function (e) {
        show_hide_spouse_name();
    });

    function chosen_init() {
        if($(".chosen-select").length){
            $(".chosen-select").chosen({
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });
        }
    }

    function show_hide_spouse_children_name_frontend() {
        var $spouse_block = $body.find('.archousa_block').show();
        $profile_children.show();
        check_and_hide_elem(
            ($spouse_block.attr("data-maritalstt") == '1' && $spouse_block.find('p').text() != ''),
            $spouse_block);
        check_and_hide_elem(
            ($profile_children.find('li').length != 0),
            $profile_children);
    }
    show_hide_spouse_children_name_frontend();

    function load_n_set_popup() {
        $body = $('body');
        $pp_wrap = $("#popup_ymp"),
        $profile_contact = $('#contact_wrap'),
        $profile_career = $('#profile_career_box'),
        $profile_educ = $('#profile_educ_box'),
        $profile_children = $('#children_block'),
        $profile_bio = $('#bio_text').show(),
        $archousa_block = $("#archousa_block");
        $success_loader = $body.find("#success_loader");

        $profile_img = $("#profile_img");
        $profile_type = $('[name="PROFILETYPE"]').val();

        if('ARCHOUSA' == $profile_type){
            post_url = '/includes/process-form/archousa_process_form.php';
        }

        $body.find('input[value="0000-00-00"]').val('');

        date_picker_init();

        accorion_init();
        chosen_init();
        if($(".tiny-editable").length){
            all_tinymce_init();
        }
    }
    load_n_set_popup();



    /*Image part*/
    function profile_image_manage() {
        (function (factory) {
            if (typeof define === 'function' && define.amd) {
                // AMD. Register as anonymous module.
                define(['jquery'], factory);
            } else if (typeof exports === 'object') {
                // Node / CommonJS
                factory(require('jquery'));
            } else {
                // Browser globals.
                factory(jQuery);
            }
        })(function ($) {

            'use strict';

            var console = window.console || { log: function () {} };

            function CropAvatar($element) {
                this.$container = $element;

                this.$avatar = $('#profile_img');
                this.$avatarModal = this.$container.find('#avatar-modal');
                this.$loading = this.$container.find('.loading');

                this.$avatarForm = this.$container.find('.avatar-form');
                this.$avatarUpload = this.$avatarForm.find('.avatar-upload');
                this.$avatarSrc = this.$avatarForm.find('.avatar-src');
                this.$avatarData = this.$avatarForm.find('.avatar-data');
                this.$avatarInput = this.$avatarForm.find('.avatar-input');
                this.$avatarSave = this.$avatarForm.find('.avatar-save');
                this.$avatarBtns = this.$avatarForm.find('.avatar-btns');

                this.$avatarWrapper = this.$container.find('.avatar-wrapper');
                this.$avatarPreview = this.$container.find('.avatar-preview').hide();

                this.submitted = false;
                this.init();
            }

            CropAvatar.prototype = {
                constructor: CropAvatar,

                support: {
                    fileList: !!$('<input type="file">').prop('files'),
                    blobURLs: !!window.URL && URL.createObjectURL,
                    formData: !!window.FormData
                },

                init: function () {
                    this.support.datauri = this.support.fileList && this.support.blobURLs;

                    if (!this.support.formData) {
                        this.initIframe();
                    }

                    this.url = this.$avatar.attr('src');

                    this.addListener();
                    // this.startCropper();
                },

                addListener: function () {
                    this.$avatarInput.on('change', $.proxy(this.change, this));
                    this.$avatarBtns.on('click', $.proxy(this.rotate, this));
                    this.$avatarSave.on('click', $.proxy(this.submit, this));
                },

                initPreview: function () {
                    var url = this.$avatar.attr('src');

                    this.$avatarPreview.html('<img src="' + url + '">');
                },

                change: function () {
                    var files;
                    var file;

                    if (this.support.datauri) {
                        files = this.$avatarInput.prop('files');

                        if (files.length > 0) {
                            file = files[0];

                            if (this.isImageFile(file)) {
                                /*if (this.url) {
                                 URL.revokeObjectURL(this.url); // Revoke the old one
                                 }*/

                                this.url = URL.createObjectURL(file);
                                this.startCropper();
                            }
                        }
                    } else {
                        file = this.$avatarInput.val();

                        if (this.isImageFile(file)) {
                            this.syncUpload();
                        }
                    }
                    this.submitted = false;
                    this.$avatarPreview.show();
                    this.$avatarBtns.show();
                    $body.find('.preview_title').show();
                },

                submit: function () {
                    if (!this.$avatarSrc.val() && !this.$avatarInput.val()) {
                        return false;
                    }

                    if (this.support.formData) {

                        //configuration
                        var max_file_size 		= 2048576; //allowed file size. (1 MB = 1048576)
                        var allowed_file_types 		= ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg']; //allowed file types

                        var proceed = true,
                            error = [],
                            total_files_size = 0,
                            $file = this.$container.find('[type="file"]'),
                            file = $file[0].files[0],
                            $message_box = $body.find('.message'),
                            img_url;

                        if(!window.File && window.FileReader && window.FileList && window.Blob){ //if browser doesn't supports File API
                            error.push("Your browser does not support new File API! Please upgrade."); //push error text
                        }else {
                            if (total_files_size > max_file_size) {
                                error.push("Total size " + total_files_size + ", Allowed size is " + max_file_size + ", Try smaller file!"); //push error text
                                proceed = false; //set proceed flag to false
                            }

                            //if everything looks good, proceed with jQuery Ajax
                            if (proceed) {
                                $body.addClass('loader-open');
                                this.ajaxUpload();
                                return false;
                            }
                        }

                        if(error.length){
                            $message_box.html(""); //reset output
                            $(error).each(function(i){ //output any error to output element
                                $(result_output).append('<div class="error">'+error[i]+"</div>");
                            });
                        }
                    }
                },

                rotate: function (e) {
                    var data;

                    if (this.active) {
                        data = $(e.target).data();

                        if (data.method) {
                            this.$img.cropper(data.method, data.option);
                        }
                    }
                },

                isImageFile: function (file) {
                    if (file.type) {
                        return /^image\/\w+$/.test(file.type);
                    } else {
                        return /\.(jpg|jpeg|png|gif)$/.test(file);
                    }
                },

                startCropper: function () {
                    var _this = this;

                    if (this.active) {
                        this.$img.cropper('replace', this.url);
                    } else {
                        this.$img = $('<img src="' + this.url + '">');
                        this.$avatarWrapper.empty().html(this.$img);
                        this.$img.cropper({
                            aspectRatio: 4/5,
                            preview: this.$avatarPreview.selector,
                            zoomable: false,
                            crop: function (e) {
                                var json = [
                                    '{"x":' + e.x,
                                    '"y":' + e.y,
                                    '"height":' + e.height,
                                    '"width":' + e.width,
                                    '"rotate":' + e.rotate + '}'
                                ].join();

                                _this.$avatarData.val(json);
                            }
                        });

                        this.active = true;
                    }

                    this.$avatarModal.one('hidden.bs.modal', function () {
                        _this.$avatarPreview.empty();
                        _this.stopCropper();
                    });

                    $cropper_img = this.$img;

                    $(window).trigger('resize');
                },

                stopCropper: function () {
                    if (this.active) {
                        // this.$img.cropper('disable');
                        this.$img.cropper('destroy');
                        // this.$img.remove();
                        this.active = false;
                    }
                },

                ajaxUpload: function () {

                    if(!process_img){
                        return false;
                    }
                    process_img = false;

                    var $form = $("<form></form>");
                    $form.html(this.$avatarForm.clone());
                    var form_data = new FormData($form[0]);
                    var _this = this;
                    form_data.append("user_id", $body.find('#popup_ymp [name="CUSTOMERID"]').first().val());
                    form_data.append("UPDATEDBY", $body.find('#popup_ymp [name="UPDATEDBY"]').first().val());
                    if($body.find('#popup_ymp [name="image_name"]').length){
                        form_data.append("image_name", $body.find('#popup_ymp [name="image_name"]').first().val());
                    }
                    form_data.append("action", 'archon_image_upload');

                    if('ARCHOUSA' == $profile_type){
                        form_data.append("ARCHOUSAID", $body.find('[name="ARCHOUSAID"]').last().val());
                        form_data.append("SPOUSEID", $body.find('[name="SPOUSEID"]').last().val());
                        form_data.append("ARCHOUSA_NAME", $body.find('[name="ARCHOUSA_NAME"]').last().val());
                    }

                    $.ajax(post_url, {
                        type: 'post',
                        data: form_data,
                        dataType: 'json',
                        processData: false,
                        contentType: false,

                        beforeSend: function () {
                            _this.submitStart();
                        },

                        success: function (data) {
                            _this.submitDone(data);
                        },

                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            _this.submitFail(textStatus || errorThrown);
                        },

                        complete: function () {
                            _this.submitEnd();
                        }
                    });
                },

                syncUpload: function () {
                    // this.$avatarSave.click();
                    // this.submit();
                },

                submitStart: function () {
                },

                submitDone: function (data) {

                    if ($.isPlainObject(data) && data.type == 'success') {
                        if (data.img_url) {
                            this.url = data.img_url + "?t=" + Date.now();

                            if (this.support.datauri || this.uploaded) {
                                this.uploaded = false;
                                this.cropDone();
                            } else {
                                this.uploaded = true;
                                this.$avatarSrc.val(this.url);
                                this.startCropper();
                            }

                            this.$avatarInput.val('');
                        } else if (data.err_msg) {
                            show_success_msg('Error!');
                            this.alert(data.err_msg);
                        }
                    } else {
                        this.alert('Failed to response');
                    }
                    // this.cropDone();
                    setTimeout(function () { hide_success_msg(); }, $total_rq_time_show_txt);

                    $body.removeClass('loader-open');
                },

                submitFail: function (msg) {
                    this.alert(msg);
                },

                submitEnd: function () {
                    this.submitted = true;
                },

                cropDone: function () {
                    // this.$avatarForm.get(0).reset();
                    $cropper_img.cropper('destroy');
                    $body.find(".avatar-preview, .avatar-btns, .preview_title").hide();
                    $body.find(".avatar-wrapper img").attr('src', this.url);

                    if(show_output_pp){
                        $profile_img.attr('src', this.url);
                        // $('.img_box').find('img').attr('src', img_url);
                        update_lastupdate_profile(new Date());
                    }
                    show_success_msg();
                    this.stopCropper();
                    // this.$avatarModal.modal('hide');
                },

                alert: function (msg) {
                    // console.log(msg);
                    $(".message").append('<div class="error">'+msg+"</div>");
                }
            };

            CropAvatar_init = function () {
                var $crop_avatar = $body.find('#crop-avatar');
                if($crop_avatar.length){
                    return new CropAvatar($crop_avatar);
                }
            };

            $(function () {
                return CropAvatar_init();
            });

        });
    }
    profile_image_manage();


    /**
     * Data table parts
     */
    $(document).ready(function() {
        var $archon_list_table = $('#archon_list_table'),
            $archon_list_search_table = $('#archon_list_search_table'),
            $officer_list_search_table = $('#officers_list_table');
        if($archon_list_table.length){
            $archon_list_table.DataTable( {
                // "ordering": false,
                "columnDefs": [
                    { "orderable": false, "targets": 3 }
                ],
                "dom": '<"top"fl<"clear">ip<"clear">>rt<"bottom"ip<"clear">>',
                "language": {
                    "search": "Search Your Results:"
                }
                // "order": [[ 0, 'asc' ]]
            } );
        }
        if($archon_list_search_table.length) {
            $archon_list_search_table.DataTable({
                // "ordering": false,
                "columnDefs": [
                    {"orderable": false, "targets": 2}
                ],
                "dom": '<"top"fl<"clear">ip<"clear">>rt<"bottom"ip<"clear">>',
                "language": {
                    "search": "Search Your Results:"
                }
                // "order": [[ 0, 'asc' ]]
            });
        }
        if($officer_list_search_table.length) {
            $officer_list_search_table.DataTable({
                // "ordering": false,
                "columnDefs": [
                    {"orderable": false, "targets": 3}
                ],
                "dom": '<"top"fl<"clear">ip<"clear">>rt<"bottom"ip<"clear">>',
                "language": {
                    "search": "Search Your Results:"
                }
                // "order": [[ 0, 'asc' ]]
            });
        }
    }).on('click', '.view_popup_close', function (e) {
        e.preventDefault();
        $body.removeClass('popup-open view-popup');
    });

    function view_profile_ajax($this) {
        $body.addClass('loader-open');
        $body.find('.pp-loader').empty();
        $current_profile_tr = $this.closest('tr');
        $("#popup_view_profile").load($this.attr('href') + ' #main', function () {
            $body.addClass('popup-open view-popup').removeClass('loader-open');
            scrollTo();
            $('#bio_text').html(htmlspecialchars_decode(
                $('#bio_text').text(), 'ENT_HTML_QUOTE_DOUBLE')).show();
            hide_elem_onlast_tr($current_profile_tr);
        });
    }

    function ajax_officers($this, $url) {
        $body.addClass('loader-open');
        $body.find('.pp-loader').empty();
        $current_profile_tr = $this.closest('tr');
        $("#popup_view_profile").load($url, function () {
            $body.addClass('popup-open view-popup').removeClass('loader-open');
            scrollTo();
            accorion_init();
            chosen_init();
            date_picker_init();
            hide_elem_onlast_tr($current_profile_tr);
        });
    }

    function remove_officer_ajax($this) {
        ajax_officers($this, $this.attr('href'));
    }

    function add_new_officer($this) {
        ajax_officers($this, '/includes/popup/add_new_officer.php');
    }

    $(document).on('click', '#view_archousa_json, #view_archon_json', function (e) {
        e.preventDefault();
        view_profile_ajax($(this));
    });


    $body.on('click', '.amu_table .view', function (e) {
        e.preventDefault();
        var $this = $(this);
        view_profile_ajax($this);
    }).on('click', '.amu_table .edit', function (e) {
        e.preventDefault();
        show_output_pp = false;
        var $this = $(this),
            $user_id = $this.closest('tr').attr('data-customerid'),
            $visitor_id = $this.closest('.amu_table').attr('data-visitor'),
            $dm_email = $this.closest('.amu_table').attr('data-dmemail');
        $body.addClass('loader-open');
        $(document).find('.pp-loader, .pika-single').remove();
        var $pp_edit = $("<div class='pp-loader'></div>");
        $body.append($pp_edit);
        $body.find('.pp-loader').empty();
        /*$.post("demo_test.asp", function(data, status){
            alert("Data: " + data + "\nStatus: " + status);
        });*/

        $current_profile_tr = $this.closest('tr');
        $pp_edit.load('/includes/popup-ajax.php?user_id='+$user_id+'&visitor='+$visitor_id+'&DM_EMAIL='+$dm_email, function () {
            $body.addClass('popup-open').removeClass('loader-open view-popup');
            scrollTo();
            load_n_set_popup();
            profile_image_manage();
            hide_elem_onlast_tr($current_profile_tr);
        });
    }).on('click', '.amu_table .remove_officer, .amu_table .update_start_date', function (e) {
        e.preventDefault();
        var $this = $(this);
        remove_officer_ajax($this);
    }).on('click', '.add_new_officer', function (e) {
        e.preventDefault();
        var $this = $(this);
        add_new_officer($this);
    });

    function hide_elem_onlast_tr($elem) {
        // console.log($elem.next('tr').length);
        if(!$elem.next('tr').length){
            $(document).find('.view_next_archon, .edit_next_archon, .remove_next_officer, .edit_next_officer').hide();
        }
    }

    $(document).on('click', '.edit_this_archon, .view_this_archon', function (e) {
        var vclass = '.edit';
        if($(this).hasClass('view_this_archon')){
            vclass = '.view';
        }
        e.preventDefault();
        if($current_profile_tr.length){
            $current_profile_tr.find('.action_td '+vclass).trigger('click');
        }
    }).on('click', '.view_next_archon, .edit_next_archon, .remove_next_officer, .edit_next_officer', function (e) {
        var vclass = '.edit';
        if($(this).hasClass('view_next_archon')){
            vclass = '.view';
        }else if($(this).hasClass('remove_next_officer')){
            vclass = '.remove_officer';
        }else if($(this).hasClass('edit_next_officer')){
            vclass = '.update_start_date';
        }
        e.preventDefault();
        if($current_profile_tr.length && $current_profile_tr.next().length){
            $current_profile_tr.next('tr').find('.action_td '+vclass).trigger('click');
        }
    });

    /**
     * Basic Search UPD
     */
    $body.on('change', '.basic_search_wrapper form select', function (e) {
        $(this).closest('form').submit();
    });

    $(".as_filter").on('click', 'h3', function (e) {
        $(this).parent().toggleClass('filter_open').find('.as_filter_main').slideToggle();
    });


    /**
     * This part is for login redirect
     */
    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i=0; i<vars.length; i++) {
            var pair = vars[i].split('=');
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return false;
    }
    // var refurl = getQueryVariable('refurl'),
    var refurl,
        $login_form = $("#loginForm"),
        login_url = $login_form.attr('action');
    if($login_form.length){
        login_url = login_url.split('?')[0];
        refurl = $("#refurl").text();
        if(refurl){
            $login_form.attr('action', login_url + '?refurl=' + refurl);
        }
    }
    /**
     * This part is for login redirect
     */

    if($body.find('.officer_list_table_wrapper').length){
        $body.addClass('officer_listing_page');
    }

}(jQuery));

