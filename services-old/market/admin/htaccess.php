<?php 
require_once('../includes/admin/access.php');
require_once('../includes/config.php');
require_once('../includes/common.php');

include("../languages/".$_GET["lang"].".php");//adding language for the type

            $rewritebase=str_replace('http://'.$_SERVER["SERVER_NAME"],"",SITE_URL);
            if ($rewritebase=="") $rewritebase="/";

            $offer=u(TYPE_OFFER_NAME);
            if ($offer=="") $offer="offer";

            $need=u(TYPE_NEED_NAME);
            if ($need=="") $need="need";

            $cat=u(T_CATEGORY);
            if ($cat=="") $cat="category";

            $typ=u(T_TYPE);
            if ($typ=="") $typ="type";

            $new=u(T_NEW_ITEM);
            if ($new=="") $new="new";

            $con=u(T_CONTACT);
            if ($con=="") $con="contact";

            $pol=u(T_PRIVACY);
            if ($pol=="") $pol="policy";

            $sm=u(T_SITEMAP);
            if ($sm=="") $sm="sitemap";

            $sch=u(T_ADV_SEARCH);
            if ($sch=="") $sch="search";

            $gm=u(T_MAP);
            if ($gm=="") $gm="map";

$htaccess_content = "ErrorDocument 404 ".$rewritebase."content/404.php
Options All -Indexes
<IfModule mod_rewrite.c>
RewriteEngine on
RewriteBase $rewritebase
RewriteRule ^([0-9]+)$ index.php?page=$1 [L]
RewriteRule ^admin/$ admin/index.php [L]
RewriteRule ^rss/$ content/feed-rss.php [L]
RewriteRule ^manage/$ content/item-manage.php [L]
RewriteRule ^$new.htm content/item-new.php [L]
RewriteRule ^$con.htm content/contact.php [L]
RewriteRule ^$pol.htm content/privacy.php [L]
RewriteRule ^$sm.htm content/site-map.php [L]
RewriteRule ^$sch.htm content/search.php [L]
RewriteRule ^$gm.htm content/map.php [L]
RewriteRule ^$offer/(.+)$ index.php?category=$1&type=0  [L]
RewriteRule ^$need/(.+)$ index.php?category=$1&type=1 [L]
RewriteRule ^(.+)/(.+)/$ index.php?category=$2 [L]
RewriteRule ^$cat/(.+) $1/ [R=301,L]
RewriteRule ^(.+)/$ index.php?category=$1 [L]
RewriteRule ^(.+)/(.+)/([0-9]+)$ index.php?category=$2&page=$3 [L]
RewriteRule ^(.+)/([0-9]+)$ index.php?category=$1&page=$2 [L]
RewriteRule ^(.+)/(.+)/(.+)/(.+)$ /$3/$4-$1.htm [R=301,L]
RewriteRule ^(.+)/(.+)/(.+)-([0-9]+).htm$  item.php?category=$2&item=$4 [L]
RewriteRule ^(.+)/(.+)-([0-9]+).htm$  item.php?category=$1&item=$3 [L]
</IfModule>";
echo $htaccess_content;
	
	//saving htaccess
		        if(is_writable('../')){
			        $file = fopen('../.htaccess' , "w+");
			        if (fwrite($file, $htaccess_content)=== FALSE) {
				        $msg="Cannot write to the configuration file '.htaccess'";
				        $succeed=false;
			        }else $succeed=true;
			        fclose($file);
		        }
		        else {
			        $msg="The configuration file '/.htaccess' is not writable. Change its permissions, then re-run the installation.";
			        $succeed=false;
		        }
		        
if ($succeed) jsRedirect(SITE_URL."/admin/settings.php");
else echo $msg;

?>
