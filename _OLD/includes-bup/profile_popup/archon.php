<?php
$user_id = $_SESSION['webInternalKey'];
//$user_id = $_SESSION['CUSTOMERCD'];

$user_data = SPP_ARCHON_DB::get_user_profile_data($user_id)['data'];

$user_img = (empty($user_data['IMAGE'])) ? '/home/assets/images/icons/archon-default.png' :
'/home/assets/images/archons/' .$user_data['IMAGE'];

$submit_dv = <<<DDD
<div class="sb_div">
    <button type="button" class="pp_submit button">Update</button> &nbsp;
    <button type="button" class="pp_submit button new_elem_submit">Submit</button> &nbsp;
    <button type="button" class="pp_delete button hide" onclick="if(confirm('Delete this?')) {delete_this_pp(jQuery(this)); } return false;">Delete</button> &nbsp;
    <button type="reset" class="pp_cancel button">Done</button> &nbsp;

</div>
DDD;

/* Career Information Block */
$user_prof_data = SPP_ARCHON_DB::get_all_user_prof($user_id)['data'];

$all_prof_name = SPP_ARCHON_DB::get_all_profession()['data'];
$all_prof_name = array_combine_(array_column($all_prof_name, 'OCCUPATIONID'), $all_prof_name);

$all_prof_title = SPP_ARCHON_DB::get_all_prof_title()['data'];
$all_prof_title = array_combine_(array_column($all_prof_title, 'SKILLID'), $all_prof_title);
/* Career Information Block */

/* Education Information Block */
$user_edu_data = SPP_ARCHON_DB::get_all_user_edu($user_id)['data'];
/* Education Information Block */

/* Spouse Information Block */
$all_spouse = SPP_ARCHON_DB::get_all_user_spouse($user_id);
$all_spouse_data = $all_spouse['data'];
/* Spouse Information Block */

/* Children Information Block */
$all_children = SPP_ARCHON_DB::get_all_user_children($user_id);
$all_children_data = $all_children['data'];
/* Children Information Block */

?>

<?php ob_start();?>
<div id="popup_ymp" class="popup-ymp">
    <form action="" method="post" id="pupup_form">
        <div class="message">

        </div>
        <div id="archon_popup_accordion" data-accordion-group>

            <?php include_once $pp_dir . "inc/archon/edit_contact.php";?><!-- edit contact  -->

            <?php include_once $pp_dir . "inc/archon/edit_img.php";?> <!--edit image-->

            <?php include_once $pp_dir . "inc/archon/edit_education.php";?><!-- end edit education  -->

            <?php include_once $pp_dir . "inc/archon/edit_career.php";?><!-- end edit career archon  -->

            <?php include_once $pp_dir . "inc/archon/edit_children.php";?><!-- edit Children  -->

            <?php include_once $pp_dir . "inc/archon/edit_bio.php";?><!-- end edit bio  -->

        </div>

        <input type="hidden" name="CUSTOMERID" value="<?php echo $user_id;?>">
        <input type="hidden" name="UPDATEDBY" value="<?php echo $user_data['FIRSTNAME'] . ' ' . $user_data['MIDDLEINITIAL'] . ' ' . $user_data['LASTNAME'];?>">
        <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo $user_data['LASTUPDATED'];?>">
        <input type="hidden" name="RETURN" value="<?php echo $_SESSION['webValidated'];?>">
        <input type="hidden" name="SPOUSEID" value="<?php echo $all_spouse_data[0]['SPOUSEID'];?>">
        <input type="hidden" name="ARCHOUSAID" value="<?php echo $all_spouse_data[0]['ARCHOUSAID'];?>">
        <input type="hidden" name="PROFILETYPE" value="ARCHON">
    </form>
    <a href="#" class="popup_close">X</a>
</div>

<?php ob_get_flush();?>