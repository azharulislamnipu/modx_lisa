<div class="profile_title_header clearfix">
    <h1 id="profile_title">
        [+FIRSTNAME+] [+MIDDLEINITIAL+] [+LASTNAME+] [+SUFFIX+]
        <!--[+ phx:if=`[+DESIGNATIONLST+]`:is=``:then=``:else=`<br/>[+DESIGNATIONLST+]`+]-->
    </h1>
    <div class="edit_btn_wrap">
        <button class="button open-popup">Edit Profile</button>
        <span class="last-updated">Last Updated: <span>[+LASTUPDATED+]</span></span>
    </div>
</div>

<div class="span-4"><div class="margin-top-none">
        <div class="width padding">


            [+phx:if=`[+IMAGE+]`:is=``:then=`
            <img id="profile_img" src="/home/assets/images/icons/archon-default.png" alt="Default Archon Image Not Found picture."/><br/><br/>
            <span class="font-small"><a href="#" class="button open-popup" data-id="archon_edit_img">Add Image</a></span>
            `:else=`
            <img id="profile_img" src="/home/assets/images/archons/[+IMAGE+]" alt="Image of [+FIRSTNAME+] [+MIDDLEINITIAL+] [+LASTNAME+] [+SUFFIX+]"/><br/><br/>
            `+]



            <div class="contact-box" id="contact_wrap">

                [[LDataDisplay? &table=`vw_elig` &display=`1` &tpl=`tpl_archon_upd_info` &filter=`CUSTOMERID,[[getPlaceholder? &name=`customercd`]],1|STATUSSTT,Delinquent,2|STATUSSTT,Inactive,2`]]

                <p><strong>Mailing Address</strong><br />
                    [+ADDRESS1+] [+ADDRESS2+]<br />
                    [+CITY+], [+STATECD+] [+ZIP+] </p>

                <p><strong>Email</strong><br />
                    [+EMAIL+]</p>

                <p><strong>Home Phone</strong><br />
                    [+HOMEPHONE+]</p>

                <p><strong>Mobile</strong><br />
                    [+MOBILEPHONE+]</p>
                </p>

                <p><strong>Birthdate</strong><br />
                    [+phx:input=`[+BIRTHDATE+]`:mydate=`%B %e`+]
                </p>

                <div class="archousa_block" data-maritalstt="[+MARITALSTT+]">
                    <strong> Archousa Name</strong>
                    <p>[+SPOUSENAME+]</p>
                    <p class="btn_p"><a class="button small_button" id="view_archon" href="/home/archousa-profile.php" >View</a>
                    </p>
                </div>

            </div>