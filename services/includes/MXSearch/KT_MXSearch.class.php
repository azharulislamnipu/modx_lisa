<?php
class KT_MXSearch{
	var $connection; //connection name
	var $databaseType; //database type
	var $searchName; //name of the class
	var $tmpTable; //name of the temporary table storing last cache update
	var $refreshCacheDelay;
	var $cacheTable; //name of the cache table storing all site search content
	var $searchType; //type of search (fulltext || normal)
	var $searchColumns; //array with the names of the searched columns
	var $whereCondition; //where condition in the Search Recordset SQL
	var $orderBy; //order by statement in the Search Recordset SQL
	var $searchFor=''; //aditional columns in the Search Recordset SQL
	var $tables;
	var $totalRows;
	var $importanceArray = array(1=>5,2=>10,3=>25,4=>50,5=>100);
	var $sql = array(
		'create_cache' => "CREATE TABLE %s (
						title_cah varchar (100) NOT NULL, 
						shortdesc_cah text NOT NULL, 
						col1_cah text NULL, 
						col2_cah text NULL, 
						col3_cah text NULL, 
						col4_cah text NULL, 
						col5_cah text NULL, 
						importance_cah INT NOT NULL, 
						url_cah varchar (255) NOT NULL);",
		'select' => "SELECT `%s` as title_cah, `%s` as shortdesc_cah, `%s` as url_cah, %s as importance_cah",						
		'MySQL' => array(
			'like' => "LIKE",	
			'fulltext_where' => array(
						'fulltext' => "MATCH (col%s_cah) AGAINST ('%s') >0 ",
						'boolean fulltext' => "MATCH (col%s_cah) AGAINST ('%s' IN BOOLEAN MODE) >0 "
						),
			'fulltext_order' => array(
						'fulltext' => "MATCH (col%s_cah) AGAINST ('%s') * %s",
						'boolean fulltext' => "MATCH (col%s_cah) AGAINST ('%s' IN BOOLEAN MODE) * %s"
						)
		)		
	);
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   KT_MXSearch
	//
	// DESCRIPTION:
	//   KT_MXSearch constructor
	//   
	// ARGUMENTS:
	//   searchName - Name of the object
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function KT_MXSearch() {
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   setConnection
	//
	// DESCRIPTION:
	//   Sets the connection and the connection type (database type)
	//
	// ARGUMENTS:
	//   connection - connection name
	//   databaseType - database type
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function setConnection(&$connection , $databaseType) {
		$this->connection = $connection;
		$this->databaseType = $databaseType;
	}
	

	
	function setCache($cacheTable, $refreshCacheDelay) {
		$this->cacheTable = $cacheTable;
		$this->refreshCacheDelay = $refreshCacheDelay*60;
	}
	
	function setTempTable($tmpTable) {
		$this->tmpTable = $tmpTable;
	}
	
	function setTables($config) {
		$this->tables = $config->Tables;
	}
	function setSearchType($searchType) {
		$this->searchType = $searchType;
	}
	
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   checkTableExists()
	//
	// DESCRIPTION:
	//   Verifies the existence of a table in the database
	//
	// ARGUMENTS:
	//   tableName - name of the table
	//
	// RETURNS:
	//   true or false
	//--------------------------------------------------------------------
	
	function checkTableExists($tableName){
		//check if table exists
		$KT_sql = "SELECT * FROM $tableName";
		$testRecord = mysql_query($KT_sql, $this->connection);
		if (mysql_error()) {
			return false;
		} else {
			return true;
		}
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   createTmpTable
	//
	// DESCRIPTION:
	//   Creates the table that holds the cache refresh information
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function createTmpTable(){
		//create settings table
		$KT_sql = "CREATE TABLE ".$this->tmpTable."(
								lastupd_tmp TEXT NOT NULL								
								);";
		mysql_query($KT_sql, $this->connection) or die(mysql_error());								
		//insert record
		$this->initTmpTable();
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   updateTmpTable
	//
	// DESCRIPTION:
	//   Updates the table that holds the cache refresh information
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   true or false
	//--------------------------------------------------------------------
	
	function updateTmpTable(){
		$now = date("Y/m/d H:i:s");
		$KT_sql = "update ".$this->tmpTable." SET lastupd_tmp = '$now'";
		mysql_query($KT_sql, $this->connection) or die(mysql_error());
		return true;
	}
	
	
	function initTmpTable() {
		$KT_sql = "INSERT INTO ".$this->tmpTable." (lastupd_tmp) VALUES('1970/01/01 00:00:00')	;";
		mysql_query($KT_sql, $this->connection) or die(mysql_error()); 								
	}
	//--------------------------------------------------------------------
	// FUNCTION:
	//   checkCacheExpired
	//
	// DESCRIPTION:
	//   Checks if the cache has expired
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   true or false
	//--------------------------------------------------------------------
	
	function checkCacheExpired() {
		$KT_sql = "select lastupd_tmp from ".$this->tmpTable;
		$mxs_result = mysql_query($KT_sql, $this->connection) or die(mysql_error());
		$row_mxs_result = mysql_fetch_assoc($mxs_result);
		if ($row_mxs_result['lastupd_tmp']) {
			$date = $row_mxs_result['lastupd_tmp'];
			if (strtotime(date("Y/m/d H:i:s")) - strtotime($date) > $this->refreshCacheDelay) {
				return true;
			} else {
				return false;
			}
		} else {
			$this->initTmpTable();
			return false;
		}
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   createCacheTable
	//
	// DESCRIPTION:
	//   Creates the cache table
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function createFulltextIndex($dbType) {
		if ($dbType=="MySQL") {
			for ($i=1;$i<=5;$i++) {
				$KT_sql = 'ALTER TABLE `'.$this->cacheTable.'` DROP INDEX `idx_description`'.$i;
				mysql_query($KT_sql, $this->connection);
				$KT_sql = 'create fulltext index idx_description'.$i.' on '.$this->cacheTable.' (col'.$i.'_cah);';
				mysql_query($KT_sql, $this->connection) or die(mysql_error()."<br>Please check that your database supports fulltext search");
			}
		}
	} 
	
	function createCacheTable() {
		$KT_sql = "drop table ".$this->cacheTable;
		mysql_query($KT_sql, $this->connection);
		$KT_sql = sprintf($this->sql['create_cache'], $this->cacheTable);
		mysql_query($KT_sql, $this->connection) or die(mysql_error());
		
		if ($this->searchType=='fulltext') {
			$this->createFulltextIndex($this->databaseType);
		}		
		 
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   fillCacheTable
	//
	// DESCRIPTION:
	//   Fills the cache table
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function fillCacheTable() {
		$this->updateTmpTable();		
		$this->createCacheTable();
		
		if (is_array($this->tables)){
		foreach($this->tables as $tableKey => $crtTable){
			$columnsString = array();
			
			$KT_TableName = $tableKey;

			//for each column...
	
			if (is_array($crtTable['searchColumns'])){
				$tmpColumnArr = array();
				foreach($crtTable['searchColumns'] as $fieldKey => $fieldValue) {
					$tmpColumnArr[$fieldValue][]	=	"`".$fieldKey."`";
				}
				for ($relIdx=1;$relIdx<=5;$relIdx++) {
					if (isset($tmpColumnArr[$relIdx])) {
						$columnsString[$relIdx] = implode(",",$tmpColumnArr[$relIdx]);
					} else {
						unset ($columnsString[$relIdx]);
					}
					
				}
				
			}else {
				die('No search columns defined in table '.$KT_TableName.'!');
			}
			//end columns
			
			//set the columns values	
			$cacheTile			= $crtTable['resultTitle'];
			$cacheDesc			= $crtTable['resultDesc'];			
			$cacheURL			= $crtTable['pageName'];
			$cacheImportance 	= $crtTable['TableImportance'];
			if (isset($crtTable['AditionalCondition'])) {
				$cacheAditionalCond = $crtTable['AditionalCondition'];			
			} else {
				unset($cacheAditionalCond);
			}
			
			//compute result url parameters
			$paramValue = $crtTable['pageParam'];
				
			$KT_sql = sprintf($this->sql['select'], $cacheTile, $cacheDesc, $paramValue, $cacheImportance);
			for ($relIdx=1;$relIdx<=5;$relIdx++) {
				if (isset($columnsString[$relIdx])) {
					$KT_sql .= ", ".$columnsString[$relIdx];	
				}
			}
			$KT_sql	.= " FROM ".$KT_TableName;
			if (isset($cacheAditionalCond) && $cacheAditionalCond != '') {
				$KT_sql .= " WHERE $cacheAditionalCond ";
			}
            
			$KT_results = mysql_query($KT_sql, $this->connection) or die(mysql_error()."<br>".$KT_sql);
            $totalRows_KT_results = mysql_num_rows($KT_results);
            if (!$totalRows_KT_results) return;
            
			$row_KT_results = mysql_fetch_assoc($KT_results);
			
			do {
				$cacheCol = array();
				$col_cah = array();
				$title_cah			= addslashes(strip_tags($row_KT_results['title_cah']));
				$shortdesc_cah		= addslashes(strip_tags($row_KT_results['shortdesc_cah']));
				for ($relIdx=1;$relIdx<=5;$relIdx++) {
					if (isset($tmpColumnArr[$relIdx])) {
						$cacheCol[$relIdx] = "";
						for ($colIdx=0;$colIdx<count($tmpColumnArr[$relIdx]);$colIdx++) {
							$cacheCol[$relIdx] .= "\r\n".$row_KT_results[str_replace("`", "", $tmpColumnArr[$relIdx][$colIdx])];
						}
					}
				}							
				$KT_colString = ''; $KT_valueString = '';
				for ($relIdx=1;$relIdx<=5;$relIdx++) {
					if (isset($cacheCol[$relIdx])) {
						$col_cah[$relIdx] = addslashes(strip_tags($cacheCol[$relIdx]));
						$KT_colString .= ", col".$relIdx."_cah";
						$KT_valueString .= ", '".$col_cah[$relIdx]."'";
						if ($this->databaseType=="PostgreSQL" && $this->searchType=="fulltext") {
							$KT_colString .= ", col".$relIdx."_vect_cah";
							$KT_valueString .= ", to_tsvector('".$col_cah[$relIdx]."')";				
						}
					} else {
						$col_cah[$relIdx] = "";
					}
				}
				$url_cah	= addslashes($cacheURL.urlencode($row_KT_results['url_cah']));
				$importance_cah = $this->importanceArray[$cacheImportance];
				$KT_sql	= "INSERT INTO ".$this->cacheTable." (title_cah, shortdesc_cah".$KT_colString.", importance_cah, url_cah) values ('$title_cah', '$shortdesc_cah'".$KT_valueString.", $importance_cah, '$url_cah');";
				mysql_query($KT_sql) or die(mysql_error());
			} while ($row_KT_results = mysql_fetch_assoc($KT_results));
				
		}//foreach table
		}else {die('No search tables defined!');}
	}
	
	function getRecordset($start = null, $max = null) {
		$kt_searchWhere = $this->getWhereCondition();
		$kt_searchOrder = $this->getOrderBy();
		$kt_columns	= $this->getSearchColumns();
		$KT_sql = sprintf("SELECT * %s FROM %s WHERE %s ORDER BY %s", $kt_columns, $this->cacheTable, $kt_searchWhere, $kt_searchOrder);
		$KT_result = mysql_query($KT_sql, $this->connection) or die(mysql_error());
		$this->totalRows = mysql_num_rows($KT_result);
		if (isset($start) && isset($max)) {
			$KT_result = mysql_query($KT_sql." LIMIT ".$start.", ".$max, $this->connection) or die(mysql_error());
		}
		return $KT_result;
	}
	
	function getTotalRows() {
		return $this->totalRows;
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   refreshCache
	//
	// DESCRIPTION:
	//   Checks if the cache is expired and refreshes the cache if necesary
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function refreshCache(){
		if (!$this->checkTableExists($this->tmpTable)){
				$this->createTmpTable($this->tmpTable);
			}
		if ($this->checkCacheExpired()){
				$this->fillCacheTable();
			} 
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   computeAll
	//
	// DESCRIPTION:
	//   Checks the type of the search and performs the right search
	//
	// ARGUMENTS:
	//   searchFor - expression to search for
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function computeAll($searchFor){
		$this->refreshCache();
		$this->setSearchTerm($searchFor);

		if ($this->searchType=='normal') {
			$this->computeNormalSearch();		
		}
		if ($this->searchType=='fulltext') {	
			$this->computeFullSearch($this->databaseType);
		}
		if ($this->searchType=='boolean fulltext') {
			$this->computeMySQLFullSearch($this->searchType);
		}
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   setSearchTerm
	//
	// DESCRIPTION:
	//   Sets the expression to search for
	//
	// ARGUMENTS:
	//   searchFor - the expression
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function setSearchTerm($searchFor){
		if (get_magic_quotes_gpc()) {
			$keywords = @$_GET[$searchFor];
		} else {
			$keywords = addslashes(@$_GET[$searchFor]);
		}
		$this->searchFor = stripslashes(strtolower($keywords));
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   computeNormalSearch
	//
	// DESCRIPTION:
	//   Computes the SQL for the normal search
	//
	// ARGUMENTS:
	//   
	// RETURNS:
	//   
	//--------------------------------------------------------------------
	
	function computeNormalSearch() {
		$searchFor = $this->searchFor;
		$like = $this->sql[$this->databaseType]['like'];
		if (!empty($searchFor))	{
			$searchCond = explode(" and ", $searchFor);
			for ($condIdx=0;$condIdx<count($searchCond);$condIdx++)	{
				if ($searchCond[$condIdx]!='') {
					if ($condIdx!=0) {
						$this->whereCondition .= " AND (";
					} else {
						$this->whereCondition .= " (";
					}	
					$searchCond[$condIdx] = str_replace(" or "," ", $searchCond[$condIdx]);
					$expr = "/\"([^\"]*)\"/m";
					$matches = array();
					preg_match_all($expr, $searchCond[$condIdx], $matches);
					$searchCond[$condIdx] = preg_replace($expr, "", $searchCond[$condIdx]);
					$searchWords = explode(" ", $searchCond[$condIdx]);
					if (is_array($matches[1])) {			
						foreach ($matches[1] as $key=>$value) {				
							array_push($searchWords, $value);
						}
					}
					$first = true;
					for ($i=0;$i<count($searchWords);$i++) {
						if ($searchWords[$i]!='') {
							if (!$first) {
								$this->whereCondition .= " OR (";
							} else {
								$this->whereCondition .= " (";
							}
							$first = false;
							$this->whereCondition .= " col1_cah $like '%".addslashes($searchWords[$i])."%' ";
							for ($relIdx=2; $relIdx <= 5; $relIdx++) {
								$this->whereCondition .= " OR col".$relIdx."_cah $like '%".addslashes($searchWords[$i])."%' ";
								if ($relIdx==5) $this->whereCondition .= ") ";
							}
						}
					}
					$this->whereCondition .= " ) ";
				}
			}
			$this->orderBy		.= " importance_cah DESC";
		} else {
			$this->whereCondition .= " 1=-1 ";
			$this->orderBy		.= " importance_cah DESC";		
		}
	}
		
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   getScore
	//
	// DESCRIPTION:
	//   Calculates the importance score 
	//
	// ARGUMENTS:
	//   text - text to search in
	//   text - text to search in
	//	 searchFor - keywords
	// RETURNS:
	//   the importance score for the keywords
	//--------------------------------------------------------------------
		
	function getScore($text) 
	{
		$searchFor = $this->searchFor;
		$textLo = strtolower($text);
		$expr = "/\"([^\"]*)\"/m";
		$matches = array();
		preg_match_all($expr, $searchFor, $matches);
		$searchFor = preg_replace($expr, "", $searchFor);
		$searchFor = str_replace('"','',$searchFor);
		$nrWords = count(explode(" ", $text));
		$searchWords = explode(" ", $searchFor);
		if (is_array($matches[1])) {
			foreach($matches[1] as $key=>$value) {
				array_push($searchWords, $value);
			}
		}
		$nrSearchWords = count($searchWords);
		$mainWordIdx = 0;
		$mainWord = "";
		
		if ($nrSearchWords==1) {
			$nrOccur = substr_count($textLo, strtolower($searchFor));
			$score = $nrOccur/$nrWords;
			return $score;
		} else {
			$score = 0;
			$allWords = true;
			for ($i=0;$i<$nrSearchWords;$i++) {
				if ($searchWords[$i]!='') {
					$nrOccur = substr_count($textLo, strtolower($searchWords[$i]));		
					if (!$nrOccur) {
						$allWords = false;
					}
					$score += $nrOccur/$nrWords;		
				}
			}
			if ($allWords) {
				$score *= 5;
			}
			return $score;
		}
	}
	
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   getOrderedArray
	//
	// DESCRIPTION:
	//   Makes a sort operation on the results for the normal search using the importance score
	//
	// ARGUMENTS:
	//   recSet - recordset object containing the results
	//	 searchFor - keywords
	// RETURNS:
	//   array with the results in the right order
	//--------------------------------------------------------------------
	
	function getOrderedArray($recSet, $start, $length)
	{
		$searchFor = $this->searchFor;
		$ordArr = array();
		$i = 0;
		$row_recSet = mysql_fetch_assoc($recSet);
		do {
			$fieldsArr = array();
			$fieldsArr['title_cah'] = $row_recSet['title_cah'];
			$fieldsArr['shortdesc_cah'] = $row_recSet['shortdesc_cah'];
			$fieldsArr['url_cah'] = $row_recSet['url_cah'];
			$score = 0;
			for ($j=1;$j<5;$j++) {
				$score += $this->importanceArray[$j] * $this->getScore($row_recSet['col'.$j.'_cah'], $searchFor);
			}
			$fieldsArr['score'] = $score * $row_recSet['importance_cah'];
			$ordArr[$i] = $fieldsArr;
			$i++;
		} while ($row_recSet = mysql_fetch_assoc($recSet));
		$ready = false;
		$nrRec = mysql_num_rows($recSet); 
		while(!$ready) {
			$ready = true;
			for($i=0;$i<$nrRec-1;$i++) {
				if ($ordArr[$i]['score']<$ordArr[$i+1]['score']) {
					$aux = $ordArr[$i];
					$ordArr[$i] = $ordArr[$i+1];
					$ordArr[$i+1] = $aux;
					$ready = false;
				}
			}
		}
		return array_slice($ordArr, $start, $length);
	}
	
	function computeFullSearch($dbType)
	{
		$searchFor = $this->searchFor;
		switch ($dbType) {
			case 'MySQL' : $this->computeMySQLFullSearch($this->searchType); break;
		}
	}
	//--------------------------------------------------------------------
	// FUNCTION:
	//   computeMySQLFullSearch
	//
	// DESCRIPTION:
	//  	Computes the SQL for the fulltext search 
	//	
	// ARGUMENTS:
	//   searchFor - search expression
	//
	// RETURNS:
	//   nothing
	//--------------------------------------------------------------------
	
	function computeMySQLFullSearch($fullType){
		$searchFor = $this->searchFor;
		if ($searchFor)	{
			$searchCond = explode(" and ", $searchFor);
			$first = true;
			for ($condIdx=0;$condIdx<count($searchCond);$condIdx++)	{
				if ($searchCond[$condIdx]!='') {
					if ($condIdx!=0) {
						$this->whereCondition .= " AND (";
					} else {
						$this->whereCondition .= " (";
					}	
					$searchCond[$condIdx] = str_replace(" or "," ", $searchCond[$condIdx]);
					if (!$first) {
						$this->orderBy .= " + ";
					} else {
						$this->orderBy .= " ( ";
					}
					$first = false;
					$this->whereCondition .= sprintf($this->sql['MySQL']['fulltext_where'][$fullType], 1, addslashes($searchCond[$condIdx]));
					$this->orderBy		.= " (".sprintf($this->sql['MySQL']['fulltext_order'][$fullType], 1, addslashes($searchCond[$condIdx]), $this->importanceArray[1]).") ";
					for ($relIdx=2; $relIdx <= 5; $relIdx++) {
						$this->orderBy		.= " + (".sprintf($this->sql['MySQL']['fulltext_order'][$fullType], $relIdx, addslashes($searchCond[$condIdx]), $this->importanceArray[$relIdx]).") ";
						$this->whereCondition .= " OR ".sprintf($this->sql['MySQL']['fulltext_where'][$fullType], $relIdx, addslashes($searchCond[$condIdx]));
					}
					$this->whereCondition .= " ) ";
				}
			}
			$this->orderBy		.= ") * importance_cah ";
			$this->searchColumns = ", ".$this->orderBy." as score ";	
			$this->orderBy		.= " DESC";
            
		} else {
			$this->whereCondition = " 1=1 ";
			$this->orderBy = " importance_cah DESC ";
		}
	}
	

	
	function getKeywords() {
		if ($this->searchFor) {
			return stripslashes(str_replace('"', '&quot;', $this->searchFor)); 
		} else {
			return '';
		}
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   getSearchColumns
	//
	// DESCRIPTION:
	//   Gets the aditional search columns
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   String with the column names or false if no aditional search columns
	//--------------------------------------------------------------------
	
	function getSearchColumns() {
		if (isset($this->searchColumns))
			return $this->searchColumns;
		else
			return false;
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   getWhereConditions
	//
	// DESCRIPTION:
	//   Gets the where conditions for the SQL in the Advanced Recordset
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   String with the conditions or false if none defined
	//--------------------------------------------------------------------
	
	function getWhereCondition(){
		if (isset($this->whereCondition))
			return $this->whereCondition;
		else
			return false;
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   getOrderBy
	//
	// DESCRIPTION:
	//   Gets the order by statement for the SQL in the Advanced Recordset
	//
	// ARGUMENTS:
	//   none
	//
	// RETURNS:
	//   String with the order by statement or false if not defined
	//--------------------------------------------------------------------
	
	function getOrderBy(){
		if (isset($this->orderBy))
			return $this->orderBy;
		else
			return false;
	}
	
	//--------------------------------------------------------------------
	// FUNCTION:
	//   formatDescription
	//
	// DESCRIPTION:
	//   Formats the description of each result for hilighting the found words
	//
	// ARGUMENTS:
	//   text - the description text
	//
	// RETURNS:
	//   the formatted text
	//--------------------------------------------------------------------
	
	function cleanText($text)
	{
		$text = stripslashes(str_replace(array(" and "," or ","\""), " ", strtolower($text)));
		return $text;
	}
	
	function formatDescription($text){
		$maxChars = 100;
		$font = array(0=>"<span class=\"highlight\">",1=>"</span>");
		$searchFor = $this->searchFor;
		if(!trim($text)) //empty result
			return "";
		$ltext		= strtolower($text);
		if ($this->databaseType=='MySQL' && $this->searchType=='fulltext') {
			$searchFor = $this->cleanText($searchFor);
		}
		$expr = "/\"([^\"]*)\"/m";
		$matches = array();
		preg_match_all($expr, $searchFor, $matches);
		$searchFor = preg_replace($expr,"",$searchFor);
		$searchFor = $this->cleanText($searchFor);		
		$arrSearchFor 	= explode(" ",$searchFor);
		if (is_array($matches[1])) {
			foreach($matches[1] as $key=>$value) {
				array_push($arrSearchFor, $value);
			}
		}
		if (strlen($text) > 100) {
			foreach ($arrSearchFor as $key=>$value) {
				if ($value!='') {	
					$pos = strpos($ltext, $value);
					if ($pos) break;
				}
			}
			$leftLength = $pos;
			$rightLength = strlen(substr($ltext, $pos));
			if ($leftLength < 50) {
				$start = 0;
				$startStr = "";
				$endStr = "...";
			} else if ($rightLength < 50) {
				$start = $pos - (100 - $rightLength);
				$startStr = "...";
				$endStr = "";
			} else {
				$start = $pos - 50;
				$startStr = "...";
				$endStr = "...";
			}
			$firstSpace = 0;
			$lastSpace = strlen($text);
			if ($start!=0) {
				$firstSpace = strrpos(substr($text, 0, $start), " ");
			}
			if ($start + 100 <strlen($text)) {
				$lastSpace = strpos($text, " ", $start + 100);
			}
			$text = $startStr.substr($text, $firstSpace, $lastSpace - $firstSpace).$endStr;		
		} 
		
		$ltext = strtolower($text);
		$indexArr = array();	
		for ($i=0;$i<count($arrSearchFor);$i++) {
			if ($arrSearchFor[$i]!='') {
				$offset = 0;
				for ($j=0;$j<substr_count($ltext, $arrSearchFor[$i]);$j++) {
						$offset = strpos($ltext, $arrSearchFor[$i], $offset);
						$indexArr[] = array($offset, 0, $i);
						$offset += strlen($arrSearchFor[$i]);
						$indexArr[] = array($offset, 1);
				}
			}
		}
		$ready=false;
		while(!$ready) {
			$ready = true;
			for($i=0;$i<count($indexArr)-1;$i++) {
				if ($indexArr[$i][0]>$indexArr[$i+1][0]) {
					$aux = $indexArr[$i];
					$indexArr[$i] = $indexArr[$i+1];
					$indexArr[$i+1] = $aux;
					$ready = false;
				}
			}
		} 
		$displayText = "";
		$end = 0;
		for ($i=0; $i<count($indexArr); $i++) {
			if ($i!=0) {
				$start = $indexArr[$i-1][0];
			} else {
				$start = 0;
			}
			$end = $indexArr[$i][0];
			$type = $indexArr[$i][1];
			$displayText .= substr($text, $start, $end-$start).$font[$type];
		}
		$displayText .= substr($text, $end);
		return $displayText;
	}
}
//end class
?>