<?php
/**
 * auth/dokuwiki_modx.class.php
 *
/*******************************************************************************
	Copyright (c) Intersel 2010 - Tous droits r serv s
********************************************************************************
  INTERSEL - SARL au capital de 12.000 � - RCS PARIS 488 379 660 - NAF 721Z
	4 Cit  d'Hauteville
	75010 PARIS
********************************************************************************
	@copyright : Intersel 2010
	@version : 0.1
	Modifications :
		- 14/08/2010 - Creation E.PODVIN
		- 03/08/2011 - update for �Rincewind� dokuwiki version
		  - change "logoff" by "logout" on the cando functions
		  - add a test in trustExternal function in order to see if a user is logged through the manager 
		- 20/01/2016 - S. Branche update for "Weatherwax" dokuwiki version
*******************************************************************************
 */
// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();

global $OSDOKUWIKI_GLOBALSUPERVISOR_ID; // id of the user general admin in ModX
//initialisation of admin users list
$OSDOKUWIKI_GLOBALSUPERVISOR_ID=array(1); // id of the user general admin in ModX

global $OSDOKUWIKI_GLOBALSUPERVISORS_GROUP; 
$OSDOKUWIKI_GLOBALSUPERVISORS_GROUP=array('admin'); //dokuwiki admin groups

global $OSDOKUWIKI_STANDARDUSERS_GROUP;
$OSDOKUWIKI_STANDARDUSERS_GROUP=array('users'); //dokuwiki standard user groups


global $modx; 
//create a local modx
if (!isset($modx)) 
    {
      define ('MODX_BASE_PATH', dirname (__file__).'/../../../../../../');
      include_once(MODX_BASE_PATH.'modxapi.php');
      $modx = new MODxAPI();
      $modx->connect();
      $modx->getSettings();  
    }

class auth_plugin_dokuwikimodx extends DokuWiki_Auth_Plugin {
	var $success = true;

	function __construct() {
		global $config_cascade;
 
		$this->cando['external'] = true;

	}

  /**
   * Do all authentication [ OPTIONAL ]
   *
   * Set $this->cando['external'] = true when implemented
   *
   * If this function is implemented it will be used to
   * authenticate a user - all other DokuWiki internals
   * will not be used for authenticating, thus
   * implementing the checkPass() function is not needed
   * anymore.
   *
   * The function can be used to authenticate against third
   * party cookies or Apache auth mechanisms and replaces
   * the auth_login() function
   *
   * The function will be called with or without a set
   * username. If the Username is given it was called
   * from the login form and the given credentials might
   * need to be checked. If no username was given it
   * the function needs to check if the user is logged in
   * by other means (cookie, environment).
   *
   * The function needs to set some globals needed by
   * DokuWiki like auth_login() does.
   *
   * @see auth_login()
   * @author  Andreas Gohr <andi@splitbrain.org>
   *
   * @param   string  $user    Username
   * @param   string  $pass    Cleartext Password
   * @param   bool    $sticky  Cookie should not expire
   * @return  bool             true on successful auth
   */
  function trustExternal($user,$pass,$sticky=false){
    global $USERINFO;
    global $conf;
    global $modx;
    global $OSDOKUWIKI_GLOBALSUPERVISOR_ID; // id of the user general admin in ModX
	global $OSDOKUWIKI_GLOBALSUPERVISORS_GROUP; 
	global $OSDOKUWIKI_STANDARDUSERS_GROUP;

	$modxcontext='';
	
    $sticky ? $sticky = true : $sticky = false; //sanity check

    $myUserId = $modx->getLoginUserID();  
  //$myDocId is the modx document where is included the wiki
    $myDocId=$modx->documentIdentifier;
    
    // non connect� => on sort
    if (empty($myUserId))  
    { 
		$modxcontext='mgr';
		$myUserId = $modx->getLoginUserID($modxcontext);  //verify if we are logged in the manager to have 'admin' access...
		if (empty($myUserId))  
		{
			$this->success =false;
			return false;
		}
    }
    $user = $USERINFO['name'] = $modx->getLoginUserName($modxcontext);
    $USERINFO['mail'] = "";

    // here an example of set rights to users... up to you to do as you would like them to be set
    //if admin then will have admin rights
    if (in_array($myUserId,$OSDOKUWIKI_GLOBALSUPERVISOR_ID)) $USERINFO['grps'] = $OSDOKUWIKI_GLOBALSUPERVISORS_GROUP;
    //else standard user right
    else $USERINFO['grps'] = $OSDOKUWIKI_STANDARDUSERS_GROUP;

    $_SERVER['REMOTE_USER'] = $user;
    $_SESSION[DOKU_COOKIE]['auth']['user'] = $user;
    $_SESSION[DOKU_COOKIE]['auth']['pass'] = '';
    $_SESSION[DOKU_COOKIE]['auth']['info'] = $USERINFO;
    
	return true;

  }
}