<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

header('Content-Type: text/html; charset=utf-8');
require 'includes'.DIRECTORY_SEPARATOR.'constants.php';
error_reporting(DEBUG ? E_ALL : E_ERROR);
require 'classes'.DIRECTORY_SEPARATOR.'errorhandler.php';
require 'classes'.DIRECTORY_SEPARATOR.'setup.php';
require 'classes'.DIRECTORY_SEPARATOR.'page.php';
require 'classes'.DIRECTORY_SEPARATOR.'adminpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'loginpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryset.php';
require 'classes'.DIRECTORY_SEPARATOR.'gallery.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlcheck.php';
require 'classes'.DIRECTORY_SEPARATOR.'auth.php';
require 'classes'.DIRECTORY_SEPARATOR.'pathparser.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlparser.php';
require 'includes'.DIRECTORY_SEPARATOR.'phpcompat.php';
require 'classes'.DIRECTORY_SEPARATOR.'htmlentitydecode.php';
require 'classes'.DIRECTORY_SEPARATOR.'rcopy.php';
$setup = new Setup();
$auth = new Auth();
$errorHandler = & new ErrorHandler();
$setup->checkLogin($auth);
$page = new AdminPage();
print $page->getHtmlHead();
print $page->getPageHeader();
$gallerySet = new GallerySet();
ob_start();
$okPass = true;
$match = true;
$okNewPass = true;
$okOldPass = true;
$changed = false;
if (isset($_POST['adminsubmitted']))
{
  $user = trim($_POST['user']);
  $password = trim($_POST['password1']);
  $match = ($password == trim($_POST['password2']));
  $okNewPass = (strlen($password) > 4);
  $okOldPass = $auth->confirmPass($_POST['password']);
  if ($match && $okOldPass && $okNewPass)
  {
    $changed = $auth->changeLogin($user, $password);
  }
}
print $page->getAdminContent($okOldPass, $match, $okNewPass, $changed);
$mainOutput = ob_get_clean();
print $errorHandler->getMessages();
print $mainOutput;
print $page->getFooter();
?>