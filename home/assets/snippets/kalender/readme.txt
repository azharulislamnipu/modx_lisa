Kalender Enhanced
----------

A tiny calendar app for MODx.
 
Created by: Danny van Ommen (dannyvanommen@gmail.com)
Created on: November 27, 2006

Modified by: Joey Livingston (tiltedsymmetry.com)
Modified on: December 8, 2007

Version: 0.1
 
Details
-------
 
"KALENDER", which is basically Dutch for "Calendar",
is a simple calendar tool that can be embedded into a website using
the MODx content management system.


Installing
----------

1. Download the snippet zip file from the MODx Repository.

2. Extract the snippet zip file and place it as a folder called "kalender" in
   your "/assets/snippets/" folder on your server.

3. You will find the following files and folders extracted:

   => readme.txt
   => kalender_screenshots.gif   
   => kalender_snippet.php
   => kalender_standard.php
   => kalender_enhanced.php
   => themes
      => gold
         => images
            => (images - *.gif files)
         => theme.css
         => theme.php
      => gold_enhanced
         => images
            => (images - *.gif files)
         => theme.css
         => theme.php
   => js
      => (overLib Javascript library - *.js files)  
      
4. Create a new snippet called "Kalender" at the MODx Manager
   (Resources=>Manager Resources=>Snippets=>New snippet)
   
5. Copy and paste everything from the file "kalender_snippet.php"
   as Snippet code and "Save".

6. Create two TVs:
	1) 	Variable Name: StartTime
		Input Type: Date
		Widget: Unixtime
		
	2) 	Variable Name: EndTime
		Input Type: Date
		Widget: Unixtime
		



Embedding Kalender into your template
-------------------------------------

To display the calendar to your website template, just use this snippet call:
 
[!Kalender?parent=1257!]
 
Where parent=1257 points to the document id number where Kalender data resides.

MODx content documents that are stored as 'children' of $parent will feed Kalender.


Managing Kalender data using the MODx manager
---------------------------------------------
 
1) Only documents that have 'StartTime' set and are placed either directly
   as a 'child-document' or any level below $parent will be reflected in the
   Kalender.
    
2) Use 'StartTime' to mark your event in the Kalender.
 
3) You may also use 'EndTime' for events that will last more than 1 day long.
 
4) Use "00:00:00" (midnight) to leave time notations blank.
 
5) When modifying the theme, take note that all CSS classes located in "theme.css"
   as well as PHP variables located in "theme.php" MUST remain in-tact.
    
* New themes may be created by simply copying all elements in the default 'gold'
  theme folder and pasting it as a new folder with a new theme name of your
  choice.
  
More on Themes
--------------

By default, we have the 'gold' theme available for you.

But if you wish to create your own theme, just create a new folder in the "themes" 
directory and copy EVERYTHING from the default 'gold' folder.

You are free to change the values, dimensions, etc of the theme files.
But it is IMPORTANT that you retain all file names, css classes and php 
variables intact.  

Example:

  If I wanted to create an "aqua" theme my file structure would look like:
  
  => themes
      => gold
         => images
            => (images - *.gif files)
         => theme.css
         => theme.php
      => aqua
         => images
            => (images - *.gif files)
         => theme.css
         => theme.php         
         
  And my snippet call would look like this:
  
  [!Kalender?parent=1257&theme=`aqua`!]


Snippet Parameters Explained
----------------------------
 
&theme [string] ['gold'] : Color theme (should be same name as theme folder).
 
&parent [int] [-1] : MODx document/folder id where calendar documents are stored.
 
&mm [int] [current month] : Month without leading zeros. Defaults to current month
                            when blank.
 
&yyyy [int] [current year] : Year. Defaults to current year when blank.  

&showInfo [string] [both] : Content to display for each event (in addition to the description), defaults to 'both':
							'intro' : just the introtext (summary) field
							'content' : just the content field
							'both' : both the introtext (summary) and the content field
							'simple' (or any other string) : display nothing (just the description field)
							
&mode [string] [enhanced] : Type of calendar to show, defaults to 'enhanced':
							'standard' : the original Kalender, small calendar with no text, 
										 highlighted event dates, tooltip popup details on hover
							'enhanced' : the enhanced Kalender, larger, expanded view (default 870px wide),
										 large date blocks, summarize list of events on each date,
										 detailed popup tooltips for each event on hover


Credits
------- 
 
As with all calendar apps, this one isn't the most original so credit goes to 
where credit is due.
   
1) The idea for this snippet came out of EasyPHPCalendar.
   (http://www.easyphpcalendar.com)
   
2) Also, Kalender extensively uses Erik Bosrup's "overLib" Javascript library
  (http://www.bosrup.com/web/overlib)


License
-------

Everything's FREE. No restrictions. Seriously.
I won't even get mad if you don't mention my name.

Just remember, you're using this at your own risk and I doubt I'll have the
time to provide you much support.

But if you want to modify, expand and change the code, feel absolutely free to do so.

Just keep in mind to respect Erik Bosrup's license when using his overLib Javascript
library.

Also, if you're happy with this Snippet and find this extremely useful,
or the company you're working for is thrilled with this calender and is even
perhaps making a bit of money out of it, as always, donations are very much welcome
and your generosity will be eternally appreciated. :)

Just email me: dannyvanommen@gmail.com

I'm Dutch-Nepalese but I live in Manila, the Philippines. (Not the richest of 
countries to be in). US$20 can go a looong way here. :D

Cheers!