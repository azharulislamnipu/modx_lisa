<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("DEGREE", true, "text", "", "", "", "");
$formValidation->addField("INSTITUTION", true, "text", "", "", "", "");
$formValidation->addField("DEGREEYEAR", true, "numeric", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an update transaction instance
$upd_spp_educ = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_educ);
// Register triggers
$return = !empty( $_POST['RETURN'] ) ? $_POST['RETURN'] : '/home/personal-profile.php';
$upd_spp_educ->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_educ->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_educ->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$upd_spp_educ->setTable("spp_educ");
$upd_spp_educ->addColumn("DEGREE", "STRING_TYPE", "POST", "DEGREE");
$upd_spp_educ->addColumn("INSTITUTION", "STRING_TYPE", "POST", "INSTITUTION");
$upd_spp_educ->addColumn("STARTDATE", "DATE_TYPE", "POST", "STARTDATE");
$upd_spp_educ->addColumn("ENDDATE", "DATE_TYPE", "POST", "ENDDATE");
$upd_spp_educ->addColumn("DEGREEYEAR", "NUMERIC_TYPE", "POST", "DEGREEYEAR");
$upd_spp_educ->addColumn("MAJOR", "STRING_TYPE", "POST", "MAJOR");
$upd_spp_educ->addColumn("MINOR1", "STRING_TYPE", "POST", "MINOR1");
$upd_spp_educ->addColumn("MINOR2", "STRING_TYPE", "POST", "MINOR2");
$upd_spp_educ->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$upd_spp_educ->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_educ->addColumn("LASTUPDATED", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_educ->addColumn("IPADDRESS", "STRING_TYPE", "VALUE", "{SERVER.REMOTE_ADDR}");
$upd_spp_educ->setPrimaryKey("DEGREEID", "NUMERIC_TYPE", "POST", "DEGREEID");

// Make an instance of the transaction object
$del_spp_educ = new tNG_delete($conn_sigma_modx);
$tNGs->addTransaction($del_spp_educ);
// Register triggers
$del_spp_educ->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_educ->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$del_spp_educ->setTable("spp_educ");
$del_spp_educ->setPrimaryKey("DEGREEID", "NUMERIC_TYPE", "POST", "DEGREEID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_educ = $tNGs->getRecordset("spp_educ");
$row_rsspp_educ = mysql_fetch_assoc($rsspp_educ);
$totalRows_rsspp_educ = mysql_num_rows($rsspp_educ);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">Update Education Information</h1>
<div class="contentBlock">
  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="DEGREE">DEGREE:</label></td>
        <td><input type="text" name="DEGREE" id="DEGREE" value="<?php echo KT_escapeAttribute($row_rsspp_educ['DEGREE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("DEGREE");?> <?php echo $tNGs->displayFieldError("spp_educ", "DEGREE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="INSTITUTION">INSTITUTION:</label></td>
        <td><input type="text" name="INSTITUTION" id="INSTITUTION" value="<?php echo KT_escapeAttribute($row_rsspp_educ['INSTITUTION']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("INSTITUTION");?> <?php echo $tNGs->displayFieldError("spp_educ", "INSTITUTION"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="STARTDATE">START DATE:</label></td>
        <td><input type="text" name="STARTDATE" id="STARTDATE" value="<?php echo KT_formatDate($row_rsspp_educ['STARTDATE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("STARTDATE");?> <?php echo $tNGs->displayFieldError("spp_educ", "STARTDATE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ENDDATE">END DATE:</label></td>
        <td><input type="text" name="ENDDATE" id="ENDDATE" value="<?php echo KT_formatDate($row_rsspp_educ['ENDDATE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ENDDATE");?> <?php echo $tNGs->displayFieldError("spp_educ", "ENDDATE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="DEGREEYEAR">DEGREE YEAR:</label></td>
        <td><input type="text" name="DEGREEYEAR" id="DEGREEYEAR" value="<?php echo KT_escapeAttribute($row_rsspp_educ['DEGREEYEAR']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("DEGREEYEAR");?> <?php echo $tNGs->displayFieldError("spp_educ", "DEGREEYEAR"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="MAJOR">MAJOR:</label></td>
        <td><input type="text" name="MAJOR" id="MAJOR" value="<?php echo KT_escapeAttribute($row_rsspp_educ['MAJOR']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MAJOR");?> <?php echo $tNGs->displayFieldError("spp_educ", "MAJOR"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="MINOR1">MINOR 1:</label></td>
        <td><input type="text" name="MINOR1" id="MINOR1" value="<?php echo KT_escapeAttribute($row_rsspp_educ['MINOR1']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MINOR1");?> <?php echo $tNGs->displayFieldError("spp_educ", "MINOR1"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="MINOR2">MINOR 2:</label></td>
        <td><input type="text" name="MINOR2" id="MINOR2" value="<?php echo KT_escapeAttribute($row_rsspp_educ['MINOR2']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MINOR2");?> <?php echo $tNGs->displayFieldError("spp_educ", "MINOR2"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="COMMENT">COMMENT:</label></td>
        <td><textarea name="COMMENT" id="COMMENT" cols="80" rows="10"><?php echo KT_escapeAttribute($row_rsspp_educ['COMMENT']); ?></textarea>
            <?php echo $tNGs->displayFieldHint("COMMENT");?> <?php echo $tNGs->displayFieldError("spp_educ", "COMMENT"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Update Information" />
		<input type="button" name="KT_Delete1" value="Delete" onclick="MM_goToURL('parent','profile_education_edit.php');return document.MM_returnValue" />
		<input type="button" name="Cancel" value="Cancel" onclick="history.go(-1)" /></td>
      </tr>
    </table>
	<input name="DEGREEID" type="hidden" value="<?php echo KT_escapeAttribute($_POST['DEGREEID']); ?>" />
    <input name="UPDATEDBY" type="hidden" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($_POST['UPDATEDBY']); ?>" />
    <input type="hidden" name="RETURN" id="RETURN" value="<?php echo KT_escapeAttribute($_POST['RETURN']); ?>" />
  </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
