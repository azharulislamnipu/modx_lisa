/**
 * SimpleViewer admin package.
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
 /**
 @var string serialized form inputs
 */
 snapshot = '';
 
/**
 @var boolean form has been submitted
 */
 submitted = false;
 
 window.addEvent('domready', formSnapshot);
 // adding beforeunload event in MooTools doesn't seem to work
 window.onbeforeunload =  confirmExit;
 
 /**
 * Serializes first form object on page load
 * @return void
 */
function formSnapshot()
{
  var forms = $$('form.public');
  if (forms.length == 0) return;
  snapshot = forms[0].toQueryString();
  forms[0].addEvent('submit', function(){
    submitted = true;
  }
  );
}

/**
 * Tests for changes in input form
 * @param object event
 * @return string
 */
function confirmExit(e)
{
  if ($$('body')[0].id == 'login') return;
  if (submitted) return;
  var forms = $$('form.public');
  if (forms.length == 0) return;
  var snap2 = forms[0].toQueryString();
  if (snap2 != snapshot)
  {
    return 'You have not saved your changes';
    //alternative syntax below
    //e.returnValue = 'You have not saved your changes';
  }
}
