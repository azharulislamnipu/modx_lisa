<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchons = "-1";
if (isset($_GET['WEB_ID'])) {
  $colname_rsArchons = $_GET['WEB_ID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchons = sprintf("SELECT * FROM vw_web_elig_no_officers WHERE WEB_ID = %s", GetSQLValueString($colname_rsArchons, "int"));
$rsArchons = mysql_query($query_rsArchons, $sigma_modx) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysql_num_rows($rsArchons);

$colname_rsEducation = "-1";
if (isset($_GET['WEB_ID'])) {
  $colname_rsEducation = $_GET['WEB_ID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsEducation = sprintf("SELECT * FROM vw_web_elig_educ WHERE WEB_ID = %s ORDER BY DEGREEYEAR ASC", GetSQLValueString($colname_rsEducation, "int"));
$rsEducation = mysql_query($query_rsEducation, $sigma_modx) or die(mysql_error());
$row_rsEducation = mysql_fetch_assoc($rsEducation);
$totalRows_rsEducation = mysql_num_rows($rsEducation);

$colname_rsOffices = "-1";
if (isset($_GET['WEB_ID'])) {
  $colname_rsOffices = $_GET['WEB_ID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOffices = sprintf("SELECT * FROM vw_web_elig_officers WHERE WEB_ID = %s ORDER BY START_DATE DESC", GetSQLValueString($colname_rsOffices, "int"));
$rsOffices = mysql_query($query_rsOffices, $sigma_modx) or die(mysql_error());
$row_rsOffices = mysql_fetch_assoc($rsOffices);
$totalRows_rsOffices = mysql_num_rows($rsOffices);

$colname_rsBoule = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname_rsBoule = $_GET['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoule = sprintf("SELECT * FROM vw_web_elig_no_officers WHERE BOULENAME = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsBoule, "text"));
$rsBoule = mysql_query($query_rsBoule, $sigma_modx) or die(mysql_error());
$row_rsBoule = mysql_fetch_assoc($rsBoule);
$totalRows_rsBoule = mysql_num_rows($rsBoule);

$colname_rsJobs = "-1";
if (isset($_GET['webInternalKey'])) {
  $colname_rsJobs = $_GET['webInternalKey'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsJobs = sprintf("SELECT spp_prof.PROFID,   spp_prof.TITLEID,   spp_skillref.DESCRIPTION AS JOBTITLE,   spp_prof.CUSTOMERID,   spp_prof.COMPANYNAME,   spp_prof.COMPANYCITY,   spp_prof.COMPANYSTCD,   spp_prof.DESCRIPTION,   spp_prof.COMPANYJOBSTART FROM spp_prof   INNER JOIN spp_skillref ON (spp_prof.TITLEID = spp_skillref.SKILLID) WHERE CUSTOMERID = %s ORDER BY COMPANYJOBSTART DESC", GetSQLValueString($colname_rsJobs, "int"));
$rsJobs = mysql_query($query_rsJobs, $sigma_modx) or die(mysql_error());
$row_rsJobs = mysql_fetch_assoc($rsJobs);
$totalRows_rsJobs = mysql_num_rows($rsJobs);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsSkills = "SELECT SKILLID, `DESCRIPTION` FROM spp_skillref ORDER BY `DESCRIPTION` ASC";
$rsSkills = mysql_query($query_rsSkills, $sigma_modx) or die(mysql_error());
$row_rsSkills = mysql_fetch_assoc($rsSkills);
$totalRows_rsSkills = mysql_num_rows($rsSkills);

$colname_rsArchonOcc = "-1";
if (isset($_GET['webInternalKey'])) {
  $colname_rsArchonOcc = $_GET['webInternalKey'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchonOcc = sprintf("SELECT spp_prof.OCCUPATIONID,   spp_occupation.DESCRIPTION,   spp_prof.CUSTOMERID FROM spp_prof   INNER JOIN spp_occupation ON (spp_prof.OCCUPATIONID = spp_occupation.OCCUPATIONID) WHERE spp_prof.CUSTOMERID = %s GROUP BY OCCUPATIONID", GetSQLValueString($colname_rsArchonOcc, "int"));
$rsArchonOcc = mysql_query($query_rsArchonOcc, $sigma_modx) or die(mysql_error());
$row_rsArchonOcc = mysql_fetch_assoc($rsArchonOcc);
$totalRows_rsArchonOcc = mysql_num_rows($rsArchonOcc);

$colname_rsArchonTitles = "-1";
if (isset($_GET['webInternalKey'])) {
  $colname_rsArchonTitles = $_GET['webInternalKey'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchonTitles = sprintf("SELECT spp_prof.CUSTOMERID,   spp_prof.TITLEID,   spp_skillref.DESCRIPTION FROM spp_prof   INNER JOIN spp_skillref ON (spp_prof.TITLEID = spp_skillref.SKILLID) WHERE spp_prof.CUSTOMERID = %s GROUP BY TITLEID", GetSQLValueString($colname_rsArchonTitles, "int"));
$rsArchonTitles = mysql_query($query_rsArchonTitles, $sigma_modx) or die(mysql_error());
$row_rsArchonTitles = mysql_fetch_assoc($rsArchonTitles);
$totalRows_rsArchonTitles = mysql_num_rows($rsArchonTitles);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  dir="ltr" lang="en-EN"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>>Sigma Pi Phi Fraternity | Archon Profile</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_regional_grammateus.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<div class="floatLeft width460">
<h1 class="yellow">Contact Information</h1>
<div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td valign="top">
  <?php 
// Show IF Conditional region10 
if (@$row_rsArchons['IMAGE'] != NULL) {
?>
      <img src="/home/assets/images/archons/<?php echo $row_rsArchons['IMAGE']; ?>" width="190"/>
      <?php 
// else Conditional region10
} else { ?>
	    <img src="../../includes/tng/styles/img_not_found.gif" alt="Add your image." width="100" height="100" />
  <?php } 
// endif Conditional region10
?>
	</td>
    <td><p><span class="larger"><?php echo $row_rsArchons['L2R_FULLNAME']; ?></span><br />
          <?php echo $row_rsArchons['ADDRESS1']; ?><br />
          <?php echo $row_rsArchons['CITY']; ?>, <?php echo $row_rsArchons['STATECD']; ?> <?php echo $row_rsArchons['ZIP']; ?> </p>
      <p>Home Phone: <?php echo $row_rsArchons['HOMEPHONE']; ?><br />
        Email: <?php echo $row_rsArchons['EMAIL']; ?><br />
      </p>	  </td>
  </tr>
</table>
</div>
<div class="contentFooter">
<script language="JavaScript">
    function contact_edit(){
        document.forms.contact.submit();
    }
</script>
  <form name="contact" action="/services/grammateus/regional/profile_contact_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsArchons['CUSTOMERID']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="contact_edit();return(FALSE);">Edit Contact Information</a></span>  
</div>
  <h1 class="yellow">Personal Information </h1>
  <div class="contentBlock">
    <?php 
// Show IF Conditional region6 
if (@$row_rsArchons['SPOUSENAME'] != NULL) {
?>
      <table border="0" cellspacing="5" cellpadding="5">
        <tr>
          <td valign="top">BIRTH DATE:</td>
          <td><?php 
// Show IF Conditional region7 
if (@$row_rsArchons['BIRTHDATE'] != NULL) {
?>
              <?php echo KT_formatDate($row_rsArchons['BIRTHDATE']); ?>
              <?php 
// else Conditional region7
} else { ?>
Click the edit link below to add your birth date.
<?php } 
// endif Conditional region7
?></td>
        </tr>
        <tr>
          <td valign="top">ARCHOUSA</td>
          <td><?php 
// Show IF Conditional region9 
if (@$row_rsArchons['SPOUSENAME'] != NULL) {
?>
              <?php echo $row_rsArchons['SPOUSENAME']; ?>
              <?php 
// else Conditional region9
} else { ?>
              Click the edit link below to add your Archousa's name.
  <?php } 
// endif Conditional region9
?></td>
        </tr>
      </table>
      
      <?php } 
// endif Conditional region6
?>
</div>
<div class="contentFooter">
<script language="JavaScript">
    function personal(){
        document.forms.personal_edit.submit();
    }
</script>
  <form name="personal_edit" action="/services/grammateus/regional/profile_personal_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsArchons['CUSTOMERID']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="personal();return(FALSE);">Edit</a></span>   
</div>

</div>



<div class="floatRight width305">
  <h1 class="yellow">Boul&#233; Information</h1>
  <div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td colspan="2"><?php echo $row_rsArchons['BOULENAME']; ?> Boul&eacute;<br />
        <?php echo $row_rsArchons['BOULECITY']; ?>, <?php echo $row_rsArchons['BOULESTATECD']; ?><br />
        <?php echo $row_rsArchons['REGIONNAME']; ?> Region </td>
  </tr>
  <?php 
// Show IF Conditional region5 
if (@$row_rsArchons['JOINDATE'] != NULL) {
?>
    <tr>
      <td valign="top">JOIN DATE </td>
      <td><?php echo KT_formatDate($row_rsArchons['JOINDATE']); ?></td>
    </tr>
    <?php } 
// endif Conditional region5
?>
  <tr>
    <?php if ($totalRows_rsOffices > 0) { // Show if recordset not empty ?>
      <td valign="top">OFFICES HELD </td>
      <td><?php do { ?>
          <div class="list"><?php echo $row_rsOffices['CPOSITION']; ?>, <?php echo $row_rsOffices['START_DATE']; ?></div>
          <?php } while ($row_rsOffices = mysql_fetch_assoc($rsOffices)); ?></td>
      <?php } // Show if recordset not empty ?></tr>
</table>
</div>
<div class="contentFooter">
<script language="JavaScript">
    function join(){
        document.forms.join_edit.submit();
    }
</script>
  <form name="join_edit" action="/services/grammateus/regional/profile_join_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsArchons['CUSTOMERID']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="join();return(FALSE);">Edit Join Date</a></span>   
</div>

</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchons);

mysql_free_result($rsEducation);

mysql_free_result($rsOffices);

mysql_free_result($rsBoule);

mysql_free_result($rsJobs);

mysql_free_result($rsSkills);

mysql_free_result($rsArchonOcc);

mysql_free_result($rsArchonTitles);
?>
