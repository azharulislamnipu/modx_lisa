<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("{SESSION.webEmail}");
  $emailObj->setCC("");
  $emailObj->setBCC("");
  $emailObj->setSubject("Involuntary Inactive Form");
  //WriteContent method
  $emailObj->setContent("<p>Archon {SESSION.webFullname},</p>\n\n<p>Attached to this email is the file INVOLUNTARY_INACTIVE_FORM.zip, which contains the Voluntary Inactive Status form in Adobe Acrobat format.</p>\n\n<p>Please follow the directions on the form to initiate the process.</p>\n\n<p>If you have any questions regarding this transfer, please contact the Office of the Grand Boul&eacute; at info@sigmapiphi.org.</p>\n\n\n");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  //Attachments
  $emailObj->addAttachment("custom");
  $emailObj->setAttachmentBaseFolder("../../grammateus/forms/");
  $emailObj->setAttachmentRenameRule("{POST.formName}");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

//start Trigger_SendEmail1 trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail1(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("info@sigmapiphi.org");
  $emailObj->setCC("");
  $emailObj->setBCC("katrinaspp@bellsouth.net,lisa.jeter@gmail.com");
  $emailObj->setSubject("Involuntary Inactive Form Request Notification");
  //WriteContent method
  $emailObj->setContent("<p>Archon {SESSION.webFullname} of {SESSION.BOULENAME} requested an Involuntary Inactive Status Form for:</p>\n\n<p>CUSTOMERCD: {GET.CUSTOMERID}</p>\n\n<p>Webmaster</p>");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail1 trigger

// begin Recordset
$colname__rsArchon = '-1';
if (isset($_GET['CUSTOMERID'])) {
  $colname__rsArchon = $_GET['CUSTOMERID'];
}
$query_rsArchon = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname__rsArchon, "int"));
$rsArchon = $sigma_modx->SelectLimit($query_rsArchon) or die($sigma_modx->ErrorMsg());
$totalRows_rsArchon = $rsArchon->RecordCount();
// end Recordset

// begin Recordset
$colname__rsGramm = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsGramm = $_SESSION['BOULENAME'];
}
$query_rsGramm = sprintf("SELECT * FROM vw_boule_officers WHERE CPOSITION='Grammateus' AND BOULENAME = %s", GetSQLValueString($colname__rsGramm, "text"));
$rsGramm = $sigma_modx->SelectLimit($query_rsGramm) or die($sigma_modx->ErrorMsg());
$totalRows_rsGramm = $rsGramm->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_request = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_request);
// Register triggers
$ins_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_request->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
$ins_spp_request->registerTrigger("AFTER", "Trigger_SendEmail", 98);
$ins_spp_request->registerTrigger("AFTER", "Trigger_SendEmail1", 98);
// Add columns
$ins_spp_request->setTable("spp_request");
$ins_spp_request->addColumn("type_id", "NUMERIC_TYPE", "POST", "type_id", "3");
$ins_spp_request->addColumn("archon_ref_id", "NUMERIC_TYPE", "POST", "archon_ref_id");
$ins_spp_request->addColumn("req_date", "DATE_TYPE", "VALUE", "{now_dt}");
$ins_spp_request->addColumn("req_cmt", "STRING_TYPE", "POST", "req_cmt");
$ins_spp_request->addColumn("req_by", "NUMERIC_TYPE", "POST", "req_by", "{SESSION.webInternalKey}");
$ins_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_request = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_request);
// Register triggers
$upd_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_request->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$upd_spp_request->setTable("spp_request");
$upd_spp_request->addColumn("complete_date", "DATE_TYPE", "POST", "complete_date");
$upd_spp_request->addColumn("last_update", "DATE_TYPE", "POST", "last_update");
$upd_spp_request->addColumn("update_by", "NUMERIC_TYPE", "POST", "update_by");
$upd_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE", "GET", "req_id");

// Make an instance of the transaction object
$del_spp_request = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_request);
// Register triggers
$del_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "../../includes/nxt/back.php");
// Add columns
$del_spp_request->setTable("spp_request");
$del_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE", "GET", "req_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_request = $tNGs->getRecordset("spp_request");
$totalRows_rsspp_request = $rsspp_request->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" --><?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    [*pagetitle*] </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_request->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_request > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <?php 
// Show IF Conditional show_req_date_on_insert_only 
if (@$_GET['req_id'] == "") {
?>
              <tr>
                <td colspan="2" class="KT_th"><p style="margin-right:200px; font-size: 14px; padding-bottom: 8px;">Complete this form to have an Involuntary Inactive form sent to your website email address. </p></td>
              </tr>
              <tr>
                <td class="KT_th">REQUESTING ARCHON:</td>
                <td><span class="style1"><?php echo $rsArchon->Fields('L2R_FULLNAME'); ?></span></td>
              </tr>
            <tr>
              <td class="KT_th">DATE REQUESTED:</td>
              <td><?php echo KT_formatDate($rsspp_request->Fields('req_date')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_req_date_on_insert_only
?>
          <?php 
// Show IF Conditional show_req_cmt_on_insert_only 
if (@$_GET['req_id'] == "") {
?>
            <tr>
              <td class="KT_th"><label for="req_cmt_<?php echo $cnt1; ?>">COMMENT:</label></td>
              <td><textarea name="req_cmt_<?php echo $cnt1; ?>" id="req_cmt_<?php echo $cnt1; ?>" cols="80" rows="5"><?php echo KT_escapeAttribute($rsspp_request->Fields('req_cmt')); ?></textarea>
                  <?php echo $tNGs->displayFieldHint("req_cmt");?> <?php echo $tNGs->displayFieldError("spp_request", "req_cmt", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_req_cmt_on_insert_only
?>
          <?php 
// Show IF Conditional show_complete_date_on_update_only 
if (@$_GET['req_id'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="complete_date_<?php echo $cnt1; ?>">DATE COMPLETED:</label></td>
              <td><input type="text" name="complete_date_<?php echo $cnt1; ?>" id="complete_date_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_request->Fields('complete_date')); ?>" size="10" maxlength="22" />
                  <?php echo $tNGs->displayFieldHint("complete_date");?> <?php echo $tNGs->displayFieldError("spp_request", "complete_date", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_complete_date_on_update_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_request_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_request->Fields('kt_pk_spp_request')); ?>" />
        <input type="hidden" name="type_id_<?php echo $cnt1; ?>" id="type_id_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_request->Fields('type_id')); ?>" />
        <input type="hidden" name="archon_ref_id_<?php echo $cnt1; ?>" id="archon_ref_id_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsArchon->Fields('CUSTOMERID')); ?>" />
        <input type="hidden" name="req_by_<?php echo $cnt1; ?>" id="req_by_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>" />
        <input type="hidden" name="last_update_<?php echo $cnt1; ?>" id="last_update_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_request->Fields('last_update')); ?>" />
        <input type="hidden" name="update_by_<?php echo $cnt1; ?>" id="update_by_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>" />
        <input name="formName" type="hidden" id="formName" value="<?php echo KT_escapeAttribute('INVOLUNTARY_INACTIVE_FORM.zip'); ?>">
        <?php
    $rsspp_request->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['req_id'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchon->Close();

$rsGramm->Close();
?>
