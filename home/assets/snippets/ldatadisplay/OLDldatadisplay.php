<?php

class LDataDisplay {

    var $table = 'sadsa';
    var $sortBy;
    var $sortDir;
    var $filter;
    var $firstItem;
    var $display;
    var $noResult;
    var $phx;
    var $tpl="";
    var $tplAlt="";
    var $tplFirst="";
    var $tplLast="";

    function LDataDisplay() {
    }
    
    function buildQuery() {
        $sql = 'SELECT * FROM '.$this->table.$this->getWhereClausule().$this->getOrder().$this->getLimits();
        return $sql;
    }
    
    function buildFilter($key, $val, $kind) {
        $res = "`".$key."`";
        switch($kind) {
            case 1: $res .= " != '"; break;
            case 2: $res .= " = '"; break;
            case 3: $res .= " < '"; break;
            case 4: $res .= " > '"; break;
            case 5: $res .= " <= '"; break;
            case 6: $res .= " >= '"; break;
            case 7: $res .= " NOT LIKE '%"; break;
            case 8: $res .= " LIKE '%"; break;
        }
        $res .= $val;
        if($kind > 6) {
            $res .= "%";
        }
        $res .= "'";
        return $res;
    }
    
    function getWhereClausule() {
        if($this->filter == "") {
            return "";
        }
        $filters = explode('|',$this->filter);
        if(sizeof($filters) < 1) {
            return "";
        }
        $res = " WHERE ";
        $k = 0;
        foreach($filters as $flt) {
            if($k == 1) {
                $res .= " AND ";
            }
            $vals = explode(',',$flt);
            $res .= $this->buildFilter($vals[0],$vals[1],$vals[2]);
            $k = 1;
        }
        return $res;
    }
    
    function getOrder() {
        if($this->sortBy == "") {
            return "";
        }
        return " ORDER BY ".$this->sortBy." ".$this->sortDir." ";
    }
    
    function getLimits() {
        if($this->display == "all" || $this->display == "") {
            return "";
        }
        if($this->firstItem == "") {
            return " LIMIT ".$this->display;
        }
        return " LIMIT ".$this->firstItem.", ".$this->display; 
    }
    
    function run() {
        global $modx;    
        if($this->tpl == "") {
            return "";
        }
        $k == 0;
        $alternate = -1;
        $first = -1;
        $last = -1;
        $sql = $this->buildQuery();
        $res = $modx->db->query($sql);
        $cnt = $modx->db->getRecordCount($res);
        if($cnt == 0) {
            echo $this->noResult;
            return "";
        }
        $fields = $modx->db->getColumnNames($res);
        $fieldNames = Array();
        foreach($fields as $fld) {
            array_push($fieldNames, "[+".$fld."+]");
        }
        $tpl = $modx->getChunk($this->tpl);
        $tplAlt;
        if($this->tplAlt != "") {
            $tplAlt = $modx->getChunk($this->tplAlt);
            $alternate = 1;
        }
        if($this->tplFirst != "") {
            $tplFirst = $modx->getChunk($this->tplFirst);
            $first = 0;
        }
        if($this->tplLast != "") {
            $tplLast = $modx->getChunk($this->tplLast);
            $last = $cnt - 1;
        }
        $output = "";
        while($row = $modx->db->getRow($res)) {
            if($k == $first) {
                $output .= str_replace($fieldNames,$row,$tplFirst);
            }
            else if($k == $last) {
                $output .= str_replace($fieldNames,$row,$tplLast);
            }
            else if($k == $alternate) {
                $output .= str_replace($fieldNames,$row,$tplAlt);
                $alternate = $alternate + 2;
            }
            else {
                $output .= str_replace($fieldNames,$row,$tpl);
            }
            $k++;
        }
        echo $output;
    }
} 

?>