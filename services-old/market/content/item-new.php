<?php
require_once('../includes/header.php');

if (!isInSpamList($client_ip)){//no spammer
	require_once('../includes/classes/resize.php');
	if ($_POST){	
		if(cP("math") == $_SESSION["mathCaptcha"])	{
			if (isEmail(cP("email"))){//is email
				if(!isSpam(cP("name"),cP("email"),cP("description"))){//check if is spam!
					
					$image_check=check_images_form();//echo $image_check;
					
					if (is_numeric($image_check)){//if the images were right, or not any image uploaded
					
						if (is_numeric(cP("price"))) $price=cP("price");
						else $price=0;
						//DB insert
						$post_password=generatePassword();					
						if (HTML_EDITOR) $desc=cPR("description");
						else $desc=cP("description");
						if (VIDEO && cp("video")!="" && strpos(cp("video"), "http://www.youtube.com/watch?v=")==0) $desc.='[youtube='.cp("video").']';//youtube video
						$title=cP("title");
						$email=cP("email");
					
						$ocdb->insert(TABLE_PREFIX."posts (idCategory,type,title,description,price,place,name,email,phone,password,ip)","".
												cP("category").",".cP("type").",'$title','$desc',$price,'".cP("place")."','".cP("name")."','$email','".cP("phone")."','$post_password','$client_ip'");
						$idPost=$ocdb->getLastID();
						//end DB insert
						
						if ($image_check>1) upload_images_form($idPost,$title);
							
						//EMAIL notify
						//generate the email to send to the client , we allow them to erase posts? mmmm
						if(FRIENDLY_URL) {
						    $linkConfirm=SITE_URL."/manage/?post=$idPost&pwd=$post_password&action=confirm";
						    $linkDeactivate=SITE_URL."/manage/?post=$idPost&pwd=$post_password&action=deactivate";
						    $linkEdit=SITE_URL."/manage/?post=$idPost&pwd=$post_password&action=edit";
						}
						else{
						    $linkConfirm=SITE_URL."/content/item-manage.php?post=$idPost&pwd=$post_password&action=confirm";
						    $linkDeactivate=SITE_URL."/content/item-manage.php?post=$idPost&pwd=$post_password&action=deactivate";
						    $linkEdit=SITE_URL."/content/item-manage.php?post=$idPost&pwd=$post_password&action=edit";
						}
						
						$body=	T_NOTIFY_EMAIL_1.
								"<a href='$linkConfirm'>$linkConfirm</a><br /><br />".
								T_NOTIFY_EMAIL_3." 
								<a href='$linkEdit'>$linkEdit</a><br />".
								T_NOTIFY_EMAIL_2." 
								<a href='$linkDeactivate'>$linkDeactivate</a>";
						echo T_THANKS_EMAIL." ".sendEmail($email,T_CONFIRM." ".$title." ". $_SERVER['SERVER_NAME'],$body);
						
						//for preventing f5
						$_SESSION["mathCaptcha"]="";
						
						require_once('../includes/footer.php');
						exit;
					}
					else echo "<div id='sysmessage'>".$image_check."</div>";//end upload verification
				}//end akismet
				else {//is spam!
					echo "<div id='sysmessage'>".T_SPAM_CONTACT."</div>";
					require_once('../includes/footer.php');
					exit;
				}
			}//email validation
			else echo "<div id='sysmessage'>".T_WRONG_EMAIL."</div>";//wrong email address
		}//captcha validation
		else echo "<div id='sysmessage'>".T_WRONG_CAPTCHA."</div>";//wrong captcha
	}//if post
	
?>

	<h3><?php echo T_NEW_ITEM;?> <?php echo $categoryName;?></h3>
	
	<form action="" method="post" onsubmit="return checkForm(this);" enctype="multipart/form-data">
	
		<?php if (!isset($idCategory)){?>	
			<?php echo T_CATEGORY;?>:<br />
			<?php 
			$query="SELECT idCategory,name,(select name from ".TABLE_PREFIX."categories where idCategory=C.idCategoryParent) FROM ".TABLE_PREFIX."categories C order by idCategoryParent";
			sqlOptionGroup($query,"category",$idCategory);
			?>
			<br />
		<?php }
		else echo "<input type='hidden' name='category' value='$idCategory' />" ?>
		
		<?php echo T_TYPE;?>:<br />
		<select id="type" name="type">
			<option value="<?php echo TYPE_OFFER;?>"><?php echo TYPE_OFFER_NAME;?></option>
			<option value="<?php echo TYPE_NEED;?>"><?php echo TYPE_NEED_NAME;?></option>
		</select>
		<br />
		<br />
		
		<?php echo T_TITLE;?>*:<br />
		<input id="title" name="title" type="text" value="<?php echo $_POST["title"];?>" size="61" maxlength="120" onblur="validateText(this);"  lang="false" />
		<input id="price" name="price" type="text" size="3" value="<?php echo $_POST["price"];?>" maxlength="8"  onkeypress="return isNumberKey(event);"   /><?php echo CURRENCY;?><br />
		<br />
		
		<?php echo T_DESCRIPTION;?>*:<br />

		<?php if (HTML_EDITOR){?>
		    <script type="text/javascript">var SITE_URL="<?php echo SITE_URL;?>";</script>
			<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/js/nicEdit.js"></script>
			<script type="text/javascript">
			//<![CDATA[
				bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
				//]]>
			</script>
			<textarea rows="10" cols="79" name="description" id="description"><?php echo stripslashes($_POST['description']);?></textarea>
		<?php }
		    else{?>
			<textarea rows="10" cols="79" name="description" id="description" onblur="validateText(this);"  lang="false"><?php echo strip_tags($_POST['description']);?></textarea><?php }?>
		<br />
		
		<?php echo T_PLACE;?>:<br />
		
		<?php if (MAP_KEY==""){//not google maps?>
		<input id="place" name="place" type="text" value="<?php echo $_POST["place"];?>" size="69" maxlength="120" /><br />
		<?php }
		else{//google maps
			if ($_POST["place"]!="") $m_value=$_POST["place"];
			else $m_value=MAP_INI_POINT;
		?>
		<input id="place" name="place" type="text" value="<?php echo $m_value;?>" onblur="showAddress(this.value);" size="69" maxlength="120" /><br />
		<div id="map" style="width: 100%; height: 200px;"></div>
		<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php echo MAP_KEY;?>" type="text/javascript"></script>
		<script type="text/javascript">var init_street="<?php echo MAP_INI_POINT;?>";</script>
		<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/js/map.js"></script>
		<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/js/mapSmall.js"></script>
		<?php }?>
		
		<?php echo T_NAME;?>*:<br />
		<input id="name" name="name" type="text" value="<?php echo $_POST["name"];?>" maxlength="75" onblur="validateText(this);"  lang="false"  /><br />
		
		<?php echo T_ITEM_EMAIL;?>*:<br />
		<input id="email" name="email" type="text" value="<?php echo $_POST["email"];?>" maxlength="120" onblur="validateEmail(this);" lang="false"  /><br />
		
		<?php echo T_PHONE;?>:<br />
		<input id="phone" name="phone" type="text" value="<?php echo $_POST["phone"];?>" maxlength="11" /><br />
		
		<?php if (VIDEO){?>
	    	<span style="cursor:pointer;" onclick="youtubePrompt();">Youtube Video</span>: <br />
	    	<input id="video" name="video" type="text" value="<?php echo $_POST["video"];?>" onclick="youtubePrompt();" size="40" /><br />
	    	<div id="youtubeVideo"></div>
		<?php } ?>
		
		<?php 
		if (MAX_IMG_NUM>0){
			echo "<input type='hidden' name='MAX_FILE_SIZE' value='".MAX_IMG_SIZE."' />";
			echo "<br />".T_UPLOAD_MAX.": ".(MAX_IMG_SIZE/1000000)."Mb ".T_FORMAT." ".IMG_TYPES."<br />";
			for ($i=1;$i<=MAX_IMG_NUM;$i++){?>
				<label><?php echo T_PICTURE;?> <?php echo $i?>:</label><input type="file" name="pic<?php echo $i?>" id="pic<?php echo $i?>" value="<?php echo $_POST["pic".$i];?>" /><br />
		<?php 	}
		}
		?>
		
		<br />
		<?php  mathCaptcha();?>
		<input id="math" name="math" type="text" size="2" maxlength="2"  onblur="validateNumber(this);"  onkeypress="return isNumberKey(event);" lang="false" />
		<br />
		<br />
		<input type="submit" id="submit" value="<?php echo T_SEND;?>" />
		
	</form>


<?php
}
else {//is spammer
	alert(T_NO_SPAM);
	jsRedirect(SITE_URL);
}

require_once('../includes/footer.php');
?>
