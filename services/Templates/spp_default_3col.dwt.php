<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  dir="ltr" lang="en-EN">
<head>
<!-- TemplateBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="CSSScriptStyle" -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- TemplateEndEditable -->

<!-- TemplateBeginEditable name="head" --> 


<!-- TemplateEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width250">
<!-- TemplateBeginEditable name="content" -->
<h1 class="yellow">Col 1</h1>
<div class="contentBlock">

</div>
<!-- TemplateEndEditable -->
</div>
<!-- #END MAIN COLUMN -->
<!-- #BEGIN SECOND COLUMN -->
<div class="floatLeft width250">
<!-- TemplateBeginEditable name="Column2" -->
  <h1 class="yellow">Col 2</h1>
  <div class="contentBlock">
   
  </div>
<!-- TemplateEndEditable -->
</div><!-- #END SECOND COLUMN -->

<!-- #BEGIN THIRD COLUMN -->
<div class="floatRight width250">
<!-- TemplateBeginEditable name="Column3" -->
  <h1 class="yellow">Col 3</h1>
  <div class="contentBlock">
   
  </div>
<!-- TemplateEndEditable -->
</div><!-- #END THIRD COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
</html>
