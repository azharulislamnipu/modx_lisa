<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

header('Content-Type: text/html; charset=utf-8');
require 'includes'.DIRECTORY_SEPARATOR.'constants.php';
error_reporting(DEBUG ? E_ALL : E_ERROR);
require 'classes'.DIRECTORY_SEPARATOR.'errorhandler.php';
require 'classes'.DIRECTORY_SEPARATOR.'setup.php';
require 'classes'.DIRECTORY_SEPARATOR.'page.php';
require 'classes'.DIRECTORY_SEPARATOR.'errorpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'loginpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryfactory.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryset.php';
require 'classes'.DIRECTORY_SEPARATOR.'gallery.php';
require 'classes'.DIRECTORY_SEPARATOR.'image.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlcheck.php';
require 'classes'.DIRECTORY_SEPARATOR.'auth.php';
require 'includes'.DIRECTORY_SEPARATOR.'phpcompat.php';
require 'classes'.DIRECTORY_SEPARATOR.'htmlentitydecode.php';
require 'classes'.DIRECTORY_SEPARATOR.'rcopy.php';
require 'classes'.DIRECTORY_SEPARATOR.'pathparser.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlparser.php';
require 'classes'.DIRECTORY_SEPARATOR.'formfield.php';
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svimage.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svcustomizepage.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2gallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2image.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2customizepage.php';
}

if (file_exists('plugins'.DIRECTORY_SEPARATOR.'tiltviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvimage.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvcustomizepage.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'autoviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avimage.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avcustomizepage.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'postcardviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvimage.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvcustomizepage.php';
}
$setup = new Setup();
$errorHandler = & new ErrorHandler();
$auth = new Auth();
$setup->checkLogin($auth);
$gallerySet = new GallerySet();
$galleryFactory = new GalleryFactory();
switch (true)
{
  case isset($_POST['createNewGallery']) :
    $galleryType = $_POST['newGalleryType'];
    $galleryRef = $gallerySet->nextGalleryRef();
    $galleryPath = GALLERY_PREFIX.$galleryRef.DIRECTORY_SEPARATOR;
    $gallery = $galleryFactory->makeGallery($galleryType, $galleryRef, $galleryPath, true);
    $galleryIndex = $gallerySet->addGalleriesDataRecord($gallery, $galleryType);
    $_SESSION['galleryIndex'] = $galleryIndex;
    $gallerySet->setNewGalleryPref($galleryType);
    $gallerySet->saveGalleriesPrefs();
    $gallerySet->saveGalleriesData();
    break;
  case isset($_GET['clone']) :
    $parentIndex = $_GET['clone'];
    $parentGalleryPath = $gallerySet->getGalleryPath($parentIndex);
    $parentRef = $gallerySet->getGalleryRef($parentIndex);
    $parentType = $gallerySet->getGalleryType($parentIndex);
    $parentPath = $gallerySet->getGalleryPath($parentIndex);
    $parent = $galleryFactory->makeGallery($parentType, $parentRef, $parentPath, false); 
    $parentPreferences = $parent->getPreferences();
    $parentTitle = $parent->getGalleryTitle();
    unset($parent);
    $galleryType = $_GET['gallerytype'];
    $galleryRef = $gallerySet->nextGalleryRef();
    $galleryPath = GALLERY_PREFIX.$galleryRef.DIRECTORY_SEPARATOR;
    $gallery = $galleryFactory->makeGallery($galleryType, $galleryRef, $galleryPath, true);
    $gallery->setPreferences($parentPreferences);
    $gallery->setGalleryTitle(CLONE_TITLE_PREFIX.$parentTitle);
    $gallery->saveGallery();
    $galleryIndex = $gallerySet->addGalleriesDataRecord($gallery, $galleryType);
    $_SESSION['galleryIndex'] = $galleryIndex;
    $gallerySet->setNewGalleryPref($galleryType);
    $gallerySet->saveGalleriesPrefs();
    $gallerySet->saveGalleriesData();
    break;
  case isset($_GET['galleryindex']) :
    $galleryIndex = $_GET['galleryindex'];
    $_SESSION['galleryIndex'] = $galleryIndex;
    $galleryRef = $gallerySet->getGalleryRef($galleryIndex);
    $galleryType = $gallerySet->getGalleryType($galleryIndex);
    $galleryPath = $gallerySet->getGalleryPath($galleryIndex);
    $gallery = $galleryFactory->makeGallery($galleryType, $galleryRef, $galleryPath, false);
    break;
  case isset($_SESSION['galleryIndex']) :
    $galleryIndex = $_SESSION['galleryIndex'];
    $galleryRef = $gallerySet->getGalleryRef($galleryIndex);
    $galleryType = $gallerySet->getGalleryType($galleryIndex);
    $galleryPath = $gallerySet->getGalleryPath($galleryIndex);
    $gallery = $galleryFactory->makeGallery($galleryType, $galleryRef, $galleryPath, false);
    break;
  default :
    $setup->noCurrentGallery($galleryFactory, $errorHandler);
    exit;
}
if (isset($_POST['customizesubmitted']))
{
  $gallery->customize($_POST);
  if ($_POST['absurl'] != $gallery->getRootUrl())
  {
    $newPath = $gallery->moveGallery($_POST['absurl']);
    if ($newPath !== false)
    {
      $gallerySet->setGalleriesDataPath($galleryIndex, $newPath);
    }
  }
  // move first then update path scheme
  $gallery->setPathScheme();
  $gallery->saveGallery();
  $galleryTitle = $gallery->getGalleryTitle();
  $gallerySet->setGalleriesDataTitle($galleryIndex, $galleryTitle);
  $gallerySet->saveGalleriesData();
}
if ($gallery->checkPath())
{
  trigger_error('path from web root contains characters not recommended for a URL', E_USER_NOTICE);
}
$page = $galleryFactory->makeCustomizePage($galleryType);

print $page->getHtmlHead();
print $page->getPageHeader();
ob_start();
print $page->getButtonbar($gallery);
print $page->getCustomizeHtml($gallerySet, $gallery);
$mainOutput = ob_get_clean();
print $errorHandler->getMessages();
print $mainOutput;
print $page->getFooter();
?>