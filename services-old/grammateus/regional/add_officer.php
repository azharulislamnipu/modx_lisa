<?php
function enforce_protocol($p){
	if($p=="https"){
		if($_SERVER['HTTPS']==""){
			header("Location:https://".$_SERVER['HTTP_HOST'].":443".$_SERVER["PHP_SELF"].(($_SERVER["QUERY_STRING"]=="")?"":("?".$_SERVER["QUERY_STRING"])));
			}
		}
		else if($p=="http"){
			if($_SERVER['HTTPS']=="on"){
			header("Location:http://".$_SERVER['HTTP_HOST'].$_SERVER["PHP_SELF"].(($_SERVER["QUERY_STRING"]=="")?"":("?".$_SERVER["QUERY_STRING"])));
		}
	}
}
enforce_protocol("https");
 
?><?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("CHAPTERCD", true, "text", "", "", "", "");
$formValidation->addField("CUSTOMERCD", true, "numeric", "", "", "", "");
$formValidation->addField("CPOSITION", true, "text", "", "", "", "");
$formValidation->addField("JOINDATE", true, "date", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("{KT_defaultSender}");
  $emailObj->setTo("lisa.jeter@gmail.com");
  $emailObj->setCC("");
  $emailObj->setBCC("");
  $emailObj->setSubject("{CPOSITION} Added by the Grammateus of the {COOKIE.REGIONNAME} Region");
  //FromFile method
  $emailObj->setContentFile("add_sire.html");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsBoules = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_rsBoules = $_COOKIE['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoules = sprintf("SELECT * FROM spp_boule WHERE REGIONNAME = %s ORDER BY BOULENAME ASC", GetSQLValueString($colname_rsBoules, "text"));
$rsBoules = mysql_query($query_rsBoules, $sigma_modx) or die(mysql_error());
$row_rsBoules = mysql_fetch_assoc($rsBoules);
$totalRows_rsBoules = mysql_num_rows($rsBoules);

$colname_rsArchons = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_rsArchons = $_COOKIE['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchons = sprintf("SELECT * FROM vw_web_elig WHERE REGIONNAME = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsArchons, "text"));
$rsArchons = mysql_query($query_rsArchons, $sigma_modx) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysql_num_rows($rsArchons);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOffices = "SELECT * FROM spp_cposition WHERE CPOSITION NOT LIKE '%egio%' AND CPOSITION NOT LIKE '%rand%' ORDER BY CPOSITION";
$rsOffices = mysql_query($query_rsOffices, $sigma_modx) or die(mysql_error());
$row_rsOffices = mysql_fetch_assoc($rsOffices);
$totalRows_rsOffices = mysql_num_rows($rsOffices);

$colname_rsArchon = "-1";
if (isset($row_rsspp_officers['CUSTOMERCD'])) {
  $colname_rsArchon = $row_rsspp_officers['CUSTOMERCD'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchon = sprintf("SELECT * FROM spp_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname_rsArchon, "int"));
$rsArchon = mysql_query($query_rsArchon, $sigma_modx) or die(mysql_error());
$row_rsArchon = mysql_fetch_assoc($rsArchon);
$totalRows_rsArchon = mysql_num_rows($rsArchon);

$colname_rsOfficers = "-1";
if (isset($row_rsArchon['CHAPTERID'])) {
  $colname_rsOfficers = $row_rsArchon['CHAPTERID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOfficers = sprintf("SELECT * FROM vw_boule_officers WHERE CPOSITION = 'Grammateus' AND BOULENAME = %s", GetSQLValueString($colname_rsOfficers, "text"));
$rsOfficers = mysql_query($query_rsOfficers, $sigma_modx) or die(mysql_error());
$row_rsOfficers = mysql_fetch_assoc($rsOfficers);
$totalRows_rsOfficers = mysql_num_rows($rsOfficers);

// Make an insert transaction instance
$ins_spp_officers = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_officers);
// Register triggers
$ins_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "../../includes/nxt/back.php");
$ins_spp_officers->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_officers->setTable("spp_officers");
$ins_spp_officers->addColumn("CHAPTERCD", "STRING_TYPE", "POST", "CHAPTERCD");
$ins_spp_officers->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$ins_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "POST", "CPOSITION");
$ins_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$ins_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "VALUE", "{COOKIE.UPDATEDBY}");
$ins_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_officers->addColumn("REGIONCD", "STRING_TYPE", "VALUE", "{rsBoules.REGIONCD}");
$ins_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "VALUE", "Active");
$ins_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_officers = $tNGs->getRecordset("spp_officers");
$row_rsspp_officers = mysql_fetch_assoc($rsspp_officers);
$totalRows_rsspp_officers = mysql_num_rows($rsspp_officers);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/JSRecordset.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/DependentDropdown.js"></script>
<?php
//begin JSRecordset
$jsObject_rsArchons = new WDG_JsRecordset("rsArchons");
echo $jsObject_rsArchons->getOutput();
//end JSRecordset
?>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_regional_grammateus.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    ADD AN OFFICER</h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_officers > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <?php 
// Show IF Conditional show_CHAPTERCD_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
            <tr>
              <td class="KT_th"><label for="CHAPTERCD_<?php echo $cnt1; ?>">BOUL&#201;:</label></td>
              <td><select name="CHAPTERCD_<?php echo $cnt1; ?>" id="CHAPTERCD_<?php echo $cnt1; ?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                <?php 
do {  
?>
                <option value="<?php echo $row_rsBoules['CHAPTERID']?>"<?php if (!(strcmp($row_rsBoules['CHAPTERID'], $row_rsspp_officers['CHAPTERCD']))) {echo "SELECTED";} ?>><?php echo $row_rsBoules['BOULENAME']?></option>
                <?php
} while ($row_rsBoules = mysql_fetch_assoc($rsBoules));
  $rows = mysql_num_rows($rsBoules);
  if($rows > 0) {
      mysql_data_seek($rsBoules, 0);
	  $row_rsBoules = mysql_fetch_assoc($rsBoules);
  }
?>
              </select>
                  <?php echo $tNGs->displayFieldError("spp_officers", "CHAPTERCD", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_CHAPTERCD_on_insert_only
?>
          <?php 
// Show IF Conditional show_CUSTOMERCD_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
            <tr>
              <td class="KT_th"><label for="CUSTOMERCD_<?php echo $cnt1; ?>">ARCHON:</label></td>
              <td><select name="CUSTOMERCD_<?php echo $cnt1; ?>" id="CUSTOMERCD_<?php echo $cnt1; ?>" wdg:subtype="DependentDropdown" wdg:type="widget" wdg:recordset="rsArchons" wdg:displayfield="FULLNAME" wdg:valuefield="CUSTOMERID" wdg:fkey="CHAPTERID" wdg:triggerobject="CHAPTERCD_<?php echo $cnt1; ?>" wdg:selected="<?php echo $row_rsspp_officers['CHAPTERCD'] ?>">
                <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_officers", "CUSTOMERCD", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_CUSTOMERCD_on_insert_only
?>
          <?php 
// Show IF Conditional show_CPOSITION_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
            <tr>
              <td class="KT_th"><label for="CPOSITION_<?php echo $cnt1; ?>">OFFICE:</label></td>
              <td><select name="CPOSITION_<?php echo $cnt1; ?>" id="CPOSITION_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                  <?php 
do {  
?>
                  <option value="<?php echo $row_rsOffices['CPOSITION']?>"<?php if (!(strcmp($row_rsOffices['CPOSITION'], $row_rsspp_officers['CPOSITION']))) {echo "SELECTED";} ?>><?php echo $row_rsOffices['CPOSITION']?></option>
                  <?php
} while ($row_rsOffices = mysql_fetch_assoc($rsOffices));
  $rows = mysql_num_rows($rsOffices);
  if($rows > 0) {
      mysql_data_seek($rsOffices, 0);
	  $row_rsOffices = mysql_fetch_assoc($rsOffices);
  }
?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_officers", "CPOSITION", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_CPOSITION_on_insert_only
?>
          <?php 
// Show IF Conditional show_JOINDATE_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
            <tr>
              <td class="KT_th"><label for="JOINDATE_<?php echo $cnt1; ?>">START DATE:</label></td>
              <td><input name="JOINDATE_<?php echo $cnt1; ?>" id="JOINDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_officers['JOINDATE']); ?>" size="10" maxlength="22" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("JOINDATE");?> <?php echo $tNGs->displayFieldError("spp_officers", "JOINDATE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_JOINDATE_on_insert_only
?>
          <?php 
// Show IF Conditional show_UPDATEUSERCD_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
            <tr>
              <td class="KT_th">UPDATED BY:</td>
              <td><?php echo KT_escapeAttribute($row_rsspp_officers['UPDATEUSERCD']); ?></td>
            </tr>
            <?php } 
// endif Conditional show_UPDATEUSERCD_on_insert_only
?>
          <?php 
// Show IF Conditional show_UPDATETMS_on_insert_only 
if (@$_GET['OFFICERID'] == "") {
?>
            <tr>
              <td class="KT_th">UPDATETMS:</td>
              <td><?php echo KT_formatDate($row_rsspp_officers['UPDATETMS']); ?></td>
            </tr>
            <?php } 
// endif Conditional show_UPDATETMS_on_insert_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_officers_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_officers['kt_pk_spp_officers']); ?>" />
        <input type="hidden" name="REGIONCD_<?php echo $cnt1; ?>" id="REGIONCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsBoules['REGIONCD']); ?>" />
        <input type="hidden" name="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" id="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_officers['COMMITTEESTATUSSTT']); ?>" />
        <?php } while ($row_rsspp_officers = mysql_fetch_assoc($rsspp_officers)); ?>
      <div class="KT_bottombuttons">
        <div>
          <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsBoules);

mysql_free_result($rsArchons);

mysql_free_result($rsOffices);

mysql_free_result($rsArchon);

mysql_free_result($rsOfficers);
?>
