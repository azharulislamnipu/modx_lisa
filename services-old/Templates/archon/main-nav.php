        <nav role="navigation">
	<div class="width relative">
		<h2 class="visuallyhidden">Site Menu</h2>
		<ul class="jetmenu color-override-yellow-orange">

<li class="secondary home quicklinks" style="display:visible; color:white;"><a class="nodup" href="javascript:void(0);">Home</a><ul class="dropdown color-override-yellow-orange">
			<!-- outerTpl -->
<li><a href="/home/53rd-grand-boule.php" title="53rd Grand Boul&#233;" >53rd Grand Boul&#233;</a></li>
<li><a href="/home/archon-home.php" title="Archon Home" >Archon Home</a></li>
<li><a href="/home/boule-tv.php" title="Boul&#233; TV" >Boul&#233; TV</a></li>
<li style="display:none;"><a href="/home/boule-family-archousa.php" title="Boulé Family" >Boulé Family</a></li>
<li><a href="http://www.sigmapiphi.org/boules/" title="Colocated Sites" >Colocated Sites</a></li>
<li><a href="/home/basic-search-upd.php" title="Directory" >Search Directory</a></li>
<li><a href="/home/personal-profile.php" title="Manage Archon Profile" >Manage Archon Profile</a></li>
<li><a href="/home/member-boule-management.php" title="Officers" >Officers</a></li>
<li><a href="/home/boule-journal.php" title="The Boul&#233; Journal" >The Boul&#233; Journal</a></li>
<li><a href="/home/committees.php" title="The Grand Boulé" >The Grand Boulé</a></li>
<li class="last"><a href="/home/" title="Visitors" >Visitors</a></li>

</ul></li>

</ul>
		<ul class="jetmenu color-override-yellow-orange">



			<!-- outerTpl -->
<!-- innerRowTpl -->
<li ><a href="/home/archon-and-member-boule-management.php" title="Administration">Administration</a><ul class="dropdown color-override-yellow-orange">
<!-- innerRowTpl -->
<li><a href="/home/archon-services.php" title="Archons">Archons</a></li>
<!-- innerRowTpl -->
<li><a href="/home/personal-profile.php" title="Profile">Profile</a></li>
<!-- innerRowTpl -->
<li><a href="/home/member-boule-management.php" title="Officers">Officers</a></li>
<!-- innerRowTpl -->
<li><a href="/home/committees.php" title="Grand Boulé">Grand Boulé</a></li>

</ul></li>
<!-- innerRowTpl -->
<li ><a href="/home/initiatives.php" title="Initiatives">Initiatives</a><ul class="dropdown color-override-yellow-orange">
<!-- innerRowTpl -->
<li><a href="/home/boule-insurance-coverage.php" title="">Boulé Insurance Coverage</a></li>
<!-- innerRowTpl -->
<li><a href="/home/boule-scholars-program.php" title="">Boulé Scholars Program</a></li>
<!-- innerRowTpl -->
<li><a href="/home/my-brothers-keeper-resources.php" title="My Brother's Keeper">My Brother's Keeper</a></li>
<!-- innerRowTpl -->
<li><a href="/home/strategic-plan-pub.php" title="">Strategic Plan Archive</a></li>

</ul></li>
<!-- innerRowTpl -->
<li ><a href="/home/publications1.php" title="">Publications</a><ul class="dropdown color-override-yellow-orange">
<!-- innerRowTpl -->
<li><a href="/home/assets/files/docs/Constitution & Bylaws Manual (Full Version) As Of (08.29.2014).pdf" title="">Constitution & Bylaws</a></li>
<!-- innerRowTpl -->
<li><a href="/home/assets/files/protocol manual.2015.pdf" title="">The Protocol Manual</a></li>
<!-- innerRowTpl -->
<li><a href="/home/boule-insurance-coverage.php" title="">Boulé Insurance Coverage</a></li>
<!-- innerRowTpl -->
<li><a href="/home/membership-reports.php" title="">Membership Reports</a></li>

</ul></li>
<!-- innerRowTpl -->
<li ><a href="/home/communications.php" title="">Communications</a><ul class="dropdown color-override-yellow-orange">
<!-- innerRowTpl -->
<li><a href="/home/53rd-grand-boule.php" title="">53rd Grand Boulé</a></li>
<!-- innerRowTpl -->
<li><a href="/home/51st-grand-boule-activities.php" title="">51st Grand Boulé Archive</a></li>
<!-- innerRowTpl -->
<li><a href="/home/grand-grammateus-newsletters.php" title="">Grand Grammateus Newsletters</a></li>
<!-- innerRowTpl -->
<li><a href="/home/52nd-grand-boule-archive.php" title="">52nd Grand Boulé Archive</a></li>
<!-- innerRowTpl -->
<li><a href="/home/news_events.php" title="">News & Events</a></li>

</ul></li>
<!-- innerRowTpl -->
<li  class="last"><a href="/home/network-all.php" title="">Network</a><ul class="dropdown color-override-yellow-orange">
<!-- innerRowTpl -->
<li><a href="/home/network-all.php" title="Family">Family</a></li>
<!-- innerRowTpl -->
<li><a href="http://lexusboule.sigmapiphi.org/new/" title="Certificates">Certificates</a></li>
<!-- innerRowTpl -->
<li><a href="/home/directories.php" title="Directories">Directories</a></li>
<!-- innerRowTpl -->
<li><a href="/home/boule-tv.php" title="">Boul&#233; TV</a></li>
<!-- innerRowTpl -->
<li><a href="/home/the-regional-boules-index.php" title="Regions">Regions</a></li>
<!-- innerRowTpl -->
<li><a href="http://www.sigmapiphi.org/boules/" title="Colocated Sites">Colocated Sites</a></li>

</ul></li>

			<li class="searchtrigger"><a href="#" class="secondary">Search <i class="fa fa-search"></i></a><div class="headersearch"><form id="ajaxSearch_form" method="post" action="search-results-new.php">
		<fieldset>
			<input type="hidden" value="oneword" name="advsearch">
			<label>
			<input id="ajaxSearch_input" class="cleardefault" type="text" placeholder="Search" value="" name="search">
			</label>
			<label>
				<input id="ajaxSearch_submit" type="submit" value="Go!" name="sub">
			</label>
		</fieldset>
		</form>
		</div></li>
			
			<li style="display: visible;"><a href="http://www.sigmapiphi.org/home/login.php?webloginmode=lo" class="secondary button">LOGOUT</a></li>
		</ul>
	</div>
	
</nav>