<?php

require_once 'ClickatellMessageBean.php';
require_once 'ClickatellDatabaseManager.php';

/**
 * Contains service layer logic for the Clickatell screens 
 */
class ClickatellService {
    
    
    /** Retrieves distribution lists from the database */
    public function getDistributionLists($modx){
        
        $numbers = array();
        $database = new ClickatellDatabaseManager($modx);
        
        $boules = $database->getDistListBoules();
        if($boules != false){            
            $numbers[ClickatellDatabaseManager::DIST_LIST_BOULES] = $boules[ClickatellDatabaseManager::DIST_LIST_BOULES];
            $numbers[ClickatellDatabaseManager::DIST_LIST_REGIONS] = $boules[ClickatellDatabaseManager::DIST_LIST_REGIONS];
        }
        
        $officers = $database->getDistListOfficers();
        if($officers != false){
            $numbers[ClickatellDatabaseManager::DIST_LIST_OFFICERS] = $officers;
        }
        
        return $numbers;        
    }
        
    /** Uses UDH headers to join extended messages that have been split */
    public function joinExtendedMessages(&$messages){
            if(!empty($messages)){

                    // Look for extended messages
                    $count = count($messages);
                    foreach($messages as $key => $m){			
                            if($m->active_ind === '1'){
                                    $i = $key;
                                    $isMatch = true;
                                    while(++$i < $count && $isMatch){
                                            if($isMatch = $m->isUdhMatch($messages[$i])){
                                                    $m->addLinkedMessage($messages[$i]);
                                            }
                                    } 
                            }
                    }

                    // Remove messages that have been joined
                    foreach($messages as $key => $m){
                            if($m->active_ind === '0'){
                                    unset($messages[$key]);
                            }
                    }

                    // Sort the array by timestamp descending
                    usort($messages, array('ClickatellService', 'sortTimestampDesc'));			
            }
    }

    /* Sorts message by timestamp, descending */
    static function sortTimestampDesc($a, $b){
        return -1 * strcmp($a->timestamp, $b->timestamp);
    } 
    
}

?>
