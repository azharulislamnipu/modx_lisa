<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchons = "-1";
if (isset($_SESSION['WEB_ID'])) {
  $colname_rsArchons = (get_magic_quotes_gpc()) ? $_SESSION['WEB_ID'] : addslashes($_SESSION['WEB_ID']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchons = sprintf("SELECT * FROM vw_web_elig_no_officers WHERE WEB_ID = %s", GetSQLValueString($colname_rsArchons, "int"));
$rsArchons = mysql_query($query_rsArchons, $sigma_modx) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysql_num_rows($rsArchons);

$colname_rsEducation = "-1";
if (isset($_SESSION['WEB_ID'])) {
  $colname_rsEducation = (get_magic_quotes_gpc()) ? $_SESSION['WEB_ID'] : addslashes($_SESSION['WEB_ID']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsEducation = sprintf("SELECT * FROM vw_web_elig_educ WHERE WEB_ID = %s ORDER BY DEGREEYEAR ASC", GetSQLValueString($colname_rsEducation, "int"));
$rsEducation = mysql_query($query_rsEducation, $sigma_modx) or die(mysql_error());
$row_rsEducation = mysql_fetch_assoc($rsEducation);
$totalRows_rsEducation = mysql_num_rows($rsEducation);

$colname_rsOffices = "-1";
if (isset($_SESSION['WEB_ID'])) {
  $colname_rsOffices = (get_magic_quotes_gpc()) ? $_SESSION['WEB_ID'] : addslashes($_SESSION['WEB_ID']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOffices = sprintf("SELECT * FROM vw_web_elig_officers WHERE WEB_ID = %s ORDER BY START_DATE DESC", GetSQLValueString($colname_rsOffices, "int"));
$rsOffices = mysql_query($query_rsOffices, $sigma_modx) or die(mysql_error());
$row_rsOffices = mysql_fetch_assoc($rsOffices);
$totalRows_rsOffices = mysql_num_rows($rsOffices);

$colname_rsBoule = "-1";
if (isset($_SESSION['BOULENAME'])) {
  $colname_rsBoule = (get_magic_quotes_gpc()) ? $_SESSION['BOULENAME'] : addslashes($_SESSION['BOULENAME']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoule = sprintf("SELECT * FROM vw_web_elig_no_officers WHERE BOULENAME = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsBoule, "text"));
$rsBoule = mysql_query($query_rsBoule, $sigma_modx) or die(mysql_error());
$row_rsBoule = mysql_fetch_assoc($rsBoule);
$totalRows_rsBoule = mysql_num_rows($rsBoule);

$colname_rsJobs = "-1";
if (isset($_SESSION['webInternalKey'])) {
  $colname_rsJobs = (get_magic_quotes_gpc()) ? $_SESSION['webInternalKey'] : addslashes($_SESSION['webInternalKey']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsJobs = sprintf("SELECT spp_prof.PROFID,   spp_prof.TITLEID,   spp_skillref.DESCRIPTION AS JOBTITLE,   spp_prof.CUSTOMERID,   spp_prof.COMPANYNAME,   spp_prof.COMPANYCITY,   spp_prof.COMPANYSTCD,   spp_prof.DESCRIPTION,   spp_prof.COMPANYJOBSTART FROM spp_prof   INNER JOIN spp_skillref ON (spp_prof.TITLEID = spp_skillref.SKILLID) WHERE CUSTOMERID = %s ORDER BY COMPANYJOBSTART DESC", GetSQLValueString($colname_rsJobs, "int"));
$rsJobs = mysql_query($query_rsJobs, $sigma_modx) or die(mysql_error());
$row_rsJobs = mysql_fetch_assoc($rsJobs);
$totalRows_rsJobs = mysql_num_rows($rsJobs);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsSkills = "SELECT SKILLID, `DESCRIPTION` FROM spp_skillref ORDER BY `DESCRIPTION` ASC";
$rsSkills = mysql_query($query_rsSkills, $sigma_modx) or die(mysql_error());
$row_rsSkills = mysql_fetch_assoc($rsSkills);
$totalRows_rsSkills = mysql_num_rows($rsSkills);

$colname_rsArchonOcc = "-1";
if (isset($_SESSION['webInternalKey'])) {
  $colname_rsArchonOcc = (get_magic_quotes_gpc()) ? $_SESSION['webInternalKey'] : addslashes($_SESSION['webInternalKey']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchonOcc = sprintf("SELECT spp_prof.OCCUPATIONID,   spp_occupation.DESCRIPTION,   spp_prof.CUSTOMERID FROM spp_prof   INNER JOIN spp_occupation ON (spp_prof.OCCUPATIONID = spp_occupation.OCCUPATIONID) WHERE spp_prof.CUSTOMERID = %s GROUP BY OCCUPATIONID", GetSQLValueString($colname_rsArchonOcc, "int"));
$rsArchonOcc = mysql_query($query_rsArchonOcc, $sigma_modx) or die(mysql_error());
$row_rsArchonOcc = mysql_fetch_assoc($rsArchonOcc);
$totalRows_rsArchonOcc = mysql_num_rows($rsArchonOcc);

$colname_rsArchonTitles = "-1";
if (isset($_SESSION['webInternalKey'])) {
  $colname_rsArchonTitles = (get_magic_quotes_gpc()) ? $_SESSION['webInternalKey'] : addslashes($_SESSION['webInternalKey']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchonTitles = sprintf("SELECT spp_prof.CUSTOMERID,   spp_prof.TITLEID,   spp_skillref.DESCRIPTION FROM spp_prof   INNER JOIN spp_skillref ON (spp_prof.TITLEID = spp_skillref.SKILLID) WHERE spp_prof.CUSTOMERID = %s GROUP BY TITLEID", GetSQLValueString($colname_rsArchonTitles, "int"));
$rsArchonTitles = mysql_query($query_rsArchonTitles, $sigma_modx) or die(mysql_error());
$row_rsArchonTitles = mysql_fetch_assoc($rsArchonTitles);
$totalRows_rsArchonTitles = mysql_num_rows($rsArchonTitles);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="../Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>>Sigma Pi Phi Fraternity | Archon Profile</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" -->
<div class="floatLeft width460">
<h1 class="yellow">Contact Information</h1>
<div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td valign="top">
  <?php 
// Show IF Conditional region10 
if (@$row_rsArchons['IMAGE'] != NULL) {
?>
	    <img src="/home/assets/images/archons/<?php echo $row_rsArchons['IMAGE']; ?>" width="190"/>
	    <?php 
// else Conditional region10
} else { ?>
	    <img src="../includes/tng/styles/img_not_found.gif" alt="Add your image." width="100" height="100" />
  <?php } 
// endif Conditional region10
?>
	
	
<script language="JavaScript">
    function image_edit(){
        document.forms.image.submit();
    }
</script>
  <form name="image" action="/services/archon/profile_image_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="image_edit();return(FALSE);">Edit Image</a></span>

	
	</td>
    <td><p><span class="larger"><?php echo $row_rsArchons['L2R_FULLNAME']; ?></span><br />
          <?php echo $row_rsArchons['ADDRESS1']; ?><br />
          <?php echo $row_rsArchons['CITY']; ?>, <?php echo $row_rsArchons['STATECD']; ?> <?php echo $row_rsArchons['ZIP']; ?> </p>
      <p>Home Phone: <?php echo $row_rsArchons['HOMEPHONE']; ?><br />
        Email: <?php echo $row_rsArchons['EMAIL']; ?><br />
      </p>
	  </td>
  </tr>
</table>
</div>
<div class="contentFooter">
<script language="JavaScript">
    function contact_edit(){
        document.forms.contact.submit();
    }
</script>
  <form name="contact" action="/services/archon/profile_contact_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="contact_edit();return(FALSE);">Edit Contact Information</a></span>  
</div>


<h1 class="yellow">Education</h1>
<div class="contentBlock">
<?php 
// Show IF Conditional region11 
if (@$row_rsEducation['DEGREE'] != NULL) {
?>
<table border="0" cellspacing="4" cellpadding="2">
  <tr valign="top">
    <td><table border="0" cellspacing="4" cellpadding="2">
      <tr valign="top">
        <td>YEAR</td>
        <td>DEGREE</td>
        <td>INSTITUTION</td>
        <td>CONCENTRATION</td>
        <td>&nbsp;</td>
      </tr>
      <?php do { ?>
        <tr valign="top">
          <td><?php echo $row_rsEducation['DEGREEYEAR']; ?></td>
          <td><?php echo $row_rsEducation['DEGREE']; ?></td>
          <td><?php echo $row_rsEducation['INSTITUTION']; ?></td>
          <td><?php echo $row_rsEducation['MAJOR']; ?>
                <?php 
// Show IF Conditional region1 
if (@$row_rsEducation['MINOR1'] != NULL) {
?>
                  <br />
                  <?php echo $row_rsEducation['MINOR1']; ?><br />
                  <?php echo $row_rsEducation['MINOR2']; ?>
                  <?php } 
// endif Conditional region1
?></td>
          <td>
<script language="JavaScript">
    function ed_edit(){
        document.forms.education_edit.submit();
	}
    function ed_del(){
        document.forms.education_delete.submit();
    }
</script>
  <form name="education_edit" action="/services/archon/profile_education_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="DEGREEID" value="<?php echo KT_escapeAttribute($row_rsEducation['DEGREEID']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
  <form name="education_delete" action="/services/archon/profile_education_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="DEGREEID" value="<?php echo KT_escapeAttribute($row_rsEducation['DEGREEID']); ?>">
  <input type="hidden" name="KT_Delete1" value="KT_Delete1">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="ed_edit();return(FALSE);">Edit</a>&nbsp; <a href="#" onclick="ed_del();return(FALSE)">Delete</a></span>		  </td>
        </tr>
        <?php 
// Show IF Conditional region2 
if (@$row_rsEducation['COMMENT'] != NULL) {
?>
          <tr valign="top">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="3"><?php echo $row_rsEducation['COMMENT']; ?></td>
          </tr>
          <?php } 
// endif Conditional region2
?>
        <?php } while ($row_rsEducation = mysql_fetch_assoc($rsEducation)); ?>
    </table></td>
    </tr>
  <tr valign="top">
  <td>  </td>
  </table>
  <?php 
// else Conditional region11
} else { ?>
Please add information about your education.
  <?php } 
// endif Conditional region11
?>

</div>
<div class="contentFooter">
<script language="JavaScript">
    function ed_add(){
        document.forms.education.submit();
    }
</script>
  <form name="education" action="/services/archon/profile_education_add.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="ed_add();return(FALSE);">Add Degree </a></span>  
</div>
<h1 class="yellow">Employment</h1>
<div class="contentBlock">
    <table width="100%" border="0" cellpadding="4" cellspacing="4">
      <tr valign="top">
        <td colspan="2" valign="top" style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">Current Position </td>
      </tr>
      <tr valign="top">
        <td valign="top">
  <?php 
// Show IF Conditional region9 
if (@$row_rsArchons['JOBTITLE'] != NULL || @$row_rsArchons['JOBTITLE'] != NULL ) {
?>
		<?php echo $row_rsArchons['JOBTITLE']; ?><br />
          <?php echo $row_rsArchons['ORGNAME']; ?><br />
          <?php echo $row_rsArchons['ALTADDRESS1']; ?> <?php echo $row_rsArchons['ALTADDRESS2']; ?><br />
          <?php echo $row_rsArchons['ALTCITY']; ?>, <?php echo $row_rsArchons['ALTSTATE']; ?> <?php echo $row_rsArchons['ALTZIP']; ?>
		  
<?php 
// else Conditional region9
} else { ?>
   Add your current position by clicking the "Edit Current Position" link.
  <?php } 
// endif Conditional region9
?>
</td>
        <td>
          <script language="JavaScript">
    function job_edit(){
        document.forms.job.submit();
    }
</script>
          <form name="job" action="/services/archon/profile_job_edit.php" method="post">
            <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
            <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
            <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
          </form>
    <span class="pub_date alignRight"><a href="#" onclick="job_edit();return(FALSE);">Edit Current Position</a></span>	</td>
      </tr>
    </table>
		
    <table width="100%" border="0" cellpadding="5" cellspacing="5">
          <tr valign="top">
            <td colspan="2" style="border-bottom: #CCCCCC 1px solid; text-transform:uppercase;">Prior Positions </td>
          </tr>
  <?php if ($totalRows_rsJobs > 0) { // Show if recordset not empty ?>
      <?php do { ?>
          <tr valign="top">
          <td style="border-bottom: #CCCCCC 1px solid;"><?php echo $row_rsJobs['JOBTITLE']; ?><br />
              <?php echo $row_rsJobs['COMPANYNAME']; ?><br />
              <?php echo $row_rsJobs['COMPANYCITY']; ?>, <?php echo $row_rsJobs['COMPANYSTCD']; ?><br />
            <?php echo $row_rsJobs['DESCRIPTION']; ?> </td>
			
          <td>
		  <script language="JavaScript">
    function career1(){
        document.forms.career_edit.submit();
    }
	function career_del(){
		document.forms.career_delete.submit();
	}
      </script>
              <form name="career_edit" action="/services/archon/profile_career_edit.php" method="post">
                <input type="hidden" name="PROFID" value="<?php echo KT_escapeAttribute($row_rsJobs['PROFID']); ?>">
                <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
                <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
                <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
              </form>
              <form name="career_delete" action="/services/archon/profile_career_edit.php" method="post">
                <input type="hidden" name="PROFID" value="<?php echo KT_escapeAttribute($row_rsJobs['PROFID']); ?>">
				<input type="hidden" name="KT_Delete1" value="KT_Delete1">
                <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
                <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
                <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
              </form>
          <span class="pub_date alignRight"><a href="#" onclick="career1();return(FALSE);">Edit </a> &nbsp;<a href="#" onclick="career_del();return(FALSE);">Delete</a></span></td>
        </tr>
        <?php } while ($row_rsJobs = mysql_fetch_assoc($rsJobs)); ?>
    <?php } // Show if recordset not empty ?>
	<?php if ($totalRows_rsJobs == 0) { // Show if recordset empty ?>
          <tr valign="top">
            <td colspan="2">Add prior positions and enhance your profile by clicking the &quot;Add New&quot; link below.</td>
          </tr>
    <?php } // Show if recordset empty ?>
    </table>

  </div>
<div class="contentFooter">
<script language="JavaScript">
    function career2(){
        document.forms.career_add.submit();
    }
</script>
  <form name="career_add" action="/services/archon/profile_career_add.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="career2();return(FALSE);">Add New</a></span>   
</div>

<h1 class="yellow">Biography</h1>
<div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
      <?php 
// Show IF Conditional region7 
if (@$row_rsArchons['BIO'] != NULL) {
?>
        <tr>
          <td><?php echo nl2br($row_rsArchons['BIO']); ?></td>
        </tr>
        <?php 
// else Conditional region7
} else { ?>
Enhance your profile by adding a biographical statement.
<?php } 
// endif Conditional region7
?>
    </table>
</div>
<div class="contentFooter">
  <script language="JavaScript">
    function bio_edit(){
        document.forms.bio.submit();
    }
  </script>
  <form name="bio" action="/services/archon/profile_bio_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="bio_edit();return(FALSE);">Edit</a></span>   
</div>
</div>



<div class="floatRight width305">
  <h1 class="yellow">Boul&#233; Information</h1>
  <div class="contentBlock">
<table border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td colspan="2"><?php echo $row_rsArchons['BOULENAME']; ?> Boul&eacute;<br />
        <?php echo $row_rsArchons['BOULECITY']; ?>, <?php echo $row_rsArchons['BOULESTATECD']; ?><br />
        <?php echo $row_rsArchons['REGIONNAME']; ?> Region </td>
  </tr>
  <?php 
// Show IF Conditional region5 
if (@$row_rsArchons['JOINDATE'] != NULL) {
?>
    <tr>
      <td valign="top">JOIN DATE </td>
      <td><?php echo KT_formatDate($row_rsArchons['JOINDATE']); ?></td>
    </tr>
    <?php } 
// endif Conditional region5
?>
  <tr>
    <?php if ($totalRows_rsOffices > 0) { // Show if recordset not empty ?>
      <td valign="top">OFFICES HELD </td>
      <td><?php do { ?>
          <div class="list"><?php echo $row_rsOffices['CPOSITION']; ?>, <?php echo $row_rsOffices['START_DATE']; ?></div>
          <?php } while ($row_rsOffices = mysql_fetch_assoc($rsOffices)); ?></td>
      <?php } // Show if recordset not empty ?></tr>
</table>
</div>
<div class="contentFooter">
<script language="JavaScript">
    function join(){
        document.forms.join_edit.submit();
    }
</script>
  <form name="join_edit" action="/services/archon/profile_join_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="join();return(FALSE);">Edit Join Date</a></span>   
</div>

  <h1 class="yellow">Personal Information </h1>
  <div class="contentBlock">
    <?php 
// Show IF Conditional region6 
if (@$row_rsArchons['SPOUSENAME'] != NULL) {
?>
      <table border="0" cellspacing="5" cellpadding="5">
        <tr>
          <td valign="top">BIRTH DATE:</td>
          <td><?php 
// Show IF Conditional region7 
if (@$row_rsArchons['BIRTHDATE'] != NULL) {
?>
              <?php echo KT_formatDate($row_rsArchons['BIRTHDATE']); ?>
              <?php 
// else Conditional region7
} else { ?>
Click the edit link below to add your birth date.
<?php } 
// endif Conditional region7
?></td>
        </tr>
        <tr>
          <td valign="top">ARCHOUSA</td>
          <td><?php 
// Show IF Conditional region9 
if (@$row_rsArchons['SPOUSENAME'] != NULL) {
?>
              <?php echo $row_rsArchons['SPOUSENAME']; ?>
              <?php 
// else Conditional region9
} else { ?>
              Click the edit link below to add your Archousa's name.
  <?php } 
// endif Conditional region9
?></td>
        </tr>
      </table>
      
      <?php } 
// endif Conditional region6
?>
</div>
<div class="contentFooter">
<script language="JavaScript">
    function personal(){
        document.forms.personal_edit.submit();
    }
</script>
  <form name="personal_edit" action="/services/archon/profile_personal_edit.php" method="post">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webFullname']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_SESSION['BOULENAME']); ?>">
  </form>
<span class="pub_date alignRight"><a href="#" onclick="personal();return(FALSE);">Edit</a></span>   
</div>
  <h1 class="yellow">Professional Career </h1>
<div class="contentBlock">
    <table border="0" cellspacing="5" cellpadding="5">
<?php if ($totalRows_rsArchonOcc == 0) { // Show if recordset empty ?>
      <tr>
        <td valign="top" colspan="2">Adding prior employment information populates your professional career window. </td>
      </tr>
<?php } // Show if recordset empty ?>	  
          <?php if ($totalRows_rsArchonOcc > 0) { // Show if recordset not empty ?>
      <tr>
            <td valign="top">OCCUPATION(S): </td>
          <td valign="top"><?php do { ?>
                    <?php echo $row_rsArchonOcc['DESCRIPTION']; ?><br />
          <?php } while ($row_rsArchonOcc = mysql_fetch_assoc($rsArchonOcc)); ?></td>
      </tr>
          <?php } // Show if recordset not empty ?>
          <?php if ($totalRows_rsArchonTitles > 0) { // Show if recordset not empty ?>
      <tr>
            <td valign="top">AREA(S) OF EXPERTISE:</td>
          <td valign="top"><?php do { ?>
                  <?php echo $row_rsArchonTitles['DESCRIPTION']; ?><br />
          <?php } while ($row_rsArchonTitles = mysql_fetch_assoc($rsArchonTitles)); ?></td>
      </tr>
          <?php } // Show if recordset not empty ?>
    </table>   
</div>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchons);

mysql_free_result($rsEducation);

mysql_free_result($rsOffices);

mysql_free_result($rsBoule);

mysql_free_result($rsJobs);

mysql_free_result($rsSkills);

mysql_free_result($rsArchonOcc);

mysql_free_result($rsArchonTitles);
?>
