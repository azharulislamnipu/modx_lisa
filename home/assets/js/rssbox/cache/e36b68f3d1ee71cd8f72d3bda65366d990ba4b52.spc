a:4:{s:8:"feedinfo";a:3:{s:4:"type";s:3:"RSS";s:7:"version";s:3:"2.0";s:8:"encoding";s:5:"UTF-8";}s:4:"info";a:3:{s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:43:"http://www.sigmapiphi.org/feeds/out/?user=1";}}s:5:"title";s:40:"Items of Interest for Fraternity Members";s:11:"description";s:40:"Items of Interest for Fraternity Members";}s:5:"items";a:50:{i:0;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:14:"Guilt by Birth";s:11:"description";s:523:"If Obama's response to terrorism is to target the innocent to get at the guilty, then our open society is diminished.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/15/opinion/15iht-edsokol.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By RONALD P. SOKOL</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:87:"http://www.nytimes.com/2010/01/15/opinion/15iht-edsokol.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:87:"http://www.nytimes.com/2010/01/15/opinion/15iht-edsokol.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263475804;s:7:"dc:date";i:1263475804;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:18:"By RONALD P. SOKOL";}}}}i:1;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:28:"Taxing Banks for the Bailout";s:11:"description";s:749:"<a href="http://www.nytimes.com/2010/01/15/us/15tax.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/15/business/15tax_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>President Obama said he wanted the tax on big financial institutions to “recover every single dime” for taxpayers.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/15/us/15tax.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JACKIE CALMES</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:74:"http://www.nytimes.com/2010/01/15/us/15tax.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:74:"http://www.nytimes.com/2010/01/15/us/15tax.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263472203;s:7:"dc:date";i:1263472203;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"By JACKIE CALMES";}}}}i:2;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:5:"Haiti";s:11:"description";s:475:"Washington must make sure that the upswelling of generosity to Haiti turns into sustained action  first to rescue, then to rebuild.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/opinion/14thu1.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/14/opinion/14thu1.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/14/opinion/14thu1.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263450603;s:7:"dc:date";i:1263450603;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:3;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Texas Shuts Door on Millions in Education Grants";s:11:"description";s:565:"Texas has opted out of the competition for a $700 million Race to the Top federal education grant, calling the program an intrusion on states’ control over education.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/education/14texas.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By SAM DILLON</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:83:"http://www.nytimes.com/2010/01/14/education/14texas.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:83:"http://www.nytimes.com/2010/01/14/education/14texas.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263447003;s:7:"dc:date";i:1263447003;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:13:"By SAM DILLON";}}}}i:4;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:75:"New Official Named With Portfolio to Unite Agencies and Improve Food Safety";s:11:"description";s:816:"<a href="http://www.nytimes.com/2010/01/14/health/policy/14fda.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/14/us/14fda_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>The White House appointed Michael R. Taylor to oversee the Food and Drug Administration’s nutrition programs and address the nation’s fractured food safety system.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/health/policy/14fda.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By GARDINER HARRIS</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:85:"http://www.nytimes.com/2010/01/14/health/policy/14fda.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:85:"http://www.nytimes.com/2010/01/14/health/policy/14fda.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263447003;s:7:"dc:date";i:1263447003;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:18:"By GARDINER HARRIS";}}}}i:5;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:42:"U.S. Mobilizes to Send Assistance to Haiti";s:11:"description";s:554:"President Obama, facing the first large-scale humanitarian crisis of his presidency, moved quickly after the earthquake to send help.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/world/americas/14prexy.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By HELENE COOPER and LIZ ROBBINS</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:88:"http://www.nytimes.com/2010/01/14/world/americas/14prexy.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:88:"http://www.nytimes.com/2010/01/14/world/americas/14prexy.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263436204;s:7:"dc:date";i:1263436204;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:32:"By HELENE COOPER and LIZ ROBBINS";}}}}i:6;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:59:"After a Year of Learning, the First Lady Seeks Out a Legacy";s:11:"description";s:782:"<a href="http://www.nytimes.com/2010/01/14/us/14michelle.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/14/us/14michelle_CA1/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>With her family settled in, Michelle Obama is now looking to influence policy by spearheading an initiative to reduce childhood obesity.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/us/14michelle.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By SHERYL GAY STOLBERG</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:79:"http://www.nytimes.com/2010/01/14/us/14michelle.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:79:"http://www.nytimes.com/2010/01/14/us/14michelle.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263436204;s:7:"dc:date";i:1263436204;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:22:"By SHERYL GAY STOLBERG";}}}}i:7;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:46:"Obama and Lawmakers Seek Accord on Health Care";s:11:"description";s:533:"President Obama and top Democrats met to resolve differences between the House and the Senate legislation.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/health/policy/14health.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By ROBERT PEAR and SHERYL GAY STOLBERG</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:88:"http://www.nytimes.com/2010/01/14/health/policy/14health.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:88:"http://www.nytimes.com/2010/01/14/health/policy/14health.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263436204;s:7:"dc:date";i:1263436204;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:38:"By ROBERT PEAR and SHERYL GAY STOLBERG";}}}}i:8;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:57:"Administration Loosens Purse Strings for Transit Projects";s:11:"description";s:756:"<a href="http://www.nytimes.com/2010/01/14/us/14streetcar.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/14/us/14streetcar/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>New guidelines will make it easier for cities and states to spend federal money on transit projects like light rail.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/us/14streetcar.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By MICHAEL COOPER</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/14/us/14streetcar.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/14/us/14streetcar.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263432602;s:7:"dc:date";i:1263432602;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:17:"By MICHAEL COOPER";}}}}i:9;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:55:"In Quake Aftermath, U.S. Suspends Deportations to Haiti";s:11:"description";s:535:"Responding to devastation from the earthquake, the Obama administration temporarily suspended deportations of illegal immigrants.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/world/americas/14deport.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JULIA PRESTON</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:89:"http://www.nytimes.com/2010/01/14/world/americas/14deport.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:89:"http://www.nytimes.com/2010/01/14/world/americas/14deport.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263425403;s:7:"dc:date";i:1263425403;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"By JULIA PRESTON";}}}}i:10;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:35:"Text of President Obama’s Remarks";s:11:"description";s:489:"Here are President Obama’s remarks Wednesday morning on the earthquake in Haiti and rescue efforts, as released by the White House.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/world/americas/14obamatext.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:92:"http://www.nytimes.com/2010/01/14/world/americas/14obamatext.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:92:"http://www.nytimes.com/2010/01/14/world/americas/14obamatext.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263407403;s:7:"dc:date";i:1263407403;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:11;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:43:"Europe&#039;s Post-Copenhagen View of Obama";s:11:"description";s:476:"On climate change, President Obama proved to be the biggest disappointment.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/opinion/14iht-edhill.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By STEVEN HILL</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:86:"http://www.nytimes.com/2010/01/14/opinion/14iht-edhill.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:86:"http://www.nytimes.com/2010/01/14/opinion/14iht-edhill.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263389403;s:7:"dc:date";i:1263389403;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"By STEVEN HILL";}}}}i:12;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:39:"Administration Says Stimulus Has Worked";s:11:"description";s:558:"In its latest progress report, the White House said both the overall economy and employment are better than they would have been without the stimulus.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/14/business/economy/14budget.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JACKIE CALMES</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:91:"http://www.nytimes.com/2010/01/14/business/economy/14budget.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:91:"http://www.nytimes.com/2010/01/14/business/economy/14budget.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263367802;s:7:"dc:date";i:1263367802;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"By JACKIE CALMES";}}}}i:13;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:50:"Obama to Propose Bank Tax to Recoup Bailout Losses";s:11:"description";s:535:"The president is expected to announce the bank fee on Thursday. It is intended to be temporary, but would probably last for several years.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/13/business/13tax.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JACKIE CALMES</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/13/business/13tax.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/13/business/13tax.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263360604;s:7:"dc:date";i:1263360604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"By JACKIE CALMES";}}}}i:14;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:52:"Talks to Begin on Creating a Bipartisan Budget Panel";s:11:"description";s:527:"An idea that has long languished in Congress could become central to the Obama administration’s pledge to reduce deficits.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/13/us/politics/13fiscal.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JACKIE CALMES</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:86:"http://www.nytimes.com/2010/01/13/us/politics/13fiscal.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:86:"http://www.nytimes.com/2010/01/13/us/politics/13fiscal.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263357004;s:7:"dc:date";i:1263357004;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"By JACKIE CALMES";}}}}i:15;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:45:"Wall St. Pay Is a Focus of Many in Washington";s:11:"description";s:792:"<a href="http://www.nytimes.com/2010/01/13/business/13bank.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/13/business/13bank_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>Tilting at Wall Street is a big game in Washington, one that will grow as federal hearings into the causes of the financial crisis begin on Wednesday.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/13/business/13bank.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By ERIC DASH</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:81:"http://www.nytimes.com/2010/01/13/business/13bank.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:81:"http://www.nytimes.com/2010/01/13/business/13bank.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263357004;s:7:"dc:date";i:1263357004;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"By ERIC DASH";}}}}i:16;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:49:"Lack of Medicare Chief Is a Strike Against Reform";s:11:"description";s:528:"President Obama has not chosen anyone to lead Medicare, but the chief would oversee big parts of the health reform.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/13/business/economy/13leonhardt.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By DAVID LEONHARDT</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:94:"http://www.nytimes.com/2010/01/13/business/economy/13leonhardt.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:94:"http://www.nytimes.com/2010/01/13/business/economy/13leonhardt.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263349803;s:7:"dc:date";i:1263349803;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:18:"By DAVID LEONHARDT";}}}}i:17;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Garment Company Removes Ad Showing Obama in Coat";s:11:"description";s:799:"<a href="http://www.nytimes.com/2010/01/12/business/media/12billboard.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/07/business/07garment_337-span/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>A billboard in Times Square featuring the president in a Weatherproof coat created controversy and will be taken down.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/12/business/media/12billboard.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By STEPHANIE CLIFFORD</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:92:"http://www.nytimes.com/2010/01/12/business/media/12billboard.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:92:"http://www.nytimes.com/2010/01/12/business/media/12billboard.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263270604;s:7:"dc:date";i:1263270604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:21:"By STEPHANIE CLIFFORD";}}}}i:18;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:47:"Low Profile for Chrysler After a Year of Change";s:11:"description";s:816:"<a href="http://www.nytimes.com/2010/01/12/automobiles/autoshow/12auto.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/12/business/12auto_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>Sergio Marchionne, the carmaker’s chief executive, said that the company had to endure a fallow period before it could refresh its vehicle lineup.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/12/automobiles/autoshow/12auto.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By BILL VLASIC</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:93:"http://www.nytimes.com/2010/01/12/automobiles/autoshow/12auto.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:93:"http://www.nytimes.com/2010/01/12/automobiles/autoshow/12auto.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263270604;s:7:"dc:date";i:1263270604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"By BILL VLASIC";}}}}i:19;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:30:"A Future That Doesn’t Guzzle";s:11:"description";s:872:"<a href="http://www.nytimes.com/2010/01/12/automobiles/autoshow/12electric.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/12/business/12electric_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>Automakers unveiled many hybrid gas-electric and battery-powered cars at the Detroit auto show, a sign of how much emphasis is being placed on the electrification of vehicles.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/12/automobiles/autoshow/12electric.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By BILL VLASIC and NICK BUNKLEY</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.nytimes.com/2010/01/12/automobiles/autoshow/12electric.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:97:"http://www.nytimes.com/2010/01/12/automobiles/autoshow/12electric.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263267002;s:7:"dc:date";i:1263267002;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:31:"By BILL VLASIC and NICK BUNKLEY";}}}}i:20;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"President Signals Flexibility on Health Plan Tax";s:11:"description";s:568:"President Obama told union leaders that he was committed to taxing high-cost insurance policies, but was willing to amend the proposal.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/12/health/policy/12health.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By SHERYL GAY STOLBERG and STEVEN GREENHOUSE</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:88:"http://www.nytimes.com/2010/01/12/health/policy/12health.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:88:"http://www.nytimes.com/2010/01/12/health/policy/12health.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263263404;s:7:"dc:date";i:1263263404;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:44:"By SHERYL GAY STOLBERG and STEVEN GREENHOUSE";}}}}i:21;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:43:"White House Opposes Challenge to Gillibrand";s:11:"description";s:533:"The White House backed Senator Kirsten E. Gillibrand, but Harold E. Ford Jr. showed no sign of being deterred.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/12/nyregion/12ford.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By RAYMOND HERNANDEZ and JEREMY W. PETERS</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:81:"http://www.nytimes.com/2010/01/12/nyregion/12ford.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:81:"http://www.nytimes.com/2010/01/12/nyregion/12ford.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263263404;s:7:"dc:date";i:1263263404;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:41:"By RAYMOND HERNANDEZ and JEREMY W. PETERS";}}}}i:22;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:29:"Privatized War, and Its Price";s:11:"description";s:480:"President Obama must keep his promise to get rid of the thousands of freelance gunmen still deployed in Iraq, Afghanistan and elsewhere.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/11/opinion/11mon1.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/11/opinion/11mon1.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/11/opinion/11mon1.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263191402;s:7:"dc:date";i:1263191402;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:23;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:24:"Action, and Now Reaction";s:11:"description";s:525:"The founders built the House of Representatives to act. And throughout President Obama’s first year, the House has acted.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/11/us/politics/11caucus.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JOHN HARWOOD</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:86:"http://www.nytimes.com/2010/01/11/us/politics/11caucus.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:86:"http://www.nytimes.com/2010/01/11/us/politics/11caucus.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263184202;s:7:"dc:date";i:1263184202;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"By JOHN HARWOOD";}}}}i:24;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:33:"G.O.P. Attacks Reid Over Comments";s:11:"description";s:818:"<a href="http://www.nytimes.com/2010/01/11/us/politics/11reid.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/11/us/11reid_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>Republicans sought to compare racially charged remarks attributed to Harry Reid as similar to those made in 2002 by Trent Lott, the G.O.P. leader who was forced to resign.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/11/us/politics/11reid.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By MARK LEIBOVICH</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:84:"http://www.nytimes.com/2010/01/11/us/politics/11reid.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:84:"http://www.nytimes.com/2010/01/11/us/politics/11reid.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263180605;s:7:"dc:date";i:1263180605;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:17:"By MARK LEIBOVICH";}}}}i:25;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:39:"Obama Plays Down Military Role in Yemen";s:11:"description";s:516:"President Obama said he has no intention of sending U.S. troops to Yemen and Somalia amid terrorism concerns.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/11/world/middleeast/11prexy.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By SARAH WHEATON</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:90:"http://www.nytimes.com/2010/01/11/world/middleeast/11prexy.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:90:"http://www.nytimes.com/2010/01/11/world/middleeast/11prexy.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263169803;s:7:"dc:date";i:1263169803;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"By SARAH WHEATON";}}}}i:26;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:50:"In ‘Game Change,’ Insight on the 2008 Campaign";s:11:"description";s:750:"<a href="http://www.nytimes.com/2010/01/11/books/11book.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/11/arts/11book_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>Reporters John Heilemann and Mark Halperin serve up a spicy smorgasbord of observations from the 2008 campaign.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/11/books/11book.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By MICHIKO KAKUTANI</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:78:"http://www.nytimes.com/2010/01/11/books/11book.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:78:"http://www.nytimes.com/2010/01/11/books/11book.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263166203;s:7:"dc:date";i:1263166203;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:19:"By MICHIKO KAKUTANI";}}}}i:27;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:52:"G.O.P. Chairman Urges Reid to Step Down Over Remarks";s:11:"description";s:592:"The Republican Party chairman called for Harry Reid to step down as Senate majority leader after revelations of Mr. Reid’s 2008 remarks about Barack Obama’s skin color.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/11/us/politics/11reidweb.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JEFF ZELENY and JOSEPH BERGER</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:87:"http://www.nytimes.com/2010/01/11/us/politics/11reidweb.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:87:"http://www.nytimes.com/2010/01/11/us/politics/11reidweb.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263151805;s:7:"dc:date";i:1263151805;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:32:"By JEFF ZELENY and JOSEPH BERGER";}}}}i:28;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:41:"Captain Obvious Learns the Limits of Cool";s:11:"description";s:553:"President Obama is no doubt on top of the Christmas terror crisis in terms of studying it top to bottom. But his inner certainty creates an outer disconnect.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/10/opinion/10dowd.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By MAUREEN DOWD</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/10/opinion/10dowd.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/10/opinion/10dowd.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263094204;s:7:"dc:date";i:1263094204;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"By MAUREEN DOWD";}}}}i:29;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:43:"Obama Urges Quick Action on Health Overhaul";s:11:"description";s:512:"President Obama said that Americans would see immediate benefits from health care reform when it is enacted.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/10/health/policy/10address.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JEFF ZELENY</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:89:"http://www.nytimes.com/2010/01/10/health/policy/10address.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:89:"http://www.nytimes.com/2010/01/10/health/policy/10address.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263083404;s:7:"dc:date";i:1263083404;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"By JEFF ZELENY";}}}}i:30;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:64:"Reid Apologizes for Remarks on Obama’s Color and ‘Dialect’";s:11:"description";s:830:"<a href="http://www.nytimes.com/2010/01/10/us/politics/10reidweb.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/10/us/politics/10reidCA/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>Senator Harry Reid is quoted in a new book as saying that Barack Obama could become the first black president because he was “light-skinned” with “no Negro dialect.”
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/10/us/politics/10reidweb.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JEFF ZELENY</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:87:"http://www.nytimes.com/2010/01/10/us/politics/10reidweb.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:87:"http://www.nytimes.com/2010/01/10/us/politics/10reidweb.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263079801;s:7:"dc:date";i:1263079801;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"By JEFF ZELENY";}}}}i:31;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:47:"The Label Factor: Is Obama a Wimp or a Warrior?";s:11:"description";s:516:"Opponents love to cast Democrats as weak, and labels stick. What it might take for the president to look strong.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/10/weekinreview/10cooper.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By HELENE COOPER</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:87:"http://www.nytimes.com/2010/01/10/weekinreview/10cooper.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:87:"http://www.nytimes.com/2010/01/10/weekinreview/10cooper.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263069003;s:7:"dc:date";i:1263069003;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"By HELENE COOPER";}}}}i:32;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:64:"Obama Says Benefits From Health Care Overhaul Would Be Immediate";s:11:"description";s:564:"In his weekly address, the president urged Congress to swiftly reconcile differences over the legislation so it could be signed into law in the coming weeks.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/10/health/policy/10webaddress.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JEFF ZELENY</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:92:"http://www.nytimes.com/2010/01/10/health/policy/10webaddress.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:92:"http://www.nytimes.com/2010/01/10/health/policy/10webaddress.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263054604;s:7:"dc:date";i:1263054604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"By JEFF ZELENY";}}}}i:33;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:25:"G.O.P. Grief and Grieving";s:11:"description";s:548:"Are the battles among establishment Republicans and the tea party folks the desperate thrashings of a dying movement or the labor pains of a new one?
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/09/opinion/09blow.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By CHARLES M. BLOW</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/09/opinion/09blow.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/09/opinion/09blow.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263015005;s:7:"dc:date";i:1263015005;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:18:"By CHARLES M. BLOW";}}}}i:34;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:17:"Jobs and Politics";s:11:"description";s:452:"This year’s midterm elections may push Congress and the White House to enact a robust job-creation agenda.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/09/opinion/09sat2.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/09/opinion/09sat2.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/09/opinion/09sat2.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263011404;s:7:"dc:date";i:1263011404;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:35;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Unions Rally to Oppose a Tax on Health Insurance";s:11:"description";s:777:"<a href="http://www.nytimes.com/2010/01/09/business/09union.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/09/business/09union01/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>Labor leaders say President Obama is betraying unions by supporting a tax on high-priced, employer-sponsored health insurance.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/09/business/09union.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By STEVEN GREENHOUSE</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:82:"http://www.nytimes.com/2010/01/09/business/09union.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:82:"http://www.nytimes.com/2010/01/09/business/09union.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263004202;s:7:"dc:date";i:1263004202;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:20:"By STEVEN GREENHOUSE";}}}}i:36;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:56:"Obama Tries to Turn Focus to Jobs, if Other Events Allow";s:11:"description";s:806:"<a href="http://www.nytimes.com/2010/01/09/business/economy/09assess.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/09/business/09assess_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>President Obama is finding that it can be hard to focus on the lackluster employment situation when so many other issues demand attention.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/09/business/economy/09assess.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JACKIE CALMES</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:91:"http://www.nytimes.com/2010/01/09/business/economy/09assess.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:91:"http://www.nytimes.com/2010/01/09/business/economy/09assess.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263004202;s:7:"dc:date";i:1263004202;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"By JACKIE CALMES";}}}}i:37;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:47:"Obama Takes New Route to Opposing Parts of Laws";s:11:"description";s:516:"The new approach will make it harder to keep track of which statutes the White House believes it can disregard.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/09/us/politics/09signing.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By CHARLIE SAVAGE</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:87:"http://www.nytimes.com/2010/01/09/us/politics/09signing.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:87:"http://www.nytimes.com/2010/01/09/us/politics/09signing.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1263000602;s:7:"dc:date";i:1263000602;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:17:"By CHARLIE SAVAGE";}}}}i:38;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:28:"A Nonstarter on Arms Control";s:11:"description";s:488:"A treaty with Russia should not limit U.S. missile defenses or nuclear modernization.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/09/opinion/09iht-edacohen.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By ARIEL COHEN</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:88:"http://www.nytimes.com/2010/01/09/opinion/09iht-edacohen.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:88:"http://www.nytimes.com/2010/01/09/opinion/09iht-edacohen.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262957402;s:7:"dc:date";i:1262957402;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"By ARIEL COHEN";}}}}i:39;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:22:"A Push for Cleaner Air";s:11:"description";s:497:"A stronger health standard governing harmful ozone, which is also known as smog, would result in cleaner air and better health for millions of Americans.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/08/opinion/08fri2.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/08/opinion/08fri2.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/08/opinion/08fri2.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262928602;s:7:"dc:date";i:1262928602;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:40;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:26:"Gates to Stay Through 2010";s:11:"description";s:565:"Defense Secretary Robert M. Gates, President Obama’s most prominent holdover from the Bush administration, is staying on for another year.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/08/us/politics/08brfs-GATESTOSTAYT_BRF.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By THE ASSOCIATED PRESS</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:101:"http://www.nytimes.com/2010/01/08/us/politics/08brfs-GATESTOSTAYT_BRF.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:101:"http://www.nytimes.com/2010/01/08/us/politics/08brfs-GATESTOSTAYT_BRF.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262928602;s:7:"dc:date";i:1262928602;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:23:"By THE ASSOCIATED PRESS";}}}}i:41;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:17:"Eight Years Later";s:11:"description";s:503:"While President Obama did not create the intelligence system, he has now gotten a bitter lesson in its weaknesses despite the mea culpas and pledges of reform.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/08/opinion/08fri1.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.nytimes.com/2010/01/08/opinion/08fri1.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:80:"http://www.nytimes.com/2010/01/08/opinion/08fri1.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262925004;s:7:"dc:date";i:1262925004;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:42;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:53:"President Is Said to Decide to Renominate Six Choices";s:11:"description";s:816:"<a href="http://www.nytimes.com/2010/01/08/us/politics/08nominate.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/08/us/08nominate_CA0/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>President Obama will renominate Dawn Johnsen to lead the Justice Department’s powerful Office of Legal Counsel, along with his choices for five other jobs.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/08/us/politics/08nominate.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By CHARLIE SAVAGE</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:88:"http://www.nytimes.com/2010/01/08/us/politics/08nominate.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:88:"http://www.nytimes.com/2010/01/08/us/politics/08nominate.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262925004;s:7:"dc:date";i:1262925004;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:17:"By CHARLIE SAVAGE";}}}}i:43;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:55:"Obama Details New Policies in Response to Terror Threat";s:11:"description";s:829:"<a href="http://www.nytimes.com/2010/01/08/us/politics/08terror.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/08/us/08terror_CA0_337-span/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>President Obama ordered a series of steps to streamline how terrorism threats are pursued, after a review of the thwarted bombing revealed shortcomings.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/08/us/politics/08terror.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JEFF ZELENY and HELENE COOPER</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:86:"http://www.nytimes.com/2010/01/08/us/politics/08terror.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:86:"http://www.nytimes.com/2010/01/08/us/politics/08terror.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262914202;s:7:"dc:date";i:1262914202;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:32:"By JEFF ZELENY and HELENE COOPER";}}}}i:44;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:19:"More Money for Math";s:11:"description";s:510:"President Obama announced a $250 million initiative to train math and science teachers.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/07/education/07brfs-MOREMONEYFOR_BRF.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By THE ASSOCIATED PRESS</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:99:"http://www.nytimes.com/2010/01/07/education/07brfs-MOREMONEYFOR_BRF.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:99:"http://www.nytimes.com/2010/01/07/education/07brfs-MOREMONEYFOR_BRF.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262845802;s:7:"dc:date";i:1262845802;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:23:"By THE ASSOCIATED PRESS";}}}}i:45;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:45:"Obama Urges Excise Tax on High-Cost Insurance";s:11:"description";s:585:"President Obama told House Democratic leaders Wednesday that they should include a tax on high-priced insurance policies in the final version of the health care legislation.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/07/health/policy/07health.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By DAVID M. HERSZENHORN</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:88:"http://www.nytimes.com/2010/01/07/health/policy/07health.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:88:"http://www.nytimes.com/2010/01/07/health/policy/07health.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262838602;s:7:"dc:date";i:1262838602;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:23:"By DAVID M. HERSZENHORN";}}}}i:46;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:47:"Democrats Wary as Two Senators Decide to Retire";s:11:"description";s:531:"The Democrats face a perilous environment that could hold major implications for this year’s midterm elections.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/07/us/politics/07dems.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By JEFF ZELENY and ADAM NAGOURNEY</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:84:"http://www.nytimes.com/2010/01/07/us/politics/07dems.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:84:"http://www.nytimes.com/2010/01/07/us/politics/07dems.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262835002;s:7:"dc:date";i:1262835002;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:33:"By JEFF ZELENY and ADAM NAGOURNEY";}}}}i:47;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:41:"Coat Maker Transforms Obama Photo Into Ad";s:11:"description";s:832:"<a href="http://www.nytimes.com/2010/01/07/business/media/07garment.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/07/business/07garment_337-span/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>A New York garment company known for publicity stunts has gotten the attention of the White House with a billboard in Times Square featuring the president.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/07/business/media/07garment.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By STEPHANIE CLIFFORD</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:90:"http://www.nytimes.com/2010/01/07/business/media/07garment.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:90:"http://www.nytimes.com/2010/01/07/business/media/07garment.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262827804;s:7:"dc:date";i:1262827804;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:21:"By STEPHANIE CLIFFORD";}}}}i:48;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:17:"Father Knows Best";s:11:"description";s:590:"Any measure taken to protect us from terrorism won’t be sufficient unless the societies from whence these suicide bombers emerge also erect political, religious and moral restraints.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/06/opinion/06friedman.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By THOMAS L. FRIEDMAN</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:84:"http://www.nytimes.com/2010/01/06/opinion/06friedman.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:84:"http://www.nytimes.com/2010/01/06/opinion/06friedman.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262752202;s:7:"dc:date";i:1262752202;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:21:"By THOMAS L. FRIEDMAN";}}}}i:49;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:38:"Another Iranian Revolution? Not Likely";s:11:"description";s:742:"<a href="http://www.nytimes.com/2010/01/06/opinion/06leverett.html?partner=rssnyt&emc=rss"><img src="http://graphics8.nytimes.com/images/2010/01/06/opinion/06oped_ready/thumbStandard.jpg" border="0" height="75" width="75" hspace="4" align="left" /></a>Protesters want change, not abolition of the Islamic Republic.
    
      <p>
    	<span>
        <a href="http://www.nytimes.com/2010/01/06/opinion/06leverett.html?partner=rssnyt&amp;emc=rss">Originally</a>&nbsp; 
                    from <a href="http://topics.nytimes.com/top/reference/timestopics/people/o/barack_obama/index.html">NYT &gt; Barack Obama</a></span>
            
                    by <span>By FLYNT LEVERETT and HILLARY MANN LEVERETT</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:84:"http://www.nytimes.com/2010/01/06/opinion/06leverett.html?partner=rssnyt&amp;emc=rss";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:84:"http://www.nytimes.com/2010/01/06/opinion/06leverett.html?partner=rssnyt&amp;emc=rss";}s:7:"pubdate";i:1262752202;s:7:"dc:date";i:1262752202;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:43:"By FLYNT LEVERETT and HILLARY MANN LEVERETT";}}}}}s:3:"url";s:57:"http://www.sigmapiphi.org/feeds/out/rss.php?feedtag=Obama";}