	<?php
		// On chapter sites, non-chapter members can't comment so GTFO
		if ( !can_view_boule_only() )
			return;
	?>


	<?php
		if ( post_password_required() ) :
			echo '<h3 class="comments-header">' . __('Password Protected', 'archon-network') . '</h3>';
			echo '<p class="alert password-protected">' . __('Enter the password to view comments.', 'archon-network') . '</p>';
			return;
		endif;

		if ( is_page() && !have_comments() && !comments_open() && !pings_open() )
			return;
	?>

	<?php if ( have_comments() ) : ?>

		<div id="comments">

			<?php
				// Only include comments
				$numTrackBacks = 0; $numComments = 0;
				foreach ( (array)$comments as $comment )
					if ( 'comment' != get_comment_type() )
						$numTrackBacks++;
					else
						$numComments++;
			?>

			<h3 id="comments">
				<?php
					printf( _n( 'One Response to %2$s', '%1$s Responses to %2$s', $numComments, 'archon-network' ),
					number_format_i18n( $numComments ), '<em>' . get_the_title() . '</em>' );
				?>
			</h3>

			<?php do_action( 'sigma_before_blog_comment_list' ) ?>

			<ol class="commentlist">
				<?php wp_list_comments(); ?>
			</ol><!-- .comment-list -->

			<?php do_action( 'sigma_after_blog_comment_list' ) ?>

			<?php if ( get_option( 'page_comments' ) ) : ?>

				<div class="comment-navigation paged-navigation">

					<?php paginate_comments_links(); ?>

				</div>

			<?php endif; ?>

		</div><!-- #comments -->

	<?php endif; ?>

		<?php if ( comments_open() ) : ?>

		<div id="respond">

			<div class="comment-avatar-box">
				<div class="avb">
					<?php echo get_avatar( 0, 50 ); ?>
				</div>
			</div>

			<div class="comment-content">

				<h3 id="reply" class="comments-header">
					<?php comment_form_title( __( 'Leave a Reply', 'archon-network' ), __( 'Leave a Reply to %s', 'archon-network' ), true ); ?>
				</h3>

				<p id="cancel-comment-reply">
					<?php cancel_comment_reply_link( __( 'Click here to cancel reply.', 'archon-network' ) ); ?>
				</p>

				<?php if ( get_option( 'comment_registration' ) && !$user_ID ) : ?>

					<p class="alert">
						<?php printf( __('You must be <a href="%1$s" title="Log in">logged in</a> to post a comment.', 'archon-network'), wp_login_url( get_permalink() ) ); ?>
					</p>

				<?php else : ?>

					<?php do_action( 'sigma_before_blog_comment_form' ) ?>

					<form action="<?php echo site_url( 'wp-comments-post.php' ) ?>" method="post" id="commentform" class="standard-form">

						<?php if ( $user_ID ) : ?>

							<p class="log-in-out">
								<?php printf( __('Logged in as %1$s.', 'archon-network'), $user_identity ); ?> <a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="<?php _e('Log out of this account', 'archon-network'); ?>"><?php _e('Log out &rarr;', 'archon-network'); ?></a>
							</p>

						<?php else : ?>

							<?php $req = get_option( 'require_name_email' ); ?>

							<p class="form-author">
								<label for="author"><?php _e('Name', 'archon-network'); ?> <?php if ( $req ) : ?><span class="required"><?php _e('*', 'archon-network'); ?></span><?php endif; ?></label>
								<input type="text" class="text-input" name="author" id="author" value="<?php echo $comment_author; ?>" size="40" tabindex="1" />
							</p>

							<p class="form-email">
								<label for="email"><?php _e('Email', 'archon-network'); ?>  <?php if ( $req ) : ?><span class="required"><?php _e('*', 'archon-network'); ?></span><?php endif; ?></label>
								<input type="text" class="text-input" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="40" tabindex="2" />
							</p>

							<p class="form-url">
								<label for="url"><?php _e('Website', 'archon-network'); ?></label>
								<input type="text" class="text-input" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="40" tabindex="3" />
							</p>

						<?php endif; ?>

						<p class="form-textarea">
							<label for="comment"><?php _e('Comment', 'archon-network'); ?></label>
							<textarea name="comment" id="comment" cols="60" rows="10" tabindex="4"></textarea>
						</p>

						<?php do_action( 'sigma_blog_comment_form' ) ?>

						<p class="form-submit">
							<input class="submit-comment button" name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('Submit', 'archon-network'); ?>" />
							<?php comment_id_fields(); ?>
						</p>

						<div class="comment-action">
							<?php do_action( 'comment_form', $post->ID ); ?>
						</div>

					</form>

					<?php do_action( 'sigma_after_blog_comment_form' ) ?>

				<?php endif; ?>

			</div><!-- .comment-content -->
		</div><!-- #respond -->

		<?php endif; ?>

		<?php if ( $numTrackBacks ) : ?>
			<div id="trackbacks">

				<span class="title"><?php the_title() ?></span>

				<?php if ( 1 == $numTrackBacks ) : ?>
					<h3><?php printf( __( '%d Trackback', 'archon-network' ), $numTrackBacks ) ?></h3>
				<?php else : ?>
					<h3><?php printf( __( '%d Trackbacks', 'archon-network' ), $numTrackBacks ) ?></h3>
				<?php endif; ?>

				<ul id="trackbacklist">
					<?php foreach ( (array)$comments as $comment ) : ?>

						<?php if ( get_comment_type() != 'comment' ) : ?>
							<li><h5><?php comment_author_link() ?></h5><em>on <?php comment_date() ?></em></li>
	  					<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
