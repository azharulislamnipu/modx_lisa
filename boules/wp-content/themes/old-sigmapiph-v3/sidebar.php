<?php do_action( 'sigma_before_sidebar' ) ?>

<div id="sidebar">
    <div class="padder">

    <?php do_action( 'sigma_inside_before_sidebar' ) ?>

	<?php /* if ( !is_user_logged_in() ) :  ?>
    <h2 class="color red"><?php _e('The Grand Boul&eacute;', 'archon-network');?></h2>
    <div class="contentBlock">

        <?php do_action( 'sigma_before_sidebar_login_form' ) ?>

        <form name="login-form" id="sidebar-login-form" class="standard-form" action="<?php echo site_url( 'wp-login.php', 'login_post' ) ?>" method="post">
            <label><?php _e( 'Username', 'archon-network' ) ?><br />
            <input type="text" name="log" id="sidebar-user-login" class="input" value="<?php echo attribute_escape(stripslashes($user_login)); ?>" /></label>

            <label><?php _e( 'Password', 'archon-network' ) ?><br />
            <input type="password" name="pwd" id="sidebar-user-pass" class="input" value="" /></label>

            <p class="forgetmenot"><label><input name="rememberme" type="checkbox" id="sidebar-rememberme" value="forever" /> <?php _e( 'Remember Me', 'archon-network' ) ?></label></p>

            <?php do_action( 'sigma_sidebar_login_form' ) ?>
            <input type="submit" name="wp-submit" id="sidebar-wp-submit" value="<?php _e('Log In'); ?>" tabindex="100" />
            <input type="hidden" name="testcookie" value="1" />
        </form>

        <?php do_action( 'sigma_after_sidebar_login_form' ) ?>

    </div>
    <?php endif; */ ?>

    <?php if( !is_multisite_primary() && !is_regional_site() ) :

		the_widget('RegionalBoules_Widget', array('show_regional' => 'no'), array('before_title' => '<h3 class="widgettitle">', 'after_title' => '</h3>'));

    	$chapter_name = get_bloginfo('name'); ?>
    	<div class="widget">
	        <h2 class="color yellow" style="clear: both"><?php echo $chapter_name . ' ' . __('Officers', 'archon-network');?></h2>
	        <div class="contentBlock">
	            <?php get_boule_leadership( $chapter_name ) ?>
	        </div>
        </div>
    <?php endif; ?>

    <?php dynamic_sidebar( 'sidebar' ) ?>

    <?php do_action( 'sigma_inside_after_sidebar' ) ?>

    </div><!-- .padder -->
</div><!-- #sidebar -->

<?php do_action( 'sigma_after_sidebar' ) ?>
