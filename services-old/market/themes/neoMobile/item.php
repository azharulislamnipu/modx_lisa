<h1><a title="<?php echo $itemTitle; ?>" href="<?php echo $_SERVER["REQUEST_URI"];?>">
			<?php echo $itemTitle; ?> <?if ($itemPrice!=0) echo " - ".getPrice($itemPrice);?></a>
</h1>

<div class="item">
	<b><?php echo T_PUBLISH_DATE;?>:</b> <?php echo setDate($itemDate);?> <?php echo substr($itemDate,strlen($itemDate)-8);?><?php echo SEPARATOR;?>
	<b><?php echo T_CONTACT_NAME;?>:</b> <?php echo $itemName; ?><?php echo SEPARATOR;?>
	<?php if ($itemPlace!=""){?>
		<b><?php echo T_LOCATION;?>:</b> 
		<?php if (MAP_KEY!=""){?>
				<a title="Map <?php echo $itemPlace;?>" href="<?php echo SITE_URL."/".u(T_MAP).".htm?address=".$itemPlace;?>" rel="gb_page_center[640, 480]"><?php echo $itemPlace;?></a>
		<?php } else echo $itemPlace;?>
		<?php echo SEPARATOR;?> 
	<?php }?>
	<?php if (COUNT_POSTS) echo "$itemViews ".T_DISPLAYED_TIMES.SEPARATOR;?>
</div>
	
<?php if (MAX_IMG_NUM>0){?>
	<div id="item">
		<?php 
			$imgDir=SITE_URL.IMG_UPLOAD.$idItem."/";
			$query="select FileName from ".TABLE_PREFIX."postsimages where idPost=$idItem order by FileName Limit ".MAX_IMG_NUM;	
			$result =$ocdb->query($query);
			$i=1;
			while ($row=mysql_fetch_assoc($result)){
				$imgName=$row["FileName"];
			 	echo "<a href='$imgDir$imgName' title='$itemTitle ".T_PICTURE." $i' target='_blank' >
			 			<img class='thumb' src='".$imgDir."thumb_$imgName' title='$itemTitle ".T_PICTURE." $i' alt='$itemTitle ".T_PICTURE." $i' /></a>";
			 	$i++;
			}
		?>
	</div>
<?php }?>

<p><?php echo $itemDescription;?></p>
	
<?php if ($itemAvailable==1){?><br />
<b><?php echo T_CONTACT;?> <?php echo $itemName.': '.$itemTitle;?></b>
	<?php if ($itemPhone!=""){?><b><?php echo T_CONTACT_PHONE;?>:</b> <?php echo encode_str($itemPhone); ?><?php }?>
	<form method="post" action="" id="contactItem" onsubmit="return checkForm(this);">
	<p>
	<?php echo T_NAME;?>*:<br />
	<input id="name" name="name" type="text" value="<?php echo cP("name");?>" maxlength="75" onblur="validateText(this);"  onkeypress="return isAlphaKey(event);" lang="false"  /><br />
	
	<?php echo T_EMAIL;?>*:<br />
	<input id="email" name="email" type="text" value="<?php echo cP("email");?>" maxlength="120" onblur="validateEmail(this);" lang="false"  /><br />
	
	<?php echo T_MESSAGE;?>*:<br />
	<textarea rows="10" cols="79" name="msg" id="msg" onblur="validateText(this);"  lang="false"><?php echo strip_tags(stripslashes($_POST['msg']));?></textarea><br />
	
	<?php  mathCaptcha();?>
	<input id="math" name="math" type="text" size="2" maxlength="2"  onblur="validateNumber(this);"  onkeypress="return isNumberKey(event);" lang="false" />
	<br />
	<br />
	<input type="hidden" name="contact" value="1" />
	<input type="submit" id="submit" value="<?php echo T_CONTACT;?>" />
	</p>
	</form> 
<?php } else echo "<div id='sysmessage'>".T_NO_LONGER_AVAILABLE."</div>";?>
