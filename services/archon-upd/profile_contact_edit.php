<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("FIRSTNAME", true, "text", "", "", "", "");
$formValidation->addField("LASTNAME", true, "text", "", "", "", "");
$formValidation->addField("ADDRESS1", true, "text", "", "", "", "");
$formValidation->addField("CITY", true, "text", "", "", "", "");
$formValidation->addField("STATECD", true, "text", "", "", "", "");
$formValidation->addField("ZIP", true, "text", "", "", "", "");
$formValidation->addField("HOMEPHONE", true, "text", "", "", "", "");
$formValidation->addField("EMAIL", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsPrefix = "SELECT * FROM spp_sal ORDER BY `PREFIX` ASC";
$rsPrefix = mysql_query($query_rsPrefix, $sigma_modx) or die(mysql_error());
$row_rsPrefix = mysql_fetch_assoc($rsPrefix);
$totalRows_rsPrefix = mysql_num_rows($rsPrefix);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsSuffix = "SELECT * FROM spp_suffix ORDER BY SUFFIX ASC";
$rsSuffix = mysql_query($query_rsSuffix, $sigma_modx) or die(mysql_error());
$row_rsSuffix = mysql_fetch_assoc($rsSuffix);
$totalRows_rsSuffix = mysql_num_rows($rsSuffix);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsState = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsState = mysql_query($query_rsState, $sigma_modx) or die(mysql_error());
$row_rsState = mysql_fetch_assoc($rsState);
$totalRows_rsState = mysql_num_rows($rsState);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsHon = "SELECT hon_id, DESIGNATIONLST FROM spp_hon ORDER BY DESIGNATIONLST ASC";
$rsHon = mysql_query($query_rsHon, $sigma_modx) or die(mysql_error());
$row_rsHon = mysql_fetch_assoc($rsHon);
$totalRows_rsHon = mysql_num_rows($rsHon);

// Make an insert transaction instance
$ins_spp_archons = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_archons);
// Register triggers
$return = !empty( $_POST['RETURN'] ) ? $_POST['RETURN'] : '/home/personal-profile.php';
$ins_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$ins_spp_archons->setTable("spp_archons");
$ins_spp_archons->addColumn("ADDRESS2", "STRING_TYPE", "POST", "ADDRESS2");
$ins_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "LASTUPDATED", "{NOW_DT}");
$ins_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY", "{POST.UPDATEDBY}");
$ins_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_archons = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("PREFIX", "STRING_TYPE", "POST", "PREFIX");
$upd_spp_archons->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$upd_spp_archons->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$upd_spp_archons->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$upd_spp_archons->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$upd_spp_archons->addColumn("DESIGNATIONLST", "STRING_TYPE", "POST", "DESIGNATIONLST");
$upd_spp_archons->addColumn("ADDRESS1", "STRING_TYPE", "POST", "ADDRESS1");
$upd_spp_archons->addColumn("ADDRESS2", "STRING_TYPE", "POST", "ADDRESS2");
$upd_spp_archons->addColumn("CITY", "STRING_TYPE", "POST", "CITY");
$upd_spp_archons->addColumn("STATECD", "STRING_TYPE", "POST", "STATECD");
$upd_spp_archons->addColumn("ZIP", "STRING_TYPE", "POST", "ZIP");
$upd_spp_archons->addColumn("HOMEPHONE", "STRING_TYPE", "POST", "HOMEPHONE");
$upd_spp_archons->addColumn("MOBILEPHONE", "STRING_TYPE", "POST", "MOBILEPHONE");
$upd_spp_archons->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$upd_spp_archons->addColumn("BIRTHDATE", "DATE_TYPE", "POST", "BIRTHDATE");
$upd_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "VALUE", "{POST.UPDATEDBY}");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$row_rsspp_archons = mysql_fetch_assoc($rsspp_archons);
$totalRows_rsspp_archons = mysql_num_rows($rsspp_archons);
?><html xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --><!DOCTYPE html>
<!--
 ____________________________________________________________
|                                                            |
|      DATE + 2006.11.14                                     |
| COPYRIGHT + Sigma Pi Phi 2006.  All Rights Reserved.       |
|____________________________________________________________|

-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-meta.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-css.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-javascript.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-links.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/JSRecordset.js"></script>
<script src="../includes/wdg/classes/CommaMenu.js"></script>
<?php
//begin JSRecordset
$jsObject_rsHon = new WDG_JsRecordset("rsHon");
echo $jsObject_rsHon->getOutput();
//end JSRecordset
?>
<script type="text/javascript" src="../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../includes/resources/calendar.js"></script><!-- InstanceEndEditable -->
</head>

<body class="home blue-bar color-override-yellow-orange">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/main-nav.php"); ?>
			<div class="width">
			    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/global-logo-tagline.php"); ?>
			</div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
	<!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- InstanceBeginEditable name="pagetitle" -->
      <section class="span-12 margin-bottom-medium">
      <div class="inner">
               <h2>Profile</h2>
              <ol class="breadcrumbs" style="margin-top:-18px;">
                <li><a class="B_homeCrumb" href="/home/archon-home.php" title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                <li><span class="B_currentCrumb">Administration</span></li>
              </ol>
           <div>
   </section>
<!-- InstanceEndEditable -->
             
             <!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<h2 class="yellow">EDIT CONTACT INFORMATION</h2>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">

  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <?php $cnt1 = 0; ?>
      <?php $cnt1++; ?>
        <?php
        // Show IF Conditional region1
        if (@$totalRows_rsspp_archons > 1) {
        ?>
        <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
                <?php }
      // endif Conditional region1
      ?>

      <fieldset >
        <legend>Personal Detail:</legend>

    <div >
       <?php
      if (@$_POST['CUSTOMERID'] != "") {
      ?>

      <div>
        <div>
          <label > Prefix </label>
          <select name="PREFIX_<?php echo $cnt1; ?>" id="PREFIX_<?php echo $cnt1; ?>">
            <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
            <?php
              do {
              ?>
            <option value="<?php echo $row_rsPrefix['PREFIX']?>"<?php if (!(strcmp($row_rsPrefix['PREFIX'], $row_rsspp_archons['PREFIX']))) {echo "SELECTED";} ?>><?php echo $row_rsPrefix['PREFIX']?></option>
            <?php
                  } while ($row_rsPrefix = mysql_fetch_assoc($rsPrefix));
                    $rows = mysql_num_rows($rsPrefix);
                    if($rows > 0) {
                        mysql_data_seek($rsPrefix, 0);
                      $row_rsPrefix = mysql_fetch_assoc($rsPrefix);
                    }
                  ?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archons", "PREFIX", $cnt1); ?>
        </div>
        <?php }
         ?>
         <div style="margin-top: 10px;">
            <span>
              <label > Full Name <a style="color:red">*</a></label>

              <?php
              // Show IF Conditional show_FIRSTNAME_on_update_only
              if (@$_POST['CUSTOMERID'] != "") {
              ?>
                <span>
                    <input type="text" placeholder="Enter First Name" name="FIRSTNAME_<?php echo $cnt1; ?>" id="FIRSTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['FIRSTNAME']); ?>" size="20" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "FIRSTNAME", $cnt1); ?> 
 
                </span>
              <?php }
              // endif Conditional show_FIRSTNAME_on_update_only
              ?>

               <?php
              // Show IF Conditional show_middleNAME_on_update_only
              if (@$_POST['CUSTOMERID'] != "") {
              ?>
               <span>
                   <input type="text" placeholder="Enter  Middle Initail" name="MIDDLEINITIAL_<?php echo $cnt1; ?>" id="MIDDLEINITIAL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['MIDDLEINITIAL']); ?>" size="20" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("MIDDLEINITIAL");?> <?php echo $tNGs->displayFieldError("spp_archons", "MIDDLEINITIAL", $cnt1); ?> 
      
              </span>
              <?php }
              // endif Conditional show_MiddleNAME_on_update_only
              ?>

               <?php
              // Show IF Conditional show_lastNAME_on_update_only
              if (@$_POST['CUSTOMERID'] != "") {
              ?>
               <span>
                  <input type="text" placeholder="Enter Last Name" name="LASTNAME_<?php echo $cnt1; ?>" id="LASTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['LASTNAME']); ?>" size="20" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "LASTNAME", $cnt1); ?> 

              </span>
              <?php }
              // endif Conditional show_lastNAME_on_update_only
              ?>
            </span>
          </div> 
          
        
        <?php
        // Show IF Conditional show_SUFFIX_on_update_only
        if (@$_POST['CUSTOMERID'] != "") {
        ?>
        <div style="margin-top: 10px;">
          <label > Suffix </label>
          <select name="SUFFIX_<?php echo $cnt1; ?>" id="SUFFIX_<?php echo $cnt1; ?>">
              <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                <?php
                do {
                ?>
              <option value="<?php echo $row_rsSuffix['SUFFIX']?>"<?php if (!(strcmp($row_rsSuffix['SUFFIX'], $row_rsspp_archons['SUFFIX']))) {echo "SELECTED";} ?>><?php echo $row_rsSuffix['SUFFIX']?></option>
              <?php
                  } while ($row_rsSuffix = mysql_fetch_assoc($rsSuffix));
                    $rows = mysql_num_rows($rsSuffix);
                    if($rows > 0) {
                        mysql_data_seek($rsSuffix, 0);
                      $row_rsSuffix = mysql_fetch_assoc($rsSuffix);
                    }
                  ?>
            </select>
          <?php echo $tNGs->displayFieldError("spp_archons", "SUFFIX", $cnt1); ?> 
        </div>
          <?php }
        // endif Conditional show_SUFFIX_on_update_only
        ?>
        
         <?php
        // Show IF Conditional show_Honorofix_on_update_only
        if (@$_POST['CUSTOMERID'] != "") {
        ?>
        <div style="margin-top: 10px;">
          <label > Honorofix </label>
          <!-- <input type="text"  /> -->
          <input  name="DESIGNATIONLST_<?php echo $cnt1; ?>" id="DESIGNATIONLST_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['DESIGNATIONLST']); ?>" size="5" maxlength="20" wdg:recordset="rsHon" wdg:subtype="CommaMenu" wdg:size="5" wdg:type="widget" wdg:displayfield="DESIGNATIONLST" wdg:valuefield="DESIGNATIONLST" />
          <?php echo $tNGs->displayFieldHint("DESIGNATIONLST");?> <?php echo $tNGs->displayFieldError("spp_archons", "DESIGNATIONLST", $cnt1); ?> 

        </div>
         <?php }
        // endif Conditional show_Honorofix_on_update_only
        ?>

        <div style="margin-top: 10px;">
          <label > Address <a style="color:red">*</a> </label>

          <?php
          // Show IF Conditional show_Address 1_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
          <span>
               <input type="text" placeholder="Enter Address 1 " name="ADDRESS1_<?php echo $cnt1; ?>" id="ADDRESS1_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ADDRESS1']); ?>" size="20" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ADDRESS1");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS1", $cnt1); ?> </td>
          </span>
           <?php }
          // endif Conditional show_Address 1_on_update_only
          ?>

           <?php
          // Show IF Conditional show_Address 2_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
          <span>
           <!--  <textarea  cols="30" rows="4" placeholder="Enter Address 2"></textarea> -->
            <input type="text"  placeholder="Enter Address 2" name="ADDRESS2_<?php echo $cnt1; ?>" id="ADDRESS2_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ADDRESS2']); ?>" size="20" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("ADDRESS2");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS2", $cnt1); ?> </td>

          </span>
           <?php }
          // endif Conditional show_Address 2_on_update_only
          ?>
        </div>

        <?php
          // Show IF Conditional show_city_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
          <div style="margin-top: 10px;">
            <label > City <a style="color:red">*</a></label>
            <input type="text" placeholder="Enter City" name="CITY_<?php echo $cnt1; ?>" id="CITY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['CITY']); ?>" size="20" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("CITY");?> <?php echo $tNGs->displayFieldError("spp_archons", "CITY", $cnt1); ?> </td>

          </div>
         <?php }
          // endif Conditional show_city_on_update_only
          ?>

          <?php
          // Show IF Conditional show_state_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
           <div style="margin-top: 10px;">
              <label > State <a style="color:red">*</a></label>
                 <select name="STATECD_<?php echo $cnt1; ?>" id="STATECD_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                    <?php
                    do {
                    ?>
                    <option value="<?php echo $row_rsState['st_cd']?>"<?php if (!(strcmp($row_rsState['st_cd'], $row_rsspp_archons['STATECD']))) {echo "SELECTED";} ?>><?php echo $row_rsState['st_nm']?></option>
                    <?php
                    } while ($row_rsState = mysql_fetch_assoc($rsState));
                      $rows = mysql_num_rows($rsState);
                      if($rows > 0) {
                          mysql_data_seek($rsState, 0);
                        $row_rsState = mysql_fetch_assoc($rsState);
                      }
                    ?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_archons", "STATECD", $cnt1); ?> 
          </div>
        <?php }
          // endif Conditional show_state_on_update_only
          ?>

          <?php
          // Show IF Conditional show_zip_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
           <div style="margin-top: 10px;">
            <label > Zip <a style="color:red">*</a></label>
             <input type="text" placeholder="Enter Zip" name="ZIP_<?php echo $cnt1; ?>" id="ZIP_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ZIP']); ?>" size="20" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ZIP");?> <?php echo $tNGs->displayFieldError("spp_archons", "ZIP", $cnt1); ?> </td>

          </div>
         <?php }
          // endif Conditional show_zip_on_update_only
          ?>
      </div>
      
      </fieldset>


       <fieldset ><!-- style="background-color: beige;" -->
        <legend>Contact Detail:</legend>

        <div>
           <?php
          // Show IF Conditional show_Home Phone_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
            <div style="margin-top: 10px;">
              <label > Home Phone <a style="color:red">*</a></label>
               <input type="text" placeholder="Enter Home Phone" name="HOMEPHONE_<?php echo $cnt1; ?>" id="HOMEPHONE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['HOMEPHONE']); ?>" size="20" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("HOMEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "HOMEPHONE", $cnt1); ?> </td>

            </div>
          <?php }
          // endif Conditional show_Home Phone_on_update_only
          ?>

          <?php
          // Show IF Conditional show_Mobile Phone_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
           <div style="margin-top: 10px;">
            <label > Mobile Phone </label>
             <input type="text" placeholder="Enter Mobile Phone" name="MOBILEPHONE_<?php echo $cnt1; ?>" id="MOBILEPHONE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['MOBILEPHONE']); ?>" size="20" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("MOBILEPHONE");?><?php echo $tNGs->displayFieldError("spp_archons", "MOBILEPHONE", $cnt1); ?></td>

          </div>
          <?php }
          // endif Conditional show_Mobile Phone_on_update_only
          ?>

          <?php
          // Show IF Conditional show_Contact Email_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
           <div style="margin-top: 10px;">
            <label > Contact Email <a style="color:red">*</a></label>
            <input type="text" placeholder="Enter Contact Email" name="EMAIL_<?php echo $cnt1; ?>" id="EMAIL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['EMAIL']); ?>" size="20" maxlength="125" />
                  <?php echo $tNGs->displayFieldHint("EMAIL");?> <?php echo $tNGs->displayFieldError("spp_archons", "EMAIL", $cnt1); ?> </td>

          </div>
          <?php }
          // endif Conditional show_Contact Email_on_update_only
          ?>

          <?php
          // Show IF Conditional show_Birth Date_on_update_only
          if (@$_POST['CUSTOMERID'] != "") {
          ?>
           <div style="margin-top: 10px;">
            <label > Birth Date </label>
            <input name="BIRTHDATE" id="BIRTHDATE" value="<?php echo KT_escapeAttribute($row_rsspp_archons['BIRTHDATE']); ?>" size="20" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("BIRTHDATE");?> <?php echo $tNGs->displayFieldError("spp_archons", "BIRTHDATE"); ?> </td>

          </div>
          <?php }
          // endif Conditional show_Birth Date_on_update_only
          ?>

        </div>
      </fieldset>


          <input type="hidden" name="kt_pk_spp_archons_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_archons['kt_pk_spp_archons']); ?>" />
            <input type="hidden" name="LASTUPDATED_<?php echo $cnt1; ?>" id="LASTUPDATED_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_archons['LASTUPDATED']); ?>" />
            <input type="hidden" name="UPDATEDBY_<?php echo $cnt1; ?>" id="UPDATEDBY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['UPDATEDBY']); ?>" />
            <input type="hidden" name="RETURN" id="RETURN" value="<?php echo KT_escapeAttribute($_POST['RETURN']); ?>" />
            <div style="margin-top: 10px;">
          
             <!--  <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onClick="return UNI_navigateCancel(event, '<?php echo $return ?>')" />
           -->
               <input type="submit" name="KT_Update1" id="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>"/>
               <input type="button" name="Cancel" id="KT_Updatecancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onClick="return UNI_navigateCancel(event, '<?php echo $return ?>')"/>
          
         
        </div>

    </form>




    <!-- <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
       

        <input type="hidden" name="kt_pk_spp_archons_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_archons['kt_pk_spp_archons']); ?>" />
        <input type="hidden" name="LASTUPDATED_<?php echo $cnt1; ?>" id="LASTUPDATED_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_archons['LASTUPDATED']); ?>" />
        <input type="hidden" name="UPDATEDBY_<?php echo $cnt1; ?>" id="UPDATEDBY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['UPDATEDBY']); ?>" />
        <input type="hidden" name="RETURN" id="RETURN" value="<?php echo KT_escapeAttribute($_POST['RETURN']); ?>" />
        <div class="KT_bottombuttons">
          <div>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onClick="return UNI_navigateCancel(event, '<?php echo $return ?>')" />
          </div>
        </div>
      </form>
    </div> -->
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
            <!-- main content body end -->	
	
	<!-- third section event and articles start -->
  	 <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
	<!-- third section event and articles end -->
	
    <footer class="width padding" style="border-top: 10px #52B609 solid;">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-content-default.php"); ?>
      </div>
    </footer>
	
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-scripts.php"); ?>
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsPrefix);

mysql_free_result($rsSuffix);

mysql_free_result($rsState);

mysql_free_result($rsHon);
?>
