<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("CUSTOMERCD", true, "numeric", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsDelPool = "-1";
if (isset($_POST['BOULECD'])) {
  $colname_rsDelPool = $_POST['BOULECD'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsDelPool = sprintf("SELECT DISTINCT * FROM vw_elig_delegate   LEFT OUTER JOIN spp_delegates ON (vw_elig_delegate.CUSTOMERID = spp_delegates.CUSTOMERCD) WHERE (spp_delegates.delegate_id IS NULL OR    spp_delegates.delegate_status != 'A') AND CHAPTERID = %s ORDER BY FULLNAME2 ASC", GetSQLValueString($colname_rsDelPool, "text"));
$rsDelPool = mysql_query($query_rsDelPool, $sigma_modx) or die(mysql_error());
$row_rsDelPool = mysql_fetch_assoc($rsDelPool);
$totalRows_rsDelPool = mysql_num_rows($rsDelPool);

// Make an insert transaction instance
$ins_spp_delegates = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_delegates);
// Register triggers
$ins_spp_delegates->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_delegates->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/delegates_dashboard.php");
$ins_spp_delegates->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
// Add columns
$ins_spp_delegates->setTable("spp_delegates");
$ins_spp_delegates->addColumn("meeting_id", "NUMERIC_TYPE", "POST", "meeting_id", "{POST.meeting_id}");
$ins_spp_delegates->addColumn("delegate_type", "STRING_TYPE", "POST", "delegate_type", "{POST.delegate_type}");
$ins_spp_delegates->addColumn("delegate_status", "STRING_TYPE", "POST", "delegate_status", "{POST.delegate_status}");
$ins_spp_delegates->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$ins_spp_delegates->addColumn("BOULECD", "STRING_TYPE", "POST", "BOULECD", "{POST.BOULECD}");
$ins_spp_delegates->addColumn("BOULECOMMENT", "STRING_TYPE", "POST", "BOULECOMMENT");
$ins_spp_delegates->addColumn("APPTDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_delegates->addColumn("APPTBY", "STRING_TYPE", "VALUE", "{POST.APPTBY}");
$ins_spp_delegates->setPrimaryKey("delegate_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_delegates = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_delegates);
// Register triggers
$upd_spp_delegates->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_delegates->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/delegates_dashboard.php");
$upd_spp_delegates->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
// Add columns
$upd_spp_delegates->setTable("spp_delegates");
$upd_spp_delegates->addColumn("meeting_id", "NUMERIC_TYPE", "POST", "meeting_id");
$upd_spp_delegates->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$upd_spp_delegates->addColumn("BOULECD", "STRING_TYPE", "POST", "BOULECD");
$upd_spp_delegates->addColumn("BOULECOMMENT", "STRING_TYPE", "POST", "BOULECOMMENT");
$upd_spp_delegates->addColumn("delegate_status", "STRING_TYPE", "POST", "delegate_status");
$upd_spp_delegates->addColumn("delegate_checkin", "CHECKBOX_1_0_TYPE", "POST", "delegate_checkin");
$upd_spp_delegates->addColumn("CONFIRMCOMMENT", "STRING_TYPE", "POST", "CONFIRMCOMMENT");
$upd_spp_delegates->addColumn("CERTIFYDATE", "DATE_TYPE", "CURRVAL", "");
$upd_spp_delegates->addColumn("CERTIFYBY", "STRING_TYPE", "CURRVAL", "");
$upd_spp_delegates->setPrimaryKey("delegate_id", "NUMERIC_TYPE", "GET", "delegate_id");

// Make an instance of the transaction object
$del_spp_delegates = new tNG_multipleDelete($conn_sigma_modx);
$tNGs->addTransaction($del_spp_delegates);
// Register triggers
$del_spp_delegates->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_delegates->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/delegates_dashboard.php");
// Add columns
$del_spp_delegates->setTable("spp_delegates");
$del_spp_delegates->setPrimaryKey("delegate_id", "NUMERIC_TYPE", "GET", "delegate_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_delegates = $tNGs->getRecordset("spp_delegates");
$row_rsspp_delegates = mysql_fetch_assoc($rsspp_delegates);
$totalRows_rsspp_delegates = mysql_num_rows($rsspp_delegates);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: true,
  merge_down_value: true
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">
    <h1>MANAGE DELEGATION</h1>
    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_delegates > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php } 
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <?php 
// Show IF Conditional show_delegate_type_on_insert_only 
if (@$_GET['delegate_id'] == "") {
?>
              <tr>
                <td class="KT_th"><label for="delegate_type_<?php echo $cnt1; ?>_1">TYPE:</label></td>
                <td><h2><?php echo $_POST['delegate_type']; ?></h2></td>
              </tr>
              <?php } 
// endif Conditional show_delegate_type_on_insert_only
?>
<tr>
              <td class="KT_th"><label for="CUSTOMERCD_<?php echo $cnt1; ?>">ARCHON:</label></td>
              <td><select name="CUSTOMERCD_<?php echo $cnt1; ?>" id="CUSTOMERCD_<?php echo $cnt1; ?>">
                  <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                  <?php 
do {  
?>
                  <option value="<?php echo $row_rsDelPool['CUSTOMERID']?>"<?php if (!(strcmp($row_rsDelPool['CUSTOMERID'], $row_rsspp_delegates['CUSTOMERCD']))) {echo "SELECTED";} ?>><?php echo $row_rsDelPool['FULLNAME2']?></option>
                  <?php
} while ($row_rsDelPool = mysql_fetch_assoc($rsDelPool));
  $rows = mysql_num_rows($rsDelPool);
  if($rows > 0) {
      mysql_data_seek($rsDelPool, 0);
	  $row_rsDelPool = mysql_fetch_assoc($rsDelPool);
  }
?>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_delegates", "CUSTOMERCD", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="BOULECOMMENT_<?php echo $cnt1; ?>">COMMENT:</label></td>
              <td><textarea name="BOULECOMMENT_<?php echo $cnt1; ?>" id="BOULECOMMENT_<?php echo $cnt1; ?>" cols="80" rows="5"><?php echo KT_escapeAttribute($row_rsspp_delegates['BOULECOMMENT']); ?></textarea>
                  <?php echo $tNGs->displayFieldHint("BOULECOMMENT");?> <?php echo $tNGs->displayFieldError("spp_delegates", "BOULECOMMENT", $cnt1); ?> </td>
            </tr>
            <?php 
// Show IF Conditional show_APPTDATE_on_insert_only 
if (@$_GET['delegate_id'] == "") {
?>
              <tr>
                <td class="KT_th">APPOINTMENT DATE:</td>
                <td><?php echo KT_formatDate($row_rsspp_delegates['APPTDATE']); ?></td>
              </tr>
              <?php } 
// endif Conditional show_APPTDATE_on_insert_only
?>
            <?php 
// Show IF Conditional show_APPTBY_on_insert_only 
if (@$_GET['delegate_id'] == "") {
?>
              <tr>
                <td class="KT_th">APPOINTED BY:</td>
                <td><?php echo KT_escapeAttribute($row_rsspp_delegates['APPTBY']); ?></td>
              </tr>
              <?php } 
// endif Conditional show_APPTBY_on_insert_only
?>
              <?php 
// Show IF Conditional show_delegate_status_on_update_only 
if (@$_GET['delegate_id'] != "") {
?>
                <tr>
                  <td class="KT_th"><label for="delegate_status_<?php echo $cnt1; ?>_1">Delegate_status:</label></td>
                  <td><div>
                      <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_delegates['delegate_status']),"A"))) {echo "CHECKED";} ?> type="radio" name="delegate_status_<?php echo $cnt1; ?>" id="delegate_status_<?php echo $cnt1; ?>_1" value="A" />
                      <label for="delegate_status_<?php echo $cnt1; ?>_1">Delegate_status:</label>
                    </div>
                      <div>
                        <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_delegates['delegate_status']),"I"))) {echo "CHECKED";} ?> type="radio" name="delegate_status_<?php echo $cnt1; ?>" id="delegate_status_<?php echo $cnt1; ?>_2" value="I" />
                        <label for="delegate_status_<?php echo $cnt1; ?>_2">Delegate_status:</label>
                      </div>
                    <?php echo $tNGs->displayFieldError("spp_delegates", "delegate_status", $cnt1); ?> </td>
                </tr>
                <?php } 
// endif Conditional show_delegate_status_on_update_only
?>
<?php 
// Show IF Conditional show_delegate_checkin_on_update_only 
if (@$_GET['delegate_id'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="delegate_checkin_<?php echo $cnt1; ?>">CHECK IN:</label></td>
                <td><input  <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_delegates['delegate_checkin']),"1"))) {echo "checked";} ?> type="checkbox" name="delegate_checkin_<?php echo $cnt1; ?>" id="delegate_checkin_<?php echo $cnt1; ?>" value="1" />
                    <?php echo $tNGs->displayFieldError("spp_delegates", "delegate_checkin", $cnt1); ?> </td>
              </tr>
              <?php } 
// endif Conditional show_delegate_checkin_on_update_only
?>
            <?php 
// Show IF Conditional show_CONFIRMCOMMENT_on_update_only 
if (@$_GET['delegate_id'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="CONFIRMCOMMENT_<?php echo $cnt1; ?>">CONFIRMATION COMMENT:</label></td>
                <td><textarea name="CONFIRMCOMMENT_<?php echo $cnt1; ?>" id="CONFIRMCOMMENT_<?php echo $cnt1; ?>" cols="80" rows="5"><?php echo KT_escapeAttribute($row_rsspp_delegates['CONFIRMCOMMENT']); ?></textarea>
                    <?php echo $tNGs->displayFieldHint("CONFIRMCOMMENT");?> <?php echo $tNGs->displayFieldError("spp_delegates", "CONFIRMCOMMENT", $cnt1); ?> </td>
              </tr>
              <?php } 
// endif Conditional show_CONFIRMCOMMENT_on_update_only
?>
            <?php 
// Show IF Conditional show_CERTIFYDATE_on_update_only 
if (@$_GET['delegate_id'] != "") {
?>
              <tr>
                <td class="KT_th">CERTIFICATION DATE:</td>
                <td><?php echo KT_formatDate($row_rsspp_delegates['CERTIFYDATE']); ?></td>
              </tr>
              <?php } 
// endif Conditional show_CERTIFYDATE_on_update_only
?>
            <?php 
// Show IF Conditional show_CERTIFYBY_on_update_only 
if (@$_GET['delegate_id'] != "") {
?>
              <tr>
                <td class="KT_th">CERTIFIED BY:</td>
                <td><?php echo KT_escapeAttribute($row_rsspp_delegates['CERTIFYBY']); ?></td>
              </tr>
              <?php } 
// endif Conditional show_CERTIFYBY_on_update_only
?>
          </table>
          <input type="hidden" name="kt_pk_spp_delegates_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_delegates['kt_pk_spp_delegates']); ?>" />
          <input type="hidden" name="meeting_id_<?php echo $cnt1; ?>" id="meeting_id_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['meeting_id']); ?>" />
          <input type="hidden" name="delegate_type_<?php echo $cnt1; ?>" id="delegate_type_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['delegate_type']); ?>" />
          <input type="hidden" name="delegate_status_<?php echo $cnt1; ?>" id="delegate_status_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['delegate_status']); ?>" />
          <input type="hidden" name="BOULECD_<?php echo $cnt1; ?>" id="BOULECD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_POST['BOULECD']); ?>" />
          <?php } while ($row_rsspp_delegates = mysql_fetch_assoc($rsspp_delegates)); ?>
        <div class="KT_bottombuttons">
          <div>
            <?php 
      // Show IF Conditional region1
      if (@$_GET['delegate_id'] == "") {
      ?>
              <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
              <?php 
      // else Conditional region1
      } else { ?>
              <div class="KT_operations">
                <input type="submit" name="KT_Insert1" value="<?php echo NXT_getResource("Insert as new_FB"); ?>" onclick="nxt_form_insertasnew(this, 'delegate_id')" />
              </div>
              <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
              <?php }
      // endif Conditional region1
      ?>
            <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/delegates_dashboard.php')" />
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>

<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsDelPool);
?>
