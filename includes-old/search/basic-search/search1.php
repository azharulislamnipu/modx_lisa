<?php
$db = get_db();
//$db->setTrace (true);
$db->join("spp_officers o", "g.CUSTOMERID=o.CUSTOMERCD", "LEFT");
$db->orderBy("g.{$type}","ASC");
if($type == 'LASTNAME' || $type == 'CITY'){
	$db->where ("g.{$type}", $_GET[$type], 'LIKE');
} else {
	$db->where ("g.{$type}", $_GET[$type]);
}
$all_users =  array(
	'data' => $db->get('vw_web_elig g', null, "g.*, o.CPOSITION, o.COMMITTEESTATUSSTT"),
	'count' => $db->count
);
//print_r ($db->trace);
//die();
if($all_users['count'] > 0) {
	ob_start();
	foreach ( $all_users['data'] as $agu) {
		if(isset($agu['COMMITTEESTATUSSTT']) && $agu['COMMITTEESTATUSSTT'] == 'Active'){
			$position = $agu['CPOSITION'];
		} else {
			$position = '<span style="display:none;">ZZZ</span>N/A';
		}
		$mphone = '';
		if($agu['MOBILEPHONE']){
			$mphone = ", " . $agu['MOBILEPHONE'];
		}
		echo <<<DDD
<tr data-customerid="{$agu['CUSTOMERID']}" data-updatedBy="[[svVars? &var=`webInternalKey`]]" data-updaterEmail="[[svVars? &var=`webEmail`]]" data-webId="{$agu['WEB_ID']}">
	<td>
		<div id="{$agu['WEB_ID']}" class="balloonstyle">
			<strong>{$agu['PREFIX']} {$agu['L2R_FULLNAME']}</strong><br />
			{$agu['EMAIL']}<br />
			{$agu['HOMEPHONE']}{$mphone}
		</div>
	</td>
	<td>{$agu['STATUS']}</td>
	<td>{$position}</td>
	<td class="action_td">
		<a href="/services/spp-directory/search-results-noedit-json.php?id={$agu['CUSTOMERID']}" class="view" id="view_{$agu['WEB_ID']}">View</a>
	</td>
</tr>
DDD;

	}
	ob_flush();
}