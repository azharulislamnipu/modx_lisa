<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  // ----- UPDATED (Start) ------
  $theValue = str_replace('*', '%', $theValue);
  // ----- UPDATED (End) ------

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rsResults = 15;
$pageNum_rsResults = 0;
if (isset($_GET['pageNum_rsResults'])) {
  $pageNum_rsResults = $_GET['pageNum_rsResults'];
}
$startRow_rsResults = $pageNum_rsResults * $maxRows_rsResults;

$colname1_rsResults = "-1";
if (isset($_GET['FIRSTNAME'])) {
  $colname1_rsResults = (get_magic_quotes_gpc()) ? $_GET['FIRSTNAME'] : addslashes($_GET['FIRSTNAME']);
}
$colname2_rsResults = "-1";
if (isset($_GET['LASTNAME'])) {
  $colname2_rsResults = (get_magic_quotes_gpc()) ? $_GET['LASTNAME'] : addslashes($_GET['LASTNAME']);
}
$colname3_rsResults = "-1";
if (isset($_GET['CHAPTERID'])) {
  $colname3_rsResults = (get_magic_quotes_gpc()) ? $_GET['CHAPTERID'] : addslashes($_GET['CHAPTERID']);
}
$colname4_rsResults = "-1";
if (isset($_GET['CITY'])) {
  $colname4_rsResults = (get_magic_quotes_gpc()) ? $_GET['CITY'] : addslashes($_GET['CITY']);
}
$colname5_rsResults = "-1";
if (isset($_GET['STATECD'])) {
  $colname5_rsResults = (get_magic_quotes_gpc()) ? $_GET['STATECD'] : addslashes($_GET['STATECD']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);

//$query_rsResults = sprintf("SELECT * FROM vw_web_elig WHERE FIRSTNAME = %s OR LASTNAME = %s = CHAPTERID OR %s OR CITY = %s OR STATECD = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname1_rsResults, "text"),GetSQLValueString($colname2_rsResults, "text"),GetSQLValueString($colname3_rsResults, "text"),GetSQLValueString($colname4_rsResults, "text"),GetSQLValueString($colname5_rsResults, "text"));

/*
 * ----- UPDATED (Start) ------
 */
$firstname  = GetSQLValueString($colname1_rsResults, "text");
$lastname   = GetSQLValueString($colname2_rsResults, "text");
$location   = GetSQLValueString($colname3_rsResults, "text");
$city       = GetSQLValueString($colname4_rsResults, "text");
$state      = GetSQLValueString($colname5_rsResults, "text");

$sql = 'SELECT * FROM vw_web_elig WHERE 1=1';

if (!empty($firstname))
{
  $sql .= ' AND FIRSTNAME LIKE '.$firstname;
}

if (!empty($lastname))
{
  $sql .= ' AND LASTNAME LIKE '.$lastname;
}

if (!empty($city))
{
  $sql .= ' AND CITY LIKE '.$city;
}

$sql .= ' AND CHAPTERID = '.$location.' AND STATECD = '.$state.' ORDER BY FULLNAME ASC';

$query_rsResults = $sql;
/* 
 * ----- UPDATED (End) -----
 */
  
$query_limit_rsResults = sprintf("%s LIMIT %d, %d", $query_rsResults, $startRow_rsResults, $maxRows_rsResults);
$rsResults = mysql_query($query_limit_rsResults, $sigma_modx) or die(mysql_error());
$row_rsResults = mysql_fetch_assoc($rsResults);

if (isset($_GET['totalRows_rsResults'])) {
  $totalRows_rsResults = $_GET['totalRows_rsResults'];
} else {
  $all_rsResults = mysql_query($query_rsResults);
  $totalRows_rsResults = mysql_num_rows($all_rsResults);
}
$totalPages_rsResults = ceil($totalRows_rsResults/$maxRows_rsResults)-1;

$queryString_rsResults = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsResults") == false && 
        stristr($param, "totalRows_rsResults") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsResults = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsResults = sprintf("&totalRows_rsResults=%d%s", $totalRows_rsResults, $queryString_rsResults);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --> 
<link rel="stylesheet" type="text/css" href="/home/assets/css/yearbook.css" media="all" />

<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">Basic Search Results</h1>
<div class="contentBlock">
    <table class="chapter" cellspacing="5">
            <tr>
              <td class="name" colspan="3">
                <h1>Results for Basic Keyword Search</h1>
		        <h2>Displaying <?php echo (min($startRow_rsResults + 1, $totalRows_rsResults)) ?> to  <?php echo min($startRow_rsResults + $maxRows_rsResults, $totalRows_rsResults) ?> of <?php echo $totalRows_rsResults ?> Archons</h2>
		        <h2><a href="<?php printf("%s?pageNum_rsResults=%d%s", $currentPage, 0, $queryString_rsResults); ?>">First</a> | <a href="<?php printf("%s?pageNum_rsResults=%d%s", $currentPage, max(0, $pageNum_rsResults - 1), $queryString_rsResults); ?>">Previous</a> | <a href="<?php printf("%s?pageNum_rsResults=%d%s", $currentPage, min($totalPages_rsResults, $pageNum_rsResults + 1), $queryString_rsResults); ?>">Next</a> | <a href="<?php printf("%s?pageNum_rsResults=%d%s", $currentPage, $totalPages_rsResults, $queryString_rsResults); ?>">Last</a></h2>
    <h2><a href="/home/basic-search.php">Search Again</a></h2></td>
              </tr>
			  
  <?php do { ?>
      <tr>
        <?php
  do { // horizontal looper version 3
?>
          <td class="member">
		    <?php 
// Show IF Conditional region1 
if (@$row_rsResults['IMAGE'] != NULL) {
?>
		      <div class="avatar"> <img border="0" width="48" src="/home/assets/images/archons/<?php echo $row_rsResults['IMAGE']; ?>" /> </div>
		      <?php } 
// endif Conditional region1
?>

<div class="noAvatar">
                <h2><a href="results.php?id=<?php echo $row_rsResults['WEB_ID']; ?>"><?php echo $row_rsResults['L2R_FULLNAME']; ?></a></h2>			
			                      
		        <p>
				<?php 
// Show IF Conditional region2 
if (@$row_rsResults['CPOSITION'] != NULL) {
?>
				  <?php echo $row_rsResults['CPOSITION']; ?><br/>
				  <?php 
// else Conditional region2
} else { ?>
				  
  <?php } 
// endif Conditional region2
?>
				<?php echo $row_rsResults['ADDRESS1']; ?><br/>
		          <?php echo $row_rsResults['CITY']; ?>, <?php echo $row_rsResults['STATECD']; ?> <?php echo $row_rsResults['ZIP']; ?><br />
		          <?php echo $row_rsResults['HOMEPHONE']; ?></p>
                          <p><a href="<?php echo $row_rsResults['WEB_EMAIL']; ?>"><?php echo $row_rsResults['WEB_EMAIL']; ?></a></p>
                        <h3><a href="boule_results.php?BOULENAME=<?php echo $row_rsResults['BOULENAME']; ?>"><?php echo $row_rsResults['BOULENAME']; ?> Boul&eacute;</a></h3>
</div>		</td>
          <?php
    $row_rsResults = mysql_fetch_assoc($rsResults);
    if (!isset($nested_rsResults)) {
      $nested_rsResults= 1;
    }
    if (isset($row_rsResults) && is_array($row_rsResults) && $nested_rsResults++ % 3==0) {
      echo "</tr><tr>";
    }
  } while ($row_rsResults); //end horizontal looper version 3
?>
      </tr>
    <?php } while ($row_rsResults = mysql_fetch_assoc($rsResults)); ?> 
      
            <tr><td class="name" colspan="3"><h2><a href="<?php printf("%s?pageNum_rsResults=%d%s", $currentPage, 0, $queryString_rsResults); ?>">First</a> | <a href="<?php printf("%s?pageNum_rsResults=%d%s", $currentPage, max(0, $pageNum_rsResults - 1), $queryString_rsResults); ?>">Previous</a> | <a href="<?php printf("%s?pageNum_rsResults=%d%s", $currentPage, min($totalPages_rsResults, $pageNum_rsResults + 1), $queryString_rsResults); ?>">Next</a> | <a href="<?php printf("%s?pageNum_rsResults=%d%s", $currentPage, $totalPages_rsResults, $queryString_rsResults); ?>">Last</a></h2>
      <h2><a href="/home/basic-search.php">Search Again</a></h2></td>
                </tr>
        </table>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsResults);
?>
