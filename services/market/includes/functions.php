<?php
////////////////////////////////////////////////////////////
//Functions: call to all the includes
////////////////////////////////////////////////////////////

//Initial defines
define('VERSION','1.6.3');
define('DEBUG', false);//if you change this to true, returns error in the page instead of email, also enables debug from phpMyDB and disables disqus
if (!DEBUG){//don't display any error message
    error_reporting(0);
    ini_set('display_errors','off');
}
else ini_set('display_errors','on');//displays error messages

if (extension_loaded('zlib')) {//check extension is loaded
    if(!ob_start("ob_gzhandler")) ob_start();//start HTML compression, if not normal buffer input mode
}

session_start();
require_once('config.php');//configuration file
require_once('error.php');//handler for errors
require_once(SITE_ROOT.'/languages/'.LANGUAGE.'.php');//loading language for the app

//loading all classes
require_once('classes/fileCache.php');//cache
require_once('classes/phpMyDb.php');//class for db handling
require_once('classes/phpSEO.php');//class for SEO handling
if (AKISMET!="") require_once('classes/Akismet.class.php');//akismet class 
require_once('classes/wordcloud.class.php');//tag generator
require_once('classes/class.phpmailer.php');//mailer
//end loading classes


require_once('common.php');//common functions
require_once('controller.php');//loads the value of the items/categories  if there's , and starts system variables
require_once('theme.php');//loads the selected theme, see define in config.php
require_once('menu.php');//menu functions generation and some functions that returns stats
require_once('sidebar.php');//sidebar functions generation
require_once('seo.php');//metas for the html, title,description, keywords
require_once('sitemap.php');//sitemap generation
require_once('twitter.php');//twitter post with bit.ly/j.mp

//special functions from the theme if they exists
if (file_exists(SITE_ROOT.'/themes/'.THEME.'/functions.php')){
	require_once(SITE_ROOT.'/themes/'.THEME.'/functions.php'); 
}


?>
