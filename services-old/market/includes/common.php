<?php
////////////////////////////////////////////////////////////
//Common Functions
////////////////////////////////////////////////////////////

function cG($name){//clean Get, to prevent mysql injection Get method
	if(get_magic_quotes_gpc()) $_GET[$name]=stripslashes($_GET[$name]); 
	$name=mysql_real_escape_string($_GET[$name]);
	return $name;
}
////////////////////////////////////////////////////////////
function cP($name){//clean post, to prevent mysql injection Post method and remove html
	if(get_magic_quotes_gpc()) $_POST[$name]=stripslashes($_POST[$name]); 
	$name=mysql_real_escape_string(strip_tags($_POST[$name]));
	return $name;
}
////////////////////////////////////////////////////////////
function cPR($name){//clean post, to prevent mysql injection Post method, but don't remove the htmltags
	if(get_magic_quotes_gpc()) $_POST[$name]=stripslashes($_POST[$name]); 
	$name=mysql_real_escape_string(ToHtml($_POST[$name]));
	return $name;
}
////////////////////////////////////////////////////////////
function ToHtml($string){//replaces for special things
	$string = str_replace ("&nbsp;"," ", $string);//problem with spaces
	$string = str_replace ("href=","rel=\"nofollow\" href=", $string);	//nofollow
	return $string;
} 
////////////////////////////////////////////////////////////
function friendly_url($url) {
	// everything to lower no spaces begin or end and replace accent characters
	$url = replace_accents(strtolower(trim($url)));
	// adding '-' for spaces and union characters
	$url = str_replace (array(' ', '&', '\r\n', '\n', '+',','), '-', $url);
	//delete and replace rest of special chars
	$url = preg_replace (array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/'),  array('', '-', ''), $url);
	return $url; //return the friendly url
}
////////////////////////////////////////////////////////////
function u($word){//returns the firendly word with html parsed
	return friendly_url(html_entity_decode($word,ENT_QUOTES,CHARSET));
}
////////////////////////////////////////////////////////////
function replace_accents($var){ //replace for accents catalan spanish and more
    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'); 
    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
    $var= str_replace($a, $b,$var);
    return $var; 
}  
////////////////////////////////////////////////////////////
function sqlOption($sql,$name,$option){//generates a select tag with the values specified on the sql, 2nd parameter name for the combo, , 3rd value selected if there's
	global $ocdb;
	$result =$ocdb->query($sql);//1 value needs to be the ID, second the Name, if there's more doens't work
	$sqloption= "<select name='".$name."' id='".$name."'>
				<option value='0'>Home</option>";
	while ($row=mysql_fetch_assoc($result)){
		$first=mysql_field_name($result, 0);
		$second=mysql_field_name($result, 1);

			if ($option==$row[$first]) { $sel="selected=selected";}
			$sqloption=$sqloption .  "<option ".$sel." value='".$row[$first]."'>" .$row[$second]. "</option>";
			$sel="";
	}
		$sqloption=$sqloption . "</select>";
		echo $sqloption;
}
////////////////////////////////////////////////////////////
function sqlOptionGroup($sql,$name,$option){//generates a select tag with the values specified on the sql, 2nd parameter name for the combo, , 3rd value selected if there's
	global $ocdb;
	$result =$ocdb->query($sql);//1 value needs to be the ID, second the Name, 3rd is the group
	//echo $sql;
	$sqloption= "<select name='".$name."' id='".$name."' onChange=\"validateNumber(this);\" lang=false ><option></option>";
	$lastLabel = "";
	while ($row=mysql_fetch_assoc($result)){
		$first=mysql_field_name($result, 0);
		$second=mysql_field_name($result, 1);
		$third= mysql_field_name($result,2);

		if($lastLabel!=$row[$third]){
			if($lastLabel!=""){
				$sqloption.="</optgroup>";
			}
			$sqloption.="<optgroup label='$row[$third]'>";
			$lastLabel = $row[$third];
		}

			if ($option==$row[$first]) { $sel="selected=selected";}
			$sqloption=$sqloption .  "<option ".$sel." value='".$row[$first]."'>" .$row[$second]. "</option>";
			$sel="";
	}
		$sqloption.="</optgroup>";
		$sqloption=$sqloption . "</select>";
		echo $sqloption;
}
////////////////////////////////////////////////////////////
function generatePassword ($length = PASSWORD_SIZE){
	  // start with a blank password
	  $password = "";
	  // define possible characters
	  $possible = "0123456789abcdefghijklmnopqrstuvwxyz"; 
	  // set up a counter
	  $i = 0; 
	  // add random characters to $password until $length is reached
	  while ($i < $length) { 
		// pick a random character from the possible ones
		$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);	
		// we don't want this character if it's already in the password
		if (!strstr($password, $char)) { 
		  $password .= $char;
		  $i++;
		}
	  }
	  // done!
	  return $password;
}
////////////////////////////////////////////////////////////
function setDate($L_date,$L_dateFormat=DATE_FORMAT){//sets a date in a format
	if(strlen($L_date)>0){
		$L_arrTemp = explode(" ",$L_date);
		$L_strDate = $L_arrTemp[0]; // 2007-07-21 year month day
		$L_arrDate = explode("-",$L_strDate);// split date 
		$L_strYear =  $L_arrDate[0];
		$L_strMonth = $L_arrDate[1];
		$L_strDay = $L_arrDate[2];
		
		if($L_dateFormat == 'yyyy-mm-dd'){//default
		    return $L_arrTemp[0];
        }
		elseif($L_dateFormat == "dd-mm-yyyy"){//day month year
			$returnDate = $L_strDay."-".$L_strMonth."-".$L_strYear;
			return $returnDate;
		}
		elseif($L_dateFormat == "mm-dd-yyyy"){//month day year
			$returnDate = $L_strMonth."-".$L_strDay."-".$L_strYear;
			return $returnDate;
		}
	}
	else return false;
}
////////////////////////////////////////////////////////////
function getTypeName($type){//get the type name
	if ($type==TYPE_OFFER){
		$type=TYPE_OFFER_NAME;
	}
	else $type=TYPE_NEED_NAME;
	
	return $type;
}
////////////////////////////////////////////////////////////
function getTypeNum($type){//get the type in number
	if ($type==TYPE_OFFER_NAME){
		$type=TYPE_OFFER;
	}
	else $type=TYPE_NEED;
	
	return $type;
}
////////////////////////////////////////////////////////////
function sendEmail($to,$subject,$body){//send email using smtp from gmail
	sendEmailComplete($to,$subject,$body,NOTIFY_EMAIL,SITE_NAME);
}
////////////////////////////////////////////////////////////
function sendEmailComplete($to,$subject,$body,$reply,$replyName){//send email using smtp from gmail
	$mail             = new PHPMailer();
	$mail->IsSMTP();
	
	//GMAIL config
	if (GMAIL==true){
		$mail->SMTPAuth   = true;                   // enable SMTP authentication
		$mail->SMTPSecure = "ssl";                  // sets the prefix to the server
		$mail->Host       = "smtp.gmail.com";       // sets GMAIL as the SMTP server
		$mail->Port       = 465;                    // set the SMTP port for the GMAIL server
		$mail->Username   = GMAIL_USER;                     // GMAIL username
		$mail->Password   = GMAIL_PASS;                     // GMAIL password
    }
	
	$mail->From       = NOTIFY_EMAIL;
	$mail->FromName   = "no-reply ".SITE_NAME;
	$mail->Subject    = $subject;
	$mail->MsgHTML($body);
	
	$mail->AddReplyTo($reply,$replyName);//they answer here
	$mail->AddAddress($to,$to);
	$mail->IsHTML(true); // send as HTML

	if(!$mail->Send()) {//to see if we return a message or a value bolean
	  echo "Mailer Error: " . $mail->ErrorInfo;
	} else return false;
	 // echo "Message sent! $to";	
}
////////////////////////////////////////////////////////////
function encode_str ($input){//converts the input into Ascii HTML, to ofuscate a bit
    for ($i = 0; $i < strlen($input); $i++) {
         $output .= "&#".ord($input[$i]).';';
    }
    //$output = htmlspecialchars($output);//uncomment to escape sepecial chars
    return $output;
}
////////////////////////////////////////////////////////////
function mathCaptcha(){//generates a captcha for the form
	$first_number=mt_rand(1, 94);//first operation number
	$second_number=mt_rand(1, 5);//second operation number
	
	$_SESSION["mathCaptcha"]=($first_number+$second_number);//operation result
	
	$operation=" <b>".encode_str($first_number ." + ". $second_number)."</b>?";//operation codifieds
	
	echo T_HOW_MUCH.$operation;			
}
////////////////////////////////////////////////////////////
function isEmail($email){//check that the email is correct
	$pattern="/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/";
	if(preg_match($pattern, $email) > 0) return true;
	else return false;
}

////////////////////////////////////////////////////////////
function jsRedirect($url){//simple JavaScript redirect
	echo "<script language='JavaScript' type='text/javascript'>location.href='$url';</script>";
}
////////////////////////////////////////////////////////////
function alert($msg){//simple JavaScript alert
	echo "<script language='JavaScript' type='text/javascript'>alert('$msg');</script>";
}
////////////////////////////////////////////////////////////
function removeRessource($_target) {//remove the done file
    //file?
    if( is_file($_target) ) {
        if( is_writable($_target) ) {
            if( @unlink($_target) ) {
                return true;
            }
        }
        return false;
    }
    //dir recursive
    if( is_dir($_target) ) {
        if( is_writeable($_target) ) {
            foreach( new DirectoryIterator($_target) as $_res ) {
                if( $_res->isDot() ) {
                    unset($_res);
                    continue;
                }
                if( $_res->isFile() ) {
                    removeRessource( $_res->getPathName() );
                } elseif( $_res->isDir() ) {
                    removeRessource( $_res->getRealPath() );
                }
                unset($_res);
            }
            if( @rmdir($_target) ) {
                return true;
            }
        }
        return false;
    }
}
////////////////////////////////////////////////////////////
function hackerDefense(){
	// begin hacker defense - Thanks Kreuznacher | wurdzwurk
	foreach ($_POST as $secvalue) {
		if ((eregi("<[^>]*script.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*object.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*iframe.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*applet.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*window.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*document.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*cookie.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*meta.*\"?[^>]*>", $secvalue)) ||
		//(eregi("<[^>]*style.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*alert.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*form.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*php.*\"?[^>]*>", $secvalue)) ||
		//(eregi("<[^>]*<?.*\"?[^>]*>", $secvalue)) ||
		(eregi("<[^>]*img.*\"?[^>]*>", $secvalue))) {
			header("Location: ".SITE_URL."/content/error-msg.php?msg=1");
			die (T_ERROR_CODE);
		}
	}
	// end hacker defense 	
}
////////////////////////////////////////////////////////////
function getPrice($amount){//returns the price for the item in the correct format
	return str_replace(array("AMOUNT","CURRENCY"),array($amount,CURRENCY),CURRENCY_FORMAT);
	//return $amount;
}
////////////////////////////////////////////////////////////
function isSpam($name,$email,$comment){//return if something is spam or not using akismet, and checking the spam list
	global $ocdb;
	$res=$ocdb->getValue("SELECT idPost FROM ".TABLE_PREFIX."posts p where isAvailable=2  and email='$email' LIMIT 1","none");//check spam tags
	if ($res==false){//nothing found
		if (AKISMET!=""){
			$akismet = new Akismet(SITE_URL ,AKISMET);//change this! or use defines with that name!
			$akismet->setCommentAuthor($name);
			$akismet->setCommentAuthorEmail($email);
			$akismet->setCommentContent($comment);
			return $akismet->isCommentSpam();
		}
		else return false;//we return is not spam since we don't have the api :(	
	}
	else return true;//ohohoho SPAMMER!
	
}
////////////////////////////////////////////////////////////
function isInSpamList($ip){//return is was taged as spam (in /manage is where we tag)
	global $ocdb;
	$res=$ocdb->getValue("SELECT idPost FROM ".TABLE_PREFIX."posts p where isAvailable=2  and ip='$ip' LIMIT 1","none");
	if ($res==false) return false;//nothing found
	else return true;//ohohoho SPAMMER!
} 

////////////////////////////////////////////////////////////
function deleteCache(){//delete cache
	$cache=new fileCache(CACHE_EXPIRE,CACHE_DATA_FILE);
	$cache->deleteCache(0);//deletes everything
	unset ($cache);
}
////////////////////////////////////////////////////////////
function advancedSearchForm(){//used in  /search and in index.php when an advanced search is done
	global $currentCategory;?>	
	<form action="<?php echo SITE_URL;?>" method="get"><table>
	<tr><td><?php echo T_CATEGORY;?>:</td><td> 
	<?php 
	$query="SELECT friendlyName,name,(select name from ".TABLE_PREFIX."categories where idCategory=C.idCategoryParent) FROM ".TABLE_PREFIX."categories C order by idCategoryParent";
	sqlOptionGroup($query,"category",$currentCategory);
	?></td></tr>
	<tr><td><?php echo T_TYPE;?>:</td><td>
		<select id="type" name="type">
			<option value="<?php echo TYPE_OFFER;?>"><?php echo TYPE_OFFER_NAME;?></option>
			<option value="<?php echo TYPE_NEED;?>"><?php echo TYPE_NEED_NAME;?></option>
		</select>
	</td></tr>
	<tr><td><?php echo T_TITLE;?>:</td><td><input type="text" name="title" value="<?php echo cG("title");?>" /></td></tr>
	<tr><td><?php echo T_DESCRIPTION;?>:</td><td><input type="text" name="desc" value="<?php echo cG("desc");?>" /></td></tr>
	<tr><td><?php echo T_PRICE;?>:</td><td><input type="text" name="price" value="<?php echo cG("price");?>" /></td></tr>
	<tr><td><?php echo T_PLACE;?>:</td><td><input type="text" name="place" value="<?php echo cG("place");?>" /></td></tr>
	<tr><td><?php echo T_SORT;?>:</td>
		<td>
			<select name="sort">
				<option></option>
				<option value="price-desc" <?php if(cG("sort")=="price-desc")  echo "selected=selected";?> ><?php echo T_PRICE;?> - Desc</option>
				<option value="price-asc" <?php if(cG("sort")=="price-asc")  echo "selected=selected";?> ><?php echo T_PRICE;?> - Asc</option>
			</select>
		</td></tr>
	<tr><td colspan=2 align="right"><input type="submit" value="<?php echo T_SEARCH;?>" /></td></tr>
	</table></form><?php 
}
////////////////////////////////////////////////////////////
function rssReader($url,$maxItems=15,$cache,$begin="",$end=""){//read RSS from the url and cache it
    $cache= (bool) $cache;
    if ($cache){
        $cacheRSS= new fileCache(CACHE_EXPIRE,CACHE_DATA_FILE);//seconds and path
        $out = $cacheRSS->cache($url);//getting values from cache
    }else $out=false;
    
    if (!$out) {	//no values in cache
        $rss = simplexml_load_file($url);
        $i=0;
        if($rss){
            $items = $rss->channel->item;
            foreach($items as $item){
                if($i==$maxItems){
                    if ($cache) $cacheRSS->cache($url,$out);//save cache	
                    return $out; 
                }
                else $out.=$begin.'<a href="'.$item->link.'" target="_blank" >'.$item->title.'</a>'.$end;
                $i++;
            }//for each
        }//if rss      		
    }
    return $out;
}
////////////////////////////////////////////////////////////
function itemURL($idPost,$category,$type,$title,$subcat=""){//returns de url for the item, if you change this be aware that you need to change it in the .htaccess as well.
   if(FRIENDLY_URL){
        if ($subcat!="" && $category!=$subcat) $url='/'.$subcat.'/'.$category.'/'.friendly_url($title).'-'.$idPost.'.htm';
        else $url='/'.$category.'/'.friendly_url($title).'-'.$idPost.'.htm'; // old= "/$idPost/$type/$category/".friendly_url($title);
   }
   else $url="/item.php?item=$idPost&type=$type&category=$category&title=".friendly_url($title);//no friendly url activated
   return $url;
}
////////////////////////////////////////////////////////////
function catURL($category,$subcat=""){//returns de url for the category, if you change this be aware that you need to change it in the .htaccess as well.
   if(FRIENDLY_URL){
       if ($subcat!="" && $category!=$subcat)  $url='/'.$subcat.'/'.$category.'/'; 
       else  $url='/'.$category.'/';  
   }
   else $url='/?category='.$category;//no friendly url activated
   return $url;
}
////////////////////////////////////////////////////////////
function typeURL($type,$category){//returns de url for the type, if you change this be aware that you need to change it in the .htaccess as well.
   if (!isset($category)) $category="all"; 
   if (FRIENDLY_URL) $url='/'.getTypeName($type).'/'.$category;
   else $url='/?category='.$category.'&type='.$type;
   return $url;
}
////////////////////////////////////////////////////////////
function newURL(){//returns de url to post new item
   if (FRIENDLY_URL) $url='/'.u(T_NEW_ITEM).'.htm';
   else $url='/content/item-new.php';
   return $url;
}
////////////////////////////////////////////////////////////
function mapURL(){//returns de url for the map
   if (FRIENDLY_URL) $url=u(T_MAP).'.htm';
   else $url='content/map.php';
   return $url;
}
////////////////////////////////////////////////////////////
function contactURL(){//returns de url for the contact
   if (FRIENDLY_URL) $url=u(T_CONTACT).'.htm';
   else $url='content/contact.php';
   return $url;
}
////////////////////////////////////////////////////////////
function check_images_form(){//get values by reference to allow change them. Used in new item and manage item
    //image check 
    $image_check=1;
    if (MAX_IMG_NUM>0){	//image upload active if there's more than 1
		$types=split(",",IMG_TYPES);//creating array with the allowed types print_r ($types);
		
		for ($i=1;$i<=MAX_IMG_NUM && is_numeric($image_check);$i++){//loop for all the elements in the form
		    
		    if (file_exists($_FILES["pic$i"]['tmp_name'])){//only for uploaded files

			    $imageInfo = getimagesize($_FILES["pic$i"]["tmp_name"]);
			    $file_mime = strtolower(substr(strrchr($imageInfo["mime"], "/"), 1 ));//image mime
			    $file_ext  = strtolower(substr(strrchr($_FILES["pic$i"]["name"], "."), 1 ));//image extension

			    if ($_FILES["pic$i"]['size'] > MAX_IMG_SIZE) {//control the size
				     $image_check=T_PICTURE." $i ".T_UPLOAD_MAX." ".(MAX_IMG_SIZE/1000000)."Mb";				
			    }
			    elseif (!in_array($file_mime,$types) || !in_array($file_ext,$types)){//the size is right checking type and extension
				     $image_check=T_PICTURE." $i no ".T_FORMAT." ".IMG_TYPES;				
			    }//end else		
			    
			    $image_check++;

			}//end if existing file
		}//end loop		
	}//end image check
	return $image_check;	
}

////////////////////////////////////////////////////////////
function upload_images_form($idPost,$title){//upload image files from the form.  Used in new item and manage item
    global $ocdb;
    //images upload and resize
	if (MAX_IMG_NUM>0){	
		//create dir for the images
		$up_path=IMG_UPLOAD_DIR.$idPost; 
		umask(0000);
		mkdir($up_path, 0755);//create folder for item
		$needFolder=false;//to know if it's needed the folder
		//upload images
		for ($i=1;$i<=MAX_IMG_NUM;$i++){
		    if (file_exists($_FILES["pic$i"]['tmp_name'])){//only for uploaded files
			    $file_name = $_FILES["pic$i"]['name'];
			    $file_name = friendly_url($title).'_'.$i. strtolower(substr($file_name, strrpos($file_name, '.')));
			    $up_file=$up_path."/".$_FILES["pic$i"]['name'];
			
			    if (move_uploaded_file($_FILES["pic$i"]['tmp_name'],$up_file)){ //file uploaded
				    //resizing images
				    $thumb=new thumbnail($up_file); 
				    $thumb->size_width(IMG_RESIZE);	   // set width  for thumbnail			   
				    $thumb->save($up_path."/".$file_name);
				    unset($thumb);
				    //create thumb
				    $thumb=new thumbnail($up_file); 
				    $thumb->size_width(IMG_RESIZE_THUMB);	   // set biggest width for thumbnail				   
				    $thumb->save($up_path."/thumb_$file_name");
				    unset($thumb);
				    @unlink($up_file);//delete old file	
				    $needFolder=true;
				    //add to db
				    $ocdb->insert(TABLE_PREFIX."postsimages (idPost,Filename)","$idPost,'$file_name'");
			    }
			}//end if file exists
		}
		if (!$needFolder) @rmdir($up_path);//the folder is not needed no files uploaded
	}
	//end images
}
////////////////////////////////////////////////////////////
function mediaPostDesc($the_content){//from a description add the media
//using http://www.robertbuzink.nl/journal/2006/11/23/youtube-brackets-wordpress-plugin/
    if (VIDEO){
        $stag = "[youtube=http://www.youtube.com/watch?v=";
        $etag = "]";
        $spos = strpos($the_content, $stag);
        if ($spos !== false){
            $epos = strpos($the_content, $etag, $spos);
            $spose = $spos + strlen($stag);
            $slen = $epos - $spose;
            $file  = substr($the_content, $spose, $slen);    
			//youtube
            $tags = '<object width="425" height="350">
                    <param name="movie" value="'.$file.'"></param>
                    <param name="wmode" value="transparent" ></param>
                    <embed src="http://www.youtube.com/v/'. $file.'" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed>
                    </object>';    
            $new_content = substr($the_content,0,$spos);
            $new_content .= $tags;
            $new_content .= substr($the_content,($epos+1));
           
            if ($epos+1 < strlen($the_content)) {//reciproco
                $new_content = mediaPostDesc($new_content);
            }
            return $new_content;
        }
        else return $the_content;
    }
    else return $the_content;
}
?>
