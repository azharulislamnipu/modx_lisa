<?php
 /**
  * Part of Airtight Interactive gallery management package.
  *
  * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
  * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
  *
  * @package svManager
  * @author Jack Hardie {@link http://www.jhardie.com}
  * @version 1.6.3 build 091231
  * @copyright Copyright (c) 2007 - 2009, Airtight Interactive
  */
 
 /**
  * AvPro page
  *
  * @package svManager
  */
  class PvProPage extends Page
  {
   /**
    * @var string overrides some of html in parent
    */
    var $customHeadHtml = '';
   /**
    * constructs ImagesPage class
    * @param string contains text for html <title></title> tags
    * @param string html id for body tag
    */
    function PvProPage()
    {
      parent::Page('svManager &ndash; pro settings', 'pro', 'navpro postcardviewer customize');
    }

   /**
    * get html for images page
    * Note that some class names are significant for manager.js
    *
    * @access public
    * @return string html
    * @param string gallery reference number
    * @param array image data
    */
    function getProHtml(&$gallery)
    {
      return '<p style="margin: 0; padding: 0">There are no <em>Pro</em> settings for PostcardViewer</p>';
    }
  }
?>
