<?php
//return '';
global $modx;
global $base_path;
global $base_url;
//$new_path = str_replace('home/', '', $base_path);

$new_path = '';
$url = $_SERVER['REQUEST_URI'];
$url_pos_archon = strrpos($url, "personal-profile");
$url_pos_archousa = strrpos($url, "archousa-profile");

if(!($url_pos_archon !== false || $url_pos_archousa !== false)){
    return false;
}

/*
 * Local
 */
$pos = strrpos(__DIR__, "htdocs");
if($pos !== false){
    $new_path = substr(__DIR__, 0, $pos) . 'htdocs/';
}

/**
 * Server
 */
/*$pos = strrpos(__DIR__, "apache");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'apache/';
}*/

if($new_path) {
	ob_start();
	if($modx->userLoggedIn()){
		define('LoggedIn', true);

		include $new_path . "includes/functions.php";

		include $new_path . "includes/profile_popup/index.php";
	}
	return ob_get_clean();
}
return false;