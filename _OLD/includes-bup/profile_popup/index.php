<?php
//$modx->getPlaceholder('archousaid');
if(!(defined('LoggedIn') && LoggedIn)){
    return false;
}

$pp_dir = $new_path . "includes/profile_popup/";

//$db = get_db();
$all_prefix = SPP_ARCHON_DB::get_all_prefix();
$all_suffix = SPP_ARCHON_DB::get_all_suffix();
$all_designation = SPP_ARCHON_DB::get_all_hos_desig();
$all_states = SPP_ARCHON_DB::get_all_state();

$all_prefix_html = create_suffix_prefix_from_array(array_column($all_prefix['data'], 'PREFIX'));
$all_suffix_html = create_suffix_prefix_from_array(array_column($all_suffix['data'], 'SUFFIX'));
//$all_designation_html = create_list_from_array($all_prefix);
//$all_states_html = create_list_from_array($all_prefix);

$main_url = $_SERVER['REQUEST_URI'];
if(strpos($main_url, 'personal-profile') !== false) {
    include $pp_dir . 'archon.php';
} elseif (strpos($main_url, 'archousa-profile') !== false){
    include $pp_dir . 'archousa.php';
}

if(!empty($_GET['edit_arc']) && $_GET['edit_arc'] == 1){ ?>
    <script>
        var popup_open = true;
    </script>
<?php }