<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Customize page
 *
 * @package svManager
 */
class TvCustomizePage extends Page
{
 /**
  * @var string overrides some of head content in Page
  */
  var $customHeadHtml = '
       <link href="colorjack/plugin.css" rel="stylesheet" type="text/css" />
       <style type="text/css">
         #plugin {
           color: #FFFFFF;
           background-color: #999999;
         }
         #H {
           border-color: #999999;
         }
         #plugCLOSE {
           color: #FFFFFF;
           background-color: #999999;
         }
       </style>
       <script src="scripts/mootools-core.js" type="text/javascript"></script>
       <script src="scripts/mootools-more.js" type="text/javascript"></script>
       <script src="colorjack/plugin.js" type="text/javaScript"></script>
       <script src="scripts/customize.js" type="text/javascript"></script>
       <script src="scripts/mooformcheck.js" type="text/javascript"></script>
';
 
 /**
  * constructs TvCustomizePage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function TvCustomizePage()
  {
    parent::Page('svManager &ndash; customize', 'tvcustomize', 'navcustomize tiltviewer customize');
  }

 /**
  * get html for customize page
  *
  * Note hack in conditional comments from
  * @link http://www.hedgerwow.com/360/bugs/css-select-free.html
  * to prevent select elements showing above the colorpicker in IE6Win
  * @access public
  * @return string html
  * @param object gallery set
  * @param array gallery attributes
  */
  function getCustomizeHtml(&$gallerySet, &$gallery)
  {
    $preferences = $gallery->getPreferences();
    $rootUrl = $gallery->getRootUrl();
    $galleriesPrefs = $gallerySet->getGalleriesPrefs();
    $galleryTitle = htmlspecialchars($preferences['galleryTitle'], ENT_QUOTES, 'UTF-8');
    $serverName = 'http://'.$_SERVER['SERVER_NAME'].'/';
    $svManagerUrl = substr(dirname($_SERVER['PHP_SELF']), 1);
    $frameColor = strtoupper(substr($preferences['frameColor'], 2, strlen($preferences['frameColor']) - 2));
    $backColor = strtoupper(substr($preferences['backColor'], 2, strlen($preferences['backColor']) - 2));
    $bkgndInnerColor = strtoupper(substr($preferences['bkgndInnerColor'], 2, strlen($preferences['bkgndInnerColor']) - 2));
    $bkgndOuterColor = strtoupper(substr($preferences['bkgndOuterColor'], 2, strlen($preferences['bkgndOuterColor']) - 2));
    $useReloadButtonChecked = ($preferences['useReloadButton'] == true) ? 'checked="checked"' : '';
    $useFlickrChecked = ($preferences['useFlickr'] == true) ? 'checked="checked"' : '';
    $useXmlChecked = ($preferences['useFlickr'] != true) ? 'checked="checked"' : '';
    $tagModeAnyChecked = ($preferences['flickrTagMode'] != 'all') ? 'checked="checked"' : '';
    $tagModeAllChecked = ($preferences['flickrTagMode'] == 'all') ? 'checked="checked"' : '';
    $showTakenByTextButtonChecked = ($preferences['flickrShowTakenByText'] == true) ? 'checked="checked"' : '';
    $showFlipButtonChecked = ($preferences['showFlipButton'] == true) ? 'checked="checked"' : '';
    $relativePathChecked = '';
    $absolutePathChecked = '';
    $httpPathChecked = '';
    switch (strtoupper($preferences['urlType']))
    {
      case 'ABSOLUTE' :
      $absolutePathChecked = 'checked="checked"';
      break;
      case 'HTTP' :
      $httpPathChecked = 'checked="checked"';
      break;
      default :
      $relativePathChecked = 'checked="checked"';
    }
    
$html = <<<EOD

<div id="plugin" class="selectfree">
 <div id="plugHEX" onmousedown="stop=0; setTimeout('stop=1',100);">F1FFCC</div>
 <!-- Note change of function name from toggle to avoid namespace conflict -->
 <div id="plugCLOSE" onmousedown="toggleDisplay('plugin')">X</div><br />
 <div id="SV" onmousedown="HSVslide('SVslide','plugin',event)" title="Saturation + Value">
  <div id="SVslide" style="TOP: -4px; LEFT: -4px;"><br /></div>
 </div>
 <form id="H" action="" onmousedown="HSVslide('Hslide','plugin',event)" title="Hue">
  <div id="Hslide" style="TOP: -7px; LEFT: -8px;"><br /></div>
  <div id="Hmodel"></div>
 </form>
 <!--[if lte IE 6.5]><iframe></iframe><![endif]-->
</div>
<form class="public" action = "{$_SERVER['PHP_SELF']}" id="customizeform" method="post">
  <div class="elementwrap">
    <h3 class="togglers">Basic settings</h3>
    <div class="accordionElements">
      <table id="settings1" cellspacing="0">
        <tr id="titleentry">
          <td class="label"><label for="title">Gallery title</label></td><td><input type="text" id="title" tabindex="1" class="text" name="galleryTitle" value="{$galleryTitle}" /></td>
        </tr>
      </table>
      <table id="settings2" cellspacing="0">
        <tr id="framecolorentry">
          <td class="label"><label for="framecolor">Frame color</label></td><td><input type="text" id="framecolor" tabindex="3" class="colorpicker" name="frameColor" value="{$frameColor}" /></td>
        </tr>
        <tr id="backgroundcolorentry">
          <td class="label"><label for="backgroundcolor">Background color</label></td><td><input type="text" class="colorpicker" id="backgroundcolor" tabindex="4" name="backColor" value="{$backColor}" /></td>
        </tr>
        <tr id="bkgndinnercolorentry">
          <td class="label"><label for="bkgndinnercolor">BG inner color</label></td><td><input type="text" class="colorpicker" id="bkgndinnercolor" tabindex="5" name="bkgndInnerColor" value="{$bkgndInnerColor}" /></td>
        </tr>
        <tr id="bkgndoutercolorentry">
          <td class="label"><label for="bkgndoutercolor">BG outer color</label></td><td><input type="text" class="colorpicker" id="bkgndoutercolor" tabindex="6" name="bkgndOuterColor" value="{$bkgndOuterColor}" /></td>
        </tr>
        <tr id="maxjpgsizeentry">
          <td class="label"><label for="maxjpgsize">Max image size, px</label></td><td><input type="text" id="maxjpgsize" tabindex="7" class="text" name="maxJPGSize" value="{$preferences['maxJPGSize']}" /></td>
        </tr>
        <tr id="linklabelentry">
          <td class="label"><label for="linklabel">Link label</label></td><td><input type="text" id="linklabel" tabindex="8" class="text" name="linkLabel" value="{$preferences['linkLabel']}" /></td>
        </tr>
        <tr id="usereloadbuttonentry">
          <td class="label"><label for="usereloadbutton">Reload button</label></td><td><input type="checkbox" class="checkbox" id="usereloadbutton" tabindex="9" {$useReloadButtonChecked} name="useReloadButton" /></td>
        </tr>
    
      </table>
    
      <table id="settings3" cellspacing="0">
        <tr id="rowsentry">
          <td class="label"><label for="rows">Rows</label></td><td colspan="2"><input type="text" class="text" id="rows" tabindex="10" name="rows" value="{$preferences['rows']}" /></td>
        </tr>
        <tr id="columnsentry">
          <td class="label"><label for="columns">Columns</label></td><td colspan="2"><input type="text" class="text" id="columns" tabindex="11" name="columns" value="{$preferences['columns']}" /></td>
        </tr>
        <tr id="useflickrentry">
          <td class="label">Gallery type</td><td><input id="xmlgallery" tabindex="12" type="radio" name="useFlickr" value="false" {$useXmlChecked} /><label for="xmlgallery">&nbsp;XML&nbsp;&nbsp;</label></td><td><input id="flickrgallery" tabindex="13" type="radio" name="useFlickr" value="true" {$useFlickrChecked} /><label for="flickrgallery">&nbsp;Flickr</label></td>
        </tr>
        <tr id="flickruseridentry">
          <td class="label"><label for="flickruserid">Flickr user ID</label></td><td colspan="2"><input type="text" class="text" id="flickruserid" tabindex="14" name="flickrUserId" value="{$preferences['flickrUserId']}" /></td>
        </tr>
        <tr id="flickrtagsentry">
          <td class="label"><label for="flickrtags">Flickr tags</label></td><td colspan="2"><input type="text" class="text" id="flickrtags" tabindex="15" name="flickrTags" value="{$preferences['flickrTags']}" /></td>
        </tr>
        <tr id="flickrtagmodeentry">
          <td class="label">Flickr tag mode</td><td><input id="tagmodeany" tabindex="16" type="radio" name="flickrTagMode" value="any" {$tagModeAnyChecked} /><label for="tagmodeany">&nbsp;Any&nbsp;&nbsp;</label></td><td><input id="tagmodeall" tabindex="17" type="radio" name="flickrTagMode" value="all" {$tagModeAllChecked} /><label for="tagmodeall">&nbsp;All</label></td>
        </tr>
        <tr id="showtakenbytextentry">
          <td class="label"><label for="showtakenbytext">Flickr taken by</label></td><td colspan="2"><input type="checkbox" class="checkbox" id="showtakenbytext" tabindex="18" {$showTakenByTextButtonChecked} name="flickrShowTakenByText" /></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="elementwrap" id="advanced">
    <h3 class="togglers">Advanced settings</h3>
    <div class="accordionElements">
      <table id="settingspaths" cellspacing="0">
        <tr id="absurlentry">
          <td class="label"><label for="absurl">Path from web root</label></td><td><input type="text" id="absurl" tabindex="2" class="text" name="absurl" value="{$rootUrl}" /></td>
        </tr>
        <tr id="urltypeentry">
            <td class="label">Save paths as</td>
            <td><input id="relativepath" tabindex="12" type="radio" name="urlType" value="RELATIVE" {$relativePathChecked} /><label for="relativepath">Relative</label>
            <input id="absolutepath" tabindex="13" type="radio" name="urlType" value="ABSOLUTE" {$absolutePathChecked} /><label for="absolutepath">Absolute</label>
            <input id="httppath" tabindex="13" type="radio" name="urlType" value="HTTP" {$httpPathChecked} /><label for="httppath">http</label></td>
          </tr>
      </table>
    </div>
  </div>
  <div id="submitinputs">
    <input type="hidden" name="customizesubmitted" value="true" /><input type="submit" name="submit" class="formbutton" value="Update" />
  </div>  
</form>
EOD;
    return $html;
  }

}
