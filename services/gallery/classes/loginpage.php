<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Admin page
 *
 * @package svManager
 */
class LoginPage extends Page
{
  /**
  * constructs LoginPage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function LoginPage($title, $bodyId='')
  {
    parent::Page($title, $bodyId);
  }
   /**
  * Returns login page content
  *
  * @return string
  * @param boolean true if form has been submitted
  * @access public
  */
  function getLoginContent($posted)
  {
    $message = ($posted) ? 'Incorrect user name or password.' : '&nbsp;';
    $loginContent = <<<EOD
    <body id="{$this->bodyId}">
	  <div id="wrapper">
    <div id="header">
      <p>&nbsp;</p>
    </div>
    <div id="content">
    <div id="welcome">
    <h1><img src="img/logologon.gif" alt="Airtight Interactive" width="159" height="28" /></h1>
    <p>Easy gallery management from <a href="http://www.simpleviewer.net" rel="external" >SimpleViewer.net</a>.</p>
    </div>
    <div id="loginform">    
      <p class="error">{$message}</p>
      <form action = "{$_SERVER['PHP_SELF']}" method="post">
        <p><label for="user" style="display: block">User name: </label><input class="text" type="text" name="user" id="user" /></p>
        <p><label for="password" style="display: block">Password: </label><input class="text" type="password" name="password" id="password" /></p>
        <p><input type="hidden" name="loginsubmitted" value="true" /><input type="submit" value="Login" class="formbutton" /></p>
        <p style="padding-top: 10px"><a href="http://www.simpleviewer.net/svmanager/documentation.html#password" title="link to svManager documentation" rel="external_blank">Lost your password?</a></p>
      </form>
    </div>
EOD;
    return $loginContent;
  }
}
