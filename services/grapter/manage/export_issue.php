<?php 
//***********
// CSV Writer
//***********
require_once("../../dwzExport/CsvExport.php");
?><?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("iss_cd", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// Start trigger
$formValidation1 = new tNG_FormValidation();
$formValidation1->addField("iss_cd", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation1);
// End trigger

// Start trigger
$formValidation2 = new tNG_FormValidation();
$formValidation2->addField("iss_cd", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation2);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsIssueNames = "SELECT iss_id, iss_nm, iss_cd, iss_deadline, iss_status FROM grapter_issues WHERE iss_id > 22";
$rsIssueNames = mysql_query($query_rsIssueNames, $sigma_modx) or die(mysql_error());
$row_rsIssueNames = mysql_fetch_assoc($rsIssueNames);
$totalRows_rsIssueNames = mysql_num_rows($rsIssueNames);

$colname_rsArticles = "-1";
if (isset($_GET['iss_cd'])) {
  $colname_rsArticles = $_GET['iss_cd'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArticles = sprintf("SELECT art_id, iss_cd, iss_nm, dept_cd, dept_nm, event_date, event_loc, art_title, art_file, date_submit, BOULENAME, email, HOMEPHONE, L2R_FULLNAME FROM vw_articles_all_v2 WHERE iss_cd = %s", GetSQLValueString($colname_rsArticles, "text"));
$rsArticles = mysql_query($query_rsArticles, $sigma_modx) or die(mysql_error());
$row_rsArticles = mysql_fetch_assoc($rsArticles);
$totalRows_rsArticles = mysql_num_rows($rsArticles);

$colname_rsImages = "-1";
if (isset($_GET['iss_cd'])) {
  $colname_rsImages = $_GET['iss_cd'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsImages = sprintf("SELECT photo_id, photo_file, ph_caption, art_id, art_title, iss_cd, dept_cd, CHAPTERID, date_submit, L2R_FULLNAME_NORMAL FROM vw_photos_all WHERE iss_cd = %s", GetSQLValueString($colname_rsImages, "text"));
$rsImages = mysql_query($query_rsImages, $sigma_modx) or die(mysql_error());
$row_rsImages = mysql_fetch_assoc($rsImages);
$totalRows_rsImages = mysql_num_rows($rsImages);

// Make a custom transaction instance
$customTransaction = new tNG_custom($conn_sigma_modx);
$tNGs->addTransaction($customTransaction);
// Register triggers
$customTransaction->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Custom1");
$customTransaction->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$customTransaction->registerTrigger("END", "Trigger_Default_Redirect", 99, "export_issue.php");
// Add columns
$customTransaction->addColumn("iss_cd", "STRING_TYPE", "POST", "iss_cd");
// End of custom transaction instance

// Make a custom transaction instance
$customTransaction1 = new tNG_custom($conn_sigma_modx);
$tNGs->addTransaction($customTransaction1);
// Register triggers
$customTransaction1->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Custom2");
$customTransaction1->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation1);
$customTransaction1->registerTrigger("END", "Trigger_Default_Redirect", 99, "export_issue.php");
// Add columns
$customTransaction1->addColumn("iss_cd", "STRING_TYPE", "GET", "iss_cd");
// End of custom transaction instance

// Make a custom transaction instance
$customTransaction2 = new tNG_custom($conn_sigma_modx);
$tNGs->addTransaction($customTransaction2);
// Register triggers
$customTransaction2->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Custom3");
$customTransaction2->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation2);
$customTransaction2->registerTrigger("END", "Trigger_Default_Redirect", 99, "export_issue.php");
// Add columns
$customTransaction2->addColumn("iss_cd", "STRING_TYPE", "POST", "iss_cd");
// End of custom transaction instance

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rscustom = $tNGs->getRecordset("custom");
$row_rscustom = mysql_fetch_assoc($rscustom);
$totalRows_rscustom = mysql_num_rows($rscustom);
?>
<?php
//************************************
//  http://www.DwZone-it.com
//  Import Export Tools - CSV Writer
//  Version 1.1.9
//************************************
$dwzCsv_rsArticles = new dwzCsvExport();
$dwzCsv_rsArticles->Init();
$dwzCsv_rsArticles->SetFileName("Articles_Export.csv");
$dwzCsv_rsArticles->SetNumberOfRecord("ALL");
$dwzCsv_rsArticles->SetStartOn("GET", "KT_Custom1");
$dwzCsv_rsArticles->SetFieldSeparator(",");
$dwzCsv_rsArticles->SetFieldLabel("true");
$dwzCsv_rsArticles->SetRecordset($rsArticles);
$dwzCsv_rsArticles->SetEncloseField("DA");
$dwzCsv_rsArticles->addItem("ARTICLE_ID", "art_id", "String");
$dwzCsv_rsArticles->addItem("ISSUE_CODE", "iss_cd", "String");
$dwzCsv_rsArticles->addItem("ISSUE_NAME", "iss_nm", "String");
$dwzCsv_rsArticles->addItem("DEPARTMENT_CODE", "dept_cd", "String");
$dwzCsv_rsArticles->addItem("DEPARTMENT_NAME", "dept_nm", "String");
$dwzCsv_rsArticles->addItem("EVENT_DATE", "event_date", "String");
$dwzCsv_rsArticles->addItem("EVENT_LOCATION", "event_loc", "String");
$dwzCsv_rsArticles->addItem("ARTICLE_TITLE", "art_title", "String");
$dwzCsv_rsArticles->addItem("ARTICLE_FILE", "art_file", "String");
$dwzCsv_rsArticles->addItem("SUBMIT_DATE", "date_submit", "String");
$dwzCsv_rsArticles->addItem("BOULE_NAME", "BOULENAME", "String");
$dwzCsv_rsArticles->addItem("FULL_NAME", "L2R_FULLNAME2", "String");
$dwzCsv_rsArticles->addItem("EMAIL", "email", "String");
$dwzCsv_rsArticles->addItem("HOME_PHONE", "HOMEPHONE", "String");
$dwzCsv_rsArticles->Execute();
//***********************
// Csv Writer
//***********************
?>
<?php
//************************************
//  http://www.DwZone-it.com
//  Import Export Tools - CSV Writer
//  Version 1.1.9
//************************************
$dwzCsv_rsImages = new dwzCsvExport();
$dwzCsv_rsImages->Init();
$dwzCsv_rsImages->SetFileName("Images_Export.csv");
$dwzCsv_rsImages->SetNumberOfRecord("ALL");
$dwzCsv_rsImages->SetStartOn("GET", "KT_Custom2");
$dwzCsv_rsImages->SetFieldSeparator(",");
$dwzCsv_rsImages->SetFieldLabel("true");
$dwzCsv_rsImages->SetRecordset($rsImages);
$dwzCsv_rsImages->SetEncloseField("DA");
$dwzCsv_rsImages->addItem("ISSUE_CODE", "iss_cd", "String");
$dwzCsv_rsImages->addItem("DEPT_CODE", "dept_cd", "String");
$dwzCsv_rsImages->addItem("ARTICLE_ID", "art_id", "String");
$dwzCsv_rsImages->addItem("ARTICLE_TITLE", "art_title", "String");
$dwzCsv_rsImages->addItem("PHOTO_ID", "photo_id", "String");
$dwzCsv_rsImages->addItem("IMAGE_NAME", "photo_file", "String");
$dwzCsv_rsImages->addItem("CAPTION", "ph_caption", "String");
$dwzCsv_rsImages->addItem("BOULE_NAME", "CHAPTERID", "String");
$dwzCsv_rsImages->addItem("SUBMITTED_BY", "L2R_FULLNAME_NORMAL", "String");
$dwzCsv_rsImages->addItem("DATE_SUBMITTED", "date_submit", "String");
$dwzCsv_rsImages->Execute();
//***********************
// Csv Writer
//***********************
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default_2col.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/grapter_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width250" style="margin-right: 20px;">
<!-- InstanceBeginEditable name="content" -->
<?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">Export Article Information</h1>
<div class="contentBlock">
<p>This form will create a comma delimited CSV file that has all the information related to the article in it, including title, author, department, submit date, and contact information. It will not include the actual word files. Use the form labeled "Download Issue Files" to get a zip archive of all submitted files for an issue.</p>

<?php
	echo $tNGs->getErrorMsg();
?>
<form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="get" name="form1" id="form1">
  <table cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr>
      <td class="KT_th"><label for="iss_cd">ISSUE:</label></td>
      <td><select name="iss_cd" id="iss_cd">
          <?php
do {  
?><option value="<?php echo $row_rsIssueNames['iss_cd']?>"<?php if (!(strcmp($row_rsIssueNames['iss_cd'], $row_rscustom['iss_cd']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsIssueNames['iss_nm']?></option>
          <?php
} while ($row_rsIssueNames = mysql_fetch_assoc($rsIssueNames));
  $rows = mysql_num_rows($rsIssueNames);
  if($rows > 0) {
      mysql_data_seek($rsIssueNames, 0);
	  $row_rsIssueNames = mysql_fetch_assoc($rsIssueNames);
  }
?>
      </select>
          <?php echo $tNGs->displayFieldError("custom", "iss_cd"); ?> </td>
    </tr>
    <tr class="KT_buttons">
      <td colspan="2"><input name="ex_article" type="hidden" value="1" />
      <input type="submit" name="KT_Custom1" id="KT_Custom1" value="Export" />      
      </td>
    </tr>
  </table>
</form>

</div>

<h1 class="yellow">Export Image Information</h1>
<div class="contentBlock">
<p>This form will create a comma delimited CSV file that has all the images for each article in it, including title, author, department, and submit date. It will not include the actual word files. Use the form labeled "Download Issue Files" to get a zip archive of all submitted files for an issue.</p>





<form method="get" id="form2" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
  <table cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr>
      <td class="KT_th"><label for="iss_cd">ISSUE</label></td>
      <td><select name="iss_cd" id="iss_cd">
        <?php
do {  
?>
        <option value="<?php echo $row_rsIssueNames['iss_cd']?>"<?php if (!(strcmp($row_rsIssueNames['iss_cd'], $row_rscustom['iss_cd']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsIssueNames['iss_nm']?></option>
<?php
} while ($row_rsIssueNames = mysql_fetch_assoc($rsIssueNames));
  $rows = mysql_num_rows($rsIssueNames);
  if($rows > 0) {
      mysql_data_seek($rsIssueNames, 0);
	  $row_rsIssueNames = mysql_fetch_assoc($rsIssueNames);
  }
?>
      </select>
          <?php echo $tNGs->displayFieldError("custom", "iss_cd"); ?> </td>
    </tr>
    <tr class="KT_buttons">
      <td colspan="2"><input type="submit" name="KT_Custom2" id="KT_Custom2" value="Export" />
      </td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
</div>
<!-- InstanceEndEditable --></div>
<!-- #END MAIN COLUMN -->
<!-- #BEGIN SECOND COLUMN -->
<div class="floatLeft width250">
<!-- InstanceBeginEditable name="Column2" -->
  <h1 class="yellow" style='display:none;">Download Issue Files</h1>
  <div class="contentBlock" style='display:none;">
   <p>This form will create a zip archive that has all of the uploaded files for the requested issue. You will be prompted to download a zip file.</p>

   <form method="post" id="form3" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
     <table cellpadding="2" cellspacing="0" class="KT_tngtable">
       <tr>
         <td class="KT_th"><label for="iss_cd">ISSUE:</label></td>
         <td><select name="iss_cd" id="iss_cd">
             <?php 
do {  
?>
             <option value="<?php echo $row_rsIssueNames['iss_cd']?>"<?php if (!(strcmp($row_rsIssueNames['iss_cd'], $row_rscustom['iss_cd']))) {echo "SELECTED";} ?>><?php echo $row_rsIssueNames['iss_nm']?></option>
             <?php
} while ($row_rsIssueNames = mysql_fetch_assoc($rsIssueNames));
  $rows = mysql_num_rows($rsIssueNames);
  if($rows > 0) {
      mysql_data_seek($rsIssueNames, 0);
	  $row_rsIssueNames = mysql_fetch_assoc($rsIssueNames);
  }
?>
           </select>
             <?php echo $tNGs->displayFieldError("custom", "iss_cd"); ?> </td>
       </tr>
       <tr class="KT_buttons">
         <td colspan="2"><input type="submit" name="KT_Custom3" id="KT_Custom3" value="Download Images" />
         </td>
       </tr>
     </table>
   </form>
   <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div><!-- #END SECOND COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsIssueNames);

mysql_free_result($rsArticles);

mysql_free_result($rsImages);
?>
