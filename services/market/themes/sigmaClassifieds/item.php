<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/greybox/AJS.js"></script>
<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/greybox/gb_scripts.js"></script>

      <div class="single_area">

		<h1><?php echo $itemTitle; ?> <?if ($itemPrice!=0) echo " - ".getPrice($itemPrice);?></h1>

        <p>
            <b><?php echo T_PUBLISH_DATE;?>:</b> <?php echo setDate($itemDate);?> <?php echo substr($itemDate,strlen($itemDate)-8);?><?php echo SEPARATOR;?>
		    <b><?php echo T_CONTACT_NAME;?>:</b> <?php echo $itemName; ?><?php echo SEPARATOR;?>
		    <?php if ($itemPlace!=""){?>
			    <b><?php echo T_LOCATION;?>:</b> 
			    <?php if (MAP_KEY!=""){?>
				    <a title="Map <?php echo $itemPlace;?>" href="<?php echo SITE_URL."/".mapURL().".?address=".$itemPlace;?>" rel="gb_page_center[640, 480]"><?php echo $itemPlace;?></a>
			    <?php } else echo $itemPlace;?>
			    <?php echo SEPARATOR;?> 
		    <?php }?>
		    <?php if (COUNT_POSTS) echo "$itemViews ".T_DISPLAYED_TIMES.SEPARATOR;?>
		    <?php if (DISQUS!=""){ ?><a href="<?php echo $_SERVER["REQUEST_URI"];?>#disqus_thread">Comments</a><?php echo SEPARATOR;?> <?php }?>
        </p>
        
        
        <?php if (MAX_IMG_NUM>0){?>
		<div id="pictures">
			<?php 
				$imgDir=SITE_URL.IMG_UPLOAD.$idItem."/";
				$query="select FileName from ".TABLE_PREFIX."postsimages where idPost=$idItem order by FileName Limit ".MAX_IMG_NUM;	
				$result =$ocdb->query($query);
				$i=1;
				while ($row=mysql_fetch_assoc($result)){
					$imgName=$row["FileName"];
				 	echo "<a href='$imgDir$imgName' title='$itemTitle ".T_PICTURE." $i' rel='gb_imageset[$idItem]'>
				 			<img class='thumb-archive' src='".$imgDir."thumb_$imgName' title='$itemTitle ".T_PICTURE." $i' alt='$itemTitle ".T_PICTURE." $i' /></a>";
				 	$i++;
				}
			?>
			<div class="clear"></div>
		</div>
	    <?php }?>
	
    <p>
		<?php echo $itemDescription;?>
		<br /><br />

        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style">
        <a href="http://www.addthis.com/bookmark.php?v=250" class="addthis_button_compact"><?php echo T_SHARE;?></a>
        <a class="addthis_button_facebook"></a>
        <a class="addthis_button_myspace"></a>
        <a class="addthis_button_google"></a>
        <a class="addthis_button_twitter"></a>
        <a class="addthis_button_print"></a>
        <a class="addthis_button_email"></a>
        <a href="<?php echo SITE_URL."/".contactURL();?>?subject=<?php echo T_BAD_USE;?>: <?php echo $itemName." (".$idItem.")";?>"><?php echo T_BAD_USE;?></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>
        <!-- AddThis Button END -->

	</p>
  </div>
  
  <?php if ($itemAvailable==1){?>
	<h3 style="cursor:pointer;" onclick="openClose('contactmail');"><?php echo T_CONTACT;?> <?php echo $itemName.': '.$itemTitle;?></h3>
	<div id="contactmail" class="contactform form" >
		<?php if ($itemPhone!=""){?><b><?php echo T_CONTACT_PHONE;?>:</b> <?php echo encode_str($itemPhone); ?><?php }?>
		<form method="post" action="" id="contactItem" onsubmit="return checkForm(this);">
		<p>
		    <label for="author"><small><?php echo T_NAME;?></small></label>*<br />
		    <input id="name" name="name" type="text" class="ico_person" value="<?php echo cP("name");?>" maxlength="75" onblur="validateText(this);"  onkeypress="return isAlphaKey(event);" lang="false"  /><br />
		</p>
		
		<p>
            <label for="author"><small><?php echo T_EMAIL;?></small></label>*<br />
		    <input id="email" name="email"  class="ico_mail" type="text" value="<?php echo cP("email");?>" maxlength="120" onblur="validateEmail(this);" lang="false"  /><br />
		</p>
		<p>
            <label for="comment"><small><?php echo T_MESSAGE;?></small></label>*<br />
		    <textarea rows="10" cols="79" name="msg" id="msg" onblur="validateText(this);"  lang="false"><?php echo strip_tags(stripslashes($_POST['msg']));?></textarea><br />
		</p>
		<p>
            <label for="comment"><small><?php  mathCaptcha();?></small></label>
		    <input id="math" name="math" type="text" size="2" maxlength="2"  onblur="validateNumber(this);"  onkeypress="return isNumberKey(event);" lang="false" />
		</p>
		<br />
		<br />
		<input type="hidden" name="contact" value="1" />
		<input type="submit" id="submit" value="<?php echo T_CONTACT;?>" />
		</p>
		</form> 
	</div>
	<?php } else echo "<div id='sysmessage'>".T_NO_LONGER_AVAILABLE."</div>";?>

	<span style="cursor:pointer;" onclick="openClose('remembermail');"> <?php echo T_REMEMBER_MAIL;?></span><br />
	<div style="display:none;" id="remembermail" >
		<form method="post" action="" id="remember" onsubmit="return checkForm(this);">
		<input type="hidden" name="remember" value="1" />
		<input onblur="this.value=(this.value=='') ? 'email' : this.value;" 
				onfocus="this.value=(this.value=='email') ? '' : this.value;" 
		id="emailR" name="emailR" type="text" value="email" maxlength="120" onblur="validateEmail(this);" lang="false"  />
		<input type="submit"  value="<?php echo T_REMEMBER;?>" />
		</form> 
	</div>

	<?php if (DISQUS!=""){ ?>
		<?php if (DEBUG){ ?><script type="text/javascript"> var disqus_developer = 1;</script><?php } ?>
	
	<div id="disqus_thread"></div><script type="text/javascript" src="http://disqus.com/forums/<?php echo DISQUS;?>/embed.js"></script>
	<noscript><a href="http://disqus.com/forums/<?php echo DISQUS;?>/?url=ref">View the discussion thread.</a></noscript>
	
	<script type="text/javascript">
	//<![CDATA[
	(function() {
		var links = document.getElementsByTagName('a');
		var query = '?';
		for(var i = 0; i < links.length; i++) {
		if(links[i].href.indexOf('#disqus_thread') >= 0) {
			query += 'url' + i + '=' + encodeURIComponent(links[i].href) + '&';
		}
		}
		document.write('<script charset="utf-8" type="text/javascript" src="http://disqus.com/forums/<?php echo DISQUS;?>/get_num_replies.js' + query + '"></' + 'script>');
	})();
	//]]>
	</script>
	<?php } ?>

</div>
