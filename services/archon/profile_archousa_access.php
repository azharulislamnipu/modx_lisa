<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Make an instance of the transaction object
$update_spp_archousa = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($update_spp_archousa);
// Register triggers
$return = !empty( $_POST['RETURN'] ) ? $_POST['RETURN'] : '/home/personal-profile.php';
$update_spp_archousa->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$update_spp_archousa->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$update_spp_archousa->setTable("spp_archousa");
$update_spp_archousa->setPrimaryKey("SPOUSEID", "NUMERIC_TYPE", "POST", "SPOUSEID");
$update_spp_archousa->addColumn("ACCESSSTATUS", "NUMERIC_TYPE", "POST", "ACCESSSTATUS");

// Determine if this is a grant or revoke query
if (isset($_POST['ACCESSSTATUS']) && $_POST['ACCESSSTATUS'] === '1') {
	$update_spp_archousa->addColumn("ACCESSGRANTDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
	$update_spp_archousa->addColumn("ACCESSGRANTBY", "STRING_TYPE", "POST", "UPDATEDBY");
} else {
	$update_spp_archousa->addColumn("ACCESSREVOKEDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
	$update_spp_archousa->addColumn("ACCESSREVOKEBY", "STRING_TYPE", "POST", "UPDATEDBY");
}

// Execute all the registered transactions
$tNGs->executeTransactions();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->

<?php
	echo $tNGs->getErrorMsg();
?><!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
