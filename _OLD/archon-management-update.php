<!-- #MAIN COLUMN -->
<div class="floatLeft width460">
	<h1 class="yellow">Manage [!getUserConfig? &field=`BOULENAME`!] Boul&#233; Archons</h1>
	<div class="contentBlock amu_top_cont">
        <p>Placing the cursor over an Archon's name will cause his contact information to appear. Click the link in order to edit the information.</p>
        <p>You may request, but not change an archon's status on the website. Click the &quot;Request Status Change&quot; link in order to submit an email to the Office of the Grand Boul&#233; detailing your request.</p>

        <div class="amu_table">
            <table width="100%" cellpadding="0" cellspacing="0" id="archon_list_table" class="display">
                <thead>
                <tr>
                    <td>ARCHON</td>
                    <td>STATUS</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>
                [!LDataDisplay? &table=`vw_gramm_archons` &sortBy=`FULLNAME` &sortDir=`ASC` &tpl=`tpl_grammArchonListing` &filter=`BOULENAME,[!getUserConfig? &field=`BOULENAME`!],1|STATUS,Deceased,2`!]
                </tbody>
            </table>
        </div>

	</div>
</div>
<!-- #END MAIN COLUMN -->
<!-- #BEGIN SECOND COLUMN -->
<div class="floatRight width305">
	<h1 class="red">Export Data for [!getUserConfig? &field=`BOULENAME`!] Boul&#233; </h1>
	<div class="contentBlock">
		<form id="boule" name="boule" method="post" action="">

			<input name="boule_export" type="hidden" id="boule" value="boule" />
			<input name="export" type="submit" value="Click to Export [!svVars? &var=`BOULENAME`!] Data" />
		</form>
	</div>

	<div class="floatRight width305">
		<h1 class="yellow">Manage  Status Changes and Transfers for [!svVars? &var=`BOULENAME`!] Boul&#233; </h1>
		<div class="contentBlock">
			<table width="100%" cellspacing="4" cellpadding="4">
				<tr valign="top">
					<td><p><strong>Transfer Request</strong><br />
							Click the link on the right if you wish to receive a transfer form for an archon. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format.</p></td>
					<td valign="bottom"><a href="/home/iakt/grammateus/forms/TRANSFER_FORM.zip">Download Transfer Form</a></td>
				</tr>
				<tr valign="top">
					<td><p><strong>Voluntary Inactive Request</strong><br />
							Click the link on the right if you wish to receive a Voluntary Inactive form. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format. <strong>Revised January 31, 2013</strong></p></td>
					<td valign="bottom"><a href="/home/iakt/grammateus/forms/VOLUNTARY_INACTIVE_FORM.zip">Download Voluntary Inactive Form</a></td>
				</tr>
				<tr valign="top">
					<td><p><strong>Involuntary Inactive Request</strong><br />
							Click the link on the right if you wish to receive a Involuntary Inactive form. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format.</p></td>
					<td valign="bottom"><a href="/home/iakt/grammateus/forms/INVOLUNTARY_INACTIVE_FORM.zip">Download Involuntary Inactive Form</a></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>