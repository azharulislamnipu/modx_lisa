<!-- #MAIN COLUMN -->
<div class="floatLeft width460">
	<h1 class="yellow">Manage [!getUserConfig? &field=`BOULENAME`!] Boul&#233; Archons</h1>
	<div class="contentBlock">
		<table width="100%" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
			<tr>
				<td colspan="2" bgcolor="F4F4F4"><p>Placing the cursor over an Archon's name will cause his contact information to appear. Click the link in order to edit the information.</p>
					<p>You may request, but not change an archon's status on the website. Click the &quot;Request Status Change&quot; link in order to submit an email to the Office of the Grand Boul&#233; detailing your request.</p></td>
			</tr>
			<tr>
				<td bgcolor="F4F4F4"><p><strong>ARCHON</strong></p></td>
				<td bgcolor="F4F4F4"><p><strong>STATUS</strong></p></td>
			</tr>

			[!LDataDisplay? &table=`vw_gramm_archons` &sortBy=`FULLNAME` &sortDir=`ASC` &tpl=`tpl_grammArchonListing` &filter=`BOULENAME,[!getUserConfig? &field=`BOULENAME`!],1|STATUS,Deceased,2`!]

		</table>
	</div>
</div>
<!-- #END MAIN COLUMN -->
<!-- #BEGIN SECOND COLUMN -->
<div class="floatRight width305">
	<!-- <h1 class="yellow">Manage [!getUserConfig? &field=`BOULENAME`!] Boul&#233; Officers</h1>
	<div class="contentBlock">
	<table>

	[!LDataDisplay? &table=`vw_boule_officers` &sortBy=`CPOSITIONID` &sortDir=`ASC` &tpl=`displayOfficers` &filter=`BOULENAME,[!getUserConfig? &field=`BOULENAME`!],1|COMMITTEESTATUSSTT,Active,1`!]

	<td>
		<p><a href="/services/grammateus/data_man/officers_mgmt.php?BOULENAME=[!getUserConfig? &field=`BOULENAME`!]&amp;CHAPTERCD=[!svVars? &var=`BOULECD`!]&amp;REGIONCD=[!svVars? &var=`REGIONCD`!]&amp;UPDATEDBY=[!svVars? &var=`webShortname`!]&amp;GRAMMID=[!svVars? &var=`webInternalKey`!]">Update  Officers' Listing</a></p>
		<p><a href="/services/grammateus/data_man/mail_officers.php?BOULENAME=[!getUserConfig? &field=`BOULENAME`!]&amp;UPDATEDBY=[!svVars? &var=`webFullname`!]&amp;DM_EMAIL=[!svVars? &var=`webEmail`!]">Submit Officers' Listing to the Office of the Grand Boul&eacute;.</a></p>
	</td>
	</tr>
	</table>
	</div> -->

	<h1 class="red">Export Data for [!getUserConfig? &field=`BOULENAME`!] Boul&#233; </h1>
	<div class="contentBlock">
		<form id="boule" name="boule" method="post" action="">

			<input name="boule_export" type="hidden" id="boule" value="boule" />
			<input name="export" type="submit" value="Click to Export [!svVars? &var=`BOULENAME`!] Data" />
		</form>
	</div>

	<div class="floatRight width305">
		<h1 class="yellow">Manage  Status Changes and Transfers for [!svVars? &var=`BOULENAME`!] Boul&#233; </h1>
		<div class="contentBlock">
			<table width="100%" cellspacing="4" cellpadding="4">
				<tr valign="top">
					<td><p><strong>Transfer Request</strong><br />
							Click the link on the right if you wish to receive a transfer form for an archon. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format.</p></td>
					<td valign="bottom"><a href="/home/iakt/grammateus/forms/TRANSFER_FORM.zip">Download Transfer Form</a></td>
				</tr>
				<tr valign="top">
					<td><p><strong>Voluntary Inactive Request</strong><br />
							Click the link on the right if you wish to receive a Voluntary Inactive form. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format. <strong>Revised January 31, 2013</strong></p></td>
					<td valign="bottom"><a href="/home/iakt/grammateus/forms/VOLUNTARY_INACTIVE_FORM.zip">Download Voluntary Inactive Form</a></td>
				</tr>
				<tr valign="top">
					<td><p><strong>Involuntary Inactive Request</strong><br />
							Click the link on the right if you wish to receive a Involuntary Inactive form. You will be able to download a form-fillable Adobe Acrobat file which has been saved in ZIP format.</p></td>
					<td valign="bottom"><a href="/home/iakt/grammateus/forms/INVOLUNTARY_INACTIVE_FORM.zip">Download Involuntary Inactive Form</a></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>