<?php

/**
 * Holds a Boule's delegate count for a meeting
 * @author Pat Heard {http://fullahead.org}
 */
 
class BouleMeetingBean {

    private $id = -1;
    private $code = '';
    private $name = '';
    private $region = '';
    private $location = '';
    
    private $count_archons = 0;    
    private $count_delegates = 0;
    private $count_delegates_appointed = 0;
    private $count_delegates_appointed_remain = 0;
    private $count_delegates_certified = 0;
    private $count_delegates_certified_remain = 0;
    private $count_alternates = 0;
    private $count_alternates_appointed = 0;
    private $count_alternates_appointed_remain = 0;
    
        
    public function __construct(array $args = null){

        // Set default arguments
        $args = array_merge(array(
            'id'                        => -1,
            'code'                      => '',
            'name'                      => '', 
            'region'                    => '', 
            'location'                  => '', 
            'count_archons'             => 0, 
            'count_delegates'           => 0, 
            'count_delegates_appointed' => 0, 
            'count_delegates_certified' => 0, 
            'count_alternates'          => 0, 
            'count_alternates_appointed'=> 0 

        ), $args);

        $this->id = $args['id'];
        $this->code = $args['code'];
        $this->name = ucwords($args['name']);
        $this->region = $args['region'];
        $this->location = $args['location'];
        $this->count_archons = $args['count_archons'];
        $this->count_delegates = $args['count_delegates'];
        $this->count_delegates_appointed = $args['count_delegates_appointed'];
        $this->count_delegates_certified = $args['count_delegates_certified'];
        $this->count_alternates = $args['count_alternates'];
        $this->count_alternates_appointed = $args['count_alternates_appointed'];
        
        // Calculated values
        $this->count_delegates_appointed_remain = $this->count_delegates - $this->count_delegates_appointed;
        $this->count_delegates_certified_remain = $this->count_delegates - $this->count_delegates_certified;
        $this->count_alternates_appointed_remain = $this->count_alternates - $this->count_alternates_appointed;
        
        // Sanity check on the above calculations
        if($this->count_delegates_appointed_remain < 0){
            $this->count_delegates_appointed_remain = 0;
        }
        if($this->count_delegates_certified_remain < 0){
            $this->count_delegates_certified_remain = 0;
        }
        if($this->count_alternates_appointed_remain < 0){
            $this->count_alternates_appointed_remain = 0;
        }        
        
    }
           
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __toString(){      
        
        return
            ' code :' . $this->code .
            ' name :' . $this->name .
            ' region :' . $this->region .
            ' location :' . $this->location . 
            ' count_archons :' . $this->count_archons .
            ' count_delegates :' . $this->count_delegates .
            ' count_delegates_appointed :' . $this->count_delegates_appointed .
            ' count_delegates_certified :' . $this->count_delegates_certified .
            ' count_alternates :' . $this->count_alternates .
            ' count_alternates_appointed :' . $this->count_alternates_appointed .
            ' count_delegates_appointed_remain :' . $this->count_delegates_appointed_remain .
            ' count_delegates_certified_remain :' . $this->count_delegates_certified_remain .
            ' count_alternates_appointed_remain :' . $this->count_alternates_appointed_remain;        
    }
}



?>