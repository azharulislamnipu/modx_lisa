		</div> <!-- #container -->

		<?php do_action( 'sigma_after_container' ) ?>
		<?php do_action( 'sigma_before_footer' ) ?>

		<div id="footer">
	    	<p>&copy 2006 - <?php echo date('Y') ?> Sigma Pi Phi Fraternity </p>

			<?php do_action( 'sigma_footer' ) ?>
		</div><!-- #footer -->

		<?php do_action( 'sigma_after_footer' ) ?>

		<?php wp_footer(); ?>

	</body>

</html>