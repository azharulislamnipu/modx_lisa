<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsFeedback = "-1";
if (isset($_GET['req_id'])) {
  $colname_rsFeedback = $_GET['req_id'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsFeedback = sprintf("SELECT * FROM vw_feedback WHERE req_id = %s", GetSQLValueString($colname_rsFeedback, "int"));
$rsFeedback = mysql_query($query_rsFeedback, $sigma_modx) or die(mysql_error());
$row_rsFeedback = mysql_fetch_assoc($rsFeedback);
$totalRows_rsFeedback = mysql_num_rows($rsFeedback);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" -->
    <h1 class="yellow">ARCHON FEEDBACK</h1>
<div class="contentBlock">
<table width="600" cellspacing="4" cellpadding="4">
  <tr>
    <td valign="top"><strong>COMMENT:</strong></td>
    <td><?php echo nl2br($row_rsFeedback['req_cmt']); ?></td>
  </tr>
  <tr>
    <td valign="top"><strong>MADE BY:</strong></td>
    <td><?php echo $row_rsFeedback['FULLNAME']; ?></td>
  </tr>
  <tr>
    <td valign="top"><strong>DATE MADE:</strong></td>
    <td><?php echo $row_rsFeedback['req_date']; ?></td>
  </tr>
  <tr>
    <td valign="top"><strong>FEEDBACK PAGE:</strong></td>
    <td><a href="<?php echo $row_rsFeedback['feedback_page']; ?>" target="_blank"><?php echo $row_rsFeedback['feedback_page']; ?></a></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td><a href="/home/review-feedback.php">Return to Feedback Listing</a></td>
  </tr>
</table>
</div>

<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsFeedback);
?>
