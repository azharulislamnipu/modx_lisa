<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Galleries</title>
    <style type="text/css">
      body {
        font-family: arial, helvetica, sans-serif;       
        color: #FFFFFF;
        background-color: #181818;
        padding: 10px;
        font-size: medium;
      }
      #wrapper {   
        width: 650px;    
        margin: 0 auto; /* centre page content */       
        text-align: center;
      }
      h1 {        
        color: #CCCCCC;
        font-size: x-large;
        padding: 19px 0 17px 0;
      }    
      p.adminlink {
        text-align: right; 
        font-size: small;		    
      }
      a:link, a:visited {
        color: #FFF;
      }
      a:hover {
        color: #CCC;
      }
      /* basic styles for default table layout */
      table {
        width: 650px;
        table-layout: fixed; /* needed to cope with very long words */
      }
      td {
        font-size: 1em;
        overflow: hidden; /* very long words will be truncated */
        padding: 0 0.5em 1.8em 0.5em;
        vertical-align: top;
      }
      table.svlinks {
        width: 100%;
        padding:10px;
      }
      table.svlinks img {
        border-color: #FFF;
      }
      
      /* basic styles for div layout option */
      div.svlinks {
        text-align: left;
      }
      div.svlinks div {
        clear: both;
      }
      div.svlinks img {
        float: left;
        margin-bottom: 10px;
        border-color: #FFF;
      }
      div.svlinks p {
        float: left;
        width: 500px;
        padding-left: 10px;
      }
      .clearboth { /* see http://www.pixelsurge.com/experiment/clearers.htm */
      	clear: both;
      	height: 0;
      	margin: 0;
      	font-size: 1px;
      	line-height: 0;
      }
    </style>
    <?php
      // User options
      //  set number of columns for table or zero for divs
      $columns = 2;
      //  set relative path to svManager (default is empty string)
      $pathToManager = '';
      // End of user options
      error_reporting(E_ALL);
      $pathToManager = ($pathToManager !== '') ? rtrim($pathToManager, '\\/').DIRECTORY_SEPARATOR : '';
      require $pathToManager.'includes'.DIRECTORY_SEPARATOR.'constants.php';
      require $pathToManager.'classes'.DIRECTORY_SEPARATOR.'indexpage.php';
      error_reporting((DEBUG) ? E_ALL : E_ERROR);
      $indexPage = new IndexPage();
      $adminLink = $pathToManager.'svmanager.php';
    ?>
  </head>
  <body>
    <div id="wrapper">
      <p class="adminlink"><a href="<?php print $adminLink; ?>">Admin Sign In</a></p>
      <h1>Galleries</h1>
        <?php
          print $indexPage->getHtml($columns, $pathToManager);
        ?>
    </div>
  </body>
</html>