<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Image
 *
 * @package svManager
 */
class Image
{
 /**
  * @var string gallery path rel svm
  */
  var $galleryPathRelSvm = '';
  
 /**
  * @var string image path relative to gallery
  */
  var $imagePathRelGallery = '';
  
 /**
  * @var string thumb path relative to gallery
  */
  var $thumbPathRelGallery = '';
  
 /**
  * set image path relative to gallery
  *
  * @access public
  * @return void
  * @param string image path
  */
  function setImagePathRelGallery($path)
  {
    $this->imagePathRelGallery = $path;
  }
  
 /**
  * get image path relative to gallery
  *
  * @access public
  * @return string image path
  */
  function getImagePathRelGallery()
  {
    return $this->imagePathRelGallery;
  }
  
  /**
  * set thumb path relative to gallery
  *
  * @access public
  * @return void
  * @param string thumb path
  */
  function setThumbPathRelGallery($path)
  {
    $this->thumbPathRelGallery = $path;
  }
  
 /**
  * get thumb path relative to gallery
  *
  * @access public
  * @return string image path
  */
  function getThumbPathRelGallery()
  {
    return $this->thumbPathRelGallery;
  }
  
 /**
  * get file name
  *
  * @access public
  * @return string file name
  */
  function getImageFileName()
  {
    return basename($this->imagePathRelGallery);
  }
  
 /**
  * get file name with no suffix
  *
  * @access public
  * @return string file name
  */
  function getCleanImageFileName()
  {
    $info = pathinfo($this->imagePathRelGallery);
    return basename($this->imagePathRelGallery,'.'.$info['extension']);
  }
  
 /**
  * get image size
  *
  * @access public
  * @return array
  */
  function getImageSizeArray()
  {
    return @getimagesize($this->galleryPathRelSvm.$this->imagePathRelGallery);
  }
  
 /**
  * get caption
  *
  * @access public
  * @return string file name
  */
  function getCaption()
  {
    return $this->caption;
  }
  
}
?>
