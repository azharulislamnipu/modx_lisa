<?php
// French language file
// If you are going to make your own language file and are usign the lighbox, 
// make a language file for it too, f.ex. /maxigallery/slidebox/slidebox_lang_en.js
$strings['previous']="&laquo; pr&eacute;c&eacute;dent";
$strings['next']="suivant &raquo;";
$strings['click_to_zoom']=" &raquo; Cliquer pour agrandir ->";
$strings['click_to_go_back']="<- Cliquer pour revenir";
$strings['click_to_open_original']= "Cliquer pour ouvrir en grand";
$strings['back_to_normal_view'] = "Revenir &agrave; la vue normale";
$strings['index']="index";
$strings['go_to_gallery']="Aller &agrave; la gallerie ->";
$strings['pictures_successfully_uploaded']="<b>Images envoy&eacute;es correctement.</b>";
$strings['changes_have_been_saved']="<b>Les modifications ont &eacute;t&eacute; enregistr&eacute;es.</b>";
$strings['delete']="Supprimer";
$strings['title']="Titre";
$strings['save_changes']="Sauvegarder les modifications";
$strings['upload_pictures']="Envoyer images";
$strings['check_to_delete_this_picture']="V&eacute;rifiez pour supprimer cette image ";
$strings['manage_pictures']="G&eacute;rer les images";
$strings['date']="Date";
$strings['description']="Description";
$strings['position']="Position";
$strings['gallery_description']="Description de la gallerie";
$strings['db_no_descr_support']="<em>Attention : La base de donn&eacute;es ne supporte pas les descriptions !</em><br />";
$strings['db_no_pos_support']="<em>Attention : La base de donn&eacute;es ne supporte pas les positions d'images !</em><br />";
$strings['picture'] = "Image:";
$strings['max_pics_reached'] = "Vous avez atteint le nombre maximum d'images pour cette gallerie, vous ne pouvez pas en envoyer plus.";
$strings['supported_types'] = "<b>Type de fichier incorrect !</b> Cette gallerie supporte uniquement les fichiers de type : jpg/jpeg, png, gif et zip (qui contient les types mentionn&eacute;s pr&eacute;c&eacute;demment)";
$strings['gif_not_supported'] = "<b>La librairie GD ne supporte pas les images au format .gif</b> Vous devez utiliser uniquement des images au format .jpg ou .png";
$strings['supported_types_inzip'] = "Erreur : L'archive ZIP contient des types de fichiers incorrects et vont &ecirc;tre ignor&eacute;s.";
$strings['max_pics_reached_some_discarded'] = "<b>Le nombre limite d'images a &eacute;t&eacute; atteint !</b> Certaines d'entre elles peuvent &eacute;tre ignor&eacute;es.";
$strings['zip_not_supported'] = "<b>Votre configuration PHP ne supporte pas les archives .ZIP !</b> Vous devez installer le <a href=\"http://www.php.net/manual/ref.zip.php\">module PHP ZIP et la librairie ZZIPlib.</a>";
$strings['database_error'] = "Erreur : La base de donn&eacute;es gallerie ne peut &ecirc;tre cr&eacute;&eacute;e !";
$strings['database_error_ownid'] = "Error: Could not create own_id field to database table!";
$strings['invalid_class'] = "Erreur: La classe est invalide. SVP, recherchez-la.!";
$strings['invalid_name_class'] = "Erreur: Le nom de la classe est invalide. ";
?>