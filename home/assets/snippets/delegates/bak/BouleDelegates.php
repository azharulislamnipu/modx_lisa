<?php

/**
 * A Boule's delegation.  Performs validation and renders the PDF
 * @author Pat Heard {http://fullahead.org}
 */
 
require_once('DelegateBean.php'); 
require_once('/var/lib/apache/fpdi/fpdf.php');
require_once('/var/lib/apache/fpdi/fpdi.php');
//require_once('fpdf/fpdf.php');
//require_once('fpdi/fpdi.php');
 
class BouleDelegates implements Serializable {

	private $name = '';	
	private $delegates = null;
        private $count_delegates = 0;
        private $count_alternates = 0; 
        private $messages = array();
	
	public function __construct(array $args = null){
            
            // Set default arguments
            $args = array_merge(array(
                'name'              => '',
                'delegates'         => array(),
                'count_delegates'   => 0,
                'count_alternates'  => 0
            ), (is_array($args) ? $args : array()));

            $this->name = $args['name'];
            $this->delegates = $args['delegates'];
            $this->count_delegates = $args['count_delegates'];
            $this->count_alternates = $args['count_alternates'];
           
	}
        
        public function serialize(){  
            return serialize(array(  
                'name'              => $this->name,  
                'delegates'         => $this->delegates,  
                'count_delegates'   => $this->count_delegates,  
                'count_alternates'  => $this->count_alternates  
            )); 
        }  

        public function unserialize($serializedData){  
            $data = unserialize($serializedData);
            
            $this->name = isset($data['name']) ? $data['name'] : '';	
            $this->delegates = isset($data['delegates']) ? $data['delegates'] : null;
            $this->count_delegates = isset($data['count_delegates']) ? $data['count_delegates'] : 0;
            $this->count_alternates = isset($data['count_alternates']) ? $data['count_alternates'] : 0; 
        }         
        
        /* Check if a delegation could be finalized */
        public function isFinalizable(){
            
            $count_delegates = 0;
            $count_ineligible = 0;
            
            // Count the members of the delegation
            foreach($this->delegates as $delegate){
                if($delegate->ineligible){
                    ++$count_ineligible;
                } else if($delegate->type === DelegateBean::TYPE_DELEGATE){
                    ++$count_delegates;
                } 
            }      

            return $count_ineligible == 0 && $count_delegates > 0;
        }
        
        /* Check if a delegation if finalized */
        public function isFinalized(){
            $isFinalized = false;
            
            // Valid?  Check if all delegates are appointed
            if($this->validate()){
                $isFinalized = true;
                foreach($this->delegates as $d){
                    if($d->appointed === false){
                        $isFinalized = false;
                        break;
                    }
                }
            }
            $this->clearMessages();
            return $isFinalized;
        }
        
        /* Check if there are any ineligible delegates in the delegation */
        public function isIneligibleDelegate(){
            $isIneligible = false;

            foreach($this->delegates as $d){
                if($d->ineligible === true){
                    $isIneligible = true;
                    $this->addMessage(array('error', '<strong>' . $d->name . '</strong> has become ineligible.  They must be removed from the delegation before it can be finalized.'));
                }
            }            
            return $isIneligible;
        }        
        
        /* Make sure everything is hunky dory */
        public function validate(){
           
            $this->messages = array();            
            
            // Check boule name
            if(empty($this->name)){
                $this->messages[] = array('error', 'Boul&eacute; name cannot be blank.');
            }              
            
            // Check count of delegates and alternates
            if($this->count_delegates == 0 || $this->count_alternates == 0){
                $this->messages[] = array('error', 'Delegate and alternate count was not loaded properly.');
            }      
            
            // Basic data validation on each delegate          
            foreach($this->delegates as $delegate){
                $messages = $delegate->validate();
                if(!empty($messages)){
                    foreach($messages as $m){
                        $this->messages[] = $m;
                    }
                }              
            }  
            
            // No basic errors?  Keep calm, carry on.
            if(empty($this->messages)){
                
                $names = '';
                $duplicates = '';
                $count_delegates = 0;
                $count_alternates = 0;
                
                // Check for duplicates
                foreach($this->delegates as $delegate){

                    if(strpos($names, $delegate->name) !== false){
                        if(strpos($duplicates, $delegate->name) === false){
                            $this->messages[] = array('error', 'Delegates and alternates must be unique in your delegation. <strong>' . $delegate->name . '</strong> appears more than once in your list.');
                            $duplicates .= $delegate->name . '|';
                        }    
                    } else {
                        $names .= $delegate->name . '|';
                    }
                    
                    // Get the counts of each type
                    if($delegate->type === DelegateBean::TYPE_DELEGATE){
                        ++$count_delegates;
                    } else if($delegate->type === DelegateBean::TYPE_ALTERNATE) {
                        ++$count_alternates;
                    }                
                }      
                
                // Check there is at least one delegate
                if($count_delegates < 1){
                    $this->messages[] = array('error', 'There must be at least one delegate assigned to the Boul&eacute;\'s delegation.');
                }
                
                // Check there aren't too many delegates or alternates
                if($count_delegates > $this->count_delegates){
                    $this->messages[] = array('error', 'There are too many delegates assigned.  The delegation cannot have more than '.$this->count_delegates.' delegate'. ($this->count_delegates == 1 ? '': 's') .'.');
                }

                if($count_alternates > $this->count_alternates){
                    $this->messages[] = array('error', 'There are too many alternates assigned.  The delegation cannot have more than '.$this->count_alternates.' alternate'. ($this->count_alternates == 1 ? '': 's') .'.');
                }                   
            } 
            
            return empty($this->messages);
        }
        
        
	public function renderPdf($pdfFile, $pdfFileName = null){
	
            if($this->validate()){
            
                $pdf = new FPDI();
                $pdf->SetCreator('Sigma Pi Phi');
                
                // Import the PDF and check that it contains pages
                $pagecount = $pdf->setSourceFile($pdfFile);
                if($pagecount > 0){
                                        
                    // Use the first page as a template for the PDF we're creating
                    $template = $pdf->importPage(1, '/MediaBox');
                    if($template !== false){
                        
                        $pdf->addPage();
                        $pdf->useTemplate($template, -5, -5);

                        // Set the font
                        $pdf->SetFont('Times', '', 12);
                        
                        // Boule Name
                        $pdf->Text(31, 53, $this->name);
                        $pdf->Text(6, 67.5, $this->name);

                        // Position of the deletage and alternate fields
			$p = array(
                            'x'	=> array(
                                DelegateBean::TYPE_DELEGATE => 11,
                                DelegateBean::TYPE_ALTERNATE => 107.5,
                            ),
                            'y'	=> array(
                                DelegateBean::TYPE_DELEGATE => 86,
                                DelegateBean::TYPE_ALTERNATE => 86,
                            ),	
                            'memberNumber' => array(
                                DelegateBean::TYPE_DELEGATE => 53,
                                DelegateBean::TYPE_ALTERNATE => 65.5,
                            ),
                            'rowHeight'	=> 9.7
			);

			// Draw each deletage and alternate
			foreach($this->delegates as $d){
                            if($d->type === DelegateBean::TYPE_DELEGATE || $d->type === DelegateBean::TYPE_ALTERNATE){
                                $pdf->Text($p['x'][$d->type], $p['y'][$d->type], $d->name);
                                
                                $pdf->SetXY($p['x'][$d->type] + $p['memberNumber'][$d->type], $p['y'][$d->type] - 1);
                                $pdf->Cell(27, 0, $d->memberNumber, 0, 0, 'R' );
                                $p['y'][$d->type] += $p['rowHeight'];
                            }
			}
                        
                        // Output to browser
                        $pdf->Output(isset($pdfFileName) ? $pdfFileName : basename($pdfFile), 'D');
                        
                    } else {
                        throw new Exception('Failed to load the first page of the form: ' . $pdfFile); 
                    }
                } else {
                   throw new Exception('Failed to import the form: ' . $pdfFile); 
                }
            } else {
                throw new Exception('Boul&eacute; data must be valid before the form can be generated.');
            }
	}

	public function isMessages() {
            return !empty($this->messages);
	}   
        
	public function clearMessages() {
            $this->messages = array();
	}         
        
	public function addMessage($msg) {
            if(!empty($msg)){
		$this->messages[] = $msg;
            }    
	}        
        
	public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
	}
	
	public function __toString(){
            return
                ' name :' . $this->name .
                ' count_delegates :' . $this->count_delegates .
                ' count_alternates :' . $this->count_alternates .
                ' delegates :' . print_r($this->delegates, true);
	}
}

?>