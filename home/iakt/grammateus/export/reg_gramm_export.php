<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

// Load the CSV classes
require_once('../../includes/csv/CSV.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// begin Recordset
$colname__rsBoulesByRegion = '-1';
if (isset($_SESSION['REGIONNAME'])) {
  $colname__rsBoulesByRegion = $_SESSION['REGIONNAME'];
}
$query_rsBoulesByRegion = sprintf("SELECT CHAPTERID, BOULENAME, CITY, STATECD, REGIONCD, REGIONNAME FROM spp_boule WHERE REGIONNAME = %s ORDER BY BOULENAME ASC", GetSQLValueString($colname__rsBoulesByRegion, "text"));
$rsBoulesByRegion = $sigma_modx->SelectLimit($query_rsBoulesByRegion) or die($sigma_modx->ErrorMsg());
$totalRows_rsBoulesByRegion = $rsBoulesByRegion->RecordCount();
// end Recordset

// begin Recordset
$colname__rsArchons = '-1';
if (isset($_GET['REGIONNAME'])) {
  $colname__rsArchons = $_GET['REGIONNAME'];
}
$colname2__rsArchons = '-1';
if (isset($_GET['CHAPTERID'])) {
  $colname2__rsArchons = $_GET['CHAPTERID'];
}
$query_rsArchons = sprintf("SELECT * FROM vw_web_elig WHERE REGIONNAME = %s AND CHAPTERID = %s ORDER BY LASTNAME ASC", GetSQLValueString($colname__rsArchons, "text"),GetSQLValueString($colname2__rsArchons, "text"));
$rsArchons = $sigma_modx->SelectLimit($query_rsArchons) or die($sigma_modx->ErrorMsg());
$totalRows_rsArchons = $rsArchons->RecordCount();
// end Recordset

// begin Recordset
$query_rsPosition = "SELECT * FROM spp_cposition WHERE NOT CPOSITION LIKE '%gran%' AND NOT CPOSITION LIKE '%regi%' AND NOT CPOSITION LIKE '%assis%' AND NOT CPOSITION LIKE '%hair%' AND NOT CPOSITION LIKE '%past%' ORDER BY CPOSITIONID";
$rsPosition = $sigma_modx->SelectLimit($query_rsPosition) or die($sigma_modx->ErrorMsg());
$totalRows_rsPosition = $rsPosition->RecordCount();
// end Recordset

// begin Recordset
$colname__rsOfficers = '-1';
if (isset($_GET['REGIONNAME'])) {
  $colname__rsOfficers = $_GET['REGIONNAME'];
}
$colname2__rsOfficers = '-1';
if (isset($_GET['CPOSITIONID'])) {
  $colname2__rsOfficers = $_GET['CPOSITIONID'];
}
$query_rsOfficers = sprintf("SELECT * FROM vw_boule_officers WHERE COMMITTEESTATUSSTT = 'Active' AND REGIONNAME = %s AND CPOSITIONID = %s ORDER BY BOULENAME, LASTNAME ASC", GetSQLValueString($colname__rsOfficers, "text"),GetSQLValueString($colname2__rsOfficers, "text"));
$rsOfficers = $sigma_modx->SelectLimit($query_rsOfficers) or die($sigma_modx->ErrorMsg());
$totalRows_rsOfficers = $rsOfficers->RecordCount();
// end Recordset

// begin Recordset
$colname__rsAllOfficers = '-1';
if (isset($_GET['REGIONNAME'])) {
  $colname__rsAllOfficers = $_GET['REGIONNAME'];
}
$query_rsAllOfficers = sprintf("SELECT * FROM vw_boule_officers WHERE COMMITTEESTATUSSTT = 'Active' AND  NOT CPOSITION LIKE '%%gran%%' AND NOT CPOSITION LIKE '%%regi%%' AND NOT CPOSITION LIKE '%%assis%%' AND NOT CPOSITION LIKE '%%hair%%' AND NOT CPOSITION LIKE '%%past%%' AND REGIONNAME = %s ORDER BY BOULENAME, CPOSITIONID ASC", GetSQLValueString($colname__rsAllOfficers, "text"));
$rsAllOfficers = $sigma_modx->SelectLimit($query_rsAllOfficers) or die($sigma_modx->ErrorMsg());
$totalRows_rsAllOfficers = $rsAllOfficers->RecordCount();
// end Recordset

// begin Recordset
$colname__rsAllArchons = '-1';
if (isset($_GET['REGIONAME'])) {
  $colname__rsAllArchons = $_GET['REGIONAME'];
}
$query_rsAllArchons = sprintf("SELECT * FROM vw_web_elig WHERE REGIONNAME = %s ORDER BY LASTNAME ASC", GetSQLValueString($colname__rsAllArchons, "text"));
$rsAllArchons = $sigma_modx->SelectLimit($query_rsAllArchons) or die($sigma_modx->ErrorMsg());
$totalRows_rsAllArchons = $rsAllArchons->RecordCount();
// end Recordset

// Begin CSVExport 	
$csvExportObj = new CSV_Export($rsOfficers);
$csvExportObj->addColumn("CPOSITION", "STRING_TYPE", "OFFICE");
$csvExportObj->addColumn("BOULENAME", "STRING_TYPE", "BOULENAME");
$csvExportObj->addColumn("PREFIX", "STRING_TYPE", "PREFIX");
$csvExportObj->addColumn("FIRSTNAME", "STRING_TYPE", "FIRSTNAME");
$csvExportObj->addColumn("MIDDLEINITIAL", "STRING_TYPE", "MIDDLE INITIAL");
$csvExportObj->addColumn("LASTNAME", "STRING_TYPE", "LASTNAME");
$csvExportObj->addColumn("SUFFIX", "STRING_TYPE", "SUFFIX");
$csvExportObj->addColumn("DESIGNATIONLST", "STRING_TYPE", "HONORIFIC");
$csvExportObj->addColumn("ADDRESS1", "STRING_TYPE", "ADDRESS 1");
$csvExportObj->addColumn("ADDRESS2", "STRING_TYPE", "ADDRESS 2");
$csvExportObj->addColumn("CITY", "STRING_TYPE", "CITY");
$csvExportObj->addColumn("STATECD", "STRING_TYPE", "STATE");
$csvExportObj->addColumn("ZIP", "STRING_TYPE", "ZIP");
$csvExportObj->addColumn("HOMEPHONE", "STRING_TYPE", "HOME PHONE");
$csvExportObj->addColumn("WEB_EMAIL", "STRING_TYPE", "EMAIL");
$csvExportObj->setFilename("BOULE_OFFICERS.csv");
$csvExportObj->setHeaders(true);
$csvExportObj->setDelimiter(",");
$csvExportObj->setEnclosure("\"");
$csvExportObj->Execute("GET", "CPOSITIONID");
// End CSVExport 	

// Begin CSVExport 	
$csvExportObj1 = new CSV_Export($rsArchons);
$csvExportObj1->addColumn("BOULENAME", "STRING_TYPE", "BOULENAME");
$csvExportObj1->addColumn("PREFIX", "STRING_TYPE", "PREFIX");
$csvExportObj1->addColumn("FIRSTNAME", "STRING_TYPE", "FIRST NAME");
$csvExportObj1->addColumn("MIDDLEINITIAL", "STRING_TYPE", "MIDDLE INITIAL");
$csvExportObj1->addColumn("LASTNAME", "STRING_TYPE", "LAST NAME");
$csvExportObj1->addColumn("SUFFIX", "STRING_TYPE", "SUFFIX");
$csvExportObj1->addColumn("DESIGNATIONLST", "STRING_TYPE", "HONORIFIC");
$csvExportObj1->addColumn("ADDRESS1", "STRING_TYPE", "ADDRESS 1");
$csvExportObj1->addColumn("ADDRESS2", "STRING_TYPE", "ADDRESS 2");
$csvExportObj1->addColumn("CITY", "STRING_TYPE", "CITY");
$csvExportObj1->addColumn("STATECD", "STRING_TYPE", "STATE");
$csvExportObj1->addColumn("ZIP", "STRING_TYPE", "ZIP");
$csvExportObj1->addColumn("HOMEPHONE", "STRING_TYPE", "HOME PHONE");
$csvExportObj1->addColumn("WEB_EMAIL", "STRING_TYPE", "EMAIL");
$csvExportObj1->setFilename("REGION_ARCHONS.csv");
$csvExportObj1->setHeaders(true);
$csvExportObj1->setDelimiter(",");
$csvExportObj1->setEnclosure("\"");
$csvExportObj1->Execute("GET", "CHAPTERID");
// End CSVExport 	

// Begin CSVExport 	
$csvExportObj2 = new CSV_Export($rsAllOfficers);
$csvExportObj2->addColumn("CPOSITION", "STRING_TYPE", "CPOSITION");
$csvExportObj2->addColumn("BOULENAME", "STRING_TYPE", "BOULENAME");
$csvExportObj2->addColumn("PREFIX", "STRING_TYPE", "PREFIX");
$csvExportObj2->addColumn("FIRSTNAME", "STRING_TYPE", "FIRST NAME");
$csvExportObj2->addColumn("MIDDLEINITIAL", "STRING_TYPE", "MIDDLE INITIAL");
$csvExportObj2->addColumn("LASTNAME", "STRING_TYPE", "LAST NAME");
$csvExportObj2->addColumn("SUFFIX", "STRING_TYPE", "SUFFIX");
$csvExportObj2->addColumn("DESIGNATIONLST", "STRING_TYPE", "HONORIFIC");
$csvExportObj2->addColumn("ADDRESS1", "STRING_TYPE", "ADDRESS 1");
$csvExportObj2->addColumn("ADDRESS2", "STRING_TYPE", "ADDRESS 2");
$csvExportObj2->addColumn("CITY", "STRING_TYPE", "CITY");
$csvExportObj2->addColumn("STATECD", "STRING_TYPE", "STATE");
$csvExportObj2->addColumn("ZIP", "STRING_TYPE", "ZIP");
$csvExportObj2->addColumn("HOMEPHONE", "STRING_TYPE", "HOME PHONE");
$csvExportObj2->addColumn("WEB_EMAIL", "STRING_TYPE", "EMAIL");
$csvExportObj2->setFilename("ALL_REGION_BOULE_OFFICERS.csv");
$csvExportObj2->setHeaders(true);
$csvExportObj2->setDelimiter(",");
$csvExportObj2->setEnclosure("\"");
$csvExportObj2->Execute("GET", "REGIONNAME");
// End CSVExport 	

// Begin CSVExport 	
$csvExportObj3 = new CSV_Export($rsAllArchons);
$csvExportObj3->addColumn("BOULENAME", "STRING_TYPE", "BOULENAME");
$csvExportObj3->addColumn("PREFIX", "STRING_TYPE", "PREFIX");
$csvExportObj3->addColumn("FIRSTNAME", "STRING_TYPE", "FIRST NAME");
$csvExportObj3->addColumn("MIDDLEINITIAL", "STRING_TYPE", "MIDDLE INITIAL");
$csvExportObj3->addColumn("LASTNAME", "STRING_TYPE", "LAST NAME");
$csvExportObj3->addColumn("SUFFIX", "STRING_TYPE", "SUFFIX");
$csvExportObj3->addColumn("DESIGNATIONLST", "STRING_TYPE", "HONORIFIC");
$csvExportObj3->addColumn("ADDRESS1", "STRING_TYPE", "ADDRESS 1");
$csvExportObj3->addColumn("ADDRESS2", "STRING_TYPE", "ADDRESS 2");
$csvExportObj3->addColumn("CITY", "STRING_TYPE", "CITY");
$csvExportObj3->addColumn("STATECD", "STRING_TYPE", "STATE");
$csvExportObj3->addColumn("ZIP", "STRING_TYPE", "ZIP");
$csvExportObj3->addColumn("HOMEPHONE", "STRING_TYPE", "HOME PHONE");
$csvExportObj3->addColumn("WEB_EMAIL", "STRING_TYPE", "EMAIL");
$csvExportObj3->setFilename("ALL_REGION_ARCHONS.csv");
$csvExportObj3->setHeaders(true);
$csvExportObj3->setDelimiter(",");
$csvExportObj3->setEnclosure("\"");
$csvExportObj3->Execute("GET", "REGIONAME");
// End CSVExport 	

//PHP ADODB document - made with PHAkt 3.7.1
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" --> 
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<style type="text/css">
<!--
.tableWidth {
	width: 450px;
}
-->
</style>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng">
<h1><?php echo strtoupper($_SESSION['REGIONNAME']); ?> REGION DATA EXPORTS</h1>
<div class="tableWidth" style="padding: 0 10px;"><p>Below are listed links to export datasets for officer and boul&#233;s, as well as full datasets for each type. Click the link beside the dataset you would like to receive. A window will appear asking if you'd like to open or download the dataset.</p>
<p>Exported data is provided in comma separated value format (.CSV) which is compatible with all spreadsheet software, including Microsoft Excel. </p>  </div>
<table class="KT_tngtable tableWidth" border="0">
      <tr>
        <td colspan="2"><h3>Export <?php echo $_SESSION['REGIONNAME']; ?> Region Boul&eacute; Officers </h3></td>
      </tr>
  <?php
  while (!$rsPosition->EOF) { 
?>
    <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
      <td><?php echo $rsPosition->Fields('CPOSITION'); ?></td>
      <td><a href="reg_gramm_export.php?CPOSITIONID=<?php echo $rsPosition->Fields('CPOSITIONID'); ?>&amp;REGIONNAME=<?php echo $_SESSION['REGIONNAME']; ?>">Click to Export Data</a></td>
    </tr>
    <?php
    $rsPosition->MoveNext(); 
  }
?>
  <tr bgcolor="#99CCFF">
    <td><strong>All <?php echo $_SESSION['REGIONNAME']; ?> Region Boul&eacute; Officers</strong></td>
    <td><a href="reg_gramm_export.php?REGIONNAME=<?php echo $_SESSION['REGIONNAME']; ?>"><strong>Click to Export Data</strong></a></td>
  </tr>
</table>

<table class="KT_tngtable tableWidth" border="0">
      <tr>
        <td colspan="2"><h3>Export <?php echo $_SESSION['REGIONNAME']; ?> Region Archons </h3></td>
      </tr>
      <?php
  while (!$rsBoulesByRegion->EOF) { 
?>
      <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
        <td><?php echo $rsBoulesByRegion->Fields('BOULENAME'); ?> | <?php echo $rsBoulesByRegion->Fields('CITY'); ?>, <?php echo $rsBoulesByRegion->Fields('STATECD'); ?></td>
        <td><a href="reg_gramm_export.php?CHAPTERID=<?php echo $rsBoulesByRegion->Fields('CHAPTERID'); ?>&amp;REGIONNAME=<?php echo $_SESSION['REGIONNAME']; ?>">Click to Export Data</a></td>
      </tr>
        <?php
    $rsBoulesByRegion->MoveNext(); 
  }
?>
  <tr>
    <td bgcolor="#99CCFF"><strong>All <?php echo $_SESSION['REGIONNAME']; ?> Region Archons</strong></td>
    <td bgcolor="#99CCFF"><a href="reg_gramm_export.php?REGIONAME=<?php echo $_SESSION['REGIONNAME']; ?>"><strong>Click to Export Data</strong></a> </td>
  </tr>
</table>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsBoulesByRegion->Close();

$rsBoulesByRegion->Close();

$rsArchons->Close();

$rsPosition->Close();

$rsOfficers->Close();

$rsAllOfficers->Close();

$rsAllArchons->Close();
?>
