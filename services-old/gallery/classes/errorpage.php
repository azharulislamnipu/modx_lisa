<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Admin page
 *
 * @package svManager
 */
class ErrorPage extends Page
{
 /**
  * constructs AdminPage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function ErrorPage()
  {
    parent::Page('svManager &ndash; error', 'error');
  }
}
?>