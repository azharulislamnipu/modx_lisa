<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("photo_file", true, "", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_ImageUpload trigger
//remove this line if you want to edit the code by hand 
function Trigger_ImageUpload(&$tNG) {
  $uploadObj = new tNG_ImageUpload($tNG);
  $uploadObj->setFormFieldName("photo_file");
  $uploadObj->setDbFieldName("photo_file");
  $uploadObj->setFolder("images/{rsArticle.iss_cd}/{rsArticle.CHAPTERID}/");
  $uploadObj->setMaxSize(51200);
  $uploadObj->setAllowedExtensions("jpg, jpeg");
  $uploadObj->setRename("custom");
  $uploadObj->setRenameRule("{rsArticle.art_id}-{rsArticle.iss_cd}-{rsArticle.dept_cd}-{rsArticle.ORGCD}-{KT_name}.{KT_ext}");
  return $uploadObj->Execute();
}
//end Trigger_ImageUpload trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArticle = "-1";
if (isset($_GET['art_id'])) {
  $colname_rsArticle = $_GET['art_id'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArticle = sprintf("SELECT art_id, CUSTOMERID, event_date, event_loc, art_title, art_file, art_content, art_cmt, iss_cd, dept_cd, CHAPTERID, ORGCD, date_submit, submit_by FROM grapter_articles WHERE art_id = %s", GetSQLValueString($colname_rsArticle, "int"));
$rsArticle = mysql_query($query_rsArticle, $sigma_modx) or die(mysql_error());
$row_rsArticle = mysql_fetch_assoc($rsArticle);
$totalRows_rsArticle = mysql_num_rows($rsArticle);

// Make an update transaction instance
$upd_grapter_photos = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_grapter_photos);
// Register triggers
$upd_grapter_photos->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_grapter_photos->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_grapter_photos->registerTrigger("END", "Trigger_Default_Redirect", 99, "submit_article.php?art_id={art_id}&ORGCD={ORGCD}&dept_cd={dept_cd}");
$upd_grapter_photos->registerTrigger("AFTER", "Trigger_ImageUpload", 97);
// Add columns
$upd_grapter_photos->setTable("grapter_photos");
$upd_grapter_photos->addColumn("photo_file", "FILE_TYPE", "FILES", "photo_file");
$upd_grapter_photos->addColumn("photo_id", "NUMERIC_TYPE", "POST", "photo_id");
$upd_grapter_photos->addColumn("art_id", "NUMERIC_TYPE", "POST", "art_id");
$upd_grapter_photos->addColumn("dept_cd", "STRING_TYPE", "POST", "dept_cd");
$upd_grapter_photos->addColumn("ORGCD", "NUMERIC_TYPE", "POST", "ORGCD");
$upd_grapter_photos->setPrimaryKey("photo_id", "NUMERIC_TYPE", "GET", "photo_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsgrapter_photos = $tNGs->getRecordset("grapter_photos");
$row_rsgrapter_photos = mysql_fetch_assoc($rsgrapter_photos);
$totalRows_rsgrapter_photos = mysql_num_rows($rsgrapter_photos);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
</head>

<body>
<?php
	echo $tNGs->getErrorMsg();
?>
<form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" enctype="multipart/form-data">
  <table cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr>
      <td class="KT_th"><label for="photo_file">IMAGE:</label></td>
      <td><input type="file" name="photo_file" id="photo_file" size="70" />
          <?php echo $tNGs->displayFieldError("grapter_photos", "photo_file"); ?> </td>
    </tr>
    <tr class="KT_buttons">
      <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="UPDATE IMAGE" />
      </td>
    </tr>
  </table>
  <input type="hidden" name="photo_id" id="photo_id" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['photo_id']); ?>" />
  <input type="hidden" name="art_id" id="art_id" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['art_id']); ?>" />
  <input type="hidden" name="dept_cd" id="dept_cd" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['dept_cd']); ?>" />
  <input type="hidden" name="ORGCD" id="ORGCD" value="<?php echo KT_escapeAttribute($row_rsgrapter_photos['ORGCD']); ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsArticle);
?>
