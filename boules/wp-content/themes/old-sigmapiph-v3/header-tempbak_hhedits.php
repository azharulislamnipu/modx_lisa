<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <title>Archon Home &raquo; Sigma Pi Phi Fraternity</title>
    <meta name="description" content="Sigma Pi Phi">
    <meta name="keywords" content="Sigma Pi Phi">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto+Slab%7CRaleway:400,700">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>" type="text/css" media="all" />
    <link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/assets/images/favicon.ico" >

    <script src="<?php echo get_template_directory_uri();?>/assets/js/vendor/modernizr-2.8.1.min.js"></script>
        <?php wp_head(); ?>
        <?php add_thickbox(); ?>
</head>
<body class="home blue-bar">

    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>

    <header>

        <nav role="navigation">
            <div class="width relative">
                <h2 class="visuallyhidden">Site Menu</h2>
                <ul class="jetmenu">
                    <li class="home"><a href="#1" class="active">Home</a></li>
                    <li><a href="#2">History</a>
                        <ul class="dropdown">
                            <li><a href="#">Website Design</a></li>
                            <li><a href="#">Hosting</a></li>
                            <li><a href="#">Design</a>
                                <ul class="dropdown">
                                    <li><a href="#">Graphics</a></li>
                                    <li><a href="#">Vectors</a></li>
                                    <li><a href="#">Photoshop</a>
                                        <ul class="dropdown">
                                            <li><a href="#">Photo editing</a></li>
                                            <li><a href="#">Business cards</a></li>
                                            <li><a href="#">Websites</a></li>
                                            <li><a href="#">Illustrations</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Fonts</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Consulting</a></li>
                        </ul>
                    </li>
                    <li><a href="#3">Officers</a>
                        <ul class="dropdown">
                            <li><a href="#">Photo editing</a></li>
                            <li><a href="#">Business cards</a></li>
                            <li><a href="#">Websites</a></li>
                            <li><a href="#">Illustrations</a></li>
                        </ul>
                    </li>
                    <li><a href="#4">Boulé Foundation</a></li>
                    <li><a href="#5">Boulé Journal</a></li>
                    <li><a href="#6">Press</a></li>
                    <li><a href="#7">Contact</a></li>
                    <li><a href="#8" class="secondary button">Login</a></li>
                </ul>
            </div>
        </nav>

        <div class="width">
            <div class="logo">
                <a href="#" title="<?php _e( 'Home', 'archon-network' ) ?>"><?php bloginfo('name') ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/header-sphinx.png" alt="" class="sphinx">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/header-logo.png" alt="Sigma Pi Phi Fraternity" class="text">
                </a>
                <em>Extraordinary Men Called To Lead</em>
            </div>
        </div>
    </header>

     <!-- large image slider - if on homepage show this -->
<!--     <div class="full-width margin-bottom-medium">
        <div class="row">
            <div class="span-8"><div class="inner">
                <div id="slider">
                    <a href="#1" class="slide">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/slide-history.jpg" alt="">
                        <div class="overlay">
                            <h2>52<sup>nd</sup> Grand Boulé in pictures</h2>
                            Suspendisse tincidunt dignissim urna, et scelerisque nibh lobortis et. Praesent tincidunt orci placerat tincidunt aliquam.  <span class="link">View photos</span>
                        </div>
                    </a>
                    <a href="#2" class="slide">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/slide-boston-harbor.jpg" alt="">
                        <div class="overlay">
                            <h2>Information to highlight</h2>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. <span class="link">Praesent in orci felis</span>. Morbi vel dolor viverra erat scelerisque pretium.
                        </div>
                    </a>
                    <a href="#3" class="slide">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/slide-san-francisco.jpg" alt="">
                        <div class="overlay">
                            <h2>Another interesting article</h2>
                            Suspendisse tincidunt dignissim urna, et scelerisque nibh lobortis et. Praesent tincidunt orci placerat tincidunt aliquam.  <span class="link">View photos</span>
                        </div>
                    </a>
                </div>
            </div></div>
        </div>
    </div> -->

<?php echo do_shortcode("[URIS id=505]"); ?>


    <div id="main" role="main">
        <div class="width padding">