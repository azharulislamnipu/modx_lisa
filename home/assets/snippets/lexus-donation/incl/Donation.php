<?php

/**
 * Represents a single donation
 * Author: Pat Heard {http://fullahead.org}
 */
class Donation {

	private $amount = 0;
	private $level = 0;
	private $entries = 0;
	private $quantity = 0;
	private $validLevels = array(
		'150:1:1', '250:2:2', '500:5:3', '750:9:4', '1000:15:5'
	);
	
	public function __construct($level='', $quantity=0){
	
		// Did we get passed a valid level?
		if(in_array($level, $this->validLevels, true)){
			$parts = explode(":", $level);
			$this->amount = intval($parts[0]);
			$this->entries = intval($parts[1]);
			$this->level = intval($parts[2]);			
			$this->quantity = $quantity;		
		}
	}
	
	/** Is this donation valid? */
	public function isValid(){
		return 
			$this->amount > 0 && is_numeric($this->amount) && 
			$this->level > 0 && is_numeric($this->level) &&
			$this->entries > 0 && is_numeric($this->entries) &&
			$this->quantity > 0 && is_numeric($this->quantity);
	}
	
	/** Total amount for this donation */
	public function getTotalAmount(){
		return $this->amount * $this->quantity;
	}
	
	/** Total entries for this donation */
	public function getTotalEntries(){
		return $this->entries * $this->quantity;
	}
	
	/** Level string used to compare with front-end <select> value */
	public function getLevelStr(){
		return $this->amount . ':' . $this->entries . ':' . $this->level;
	}

	/** Determine if a front-end field is selected */
	public function isSelected($field, $value){
		$isSelected = false;
		switch($field){
			case 'level':
				$isSelected = $value === $this->getLevelStr();
				break;
			case 'quantity':
				$isSelected = $value == $this->quantity;
				break;
		}		
		if($isSelected){
			return 'selected="selected"';
		}

	}	
	
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}	
	
	public function __toString(){
		return 
			"\n" . 
			' Amount: ' . $this->amount . 
			' Level: ' 	. $this->level . 
			' Entries: ' . $this->entries .  
			' Quantity: ' . $this->quantity;
	}	
}

?>