<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

header('Content-Type: text/html; charset=utf-8');
require 'includes'.DIRECTORY_SEPARATOR.'constants.php';
error_reporting(DEBUG ? E_ALL : E_ERROR);
require 'classes'.DIRECTORY_SEPARATOR.'errorhandler.php';
require 'classes'.DIRECTORY_SEPARATOR.'setup.php';
require 'classes'.DIRECTORY_SEPARATOR.'page.php';
require 'classes'.DIRECTORY_SEPARATOR.'errorpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'loginpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'sortpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryfactory.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryset.php';
require 'classes'.DIRECTORY_SEPARATOR.'gallery.php';
require 'classes'.DIRECTORY_SEPARATOR.'image.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlcheck.php';
require 'classes'.DIRECTORY_SEPARATOR.'auth.php';
require 'includes'.DIRECTORY_SEPARATOR.'phpcompat.php';
require 'classes'.DIRECTORY_SEPARATOR.'htmlentitydecode.php';
require 'classes'.DIRECTORY_SEPARATOR.'rcopy.php';
require 'classes'.DIRECTORY_SEPARATOR.'pathparser.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlparser.php';
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svimage.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2gallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2image.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'tiltviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvimage.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'autoviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avimage.php'; 
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'postcardviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvimage.php';
}
$setup = new Setup();
$errorHandler = & new ErrorHandler();
$auth = new Auth();
$setup->checkLogin($auth);
$gallerySet = new GallerySet();
$galleryFactory = new GalleryFactory();
if (!isset($_SESSION['galleryIndex']) || $_SESSION['galleryIndex'] === 'new')
{
  $page = $galleryFactory->makeErrorPage(); 
  print $page->getHtmlHead();
  print $page->getPageHeader();
  print $errorHandler->getMessages();
  print '<p>Please go to the <a href="svmanager.php">galleries page</a> and select a gallery to edit</p>';
  print $page->getFooter();
  exit;
}

$galleryIndex = $_SESSION['galleryIndex'];
$galleryRef = $gallerySet->getGalleryRef($galleryIndex);
$galleryType = $gallerySet->getGalleryType($galleryIndex);
$galleryPath = $gallerySet->getGalleryPath($galleryIndex);
$gallery = $galleryFactory->makeGallery($galleryType, $galleryRef, $galleryPath);
if (isset($_POST['sortsubmitted']))
{
  $gallery->setSortOrder($_POST['sortOrder']);
  $gallery->sortGalleryImages();
  if (($_POST['sortoutput'] != 'nosort') && ($_POST['sortOrder'] == 'dragndrop'))
  {
    $gallery->setDragnDrop($_POST['sortoutput']);
  }
  $gallery->saveGallery();
}
$imageObjects = $gallery->getImageObjects();
$page = $galleryFactory->makeSortPage($galleryType);
print $page->getHtmlHead();
print $page->getPageHeader();
ob_start();
print $page->getButtonbar($gallery);
if (count($imageObjects) == 0)
{
  print $errorHandler->getMessages();
  print '<p>You have no images in this gallery</p>';
  print $page->getFooter();
  exit;
}
print $page->getSortHtml($gallery);
$mainOutput = ob_get_clean();

print $errorHandler->getMessages();
print $mainOutput;
print $page->getFooter();
?>