<?php
		/* ------------------------------------------------------------
		:: Snippet: CALx
		---------------------------------------------------------------
		
		---------------------------------------------------------------
		:: Language file  : DUTCH
		--------------------------------------------------------------*/
		// Name of the month
  	$month[0] = "Januari" ;
  	$month[1] = "Februari" ;
  	$month[2] = "Maart" ;
  	$month[3] = "April" ;
  	$month[4] = "Mei" ;
  	$month[5] = "Juni" ;
  	$month[6] = "Juli" ;
  	$month[7] = "Augustus" ;
  	$month[8] = "September" ;
  	$month[9] = "Oktober" ;
  	$month[10] = "November" ;
  	$month[11] = "December" ;

  	// First letter of the month
  	$day[0] = "m" ;
  	$day[1] = "di" ;
  	$day[2] = "wo" ;
  	$day[3] = "do" ;
  	$day[4] = "vr" ;
  	$day[5] = "za" ;
  	$day[6] = "zo" ;

   	// Noms des jours de la semaine en entier
  	$dayName[0] = "montag" ;
  	$dayName[1] = "dienstag" ;
  	$dayName[2] = "mittwoch" ;
  	$dayName[3] = "donnerstag" ;
  	$dayName[4] = "freitag" ;
  	$dayName[5] = "samstag" ;
  	$dayName[6] = "sonntag" ;
		  	
  	// Message d'erreur en cas de date invalide
  	$error01 = "Fout: Onjuiste datum";
  	
  	// Message d'erreur lorsque le calendrier ne peut pas �tre cr�er
  	$msgCannotCreateCALx = "De kalender kan niet gecreeerd worden";
  	
  	// Message pour les info bulles : overlib
  	$closeText = "Sluit";
	$titleText = "Informatie";
	$startStringUniqEvent = "Er zijn ";
	$endStringUniqEvent = " evenementen op schema op deze dag.";
  	$stringDetails = "Klik hier voor meer details";
  	// Configuration des informations de la page de liens vers le multievent
  	$pagetitle = "Lijst van evenementen";
  	$longtitle = "Lijst van evenementen";
  	$description = "Lijst van evenementen";
  	$introtext = "Lijst van evenementen";
  	$eventOf = "Evenementen van ";
  	$noEvent = "Geen evenementen op schema op deze dag.";
  	
  	// Message pour les info bulles des liens de changements de mois
  	$previousYear = "Toon het vorig jaar";
  	$previousMonth = "Toon de vorige maand";
  	$nextMonth = "Toon de volgende maand";
  	$nextYear = "Toon het volgend jaar";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowAYear="Jahres�bersicht";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowMonthEvents="Zeige alle geplanten Termine f�r diesen Monat";
?>
