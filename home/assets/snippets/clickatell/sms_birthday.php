<?php

/*
 * Send an SMS message to an Archon on their birthday
 * Author: Pat Heard {http://fullahead.org}
 */

require '/opt/lampp/htdocs/modx_lisa/home/assets/snippets/clickatell/incl/ClickatellText.php';

// Database details
$host = "localhost";
$user = "sigma-admin";
$password = "beaure";
$database = "modx_dev";

// Attempt to open a database connection
$con = mysql_connect($host, $user, $password); 
if (!$con) {
    die('Could not connect to database for birthday SMS: ' . mysql_error());
}

// Select the MODX database
if(!mysql_select_db($database)){
    die('Could not select "' . $database . '" database: ' . mysql_error()); 
}

// Determine if today is user's birthday, accounting for Feb 29th leap year birthdays
$result = mysql_query("
    SELECT DISTINCT 
        LASTNAME,
        MOBILEPHONE
    FROM 
        spp_archons  
    WHERE
        MOBILEPHONE IS NOT NULL
        AND ( 
            DATE_FORMAT(BIRTHDATE,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d')
        ) OR (
            DATE_FORMAT(BIRTHDATE,'%m-%d') = '02-29'
            AND DATE_FORMAT(NOW(),'%m-%d') = '03-01'
            AND (
                DATE_FORMAT(NOW(),'%Y') % 4 <> 0
                OR (
                    DATE_FORMAT(NOW(),'%Y') % 100 = 0
                    AND DATE_FORMAT(NOW(),'%Y') % 400 <> 0
                )
        )
    )
");


// Build up our array of birthday-Arhons
$archons = array();
while($row = mysql_fetch_array($result)) {
    $archons[] = (object)array(
        'LASTNAME'      => $row['LASTNAME'],
        'MOBILEPHONE'   => (preg_match('/^1.*/', $row['MOBILEPHONE']) ? $row['MOBILEPHONE'] : '1'.$row['MOBILEPHONE'])
    ); 
}
mysql_close($con);

// Did we find anyone with their birthday today AND a mobile phone number specified?
if(!empty($archons)){
    
    // Get our ClickatellText object that we'll use
    $text = new ClickatellText();
    $birthday_message = 
        "Archon ARCHON_LASTNAME,\n".
        "Happy Birthday from our beloved Fraternity!\n".
        "Yours,\n".
        "Grand Sire Archon Payne\n";
    
    // Foreach Archon, load the text object, validate and send if A'OK 
    foreach($archons as $archon){
        $text->setTo($text->cleanPhoneNumber($archon->MOBILEPHONE));         
        if($text->isValidNumber($text->to)){
            $text->setText(str_replace('ARCHON_LASTNAME', $archon->LASTNAME, $birthday_message));
            $text->send();
        }     
    }
}

// C'est tout!
?>