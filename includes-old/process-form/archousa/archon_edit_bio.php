<?php
$data = $response = array();
parse_str($_POST['data'], $data);
array_walk($data, 'test_in_array');
$response['type'] = 'success';


if (empty($data['BIO'])) {
    $response['type'] = 'error';
    $response['html'] = '<p>Bio is Empty.</p>';
}

if($response['type'] !== 'error'){
    $db = get_db();
    $data = Array (
        'BIO'            => $data['BIO'],
        'LASTUPDATED'       => date("Y-m-d H:i:s"),
    );
    $db->where ('ARCHOUSAID', $archousa_id);
    if ($db->update ('spp_archousa', $data)){
        $response['type'] = 'success';
        $response['html'] = htmlspecialchars_decode($data['BIO'], ENT_NOQUOTES);
        SPP_ARCHON_DB::update_lastupdate_date($archousa_id, 'ARCHOUSAID', 'spp_archousa');
    } else{
        $response['type'] = 'error';
//        $response['html'] = $db->getLastError();
        $response['html'] = '<p>Error occurred! Please try again.</p>';
    }
}

die(json_encode($response));