<?php

require_once 'Donation.php';
require_once 'AuthorizeNetUserInfo.php';

/**
 * Represents the total Lexus Donation for a single session
 * Author: Pat Heard {http://fullahead.org}
 */
class LexusDonation {

	private $transaction_id = 0;
	private $customer_id = 0;
	private $draw_numbers_start = 0;
	private $draw_numbers_end = 0;
	private $donations = array();
	private $levels = 0;
	private $messages = array();
	
	// Personal info of the person donating
	public $userInfo = null;	
	
	
    public function __construct($customer_id=0, $levels=1) {
        $this->customer_id = $customer_id;
		$this->levels = $levels;
		$this->userInfo = new AuthorizeNetUserInfo();
    }	
	
	/** The donator's personal data */
	public function setUserInfo($userInfo){		
		$this->userInfo = $userInfo;
	}
	
	/** Database fields to insert this donation */
	public function getDatabaseFields(){
		$fields = array(
			'anet_trans_id'		=> $this->transaction_id,
			'CUSTOMERID'		=> $this->customer_id,
			'don_tot_entries'	=> $this->getTotalEntries(), 
			'don_tot_amt'		=> $this->getTotalAmount(),
			'draw_numbers_start'	=> $this->draw_numbers_start,
			'draw_numbers_end'	=> $this->draw_numbers_end,
			'FIRSTNAME'		=> mysql_escape_string($this->userInfo->first_name),
			'LASTNAME'		=> mysql_escape_string($this->userInfo->last_name),
			'ADDRESS1'		=> mysql_escape_string($this->userInfo->address),
			'CITY'			=> mysql_escape_string($this->userInfo->city),
			'STATECD'		=> mysql_escape_string($this->userInfo->state),
			'ZIP'			=> mysql_escape_string($this->userInfo->zip),			
			'MOBILEPHONE'		=> '',
			'HOMEPHONE'		=> mysql_escape_string($this->userInfo->phone),
			'EMAIL'			=> mysql_escape_string($this->userInfo->email),
			'billing_phone'		=> mysql_escape_string($this->userInfo->billing_phone),
			'billing_email'		=> mysql_escape_string($this->userInfo->billing_email),
			'billing_firstname'	=> mysql_escape_string($this->userInfo->billing_first_name),
			'billing_lastname'	=> mysql_escape_string($this->userInfo->billing_last_name),
			'billing_address'	=> mysql_escape_string($this->userInfo->billing_address),
			'billing_city'		=> mysql_escape_string($this->userInfo->billing_city),
			'billing_state'		=> mysql_escape_string($this->userInfo->billing_state),
			'billing_zip'		=> mysql_escape_string($this->userInfo->billing_zip),                    
                        'payment_type'		=> mysql_escape_string($this->userInfo->payment_type),
			'trans_date'		=> date('Y-m-d H:i:s')
		);                
		for($i = 1; $i <= $this->levels; $i++){
			$fields['don_level' . $i] = $this->getTotalLevel($i);
		}
		return $fields;
	}
	
	public function getDonations(){
		if(empty($this->donations)){
			$this->addDonation(new Donation('150:1:1', 1));
		} 
		return $this->donations;
	}
	
	/** Adds a donation */
	public function addDonation(Donation $donation){
		if($donation->isValid()){
			$this->donations[] = $donation;
			return true;
		}
		return false;
	}
	
	/** Cleans out the donation array */
	public function clearDonations(){
		$this->donations = array();
	}
	
	/** Gets the total amount of the donation(s) */
	public function getTotalAmount(){	
		$total = 0;
		foreach($this->donations as $d){
			if($d->isValid()){
				$total += $d->getTotalAmount();
			}
		}
		return $total;
	}
	
	/** Gets the total entries of the donation(s) */
	public function getTotalEntries(){	
		$total = 0;
		foreach($this->donations as $d){
			if($d->isValid()){
				$total += $d->getTotalEntries();
			}
		}
		return $total;
	}	
	
	/** Gets the total donations at a given level */
	public function getTotalLevel($level=0){
		$total = 0;
		if($level > 0){
			foreach($this->donations as $d){
				if($d->level == $level){
					$total += $d->quantity;
				}
			}
		}
		return $total;
	}
	
	/** Is this a valid donation */
	public function isValid(){
	
		// Are all the donations valid?
		foreach($this->donations as $d){
			if(!$d->isValid()){
				return false;
			}
		}
		
		return 
			count($this->donations) > 0 &&
			$this->customer_id != 0;
	}	
	
	/** Are there messages */
	public function isMessages(){
		return !empty($this->messages);
	}		

	/** Sets the draw numbers for the donation */
	public function setDrawNumbers($currEnd){			
		$this->draw_numbers_start = $currEnd + 1;
		$this->draw_numbers_end = $currEnd + $this->getTotalEntries();
	}
	
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}

	public function __toString(){
		$str = 
			' Transaction ID: ' . $this->transaction_id . 
			' Customer ID: ' 	. $this->customer_id . 
			' Total entries: ' 	. $this->getTotalEntries() .  
			' Total amount: ' 	. $this->getTotalAmount() . 
			' Donations: ';
		
		foreach($this->donations as $d){
			$str .= $d;
		}
                
                $str .= ' User Info: ' . $this->userInfo;
			
		return $str;
	}	
}

?>