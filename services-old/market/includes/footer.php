<?php 
////////////////////////////////////////////////////////////
//Common footer for all the themes
////////////////////////////////////////////////////////////
require_once(SITE_ROOT.'/themes/'.THEME.'/footer.php');?>

<?php if (ANALYTICS!=""){?>
<script type="text/javascript">
window.google_analytics_uacct = "<?php echo ANALYTICS;?>";
</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("<?php echo ANALYTICS;?>");
pageTracker._trackPageview();
} catch(err) {}</script>
<?php }?>


<?php
$ocdb->closeDB();
$ocdb->returnDebug();
echo "<!--".$ocdb->getQueryCounter().T_GENERATED.round((microtime(true)-$app_time),3)."s and ".$ocdb->getQueryCounter("cache")." queries cached-->"; ?>
</body>
</html>
