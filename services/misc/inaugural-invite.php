<?php require_once('../Connections/sigma_modx.php'); ?>
<?php

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("ARCHON", true, "text", "", "", "", "");
$formValidation->addField("BOULENAME", true, "text", "", "", "", "");
$formValidation->addField("MAILING_ADDRESS", true, "text", "", "", "", "");
$formValidation->addField("CITY", true, "text", "", "", "", "");
$formValidation->addField("STATE", true, "text", "", "", "", "");
$formValidation->addField("ZIP", true, "text", "", "", "", "");
$formValidation->addField("EMAIL", true, "text", "", "", "", "");
$formValidation->addField("NUMTICKET", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("{EMAIL}");
  $emailObj->setTo("legatissima@gmail.com");
  $emailObj->setCC("{EMAIL}");
  $emailObj->setBCC("legatissima@gmail.com");
  $emailObj->setSubject("REQUEST: Presidential Inaugural Reception Invitation Request");
  //WriteContent method
  $emailObj->setContent("<p>Invitations are for Boul� members and Archousa only.</p>\n<p>The following Archon has submitted a request for a Presidential Inaugural Reception Invitation:</p> \n\n<p>Number of Invitations Requested: {NUMTICKET}</p>\n<P>Archon: {ARCHON}<br />\nArchousa: {ARCHOUSA}</P>\n<P>{BOULENAME} Boul&#233;</P>\n<P>{MAILING_ADDRESS}<br />\n<P>{CITY}, {STATE} {ZIP}<br />\n<P>{EMAIL}</P>\n\n<P>Date/Time Submitted: {TIMESTAMP}</P>\n\n\n");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = mysql_query($query_rsStates, $sigma_modx) or die(mysql_error());
$row_rsStates = mysql_fetch_assoc($rsStates);
$totalRows_rsStates = mysql_num_rows($rsStates);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoules = "SELECT BOULENAME, concat_ws('', BOULENAME, ' (', CITY, ', ', STATECD, ')') AS LOCATION FROM spp_boule ORDER BY BOULENAME ASC";
$rsBoules = mysql_query($query_rsBoules, $sigma_modx) or die(mysql_error());
$row_rsBoules = mysql_fetch_assoc($rsBoules);
$totalRows_rsBoules = mysql_num_rows($rsBoules);

<?php
// Make a custom transaction instance
$customTransaction = new tNG_custom($conn_sigma_modx);
$tNGs->addTransaction($customTransaction);
// Register triggers
$customTransaction->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Custom1");
$customTransaction->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$customTransaction->registerTrigger("END", "Trigger_Default_Redirect", 99, "/services/misc/thank-you.php");
$customTransaction->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$customTransaction->addColumn("ARCHON", "STRING_TYPE", "POST", "ARCHON");
$customTransaction->addColumn("ARCHOUSA", "STRING_TYPE", "POST", "ARCHOUSA");
$customTransaction->addColumn("NUMTICKET", "STRING_TYPE", "POST", "NUMTICKET", "1");
$customTransaction->addColumn("BOULENAME", "STRING_TYPE", "POST", "BOULENAME");
$customTransaction->addColumn("MAILING_ADDRESS", "STRING_TYPE", "POST", "MAILING_ADDRESS");
$customTransaction->addColumn("CITY", "STRING_TYPE", "POST", "CITY");
$customTransaction->addColumn("STATE", "STRING_TYPE", "POST", "STATE");
$customTransaction->addColumn("ZIP", "STRING_TYPE", "POST", "ZIP");
$customTransaction->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$customTransaction->addColumn("TIMESTAMP", "DATE_TYPE", "POST", "TIMESTAMP", "{NOW_DT}");
// End of custom transaction instance

// Execute all the registered transactions
$tNGs->executeTransactions();
?>
<?php
// Get the transaction recordset
$rscustom = $tNGs->getRecordset("custom");
$row_rscustom = mysql_fetch_assoc($rscustom);
$totalRows_rscustom = mysql_num_rows($rscustom);
?>
// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rscustom = $tNGs->getRecordset("custom");
$row_rscustom = mysql_fetch_assoc($rscustom);
$totalRows_rscustom = mysql_num_rows($rscustom);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_default.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">Invitation Request</h1>
<div class="contentBlock">
<p style="width:475px;">Sigma Pi Phi Fraternity will co-sponsor with the 100 Black Men of America, a Presidential Inaugural Reception on Sunday evening, January 20, 2013 at the City Club at 555 13th Street, NW from 6:00 P. M. until 10:00 P. M. in Washington, D.C.</p>
<h2 style="width:475px;">Invitations will be awarded on a &#8220;first-come, first-served basis.&#8221;</h2>
<p style="width:475px;">Please complete and submit the form below in order to request a ticket.</p>
  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="ARCHON">ARCHON NAME:</label></td>
        <td><input type="text" name="ARCHON" id="ARCHON" value="<?php echo KT_escapeAttribute($row_rscustom['ARCHON']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ARCHON");?> <?php echo $tNGs->displayFieldError("custom", "ARCHON"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ARCHOUSA">ARCHOUSA NAME:</label></td>
        <td><input type="text" name="ARCHOUSA" id="ARCHOUSA" value="<?php echo KT_escapeAttribute($row_rscustom['ARCHOUSA']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ARCHOUSA");?> <?php echo $tNGs->displayFieldError("custom", "ARCHOUSA"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"># of TICKETS </td>
        <td><select name="NUMTICKET" id="NUMTICKET">
          <option value="1">1</option>
          <option value="2">2</option>
        </select>
          <?php echo $tNGs->displayFieldError("custom", "NUMTICKET"); ?></td>
      </tr>
      <tr>
        <td class="KT_th"><label for="BOULENAME">BOUL&#201; NAME:</label></td>
        <td><select name="BOULENAME" id="BOULENAME">
		<option value="">Please choose your boul&#233;.</option>
            <?php 
do {  
?>
            <option value="<?php echo $row_rsBoules['BOULENAME']?>"<?php if (!(strcmp($row_rsBoules['BOULENAME'], $row_rscustom['BOULENAME']))) {echo "SELECTED";} ?>><?php echo $row_rsBoules['LOCATION']?></option>
            <?php
} while ($row_rsBoules = mysql_fetch_assoc($rsBoules));
  $rows = mysql_num_rows($rsBoules);
  if($rows > 0) {
      mysql_data_seek($rsBoules, 0);
	  $row_rsBoules = mysql_fetch_assoc($rsBoules);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("custom", "BOULENAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="MAILING_ADDRESS">MAILING ADDRESS:</label></td>
        <td><input type="text" name="MAILING_ADDRESS" id="MAILING_ADDRESS" value="<?php echo KT_escapeAttribute($row_rscustom['MAILING_ADDRESS']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MAILING_ADDRESS");?> <?php echo $tNGs->displayFieldError("custom", "MAILING_ADDRESS"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="CITY">CITY:</label></td>
        <td><input type="text" name="CITY" id="CITY" value="<?php echo KT_escapeAttribute($row_rscustom['CITY']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("CITY");?> <?php echo $tNGs->displayFieldError("custom", "CITY"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="STATE">STATE:</label></td>
        <td><select name="STATE" id="STATE">
		<option value="">Please choose your state.</option>
            <?php 
do {  
?>
            <option value="<?php echo $row_rsStates['st_cd']?>"<?php if (!(strcmp($row_rsStates['st_cd'], $row_rscustom['STATE']))) {echo "SELECTED";} ?>><?php echo $row_rsStates['st_nm']?></option>
            <?php
} while ($row_rsStates = mysql_fetch_assoc($rsStates));
  $rows = mysql_num_rows($rsStates);
  if($rows > 0) {
      mysql_data_seek($rsStates, 0);
	  $row_rsStates = mysql_fetch_assoc($rsStates);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("custom", "STATE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ZIP">ZIP:</label></td>
        <td><input type="text" name="ZIP" id="ZIP" value="<?php echo KT_escapeAttribute($row_rscustom['ZIP']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ZIP");?> <?php echo $tNGs->displayFieldError("custom", "ZIP"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="EMAIL">EMAIL:</label></td>
        <td><input type="text" name="EMAIL" id="EMAIL" value="<?php echo KT_escapeAttribute($row_rscustom['EMAIL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("EMAIL");?> <?php echo $tNGs->displayFieldError("custom", "EMAIL"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Custom1" id="KT_Custom1" value="Send Request" />        </td>
      </tr>
    </table>
    <input type="hidden" name="TIMESTAMP" id="TIMESTAMP" value="<?php echo KT_formatDate($row_rscustom['TIMESTAMP']); ?>" />
  </form>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
<!-- Piwik -->
<a href="http://piwik.org" title="open source Google Analytics" onclick="window.open(this.href);return(false);">
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.sigmapiphi.org/piwik/" : "http://www.sigmapiphi.org/piwik/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
piwik_action_name = '';
piwik_idsite = 1;
piwik_url = pkBaseURL + "piwik.php";
piwik_log(piwik_action_name, piwik_idsite, piwik_url);
</script>
<object><noscript><p>open source Google Analytics <img src="http://www.sigmapiphi.org/piwik/piwik.php?idsite=1" style="border:0" alt=""/></p></noscript></object></a>
<!-- End Piwik Tag -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsStates);

mysql_free_result($rsStates);

mysql_free_result($rsBoules);
?>
