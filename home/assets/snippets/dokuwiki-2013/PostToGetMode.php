<?php
/*******************************************************************************
	Copyright (c) Intersel 2010 - Tous droits r�serv�s
********************************************************************************
  INTERSEL - SARL au capital de 12.000 � - RCS PARIS 488 379 660 - NAF 721Z
	4 Cit� d'Hauteville
	75010 PARIS
********************************************************************************
	@copyright : Intersel 2010
	@version : 0.1
	Modifications :
		- 16/08/2010 - Creation E.PODVIN
********************************************************************************
* In order to send the posted values if needed from modx template url 
* throught dokuwiki calls and that should be redirect to ModX
* as the 'search' input for example...
**/

//$urlcall_byGetMode=basename($_SERVER['PHP_SELF']);
$file2redirect=$_GET['file2redirect'];
$ampers='?';
foreach($_POST as $key=>$value)
{
  $file2redirect.="$ampers$key=$value";
  $ampers='&';
}
//echo $file2redirect;exit;
header("Location: $file2redirect"); 
?>
