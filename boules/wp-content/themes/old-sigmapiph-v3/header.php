<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--[if lt IE 7]>      <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->

    <head profile="http://gmpg.org/xfn/11">

        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

        <title><?php wp_title() ?></title>

        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_incl/css/mobile.css" type="text/css" media="handheld, only screen and (max-device-width:480px)" />
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> <?php _e( 'Blog Posts RSS Feed', 'archon-network' ) ?>" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> <?php _e( 'Blog Posts Atom Feed', 'archon-network' ) ?>" href="<?php bloginfo('atom_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />

        <?php wp_head(); ?>
        <?php add_thickbox(); ?>

    </head>

    <body <?php body_class() ?> id="bp-default">

        <?php do_action( 'sigma_before_header' ) ?>

        <div id="header">

            <h1 id="logo"><a href="/home" title="<?php _e( 'Home', 'archon-network' ) ?>"><?php bloginfo('name') ?></a></h1>

			<?php
			$header_image = get_header_image();
			if(empty($header_image)) {
				$header_image = HEADER_IMAGE;
			} ?>
			<img src="<?php echo $header_image ?>" alt="Sigma Pi Phi" />

            <?php if(!is_multisite_primary()) : ?>
                <h2 id="tagline"><?php bloginfo( 'description' )?></h2>
                <div id="extraordinary"></div>
            <?php endif; ?>

			<?php do_action( 'sigma_header' ) ?>

        </div><!-- #header -->

        <?php do_action( 'sigma_after_header' ) ?>
        <?php do_action( 'sigma_before_container' ) ?>

        <div id="container">

            <ul id="nav">
                <li<?php if (is_front_page() || is_home() ) : ?> class="selected"<?php endif; ?>>
                    <a href="<?php bloginfo('url') ?>" title="<?php _e( 'Welcome', 'archon-network' ) ?>"><?php _e( 'Welcome', 'archon-network' ) ?></a>
                </li>

				<?php wp_nav_menu(array(
					'theme_location' 	=> 'primary',
					'container' 		=> '',
					'items_wrap'		=> '%3$s',
					'walker'		=> new privacy_walker()
				)); ?>

				<?php if(is_multisite_primary()):
					do_action( 'sigma_nav_items' );
				endif; ?>

            </ul>
