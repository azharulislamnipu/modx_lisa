<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr">
<!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/map_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --> 


<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">Basic Search</h1>
<div class="contentBlock">
<p align="center">
Move your mouse over the map below to see an outline of each region.<br>
Click the region to see a list of boul&eacute;s for that region.<br>
Click a boul&eacute; name to list the members of the boul&eacute;.</p>

<div id="map"
          onmouseover="document.getElementById('img').src='/services/search/images/chapterMap/usa_on.gif'"
          onmouseout="document.getElementById('img').src='/services/search/images/chapterMap/usa_off.gif'">

        <img src="/services/search/images/usa_off.gif" alt="Boul&eacute; Map" name="img" width="550" height="320" id="img" title="Boul&eacute; Map"/>

        <ul>

          <li id="pacific"><a href="#nogo" class="tl" onclick="map.showPopup('pPacific')" title="View Pacific Region Boul&eacute;s">Pacific Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pPacific" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Pacific Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=PA" class="close">Pacific Region Officers</a></p>
                <p><a href="http://www.agboule07.com/" class="close" target="_blank">Pacific Region Meeting Website</a></p>
				<p><a href="javascript:map.hidePopup('pPacific')" title="Close Popup" class="close">Close Pacific Popup</a></p>
                <div>
                  <?php
  while (!$rsPacific->EOF) { 
?>
                  <a href="/services/search/boule_results.php?BOULENAME=<?php echo $rsPacific->Fields('BOULENAME'); ?>"><?php echo $rsPacific->Fields('BOULENAME'); ?> (<?php echo $rsPacific->Fields('CITY'); ?>, <?php echo $rsPacific->Fields('STATECD'); ?>)</a>
                    <?php
    $rsPacific->MoveNext(); 
  }
?>
                </div>
              </div>
            <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="western"><a href="#nogo" class="tl" onclick="map.showPopup('pWestern')" title="View Western Region Boul&eacute;s">Western Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pWestern" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Western Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=WE" class="close">Western Region Officers</a></p>
                <p><a href="http://www.deltamuboule.org/" class="close" target="_blank">Western Region Meeting Website</a></p>
                <p><a href="javascript:map.hidePopup('pWestern')" title="Close Popup" class="close">Close Western Popup</a></p>
              <div>
<?php
  while (!$rsWest->EOF) { 
?>
                  <a href="/services/search/boule_results.php?BOULENAME=<?php echo $rsWest->Fields('BOULENAME'); ?>"><?php echo $rsWest->Fields('BOULENAME'); ?> (<?php echo $rsWest->Fields('CITY'); ?>, <?php echo $rsWest->Fields('STATECD'); ?>)</a>
                    <?php
    $rsWest->MoveNext(); 
  }
?>
				</div>
              </div>
              <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="central"><a href="#nogo" class="tl" onclick="map.showPopup('pCentral')" title="View Central Region Boul&eacute;s">Central Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pCentral" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Central Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=CE" class="close">Central Region Officers</a></p>
                <p><a href="javascript:map.hidePopup('pCentral')" title="Close Popup" class="close">Close Central Popup</a></p>
                <div>
<?php
  while (!$rsCentral->EOF) { 
?>
                  <a href="/services/search/boule_results.php?BOULENAME=<?php echo $rsCentral->Fields('BOULENAME'); ?>"><?php echo $rsCentral->Fields('BOULENAME'); ?> (<?php echo $rsCentral->Fields('CITY'); ?>, <?php echo $rsCentral->Fields('STATECD'); ?>)</a>
                    <?php
    $rsCentral->MoveNext(); 
  }
?>
                </div>
              </div>
            <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="northeast"><a href="#nogo" class="tl" onclick="map.showPopup('pNortheast')" title="View Northeast Region Boul&eacute;s">Northeast Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pNortheast" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Northeast Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=NE" class="close">Northeast Region Officers</a></p>
                <p><a href="http://www.rochesterboule.org/" class="close" target="_blank">Northeast Region Meeting Website</a></p>
                <p><a href="javascript:map.hidePopup('pNortheast')" title="Close Popup" class="close">Close Northeast Popup</a></p>
              <div>
<?php
  while (!$rsNortheast->EOF) { 
?>
                  <a href="/services/search/boule_results.php?BOULENAME=<?php echo $rsNortheast->Fields('BOULENAME'); ?>"><?php echo $rsNortheast->Fields('BOULENAME'); ?> (<?php echo $rsNortheast->Fields('CITY'); ?>, <?php echo $rsNortheast->Fields('STATECD'); ?>)</a>
                    <?php
    $rsNortheast->MoveNext(); 
  }
?>
                </div>
              </div>
            <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="southeast"><a href="#nogo" class="tl" onclick="map.showPopup('pSoutheast')" title="View Southeast Region Boul&eacute;s">Southeast Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pSoutheast" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Southeast Region</h1>
                <p><a href="regional_officer_results.php?REGIONCD=SE" class="close">Southeast Region Officers</a></p>
                <p><a href="http://www.gammakappaboule.org/" class="close" target="_blank">Southeast Region Meeting Website</a></p>
                <p><a href="javascript:map.hidePopup('pSoutheast')" title="Close Popup" class="close">Close Southeast Popup</a></p>
                <div>
<?php
  while (!$rsSoutheast->EOF) { 
?>
                  <a href="/services/search/boule_results.php?BOULENAME=<?php echo $rsSoutheast->Fields('BOULENAME'); ?>"><?php echo $rsSoutheast->Fields('BOULENAME'); ?> (<?php echo $rsSoutheast->Fields('CITY'); ?>, <?php echo $rsSoutheast->Fields('STATECD'); ?>)</a>
                    <?php
    $rsSoutheast->MoveNext(); 
  }
?>
                </div>
              </div>
            <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>
        </ul>
  </div>

</div>

<table align="center" cellpadding="2" cellspacing="4">
  <form action="/services/search/lastname_results.php?LASTNAME=<?php echo $_POST['LASTNAME']; ?>" method="get" name="LASTNAME">
    <tr>
    <td>Search by Last Name</td>
    <td><input id="LASTNAME" name="LASTNAME" type="text" size="20" maxlength="20" /> 
	<input type="submit" class="macButton" name="Submit1" value="Submit"></td>
    <td>&nbsp;</td>
  </tr></form>
<form id="CITY" name="CITY" method="get" action="/services/search/city_results.php?CITY=<?php echo $rsByCity->Fields('CITY'); ?>">
  <tr class="searchForm">
    <td>Search by City </td>
    <td><input name="CITY" type="text" id="CITY" size="20" maxlength="20">
      <input type="submit" class="macButton" name="Submit2" value="Submit"></td>
    <td>&nbsp;</td>
  </tr>
</form>
<form class="searchForm" action="/services/search/boule_results.php?BOULENAME=<?php echo $_POST['BOULENAME']; ?>" method="get" name="BOULENAME">  <tr>
    <td>Search by Boul&eacute; </td>
    <td><select name="BOULENAME" id="BOULENAME">
      <option value="">Select a boul&eacute;.</option>
      <?php
  while(!$rsBoule->EOF){
?>
      <option value="<?php echo $rsBoule->Fields('BOULENAME')?>"><?php echo $rsBoule->Fields('BOULENAME'). " (" .$rsBoule->Fields('CITY'). ", ". $rsBoule->Fields('STATECD').")"  ?> </option>
      <?php
    $rsBoule->MoveNext();
  }
  $rsBoule->MoveFirst();
?>
    </select>
      <input type="submit" class="macButton" name="Submit3" value="Submit"></td>
    <td>&nbsp;</td>
  </tr>
</form>
<form id="STATECD" name="STATECD" method="get" action="/services/search/state_results.php?STATECD=<?php echo $_POST['STATECD']; ?>" class="searchForm">
  <tr>
    <td>Search by State </td>
    <td><select name="STATECD">
      <option value="">Select a state.</option>
      <?php
  while(!$rsStates->EOF){
?>
      <option value="<?php echo $rsStates->Fields('st_cd')?>"><?php echo $rsStates->Fields('st_nm')?></option>
      <?php
    $rsStates->MoveNext();
  }
  $rsStates->MoveFirst();
?>
    </select>
      <input type="submit" class="macButton" name="Submit4" value="Submit"></td>
    <td>&nbsp;</td>
  </tr>
</form>
</table>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
