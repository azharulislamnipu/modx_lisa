<?php
/*
	Copyright (c) InterAKT Online 2000-2006. All rights reserved.
*/

	$KT_XML_uploadErrorMsg = '<strong>File not found:</strong> <br />%s<br /><strong>Please upload the includes/ folder to the testing server.</strong> <br /><a href="http://www.interaktonline.com/error/?error=upload_includes" onclick="return confirm(\'Some data will be submitted to InterAKT. Do you want to continue?\');" target="KTDebugger_0">Online troubleshooter</a>';
	$KT_XML_uploadFileList = array('../common/KT_common.php', '../common/lib/db/KT_Db.php', 'XML_Export.class.php');

	for ($KT_XML_i=0;$KT_XML_i<sizeof($KT_XML_uploadFileList);$KT_XML_i++) {
		$KT_XML_uploadFileName = dirname(realpath(__FILE__)). '/' . $KT_XML_uploadFileList[$KT_XML_i];
		if (file_exists($KT_XML_uploadFileName)) {
			require_once($KT_XML_uploadFileName);
		} else {
			die(sprintf($KT_XML_uploadErrorMsg,$KT_XML_uploadFileList[$KT_XML_i]));
		}
	}
?>