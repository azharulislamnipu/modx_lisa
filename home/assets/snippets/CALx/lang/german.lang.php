<?php
		/* ------------------------------------------------------------
		:: Snippet: CALx
		---------------------------------------------------------------
		
		---------------------------------------------------------------
		:: Language file  : GERMAN / DEUTSCH
		--------------------------------------------------------------*/
		// Name of the month
  	$month[0] = "Januar" ;
  	$month[1] = "Februar" ;
  	$month[2] = "M�rz" ;
  	$month[3] = "April" ;
  	$month[4] = "Mai" ;
  	$month[5] = "Juni" ;
  	$month[6] = "Juli" ;
  	$month[7] = "August" ;
  	$month[8] = "September" ;
  	$month[9] = "Oktober" ;
  	$month[10] = "November" ;
  	$month[11] = "Dezember" ;

  	// First letter of the month
  	$day[0] = "mo" ;
  	$day[1] = "di" ;
  	$day[2] = "mi" ;
  	$day[3] = "do" ;
  	$day[4] = "fr" ;
  	$day[5] = "sa" ;
  	$day[6] = "so" ;

   	// Noms des jours de la semaine en entier
  	$dayName[0] = "montag" ;
  	$dayName[1] = "dienstag" ;
  	$dayName[2] = "mittwoch" ;
  	$dayName[3] = "donnerstag" ;
  	$dayName[4] = "freitag" ;
  	$dayName[5] = "samstag" ;
  	$dayName[6] = "sonntag" ;
		   	
  	// Message d'erreur en cas de date invalide
  	$error01 = "Fehler : falsches Datum";
  	
  	// Message d'erreur lorsque le calendrier ne peut pas �tre cr�er
  	$msgCannotCreateCALx = "Der Kalender konnte nicht erstellt werden";
  	
  	// Message pour les info bulles : overlib
  	$closeText="Schlie�en";
		$titleText="Informationen";
		$startStringUniqEvent = "Es gibt ";
		$endStringUniqEvent = " geplante Termine f�r dieses Datum.";
  	$stringDetails = "Klick hier f�r mehr Details";
  	// Configuration des informations de la page de liens vers le multievent
  	$pagetitle="Terminliste";
  	$longtitle="Terminliste";
  	$description="Terminliste";
  	$introtext="Terminliste";
  	$eventOf = "Termine vom ";
  	$noEvent = "Kein geplanter Terminen f�r dieses Datum.";
  	$noEventMonth = "Es gibt keine geplanten Termine f�r diesen Monat";
  	
  	// Message pour les info bulles des liens de changements de mois
  	$previousYear="zeige das vergangene Jahr";
  	$previousMonth="zeige den vergangenen Monat";
  	$nextMonth="zeige den n�chsten Monat";
  	$nextYear="zeige das n�chste Jahr";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowAYear="Jahres�bersicht";
  	
  	//Message du lien vers l'ann�e compl�te
  	$msgShowMonthEvents="Zeige alle geplanten Termine f�r diesen Monat";
?>