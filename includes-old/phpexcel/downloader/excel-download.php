<?php
//echo "<pre>";
//var_dump($boulcd);

$eligible_values = array(
    'CHAPTERID' => 'BOULE NAME',
    'JOINDATE' => 'JOIN DATE',
    'PREFIX' => 'PREFIX',
    'FIRSTNAME' => 'FIRST NAME',
    'MIDDLEINITIAL' => 'MIDDLE INITIAL',
    'LASTNAME' => 'LAST NAME',
    'SUFFIX' => 'SUFFIX',
    'DESIGNATIONLST' => 'HONORIFIC',
    'ADDRESS1' => 'ADDRESS1',
    'ADDRESS2' => 'ADDRESS2',
    'CITY' => 'CITY',
    'STATECD' => 'STATE',
    'ZIP' => 'ZIP',
    'HOMEPHONE' => 'HOME PHONE',
    'MOBILEPHONE' => 'MOBILE PHONE',
    'EMAIL' => 'EMAIL',
    'MARITALSTT' => 'MARITAL STATUS',
    'BIRTHDATE' => 'BIRTH DATE',
    'DECEASEDDATE' => 'DECEASED DATE',
    'SPOUSENAME' => 'SPOUSE NAME',
);

$db = get_db();
$db = get_db();
$db->where ("CHAPTERID", $boulcd);
$bdata = $db->get('spp_archons', null, array_keys($eligible_values));

if($db->count == 0){
    header('Location:  '.  $_SERVER['HTTP_REFERER']);
    exit;
}

/** Set default timezone (will throw a notice otherwise) */
date_default_timezone_set('America/New_York');

// include PHPExcel
include_once $include_folder . '/phpexcel/PHPExcel.php';

// create new PHPExcel object
$objPHPExcel = new PHPExcel;

// set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

// set default font size
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);

// create the writer
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

 

/**

 * Define currency and number format.

 */

// currency format, € with < 0 being in red color
$currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

// number format, with thousands separator and two decimal points.
$numberFormat = '#,#0.##;[Red]-#,#0.##';

 

// writer already created the first sheet for us, let's get it
$objSheet = $objPHPExcel->getActiveSheet();

// rename the sheet
$objSheet->setTitle($boulcd .' Data');

$letter = range("A","Z");

$row_index = 0;
foreach (array_values($eligible_values) as $key => $frow){
    ++$row_index;
    $cell_id = $letter[0].$row_index;
    $objSheet->getStyle($cell_id)->getFont()->setBold(true)->setSize(14);
    $objSheet->getCell($cell_id)->setValue($frow);
    $objSheet->getColumnDimension($letter[$row_index-1])->setAutoSize(true);
}

foreach ($bdata as $index=>$db_data){
    $col_id = $letter[$index+1];
    $row_index = 0;
    foreach ($db_data as $key=>$data){
        ++$row_index;
        $cell_id = $col_id.$row_index;
        $objSheet->getCell($cell_id)->setValue($data);
    }
}


//Setting the header type
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $boulcd . ' Data.xls"');
header('Cache-Control: max-age=0');

$objWriter->save('php://output');

/* If you want to save the file on the server instead of downloading, replace the last 4 lines by 
	$objWriter->save('test.xlsx');
*/
die();
