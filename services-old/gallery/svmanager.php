<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
header('Content-Type: text/html; charset=utf-8');
require 'includes'.DIRECTORY_SEPARATOR.'constants.php';
error_reporting(DEBUG ? E_ALL : E_ERROR);
require 'classes'.DIRECTORY_SEPARATOR.'errorhandler.php';
require 'classes'.DIRECTORY_SEPARATOR.'setup.php';
require 'classes'.DIRECTORY_SEPARATOR.'page.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleriespage.php';
require 'classes'.DIRECTORY_SEPARATOR.'loginpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryfactory.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryset.php';
require 'classes'.DIRECTORY_SEPARATOR.'gallery.php';
require 'classes'.DIRECTORY_SEPARATOR.'image.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlcheck.php';
require 'classes'.DIRECTORY_SEPARATOR.'auth.php';
require 'includes'.DIRECTORY_SEPARATOR.'phpcompat.php';
require 'classes'.DIRECTORY_SEPARATOR.'htmlentitydecode.php';
require 'classes'.DIRECTORY_SEPARATOR.'rcopy.php';
require 'classes'.DIRECTORY_SEPARATOR.'pathparser.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlparser.php';
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svimage.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2gallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2image.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'tiltviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvimage.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'autoviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avimage.php';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'postcardviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvimage.php';
}
$setup = new Setup();
// Assigning the return value of new by reference is deprecated in php5 but needed for php4.
// So error_reporting should be set to maximum of E_ALL & ~E_DEPRECATED in php.ini
// Could also be set in .htaccess on Unix systems.
// Note that E_ALL has different values in the different php versions and is 30719 in php 5.3
$errorHandler = & new ErrorHandler();
$auth = new Auth();
$galleryFactory = new GalleryFactory();
$setup->checkLogin($auth);
$page = new GalleriesPage();
print $page->getHtmlHead();
print $page->getPageHeader();
$gallerySet = new GallerySet();

switch (true)
{
  case isset($_GET['remove']) :
    $removed = $gallerySet->removeGallery($_GET['remove']);
    if ($removed && isset($_SESSION['galleryIndex']) && $_SESSION['galleryIndex'] == $_GET['remove'])
    {
    	unset($_SESSION['galleryIndex']);
    }
    break;  
  case isset($_GET['rebuild']) :
    $galleryIndex = $_GET['rebuild'];
    $galleryTitle = $gallerySet->getGalleryTitle($galleryIndex);
    $galleryType = $gallerySet->getGalleryType($galleryIndex);
    $galleryPath = $gallerySet->getGalleryPath($galleryIndex);
    $galleryRef = $gallerySet->getGalleryRef($galleryIndex);
    if ($galleryType == 'simpleviewer' && file_exists($galleryPath.'simpleviewer.swf'))
    {
      $galleryType = 'simpleviewer2';
      $gallerySet->setGalleryType($galleryIndex, $galleryType);
    }
    $gallery = $galleryFactory->makeGallery($galleryType, $galleryRef, $galleryPath, false);
    $gallery->rebuild(); 
}
if (isset($_GET['newSort']))
{
  $gallerySet->setGallerySortOrder($_GET['newSort']);
  unset ($_SESSION['galleryIndex']);
}
else
{
  $gallerySet->setGallerySortOrder('reSort');
}
if ($_SESSION[SESSION_DEFAULT_PASS])
{
  trigger_error('Installation password is still in use &ndash; please <a href="svmadmin.php" title="link to admin screen">reset your password</a>', E_USER_NOTICE);
}
ob_start();
print $page->getGalleriesHtml($gallerySet);
$mainOutput = ob_get_clean();
print $errorHandler->getMessages();
print $mainOutput;
print $page->getFooter();
?>