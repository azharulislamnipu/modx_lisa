<?php
// TODO array column skillid and description to show on search result
$errorMsg    = null;
$hasErrors   = true;
$currentpage = 0;
$sort        = 'LASTNAME';
$sortdir     = 'ASC';
if ( isset( $_GET['currentpage'] ) ) {
	$currentpage = intval( $_GET['currentpage'] );
}
if ( isset( $_GET['sort'] ) ) {
	$sort = $_GET['sort'];
}
if ( isset( $_GET['sortdir'] ) ) {
	$sortdir = $_GET['sortdir'];
}

$db = get_db();
//mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsIndustry     = "SELECT DESCRIPTION, OCCUPATIONCD, OCCUPATIONID  FROM spp_occupation  GROUP BY DESCRIPTION ORDER BY `DESCRIPTION` ASC";
$rsIndustry           = $db->rawQuery( $query_rsIndustry );
$totalRows_rsIndustry = is_array( $rsIndustry ) ? count( $rsIndustry ) : 0;

$query_rsStates     = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates           = $db->rawQuery( $query_rsStates );
$totalRows_rsStates = is_array( $rsStates ) ? count( $rsStates ) : 0;

$query_rsBoule     = "SELECT * FROM spp_boule ORDER BY ORGCD ASC";
$rsBoule           = $db->rawQuery( $query_rsBoule );
$totalRows_rsBoule = is_array( $rsBoule ) ? count( $rsBoule ) : 0;

$query_rsRegions     = "SELECT DISTINCT REGIONCD, REGIONNAME FROM spp_boule ORDER BY REGIONNAME ASC";
$rsRegions           = $db->rawQuery( $query_rsRegions );
$totalRows_rsRegions = is_array( $rsRegions ) ? count( $rsRegions ) : 0;

$query_rsOfficers     = "SELECT * FROM spp_cposition WHERE CPOSITION NOT LIKE '%Grand%' AND CPOSITION NOT LIKE '%past regional sire archons council%' AND CPOSITION NOT LIKE '%committe%' AND CPOSITION NOT LIKE '%hair%' AND CPOSITION NOT LIKE '%ounse%' ORDER BY CPOSITIONID ASC";
$rsOfficers           = $db->rawQuery( $query_rsOfficers );
$totalRows_rsOfficers = is_array( $rsOfficers ) ? count( $rsOfficers ) : 0;


$query_rsSkills     = "SELECT * FROM spp_skillref ORDER BY DESCRIPTION";
$rsSkills           = $db->rawQuery( $query_rsSkills );
$totalRows_rsSkills = is_array( $rsSkills ) ? count( $rsSkills ) : 0;



//SEARCH SQL
$searchpars_out = null;

$searchsql  = '';
$bsearchsql = '';


if ( isset( $_GET['q'] ) ) {
	$keys = $_GET['q'] . '';
	if ( strlen( $keys ) > 0 ) {
		$searchpars_out .= 'Keywords = ' . $keys;
//		echo "<pre>";
		$searchsql = create_search_query_from_all( $keys, 'vw_search_y2', $db );
		$hasErrors = false;
	}
}

/*$search_terms = array(
	'firstname' => 'FIRSTNAME',
	'lastname'  => 'LASTNAME',
	'boule'     => 'CHAPTERID',
	'state'     => 'STATECD',
	'region'    => 'REGIONCD',
	'office'    => 'CPOSITION',
	'title'     => 'JOBTITLE',
	'company'   => 'COMPANYNAME',
	'school'    => 'INSTITUTION',
	'industry'  => 'OCCUPATION_TYPE',
);

foreach ( $search_terms as $key => $search_term ) {
	$searchsql = addSearchPar( $searchsql, $key, $search_term, true );
}*/

$searchsql = addSearchPar($searchsql, 'firstname', 'FIRSTNAME', true);
$searchsql = addSearchPar($searchsql, 'lastname', 'LASTNAME', true);
$searchsql = addSearchPar($searchsql, 'boule', 'CHAPTERID', false);
$searchsql = addSearchPar($searchsql, 'city', 'CITY', true);
$searchsql = addSearchPar($searchsql, 'state', 'STATECD', true);
$searchsql = addSearchPar($searchsql, 'region', 'REGIONCD', false);
$searchsql = addSearchPar($searchsql, 'office', 'CPOSITION', false);
$searchsql = addSearchPar($searchsql, 'title', 'TITLEID', true);
$searchsql = addSearchPar($searchsql, 'company', 'COMPANYNAME', true);
$searchsql = addSearchPar($searchsql, 'school', 'INSTITUTION', true);
$searchsql = addSearchPar($searchsql, 'industry', 'OCCUPATION_TYPE', false);

if ( strlen( $searchsql ) > 0 ) {
//	$searchsql .= " AND COMMITTEESTATUSSTT = 'Active' ";
	$searchsql .= " AND STATUS != 'Deceased' ";
	$hasErrors = false;
}
$totalRows_rsSearch = null;
if ( ! $hasErrors ) {
//echo "<pre>";
//    var_dump($searchsql);
//	$searchressql = '(SELECT CUSTOMERID, WEB_ID, PREFIX, FIRSTNAME, LASTNAME, L2R_FULLNAME, BOULENAME, CITY, STATECD, EMAIL, HOMEPHONE, CPOSITION, JOB_TITLE, IMAGE, OCCUPATION_TYPE FROM vw_search_y2 WHERE ' . $searchsql . ' AND CPOSITION = "Sire Archon" ' . ' GROUP BY BOULENAME, CPOSITION' . ' ORDER BY ' . $sort . ' ' . $sortdir . ') ';
//	$searchressql .= ' UNION ALL ' . '(SELECT CUSTOMERID, WEB_ID, PREFIX, FIRSTNAME, LASTNAME, L2R_FULLNAME, BOULENAME, CITY, STATECD, EMAIL, HOMEPHONE, CPOSITION, JOB_TITLE, IMAGE, OCCUPATION_TYPE FROM vw_search_y2 WHERE ' . $searchsql . ' AND CPOSITION != "Sire Archon" '. ' ORDER BY ' . $sort . ' ' . $sortdir .')';

//	$searchressql = '(SELECT CUSTOMERID, WEB_ID, PREFIX, FIRSTNAME, LASTNAME, L2R_FULLNAME, BOULENAME, CITY, STATECD, EMAIL, HOMEPHONE, CPOSITION, JOB_TITLE, IMAGE, OCCUPATION_TYPE FROM vw_search_y2 WHERE ' . $searchsql . ' ORDER BY ' . $sort . ' ' . $sortdir .')';

    $searchressql = '(SELECT * FROM vw_search_y2 WHERE ' . $searchsql .' ORDER BY ' . $sort . ' ' . $sortdir .')';

//	var_dump($searchressql);
//	die();
////
//	var_dump($searchsql);
//	die();


//	var_dump($searchressql);
//	die();

	$rsArchons          = $db->rawQuery( $searchressql );
	$totalRows_rsSearch = is_array( $rsArchons ) ? count( $rsArchons ) : 0;

//	$bsearch = array('CUSTOMERID', '');
//	$breplace = array('CUSTOMERCD',);
//    $bsearchsql = str_replace('', '', $searchsql);

//	$all_boule_officers_sql = 'SELECT * FROM vw_boule_officers WHERE ' . $bsearchsql . ' GROUP BY WEB_ID ORDER BY ' . $sort . ' ' . $sortdir;
//	$all_boule_officers = $db->rawQuery($all_boule_officers_sql);

	$all_boule_officers       = SPP_ARCHON_DB::get_active_boule_officers( "%" );
	$all_boule_officers       = $all_boule_officers['data'];
	$totalRows_boule_officers = is_array( $all_boule_officers ) ? count( $all_boule_officers ) : 0;

//    $searchpagessql = 'SELECT COUNT(*) as cnt FROM vw_search_y2 WHERE ' . $searchsql . ' GROUP BY WEB_ID';
	//echo $searchressql;
//    $rsPageArchons = mysqli_query($sigma_modx, $searchpagessql) or die(mysqli_error($sigma_modx));
	//$row = mysqli_fetch_assoc($rsPageArchons);
//    $totalRows_rsSearch = mysqli_num_rows($rsPageArchons);
} else {
	$errorMsg = 'You must specify at least one field!';
}


$filter_class = 'filter_open';
if ( $totalRows_rsSearch > 0 ) {
	$filter_class = 'filter_close';
}

?>
<!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --><!DOCTYPE html>
<!--
 ____________________________________________________________
|                                                            |
|      DATE + 2006.11.14                                     |
| COPYRIGHT + Sigma Pi Phi 2006.  All Rights Reserved.       |
|____________________________________________________________|

-->
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" xmlns="http://www.w3.org/1999/html"> <!--<![endif]-->
<head>
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Sigma Pi Phi Fraternity |</title>
    <!-- InstanceEndEditable -->
	<?php include( $_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/head-meta.php" ); ?>
	<?php include( $_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/head-css-main.php" ); ?>
	<?php include( $_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/head-javascript.php" ); ?>
	<?php include( $_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/head-links.php" ); ?>

    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
        <!--
        .style1 {
            font-weight: bold
        }

        -->
    </style>
    <!-- InstanceEndEditable -->
</head>

<body class="home blue-bar color-override-yellow-orange">
<ul class="visuallyhidden focusable">
    <li><a href="#nav">Skip to navigation</a></li>
    <li><a href="#main">Skip to main content</a></li>
</ul>
<header>
	<?php include( $_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/main-nav.php" ); ?>
    <div class="width">
		<?php include( $_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/global-logo-tagline.php" ); ?>
    </div>
</header>
<!-- large image slider content toggle start -->
<?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
<!-- large image slider content toggle end-->

<!-- main content body start -->
<div id="main" role="main">
    <div class="width padding">
        <!-- shows the content, pagetitle, breadcrumb and sidebar -->
        <!-- InstanceBeginEditable name="pagetitle" -->
        <section class="span-12 margin-bottom-medium">
            <div class="inner">
                <h1>Membership Directory</h1>
                <ol class="breadcrumbs" style="margin-top:-18px;">
                    <li><a class="B_homeCrumb" href="/home/archon-home.php"
                           title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                    <li><span class="B_currentCrumb">Administration</span></li>
                </ol>
                <div>
        </section>



        <!-- InstanceBeginEditable name="content" -->

        <div class="width margin-bottom-large span-8 float-left as_wrap <?php echo $filter_class . 's'; ?>">


            <div class="clearfix adsc_top">
                <h1 class="">Advanced Search</h1>
                <div class="search-top">
                    <ul>
                        <li><a href="/home/basic-search-upd.php" class="btn">Basic Search</a></li>
                        <li><a href="../boules/business-directory.php" class="btn">Busisness Directory</a></li>
                        <li><a href="#" class="open_sfilter">Search Again</a> </li>
                    </ul>
                </div>
            </div>



            <div class="as_filter <?php echo $filter_class; ?>">
                <h3><span CLASS="sf_txt">Click Here to open</span> Search Filters</h3>

                <form name="MX_Search_Form" method="GET" action="" class="as_filter_main">
                    <div class="filter_item clearfix">
                        <label for="q">Keywords:</label>
                        <div class="filter_input"><input name="q" type="text" id="q" size="20" value=""/></div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="state">State:</label>
                        <div class="filter_input">
                            <select name="state" id="state" class="chosen-select">
                                <option value="">Anywhere</option>
								<?php
								if ( $totalRows_rsStates > 0 ) {
									foreach ( $rsStates as $rsState ) { ?>
                                        <option value="<?php echo $rsState['st_cd'] ?>"><?php echo $rsState['st_nm'] ?></option>
									<?php }
								}
								?>
                            </select>
                        </div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="city">City:</label>
                        <div class="filter_input"><input name="city" type="text" id="city" size="20" value=""/>
                        </div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="firstname">First Name:</label>
                        <div class="filter_input">
                            <input name="firstname" type="text" id="firstname" size="20" value=""/>
                        </div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="lastname">Last Name:</label>
                        <div class="filter_input"><input name="lastname" type="text" id="lastname" size="20"
                                                         value=""/></div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="boule">Boul&eacute;:</label>
                        <div class="filter_input">
                            <select name="boule" id="boule" class="chosen-select">
                                <option value="">Anywhere</option>
								<?php
								if ( $totalRows_rsBoule > 0 ) {
									foreach ( $rsBoule as $rsBoul ) { ?>
                                        <option value="<?php echo $rsBoul['CHAPTERID'] ?>"><?php echo $rsBoul['BOULENAME'] ?></option>
									<?php }
								}
								?>
                            </select>
                        </div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="region">Region:</label>
                        <div class="filter_input">
                            <select name="region" id="region" class="chosen-select">
                                <option value="">Anywhere</option>
								<?php
								if ( $totalRows_rsRegions > 0 ) {
									foreach ( $rsRegions as $rsRegion ) { ?>
                                        <option value="<?php echo $rsRegion['REGIONCD'] ?>"><?php echo $rsRegion['REGIONNAME'] ?></option>
									<?php }
								}
								?>
                            </select>
                        </div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="office">Office:</label>
                        <div class="filter_input">
                            <select name="office[]" size="5" multiple="multiple" id="office" class="chosen-select"
                                    data-placeholder="Select Office Position">
								<?php
								if ( $totalRows_rsOfficers > 0 ) {
									foreach ( $rsOfficers as $rsOfficer ) { ?>
                                        <option value="<?php echo $rsOfficer['CPOSITION'] ?>"><?php echo $rsOfficer['CPOSITION'] ?></option>
									<?php }
								}
								?>
                            </select>
                        </div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="title">Occupation:</label>
                        <div class="filter_input">

                            <select name="title"  id="title" class="chosen-select">

                            <option value=""> Select Occupation</option>
							<?php
							if ( $totalRows_rsSkills > 0 ) {
								foreach ( $rsSkills as $rsSkills ) { ?>
                                    <option value="<?php echo $rsSkills['SKILLID'] ?>"><?php echo $rsSkills['DESCRIPTION'] ?></option>
								<?php }
							}
							?>
                            </select>

                        </div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="company">Company:</label>
                        <div class="filter_input"><input name="company" type="text" id="company" size="20"
                                                         value=""/></div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="school">School:</label>
                        <div class="filter_input"><input name="school" type="text" id="school" size="20" value=""/>
                        </div>
                    </div>
                    <div class="filter_item clearfix">
                        <label for="industry">Industry:</label>
                        <div class="filter_input">
                            <select name="industry[]" size="5" multiple="multiple" id="industry"
                                    class="chosen-select" data-placeholder="Select Industry">
								<?php
								if ( $totalRows_rsIndustry > 0 ) {
									foreach ( $rsIndustry as $rsIndustr ) { ?>
                                        <option value="<?php echo $rsIndustr['DESCRIPTION'] ?>"><?php echo $rsIndustr['DESCRIPTION'] ?></option>
									<?php }
								}
								?>
                            </select>
                        </div>
                    </div>

                    <!-- ERROR MESSAGES -->
                    <div class="error_msg">
						<?php
						if ( isset( $_GET['q'] ) ) {
							//echo 'results '. $totalRows_rsSearch;
							if ( $hasErrors && ! is_null( $errorMsg ) ) {
								echo '<p class="msg error">' . $errorMsg . '</p>';
							} else if ( ( $totalRows_rsSearch == 0 ) ) { // Show if recordset empty
								?>
                                <p class="msg error">No records found matching your criteria!</p>
							<?php }
						} // Show if recordset empty ?>
                    </div>
                    <!-- END ERROR MESSAGES -->

                    <div class="submit_dv">
                        <input type="submit" name="search" id="search" value="Search" class="button"/>
                    </div>

                </form>
            </div>

            <div class="as_result">
				<?php
				if ( $totalRows_rsSearch > 0 ) {
					?>
                    <div id="search_therms" class="clearfix">
                        <strong>Searching For:</strong> &nbsp;
                        <span>
                                <?php
                                echo substr( $searchpars_out, 0, - 2 );;
                                ?>
                            </span>
                    </div>
                    <h3>Search Results</h3>
                    <a name="#search-results"></a>
                    <div class="search_result">
                        <div class="amu_table search-table" data-visitor="<?php echo isset($user_id) ? $user_id : ''; ?>"
                             data-dmemail="<?php echo isset($user_email) ? $user_email : ''; ?>">
                            <table width="100%" cellpadding="0" cellspacing="0" id="archon_list_search_table"
                                   class="display">
                                <thead>
                                <tr>
                                    <td>ARCHON</td>
                                    <td>Office Title</td>
                                    <td>Action</td>
                                </tr>
                                </thead>
                                <tbody>
								<?php include 'search.php'; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
					<?php
				}
				?>
            </div>


            <!-- InstanceEndEditable -->
            <!-- main content body end -->

            <!-- third section event and articles start -->
			<?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
            <!-- third section event and articles end -->

        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="row" style="border-top: 10px #52B609 solid;"></div>
<footer class="width padding">
    <!-- 3 text content blocks -->
    <div class="row grid-12">
		<?php include( $_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/footer-content-default.php" ); ?>
    </div>
</footer>

<div class="popup_view_profile" id="popup_view_profile"></div>
<div class="ymp_loader"></div>
<div id="success_loader" class="ymp_loader_success"></div>


<?php include( $_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/footer-scripts-main.php" ); ?>

</body>
<!-- InstanceEnd --></html>