<?php

/**
 * Custom wp_nav_menu menu walker that considers the privacy setting if it encounters a category menu item
 */

class privacy_walker extends Walker_Nav_Menu
{
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;

		// Categories
		if( $item->object === "category" ){

			$cat = get_category($item->object_id);

            // Private category that the user can't view
			if( is_private_category( $cat ) && !can_view_all_archons() ){
            	return;
			}

			// No content in the category
            if( !is_category_content( $item->object_id ) ){
				return;
            }

		// Pages
		} else if( $item->object === "page" ) {

			$post_status = get_post_status( $item->object_id );
			$post_visible_to = get_post_meta( $item->object_id, '_visible_to', true );
			if ( !can_view( $post_status, $post_visible_to ) ) {
				return;
			}
		}

		// Omit empty items from the main menu

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

?>