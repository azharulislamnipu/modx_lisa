<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Models a SimpleViewer gallery
 *
 * @package svManager
 */
class SvGallery extends Gallery
{  
 /**
  * @var array gallery settings to be stored in preferences file
  */
  var $storedInPreferences = array(
  'sortOrder' => '',
  'backgroundColor' => '',
  'addLinks' => true,
  'htmlTitle' => '',
  'urlType'=>'');
  
 /**
  * @var array preferences stored in xml or preferences file
  * does not include imagePath or thumbPath
  * 'maxImageWidth' integer maximum image width in pixels, minimum 0, no upper limit
  * 'maxImageHeight' integer maximum image height in pixels, minimum 0, no upper limit
  * 'textColor' string colour of gallery text e.g. 0xFFFFFF
  * 'frameColor' string colour of gallery frame e.g. 0xFFFFFF
  * 'frameWidth' integer frame width in pixels, minimum 0, no upper limit
  * 'stagePadding' integer gallery stage padding in pixels, minimum 0, no upper limit
  * 'thumbnailColumns' integer number of thumbnail columns, minimum 0, no upper limit
  * 'thumbnailRows' integer number of thumbnail rows, minimum 0, no upper limit
  * 'navPosition' string navigation position left, right, top, bottom
  * 'title' string gallery title including permitted html
  * 'enableRightClickOpen' boolean enable opening of images with right (ctrl) click
  * 'backgroundImagePath' string background image path, can be relative or absolute?
  * 'addLinks' boolean add links to captions
  * 'navPadding' string navigation padding
  * 'vAlign' string gallery vertical alignment
  * 'hAlign' string gallery horizontal alignment
  * 'sortOrder' string image sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  * 'backgroundColor' string background colour, e.g. 0x181818;
  * 'addLinks' boolean add links to captions
  * 'htmlTitle' contents of html title tag in index file
  */
  var $preferences = array();
  
 /**
  * @var string name of image class
  */
  var $imageClass = 'SvImage';
  
 /**
  * @var array of image objects
  */
  var $imageObjects = array();
    
 /**
  * @var string default path to master gallery
  */
  var $masterPath = SV_MASTER_PATH;
  
 /**
  * @var string to prepend to paths
  */
  var $pathScheme = '';
  
 /**
  * @var integer count of files in master gallery
  */
  var $masterFileCount = SV_MASTER_FILECOUNT;
  
 /**
  * @var string default image path
  */
  var $imageDirPathRelGallery = SV_DEFAULT_IMAGE_PATH;
  
 /**
  * @var string default thumb path
  */
  var $thumbDirPathRelGallery = SV_DEFAULT_THUMB_PATH;

 /**
  * @var integer thumb width
  */ 
  var $thumbWidth = SV_THUMB_WIDTH;

 /**
  * @var integer thumb height
  */
  var $thumbHeight = SV_THUMB_HEIGHT;
  
 /**
  * @var integer thumb quality
  */
  var $thumbQuality = SV_THUMB_QUALITY;
  
 /**
  * @var string name of viewer swf file
  */
  var $viewerSwf = SV_DEFAULT_SWF;
  
 /**
  * @var string acceptable html tags in captions
  */
  var $okTags = '<font><b><i><br><br />';
  
 /**
  * @var string acceptable html tags in gallery title
  */
  var $okTitleTags = '<a><b><i><u><font><br><br />';
  
  /**
   * Constructs SvGallery
   *
   * Attempts to read local copies of preferences.txt and gallery.xml
   * Parses gallery.xml file into class settings and imageObjects array.
   * The gallery title from the parsing (which may contain html) takes precedence
   * Note that the parsing translates html entities (e.g. in the title) back to html
   * If local file has been corrupted then default values are used.
   * 
   * @access public
   * @param string gallery reference (permanent reference not the galleriesData index)
   * @param string gallery path
   * @param boolean new gallery
   *
   */  
  function SvGallery($ref, $path, $newGallery = false)
  {
    parent::Gallery();
    $this->galleryRef = $ref;
    $this->galleryPathRelSvm = $path;
    $this->pathScheme = ($this->pathScheme == '') ? '' : rtrim($this->pathScheme, '\\/').'/';
    $this->preferences['sortOrder'] = SV_DEFAULT_SORT_ORDER;
    $this->preferences['backgroundColor'] = SV_BACKGROUND_COLOR;
    $this->preferences['addLinks'] = SV_DEFAULT_ADD_LINKS;
    $this->preferences['htmlTitle'] = SV_GALLERY_TITLE;
    $this->preferences['maxImageWidth'] =  SV_MAX_IMAGE_WIDTH;
    $this->preferences['maxImageHeight'] = SV_MAX_IMAGE_HEIGHT;
    $this->preferences['textColor'] = SV_TEXT_COLOR;
    $this->preferences['frameColor'] = SV_FRAME_COLOR;
    $this->preferences['frameWidth'] = SV_FRAME_WIDTH;
    $this->preferences['stagePadding'] = SV_STAGE_PADDING;
    $this->preferences['thumbnailColumns'] = SV_THUMBNAIL_COLUMNS;
    $this->preferences['thumbnailRows'] = SV_THUMBNAIL_ROWS;
    $this->preferences['navPosition'] = SV_NAV_POSITION;
    $this->preferences['title'] = SV_GALLERY_TITLE;
    $this->preferences['enableRightClickOpen'] = SV_ENABLE_RIGHT_CLICK_OPEN;
    $this->preferences['backgroundImagePath'] = SV_BACKGROUND_IMAGE_PATH;
    $this->preferences['navPadding'] = SV_NAV_PADDING;
    $this->preferences['vAlign'] = SV_V_ALIGN;
    $this->preferences['hAlign'] = SV_H_ALIGN;
    $this->preferences['urlType'] = AV_URL_TYPE;
    if ($newGallery)
    {
      $this->addGallery($ref);
    }
    if (!file_exists($this->trimSeparator($this->galleryPathRelSvm)))
    {
      // exit
      trigger_error('cannot find gallery folder '.$this->galleryPathRelSvm, E_USER_ERROR);
    }
    $prefs = $this->readPreferences();
    if ($prefs !== false)
    {
      $this->preferences = array_merge($this->preferences, $prefs);
    }
    $this->setPathScheme();
    $xmlStruct = $this->parseXml($this->getXmlPath());
    if ($xmlStruct !== false)
    {
      $att = $this->xmlParser->parseAttributes($xmlStruct, SV_XML_SETTINGS_TAG);
      if ($att !== false)
      {
        if (isset($att['imagePath']))
        {
          $att['imagePath'] = trim($att['imagePath']);
          if ($att['imagePath'] != '')
          {
            $this->imageDirPathRelGallery = $this->getPathRelGallery($att['imagePath']);
          }
          //unset ($att['imagePath']);
        }
        if (isset($att['thumbPath']))
        {
          $att['thumbPath'] = trim($att['thumbPath']);
          if ($att['thumbPath'] != '')
          {
            $this->thumbDirPathRelGallery = $this->getPathRelGallery($att['thumbPath']);
          }
          //unset ($att['thumbPath']);
        }
        if (isset($att['backgroundImagePath']))
        {
          $att['backgroundImagePath'] = trim($att['backgroundImagePath']);
        }
        $this->preferences = array_merge($this->preferences, $att);
      }
      $this->okTags = ($this->preferences['addLinks']) ? '<font><b><i><br><br />' : '<a><u><font><b><i><br><br />';
      $this->imageObjects = $this->parseImageTags($xmlStruct);
    }
    $this->preferences['htmlTitle'] = (USE_HTML_TITLE === false) ? strip_tags($this->htmlEntityDecode->decode($this->preferences['title'])) : HTML_TITLE;
    $this->saveGallery();
  }


 /**
  * Get gallery title
  *
  * @access public
  * @return string
  */
  function getGalleryTitle()
  {
    return $this->preferences['title'];
  }
  
 /**
  * Set gallery title
  * @access public
  * @return void
  * @param string
  */
  function setGalleryTitle($title)
  {
    $this->preferences['title'] = $title;
    return;
  }
  
 /**
  * Get gallery title stripped of html
  *
  * @access public
  * @return string
  */
  function getGalleryTitleText()
  {
    return strip_tags($this->htmlEntityDecode->decode($this->preferences['title']));
  }
  
 /**
  * Get sort order
  *
  * @access public
  * @return string
  */
  function getSortOrder()
  {
    return $this->preferences['sortOrder'];
  }
  
 /**
  * Set sort order in $this->preferences
  *
  * @access public
  * @return void
  * @param string sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  */
  function setSortOrder($sortOrder)
  {
    $this->preferences['sortOrder'] = $sortOrder;
    return;
  }

 /**
  * Get xml path
  *
  * @access private
  * @return string calculated xml path
  */
  function getXmlPath()
  {
    return $this->galleryPathRelSvm.SV_XML_FILE;
  }

  /**
   * Set captions in image data
   *
   * @access public
   * @return  boolean true on success
   * @param array captions
   */
  function setImageCaptions($post)
  {
    $captions = $post['cap'];
    $i=0;
    foreach (array_keys($this->imageObjects) as $key)
    {
      $this->imageObjects[$key]->setCaption($captions[$key], $i, $this->okTags);
      $i++;
    }
    return true;
  }
     
  /** 
   * Get stored image data from class
   *
   * @access public
   * @return array
   */
   function getImageObjects()
   {
     return $this->imageObjects;
   }


  /**
   * Extract file names and captions from xml structured array
   *
   * Any empty image tags are silently ignored
   * @access private
   * @return array of image objects
   * @param values array as generated by xml_parse_into_struct
   */
  function parseImageTags($xmlStruct)
  {
    $imageObjects = array();
    $vals = $xmlStruct['vals'];
    $imageTagOpen = false;
    $fileName = '';
    $caption = '';
    foreach ($vals as $tagInfo)
    {
      $imageTagOpen = ((strtolower($tagInfo['tag']) == 'image' && $tagInfo['type'] == 'open') || $imageTagOpen);
      if (strtolower($tagInfo['tag']) == 'filename')
      {
        $fileName = $this->htmlEntityDecode->decode($tagInfo['value']);
      }
      if (strtolower($tagInfo['tag']) == 'caption' && isset($tagInfo['value']))
      {
        $caption = strip_tags($tagInfo['value'], $this->okTags);
      }
      if ($imageTagOpen && strtolower($tagInfo['tag']) == 'image' && $tagInfo['type'] == 'close')
      {
        if ($fileName != '')
        {
          $imageSize = @getimagesize($this->galleryPathRelSvm.$this->imageDirPathRelGallery.$fileName);
          if ($imageSize != false)
          {
            $imageObjects[] = new $this->imageClass($this->galleryPathRelSvm, $this->imageDirPathRelGallery.$fileName, $this->thumbDirPathRelGallery.$fileName, $caption);
          }
          $fileName = '';
          $caption = '';
          $imageTagOpen = false;
        }
      }
    }
    return $imageObjects;
  }

  /**
   * Construct xml string and write to file
   *
   * @access private
   * @param string file path
   * @return boolean
   */
   function writeXml($xmlPath)
   {
     $imageObjects = $this->imageObjects;
     $attributes = array_diff_key($this->preferences, $this->storedInPreferences);
     $addLinks = $this->preferences['addLinks'];
     $openTag = (SV_UNDERLINE_CAPTION_LINKS) ? '<u>' : '';
     $closeTag = (SV_UNDERLINE_CAPTION_LINKS) ? '</u>' : '';
     $relImagePath = $this->getImageDirPathRelSvm();
     $xml = '<?xml version="1.0" encoding="UTF-8"'. "?>";
     $xml .= '<'.SV_XML_SETTINGS_TAG;
     $attributes['title'] = htmlspecialchars($attributes['title'], ENT_QUOTES, 'UTF-8');
     $attributes['backgroundImagePath'] = htmlspecialchars($attributes['backgroundImagePath'],  ENT_QUOTES, 'UTF-8');
     $attributes['imagePath'] = $this->convertRelToScheme($this->imageDirPathRelGallery, $this->galleryPathRelSvm, $this->pathScheme);
     $attributes['thumbPath'] = $this->convertRelToScheme($this->thumbDirPathRelGallery, $this->galleryPathRelSvm, $this->pathScheme);
    foreach ($attributes as $name => $value)
    {
       $xml .= '
 '.$name.' = "'.$value.'"';
    }
    $xml .= '>
';
    foreach ($imageObjects as $key=>$imageObject)
    {
      $fileName = htmlspecialchars($imageObject->getImageFileName(), ENT_QUOTES, 'UTF-8');
      $caption = $imageObject->getCaption();
      $xml .= '<image>
  <filename>'.$fileName.'</filename>
';
      if (strlen($caption) == 0)
      {
        $xml .= '</image>
';
        continue;
      }
      if ($addLinks)
      {
        $imageUrl = $this->convertRelToScheme($imageObject->getImagePathRelGallery(), $this->galleryPathRelSvm, $this->pathScheme);
        $xml .= '  <caption><![CDATA[<a href="'.$imageUrl.'" target="_blank">'.$openTag.$caption.$closeTag.'</a>]]></caption>
';
      }
      else
      {
        $xml .= '  <caption><![CDATA['.$caption.']]></caption>
';
      }
      $xml .= '</image>
';
    }
    $xml .= '</'.SV_XML_SETTINGS_TAG.'>
';
    
    if(@!file_put_contents($xmlPath, $xml, FPC_LOCK))
    {
      trigger_error('unable to write xml to file '.$xmlPath, E_USER_NOTICE);
      return false;
    }
    return true; 
  }

 /** Clean data from customize form and update class properties
  *
  * @access public
  * @return array
  * @param array as $_POST
  */
  function customize($newSettings)
  {
    $newSettings = array_map('trim', $newSettings);
    $this->preferences['title'] = strip_tags($newSettings['title'], $this->okTitleTags);
    $this->preferences['htmlTitle'] = (USE_HTML_TITLE === false) ? strip_tags($newSettings['title']) : HTML_TITLE;
    $this->preferences['frameWidth'] = max(0, $newSettings['frameWidth']);
    $this->preferences['stagePadding'] = max(0, $newSettings['stagePadding']);
    $this->preferences['navPadding'] = max(0, $newSettings['navPadding']);
    $this->preferences['thumbnailColumns'] = max(0, $newSettings['thumbnailColumns']);
    $this->preferences['thumbnailRows'] = max(0, $newSettings['thumbnailRows']);
    $this->preferences['navPosition'] = $newSettings['navPosition'];
    $this->preferences['maxImageWidth'] = max(0, $newSettings['maxImageWidth']);
    $this->preferences['maxImageHeight'] = max(0, $newSettings['maxImageHeight']);
    $this->preferences['enableRightClickOpen'] = (isset($newSettings['enableRightClickOpen'])) ? 'true' : 'false';
    $this->preferences['textColor'] = $this->cleanHex($newSettings['textColor'], 6);
    $this->preferences['frameColor'] = $this->cleanHex($newSettings['frameColor'], 6);
    $this->preferences['addLinks'] = isset($newSettings['addLinks']);
    $this->preferences['backgroundColor'] = $this->cleanHex($newSettings['backgroundColor'], 6);
    $this->preferences['backgroundImagePath'] = $newSettings['backgroundImagePath'];
    $this->preferences['urlType'] = $newSettings['urlType'];
    $validAlignments = array('center', 'top', 'bottom', 'left', 'right');
    $alignments = explode("_", $newSettings['alignment']);
    $this->preferences['vAlign'] = (in_array(strtolower($alignments[0]), $validAlignments)) ? strtolower($alignments[0]) : 'center';
    $this->preferences['hAlign'] = (in_array(strtolower($alignments[1]), $validAlignments)) ? strtolower($alignments[1]) : 'center';
    return true;
  }
}


?>