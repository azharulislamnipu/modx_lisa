<?php
require_once('../includes/header.php');
?>
<h3><?php echo SITE_NAME.' '.T_SITEMAP;?>:</h3>

<h4><?php echo T_SUB_MENU;?></h4>
<ul>
<?php
	foreach($resultSitemap as $row){
		 echo '<li><a title="'.htmlentities($row['description'], ENT_QUOTES, CHARSET).'" href="'.catURL($row['friendlyName'],$row['parent']).'">'.$row['name'].'</a></li>';
	}
?>
</ul>
<br	/>

<h4><?php echo T_LINKS;?>:</h4>
<ul>
    <?php if(FRIENDLY_URL) {?>
	    <li><a href="<?php echo SITE_URL."/".u(T_ADV_SEARCH);?>.htm"><?php echo T_ADV_SEARCH;?></a></li>
	    <li><a href="<?php echo SITE_URL."/".u(T_SITEMAP);?>.htm"><?php echo T_SITEMAP;?></a></li>
	    <li><a href="<?php echo SITE_URL."/".u(T_PRIVACY);?>.htm"><?php echo T_PRIVACY;?></a></li>
    <?php }else { ?>
        <li><a href="<?php echo SITE_URL;?>/content/search.php"><?php echo T_ADV_SEARCH;?></a></li>
        <li><a href="<?php echo SITE_URL;?>/content/site-map.php"><?php echo T_SITEMAP;?></a></li>
	    <li><a href="<?php echo SITE_URL;?>/content/privacy.php"><?php echo T_PRIVACY;?></a></li>
    <?php } ?>
    <li><a href="<?php echo SITE_URL."/".contactURL();?>"><?php echo T_CONTACT;?></a></li>
    <li><a href="<?php echo SITE_URL."/".contactURL()."?subject=".T_SUGGEST;?>"><?php echo T_SUGGEST;?></a></li>
    <li><a href="<?php echo SITE_URL.newURL();?>"><?php echo T_NEW_ITEM;?></a></li>
	<li><a href="<?php echo SITE_URL;?>/admin/"><?php echo T_ADMIN;?></a></li>
</ul>

<?php
require_once('../includes/footer.php');
?>
