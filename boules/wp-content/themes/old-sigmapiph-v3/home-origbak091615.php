<?php get_header(); ?>


            <!-- 3 round images -->
            <div class="row grid-12 margin-top-medium">
                <section class="span-4 align-center"><div class="inner">
                    <a href="#"><img src="assets/images/round-centennial.jpg" alt="" class="width-80"></a>
                    <h2>Centennial Celebration</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in orci felis. Morbi vel dolor viverra erat scelerisque pretium.</p>
                    <p><a href="#"><strong>Keep reading</strong></a></p>
                </div></section>
                <section class="span-4 align-center margin-top-large-mobile"><div class="inner">
                    <a href="#"><img src="assets/images/round-sigma-pi-phi-history.jpg" alt="" class="width-80"></a>
                    <h2>Our History</h2>
                    <p>Maecenas aliquet, metus eu vulputate facilisis, purus eros volutpat nibh, at viverra ante elit id nullam vestibulum aliquet auctor.</p>
                    <p><a href="#"><strong>Learn more</strong></a></p>
                </div></section>
                <section class="span-4 align-center margin-top-large-mobile"><div class="inner">
                    <a href="#"><img src="assets/images/round-archives.jpg" alt="" class="width-80"></a>
                    <h2>Journal Archives</h2>
                    <p>Quisque vehicula, posuere aliquet diam mollis id. Nullam vestibulum aliquet auctor. Donec sed hendrerit eros, at aliquam magna.</p>
                    <p><a href="#"><strong>Browse archives</strong></a></p>
                </div></section>
            </div>

        </div>
    </div>

    <!-- Events and Press Releases -->
    <section id="events">
        <div class="width padding">

            <div class="row grid-12">

                <div class="span-4"><div class="inner">
                    <h2>Events</h2>
                    <ul class="dates">
                        <li>
                            <strong><a href="#">52nd Grand Boulé</a></strong>
                            Duis massa mauris, ultrices eget scelerisque ut, ornare in metus. Maecenas placerat arcu.
                            <span class="date"><strong>11</strong>JUL</span>
                        </li>
                        <li>
                            <strong><a href="#">Halloween Costume Ball</a></strong>
                            Duis massa mauris, ultrices eget scelerisque ut, ornare in metus. Maecenas placerat.
                            <span class="date"><strong>31</strong>OCT</span>
                        </li>
                        <li>
                            <strong><a href="#">Alpha Boulé Gala</a></strong>
                            Massa mauris, ultrices eget scelerisque ut, ornare in metus. Maecenas placerat imperdiet.
                            <span class="date"><strong>20</strong>JUL</span>
                        </li>
                        <li><p><strong><a href="#">More events <i class="fa fa-chevron-right"></i></a></strong></p></li>
                    </ul>
                </div></div>

                <div class="span-8"><div class="inner">
                    <h2>Press Releases</h2>
                    <ul class="lines">
                        <li>
                            <p><strong><a href="#">Archon Craig Thompson Is Elected to International Association of Defense Counsel</a></strong><br>
                            At its annual meeting in Vienna, Austria, on August 5, 2014, the International Association of Defense Counsel (IADC) elected new members to its board of directors including Gamma Archon Craig A. Thompson. Archon Thompson will serve a three-year term.</p>
                        </li>
                        <li>
                            <p><strong><a href="#">Archon Michael A. Lawson Is Appointed by President Obama as U.S. Ambassador to the International Civil Aviation Organization</a></strong><br>
                            Pasadena’s Gamma Zeta Boulé Archon Michael A. Lawson was confirmed by the United States Senate on July 21, 2014, as United States Ambassador to the United Nations’ International Civil Aviation Organization (ICAO). The international aviation body was formed in 1944 and assists...</p>
                        </li>
                        <li>
                            <p><strong><a href="#">Archon Craig Thompson Is Elected to International Association of Defense Counsel</a></strong><br>
                            At its annual meeting in Vienna, Austria, on August 5, 2014, the International Association of Defense Counsel (IADC) elected new members to its board of directors including Gamma Archon Craig A. Thompson. Archon Thompson will serve a three-year term.</p>
                        </li>
                        <li><p><strong><a href="#">More press releases <i class="fa fa-chevron-right"></i></a></strong></p></li>
                    </ul>
                </div></div>

            </div>

        </div>
    </section>

<?php get_footer(); ?>