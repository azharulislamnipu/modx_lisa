<div id="footer">

  <div id="copyright">
    &copy; 2006 - <?php echo date("Y"); ?> Sigma Pi Phi Fraternity
  </div>

</div>
<!-- Piwik -->
<a href="http://piwik.org" title="open source Google Analytics" onclick="window.open(this.href);return(false);">
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.sigmapiphi.org/piwik/" : "http://www.sigmapiphi.org/piwik/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
piwik_action_name = '';
piwik_idsite = 1;
piwik_url = pkBaseURL + "piwik.php";
piwik_log(piwik_action_name, piwik_idsite, piwik_url);
</script>
<object><noscript><p>open source Google Analytics <img src="http://www.sigmapiphi.org/piwik/piwik.php?idsite=1" style="border:0" alt=""/></p></noscript></object></a>
<!-- End Piwik Tag -->
