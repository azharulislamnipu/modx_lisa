<?php

require_once 'ArchonBean.php';
require_once 'ArchonDuesBean.php';
//require_once '/opt/lampp/htdocs/modx_lisa/home/index.php';
define('MODX_API_MODE', true); // Allows this class to use the MODx API

class ArchonDuesDatabaseManager {

    const OMIT_STATUS = '"Dropped", "Deceased"';
    const COMPLETE_TRANSACTION_ID = 'complete';
    const SUBMIT_TRANSACTION_ID = 'submit';
    const EDIT_TRANSACTION_ID = null;

    private $modx = null;

    /** Initialize the database manager */
    public function __construct(&$modx = null){
        if($modx == null){
            die('You must pass a valid $modx object reference to the ArchonDuesDatabaseManager.');
        }
        $this->modx = $modx;
    }

    /** Load the Boule roster for a given year */
    public function getArchonRoster($year = -1, $bouleCode = ''){

        $roster = array();

        if($year > -1 && !empty($bouleCode)){

            // Get the archons for the given Boule
            $results = $this->modx->db->query('
                SELECT
                        G.CHAPTERID,
                        G.STATUS,
                        G.CUSTOMERID,
                        G.FULLNAME,
                        G.PREFIX,
                        G.FIRSTNAME,
                        G.MIDDLEINITIAL,
                        G.LASTNAME,
                        G.SUFFIX,
                        G.JOINDATE,
                        G.BIRTHDATE,
                        G.ADDRESS1,
                        G.ADDRESS2,
                        G.CITY,
                        G.STATECD,
                        G.ZIP,
                        G.HOMEPHONE,
                        G.WORKPHONE,
                        G.EMAIL,
                        G.ALTEMAIL,
                        G.REGIONCD,
                        G.REGIONCD,
                        G.COMMITTEESTATUSSTT,
                        G.CPOSITION,
                        G.CUSTOMERTYPE,
                        A.MOBILEPHONE,
                        D.dues_id,
                        D.transaction_id,
                        D.dues_start_dt,
                        D.dues_amt,
                        D.addl_amt,
                        D.status_req,
                        D.dues_cmt,
                        D.create_dt,
                        D.create_by,
                        D.last_update,
                        D.update_by

                FROM vw_gramm_archons G

                JOIN spp_archons A
                ON	G.CUSTOMERID = A.CUSTOMERID

                LEFT JOIN spp_dues D
                ON	G.CUSTOMERID = D.CUSTOMERCD
                AND     YEAR(D.dues_start_dt) = ' . $this->modx->db->escape($year) . '

                WHERE
                        G.CHAPTERID = "' . $this->modx->db->escape($bouleCode) . '"
                        AND G.STATUS NOT IN (' . ArchonDuesDatabaseManager::OMIT_STATUS . ')

                ORDER BY
                        G.LASTNAME
            ');

            if($this->modx->db->getRecordCount($results) >= 1) {
                while($row = $this->modx->db->getRow($results)){

                    // Load the archon
                    $archon = new ArchonBean(array(
                        'customerid' => $row['CUSTOMERID'],
                        'customertype' => $row['CUSTOMERTYPE'],
                        'chapterid' => $row['CHAPTERID'],
                        'regioncd' => $row['REGIONCD'],
                        'status' => $row['STATUS'],
                        'fullname' => $row['FULLNAME'],
                        'prefix' => $row['PREFIX'],
                        'first_name' =>$row['FIRSTNAME'],
                        'middle_name' => $row['MIDDLEINITIAL'],
                        'last_name' => $row['LASTNAME'],
                        'suffix' => $row['SUFFIX'],
                        'address1' => $row['ADDRESS1'],
                        'address2' => $row['ADDRESS2'],
                        'birth_date' => $row['BIRTHDATE'],
                        'join_date' => $row['JOINDATE'],
                        'city' => $row['CITY'],
                        'state' => $row['STATECD'],
                        'zip' => $row['ZIP'],
                        'phone_home' => $row['HOMEPHONE'],
                        'phone_work' => $row['WORKPHONE'],
                        'phone_mobile' => $row['MOBILEPHONE'],
                        'email_home' => $row['EMAIL'],
                        'email_work' => $row['ALTEMAIL'],
                        'officer_position' => $row['CPOSITION'],
                        'officer_status' => $row['COMMITTEESTATUSSTT']
                    ));

                    // Now load their dues record if it exists
                    if($row['dues_id']){
                        $dues = array(
                            'id' => $row['dues_id'],
                            'transaction_id' => $row['transaction_id'],
                            'customercd' => $archon->customerid,
                            'archon' => $archon,
                            'dues_start_date' => $row['dues_start_dt'],
                            'dues_amount' => $row['dues_amt'],
                            'dues_amount_additional' => $row['addl_amt'],
                            'comment' => $row['dues_cmt'],
                            'status_current' => $archon->status,
                            'status_requested' => $row['status_req'],
                            'created_date' => $row['create_dt'],
                            'created_by' => $row['create_by'],
                            'updated_date' => $row['last_update'],
                            'updated_by' => $row['update_by']
                        );
                    } else {
                        $dues = array('archon' => $archon);
                    }
                    $roster[$archon->customerid] = new ArchonDuesBean($dues);
                }
            }
        }
        return $roster;

    }

    /**
     * Returns the pay type for a given boule (determines if Roster is paid by e-check)
     * @param type $chapterid
     * @return type
     */
    public function getBoulePayType($chapterid){
        $paytype = false;
        if($chapterid) {
            $paytype = $this->modx->db->getValue($this->modx->db->select('PAYTYPE', $this->modx->getFullTableName('spp_boule'), "CHAPTERID = '" . $this->modx->db->escape($chapterid) . "'"));
        }
        return $paytype;
    }

    /**
     * Returns the officers for a given Boule
     * @param type $chapterid
     * @return \ArchonBean
     */
    public function getBouleOfficers($chapterid) {

        $officers = array();
        if(isset($chapterid)) {

            // Get the archons for the given Boule
            $results = $this->modx->db->query('
                SELECT
                    spp_officers.CPOSITION,
                    spp_officers.COMMITTEESTATUSSTT,
                    spp_archons.FIRSTNAME,
                    spp_archons.LASTNAME,
                    spp_archons.SUFFIX,
                    spp_archons.EMAIL
                FROM
                    spp_officers
                    INNER JOIN spp_archons ON (spp_officers.CUSTOMERCD = spp_archons.CUSTOMERID)
                WHERE
                    spp_officers.CHAPTERCD = "'.$this->modx->db->escape($chapterid).'" AND
                    spp_officers.COMMITTEESTATUSSTT = "active" AND
                    spp_officers.CPOSITION NOT LIKE "%egio%"
            ');

            if($this->modx->db->getRecordCount($results) >= 1) {
                while(($row = $this->modx->db->getRow($results))){
                    // Load the officer
                    $officers[$row['CPOSITION']] = new ArchonBean(array(
                        'chapterid' => $chapterid,
                        'first_name' =>$row['FIRSTNAME'],
                        'last_name' => $row['LASTNAME'],
                        'suffix' => $row['SUFFIX'],
                        'email_home' => $row['EMAIL'],
                        'officer_position' => $row['CPOSITION'],
                        'officer_status' => $row['COMMITTEESTATUSSTT']
                    ));
                }
            }
        }
        return $officers;
    }

    public function getOfficerInfo($customercd){

        $officer = null;
        if(isset($customercd)) {

            // Get the archons for the given Boule
            $results = $this->modx->db->query('
                SELECT
                    CHAPTERID,
                    FIRSTNAME,
                    LASTNAME,
                    ADDRESS1,
                    CITY,
                    STATECD,
                    ZIP,
                    HOMEPHONE,
                    MOBILEPHONE,
                    EMAIL,
                    O.CPOSITION
                FROM spp_archons A

                LEFT JOIN spp_officers O
                ON A.CUSTOMERID = O.CUSTOMERCD

                WHERE
                    CUSTOMERID = ' . $this->modx->db->escape($customercd) . '
            ');
            if($this->modx->db->getRecordCount($results) >= 1) {
                if(($row = $this->modx->db->getRow($results))){
                    // Load the officer
                    $officer = new ArchonBean(array(
                        'chapterid' => $row['CHAPTERID'],
                        'first_name' =>$row['FIRSTNAME'],
                        'last_name' => $row['LASTNAME'],
                        'address1' => $row['ADDRESS1'],
                        'city' => $row['CITY'],
                        'state' => $row['STATECD'],
                        'zip' => $row['ZIP'],
                        'phone_home' => $row['HOMEPHONE'],
                        'phone_mobile' => $row['MOBILEPHONE'],
                        'email_home' => $row['EMAIL'],
                        'officer_position' => $row['CPOSITION']
                    ));
                }
            }
        }
        return $officer;
    }



    /** Gets the Dues Summary for a given boule and year */
    public function getDuesRequested($year = -1, $bouleName = ''){

        $duesRequested = array();
        if($year > -1 && !empty($bouleName)){

            // Get the archons for the given Boule
            $results = $this->modx->db->query('
                SELECT
                        SUM(dues_amt + addl_amt) AS dues_total,
                        COUNT(dues_id) AS status_count,
                        status_req

                FROM spp_dues D

                JOIN spp_archons A
                ON	A.CUSTOMERID = D.CUSTOMERCD

                WHERE
                        CHAPTERID = "' . $this->modx->db->escape($bouleName) . '"
                        AND YEAR(dues_start_dt) = ' . $this->modx->db->escape($year) . '

                GROUP BY status_req
		ORDER BY status_req
            ');

            if($this->modx->db->getRecordCount($results) >= 1) {
                while($row = $this->modx->db->getRow($results)){

                    // Load the dues requested
                    $duesRequested[$row['status_req']] = array(
                        'dues_total' => $row['dues_total'],
                        'status_count' => $row['status_count']
                    );
                }
            }
        }
        return $duesRequested;

    }

    /** Gets the total dues for a Boule */
    public function getBouleDuesTotal($year = -1, $bouleName = ''){

        $amount = -1;
        if($year > -1 && !empty($bouleName)){

            // Get the archons for the given Boule
            $results = $this->modx->db->query('
                SELECT
                       SUM(dues_amt + addl_amt) AS dues_total,
                       CHAPTERID

                FROM spp_dues D

                JOIN spp_archons A
                ON	A.CUSTOMERID = D.CUSTOMERCD

                WHERE
                        CHAPTERID = "' . $this->modx->db->escape($bouleName) . '"
                        AND YEAR(dues_start_dt) = ' . $this->modx->db->escape($year) . '

                GROUP BY CHAPTERID
            ');

            if($this->modx->db->getRecordCount($results) >= 1) {
                if(($row = $this->modx->db->getRow($results))){
                    $amount = $row['dues_total'];
                }
            }
        }
        return $amount;

    }


    /**
        *  Checks if there's a concurrency error
        *  @param Array of database insert values
        *  @return mixed Integer dues_id on successful INSERT, true on successful UPDATE, false on failure
        */
    public function isConcurrencyError($data = null){

            $isConcurrentError = true;
            $operation = empty($data['last_update']) ? 'INSERT' : 'UPDATE';

            switch($operation){
                // Check if a record already exists for the dues year
                case 'INSERT':
                    if(isset($data['CUSTOMERCD']) && isset($data['dues_start_dt'])){
                        $result = $this->modx->db->select('dues_id', $this->modx->getFullTableName('spp_dues'), "CUSTOMERCD = " . $this->modx->db->escape($data['CUSTOMERCD']) . " AND dues_start_dt = '" . $this->modx->db->escape($data['dues_start_dt']) . "'");
                        $isConcurrentError = $this->modx->db->getRecordCount($result) !== 0;
                    }
                    break;

                // Check if the record isn't in the same state as when it was last updated
                case 'UPDATE':
                    if(isset($data['CUSTOMERCD']) && isset($data['dues_start_dt']) && isset($data['last_update'])){
                        $result = $this->modx->db->select('dues_id', $this->modx->getFullTableName('spp_dues'), "CUSTOMERCD = " . $this->modx->db->escape($data['CUSTOMERCD']) . " AND last_update = '" . $this->modx->db->escape($data['last_update']) . "' AND dues_start_dt = '" . $this->modx->db->escape($data['dues_start_dt']) . "'");
                        $isConcurrentError = $this->modx->db->getRecordCount($result) === 0;
                    }
                    break;

            }
            return $isConcurrentError;
    }



    /**
        *  INSERTs or UPDATEs an spp_dues record for an Archon
        *  @param Array of database insert values
        *  @return mixed Integer dues_id on successful INSERT, true on successful UPDATE, false on failure
        */
    public function recordArchonDues($data = null){

            $success = false;
            if(isset($data['CUSTOMERCD']) && isset($data['dues_start_dt'])){

                // Check if this is an insert or update
                $result = $this->modx->db->select('dues_id, transaction_id', $this->modx->getFullTableName('spp_dues'), "CUSTOMERCD = " . $this->modx->db->escape($data['CUSTOMERCD']) . " AND dues_start_dt = '" . $this->modx->db->escape($data['dues_start_dt']) . "'");
                if($this->modx->db->getRecordCount($result)) {
                        if(($row = $this->modx->db->getRow($result))){
                            $data['dues_id'] = $row['dues_id'];
                            $data['transaction_id'] = $row['transaction_id'];
                            $success = $this->updateArchonDues($data);
                        }
                } else {
                        $success = $this->insertArchonDues($data);
                }
            }
            return $success;
    }

     /**
	 *  Add an spp_dues record for an Archon
	 *  @param Array of database insert values
	 *  @return mixed Integer dues_id on success, false on failure
	 */
    private function insertArchonDues($data = null){

        $success = false;
        if(isset($data['CUSTOMERCD']) && $data['CUSTOMERCD'] !== -1){

            // Insert the row
            $success = $this->modx->db->insert(
                array(
                        'CUSTOMERCD' => $this->modx->db->escape($data['CUSTOMERCD']),
                        'dues_start_dt'	=> $this->modx->db->escape($data['dues_start_dt']),
                        'dues_amt'	=> $this->modx->db->escape($data['dues_amt']),
                        'addl_amt'	=> $this->modx->db->escape($data['addl_amt']),
                        'status_current' => $this->modx->db->escape($data['status_current']),
                        'status_req' => $this->modx->db->escape($data['status_req']),
                        'dues_cmt'	=> $this->modx->db->escape($data['dues_cmt']),
                        'create_dt'	=> $this->modx->db->escape($data['create_dt']),
                        'create_by'	=> $this->modx->db->escape($data['create_by']),
                        'last_update'	=> $this->modx->db->escape($data['update_dt'])
                ),
                $this->modx->getFullTableName('spp_dues'));

        }
	return $success;
    }

    /**
	 *  Updates an spp_dues record for an Archon
	 *  @param Array of database update values
	 *  @return boolean
	 */
    private function updateArchonDues($data = null){

        $success = false;
        if(isset($data['dues_id']) && $data['dues_id'] !== -1 && $data['transaction_id'] === null){

            $success = $this->modx->db->update(
                array(
                    'CUSTOMERCD' => $this->modx->db->escape($data['CUSTOMERCD']),
                    'dues_start_dt' => $this->modx->db->escape($data['dues_start_dt']),
                    'dues_amt'	=> $this->modx->db->escape($data['dues_amt']),
                    'addl_amt'	=> $this->modx->db->escape($data['addl_amt']),
                    'status_current' => $this->modx->db->escape($data['status_current']),
                    'status_req' => $this->modx->db->escape($data['status_req']),
                    'dues_cmt'	=> $this->modx->db->escape($data['dues_cmt']),
                    'update_by'	=> $this->modx->db->escape($data['update_by']),
                    'last_update' => $this->modx->db->escape($data['update_dt'])
                ),
                $this->modx->getFullTableName('spp_dues'), 'dues_id='. $this->modx->db->escape($data['dues_id']));
		}
        return $success;
    }


    /**
        *  Updates the spp_dues transaction ID for a given boule and year
        *  @param int year of dues roster
        *  @param String Boule code
        *  @param String Transaction ID value
        *  @return boolean success
        */
    public function updateTransactionId($data){

        $success = false;

        if(isset($data['year']) &&
           isset($data['last_update']) &&
           isset($data['update_by']) &&
           isset($data['boulename'])){

            // Handle special case of setting transaction_id to null
            $transaction_id = !empty($data['transaction_id']) ? '"' . $this->modx->db->escape($data['transaction_id']) . '"' : 'null';
            $success = $this->modx->db->query('
                UPDATE spp_dues DUES
                INNER JOIN (
                    SELECT
                        SUB_D.dues_id
                    FROM
                        spp_dues SUB_D
                    JOIN spp_archons A
                    ON	A.CUSTOMERID = SUB_D.CUSTOMERCD
                    WHERE
                        CHAPTERID = "' . $this->modx->db->escape($data['boulename']) . '"
                        AND YEAR(SUB_D.dues_start_dt) = ' . $this->modx->db->escape($data['year']) . '
                ) ARCHONS
                SET transaction_id = ' . $transaction_id . ',
                    last_update =  "' . $this->modx->db->escape($data['last_update']) . '",
                    update_by =  "' . $this->modx->db->escape($data['update_by']) . '"
                WHERE
                        ARCHONS.dues_id = DUES.dues_id
            ');
	}
        return $success;
    }


    /**
        *  Gets the current transaction ID status for a boule
        *  @param int year of dues roster
        *  @param String Boule code
        *  @return mixed String transaction ID on success, false on fail
        */
    public function getBouleTransactionId($year, $bouleName){

        $transaction_id = false;
        if(isset($year) && isset($bouleName)){

            $result = $this->modx->db->query('
                SELECT
                    transaction_id
                FROM
                    spp_dues SUB_D
                JOIN spp_archons A
                ON	A.CUSTOMERID = SUB_D.CUSTOMERCD
                WHERE
                    CHAPTERID = "' . $this->modx->db->escape($bouleName) . '"
                    AND YEAR(SUB_D.dues_start_dt) = ' . $this->modx->db->escape($year) . '
                GROUP BY
                    transaction_id
            ');
            if($this->modx->db->getRecordCount($result) === 1) {
                if(($row = $this->modx->db->getRow($result))){
                    $transaction_id = $row['transaction_id'];
                }
            }
	}
        return $transaction_id;
    }

    /**
        *  Gets the ID of the officer in the UPDATE_BY field
        *  @param int year of dues roster
        *  @param String Boule name
        *  @return mixed int update_by on success, false on fail
        */
    public function getOfficerUpdateBy($year, $bouleName){

        $update_by = false;
        if(isset($year) && isset($bouleName)){

            $result = $this->modx->db->query('
                SELECT
                    update_by
                FROM
                    spp_dues SUB_D
                JOIN spp_archons A
                ON A.CUSTOMERID = SUB_D.CUSTOMERCD
                WHERE
                    CHAPTERID = "' . $this->modx->db->escape($bouleName) . '"
                    AND YEAR(SUB_D.dues_start_dt) = ' . $this->modx->db->escape($year) . '
                LIMIT 1
            ');
            if($this->modx->db->getRecordCount($result) === 1) {
                if(($row = $this->modx->db->getRow($result))){
                    $update_by = $row['update_by'];
                }
            }
	}
        return $update_by;
    }


    /**
        *  Determins if a roster can be completed
        *  @param int year of dues roster
        *  @param String Boule code
        *  @return boolean
        */
    public function isRosterCompletable($year, $bouleCode, $bouleName){
        if(isset($year) && isset($bouleCode)){
            return $this->isRosterStateable($year, $bouleCode, $bouleName, 'IS NULL');
        }
    }

    /**
        *  Determins if a roster can be submitted
        *  @param int year of dues roster
        *  @param String Boule code
        *  @return boolean
        */
    public function isRosterSubmitable($year, $bouleCode, $bouleName){
        if(isset($year) && isset($bouleCode)){
            return $this->isRosterStateable($year, $bouleCode, $bouleName, '= "' . ArchonDuesDatabaseManager::COMPLETE_TRANSACTION_ID . '"');
        }
    }

    /**
        *  Determins if a roster is in a valid state to have a complete of finalize action preformed
        *  @param int year of dues roster
        *  @param String Boule code No spaces, camel case (e.g. "EpsilonNu")
	 	*  @param String Boule name Contains spaces (e.g. "Epsilon Nu")
        *  @param String comparison operator to determine if roster is completable or submitable
        *  @return boolean
        */
    private function isRosterStateable($year, $bouleCode, $bouleName, $transaction_id){

        $dues_count = -1;
        $archon_count = -1;

        $result = $this->modx->db->query('
            SELECT
                COUNT(*) AS DUES_COUNT
            FROM
                spp_dues D
            JOIN
				vw_gramm_archons G
				ON G.CUSTOMERID = D.CUSTOMERCD
				AND G.STATUS NOT IN (' . ArchonDuesDatabaseManager::OMIT_STATUS . ')
            WHERE
                G.CHAPTERID = "' . $this->modx->db->escape($bouleCode) . '"
                AND YEAR(dues_start_dt) = ' . $this->modx->db->escape($year) . '
                AND transaction_id ' . $transaction_id
        );
        if($this->modx->db->getRecordCount($result) === 1) {
            if(($row = $this->modx->db->getRow($result))){
                $dues_count = $row['DUES_COUNT'];
            }
        }

        $result = $this->modx->db->query('
            SELECT
                COUNT(*) AS ARCHON_COUNT
            FROM
                vw_gramm_archons
            WHERE
                CHAPTERID = "' . $this->modx->db->escape($bouleCode) . '"
                AND STATUS NOT IN (' . ArchonDuesDatabaseManager::OMIT_STATUS . ')
        ');
        if($this->modx->db->getRecordCount($result) === 1) {
            if(($row = $this->modx->db->getRow($result))){
                $archon_count = $row['ARCHON_COUNT'];
            }
        }
        return $dues_count > 0 && $archon_count > 0 && $dues_count === $archon_count;
    }

}

?>
