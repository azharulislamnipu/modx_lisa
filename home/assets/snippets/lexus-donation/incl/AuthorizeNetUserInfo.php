<?php

/**
 * Represents a user's info required by an Authorize.net transaction
 * Author: Pat Heard {http://fullahead.org}
 */
 
class AuthorizeNetUserInfo {

	private $amount = 0;
	private $card_num = '';
	private $exp_month = '';
	private $exp_year = '';
	private $first_name = '';
	private $last_name = '';
	private $address = '';
	private $city = '';
	private $state = '';
	private $zip = '';
	private $card_code = '';
	private $phone = '';
	private $email = '';
        
	private $billing_phone = '';
	private $billing_email = '';        
        private $billing_first_name = '';    
        private $billing_last_name = '';    
        private $billing_address = '';    
        private $billing_city = '';    
        private $billing_state = '';    
        private $billing_zip = '';   
        
        private $payment_type = '';
	private $messages = array();
	
	/** Construction powers activate! */
    public function __construct(array $args = null){
		if(isset($args)){
		
			$args = array_merge(array(
				'amount' => 0,
				'card_num' => '',
				'exp_month' => '',
				'exp_year' => '',
				'first_name' => '',
				'last_name' => '',
				'address' => '',
				'city' => '',
				'state' => '',
				'zip' => '',
				'card_code' => '',
				'phone' => '',
				'email' => '',
				'billing_phone' => '',
				'billing_email' => '',
                                'billing_first_name' => '', 
                                'billing_last_name' => '', 
                                'billing_address' => '', 
                                'billing_city' => '', 
                                'billing_state' => '', 
                                'billing_zip' => '',
                                'payment_type' => 'credit'
			), $args);
		
			$this->amount = $args['amount'];
			$this->card_num = $args['card_num'];
			$this->exp_month = $args['exp_month'];
			$this->exp_year = $args['exp_year'];
			$this->first_name = $args['first_name'];
			$this->last_name = $args['last_name'];
			$this->address = $args['address'];
			$this->city = $args['city'];
			$this->state = $args['state'];
			$this->zip = $args['zip'];
			$this->card_code = $args['card_code'];
			$this->phone = $args['phone'];
			$this->email = $args['email'];                        
			$this->billing_phone = $args['billing_phone'];
			$this->billing_email = $args['billing_email'];                        
			$this->billing_first_name = $args['billing_first_name'];
			$this->billing_last_name = $args['billing_last_name'];
			$this->billing_address = $args['billing_address'];
			$this->billing_city = $args['billing_city'];
			$this->billing_state = $args['billing_state'];
			$this->billing_zip = $args['billing_zip'];
                        $this->payment_type = $args['payment_type'];
		}
    }	
	
	/** Returns the transaction fields for an Authorize.net payment */
	public function getTransactionFields(){
		return
			array(
				'amount' 	=> $this->amount, 
				'card_num' 	=> $this->card_num, 
				'exp_date' 	=> $this->exp_month . $this->exp_year,
				'first_name'    => $this->billing_first_name,
				'last_name'     => $this->billing_last_name,
				'address' 	=> $this->billing_address,
				'city' 		=> $this->billing_city,
				'state' 	=> $this->billing_state,
				'zip' 		=> $this->billing_zip,
				'card_code'     => $this->card_code,
				'description'   => 'Sigma Pi Phi Lexus Donation: 2012'
			);		
	}
	
	/** Makes sure the data is correct for a transaction */
	public function validate($is_transaction_validate = true){
		$msgs = array();
		
                // Validate a billing transaction
		if($is_transaction_validate === true){
                    
                    if(empty($this->amount) || !is_numeric($this->amount)){
                            $this->messages[] = 'A numeric donation amount is required.';
                    } else if(intval($this->amount) <= 0){
                            $this->messages[] = 'Donation amount must be greater than 0.';
                    }

                    if(empty($this->card_num)){ 
                            $this->messages[] = 'Credit card number is required.';
                    } else if(!is_numeric($this->card_num)){
                            $this->messages[] = 'Credit card number must be numeric.';
                    } else if(!$this->is_valid_luhn($this->card_num)){
                            $this->messages[] = 'Credit card number does not have a valid format.  Please check your card and try again.';
                    }

                    if(empty($this->exp_month)){
                            $this->messages[] = 'Expiry month is required.';
                    } else if(!preg_match('/[0-9]{2}/', $this->exp_month)){
                            $this->messages[] = 'Expiry month must be a 2 digit number.';
                    } else {
                            $num = intval($this->exp_month);
                            if($num < 1 || $num > 12){
                                    $this->messages[] = 'Expiry month is not valid.  It must be a value from 1 to 12.';
                            }
                    }

                    if(empty($this->exp_year)){
                            $this->messages[] = 'Expiry year is required.';
                    } else if(!preg_match('/[0-9]{4}/', $this->exp_year)){
                            $this->messages[] = 'Expiry year must be a 4 digit number.';
                    }		

                    if(empty($this->card_code)){
                            $this->messages[] = 'CCV is required.';
                    } else if(!preg_match('/[0-9]{3,4}/', $this->card_code)){
                            $this->messages[] = 'CCV must be a 3 or 4 digit number.';
                    }


                    if(empty($this->billing_first_name)){
                            $this->messages[] = 'Billing first name is required.';
                    } else if(strlen($this->billing_first_name) > 40){
                            $this->messages[] = 'Billing first name must be 40 characters or less.';
                    }

                    if(empty($this->billing_last_name)){
                            $this->messages[] = 'Billing last name is required.';
                    } else if(strlen($this->billing_last_name) > 40){
                            $this->messages[] = 'Billing last name must be 40 characters or less.';
                    }

                    if(empty($this->billing_address)){
                            $this->messages[] = 'Billing address is required.';
                    } else if(strlen($this->billing_address) > 40){
                            $this->messages[] = 'Billing address must be 40 characters or less.';
                    }

                    if(empty($this->billing_city)){
                            $this->messages[] = 'Billing city is required.';
                    } else if(strlen($this->billing_city) > 40){
                            $this->messages[] = 'Billing city must be 40 characters or less.';
                    }

                    if(empty($this->billing_state)){
                            $this->messages[] = 'Billing state is required.';
                    } else if(strlen($this->billing_state) > 40){
                            $this->messages[] = 'Billing state must be 40 characters or less.';
                    }

                    if(empty($this->billing_zip)){
                            $this->messages[] = 'Billing zip code is required.';
                    } else if(!preg_match('/([0-9]{5})|([A-Za-z][0-9][A-Za-z]\s?[0-9][A-Za-z][0-9])/', $this->billing_zip)){
                            $this->messages[] = 'Billing zip code must be 5 digits or postal code must be 6 characters.';
                    }                        


                    if(empty($this->billing_email)){
                            $this->messages[] = 'Billing email is required.';
                    } else if(strlen($this->billing_email) > 125){
                            $this->messages[] = 'Billing email must be 125 characters or less.';
                    } else if(!filter_var($this->billing_email, FILTER_VALIDATE_EMAIL)){
                            $this->messages[] = 'Billing email address is not valid.';
                    }	

                    if(empty($this->billing_phone)){
                            $this->messages[] = 'Billing phone number is required.';
                    } else if(!$this->isValidNumber($this->cleanPhoneNumber($this->billing_phone))){
                            $this->messages[] = 'Billing phone number must be between 10 to 16 digits long.';
                    } 
                   
                // Validation on behalf user data    
		} else {
		
                    if(empty($this->first_name)){
                            $this->messages[] = 'First name is required.';
                    } else if(strlen($this->first_name) > 40){
                            $this->messages[] = 'First name must be 40 characters or less.';
                    }

                    if(empty($this->last_name)){
                            $this->messages[] = 'Last name is required.';
                    } else if(strlen($this->last_name) > 40){
                            $this->messages[] = 'Last name must be 40 characters or less.';
                    }

                    if(empty($this->address)){
                            $this->messages[] = 'Address is required.';
                    } else if(strlen($this->address) > 40){
                            $this->messages[] = 'Address must be 40 characters or less.';
                    }

                    if(empty($this->city)){
                            $this->messages[] = 'City is required.';
                    } else if(strlen($this->city) > 40){
                            $this->messages[] = 'City must be 40 characters or less.';
                    }

                    if(empty($this->state)){
                            $this->messages[] = 'State is required.';
                    } else if(strlen($this->state) > 40){
                            $this->messages[] = 'State must be 40 characters or less.';
                    }

                    if(empty($this->zip)){
                            $this->messages[] = 'Zip code is required.';
                    } else if(!preg_match('/([0-9]{5})|([A-Za-z][0-9][A-Za-z]\s?[0-9][A-Za-z][0-9])/', $this->zip)){
                            $this->messages[] = 'ZIP code must be 5 digits or postal code must be 6 characters.';
                    }		
		
                }
                
		if(empty($this->email)){
			$this->messages[] = 'Email is required.';
		} else if(strlen($this->email) > 125){
			$this->messages[] = 'Email must be 125 characters or less.';
		} else if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
			$this->messages[] = 'Email address is not valid.';
		}	

		if(empty($this->phone)){
			$this->messages[] = 'Phone number is required.';
		} else if(!$this->isValidNumber($this->cleanPhoneNumber($this->phone))){
			$this->messages[] = 'Phone number must be between 10 to 16 digits long.';
		}	
                
		if(strlen($this->payment_type) > 15){
			$this->messages[] = 'Payment type must be 15 characters or less.';
		}             
		
		return !$this->isMessages();
	}
	
	/** Are there messages */
	public function isMessages(){
		return !empty($this->messages);
	}	

	/** Clearout old messages */
	public function clearMessages(){
		$this->messages = array();
	}		
	
	/** Performs Luhn check on the credit card number */
	private function is_valid_luhn($number) {
		settype($number, 'string');
		$sumTable = array(
			array(0,1,2,3,4,5,6,7,8,9),
			array(0,2,4,6,8,1,3,5,7,9)
		);
		$sum = 0;
		$flip = 0;
		for ($i = strlen($number) - 1; $i >= 0; $i--) {
			$sum += $sumTable[$flip++ & 0x1][$number[$i]];
		}
		return $sum % 10 === 0;
	}	
        
	/** Make it harder to ---- something ---- and ----- a hammer */
	public function obfuscate() {                
                $length = strlen($this->card_num);
                $this->card_num = 'XXXX XXXX XXXX ' . ($length > 4 ? substr($this->card_num, (strlen($this->card_num) - 4)) : 'XXXX');
                $this->card_code = 'XXX';
	}        
	
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	/** Cleans everything but numeric chars out of a phone number */
	private function cleanPhoneNumber($n=''){
		return preg_replace('/[^0-9]/', '', $n);
	}	
	
	/** Valid phone number? */
	private function isValidNumber($n=''){
		return preg_match('/^[0-9]{10,16}$/', $n);
	}
	
	public function __toString(){			
		return 
			' amount: ' . $this->amount . 
			' card_num: ' . $this->card_num . 
			' exp_month: ' . $this->exp_month . 
			' exp_year: ' . $this->exp_year . 
			' first_name: ' . $this->first_name . 
			' last_name: ' . $this->last_name . 
			' address: ' . $this->address . 
			' city: ' . $this->city . 
			' state: ' . $this->state . 
			' zip: ' . $this->zip . 
			' card_code: ' . $this->card_code . 
			' phone: ' . $this->phone . 
			' email: ' . $this->email . 
			' billing_phone: ' . $this->billing_phone . 
			' billing_email: ' . $this->billing_email .                        
			' billing_first_name: ' . $this->billing_first_name . 
			' billing_last_name: ' . $this->billing_last_name .
			' billing_address: ' . $this->billing_address . 
			' billing_city: ' . $this->billing_city .
			' billing_state: ' . $this->billing_state . 
			' billing_zip: ' . $this->billing_zip .  
                        ' payment_type: ' . $this->payment_type; 
	}	
}
?>