<?php
/**
 * Plugin Name: Recent Network Updates
 * Description: Displays recent updates from across the Archon Network
 * Version: 1.0
 * Author: Pat Heard
 * Author URI: http://fullahead.org
 */


require_once('post-indexer.php');
require_once(dirname(__FILE__) . '/../functions/template-functions.php');

class RecentUpdates_Widget extends WP_Widget {

	const MAX_UPDATES = 30;
	const UDPATES_PER_PAGE = 30;
	const UDPATES_PER_BLOG = 3;

	function RecentUpdates_Widget() {
		$widget_ops = array( 'classname' => 'recent-updates', 'description' => __('Displays recent updates from across the Archon Network', 'archon-network') );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'recent-updates-widget' );
		$this->WP_Widget( 'recent-updates-widget', __('Recent Updates', 'archon-network'), $widget_ops, $control_ops );
	}

	/**
	 * Retrieve the recent posts for the network
	 */
	private function getRecentPosts ($max_updates, $region = '') {
		global $wpdb;

		$posts = get_transient('recent_updates_posts_' . $region);
		if ( false === $posts ) {

			// Get the training blog ID and omit its posts
			$trainingId = get_id_from_blogname( 'metic' );
		
			// Get posts from all regions
			if ($region === '') {
				$results = $wpdb->get_results(
					$wpdb->prepare(
						'SELECT blog_id, post_title, post_permalink, post_published_stamp, post_published_gmt, post_modified_gmt FROM '.$wpdb->base_prefix.'site_posts WHERE blog_id NOT IN (1, ' . $trainingId . ') ORDER BY post_published_stamp DESC LIMIT %d', $max_updates
					)
				);

			// Limit posts to a single region
			} else {

				// Get the blog IDs for the given region
				$blog_ids = array();
				$regions = group_sites_by_region($region);
				foreach ($regions as $region) {
					foreach ($region as $regional_site) {
						$blog_ids[] = $regional_site->blog_id;
					}
				}

				// Get the posts for that region's blogs
				if (!empty($blog_ids)) {
					$in_blog_ids = implode( ',', $blog_ids);
					$results = $wpdb->get_results(
						'SELECT blog_id, post_title, post_permalink, post_published_stamp, post_published_gmt, post_modified_gmt FROM ' . $wpdb->base_prefix . 'site_posts WHERE blog_id NOT IN (1, ' . $trainingId . ') AND blog_id IN (' . $in_blog_ids . ') ORDER BY post_published_stamp DESC LIMIT ' . $max_updates
					);
				}
			}

			if (!empty($results)) {

				// Lookup the blog name for the returned posts
				$bloginfo = array();
				foreach ($results as $post) {
					if (!isset($bloginfo['info' . $post->blog_id])) {
						$bloginfo[$post->blog_id] = array(
							'name' => get_blog_option($post->blog_id, 'blogname'),
							'post_count' => 0
						);
					}
				}

				// Store the posts and bloginfo in the cache for 5 minutes
				$posts = array(
					'posts' => $results,
					'bloginfo' => $bloginfo
				);
				set_transient( 'recent_updates_posts_' . $region, $posts, 300);
			}
		}
		return $posts;
	}


	/**
	 * Checks that a numeric value is greater than 0 and less than a defined max
	 */
	private function checkNumericUpdate ($value, $max) {
		if (!is_numeric($value)) {
			$value = $max;
		} else {
			$value = intval($value);
			if ($value > $max || $value < 1) {
				$value = $max;
			}
		}
		return $value;
	}

	function widget( $args, $instance ) {
		extract( $args );

		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
		$region = isset($instance['region']) ? $instance['region'] : '';
		$max_updates = isset($instance['max_updates']) ? $instance['max_updates'] : RecentUpdates_Widget::MAX_UPDATES;
		$updates_per_page = isset($instance['updates_per_page']) ? $instance['updates_per_page'] : RecentUpdates_Widget::UDPATES_PER_PAGE;

		echo $before_widget;

		// Display the widget title
		if ($title) {
			echo $before_title . $title . $after_title;		}

		?>
	
		<?php

		// Display recent udpates
		$posts = $this->getRecentPosts($max_updates, $region);
		if (!empty($posts['posts'])) {
			$bloginfo = $posts['bloginfo'];

			?>
			<ul class="recent-updates page active"> <?php

			// Output the recent posts
			$post_count = 0;
			foreach ($posts['posts'] as $post) {

				// Make sure we haven't flooded the updates with a single blog
				if ($bloginfo[$post->blog_id]['count'] >= RecentUpdates_Widget::UDPATES_PER_BLOG) {
					continue;
				} else if ($post_count >=  $updates_per_page) {
					?> </ul><ul class="recent-updates page"> <?php
					$post_count = 0;
				}

				?>
				
				<li>
					<!-- <?php //if ( has_post_thumbnail() ) : ?>
					    <a href="<?php //the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php //echo get_the_post_thumbnail($post->blog_id, 'post-thumbnail'); ?>
					    </a>
					<?php //endif; ?> -->
					<a href="<?php echo $post->post_permalink ?>" title="<?php _e('View update', 'archon-network') ?>">
						
					<span class="title"><?php echo $post->post_title ?></span>
					<!-- <span class="updated"><?php //echo date('F j, Y', strtotime($post->post_modified_gmt)); ?></span> -->
					<span class="updated"><?php echo gmdate('F j, Y',($post->post_published_stamp)); ?></span>
					<span class="blogname"><?php echo $bloginfo[$post->blog_id]['name'] ?></span>
					<?php
					 //$timestamp=$post->post_published_stamp;
					 //echo gmdate("Y-m-d\TH:i:s\Z", $timestamp);
					 ?>
					</a>
				</li>
				<?php
				$post_count++;
				$bloginfo[$post->blog_id]['count']++;
			}

			?> </ul>
		

			<script>
				(function($) {

					var Pager = {
						container : $('.pages'),
						pages : null,
						pageLinks : null,
						count : 0,

						init : function () {
							this.pages = this.container.find('.page');
							this.count = this.pages.length;

							if (this.count > 1) {
								this.pages.not(':first-child').hide();
								for(var i = 0; i < this.count; i++) {
									this.container.append('<span class="link-page" title="Show page ' + (i+1) + '" data-page="'+i+'">' + (i+1) + '</span>');
								}

								this.container.append('<span class="link-all" title="Show all pages"><?php _e('All','archon-network') ?></span>');
								this.pageLinks = this.container.find('.link-page, .link-all');
								this.pageLinks.first().addClass('active');
								this.pageLinks.on('click', Pager.click);
							}
						},

						click : function() {
							var $link = $(this);

							Pager.pageLinks.removeClass('active');
							$link.addClass('active');

							// Show a specific page
							if ($link.hasClass('link-page')) {
								Pager.pages.hide().removeClass('active');
								Pager.pages.eq($link.data('page')).show().addClass('active');

							// Show all pages
							} else {
								Pager.pages.show().removeClass('active');
								Pager.pages.first().addClass('active');
							}
						}
					};
					Pager.init();

				}(jQuery));
			</script>

			<?php

		} else {
			_e('No recent updates found...', 'archon-network');
		}

		?></div>
	<?php

		echo $after_widget;
	}

	//Update the widget
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['max_updates'] = strip_tags( $new_instance['max_updates'] );
		$instance['updates_per_page'] = strip_tags( $new_instance['updates_per_page'] );
		$instance['region'] = strip_tags( $new_instance['region'] );

		$instance['max_updates'] = $this->checkNumericUpdate($instance['max_updates'], RecentUpdates_Widget::MAX_UPDATES);
		$instance['updates_per_page'] = $this->checkNumericUpdate($instance['updates_per_page'], RecentUpdates_Widget::UDPATES_PER_PAGE);

		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'title' => __('Recent Updates', 'archon-network'), 'max_updates' => RecentUpdates_Widget::MAX_UPDATES, 'updates_per_page' => RecentUpdates_Widget::UDPATES_PER_PAGE, 'region' => '' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'archon-network'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'region' ); ?>"><?php _e('Show updates for:', 'archon-network'); ?></label>
			<select style="width:100%;" id="<?php echo $this->get_field_id( 'region' ); ?>" name="<?php echo $this->get_field_name( 'region' ); ?>">
				<option value="" <?php echo ($instance['region'] === '' ? 'selected="selected"' : ''); ?>><?php _e('All regions', 'archon-network'); ?></option>
				<option value="Central" <?php echo ($instance['region'] === 'Central' ? 'selected="selected"' : ''); ?>><?php _e('Central', 'archon-network'); ?></option>
				<option value="Northeast" <?php echo ($instance['region'] === 'Northeast' ? 'selected="selected"' : ''); ?>><?php _e('Northeast', 'archon-network'); ?></option>
				<option value="Pacific" <?php echo ($instance['region'] === 'Pacific' ? 'selected="selected"' : ''); ?>><?php _e('Pacific', 'archon-network'); ?></option>
				<option value="Southeast" <?php echo ($instance['region'] === 'Southeast' ? 'selected="selected"' : ''); ?>><?php _e('Southeast', 'archon-network'); ?></option>
				<option value="West" <?php echo ($instance['region'] === 'West' ? 'selected="selected"' : ''); ?>><?php _e('West', 'archon-network'); ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'max_updates' ); ?>"><?php _e('Number of updates (max is '.RecentUpdates_Widget::MAX_UPDATES.'):', 'archon-network'); ?></label>
			<input id="<?php echo $this->get_field_id( 'max_updates' ); ?>" name="<?php echo $this->get_field_name( 'max_updates' ); ?>" value="<?php echo $instance['max_updates']; ?>" style="width:100%;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'updates_per_page' ); ?>"><?php _e('Number of updates to show per page:', 'archon-network'); ?></label>
			<input id="<?php echo $this->get_field_id( 'updates_per_page' ); ?>" name="<?php echo $this->get_field_name( 'updates_per_page' ); ?>" value="<?php echo $instance['updates_per_page']; ?>" style="width:100%;" />
		</p>

	<?php
	//desc ' . $wpdb->base_prefix . 'site_posts;
	}
}

?>