<tr data-customerid="[+CUSTOMERID+]" data-updatedBy="[[svVars? &var=`webInternalKey`]]" data-updaterEmail="[[svVars? &var=`webEmail`]]" data-webId="[+WEB_ID+]">
	<td>
		<script language="JavaScript">
            function edit_[+CUSTOMERID+](){
                document.forms.form_[+CUSTOMERID+].submit();
            }
		</script>
		<form name="form_[+CUSTOMERID+]" action="/services/grammateus/member/archons_edit.php" method="get">
			<input type="hidden" name="CUSTOMERID" value="[+CUSTOMERID+]">
			<input type="hidden" name="GRAMMID" value="[+WEB_ID+]">
			<input type="hidden" name="UPDATEDBY" value="[[svVars? &var=`webFullname`]]">
			<input type="hidden" name="BOULENAME" value="[+BOULENAME+]">
			<input type="hidden" name="REGIONNAME" value="[+REGIONNAME+]">
		</form>
		<a href="#" rel="[+WEB_ID+]" onclick="edit_[+CUSTOMERID+]();return(FALSE);" style="display: none;">[+FULLNAME+]</a>
		<div id="[+WEB_ID+]" class="balloonstyle">
			<strong>[+PREFIX+] [+L2R_FULLNAME+]</strong><br />
			[+ADDRESS1+]<br />
			[+CITY+], [+STATECD+] [+ZIP+]
		</div>


	</td>
	<td>[+STATUS+] {{showsEmeritus}}</td>
	<td class="action_td">
        <a href="/services/spp-directory/search-results-json.php?id=[+CUSTOMERID+]" class="view" id="view_[+WEB_ID+]">View</a>,
        <a href="#" class="edit" id="edit_[+CUSTOMERID+]">Edit</a>,
		<a href="/services/grammateus/member/status_request.php?CUSTOMERID=[+CUSTOMERID+]&amp;UPDATEDBY=[[svVars? &var=`webInternalKey`]]&amp;DM_EMAIL=[[svVars? &var=`webEmail`]]&amp;KT_back=1">Request Status Change</a>
    </td>
</tr>
