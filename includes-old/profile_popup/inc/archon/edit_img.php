<div id="archon_edit_img" class="edit_image accordion parent-elem" data-accordion>
    <div data-control><h3>Edit Image</h3></div>
    <div data-content>
        <div class="data-content-inner clearfix ymp_wrapper grid-12">
            <div id="crop-avatar" class="ymp-row">
                <!-- Cropping modal -->
                <div class="avatar-form">
                    <div class="avatar-body">
                        <div class="row ymp-row">
                            <div class="span-9">
                                <div class="avatar-wrapper img-box">
                                    <img src="<?php echo $user_img;?>" alt="" />
                                </div>


                                <div class="avatar-btns ymp-btn-group">
                                    <div class="ymp-btn-group-inner">
                                        <div class="btn-group">
                                            <button type="button" class="btn button" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>
                                            <button type="button" class="btn button" data-method="rotate" data-option="-15">-15deg</button>
                                            <button type="button" class="btn button" data-method="rotate" data-option="-30">-30deg</button>
                                            <button type="button" class="btn button" data-method="rotate" data-option="-45">-45deg</button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn button" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>
                                            <button type="button" class="btn button" data-method="rotate" data-option="15">15deg</button>
                                            <button type="button" class="btn button" data-method="rotate" data-option="30">30deg</button>
                                            <button type="button" class="btn button" data-method="rotate" data-option="45">45deg</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="span-3">
                                <h3 class="preview_title">Preview Image</h3>
                                <div class="avatar-preview preview-lg">
                                    <img src="<?php echo $user_img;?>" alt="" />
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <!-- Upload image and data -->
                            <div class="avatar-upload">
                                    <input type="hidden" class="avatar-src" name="avatar_src">
                                    <input type="hidden" class="avatar-data" name="avatar_data">
                                    <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                </div>


                                <button type="button" class="avatar-save">Done</button>

                            </div>

                    </div>

                </div>
                <?php echo $submit_dv;?>
            </div>
        </div>
    </div>
</div>