<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("number", true, "numeric", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("{rsArchon.EMAIL}");
  $emailObj->setCC("");
  $emailObj->setBCC("legatissima@gmail.com");
  $emailObj->setSubject("REQUEST: Presidential Inaugural Reception Invitation Request");
  //WriteContent method
  $emailObj->setContent("<p>Your request to attend the Presidential Inaugural Reception in Washington, DC has been received.</p>\n<p>The Office of the Grand Boul&#233; will award invitations on a first-come, first served basis. You will receive correspondence regarding the status of your request shortly.</p>\n<p>Happy Holidays!</p>\n<p>Below is the information you submitted:</p> \n\n<table>\n<tr>\n<td align=\"right\" valign=\"top\">Number of Invitations Requested:</td>\n<td>{number}</td>\n</tr>\n<tr>\n<td align=\"right\" valign=\"top\">Archon:</td>\n<td>{rsArchon.L2R_FULLNAME}<br />{rsArchon.BOULENAME} Boul&#233;</td>\n</tr>\n<tr>\n<td align=\"right\" valign=\"top\">Archousa/Female Guest: </td>\n<td>{guest_name}</td>\n</tr>\n<tr>\n<td align=\"right\" valign=\"top\">Contact Information:</td>\n<td>{rsArchon.ADDRESS1}<br />{rsArchon.CITY}, {rsArchon.STATECD} {rsArchon.ZIP}<br />{rsArchon.EMAIL}</td>\n</tr>\n<tr>\n<td align=\"right\" valign=\"top\">Date/Time Submitted:</td>\n<td>{submit_date}</td>\n</tr>\n</table>\n");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchon = "-1";
if (isset($_GET['WEB_ID'])) {
  $colname_rsArchon = $_GET['WEB_ID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchon = sprintf("SELECT CUSTOMERID, REGIONNAME, REGIONCD, BOULENAME, CHAPTERID, BOULECITY, BOULESTATECD, STATUS, STATUSSTT, CUSTOMERCLASSSTT, CUSTOMERTYPE, JOINDATE, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, SUFFIX, DESIGNATIONLST, L2R_FULLNAME, FULLNAME, FULLNAME2, L2R_FULLNAME2, ADDRESS1, ADDRESS2, CITY, STATECD, ZIP, HOMEPHONE, EMAIL, JOBTITLE, OCCUPATIONCD, SKILLCDLST, ORGNAME, ALTADDRESS1, ALTADDRESS2, ALTCITY, ALTSTATE, ALTZIP, WORKPHONE, ALTEMAIL, BIRTHDATE, SPOUSENAME, LASTUPDATED, UPDATEDBY, GRAMMUPDATE, GRAMM, IMAGE, DECEASEDDATE, WEB_EMAIL, CPOSITION, WEB_ID FROM vw_web_elig WHERE WEB_ID = %s", GetSQLValueString($colname_rsArchon, "int"));
$rsArchon = mysql_query($query_rsArchon, $sigma_modx) or die(mysql_error());
$row_rsArchon = mysql_fetch_assoc($rsArchon);
$totalRows_rsArchon = mysql_num_rows($rsArchon);

// Make an insert transaction instance
$ins_spp_rsvp = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_rsvp);
// Register triggers
$ins_spp_rsvp->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_rsvp->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_rsvp->registerTrigger("END", "Trigger_Default_Redirect", 99, "thank-you.php?rsvp_id={rsvp_id}&WEB_ID={rsArchon.WEB_ID}");
$ins_spp_rsvp->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_rsvp->setTable("spp_rsvp");
$ins_spp_rsvp->addColumn("event_id", "NUMERIC_TYPE", "POST", "event_id", "1");
$ins_spp_rsvp->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD", "{rsArchon.CUSTOMERID}");
$ins_spp_rsvp->addColumn("number", "NUMERIC_TYPE", "POST", "number");
$ins_spp_rsvp->addColumn("guest_name", "STRING_TYPE", "POST", "guest_name");
$ins_spp_rsvp->addColumn("submit_date", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_rsvp->setPrimaryKey("rsvp_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_rsvp = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_rsvp);
// Register triggers
$upd_spp_rsvp->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_rsvp->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_rsvp->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
$upd_spp_rsvp->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$upd_spp_rsvp->setTable("spp_rsvp");
$upd_spp_rsvp->addColumn("event_id", "NUMERIC_TYPE", "POST", "event_id");
$upd_spp_rsvp->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "POST", "CUSTOMERCD");
$upd_spp_rsvp->addColumn("number", "NUMERIC_TYPE", "POST", "number");
$upd_spp_rsvp->addColumn("guest_name", "STRING_TYPE", "POST", "guest_name");
$upd_spp_rsvp->addColumn("submit_date", "DATE_TYPE", "VALUE", "submit_date");
$upd_spp_rsvp->setPrimaryKey("rsvp_id", "NUMERIC_TYPE", "GET", "rsvp_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_rsvp = $tNGs->getRecordset("spp_rsvp");
$row_rsspp_rsvp = mysql_fetch_assoc($rsspp_rsvp);
$totalRows_rsspp_rsvp = mysql_num_rows($rsspp_rsvp);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>

<script type="text/javascript">
  
function showHide() {
	var field = document.getElementById("number_1");
	var name = document.getElementById("guest_name_1");
	
	if(field.value == '1') {
		document.getElementById("gname").style.visibility = "collapse";
		name.value = "";
	   }
	else
	{
	  document.getElementById("gname").style.visibility = "visible";
	}
}
</script>
<script type="text/javascript">
function checkGuest() {
	var field = document.getElementById("number_1");
	var name = document.getElementById("guest_name_1");
	
	if(field.value == '1') {
		return true;
	   }
	else {
		if(name.value.trim()=='' && field.value == '2') {
			alert("You must provide your Archousa or guest's name.");
			return false;
			}
	}

}
</script>	
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<h1 class="yellow">Inaugural Reception Invitation Request</h1>
<div class="contentBlock">
  <img src="reception-image.jpg" width="599" height="369" />
  <p style="width:599px;">Sigma Pi Phi Fraternity will co-sponsor with the 100 Black Men of America a Presidential Inaugural Reception on Sunday evening, January 20, 2013 at the City Club at 555 13th Street, NW from 6:00 P. M. until 10:00 P. M. in Washington, D.C.</p>
<ul>
  <li>An Archon who wishes to attend may request an invitation for himself and his Archousa or female guest. </li>
  <li>No more than two invitations per Archon will be awarded. </li>
  <li>There are a limited number of invitations available.</li>
  <li>Invitations will be awarded on a &ldquo;first-come, first-served&rdquo; basis.&nbsp; </li>
  <li>A confirmed Archon (and his guest) will be placed on the official guest list by name.</li>
  <li>Check in required. Invitations are non-transferable.</li>
</ul>
<h2 style="width:599px;">Archon <?php echo $row_rsArchon['LASTNAME']; ?>, please indicate the number of invitations you are requesting. </h2>
  <?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_rsvp > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <tr>
              <td class="KT_th"><label for="number_<?php echo $cnt1; ?>">Number:</label></td>
              <td><select name="number_<?php echo $cnt1; ?>" id="number_<?php echo $cnt1; ?>" onchange="showHide()">
                  <option value="1" <?php if (!(strcmp(1, KT_escapeAttribute($row_rsspp_rsvp['number'])))) {echo "SELECTED";} ?>>1</option>
                  <option value="2" <?php if (!(strcmp(2, KT_escapeAttribute($row_rsspp_rsvp['number'])))) {echo "SELECTED";} ?>>2</option>
                </select>
                  <?php echo $tNGs->displayFieldError("spp_rsvp", "number", $cnt1); ?> </td>
            </tr>
            <tr style="visibility:collapse" id="gname">
              <td class="KT_th"><label for="guest_name_<?php echo $cnt1; ?>">Archousa /<br />
                Female Guest Full Name:</label></td>
              <td><input type="text" name="guest_name_<?php echo $cnt1; ?>" id="guest_name_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp['guest_name']); ?>" size="50" maxlength="80" />
                  <?php echo $tNGs->displayFieldHint("guest_name");?> <?php echo $tNGs->displayFieldError("spp_rsvp", "guest_name", $cnt1); ?> </td>
            </tr>
          </table>
          <input type="hidden" name="submit_date_<?php echo $cnt1; ?>" id="submit_date_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp['submit_date']); ?>" />
          <input type="hidden" name="kt_pk_spp_rsvp_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp['kt_pk_spp_rsvp']); ?>" />
          <input type="hidden" name="event_id_<?php echo $cnt1; ?>" id="event_id_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp['event_id']); ?>" />
          <input type="hidden" name="CUSTOMERCD_<?php echo $cnt1; ?>" id="CUSTOMERCD_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsArchon['CUSTOMERID']); ?>" />
          <?php } while ($row_rsspp_rsvp = mysql_fetch_assoc($rsspp_rsvp)); ?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1
      if (@$_GET['rsvp_id'] == "") {
      ?>
              <input type="submit" name="KT_Insert1" id="KT_Insert1" value="Submit"  onclick="return checkGuest()"/>
              <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <?php }
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>

</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchon);
?>
