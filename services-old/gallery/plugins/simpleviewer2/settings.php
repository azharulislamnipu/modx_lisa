<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
  // SINCE VERSION 1.7, MOST DEFAULTS FOR NEW GALLERIES CAN BE SET IN svmanager/plugins/simpleviewer2/settings.ini
  define('SV2_IMAGE_PATH', 'images/');
  define('SV2_THUMB_PATH', 'thumbs/');
  define('SV2_IMAGE_LINK_URL', '');
  define('SV2_IMAGE_LINK_TARGET', '');
  define('SV2_XML_FILE', 'gallery.xml');
  define('SV2_XML_SETTINGS_TAG', 'simpleviewergallery');
  define('SV2_MASTER_PATH', 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2master'.DIRECTORY_SEPARATOR);
  define('SV2_MASTER_FILECOUNT', 11);
  define('SV2_SWF', 'simpleviewer.swf');
  define('SV2_IMAGE_CAPTION', '');
  define('SV2_THUMB_WIDTH', 75);
  define('SV2_THUMB_HEIGHT', 75);
  define('SV2_THUMB_QUALITY', 85);
  // Underline caption links when Auto links is on
  define('SV2_UNDERLINE_CAPTION_LINKS', true);
  define('SV2_CREATE_NEW', true);
?>