<?php
error_reporting(0);
include_once "../../home/modxapi.php";
$modx = new MODxAPI();
$modx->connect();
//$modx->startSession();

//var_dump($_SESSION['CUSTOMERCD']);
//die();

if(empty($_SESSION['CUSTOMERCD'])){
	$url = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/home/login.php';
	header('Location:  '.  $url);
	exit;
}
$base_path = str_replace('/home', '', $_api_path);

$visitor_id = $_SESSION['CUSTOMERCD'];

$new_path = '';
$url = $_SERVER['REQUEST_URI'];
/*
 * Local
 */

$pos = strrpos(__DIR__, "htdocs");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'htdocs/';
}
/**
 * Server
 */
/*$pos = strrpos(__DIR__, "apache");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'apache/';
}*/

if(isset($new_path)) {
	ob_start();
	define('LoggedIn', true);
	include $new_path . "includes/functions.php";
	include $new_path . "includes/search/advance-search/index.php";
	ob_flush();
}