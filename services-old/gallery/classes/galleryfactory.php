<?php
/**
 * SimpleViewer admin package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

/**
 * Gallery factory class
 *
 * @access public
 * @package svManager
 */
Class GalleryFactory
{
 /**
  * @var
  */
  
 /**
  * Constructs GalleryFactory
  * 
  */
  function GalleryFactory()
  {
  }

 /**
  * Creates gallery object
  *
  * @access public
  * @param string 'simpleviewer' | 'tiltviewer'
  * @param string gallery reference. This is the permanent reference not the index.
  * @param string gallery path
  * @param boolean new gallery
  * @return object gallery object
  */
  function makeGallery($galleryType, $ref, $path, $newGallery=false)
  {
    switch ($galleryType)
    {
      case 'simpleviewer' :
        return new SvGallery($ref, $path, $newGallery);
      case 'simpleviewer2' :
        return new Sv2Gallery($ref, $path, $newGallery);
      case 'tiltviewer' :
        return new TvGallery($ref, $path, $newGallery);
      case 'autoviewer' :
        return new AvGallery($ref, $path, $newGallery);
      case 'postcardviewer' :
        return new PvGallery($ref, $path, $newGallery);
      default :
        trigger_error('cannot recognise gallery type '.$galleryType, E_USER_NOTICE);
        return false;
    }
  }
 /**
  * Creates customize page object
  *
  * @access public
  * @param string gallery type
  * @return object page object
  */
  function makeCustomizePage($galleryType)
  {
    switch ($galleryType)
    {
      case 'simpleviewer' :
        return new SvCustomizePage();
      case 'simpleviewer2' :
        return new Sv2CustomizePage();
      case 'simpleviewer2pro' :     
        return new Sv2ProPage();
      case 'tiltviewer' :
        return new TvCustomizePage();
      case 'autoviewer' :
        return new AvCustomizePage();
      case 'postcardviewer' :
        return new PvCustomizePage();
      default :
        return new ErrorPage();
    }
  }
 /**
  * Creates pro page object
  *
  * @access public
  * @param string gallery type
  * @return object page object
  */
  function makeProPage($galleryType)
  {
    switch ($galleryType)
    {
      case 'simpleviewer' :
        return new SvProPage();
      case 'simpleviewer2' :
        return new Sv2ProPage();
      case 'tiltviewer' :
        return new TvProPage();
      case 'autoviewer' :
        return new AvProPage();
      case 'postcardviewer' :
        return new PvProPage();
      default :
        return new ErrorPage();
    }
  }
 /**
  * Creates images page object
  *
  * @access public
  * @param string gallery type
  * @return object page object
  */
  function makeImagesPage($galleryType)
  {
    switch ($galleryType)
    {
      case 'simpleviewer' :     
        return new SvImagesPage();
      case 'simpleviewer2' :     
        return new Sv2ImagesPage();
      case 'tiltviewer' :
        return new TvImagesPage();
      case 'autoviewer' :
        return new AvImagesPage();
      case 'postcardviewer' :
        return new PvImagesPage();
      default :
        return new ErrorPage();
    }
  }
 /**
  * Creates upload page object
  *
  * @access public
  * @param string gallery type
  * @return object page object
  */
  function makeUploadPage($galleryType)
  {  
     return new UploadPage($galleryType);
  }
  
 /**
  * Creates sort page object
  *
  * @access public
  * @param string gallery type
  * @return object page object
  */
  function makeSortPage($galleryType)
  {  
     return new SortPage($galleryType);
  }

  
 /**
  * Creates error page object
  *
  * @access public
  * @return object page object
  */
  function makeErrorPage()
  {  
     return new ErrorPage();
  }


}

?>