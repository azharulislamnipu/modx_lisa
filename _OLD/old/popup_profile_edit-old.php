<?php
global $modx;
global $base_path;
global $base_url;
$new_path = str_replace('home/', '', $base_path);
include $new_path . "includes/functions.php";

$db = get_db();
$all_prefix = get_all_prefix();
$all_suffix = get_all_suffix();
$all_designation = get_all_hos_desig();
$all_states = get_all_state();

$user_id = $_SESSION['CUSTOMERCD'];

$user_data = get_user_profile_data($user_id)['data'];
?>

<?php ob_start();?>
    <div id="popup_ymp" class="popup-ymp">
        <form action="" method="post" id="pupup_form">
            <div class="message">

            </div>
            <div class="edit_image" id="archon_edit_img">
                <h3>Edit Image</h3>
                <div class="img_box">
                </div>
                <input name="__files[]" type="file" />
            </div><!--edit image-->

            <div class="edit_contact" id="archon_edit_contact">
                <h3 class="text-center">Edit Contact Information</h3>
                <div class="edit_contact_inner ymp_form_input">
                    <h4>Personal Detail</h4>
                    <div class="clearfix ymp_wrapper grid-12">
                        <div class="ymp-row">
                            <div class="span-6">
                                <label for="PREFIX">Prefix</label>
                                <select name="PREFIX" id="PREFIX">
                                    <option value=""><?php echo "Select one..."; ?></option>
                                    <?php foreach (array_column($all_prefix['data'], 'PREFIX') as $prefix){
                                        echo "<option value='$prefix'" . selected($prefix, $user_data['PREFIX'], false) . ">$prefix</option>";
                                    }?>
                                </select>

                            </div>
                            <div class="span-6">
                                <label for="SUFFIX">Suffix</label>
                                <select name="SUFFIX" id="SUFFIX">
                                    <option value=""><?php echo "Select one..."; ?></option>
                                    <?php foreach (array_column($all_suffix['data'], 'SUFFIX') as $suffix){
                                        echo "<option value='$suffix'" . selected($suffix, $user_data['SUFFIX'], false) . ">$suffix</option>";
                                    }?>
                                </select>
                            </div>

                            <label class="span-12">Full Name <span class="required_ind">*</span></label>
                            <div class="span-4">
                                <input class="input-required input-validated" type="text" name="FIRSTNAME" size="20" maxlength="40" id="FIRSTNAME" value="<?php echo $user_data['FIRSTNAME'];?>" placeholder="Enter First Name">
                            </div>
                            <div class="span-4">
                                <input type="text" name="MIDDLEINITIAL" size="20" maxlength="40" id="MIDDLEINITIAL" value="<?php echo $user_data['MIDDLEINITIAL'];?>" placeholder="Enter Middle Initail">
                            </div>
                            <div class="span-4">
                                <input type="text" name="LASTNAME" size="20" maxlength="40" id="LASTNAME" value="<?php echo $user_data['LASTNAME'];?>" placeholder="Enter Last Name">
                            </div>

                            <div class="span-12">
                                <label for="DESIGNATIONLST">Honorofix</label>
                                <select multiple size="5" name="DESIGNATIONLST[]" id="DESIGNATIONLST">
                                    <?php
                                    $desig_selected = explode(",", $user_data['DESIGNATIONLST']);
                                    foreach (array_column($all_designation['data'], 'DESIGNATIONLST') as $desig){
                                        $selected = '';
                                        if(in_array($desig, $desig_selected)){
                                            $selected = ' selected="selected"';
                                        }
                                        echo "<option value='$desig'{$selected}>$desig</option>";
                                    }?>
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            <label class="span-12" for="ADDRESS1">Address <span class="required_ind">*</span></label>
                            <div class="span-6">
                                <input class="input-required input-validated" type="text" placeholder="Enter Address 1 " name="ADDRESS1" id="ADDRESS1" value="<?php echo $user_data['ADDRESS1']; ?>" size="20" maxlength="40" />
                            </div>
                            <div class="span-6">
                                <input type="text" placeholder="Enter Address 2" name="ADDRESS2" id="ADDRESS2" value="<?php echo $user_data['ADDRESS2']; ?>" size="20" maxlength="40" />
                            </div>
                            
                            <div class="span-4">
                                <label for="CITY">City <span class="required_ind">*</span></label>
                                <input class="input-required input-validated" type="text" placeholder="Enter City" name="CITY" id="CITY" value="<?php echo $user_data['CITY']; ?>" size="20" maxlength="40" />
                            </div>

                            <div class="span-4">
                                <label for="STATECD">State <span class="required_ind">*</span></label>
                                <select name="STATECD" id="STATECD" class="input-required input-validated">
                                    <option value=""><?php echo "Select one..."; ?></option>
                                    <?php foreach ($all_states['data'] as $state){
                                        echo "<option value='{$state['st_cd']}'" . selected($state['st_cd'], $user_data['STATECD'], false) . ">{$state['st_nm']}</option>";
                                    }?>
                                </select>
                            </div>

                            <div class="span-4">
                                <label for="ZIP">Zip <span class="required_ind">*</span></label>
                                <input class="input-required input-validated" type="text" placeholder="Enter Zip" name="ZIP" id="ZIP" value="<?php echo $user_data['ZIP']; ?>" size="20" maxlength="40" />
                            </div>

                        </div>
                    </div>

                    <h4>Contact Detail</h4>
                    <div class="clearfix ymp_wrapper grid-12">
                        <div class="ymp-row">
                            <div class="span-6">
                                <label for="HOMEPHONE">Home Phone <span class="required_ind">*</span></label>
                                <input class="input-required input-validated" type="text" name="HOMEPHONE" size="20" maxlength="40" id="HOMEPHONE" value="<?php echo $user_data['HOMEPHONE'];?>" placeholder="Enter Home Phone">
                            </div>
                            
                            <div class="span-6">
                                <label for="MOBILEPHONE">Mobile Phone</label>
                                <input type="text" name="MOBILEPHONE" size="20" maxlength="40" id="MOBILEPHONE" value="<?php echo $user_data['MOBILEPHONE'];?>" placeholder="Enter Mobile Phone">
                            </div>

                            <div class="span-6">
                                <label for="EMAIL">Contact Email <span class="required_ind">*</span></label>
                                <input class="input-required input-validated" type="email" name="EMAIL" size="20" maxlength="125" id="EMAIL" value="<?php echo $user_data['EMAIL'];?>" placeholder="Enter Contact Email">
                            </div>

                            <div class="span-6">
                                <label for="BIRTHDATE">Birth Date</label>
                                <div class="clearfix birth_date_input_wrap">
                                    <a class="birth_date_btn" id="BIRTHDATE_btn" href="#"><img src="<?php echo $base_url; ?>/assets/templates/archon/images/calendar-icon.png" alt="Select Date"></a>
                                    <div class="bdinput">
                                        <input type="text" name="BIRTHDATE" size="20" maxlength="40" id="BIRTHDATE" value="<?php echo $user_data['BIRTHDATE'];?>" placeholder="">
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div> <!--end edit contact-->

            <div class="edit_career" id="archon_edit_career">

            </div><!--end edit archon-->

            <div class="edit_education" id="archon_edit_education">

            </div> <!--end edit education-->

            <div class="edit_bio" id="archon_edit_bio">
                <h3 class="text-center">Edit Biography</h3>
                <div class="edit_bio_inner ymp_form_input">
                    <label class="span-12">Biography <span class="required_ind">*</span></label>
                    <textarea class="input-required input-validated" name="BIO" id="BIO" cols="90" rows="10"><?php echo $user_data['BIO'];?></textarea>
                </div>
            </div><!--end edit bio-->

            <input type="hidden" name="CUSTOMERID" value="<?php echo $user_id;?>">
            <input type="hidden" name="UPDATEDBY" value="<?php echo $_SESSION['webShortname'];?>">
            <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo $user_data['LASTUPDATED'];?>">
            <input type="hidden" name="RETURN" value="<?php echo $_SESSION['webValidated'];?>">
            <div class="sb_div">
                <button type="reset" class="pp_cancel button">Cancel</button> &nbsp;
                <button type="submit" class="pp_submit button">Submit</button>
            </div>
        </form>
        <a href="#" class="popup_close">X</a>
    </div>

<?php return ob_get_clean();
?>