<?php get_header(); ?>

<!-- <div id="content">
    <div class="padder"> -->

    <?php
        do_action( 'sigma_before_blog_home' );
        $bouleName = get_bloginfo( 'name' );
        $bouleUrl = get_bloginfo( 'url' );
		$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    ?>

	<!-- large image slider -->
	<div class="width margin-bottom-medium">
		<div class="row">
			<div class="span-8"><div class="inner">
				
			<div id="slider">
				<?php echo do_shortcode("[huge_it_slider id='1']"); ?>
				<?php //echo do_shortcode("[URIS id=89]"); ?>
			</div>
				
			
			</div></div>
		</div>
	</div>	
	
	<?php if( is_user_logged_in() ) : ?>
	<!-- woo slider -->
	
	<div class="width margin-bottom-medium an_wc_feat_row">
		<div class="row">
			<div class="span-8"><div class="inner">
				
			<div id="sigma-woocarousal">
				<?php echo do_shortcode("[wpb-feature-product title='Featured Boulé Merchandise']"); ?>
				<?php //echo do_shortcode("[URIS id=89]"); 
					//echo do_shortcode('[featured_products per_page="12" columns="4"]'); ?>
			</div>
			
			</div></div>
		</div>
	</div>
	
	<?php endif; ?>
	

            <!-- 3 round images -->
            <div class="row grid-12 margin-top-medium padding">
				<div class="round width padding">
				
			<?php if ( is_active_sidebar( 'three_round_images' ) ) : ?>
				
				<?php dynamic_sidebar( 'three_round_images' ); ?>
				 
				<?php endif; ?>
			</div>
        
            </div>

        </div>
    
	  <!-- history and officers -->

			  <div class="width padding divider-top">
			 <div class="row grid-12">
				 		<div class="span-8">
							<!-- <div class="inner">
							<h2>Recent Updates</h2>
						</div> -->
						
		<section class="span-12">
			<div class="inner">
		<?php //if( $page === 1 && !is_multisite_primary() && !is_regional_site() ) :
			//if(is_page_content('history')): ?>
				<!-- Boule history -->
			    <!-- <h2><?php //echo $bouleName . ' ' .  __('History', 'archon-network')?></h2> -->
			    <!-- <div class="contentBlock marginBottomLarge"> -->
			        <?php //get_page_content_by_slug( 'history' ) ?>
			    <!-- </div> -->
				<?php //endif;
				//endif;
		

		// Recent posts
		//locate_template( array( 'home_aggregate.php' ), true );

		//do_action( 'sigma_after_blog_home' ) ?>
		<?php if ( is_active_sidebar( 'home_large_block' ) ) : ?>
		
			<?php dynamic_sidebar( 'home_large_block' ); ?>
		 
			<?php endif; ?>
			
			<?php
			// Check that Archon plugin functions have been loaded
				 if(function_exists('is_multisite_primary') && function_exists('is_regional_site')){
					if( is_multisite_primary() || is_regional_site() )  :  ?>
			<div class="recent-updates featured span-12 float-left">
			<div class="contentBlock pages">

			<?php get_featured_image_posts(); ?>
				<?php get_featured_image_posts_secondary(); ?>
			</div>
			</div>
			<!-- <div class="featured span-3 float-left">
				<?php //the_widget('RecentUpdates_Widget'); ?>
			</div> -->
			
			 <?php endif; ?>
			 <?php } ?>
	</div><!-- #inner-->
		</section><!-- #span8-->
		</div>
		
		<section class="span-4 float-right"><div class="inner">
		    <?php 
			// Check that Archon plugin functions have been loaded
				if(function_exists('is_multisite_primary') && function_exists('is_regional_site')){
			//if( !is_multisite_primary() ) {
if( is_multisite_primary() ) {
			//	the_widget('RegionalBoules_Widget', array('show_regional' => 'no'), array('before_title' => '<h3 class="widgettitle">', 'after_title' => '</h3>'));

			if ( is_active_sidebar( 'home_right_block' ) ) : ?>
	
				<?php dynamic_sidebar( 'home_right_block' ); ?>
	 
			<?php endif; ?>
					<?php } elseif( !is_regional_site() ) {
			//	the_widget('RegionalBoules_Widget', array('show_regional' => 'no'), array('before_title' => '<h3 class="widgettitle">', 'after_title' => '</h3>'));

		    	$chapter_name = get_bloginfo('name'); ?>
		    	<div class="widget">
			        <h2 class="color yellow" style="clear: both"><?php echo $chapter_name . ' ' . __('Officers', 'archon-network');?></h2>
			        <div class="contentBlock">
			            <?php get_boule_leadership( $chapter_name ) ?>
			        </div>
		        </div> 
				<?php } else { ?> 
		
				<?php if ( is_active_sidebar( 'home_right_block' ) ) : ?>
		
					<?php dynamic_sidebar( 'home_right_block' ); ?>
		 
				<?php endif; ?>
		<?php } ?>
		
				<?php } ?>
   		

				
			<!-- </div> -->
			
		
			</div><!-- #inner-->
		</section><!-- #span4 -->
			</div><!-- #width padding -->
	</div><!-- #row grid-12 -->
	</div><!-- container -->

    <!-- Events and Press Releases -->
    <!-- <section id="events">
        <div class="width padding">

            <div class="row grid-12"> -->
				
				<?php //if ( is_active_sidebar( 'event_block' ) ) : ?>
				
					<?php //dynamic_sidebar( 'event_block' ); ?>
				 
					<?php //endif; ?>
				<!-- </div> --><!-- row grid12-->

   <!-- <div class="row grid-12"> -->
               
			
              

		
		    <?php //if( !is_multisite_primary() && !is_regional_site() ) :

				//the_widget('RegionalBoules_Widget', array('show_regional' => 'no'), array('before_title' => '<h3 class="widgettitle">', 'after_title' => '</h3>'));

		    	//$chapter_name = get_bloginfo('name'); ?>
		    	<!-- <div class="widget">
			        <h2 class="color yellow" style="clear: both"><?php //echo $chapter_name . ' ' . __('Officers', 'archon-network');?></h2>
			        <div class="contentBlock">
			            <?php //get_boule_leadership( $chapter_name ) ?>
			        </div>
		        </div> -->
		    <?php //endif; ?>
		
            <!-- </div> -->

        <!-- </div>
    </section> -->


    <!-- </div> --><!-- #content -->
<!-- </div> --><!-- end padder -->

<?php //locate_template( array( 'sidebar.php' ), true ) ?>

<?php get_footer(); ?>