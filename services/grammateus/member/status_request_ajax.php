<?php
error_reporting(0);
if(empty($_POST)){
    die('Not permitted');
}


//var_dump($_POST);
//die();


require_once('../../Connections/sigma_modx.php');
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");


// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("chnge_stat_to", true, "text", "", "", "", "");
$formValidation->addField("req_cmt", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger


$ins_spp_request = new tNG_insert($conn_sigma_modx);

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
//  $emailObj->setTo("pervej86@gmail.com");
  $emailObj->setTo("tonny.joseph@sigmapiphi.org");
  $emailObj->setCC("{rsDataManager.WEB_EMAIL}");
  $emailObj->setBCC("webmaster@sigmapiphi.org");
  $emailObj->setSubject("STATUS CHANGE REQUEST | {rsArchons.BOULENAME}");
  //WriteContent method
  $emailObj->setContent("<p>The following boul� wishes to change an archon's status:</p>\n<table border=\"0\">\n<tr valign=\"top\"><td>BOUL�:</td>\n<td>{rsArchons.BOULENAME}</td></tr>\n<tr valign=\"top\"><td>ARCHON:</td>\n<td>{rsArchons.L2R_FULLNAME}</td></tr>\n<tr valign=\"top\"><td>REQUESTED STATUS:</td>\n<td>{chnge_stat_to}</td></tr>\n<tr valign=\"top\"><td>CUSTOMERCD:</td>\n<td>{archon_ref_id}</td></tr>\n<tr valign=\"top\"><td>COMMENT:</td>\n<td>{req_cmt}</td></tr>\n<tr valign=\"top\"><td>SUBMITTED BY:</td>\n<td>{rsDataManager.L2R_FULLNAME}, Grammateus, {rsArchons.BOULENAME} Boul�</td></tr>\n</table>");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  $emailObj->Execute();
  global $response;
  die(json_encode($response));
}
//end Trigger_SendEmail trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
	global $sigma_modx;
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

	$theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($sigma_modx, $theValue) : mysqli_escape_string($sigma_modx, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
	global $sigma_modx;
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

	$theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($sigma_modx, $theValue) : mysqli_escape_string($sigma_modx, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


/**
 * Set variables
 */

$post_data = array();
$columns = array('chnge_stat_to', 'req_cmt', 'req_by',
	'type_id', 'req_date', 'last_update', 'archon_ref_id',
	'DM_EMAIL', 'KT_Insert1');
//var_dump($_POST);
//die();

/**
 * Local
 */
$pos = strrpos(__DIR__, "htdocs");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'htdocs/';
}


/**
 * Server
 */

/*$pos = strrpos(__DIR__, "apache");
if($pos !== false){
    $new_path = substr(__DIR__, 0, $pos) . 'apache/';
}*/


//require $new_path . "includes/functions.php";


$data = $response = array();
parse_str($_POST['data'], $data);
//array_walk($data, 'test_in_array');
$response['type'] = 'success';

$post_data['chnge_stat_to'] = $data['chnge_stat_to'];
$post_data['req_cmt'] = trim(htmlspecialchars_decode($data['req_cmt'], ENT_NOQUOTES));
$post_data['req_by'] = $data['visitor_id'];
$post_data['type_id'] = '';
$post_data['req_date'] = '';
$post_data['last_update'] = '';
$post_data['archon_ref_id'] = $_POST['user_id'];
$post_data['DM_EMAIL'] = $data['DM_EMAIL'];
$post_data['KT_Insert1'] = $data['KT_Insert1'];

$_POST = $post_data;

/**
 * End custom code variable
 */

$colname_rsArchons = "-1";
if (isset($_POST['archon_ref_id'])) {
  $colname_rsArchons = $_POST['archon_ref_id'];
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsArchons = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsArchons, "text"));
$rsArchons = mysqli_query($sigma_modx, $query_rsArchons) or die(mysqli_error($sigma_modx));
$row_rsArchons = mysqli_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysqli_num_rows($rsArchons);

$colname_rsDataManager = "-1";
if (isset($_POST['req_by'])) {
  $colname_rsDataManager = $_POST['req_by'];
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsDataManager = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname_rsDataManager, "int"));
$rsDataManager = mysqli_query($sigma_modx, $query_rsDataManager) or die(mysqli_error($sigma_modx));
$row_rsDataManager = mysqli_fetch_assoc($rsDataManager);
$totalRows_rsDataManager = mysqli_num_rows($rsDataManager);

// Make an insert transaction instance
//$ins_spp_request = new tNG_insert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_request);
// Register triggers
$ins_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_request->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archon-management-update.php?BOULENAME={rsArchon.CHAPTERID}");
$ins_spp_request->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_request->setTable("spp_request");
$ins_spp_request->addColumn("archon_ref_id", "NUMERIC_TYPE", "POST", "archon_ref_id", "");
$ins_spp_request->addColumn("chnge_stat_to", "STRING_TYPE", "POST", "chnge_stat_to");
$ins_spp_request->addColumn("req_cmt", "STRING_TYPE", "POST", "req_cmt");
$ins_spp_request->addColumn("req_by", "NUMERIC_TYPE", "POST", "req_by", "{GET.webInternalKey}");
$ins_spp_request->addColumn("type_id", "NUMERIC_TYPE", "POST", "type_id", "1");
$ins_spp_request->addColumn("req_date", "DATE_TYPE", "POST", "req_date", "{NOW_DT}");
$ins_spp_request->addColumn("last_update", "DATE_TYPE", "POST", "last_update", "{now_dt}");
$ins_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
/*$rsspp_request = $tNGs->getRecordset("spp_request");
$row_rsspp_request = mysqli_fetch_assoc($rsspp_request);
$totalRows_rsspp_request = mysqli_num_rows($rsspp_request);

if(!$totalRows_rsspp_request){
	$response['type'] = 'error';
}*/

mysqli_free_result($rsArchons);

mysqli_free_result($rsDataManager);

die(json_encode($response));