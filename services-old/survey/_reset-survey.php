<?php require_once('../Connections/limesurvey.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_limesurvey = new KT_connection($limesurvey, $database_limesurvey);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an insert transaction instance
$ins_lime_tokens_51728 = new tNG_multipleInsert($conn_limesurvey);
$tNGs->addTransaction($ins_lime_tokens_51728);
// Register triggers
$ins_lime_tokens_51728->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_lime_tokens_51728->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_lime_tokens_51728->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_lime_tokens_51728->setTable("lime_tokens_51728");
$ins_lime_tokens_51728->setPrimaryKey("attribute_1", "STRING_TYPE");

// Make an update transaction instance
$upd_lime_tokens_51728 = new tNG_multipleUpdate($conn_limesurvey);
$tNGs->addTransaction($upd_lime_tokens_51728);
// Register triggers
$upd_lime_tokens_51728->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_lime_tokens_51728->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_lime_tokens_51728->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_lime_tokens_51728->setTable("lime_tokens_51728");
$upd_lime_tokens_51728->addColumn("completed", "STRING_TYPE", "POST", "completed");
$upd_lime_tokens_51728->setPrimaryKey("attribute_1", "STRING_TYPE", "GET", "attribute_1");

// Make an instance of the transaction object
$del_lime_tokens_51728 = new tNG_multipleDelete($conn_limesurvey);
$tNGs->addTransaction($del_lime_tokens_51728);
// Register triggers
$del_lime_tokens_51728->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_lime_tokens_51728->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_lime_tokens_51728->setTable("lime_tokens_51728");
$del_lime_tokens_51728->setPrimaryKey("attribute_1", "STRING_TYPE", "GET", "attribute_1");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rslime_tokens_51728 = $tNGs->getRecordset("lime_tokens_51728");
$row_rslime_tokens_51728 = mysql_fetch_assoc($rslime_tokens_51728);
$totalRows_rslime_tokens_51728 = mysql_num_rows($rslime_tokens_51728);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
</head>

<body>


<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['attribute_1'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    Lime_tokens_51728 </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rslime_tokens_51728 > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        </table>
        <input type="hidden" name="kt_pk_lime_tokens_51728_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rslime_tokens_51728['kt_pk_lime_tokens_51728']); ?>" />
        <input type="hidden" name="completed_<?php echo $cnt1; ?>" id="completed_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rslime_tokens_51728['completed']); ?>" />
        <?php } while ($row_rslime_tokens_51728 = mysql_fetch_assoc($rslime_tokens_51728)); ?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1
      if (@$_GET['attribute_1'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php }
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</body>
</html>
