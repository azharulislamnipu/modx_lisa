<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

//MX Widgets3 include
require_once('../../includes/wdg/WDG.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("JOINDATE", true, "date", "date", "", "", "Please provide the date the archon assumed office.");
$tNGs->prepareValidation($formValidation);
// End trigger

// begin Recordset
$colname__rsArchon = '-1';
if (isset($_GET['CUSTOMERID'])) {
  $colname__rsArchon = $_GET['CUSTOMERID'];
}
$query_rsArchon = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname__rsArchon, "int"));
$rsArchon = $sigma_modx->SelectLimit($query_rsArchon) or die($sigma_modx->ErrorMsg());
$totalRows_rsArchon = $rsArchon->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_officers = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_officers);
// Register triggers
$ins_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$ins_spp_officers->setTable("spp_officers");
$ins_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_officers = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_officers);
// Register triggers
$upd_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$upd_spp_officers->setTable("spp_officers");
$upd_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "CURRVAL", "");
$upd_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "POST", "JOINDATE");
$upd_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "CURRVAL", "");
$upd_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "CURRVAL", "");
$upd_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE", "GET", "OFFICERID");

// Make an instance of the transaction object
$del_spp_officers = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_officers);
// Register triggers
$del_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$del_spp_officers->setTable("spp_officers");
$del_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE", "GET", "OFFICERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_officers = $tNGs->getRecordset("spp_officers");
$totalRows_rsspp_officers = $rsspp_officers->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wdg="http://www.interaktonline.com/MXWidgets" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<script type="text/javascript" src="../../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../../includes/resources/calendar.js"></script>
<style type="text/css">
<!--
.style2 {font-size: 16px}
-->
</style>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    Change Officer Start Date </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_officers->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_officers > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <?php 
// Show IF Conditional show_CPOSITION_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
              <tr>
                <td class="KT_th">&nbsp;</td>
                <td><span class="style2"><?php echo $rsArchon->Fields('L2R_FULLNAME'); ?></span></td>
              </tr>
            <tr>
              <td class="KT_th">OFFICE:</td>
              <td><?php echo KT_escapeAttribute($rsspp_officers->Fields('CPOSITION')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_CPOSITION_on_update_only
?>
          <?php 
// Show IF Conditional show_JOINDATE_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="JOINDATE_<?php echo $cnt1; ?>">START DATE:</label></td>
              <td><input name="JOINDATE_<?php echo $cnt1; ?>" id="JOINDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_officers->Fields('JOINDATE')); ?>" size="10" maxlength="22" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:mondayfirst="false" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
                  <?php echo $tNGs->displayFieldHint("JOINDATE");?> <?php echo $tNGs->displayFieldError("spp_officers", "JOINDATE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_JOINDATE_on_update_only
?>
          <?php 
// Show IF Conditional show_UPDATEUSERCD_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
            <tr>
              <td class="KT_th">UPDATED BY:</td>
              <td><?php echo KT_escapeAttribute($rsspp_officers->Fields('UPDATEUSERCD')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_UPDATEUSERCD_on_update_only
?>
          <?php 
// Show IF Conditional show_UPDATETMS_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
            <tr>
              <td class="KT_th">LAST UPDATED:</td>
              <td><?php echo KT_formatDate($rsspp_officers->Fields('UPDATETMS')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_UPDATETMS_on_update_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_officers_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_officers->Fields('kt_pk_spp_officers')); ?>" />
        <?php
    $rsspp_officers->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['OFFICERID'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <!-- <input type="submit" name="KT_Delete1" value="<?php //echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php //echo NXT_getResource("Are you sure?"); ?>');" /> -->
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
  <!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchon->Close();
?>
