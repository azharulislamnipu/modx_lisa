<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsSiresByRegion = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_rsSiresByRegion = $_COOKIE['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsSiresByRegion = sprintf("SELECT * FROM vw_boule_officers WHERE REGIONNAME = %s AND COMMITTEESTATUSSTT = 'Active' AND    (vw_boule_officers.CPOSITION LIKE 'Sire%%' OR vw_boule_officers.CPOSITION LIKE 'Immed%%') ORDER BY BOULENAME ASC", GetSQLValueString($colname_rsSiresByRegion, "text"));
$rsSiresByRegion = mysql_query($query_rsSiresByRegion, $sigma_modx) or die(mysql_error());
$row_rsSiresByRegion = mysql_fetch_assoc($rsSiresByRegion);
$totalRows_rsSiresByRegion = mysql_num_rows($rsSiresByRegion);

$colname_result = "-1";
if (isset($_COOKIE['GRAMMID'])) {
  $colname_result = $_COOKIE['GRAMMID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_result = sprintf("SELECT REGIONNAME, BOULENAME, CHAPTERID FROM vw_web_elig_no_officers WHERE CUSTOMERID = %s", GetSQLValueString($colname_result, "int"));
$result = mysql_query($query_result, $sigma_modx) or die(mysql_error());
$row_result = mysql_fetch_assoc($result);
$totalRows_result = mysql_num_rows($result);

$colname_exSires = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_exSires = $_COOKIE['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_exSires = sprintf("SELECT vw_boule_officers.CPOSITION AS OFFICE,   vw_boule_officers.BOULENAME,   vw_boule_officers.PREFIX,   vw_boule_officers.FIRSTNAME,   vw_boule_officers.MIDDLEINITIAL,   vw_boule_officers.LASTNAME,   vw_boule_officers.SUFFIX AS SUFFIX1,   vw_boule_officers.DESIGNATIONLST AS SUFFIX,   vw_boule_officers.ADDRESS1,   vw_boule_officers.CITY,   vw_boule_officers.STATECD AS STATE,   vw_boule_officers.ZIP,   vw_boule_officers.WEB_EMAIL AS EMAIL,   vw_boule_officers.HOMEPHONE AS HOME_PHONE,   vw_boule_officers.ALTEMAIL AS WORK_EMAIL,   vw_boule_officers.WORKPHONE AS WORK_PHONE FROM vw_boule_officers WHERE REGIONNAME = %s AND COMMITTEESTATUSSTT = 'Active' AND    (vw_boule_officers.CPOSITION LIKE 'Sire%%' OR vw_boule_officers.CPOSITION LIKE 'Immed%%') ORDER BY BOULENAME ASC", GetSQLValueString($colname_exSires, "text"));
$exSires = mysql_query($query_exSires, $sigma_modx) or die(mysql_error());
$row_exSires = mysql_fetch_assoc($exSires);
$totalRows_exSires = mysql_num_rows($exSires);

//Export to Excel Server Behavior
if (isset($_POST['sire_export'])&&($_POST['sire_export']=="sire_archons")){
	$delim="";
	$delim_replace="";
	if($delim==""){
		$lang=(strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],",")===false)?$_SERVER['HTTP_ACCEPT_LANGUAGE']:substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],","));
		$semi_array=array("af","zh-hk","zh-mo","zh-cn","zh-sg","zh-tw","fr-ch","de-li","de-ch","it-ch","ja","ko","es-do","es-sv","es-gt","es-hn","es-mx","es-ni","es-pa","es-pe","es-pr","sw");
		$delim=(in_array($lang,$semi_array) || substr_count($lang,"en")>0)?",":";";
	}
	$output="";
	$include_hdr="1";
	if($include_hdr=="1"){
		$totalColumns_exSires=mysql_num_fields($exSires);
		for ($x=0; $x<$totalColumns_exSires; $x++) {
			if($x==$totalColumns_exSires-1){$comma="";}else{$comma=$delim;}
			$output = $output.(ereg_replace("_", " ",mysql_field_name($exSires, $x))).$comma;
		}
		$output = $output."\r\n";
	}

	do{$fixcomma=array();
    		foreach($row_exSires as $r){array_push($fixcomma,ereg_replace($delim,$delim_replace,$r));}
		$line = join($delim,$fixcomma);
    		$line=ereg_replace("\r\n", " ",$line);
    		$line = "$line\n";
    		$output=$output.$line;}while($row_exSires = mysql_fetch_assoc($exSires));
	header("Content-Type: application/xls");
	header("Content-Disposition: attachment; filename=Sire_Archons.csv");
	header("Content-Type: application/force-download");
	header("Cache-Control: post-check=0, pre-check=0", false);
	echo $output;
	die();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_regional_grammateus.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow"><?php echo $_COOKIE['REGIONNAME']; ?> Region Sire Archons</h1>
<div class="contentBlock">
<table width="100%" cellpadding="2">
<tr>
  <td><a href="/home/reg_gramm_mgmt.php">Back to Officer's Listing</a></td>
  <td><form id="sire" name="sire" method="post" action="">
    <input name="sire_export" type="hidden" id="sire_archons" value="sire_archons" />
    <input name="export" type="submit" value="Click to Export Sire Archons Data" />
    </form>
  </td>
  <td align="right">&nbsp;</td>
  <td colspan="2" align="right"><a href="add_officer.php?KT_back=1">Add New Officer</a></td>
  </tr>
  <tr>
    <td><strong>BOUL&Eacute;</strong></td>
    <td><strong>ARCHON</strong></td>
    <td><strong>OFFICE</strong></td>
    <td><strong>START DATE</strong></td>
    <td><strong>EDIT</strong></td>
  </tr>
  <?php do { ?>
    <tr valign="top">
      <td><?php echo $row_rsSiresByRegion['BOULENAME']; ?></td>
      <td>
<script language="JavaScript">
    function edit_<?php echo $row_rsSiresByRegion['CUSTOMERCD'];?>(){
        document.forms.form_<?php echo $row_rsSiresByRegion['CUSTOMERCD']; ?>.submit();
    }
</script>
  <form name="form_<?php echo $row_rsSiresByRegion['CUSTOMERCD']; ?>" action="/services/grammateus/regional/sires_edit.php" method="get">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsSiresByRegion['CUSTOMERCD']); ?>">
  <input type="hidden" name="GRAMMID" value="<?php echo KT_escapeAttribute($_COOKIE['GRAMMID']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_COOKIE['UPDATEDBY']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_COOKIE['BOULENAME']); ?>">
  <input type="hidden" name="REGIONNAME" value="<?php echo KT_escapeAttribute($_COOKIE['REGIONNAME']); ?>">
  </form>
   
      
      <a href="#" rel="<?php echo $row_rsSiresByRegion['WEB_ID']; ?>" onclick="edit_<?php echo $row_rsSiresByRegion['CUSTOMERCD'];?>();return(FALSE);"><?php echo $row_rsSiresByRegion['FULLNAME']; ?></a>
          <div id="<?php echo $row_rsSiresByRegion['WEB_ID']; ?>" class="balloonstyle">
            <?php echo $row_rsSiresByRegion['ADDRESS1']; ?><br />
            <?php echo $row_rsSiresByRegion['CITY']; ?>, <?php echo $row_rsSiresByRegion['STATECD']; ?> <?php echo $row_rsSiresByRegion['ZIP']; ?><br />
            HOME PHONE: <?php echo $row_rsSiresByRegion['HOMEPHONE']; ?><br />
            EMAIL: <?php echo $row_rsSiresByRegion['EMAIL']; ?><br />
            BUSINESS PHONE: <?php echo $row_rsSiresByRegion['WORKPHONE']; ?><br />
            BUSINESS EMAIL: <?php echo $row_rsSiresByRegion['ALTEMAIL']; ?>    </div></td>
      <td><?php echo $row_rsSiresByRegion['CPOSITION']; ?></td>
      <td><?php echo $row_rsSiresByRegion['STARTDATE']; ?></td>
      <td><a href="change_date.php?OFFICERID=<?php echo $row_rsSiresByRegion['OFFICERID']; ?>&amp;CUSTOMERCD=<?php echo $row_rsSiresByRegion['CUSTOMERCD']; ?>&amp;KT_back=1">Change Start Date</a><br />
        <a href="remove_officer.php?OFFICERID=<?php echo $row_rsSiresByRegion['OFFICERID']; ?>&amp;CUSTOMERCD=<?php echo $row_rsSiresByRegion['CUSTOMERCD']; ?>&amp;KT_back=1">Remove from Office</a></td>
    </tr>
    <?php } while ($row_rsSiresByRegion = mysql_fetch_assoc($rsSiresByRegion)); ?>
<tr>
  <td><a href="/home/reg_gramm_mgmt.php">Back to Officer's Listing</a></td>
  <td align="right">&nbsp;</td>
  <td align="right">&nbsp;</td>
  <td colspan="2" align="right"><a href="add_officer.php?KT_back=1">Add New Officer</a></td>
  </tr>
</table>
</div>
<!-- InstanceEndEditable --></div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsSiresByRegion);

mysql_free_result($result);

mysql_free_result($exSires);
?>
