<?php

/**
 * Responsible for building and displaying Roster list.
 * Includes functions for retrieval, filtering and sorting.
 * @author Pat Heard {http://fullahead.org}
 */

require_once 'ArchonBean.php';
require_once 'ArchonDuesBean.php';
require_once 'RosterUtils.php';

class Roster {

	const PAYTYPE_ECHECK = 'E';

	private $year = -1;
	private $regioncd = '';
	private $chapterid = '';
	private $chaptername = '';
	private $archons = null;
	private $officers = null;
	private $paytype = null;
	private $status_count = null;
	private $is_completable = false;
	private $is_submittable = false;
	private $is_finalized = false;
	private $is_editable = false;

	public function __construct(array $args = null){

		// Set default arguments
		$args = array_merge(array(
			'year' => -1,
			'regioncd' => '',
			'chapterid' => '',
			'chaptername' => '',
			'archons' => null,
			'paytype' => null,
			'status_count' => null,
			'is_completable' => null,
			'is_submittable' => null,
			'is_finalized' => null,
			'is_editable' => null,
		), $args);

		$this->year = $args['year'];
		$this->regioncd = $args['regioncd'];
		$this->chapterid = $args['chapterid'];
		$this->chaptername = $args['chaptername'];
		$this->archons = $args['archons'];
		$this->paytype = $args['paytype'];
		$this->status_count = $args['status_count'];
		$this->is_completable = $args['is_completable'];
		$this->is_finalized = $args['is_finalized'];
		$this->is_submittable = $args['is_submittable'];
		$this->is_editable = $args['is_editable'];

		// Get the status counts and officers
		$this->setStatusCounts();
		$this->setOfficers();

        // Set the boolean properties based on the state of the Archons
		$this->setCompletable();
		$this->setSubmittable();
		$this->setFinalized();
		$this->setEditable();
	}

	/**
	 * Populates the counts in the status associative arrays
	 */
	public function setStatusCounts(){
		if($this->archons !== null){
			$status_requested_count = array();
			$status_current_count = array();
			$dues = array();

			// Build up the requested and current counts
			foreach($this->archons as $archon){
				if($archon->isDuesRecorded()){
					$this->addStatusCount($status_requested_count, $archon->status_requested, 1); // Requested status
					$this->addStatusCount($dues, $archon->status_requested, ($archon->dues_amount + $archon->dues_amount_additional)); // Dues for requested status
				}
				$this->addStatusCount($status_current_count, $archon->archon->status, 1);     // Current status
			}

            // Merge the 2 status types into the status_count 2D array
            $this->status_count = array();
            foreach(Status::get_all_status() as $status){
                $this->status_count[$status] = array(
                    'current' => ((isset($status_current_count[$status]) ? $status_current_count[$status] : 0)),
                    'requested' => ((isset($status_requested_count[$status]) ? $status_requested_count[$status] : 0)),
                    'dues' => ((isset($dues[$status]) ? $dues[$status] : 0))
                );
            }
		}
	}

        /**
         * Populates the officers of the Boule
         */
        public function setOfficers($officers = null) {
            if(empty($officers)) {
                $officers = $this->findOfficers();
            }

            // Populate the required types in the correct order
            $this->officers = array();
            foreach(Officers::get_all_officers() as $position) {
                $this->officers[$position] = isset($officers[$position]) ? $officers[$position] : null;
            }
        }

        /**
         * Looks through the roster to find the officers
         */
        private function findOfficers(){
            $officers = array();
            if($this->archons !== null){

                // Find the officers
                foreach($this->archons as $dues){
                    if($dues->archon->officer_status === 'Active') {
                        $officers[$dues->archon->officer_position] = $dues->archon;
                    }
                }
            }
            return $officers;
        }

	/**
	 * Adds the integer $i to the given $key in the passed $arr array
	 */
	private function addStatusCount(&$arr, $key, $i){
		if($arr !== null){
			if(isset($arr[$key])){
				$arr[$key] += $i;
			} else {
				$arr[$key] = $i;
			}
		}
	}

	/**
	 * Can this Roster be completed (all Archons have Dues recorded)
	 */
	public function setCompletable(){
		$this->is_completable = false;
		if($this->archons !== null){
			foreach($this->archons as $archon){
				if(!($this->is_completable = $archon->isCompletable())){
					break;
				}
			}
		}
	}

	/**
	 * Can this Roster be submitted to the Office of the Grand Boule (roster has been marked as completed)
	 */
	public function setSubmittable(){
		$this->is_submittable = false;
		if($this->archons !== null){
			foreach($this->archons as $archon){
				if(!($this->is_submittable = $archon->isSubmittable())){
					break;
				}
			}
		}
	}


	/**
	 * Can this Roster be converted back to an editable state?
	 */
	public function setEditable(){
		$this->is_editable = false;
		if($this->archons !== null){
			foreach($this->archons as $archon){
				if(!($this->is_editable = $archon->isEditable())){
					break;
				}
			}
		}
	}

	/**
	 * Is this roster finalized?  (already submitted)
	 */
	public function setFinalized(){
		$this->is_finalized = false;
		if($this->archons !== null){
			foreach($this->archons as $archon){
				if(!($this->is_finalized = $archon->isFinalized())){
					break;
				}
			}
		}
	}

    /**
     * Returns a string representation of the Roster's status
     * @return string Status of the roster
     */
    public function getRosterStatus(){
        if($this->is_completable){
            return 'completable';
        } else if ($this->is_submittable){
            return 'submittable';
        } else if ($this->is_finalized){
            return 'finalized';
        } else {
            return 'editable';
        }
    }

    /**
     * Creates a PDF based on this Roster
     */
    public function isECheck(){
        return $this->paytype === Roster::PAYTYPE_ECHECK;
    }

	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function __toString(){
		return
			' year: ' . $this->year .
			' regioncd: ' . $this->regioncd .
			' chapterid: ' . $this->chapterid .
			' archons: ' . print_r($this->archons, true) .
			' status_count: ' . print_r($this->status_count, true) .
			' is_completable: ' . $this->is_completable .
                        ' is_submittable: ' . $this->is_submittable .
			' is_finalized: ' . $this->is_finalized .
			' is_editable: ' . $this->is_editable;
	}

}
?>