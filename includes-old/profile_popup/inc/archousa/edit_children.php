<div id="archon_edit_children" class="accordion parent-elem edit_children" data-accordion>
    <div data-control><h3>Edit Children</h3></div>
    <div data-content>
        <div class="data-content-inner data-content-inner-tabs ymp_form_input">
            <?php

            $i = 0;

            if(!empty($all_children_data)) {

                foreach ($all_children_data as $index => $children_single) {                    ?>
                    <div id="archon_edit_children_<?php echo $index; ?>"
                         class="accordion parent-elem edit_career_inner" data-accordion>
                        <div data-control>
                            <h3><?php echo $children_single['FIRSTNAME'] . ' ' . $children_single['MIDDLEINITIAL'] . ' ' . $children_single['LASTNAME']; ?></h3></div>
                        <div data-content>
                            <div class="data-content-inner clearfix ymp_wrapper grid-12">
                                <div class="ymp-row">
                                    <div class="ymp-names table-wrap">
                                        <div class="table-row">
                                            <div class="table-cell">
                                                <label for="FIRSTNAME_<?php echo $index; ?>">First Name <span
                                                        class="required_ind">*</span></label>
                                                <input class="input-required input-validated"
                                                        name="FIRSTNAME"
                                                        id="FIRSTNAME_<?php echo $index; ?>" value="<?php echo $children_single['FIRSTNAME'];?>" />
                                            </div><!--End Item-->

                                            <div class="table-cell">
                                                <label for="MIDDLEINITIAL_<?php echo $index; ?>">Middle Name</label>
                                                <input class=""
                                                        name="MIDDLEINITIAL"
                                                        id="MIDDLEINITIAL_<?php echo $index; ?>" value="<?php echo $children_single['MIDDLEINITIAL'];?>" />
                                            </div><!--End Item-->

                                            <div class="table-cell">
                                                <label for="LASTNAME_<?php echo $index; ?>">Last Name <span
                                                        class="required_ind">*</span></label>
                                                <input class="input-required input-validated"
                                                        name="LASTNAME"
                                                        id="LASTNAME_<?php echo $index; ?>" value="<?php echo $children_single['LASTNAME'];?>" />
                                            </div><!--End Item-->

                                            <div class="table-cell">
                                        <label for="SUFFIX_<?php echo $index; ?>">Suffix</label>

                                                <select name="SUFFIX" id="SUFFIX_<?php echo $index; ?>">
                                                    <option value=""><?php echo "Select one..."; ?></option>
                                                    <?php foreach (array_column($all_suffix['data'], 'SUFFIX') as $suffix){
                                                        echo "<option value='$suffix'" . selected($suffix, $children_single['SUFFIX'], false) . ">$suffix</option>";
                                                    }?>
                                                </select>
                                    </div><!--End Item-->
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="span-6">
                                        <label>Gender <span
                                                    class="required_ind">*</span></label>
                                        <div class="checkbox_radio_wrap radio_wrap">
                                            <input id="male_<?php echo $index; ?>" type="radio" name="GENDERs" value="M"
                                            <?php checked('M',$children_single['GENDER'] );?>>
                                            <label for="male_<?php echo $index; ?>">Male</label>
                                            <input id="female_<?php echo $index; ?>" type="radio" name="GENDERs" value="F" <?php checked('F',$children_single['GENDER'] );?>>
                                            <label for="female_<?php echo $index; ?>">Female</label>
                                            <input class="input-required input-validated" type="hidden" name="GENDER" value="<?php echo $children_single['GENDER'];?>">
                                        </div>
                                    </div><!--End Item-->

                                    <div class="span-6">
                                        <label for="child_BIRTHDATE_<?php echo $index; ?>">Birth Date <span
                                                    class="required_ind">*</span></label>
                                        <div class="clearfix birth_date_input_wrap">
                                            <a class="birth_date_btn"
                                               id="child_BIRTHDATE_<?php echo $index; ?>_btn"
                                               href="#"><img
                                                    src="<?php echo $base_url; ?>/assets/templates/archon/images/calendar-icon.png"
                                                    alt="Select Date"></a>
                                            <div class="bdinput">
                                                <input type="text" data-date="" name="BIRTHDATE"
                                                       size="20" maxlength="40"
                                                       id="child_BIRTHDATE_<?php echo $index; ?>"
                                                       value="<?php echo $children_single['BIRTHDATE']; ?>"
                                                       placeholder=""
                                                class="input-required input-validated">
                                            </div>
                                        </div>
                                    </div><!--End Item-->

                                    <div class="clearfix"></div>
                                </div>

                                <input type="hidden" name="CHILDID"
                                       value="<?php echo $children_single['CHILDID']; ?>">

                                <?php echo $submit_dv; ?>
                            </div>
                        </div>
                    </div>
                <?php }

                $i = count($all_children_data);
            }

            while ($i < 20){?>

                <div id="archon_edit_children_<?php echo $i; ?>"
                     class="accordion parent-elem edit_children_inner add_children_inner" data-accordion>
                    <div data-control>
                        <h3>New Child</h3></div>
                    <div data-content>
                        <div class="data-content-inner clearfix ymp_wrapper grid-12">
                            <div class="ymp-row">

                                <div class="ymp-names table-wrap">
                                    <div class="table-row">

                                        <div class="table-cell">
                                            <label for="FIRSTNAME_<?php echo $i; ?>">First Name <span
                                                        class="required_ind">*</span></label>
                                            <input class="input-required "
                                                   name="FIRSTNAME"
                                                   id="FIRSTNAME_<?php echo $i; ?>" value="" />
                                        </div><!--End Item-->

                                        <div class="table-cell">
                                            <label for="MIDDLEINITIAL_<?php echo $i; ?>">Middle Name</label>
                                            <input class=""
                                                   name="MIDDLEINITIAL"
                                                   id="MIDDLEINITIAL_<?php echo $i; ?>" value="" />
                                        </div><!--End Item-->

                                        <div class="table-cell">
                                            <label for="LASTNAME_<?php echo $i; ?>">Last Name <span
                                                        class="required_ind">*</span></label>
                                            <input class="input-required "
                                                   name="LASTNAME"
                                                   id="LASTNAME_<?php echo $i; ?>" value="" />
                                        </div><!--End Item-->

                                        <div class="table-cell">
                                            <label for="SUFFIX_<?php echo $i; ?>">Suffix</label>
                                            <select name="SUFFIX" id="SUFFIX_<?php echo $i; ?>">
                                                <option value=""><?php echo "Select one..."; ?></option>
                                                <?php echo $all_suffix_html;?>
                                            </select>
                                        </div><!--End Item-->
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="span-6">
                                    <label>Gender <span
                                                class="required_ind">*</span></label>
                                    <div class="checkbox_radio_wrap radio_wrap">
                                        <input id="male_<?php echo $i; ?>" type="radio" name="GENDERs" value="M">
                                        <label for="male_<?php echo $i; ?>">Male</label>
                                        <input id="female_<?php echo $i; ?>" type="radio" name="GENDERs" value="F">
                                        <label for="female_<?php echo $i; ?>">Female</label>
                                        <input class="input-required " type="hidden" name="GENDER" value="">
                                    </div>
                                </div><!--End Item-->

                                <div class="span-6">
                                    <label for="child_BIRTHDATE_<?php echo $i; ?>">Birth Date <span
                                                class="required_ind">*</span></label>
                                    <div class="clearfix birth_date_input_wrap">
                                        <a class="birth_date_btn"
                                           id="child_BIRTHDATE_<?php echo $i; ?>_btn"
                                           href="#"><img
                                                    src="<?php echo $base_url; ?>/assets/templates/archon/images/calendar-icon.png"
                                                    alt="Select Date"></a>
                                        <div class="bdinput">
                                            <input type="text" data-date="" name="BIRTHDATE"
                                                   size="20" maxlength="40"
                                                   id="child_BIRTHDATE_<?php echo $i; ?>"
                                                   value=""
                                                   placeholder=""
                                            class="input-required ">
                                        </div>
                                    </div>
                                </div><!--End Item-->

                                <div class="clearfix"></div>
                            </div>

                            <input type="hidden" name="CHILDID"
                                   value="">

                            <?php echo $submit_dv; ?>
                        </div>
                    </div>
                </div>
                <?php
                ++$i;
            }


            ?>

        </div>
        <div class="sb_div">
            <button class="button add_new_child">Add New Child</button>
        </div>

    </div>
</div>