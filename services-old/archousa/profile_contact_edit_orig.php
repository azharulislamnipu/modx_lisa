<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("PREFIX", true, "text", "", "", "", "");
$formValidation->addField("FIRSTNAME", true, "text", "", "", "", "");
$formValidation->addField("LASTNAME", true, "text", "", "", "", "");
$formValidation->addField("EMAIL", true, "text", "", "", "", "You must provide your Archousa's email address.");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsHon = "SELECT * FROM spp_hon ORDER BY DESIGNATIONLST ASC";
$rsHon = mysql_query($query_rsHon, $sigma_modx) or die(mysql_error());
$row_rsHon = mysql_fetch_assoc($rsHon);
$totalRows_rsHon = mysql_num_rows($rsHon);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOccup = "SELECT * FROM spp_occupation ORDER BY `DESCRIPTION` ASC";
$rsOccup = mysql_query($query_rsOccup, $sigma_modx) or die(mysql_error());
$row_rsOccup = mysql_fetch_assoc($rsOccup);
$totalRows_rsOccup = mysql_num_rows($rsOccup);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsTitle = "SELECT SKILLID, SKILLCD, `DESCRIPTION` FROM spp_skillref ORDER BY `DESCRIPTION` ASC";
$rsTitle = mysql_query($query_rsTitle, $sigma_modx) or die(mysql_error());
$row_rsTitle = mysql_fetch_assoc($rsTitle);
$totalRows_rsTitle = mysql_num_rows($rsTitle);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsState = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsState = mysql_query($query_rsState, $sigma_modx) or die(mysql_error());
$row_rsState = mysql_fetch_assoc($rsState);
$totalRows_rsState = mysql_num_rows($rsState);

// Make an update transaction instance
$upd_spp_archousa = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archousa);
// Register triggers
$upd_spp_archousa->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archousa->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archousa->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-profile.php");
// Add columns
$upd_spp_archousa->setTable("spp_archousa");
$upd_spp_archousa->addColumn("PREFIX", "STRING_TYPE", "POST", "PREFIX");
$upd_spp_archousa->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$upd_spp_archousa->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$upd_spp_archousa->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$upd_spp_archousa->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$upd_spp_archousa->addColumn("DESIGNATIONLST", "STRING_TYPE", "POST", "DESIGNATIONLST");
$upd_spp_archousa->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$upd_spp_archousa->addColumn("HOMEPHONE", "STRING_TYPE", "POST", "HOMEPHONE");
$upd_spp_archousa->addColumn("MOBILEPHONE", "STRING_TYPE", "POST", "MOBILEPHONE");
$upd_spp_archousa->addColumn("OCCUPATIONCD", "STRING_TYPE", "POST", "OCCUPATIONCD");
$upd_spp_archousa->addColumn("JOBTITLE", "STRING_TYPE", "POST", "JOBTITLE");
$upd_spp_archousa->addColumn("ORGNAME", "STRING_TYPE", "POST", "ORGNAME");
$upd_spp_archousa->addColumn("ALTCITY", "STRING_TYPE", "POST", "ALTCITY");
$upd_spp_archousa->addColumn("ALTSTATE", "STRING_TYPE", "POST", "ALTSTATE");
$upd_spp_archousa->addColumn("ALTZIP", "STRING_TYPE", "POST", "ALTZIP");
$upd_spp_archousa->addColumn("WORKPHONE", "STRING_TYPE", "POST", "WORKPHONE");
$upd_spp_archousa->addColumn("ALTEMAIL", "STRING_TYPE", "POST", "ALTEMAIL");
$upd_spp_archousa->addColumn("BIRTHDATE", "DATE_TYPE", "POST", "BIRTHDATE");
$upd_spp_archousa->addColumn("ANNIVERSARYDATE", "DATE_TYPE", "POST", "ANNIVERSARYDATE");
$upd_spp_archousa->addColumn("UPDATEDBY", "STRING_TYPE", "VALUE", "{SESSION.webShortname}");
$upd_spp_archousa->addColumn("LASTUPDATED", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archousa->setPrimaryKey("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archousa = $tNGs->getRecordset("spp_archousa");
$row_rsspp_archousa = mysql_fetch_assoc($rsspp_archousa);
$totalRows_rsspp_archousa = mysql_num_rows($rsspp_archousa);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  dir="ltr" lang="en-EN"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">Page Name</h1>
<div class="contentBlock">
  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="PREFIX">SALUTATION:</label></td>
        <td><input type="text" name="PREFIX" id="PREFIX" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['PREFIX']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("PREFIX");?> <?php echo $tNGs->displayFieldError("spp_archousa", "PREFIX"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="FIRSTNAME">FIRST NAME:</label></td>
        <td><input type="text" name="FIRSTNAME" id="FIRSTNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['FIRSTNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("spp_archousa", "FIRSTNAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="MIDDLEINITIAL">MIDDLE INITIAL:</label></td>
        <td><input type="text" name="MIDDLEINITIAL" id="MIDDLEINITIAL" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['MIDDLEINITIAL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MIDDLEINITIAL");?> <?php echo $tNGs->displayFieldError("spp_archousa", "MIDDLEINITIAL"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="LASTNAME">LAST NAME:</label></td>
        <td><input type="text" name="LASTNAME" id="LASTNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['LASTNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("spp_archousa", "LASTNAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="SUFFIX">SUFFIX:</label></td>
        <td><input type="text" name="SUFFIX" id="SUFFIX" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['SUFFIX']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("SUFFIX");?> <?php echo $tNGs->displayFieldError("spp_archousa", "SUFFIX"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="DESIGNATIONLST">HONORIFIC(S):</label></td>
        <td><select name="DESIGNATIONLST" id="DESIGNATIONLST">
            <?php 
do {  
?>
            <option value="<?php echo $row_rsHon['DESIGNATIONLST']?>"<?php if (!(strcmp($row_rsHon['DESIGNATIONLST'], $row_rsspp_archousa['DESIGNATIONLST']))) {echo "SELECTED";} ?>><?php echo $row_rsHon['DESIGNATIONLST']?></option>
            <?php
} while ($row_rsHon = mysql_fetch_assoc($rsHon));
  $rows = mysql_num_rows($rsHon);
  if($rows > 0) {
      mysql_data_seek($rsHon, 0);
	  $row_rsHon = mysql_fetch_assoc($rsHon);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archousa", "DESIGNATIONLST"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="EMAIL">CONTACT EMAIL:</label></td>
        <td><input type="text" name="EMAIL" id="EMAIL" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['EMAIL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("EMAIL");?> <?php echo $tNGs->displayFieldError("spp_archousa", "EMAIL"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="HOMEPHONE">HOME PHONE:</label></td>
        <td><input type="text" name="HOMEPHONE" id="HOMEPHONE" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['HOMEPHONE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("HOMEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "HOMEPHONE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="MOBILEPHONE">MOBILE PHONE:</label></td>
        <td><input type="text" name="MOBILEPHONE" id="MOBILEPHONE" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['MOBILEPHONE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MOBILEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "MOBILEPHONE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="OCCUPATIONCD">PROFESSION:</label></td>
        <td><select name="OCCUPATIONCD" id="OCCUPATIONCD">
            <?php 
do {  
?>
            <option value="<?php echo $row_rsOccup['DESCRIPTION']?>"<?php if (!(strcmp($row_rsOccup['DESCRIPTION'], $row_rsspp_archousa['OCCUPATIONCD']))) {echo "SELECTED";} ?>><?php echo $row_rsOccup['DESCRIPTION']?></option>
            <?php
} while ($row_rsOccup = mysql_fetch_assoc($rsOccup));
  $rows = mysql_num_rows($rsOccup);
  if($rows > 0) {
      mysql_data_seek($rsOccup, 0);
	  $row_rsOccup = mysql_fetch_assoc($rsOccup);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archousa", "OCCUPATIONCD"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="JOBTITLE">TITLE:</label></td>
        <td><select name="JOBTITLE" id="JOBTITLE">
            <?php 
do {  
?>
            <option value="<?php echo $row_rsTitle['DESCRIPTION']?>"<?php if (!(strcmp($row_rsTitle['DESCRIPTION'], $row_rsspp_archousa['JOBTITLE']))) {echo "SELECTED";} ?>><?php echo $row_rsTitle['DESCRIPTION']?></option>
            <?php
} while ($row_rsTitle = mysql_fetch_assoc($rsTitle));
  $rows = mysql_num_rows($rsTitle);
  if($rows > 0) {
      mysql_data_seek($rsTitle, 0);
	  $row_rsTitle = mysql_fetch_assoc($rsTitle);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archousa", "JOBTITLE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ORGNAME">ORGANIZATION NAME:</label></td>
        <td><input type="text" name="ORGNAME" id="ORGNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ORGNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ORGNAME");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ORGNAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTCITY">CITY:</label></td>
        <td><input type="text" name="ALTCITY" id="ALTCITY" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ALTCITY']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTCITY");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ALTCITY"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTSTATE">STATE:</label></td>
        <td><select name="ALTSTATE" id="ALTSTATE">
            <?php 
do {  
?>
            <option value="<?php echo $row_rsState['st_cd']?>"<?php if (!(strcmp($row_rsState['st_cd'], $row_rsspp_archousa['ALTSTATE']))) {echo "SELECTED";} ?>><?php echo $row_rsState['st_nm']?></option>
            <?php
} while ($row_rsState = mysql_fetch_assoc($rsState));
  $rows = mysql_num_rows($rsState);
  if($rows > 0) {
      mysql_data_seek($rsState, 0);
	  $row_rsState = mysql_fetch_assoc($rsState);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archousa", "ALTSTATE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTZIP">ZIP:</label></td>
        <td><input type="text" name="ALTZIP" id="ALTZIP" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ALTZIP']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTZIP");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ALTZIP"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="WORKPHONE">WORK PHONE:</label></td>
        <td><input type="text" name="WORKPHONE" id="WORKPHONE" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['WORKPHONE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("WORKPHONE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "WORKPHONE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTEMAIL">WORK EMAIL:</label></td>
        <td><input type="text" name="ALTEMAIL" id="ALTEMAIL" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ALTEMAIL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTEMAIL");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ALTEMAIL"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="BIRTHDATE">BIRTH DATE:</label></td>
        <td><input type="text" name="BIRTHDATE" id="BIRTHDATE" value="<?php echo KT_formatDate($row_rsspp_archousa['BIRTHDATE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("BIRTHDATE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "BIRTHDATE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ANNIVERSARYDATE">ANNIVERSARY DATE:</label></td>
        <td><input type="text" name="ANNIVERSARYDATE" id="ANNIVERSARYDATE" value="<?php echo KT_formatDate($row_rsspp_archousa['ANNIVERSARYDATE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ANNIVERSARYDATE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ANNIVERSARYDATE"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Update" /> <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/archousa-profile.php')" />
        </td>
      </tr>
    </table>
   <input type="hidden" name="ARCHOUSAID" id="ARCHOUSAID" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ARCHOUSAID']); ?>" />
    <input type="hidden" name="UPDATEDBY" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['UPDATEDBY']); ?>" />
     <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo KT_formatDate($row_rsspp_archousa['LASTUPDATED']); ?>" />
  </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsHon);

mysql_free_result($rsOccup);

mysql_free_result($rsTitle);

mysql_free_result($rsState);
?>
