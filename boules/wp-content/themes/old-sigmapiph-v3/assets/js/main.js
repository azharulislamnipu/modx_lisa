/*
 * Main javascript behaviour of the pages
 */
/* global Modernizr: false, jQuery: false */
(function($) {
	"use strict";
	
/**
 * Input placeholder polyfill
 */ 
if(!Modernizr.input.placeholder) {
	var $search = $('header input[type="text"], header input[type="password"]'),
		placeholder = $search.attr('placeholder');
	$search.on('focus', function() {
		if(this.value === placeholder) {
			this.value = '';
		}
	}).on('blur', function() {
		if(this.value === '') {
			this.value = placeholder;
		}
	}).trigger('blur');
}

/**
 * Image slider
 */ 
var Slider = {

	container : $('#slider'),
	slides : $('#slider').find('.slide'),
	timeout : null,
	trans : {delay : 4500, speed : 1000},

	init : function () {	
		var $first;	
	
		if (this.slides.length > 0) {
			$first = this.slides.first().addClass('current');	
			this.slides.not(':first-child').remove();			
		
			$first.one('load', function () {				
				Slider.transition($first, Slider.getNextIdx($first));	
			});	
			if (Slider.isImgReady($first)) {
				$first.trigger('load');			
			}			

			this.initJumpLinks();
			this.initSwipe();			
		}
	},
	
	initJumpLinks : function () {
		var	$idx,
			$idxLinks,
			$idxContainer,
			click = function () {
				var $link = $(this),
					$current = Slider.slides.filter('.current'),
					idx = $link.data('idx');
			
				if ( idx !== Slider.slides.index( $current ) ) {			
					$idxLinks.off('click');				
					Slider.stop();
					Slider.transition($current, idx, 0);
					
					setTimeout(function () {
						$idxLinks.on('click', click);
					}, Slider.trans.speed + 100);
				}
			};
	
		$idx = $('<i class="fa fa-circle-o">');
		$idxContainer = $('<div class="idx-cont">').appendTo(this.slides.find( ".overlay" ));
		for (var i = 0, l = this.slides.length; i < l; i++) {
			$idxContainer.append($idx.clone().data('idx', i));
		}
		$idxLinks = $idxContainer.find( 'i' );
		$idxLinks.first().removeClass('fa-circle-o').addClass('fa-circle').addClass('active');
		$idxLinks.on( 'click', click );			
	},
	
	initArrowLinks : function () {
	
		$( '<i class="fa fa-chevron-right next">' ).appendTo( this.container );
		$( '<i class="fa fa-chevron-left prev">' ).appendTo( this.container );
		this.container.on( "click", ".next, .prev", function() {
			var $current = Slider.slides.filter('.current'),
				idx = $( this ).hasClass( "prev" ) ? Slider.getPrevIdx( $current ) :  Slider.getNextIdx( $current );
			Slider.stop();
			Slider.transition( $current, idx, 0 );
		});
	},
	
	initSwipe : function() {
		Slider.container.on('swipeleft swiperight', function(e) {
			var $current = Slider.slides.filter('.current'),
				idx = e.type === 'swipeleft' ? Slider.getNextIdx($current) : Slider.getPrevIdx($current);
		
			Slider.stop();
			Slider.transition($current, idx, 0);
		});
	},
	
	transition : function ($current, nextIdx, delay) {
		var $next = Slider.slides.eq(nextIdx),
			$idxLinks,
			isManual = typeof delay !== 'undefined';
			
		if (Slider.container.find($next).length === 0) {
			Slider.container.append($next);
		}
		
		if(Slider.isImgReady($next)) {				
			Slider.timeout = setTimeout(function () {
			
				// Fade in the next image and hide the current one
				$next.addClass('next').css({opacity: 0, display: 'block'});
				$next.stop().fadeTo(Slider.trans.speed, 1, function () {
					$current.css({display: 'none'}).removeClass('current');
					$next.removeClass('next').addClass('current');
				});
				
				// Mark the active jump link
				Slider.container.find( '.idx-cont i' ).removeClass('fa-circle').removeClass('active').addClass('fa-circle-o');
				$next.find( '.idx-cont i' ).eq( nextIdx ).removeClass('fa-circle-o').addClass('fa-circle').addClass('active');
				
				// Only auto transition if the user hasn't manually change the photo
				if (!isManual) {
					Slider.transition($next, Slider.getNextIdx($next));
				}
			}, (isManual ? delay : Slider.trans.delay));
		
		} else {			
			$next.one('load', function () {	
				Slider.transition($current, nextIdx, delay);	
			});				
		}
	},
	
	getNextIdx : function ($slide) {
		var idx = Slider.slides.index($slide);
		return (idx + 1 === Slider.slides.length ? 0 : idx + 1);
	},
	
	getPrevIdx : function ($slide) {
		var idx = Slider.slides.index($slide);
		return (idx - 1 < 0 ? Slider.slides.length -1 : idx - 1);
	},	
	
	isImgReady : function ($slide) {		
		return $slide.height() !== 0 || $slide[0].complete;
	},	
	
	stop : function () {
		clearTimeout(Slider.timeout);
	}
};
Slider.init();


/*
 * Expandable menu items
 */
$( ".expandable > li > a" ).on( "click", function(){
	var $link = $( this ),
		$item = $link.parent( "li" ),
		$arrow = $link.children( ".fa" ),
		isOpen = $item.hasClass( "open" );
		
	if( isOpen ) {
		$arrow.removeClass( "fa-down" ).addClass( "fa-right" );
	} else {
		$arrow.removeClass( "fa-right" ).addClass( "fa-down" );
	}
	$item.toggleClass( "open" );
	return false;
}); 
 

/*
 * Mobile menu
 */
var $mobileMenu, $mobilePanel, $linkLogin, $linkLogout, $linkProfile, $linkHome,
	$header = $( 'header' ),
	$menu = $header.find( '.jetmenu' );
if ( $menu ) {
	$mobilePanel = $( '<div class="menu-panel">' );
	$linkHome = $menu.find( '.home' );
	$linkLogin = $header.find( '.login' );
	$linkProfile = $header.find( '.profile' );
	
	// Create a copy of the menu and convert to the mobile menu
	$mobileMenu = $menu.clone().removeClass().addClass( "jetmenu" );
	$mobileMenu
		.addClass( 'menu-mobile' )
		.removeClass( 'jetmenu' );
	
	// Add account links
	if ( $linkLogin.length ) {
		$linkHome.after( $linkLogin.clone().wrap( "<li class='login'>" ).parent() );
	}
	if ( $linkProfile.length ) {
		$linkHome.after( $linkProfile.clone().text( "Profile" ).wrap( "<li class='profile'>" ).parent() );
	}
	
	// Add the menu opening link
	$linkHome.after( '<li class="open"><a href="#">Menu <i class="fa fa-bars"></i></a></li>' );
	
	// Remove all megamenu and heading markup from the menu.  Only want the lists of links
	$mobileMenu.find( '.megamenu' ).replaceWith( function() {
		return $( 'ul', this );
	});
	
	// Check each menu <li> to find the accordion triggers
	$mobileMenu.find( 'li' ).each( function(){
		var x, text, $mainlist,
			$li = $( this ),
			$link = $li.children( 'a' ),
			$sublists = $li.children( 'ul' ),
			$spacer = $( '<i class="fa fa-fw">' ),
			chevron = '<i class="fa fa-fw fa-chevron-right"></i>';
			
		
		// Found a list with a link and sub-items: convert the link to a trigger
		if ( $link.length ) {
			if ( $sublists.length ) {
				$link = $link.detach();			
			
				// Convert the link to an accordion trigger
				text = $link.text();
				$li.prepend( '<span class="trigger">' + chevron + text + '</span>' );			
				
				// Convert all nested sublists into a single list
				$mainlist = $( '<ul>' );
				$sublists.each( function(){
					var $sublist = $( this ),
						$title = $sublist.find( '.title' );
						
					// If this is a megamenu, detach it and add to the accordion menu.
					// Dropdowns don't need this because they already have the correct markup.
					if ( $title.length > 0 ) {
						$sublist = $sublist.detach(),
						$title = $title.detach();
						
						$title.children().replaceWith( function() {
							return $( '<span class="trigger">' + chevron + $( this ).text() + '</span>' );
						});
						$title.append( $sublist );
						$mainlist.append( $title );
					
					}
				});
				
				// Add the converted sublist and main level link
				$link.text( text + ' - More' );
				$link.prepend( $spacer );
				$mainlist.append( $link.wrap( '<li>' ) );
				$li.append( $mainlist );
				
			} else {
				$link.prepend( $spacer );
				$li.append( $link );
			}	
			
		}
	});
	
	// Add the close link
	$mobileMenu.first().prepend( '<li class="close"><i class="fa fa-cancel">' );
	
	// Bind mobile menu events
	// Expand/contract of menu accordions
	$mobileMenu.find( '.trigger' ).on( 'click touchstart', function( e ) {
		var $trigger = $( this ),
			$li = $trigger.parent(),
			$closers = $mobilePanel.find( 'li' ).not( $trigger.parents() );
			
		e.preventDefault();
		e.stopPropagation();
		
		// Close everything that's not a parent of the clicked menu item
		$closers.removeClass( 'open' );
		$closers.find( '.fa-chevron-down' ).removeClass( 'fa-chevron-down' );
		
		$li.toggleClass( 'open' );
		if ( $li.hasClass( 'open' ) ) {
			$li.find( '.fa-chevron-right' ).first().addClass( 'fa-chevron-down' );
		} else {
			$li.find( '.fa-chevron-down' ).first().removeClass( 'fa-chevron-down' );
		}

	});
	
	// Toggle the menu panel
	$menu.find( '.open a' ).on( 'click touchstart', function( e ) {
		e.preventDefault();
		e.stopPropagation();

		if ( $mobilePanel.is( ':visible' ) ) {		
			$mobilePanel.hide();
		} else {
			$mobilePanel.show();		
		}
	});
	
	// Close the menu panel
	$mobileMenu.find( '.close' ).on( 'click MSPointerDown touchstart', function() {
		$mobilePanel.hide();
	});
	$( 'body' ).on( 'click MSPointerDown touchstart', function( e ) {		
		if ( $mobilePanel.is( ':visible' ) ) {		
			if ( !$( e.target ).parents( '.menu-panel' ).length ) {
				$mobilePanel.hide();
			}
		}
	});
	
	// Add the mobile menu to the <header>
	$mobilePanel.append( $mobileMenu );
	$header.append( $mobilePanel );
}


/*
 * NAME: Jet Responsive Megamenu 
 * AUTHOR PAGE: http://codecanyon.net/user/marcoarib
 * ITEM PAGE: http://codecanyon.net/item/jet-responsive-megamenu/5719593
*/

$.fn.jetmenu = function(options){
	var settings = {
		 indicator	 		:true     			// indicator that indicates a submenu
		,speed	 			:150     			// submenu speed
		,delay				:150				// submenu show delay
		,hideClickOut		:true     			// hide submenus when click outside menu
		,align				:"left"				// menu alignment (left/right)
		,submenuTrigger		:"hover"			// defines if submenu appears after hover/click
	}
	$.extend( settings, options );
	
	var menu = $(".jetmenu");
		
	if(settings.indicator == true){
		$(menu).find("ul a").each(function(){
			if($(this).siblings(".dropdown, .megamenu").length > 0){
				$(this).append("<span class='indicator'><i class='fa fa-chevron-right'></i></span>");
			}
		});
	}
	
	menu.find( ".dropdown" ).each(function(){
		$( this ).parent( "li" ).addClass( "relative" );
	});
		
	start();
	
	$(window).resize(function() {
		if(settings.align == "right"){
			fixSubmenuRight();
		}
		else{
			fixSubmenuLeft();
		}
	});
	
	function start(){
		bindHover();
		if(settings.align == "right"){
			rightAlignMenu();
		}
		else{
			fixSubmenuLeft();
		}
	}
	
	function bindHover(){
		if (navigator.userAgent.match(/Mobi/i) || window.navigator.msMaxtouchPoints > 0 || settings.submenuTrigger == "click"){						
			$(menu).find("a").on("click MSPointerDown touchstart", function(e){
				e.stopPropagation(); 
				e.preventDefault();
				$(this).parent("li").siblings("li").find(".dropdown, .megamenu").stop(true, true).fadeOut(settings.speed);
				if($(this).siblings(".dropdown, .megamenu").css("display") == "none"){
					$(this).siblings(".dropdown, .megamenu").stop(true, true).delay(settings.delay).fadeIn(settings.speed);
					return false; 
				}
				else{
					$(this).siblings(".dropdown, .megamenu").stop(true, true).fadeOut(settings.speed);
					$(this).siblings(".dropdown").find(".dropdown").stop(true, true).fadeOut(settings.speed);
				}
				window.location.href = $(this).attr("href");
			});
			
			$(menu).find("li").bind("mouseleave", function(){
				$(this).children(".dropdown, .megamenu").stop(true, true).fadeOut(settings.speed);
			});
			
			if(settings.hideClickOut == true){
				$(document).bind("click.menu MSPointerDown touchstart.menu", function(ev){
					if($(ev.target).closest(menu).length == 0){
						$(menu).find(".dropdown, .megamenu").fadeOut(settings.speed);
					}
				});
			}
		}
		else{
			$(menu).find("li").bind("mouseenter", function(){
				$(this).children(".dropdown, .megamenu").stop(true, true).delay(settings.delay).fadeIn(settings.speed);
				$(this).find(".arrow-up").stop(true, true).delay(settings.delay).fadeIn(settings.speed);
				
			}).bind("mouseleave", function(){
				$(this).children(".dropdown, .megamenu").stop(true, true).fadeOut(settings.speed);
				$(this).find(".arrow-up").stop(true, true).fadeOut(settings.speed);
			});
		}
	}
	
	function rightAlignMenu(){
		$(menu).children("li").addClass("jsright");
		var items = $(menu).children("li");
		$(menu).children("li:not(.showhide)").detach();
		for(var i = items.length; i >= 1; i--){
			$(menu).append(items[i]);
		}			
		fixSubmenuRight();
	}
	
	function fixSubmenuRight(){
		$(menu).children("li").removeClass("last");
		var items = $(menu).children("li");
		for(var i = 1; i <= items.length; i++){
			if($(items[i]).children(".dropdown, .megamenu").length > 0){
				var lastItemsWidth = 0;
				for(var y = 1; y <= i; y++){
					lastItemsWidth = lastItemsWidth + $(items[y]).outerWidth();
				}
				if($(items[i]).children(".dropdown, .megamenu").outerWidth() > lastItemsWidth){
					$(items[i]).addClass("last");
				}
			}
		}
	}
	
	function fixSubmenuLeft(){
		$(menu).children("li").removeClass("fix-sub");
		var items = $(menu).children("li");
		var menuWidth = $(menu).outerWidth();
		var itemsWidth = 0;
		var item;
		var childMenus;
		for(var i = 1; i <= items.length; i++){
			item = $(items[i]);
			
			childMenus = item.children(".dropdown, .megamenu");
			childMenus.css( "width", "auto" );
			
			if(childMenus.length > 0){
				itemsWidth = item.width() - parseInt( childMenus.css( "marginLeft" ), 10 );
				
				if(item.position().left + childMenus.outerWidth() > menuWidth){
					item.addClass("fix-sub");
					
				} else if ( itemsWidth > childMenus.width() ) {
					childMenus.css( "width", itemsWidth );
					childMenus.find( ".dropdown" ).each( function(){
						$(this).css( "width", itemsWidth );
					});
				}
			}
		}
	}
}
// Initialize the menu when the doc is ready
$( ".jetmenu" ).jetmenu(); 

}(jQuery));