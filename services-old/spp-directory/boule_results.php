<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_sigma_remote = new KT_connection($sigma_remote, $database_sigma_remote);

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listrsArchons3 = new TFI_TableFilter($conn_sigma_remote, "tfi_listrsArchons3");
$tfi_listrsArchons3->addColumn("FIRSTNAME", "STRING_TYPE", "FIRSTNAME", "%");
$tfi_listrsArchons3->addColumn("LASTNAME", "STRING_TYPE", "LASTNAME", "%");
$tfi_listrsArchons3->addColumn("BOULENAME", "STRING_TYPE", "BOULENAME", "%");
$tfi_listrsArchons3->addColumn("CITY", "STRING_TYPE", "CITY", "%");
$tfi_listrsArchons3->addColumn("STATECD", "STRING_TYPE", "STATECD", "%");
$tfi_listrsArchons3->addColumn("EMAIL", "STRING_TYPE", "EMAIL", "%");
$tfi_listrsArchons3->addColumn("HOMEPHONE", "STRING_TYPE", "HOMEPHONE", "%");
$tfi_listrsArchons3->addColumn("CPOSITION", "STRING_TYPE", "CPOSITION", "%");
$tfi_listrsArchons3->addColumn("WEB_ID", "NUMERIC_TYPE", "WEB_ID", "=");
$tfi_listrsArchons3->Execute();

// Sorter
$tso_listrsArchons3 = new TSO_TableSorter("rsArchons", "tso_listrsArchons3");
$tso_listrsArchons3->addColumn("FIRSTNAME");
$tso_listrsArchons3->addColumn("LASTNAME");
$tso_listrsArchons3->addColumn("BOULENAME");
$tso_listrsArchons3->addColumn("CITY");
$tso_listrsArchons3->addColumn("STATECD");
$tso_listrsArchons3->addColumn("EMAIL");
$tso_listrsArchons3->addColumn("HOMEPHONE");
$tso_listrsArchons3->addColumn("CPOSITION");
$tso_listrsArchons3->addColumn("WEB_ID");
$tso_listrsArchons3->setDefault("LASTNAME");
$tso_listrsArchons3->Execute();

// Navigation
$nav_listrsArchons3 = new NAV_Regular("nav_listrsArchons3", "rsArchons", "../", $_SERVER['PHP_SELF'], 25);

//NeXTenesio3 Special List Recordset
$maxRows_rsArchons = $_SESSION['max_rows_nav_listrsArchons3'];
$pageNum_rsArchons = 0;
if (isset($_GET['pageNum_rsArchons'])) {
  $pageNum_rsArchons = $_GET['pageNum_rsArchons'];
}
$startRow_rsArchons = $pageNum_rsArchons * $maxRows_rsArchons;

$colname_rsArchons = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname_rsArchons = $_GET['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter_rsArchons = "1=1";
if (isset($_SESSION['filter_tfi_listrsArchons3'])) {
  $NXTFilter_rsArchons = $_SESSION['filter_tfi_listrsArchons3'];
}
// Defining List Recordset variable
$NXTSort_rsArchons = "LASTNAME";
if (isset($_SESSION['sorter_tso_listrsArchons3'])) {
  $NXTSort_rsArchons = $_SESSION['sorter_tso_listrsArchons3'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_rsArchons = sprintf("SELECT * FROM vw_elig WHERE BOULENAME = %s AND  {$NXTFilter_rsArchons} ORDER BY {$NXTSort_rsArchons} ", GetSQLValueString($colname_rsArchons, "text"));
$query_limit_rsArchons = sprintf("%s LIMIT %d, %d", $query_rsArchons, $startRow_rsArchons, $maxRows_rsArchons);
$rsArchons = mysql_query($query_limit_rsArchons, $sigma_modx) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);

if (isset($_GET['totalRows_rsArchons'])) {
  $totalRows_rsArchons = $_GET['totalRows_rsArchons'];
} else {
  $all_rsArchons = mysql_query($query_rsArchons);
  $totalRows_rsArchons = mysql_num_rows($all_rsArchons);
}
$totalPages_rsArchons = ceil($totalRows_rsArchons/$maxRows_rsArchons)-1;
//End NeXTenesio3 Special List Recordset//NeXTenesio3 Special List Recordset
$maxRows_rsArchons = $_SESSION['max_rows_nav_listrsArchons3'];
$pageNum_rsArchons = 0;
if (isset($_GET['pageNum_rsArchons'])) {
  $pageNum_rsArchons = $_GET['pageNum_rsArchons'];
}
$startRow_rsArchons = $pageNum_rsArchons * $maxRows_rsArchons;

$colname_rsArchons = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname_rsArchons = $_GET['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter_rsArchons = "1=1";
if (isset($_SESSION['filter_tfi_listrsArchons3'])) {
  $NXTFilter_rsArchons = $_SESSION['filter_tfi_listrsArchons3'];
}
// Defining List Recordset variable
$NXTSort_rsArchons = "LASTNAME";
if (isset($_SESSION['sorter_tso_listrsArchons3'])) {
  $NXTSort_rsArchons = $_SESSION['sorter_tso_listrsArchons3'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);

$query_rsArchons = sprintf("SELECT * FROM vw_elig WHERE BOULENAME = %s AND  {$NXTFilter_rsArchons} ORDER BY {$NXTSort_rsArchons} ", GetSQLValueString($colname_rsArchons, "text"));
$query_limit_rsArchons = sprintf("%s LIMIT %d, %d", $query_rsArchons, $startRow_rsArchons, $maxRows_rsArchons);
$rsArchons = mysql_query($query_limit_rsArchons, $sigma_modx) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);

if (isset($_GET['totalRows_rsArchons'])) {
  $totalRows_rsArchons = $_GET['totalRows_rsArchons'];
} else {
  $all_rsArchons = mysql_query($query_rsArchons);
  $totalRows_rsArchons = mysql_num_rows($all_rsArchons);
}
$totalPages_rsArchons = ceil($totalRows_rsArchons/$maxRows_rsArchons)-1;
//End NeXTenesio3 Special List Recordset

$colname_rsOfficers = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname_rsOfficers = $_GET['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOfficers = sprintf("SELECT * FROM vw_boule_officers WHERE BOULENAME = %s  AND COMMITTEESTATUSSTT = 'Active' ORDER BY CPOSITIONTYPE DESC,CPOSITIONRANK ASC", GetSQLValueString($colname_rsOfficers, "text"));
$rsOfficers = mysql_query($query_rsOfficers, $sigma_modx) or die(mysql_error());
$row_rsOfficers = mysql_fetch_assoc($rsOfficers);
$totalRows_rsOfficers = mysql_num_rows($rsOfficers);

$nav_listrsArchons3->checkBoundries();

// Initialize the Alternate Color counter
$ac_sw1 = 0;
?><!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --><!DOCTYPE html>
<!--
 ____________________________________________________________
|                                                            |
|      DATE + 2006.11.14                                     |
| COPYRIGHT + Sigma Pi Phi 2006.  All Rights Reserved.       |
|____________________________________________________________|

-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-meta.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-css.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-javascript.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-links.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_FIRSTNAME {width:70px; overflow:hidden;}
  .KT_col_LASTNAME {width:70px; overflow:hidden;}
  .KT_col_BOULENAME {width:105px; overflow:hidden;}
  /*.KT_col_CITY {width:56px; overflow:hidden;}*/
  .KT_col_STATECD {width:28px; overflow:hidden;}
  .KT_col_EMAIL {width:155px;; overflow:hidden;}
  .KT_col_HOMEPHONE {width:100px; overflow:hidden;}
  .KT_col_JOBTITLE {width:105px; overflow:hidden;}
  .KT_col_CPOSITION {width:70px; overflow:hidden;}
  .KT_col_WEB_ID {width:35px; overflow:hidden;}
</style><!-- InstanceEndEditable -->
</head>

<body class="home blue-bar  color-override-yellow-orange">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/main-nav.php"); ?>
			<div class="width">
			    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/global-logo-tagline.php"); ?>
			</div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
	<!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- InstanceBeginEditable name="pagetitle" -->
                            <section class="span-12 margin-bottom-medium">
                            <div class="inner">
                                     <h1>Boul&#233; Search Results</h1>
                                     <ol class="breadcrumbs" style="margin-top:-18px;">
                                      <li><a class="B_homeCrumb" href="/home/archon-home.php" title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                                      <li><span class="B_currentCrumb">Administration</span></li>
                                    </ol>
                     <div>
             </section>
<!-- InstanceEndEditable -->
             
             <!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listrsArchons3">
 
  <div class="KT_tnglist ">
  
    <h3 class="margin-top-large"><?php echo $_GET['BOULENAME']; ?> Boul&eacute; Officers </h3>
     <?php do { ?>
             <ul><li> <strong><?php echo $row_rsOfficers['CPOSITION']; ?></strong>,<a href="/services/spp-directory/search-results.php?id=<?php echo $row_rsOfficers['WEB_ID']; ?>" target="_blank"><?php echo $row_rsOfficers['L2R_FULLNAME2']; ?></a>, <a href="mailto:<?php echo $row_rsOfficers['EMAIL']; ?>"><?php echo $row_rsOfficers['EMAIL']; ?></a>, <a href="tel:<?php echo $row_rsOfficers['HOMEPHONE']; ?>"><?php echo $row_rsOfficers['HOMEPHONE']; ?></a></li></ul>
             <?php } while ($row_rsOfficers = mysql_fetch_assoc($rsOfficers)); ?>
<p>&nbsp;</p>
  
  
      <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">

        
    <h3 align="left" class="margin-top-large">All <?php echo $_GET['BOULENAME']; ?> Boul&eacute; Members </h3>
              <div align="left">Sort By: <a href="<?php echo $tso_listrsArchons3->getSortLink('FIRSTNAME'); ?>">FIRST NAME</a>  ,<a href="<?php echo $tso_listrsArchons3->getSortLink('LASTNAME'); ?>">LAST NAME</a> , <a href="<?php echo $tso_listrsArchons3->getSortLink('BOULENAME'); ?>">BOUL&Eacute;</a>,  <a href="<?php echo $tso_listrsArchons3->getSortLink('CITY'); ?>">CITY</a>,  <a href="<?php echo $tso_listrsArchons3->getSortLink('STATECD'); ?>">STATE</a>,  <a href="<?php echo $tso_listrsArchons3->getSortLink('EMAIL'); ?>">EMAIL</a>,  <a href="<?php echo $tso_listrsArchons3->getSortLink('HOMEPHONE'); ?>">HOME PHONE</a>,  <a href="<?php echo $tso_listrsArchons3->getSortLink('WEB_ID'); ?>">PROFILE</a>              </div>
              <p align="left">  DISPLAYING  
    <?php
  $nav_listrsArchons3->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?></p>
              
<div class="KT_options"> <a href="<?php echo $nav_listrsArchons3->getShowAllLink(); ?>">
  <div align="left"><?php echo NXT_getResource("Show"); ?>
    <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listrsArchons3'] == 1) {
?>
      <a href="<?php echo $nav_listrsArchons3->getShowAllLink(); ?>"><?php echo $_SESSION['default_max_rows_nav_listrsArchons3']; ?></a>
        <?php 
  // else Conditional region1
  } else { ?>
        <?php echo NXT_getResource("all"); ?>
        <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
    &nbsp;
    <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listrsArchons3'] == 1) {
?>
      <a href="<?php echo $tfi_listrsArchons3->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
      <?php 
  // else Conditional region2
  } else { ?>
      <a href="<?php echo $tfi_listrsArchons3->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
      <?php } 
  // endif Conditional region2
?>
  </div>
</div>                             
             
        
        <table cellpadding="2" cellspacing="0" >
          <thead>
            <tr class="KT_row_order">
              <th colspan="8" class="KT_sorter KT_col_FIRSTNAME <?php echo $tso_listrsArchons3->getSortIcon('FIRSTNAME'); ?>" id="FIRSTNAME"> 
             </th>
            </tr>
            <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listrsArchons3'] == 1) {
?>
              <tr class="KT_row_filter">
                <td colspan="2"><input type="text" name="tfi_listrsArchons3_FIRSTNAME" id="tfi_listrsArchons3_FIRSTNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_FIRSTNAME']); ?>" size="10" maxlength="20" />                  <input type="text" name="tfi_listrsArchons3_LASTNAME" id="tfi_listrsArchons3_LASTNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_LASTNAME']); ?>" size="10" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_BOULENAME" id="tfi_listrsArchons3_BOULENAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_BOULENAME']); ?>" size="10" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_CITY" id="tfi_listrsArchons3_CITY" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_CITY']); ?>" size="10" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_STATECD" id="tfi_listrsArchons3_STATECD" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_STATECD']); ?>" size="2" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_EMAIL" id="tfi_listrsArchons3_EMAIL" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_EMAIL']); ?>" size="15" maxlength="20" /></td>
                <td><input type="text" name="tfi_listrsArchons3_HOMEPHONE" id="tfi_listrsArchons3_HOMEPHONE" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons3_HOMEPHONE']); ?>" size="15" maxlength="20" /></td>
                 <td><input type="submit" name="tfi_listrsArchons3" value="<?php echo NXT_getResource("Filter"); ?>" />&nbsp;</td>
              </tr>
              <?php } 
  // endif Conditional region3
?>
          </thead>
          <tbody>
            <?php if ($totalRows_rsArchons == 0) { // Show if recordset empty ?>
              <tr>
                <td colspan="8"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
              </tr>
              <?php } // Show if recordset empty ?>
            <?php if ($totalRows_rsArchons > 0) { // Show if recordset not empty ?>
              <?php do { ?>
                <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                  <td style="white-space:normal;"><div class="KT_col_FIRSTNAME"><?php echo KT_FormatForList($row_rsArchons['FIRSTNAME'], 15); ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_LASTNAME"><?php echo KT_FormatForList($row_rsArchons['LASTNAME'], 15); ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_BOULENAME"><?php echo KT_FormatForList($row_rsArchons['BOULENAME'], 25); ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_CITY"><?php echo $row_rsArchons['CITY']; ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_STATECD"><?php echo $row_rsArchons['STATECD']; ?></div></td>
                  <td style="white-space:normal;"><div class="KT_col_EMAIL"><a href="mailto:<?php echo $row_rsArchons['EMAIL']; ?>"><?php echo $row_rsArchons['EMAIL']; ?></a></div></td>
                  <td style="white-space:normal;"><div class="KT_col_HOMEPHONE"><a href="tel:<?php echo KT_FormatForList($row_rsArchons['HOMEPHONE'], 20); ?>"><?php echo KT_FormatForList($row_rsArchons['HOMEPHONE'], 20); ?></a></div></td>
                   <td style="white-space:normal;"><div class="KT_col_WEB_ID"><a href="/services/directory/results.php?id=<?php echo $row_rsArchons['WEB_ID']; ?>" target="_blank">VIEW</a></div></td>
                <?php } while ($row_rsArchons = mysql_fetch_assoc($rsArchons)); ?>
              <?php } // Show if recordset not empty ?>
          </tbody>
        </table>
        <div class="KT_bottomnav margin-bottom-large">
        <div>
            <?php
            $nav_listrsArchons3->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </form>
    </div>

<!-- InstanceEndEditable -->
            <!-- main content body end -->	
	
	<!-- third section event and articles start -->
  	 <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
	<!-- third section event and articles end -->
	
    <footer class="width padding" style="border-top: 10px #52B609 solid;">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-content-default.php"); ?>
      </div>
    </footer>
	
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-scripts.php"); ?>
</body><!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchons);

mysql_free_result($rsOfficers);
?>
