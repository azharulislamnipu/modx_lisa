<?php

/*
 * Renders the finalized Boule delegation selection PDF to the browser
 * @author Pat Heard {http://fullahead.org}
 */

require_once '../BouleDelegates.php'; 

$isSuccess = false;
if(isset($_POST['boule_delegation'])){

    $delegates = unserialize(urldecode($_POST['boule_delegation']));
    if($delegates instanceof BouleDelegates && $delegates->isFinalized()){
        $delegates->renderPdf('51st-Grande-Boule-Delegation-Selections.pdf');
        $isSuccess = true;
    } 
}

if(!$isSuccess){
    header('Location: /home/delegates_dashboard.php?msg-css=error&msg=' . urlencode('Failed to print the Boul&eacute; delegation form. Please try again.'));
}

?>