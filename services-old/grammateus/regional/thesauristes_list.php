<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsThesauristesByRegion = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_rsThesauristesByRegion = $_COOKIE['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsThesauristesByRegion = sprintf("SELECT * FROM vw_boule_officers WHERE REGIONNAME = %s AND COMMITTEESTATUSSTT = 'Active' AND    (vw_boule_officers.CPOSITION LIKE '%%thesau%%') ORDER BY BOULENAME ASC", GetSQLValueString($colname_rsThesauristesByRegion, "text"));
$rsThesauristesByRegion = mysql_query($query_rsThesauristesByRegion, $sigma_modx) or die(mysql_error());
$row_rsThesauristesByRegion = mysql_fetch_assoc($rsThesauristesByRegion);
$totalRows_rsThesauristesByRegion = mysql_num_rows($rsThesauristesByRegion);

$colname_result = "-1";
if (isset($_COOKIE['GRAMMID'])) {
  $colname_result = $_COOKIE['GRAMMID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_result = sprintf("SELECT REGIONNAME, BOULENAME, CHAPTERID FROM vw_web_elig_no_officers WHERE CUSTOMERID = %s", GetSQLValueString($colname_result, "int"));
$result = mysql_query($query_result, $sigma_modx) or die(mysql_error());
$row_result = mysql_fetch_assoc($result);
$totalRows_result = mysql_num_rows($result);

$colname_exThes = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_exThes = $_COOKIE['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_exThes = sprintf("SELECT vw_boule_officers.CPOSITION AS OFFICE,   vw_boule_officers.BOULENAME,   vw_boule_officers.PREFIX,   vw_boule_officers.FIRSTNAME,   vw_boule_officers.MIDDLEINITIAL,   vw_boule_officers.LASTNAME,   vw_boule_officers.SUFFIX AS SUFFIX1,   vw_boule_officers.DESIGNATIONLST AS SUFFIX,   vw_boule_officers.ADDRESS1,   vw_boule_officers.CITY,   vw_boule_officers.STATECD AS STATE,   vw_boule_officers.ZIP,   vw_boule_officers.WEB_EMAIL AS EMAIL,   vw_boule_officers.HOMEPHONE AS HOME_PHONE,   vw_boule_officers.ALTEMAIL AS WORK_EMAIL,   vw_boule_officers.WORKPHONE AS WORK_PHONE FROM vw_boule_officers WHERE REGIONNAME = %s AND COMMITTEESTATUSSTT = 'Active' AND    (vw_boule_officers.CPOSITION LIKE 'Thes%%') ORDER BY BOULENAME ASC", GetSQLValueString($colname_exThes, "text"));
$exThes = mysql_query($query_exThes, $sigma_modx) or die(mysql_error());
$row_exThes = mysql_fetch_assoc($exThes);
$totalRows_exThes = mysql_num_rows($exThes);

//Export to Excel Server Behavior
if (isset($_POST['thesauristes_export'])&&($_POST['thesauristes_export']=="thesauristes")){
	$delim="";
	$delim_replace="";
	if($delim==""){
		$lang=(strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],",")===false)?$_SERVER['HTTP_ACCEPT_LANGUAGE']:substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],","));
		$semi_array=array("af","zh-hk","zh-mo","zh-cn","zh-sg","zh-tw","fr-ch","de-li","de-ch","it-ch","ja","ko","es-do","es-sv","es-gt","es-hn","es-mx","es-ni","es-pa","es-pe","es-pr","sw");
		$delim=(in_array($lang,$semi_array) || substr_count($lang,"en")>0)?",":";";
	}
	$output="";
	$include_hdr="1";
	if($include_hdr=="1"){
		$totalColumns_exThes=mysql_num_fields($exThes);
		for ($x=0; $x<$totalColumns_exThes; $x++) {
			if($x==$totalColumns_exThes-1){$comma="";}else{$comma=$delim;}
			$output = $output.(ereg_replace("_", " ",mysql_field_name($exThes, $x))).$comma;
		}
		$output = $output."\r\n";
	}

	do{$fixcomma=array();
    		foreach($row_exThes as $r){array_push($fixcomma,ereg_replace($delim,$delim_replace,$r));}
		$line = join($delim,$fixcomma);
    		$line=ereg_replace("\r\n", " ",$line);
    		$line = "$line\n";
    		$output=$output.$line;}while($row_exThes = mysql_fetch_assoc($exThes));
	header("Content-Type: application/xls");
	header("Content-Disposition: attachment; filename=Thesauristes.csv");
	header("Content-Type: application/force-download");
	header("Cache-Control: post-check=0, pre-check=0", false);
	echo $output;
	die();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_regional_grammateus.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow"><?php echo $_COOKIE['REGIONNAME']; ?> Region Thesauristes</h1>
<div class="contentBlock">
<table width="100%" cellpadding="2">
<tr>
  <td><a href="/home/reg_gramm_mgmt.php">Back to Officer's Listing</a></td>
  <td><form id="thes" name="thes" method="post" action="">
    <input name="thesauristes_export" type="hidden" id="thesauristes" value="thesauristes" />
    <input name="export" type="submit" value="Click to Export Thesauristes Data" />
  </form></td>
  <td align="right">&nbsp;</td>
  <td colspan="2" align="right"><a href="add_officer.php?KT_back=1">Add New Officer</a></td>
  </tr>
  <tr>
    <td><strong>BOUL&Eacute;</strong></td>
    <td><strong>ARCHON</strong></td>
    <td><strong>OFFICE</strong></td>
    <td><strong>START DATE</strong></td>
    <td><strong>EDIT</strong></td>
  </tr>
  <?php do { ?>
    <tr valign="top">
      <td><?php echo $row_rsThesauristesByRegion['BOULENAME']; ?></td>
      <td>
<script language="JavaScript">
    function edit_<?php echo $row_rsThesauristesByRegion['CUSTOMERCD'];?>(){
        document.forms.form_<?php echo $row_rsThesauristesByRegion['CUSTOMERCD']; ?>.submit();
    }
</script>
  <form name="form_<?php echo $row_rsThesauristesByRegion['CUSTOMERCD']; ?>" action="/services/grammateus/regional/thesauristes_edit.php" method="get">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsThesauristesByRegion['CUSTOMERCD']); ?>">
  <input type="hidden" name="GRAMMID" value="<?php echo KT_escapeAttribute($_COOKIE['GRAMMID']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_COOKIE['UPDATEDBY']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_COOKIE['BOULENAME']); ?>">
  <input type="hidden" name="REGIONNAME" value="<?php echo KT_escapeAttribute($_COOKIE['REGIONNAME']); ?>">
  </form>
   
      
      <a href="#" rel="<?php echo $row_rsThesauristesByRegion['WEB_ID']; ?>" onclick="edit_<?php echo $row_rsThesauristesByRegion['CUSTOMERCD'];?>();return(FALSE);"><?php echo $row_rsThesauristesByRegion['FULLNAME']; ?></a>
          <div id="<?php echo $row_rsThesauristesByRegion['WEB_ID']; ?>" class="balloonstyle">
            <?php echo $row_rsThesauristesByRegion['ADDRESS1']; ?><br />
            <?php echo $row_rsThesauristesByRegion['CITY']; ?>, <?php echo $row_rsThesauristesByRegion['STATECD']; ?> <?php echo $row_rsThesauristesByRegion['ZIP']; ?><br />
            HOME PHONE: <?php echo $row_rsThesauristesByRegion['HOMEPHONE']; ?><br />
            EMAIL: <?php echo $row_rsThesauristesByRegion['EMAIL']; ?><br />
            BUSINESS PHONE: <?php echo $row_rsThesauristesByRegion['WORKPHONE']; ?><br />
            BUSINESS EMAIL: <?php echo $row_rsThesauristesByRegion['ALTEMAIL']; ?>    </div></td>
      <td><?php echo $row_rsThesauristesByRegion['CPOSITION']; ?></td>
      <td><?php echo $row_rsThesauristesByRegion['STARTDATE']; ?></td>
      <td><a href="change_date.php?OFFICERID=<?php echo $row_rsThesauristesByRegion['OFFICERID']; ?>&amp;CUSTOMERCD=<?php echo $row_rsThesauristesByRegion['CUSTOMERCD']; ?>&amp;KT_back=1">Change Start Date</a><br />
        <a href="remove_officer.php?OFFICERID=<?php echo $row_rsThesauristesByRegion['OFFICERID']; ?>&amp;CUSTOMERCD=<?php echo $row_rsThesauristesByRegion['CUSTOMERCD']; ?>&amp;KT_back=1">Remove from Office</a></td>
    </tr>
    <?php } while ($row_rsThesauristesByRegion = mysql_fetch_assoc($rsThesauristesByRegion)); ?>
<tr>
  <td><a href="/home/reg_gramm_mgmt.php">Back to Officer's Listing</a></td>
  <td align="right">&nbsp;</td>
  <td align="right">&nbsp;</td>
  <td colspan="2" align="right"><a href="add_officer.php?KT_back=1">Add New Officer</a></td>
  </tr>
</table>
</div>
<!-- InstanceEndEditable --></div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsThesauristesByRegion);

mysql_free_result($result);

mysql_free_result($exThes);
?>
