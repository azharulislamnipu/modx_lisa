a:4:{s:8:"feedinfo";a:3:{s:4:"type";s:3:"RSS";s:7:"version";s:3:"2.0";s:8:"encoding";s:5:"UTF-8";}s:4:"info";a:3:{s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:43:"http://www.sigmapiphi.org/feeds/out/?user=1";}}s:5:"title";s:40:"Items of Interest for Fraternity Members";s:11:"description";s:40:"Items of Interest for Fraternity Members";}s:5:"items";a:50:{i:0;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:41:"Rose: Finding out ACL torn was like death";s:11:"description";s:380:"CHICAGO (AP) &mdash; Derrick Rose says finding out he had a torn ACL was like death.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12857-rose-finding-out-acl-torn-was-like-death.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:90:"http://www.chicagodefender.com/article-12857-rose-finding-out-acl-torn-was-like-death.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/b990dab361e9e7eceb3a75a911748aa6";}s:7:"pubdate";i:1345222852;s:7:"dc:date";i:1345222852;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:1;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Ill. lawmaker could be expelled, still on ballot";s:11:"description";s:475:"SPRINGFIELD, Ill. (AP) &mdash; The Illinois House might expel one of its members Friday, but Rep. Derrick Smith remains on the November ballot and still could be re-elected.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12859-ill-lawmaker-could-be-expelled-still-on-ballot.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:96:"http://www.chicagodefender.com/article-12859-ill-lawmaker-could-be-expelled-still-on-ballot.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/55e4b82b0d6d3d3949547504c1c802a9";}s:7:"pubdate";i:1345222852;s:7:"dc:date";i:1345222852;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:2;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:60:"Patrick Kennedy: Rep. Jackson in &#039;deep&#039; depression";s:11:"description";s:548:"CHICAGO (AP) &mdash; U.S. Rep. Jesse Jackson Jr. is in a "deep" depression and has "a lot of work" ahead of him on the road to recovery, former Rhode Island U.S. Rep. Patrick Kennedy said Thursday after visiting the hospitalized Chicago Democrat.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12856-patrick-kennedy-rep-jackson-in-deep-depression.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:96:"http://www.chicagodefender.com/article-12856-patrick-kennedy-rep-jackson-in-deep-depression.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/0d496c935c0ba5852aef9d6af3bd73ff";}s:7:"pubdate";i:1345222852;s:7:"dc:date";i:1345222852;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:3;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:50:"Emanuel announces $275K for immigrant scholarships";s:11:"description";s:435:"CHICAGO (AP) &mdash; Chicago officials say they've raised $275,000 so far in private donations for an immigrant scholarship fund.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12858-emanuel-announces-$275k-for-immigrant-scholarships.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:100:"http://www.chicagodefender.com/article-12858-emanuel-announces-$275k-for-immigrant-scholarships.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/985536836ab54f1bee89f30bc9851c77";}s:7:"pubdate";i:1345222852;s:7:"dc:date";i:1345222852;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:4;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:62:"Interior Designer Builds Close Relationship with Local Artists";s:11:"description";s:607:"Gallerist and textile designer Maya-Camille Broussard doesn&rsquo;t believe the color scheme of a room should ever dictate a homeowner's decision to purchase a piece. &ldquo;Instead of the artwork being an accessory to the room, I convince them to let the art be the star,&rdquo; she said.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12854-interior-designer-builds-close-relationship-with-local-artists.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:112:"http://www.chicagodefender.com/article-12854-interior-designer-builds-close-relationship-with-local-artists.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/1e45c05c63714e28016ea5ca3684077f";}s:7:"pubdate";i:1345050005;s:7:"dc:date";i:1345050005;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:5;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:26:"MUNTU Celebrates 40 Years!";s:11:"description";s:745:"Muntu Dance Theatre of Chicago is celebrating 40 years of African/African-American dance, music and folklore.  Throughout the year, the company will be performing across the nation with spectacular performances.  On August 11 Muntu will hold two shows at The Center for Performing Arts at Governors State University in University Park.  Joan Gray, President of Muntu, spoke with the Defender about the evolution of Muntu and celebrating 40 years of African dance.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12855-muntu-celebrates-40-years_.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:76:"http://www.chicagodefender.com/article-12855-muntu-celebrates-40-years_.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/8aff5b671dc32a90c2b54bafbe50402a";}s:7:"pubdate";i:1345050005;s:7:"dc:date";i:1345050005;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:6;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:36:"President Obama responds to our call";s:11:"description";s:507:"As many were getting ready for the Bud Billiken Parade, President Barack Obama prepared a videotaped message wishing all parade goers a festive time and speaking out about the violence that&rsquo;s plagued the city.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12853-president-obama-responds-to-our-call.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:86:"http://www.chicagodefender.com/article-12853-president-obama-responds-to-our-call.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/c4b34d0a3bb0bf1b80983f5bb0ef0d14";}s:7:"pubdate";i:1345050005;s:7:"dc:date";i:1345050005;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:7;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:39:"Illinois Lottery warns of ongoing scams";s:11:"description";s:459:"SPRINGFIELD, Ill. (AP) &mdash; Illinois Lottery officials say residents of the state should be on guard against a number of ever-present scams related to lotteries.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12852-illinois-lottery-warns-of-ongoing-scams.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:89:"http://www.chicagodefender.com/article-12852-illinois-lottery-warns-of-ongoing-scams.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/f9f30e5b702a67403b28245b67c73070";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:8;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:53:"Illinois employers can&#039;t ask for Facebook logins";s:11:"description";s:446:"CHICAGO (AP) &mdash; Illinois is making it illegal for employers to ask job-seekers to hand over passwords to their social networking accounts.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12851-illinois-employers-cant-ask-for-facebook-logins.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.chicagodefender.com/article-12851-illinois-employers-cant-ask-for-facebook-logins.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/7447fb3c1fb613ee648ffa4668f219fa";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:9;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:66:"I&#039;m still waiting Mr. President on a response to our violence";s:11:"description";s:520:"In just about each local newscast there's a report about gun violence and its victims. And the headlines the next morning with a total of those shot and the fatalities the previous day gives me a headache.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12845-im-still-waiting-mr-president-on-a-response-to-our-violence.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:109:"http://www.chicagodefender.com/article-12845-im-still-waiting-mr-president-on-a-response-to-our-violence.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/d3be37fe98699d452a95c68f8f651bc9";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:10;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:42:"Natural Hair Show Helps Fund Austin Garden";s:11:"description";s:422:"Loving the kinks, curls or volume-less hair means embracing your birth-given locks and exhorting confidence wherever you go.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12843-natural-hair-show-helps-fund-austin-garden.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:92:"http://www.chicagodefender.com/article-12843-natural-hair-show-helps-fund-austin-garden.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/7ac51629308765067483916e60904f32";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:11;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:25:"Harvey throws first pitch";s:11:"description";s:561:"Comedian, syndicated radio personality and game show host Steve Harvey threw out the first pitch at Sunday&rsquo;s Cubs game against the St. Louis Cardinals. Harvey&rsquo;s new talk show, debuting this fall, will be taped in Chicago at NBC Tower on Columbus Drive.                
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12846-harvey-throws-first-pitch.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:75:"http://www.chicagodefender.com/article-12846-harvey-throws-first-pitch.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/a36cc8c314f5a0cf8ed018bafafd4427";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:12;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:47:"House heading toward election-year tax showdown";s:11:"description";s:468:"WASHINGTON (AP) &mdash; An election-year tax faceoff between Democrats and Republicans in the GOP-controlled House is heading toward a predictable outcome Wednesday.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12850-house-heading-toward-election-year-tax-showdown.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.chicagodefender.com/article-12850-house-heading-toward-election-year-tax-showdown.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/83ee23f9d12f9a637e65f78fed7c4943";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:13;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:43:"Excitement brews about food truck ordinance";s:11:"description";s:537:"South Side baked goods entrepreneur Randall Hunt can't wait to acquire a food truck to spread his array of treats throughout Chicago now that the City Council passed an ordinance allowing vendors to prepare cooked food on their vehicles. 
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12847-excitement-brews-about-food-truck-ordinance.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:93:"http://www.chicagodefender.com/article-12847-excitement-brews-about-food-truck-ordinance.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/e0775519ad340ea5da675367c5cf94d8";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:14;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:17:"Burroughs Harbor?";s:11:"description";s:666:"U.S. Reps. Bobby Rush (D-1st) and Danny Davis (D-7th) spearheads a petition to get the 31st Street Harbor renamed after the late Dr. Margaret Burroughs. Burroughs, co-founder of the DuSable Museum of African-American History, died in November 2010. She also started the South Side Community Art Center and the Lake Meadows Art Fair and was most recently the Chicago Park District Commissioner. 
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12848-burroughs-harbor.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:66:"http://www.chicagodefender.com/article-12848-burroughs-harbor.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/681a89fdd7f1ca0ead80d3621f6b6134";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:15;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:39:"Musical tribute to William H. Abernathy";s:11:"description";s:469:"Stanley Abernathy, trumpet and flugelhorn virtuoso and his sisters Sandra and Pamela, are trying to raise funds for the music dedication for their father William H. Abernathy.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12844-musical-tribute-to-william-h-abernathy.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:88:"http://www.chicagodefender.com/article-12844-musical-tribute-to-william-h-abernathy.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/f7a0ff2c681d4028a40d26fc88795b2c";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:16;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:64:"Beyond the locker room: Local boxer looks to knock out big names";s:11:"description";s:558:"Standing at 5 feet 10 inches and weighing 152 pounds, you'd never expect Alex Martin Jr. to be as dangerous in the ring as he is, but the hometown boxer is making a name for himself as a fighter to be respected inside and outside the ring.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12849-beyond-the-locker-room-local-boxer-looks-to-knock-out-big-names.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:113:"http://www.chicagodefender.com/article-12849-beyond-the-locker-room-local-boxer-looks-to-knock-out-big-names.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/de38101c7e28919b12b058b89f6e4d00";}s:7:"pubdate";i:1343845818;s:7:"dc:date";i:1343845818;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:17;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:46:"Police: Titans player dies in apparent suicide";s:11:"description";s:423:"TAMPA, Fla. (AP) &mdash; Tennessee Titans reserve receiver O.J. Murdock has died of an apparent suicide, Tampa police say.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12841-police-titans-player-dies-in-apparent-suicide.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:95:"http://www.chicagodefender.com/article-12841-police-titans-player-dies-in-apparent-suicide.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/2367f5b1ec7c92f5baec2431e61ab43e";}s:7:"pubdate";i:1343680221;s:7:"dc:date";i:1343680221;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:18;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:33:"Guards at 7 Ill. prisons searched";s:11:"description";s:697:"SPRINGFIELD, Ill. (AP) &mdash; Illinois authorities took the unusual step of searching guards and other prison employees for contraband as they left at least seven facilities last week, sparking worker allegations that the checks may have been reprisals for complaints about overcrowding and understaffing and inside information leaked to the news media, workers and union officials told The Associated Press.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12838-guards-at-7-ill-prisons-searched.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:82:"http://www.chicagodefender.com/article-12838-guards-at-7-ill-prisons-searched.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/39335b0ee43fe96f520f952343412a6f";}s:7:"pubdate";i:1343680221;s:7:"dc:date";i:1343680221;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:19;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:41:"NFL launches wellness program for players";s:11:"description";s:563:"NEW YORK (AP) &mdash; In an offseason marked by Junior Seau's suicide and scores of lawsuits over brain injuries, the NFL on Thursday launched a comprehensive wellness program for current and retired players &mdash; including a confidential mental health phone line.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12842-nfl-launches-wellness-program-for-players.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:91:"http://www.chicagodefender.com/article-12842-nfl-launches-wellness-program-for-players.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/3dbd95e1068190e12a1f621c07ffb61c";}s:7:"pubdate";i:1343680221;s:7:"dc:date";i:1343680221;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:20;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:50:"Williams sisters win opening Olympic doubles match";s:11:"description";s:489:"WIMBLEDON, England (AP) &mdash; Venus and Serena Williams began a bid for their third Olympic doubles gold medal by beating Sorana Cirstea and Simona Halep of Romania 6-3, 6-2 Monday.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12840-williams-sisters-win-opening-olympic-doubles-match.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:100:"http://www.chicagodefender.com/article-12840-williams-sisters-win-opening-olympic-doubles-match.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/59d69576c19654c4f302a1157492744c";}s:7:"pubdate";i:1343680221;s:7:"dc:date";i:1343680221;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:21;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:51:"Cash, US Olympic women&#039;s team ready for Angola";s:11:"description";s:379:"LONDON (AP) &mdash; Swin Cash's Olympic basketball career has come full circle.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12839-cash-us-olympic-womens-team-ready-for-angola.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:94:"http://www.chicagodefender.com/article-12839-cash-us-olympic-womens-team-ready-for-angola.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/092e412d9f91210a92d250b3b421df7d";}s:7:"pubdate";i:1343680221;s:7:"dc:date";i:1343680221;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:22;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:41:"Brizard reflects on his first year at CPS";s:11:"description";s:590:"The first 13 months on the job for Chicago Public Schools CEO Jean-Claude Brizard has been both tumultuous and rewarding as he attempts to change the direction of an oft-challenging and complicated education system faced with massive budget problems, achievement shortfalls and labor disputes.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12835-brizard-reflects-on-his-first-year-at-cps.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:91:"http://www.chicagodefender.com/article-12835-brizard-reflects-on-his-first-year-at-cps.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/e378be786392120b4b5f6c0cb2b3d0f6";}s:7:"pubdate";i:1343329216;s:7:"dc:date";i:1343329216;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:23;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:54:"Mixed feelings about notion of an elected school board";s:11:"description";s:709:"While an attempt to place an advisory referendum on the Nov.6 ballot that would probe whether voters preferred an elected city school board instead of one appointed by Mayor Rahm Emanuel was struck down due to a technicality, several ward alderman who pushed for the measure said they will continue to move ahead on the issue to ensure their constituents voices are heard when it comes to education.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12836-mixed-feelings-about-notion-of-an-elected-school-board.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:104:"http://www.chicagodefender.com/article-12836-mixed-feelings-about-notion-of-an-elected-school-board.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/971bf527c1b3e33a649a2a21cc59770b";}s:7:"pubdate";i:1343329216;s:7:"dc:date";i:1343329216;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:24;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:22:"Isiah and the children";s:11:"description";s:538:"Former Detroit Pistons Point Guard and NBA Hall of Famer Isiah Thomas joined forces with the Housing Authority of Cook County (HACC) and Sheriff Tom Dart to share the secrets of his success with young residents of one of the state&rsquo;s poorest communities. 
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12832-isiah-and-the-children.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:72:"http://www.chicagodefender.com/article-12832-isiah-and-the-children.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/26222b1c0cd5c954c2f675bfd711f193";}s:7:"pubdate";i:1343327438;s:7:"dc:date";i:1343327438;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:25;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"2012 Bud Billiken King and Queen contest winners";s:11:"description";s:436:"King: Dakota James		Queen: Autumn Caldwell	
Prince: Isaiah Day		Princess: Zaria Irving		
Lord: Thomas Jackson		Lady: Erin Chapman		

    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12833-2012-bud-billiken-king-and-queen-contest-winners.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:98:"http://www.chicagodefender.com/article-12833-2012-bud-billiken-king-and-queen-contest-winners.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/fdd9b12af2ddbcc29d3d6cef3e621ad9";}s:7:"pubdate";i:1343327438;s:7:"dc:date";i:1343327438;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:26;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:60:"Save the Date: Chicago Football Classic &amp;ndash; Sept. 29";s:11:"description";s:463:"The 15th Annual Chicago Football Classic returns Sept. 29 with Albany State University and Kentucky State University ready to lock horns at Soldier Field. 
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12830-save-the-date-chicago-football-classic-ndash-sept-29.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:102:"http://www.chicagodefender.com/article-12830-save-the-date-chicago-football-classic-ndash-sept-29.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/0e574954a118b7d52f2236aa93242e49";}s:7:"pubdate";i:1343327438;s:7:"dc:date";i:1343327438;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:27;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:56:"Beyond the Locker Room: U.S. Olympic Squad No Dream Team";s:11:"description";s:624:"Clad in uniforms of the 1992 U.S. Olympic men&rsquo;s basketball Dream Team, the current edition of gold medal hopefuls gave an uninspired performance against Argentina, barely winning the exhibition game by a score of 86-80 and making it obvious that this team isn&rsquo;t close to the dominance of the 1992 squad.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12829-beyond-the-locker-room-us-olympic-squad-no-dream-team.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:103:"http://www.chicagodefender.com/article-12829-beyond-the-locker-room-us-olympic-squad-no-dream-team.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/d5f1fd654da82685c5353990811d4a2d";}s:7:"pubdate";i:1343327438;s:7:"dc:date";i:1343327438;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:28;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:50:"Illinois governor signs 2 consumer protection laws";s:11:"description";s:514:"CHICAGO (AP) &mdash; Illinois Gov. Pat Quinn signed two new consumer protection laws Wednesday, one restricting high-risk home loans and the other aimed at preventing debtors from unfairly being sent to jail.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12831-illinois-governor-signs-2-consumer-protection-laws.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:100:"http://www.chicagodefender.com/article-12831-illinois-governor-signs-2-consumer-protection-laws.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/04a5744fb7b69473554dda7c52cf2960";}s:7:"pubdate";i:1343327438;s:7:"dc:date";i:1343327438;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:29;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Muhammad Ali gala kicks off Olympic party season";s:11:"description";s:496:"LONDON (AP) &mdash; Muhammad Ali was the star of a London charity gala Wednesday that set off the Olympic party season - though with a gentle jog rather than an A-list burst out of the blocks.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12825-muhammad-ali-gala-kicks-off-olympic-party-season.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:98:"http://www.chicagodefender.com/article-12825-muhammad-ali-gala-kicks-off-olympic-party-season.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/8b7fde2262e6d849e80d7d39fa03eaa1";}s:7:"pubdate";i:1343313017;s:7:"dc:date";i:1343313017;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:30;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Police: 1 injured in Illinois workplace shooting";s:11:"description";s:530:"MANTENO, Ill. (AP) &mdash; Police in northern Illinois were searching Wednesday for a suspect they say wounded a man in a workplace shooting at a distribution warehouse in northeastern Illinois, a law enforcement official said.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12827-police-1-injured-in-illinois-workplace-shooting.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.chicagodefender.com/article-12827-police-1-injured-in-illinois-workplace-shooting.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/c65432e749b94172198a33de942dfff9";}s:7:"pubdate";i:1343313017;s:7:"dc:date";i:1343313017;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:31;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Obama creating African-American education office";s:11:"description";s:426:"WASHINGTON (AP) &mdash; President Barack Obama is creating a new office to bolster education of African-American students.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12826-obama-creating-african-american-education-office.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:98:"http://www.chicagodefender.com/article-12826-obama-creating-african-american-education-office.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/6f02470456b0fa63a6d974a4044a9cec";}s:7:"pubdate";i:1343313017;s:7:"dc:date";i:1343313017;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:32;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:47:"Urlacher says Bears poised to meet expectations";s:11:"description";s:390:"BOURBONNAIS, Ill. (AP) &mdash; Brian Urlacher's outlook is as healthy as his left knee.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12828-urlacher-says-bears-poised-to-meet-expectations.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.chicagodefender.com/article-12828-urlacher-says-bears-poised-to-meet-expectations.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/b29c4a2a09dde6ba7feccd035a179582";}s:7:"pubdate";i:1343313017;s:7:"dc:date";i:1343313017;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:33;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:45:"Report: Suspect described killings in package";s:11:"description";s:509:"DENVER (AP) &mdash; As reports emerged of a suspicious package sent to a university the suspect in the Colorado theater shooting once attended, the first memorial service was held for a victim of the massacre.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12822-report-suspect-described-killings-in-package.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:94:"http://www.chicagodefender.com/article-12822-report-suspect-described-killings-in-package.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/ee41f04244291daee1fc2d34b3153f37";}s:7:"pubdate";i:1343313017;s:7:"dc:date";i:1343313017;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:34;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"AIDS experts: Focus on pregnant women not enough";s:11:"description";s:533:"WASHINGTON (AP) &mdash; The AIDS epidemic increasingly is a female one, and women are making the case at the world's largest AIDS meeting that curbing it will require focusing on poverty and violence, not just pregnancy and pills.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12823-aids-experts-focus-on-pregnant-women-not-enough.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.chicagodefender.com/article-12823-aids-experts-focus-on-pregnant-women-not-enough.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/f817e5fda5f0c08eb0fcf13ad5a12591";}s:7:"pubdate";i:1343313017;s:7:"dc:date";i:1343313017;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:35;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:44:"Gun violence shifts to forefront of campaign";s:11:"description";s:564:"WASHINGTON (AP) &mdash; Days after the mass shootings in Colorado, guns shifted to the forefront of the presidential campaign as President Barack Obama and Republican candidate Mitt Romney engaged in their most extensive discussions on the issue since the tragedy.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12824-gun-violence-shifts-to-forefront-of-campaign.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:94:"http://www.chicagodefender.com/article-12824-gun-violence-shifts-to-forefront-of-campaign.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/0d59d175e6118e6cff4f41de1760fd9f";}s:7:"pubdate";i:1343313017;s:7:"dc:date";i:1343313017;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:36;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:47:"Chicago starts tear-down of abandoned buildings";s:11:"description";s:459:"CHICAGO (AP) &mdash; Wrecking crews have begun moving across Chicago to demolish abandoned buildings as part of an effort to stop gang violence in the city.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12821-chicago-starts-tear-down-of-abandoned-buildings.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.chicagodefender.com/article-12821-chicago-starts-tear-down-of-abandoned-buildings.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/49edc6da11f406655e4804a9928776d6";}s:7:"pubdate";i:1342202433;s:7:"dc:date";i:1342202433;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:37;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Report on abuse scandal tarnishes Paterno legacy";s:11:"description";s:614:"STATE COLLEGE, Pa. (AP) &mdash; A blistering report that claims Joe Paterno and other top Penn State officials concealed what they knew about Jerry Sandusky's sexual abuse of children may prove to be an indelible stain on the beloved coach's 61-year tenure at the school where he preached "success with honor."
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12817-report-on-abuse-scandal-tarnishes-paterno-legacy.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:98:"http://www.chicagodefender.com/article-12817-report-on-abuse-scandal-tarnishes-paterno-legacy.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/e025f7fa1ed7ccd82f9e2d465555481b";}s:7:"pubdate";i:1342202433;s:7:"dc:date";i:1342202433;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:38;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"CPS says 12 principals abused free-lunch program";s:11:"description";s:481:"CHICAGO (AP) &mdash; Twelve Chicago Public Schools principals and assistant principals are accused of falsifying documents to get free or reduced lunches for their own children.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12820-cps-says-12-principals-abused-free-lunch-program.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:98:"http://www.chicagodefender.com/article-12820-cps-says-12-principals-abused-free-lunch-program.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/7bdf397b9cb49de081621b343ba7494d";}s:7:"pubdate";i:1342202433;s:7:"dc:date";i:1342202433;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:39;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:50:"Griffin out, Davis has shot for US basketball team";s:11:"description";s:416:"LAS VEGAS (AP) &mdash; Anthony Davis was seated on the court, the best players in the world towering above him.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12819-griffin-out-davis-has-shot-for-us-basketball-team.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:99:"http://www.chicagodefender.com/article-12819-griffin-out-davis-has-shot-for-us-basketball-team.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/0e17eb3599d12001ad0fb61bbc358464";}s:7:"pubdate";i:1342202433;s:7:"dc:date";i:1342202433;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:40;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:50:"Jordan: 1992 Dream Team better than 2012 USA squad";s:11:"description";s:461:"CHARLOTTE, N.C. (AP) &mdash; Michael Jordan said there's no way Kobe Bryant and this year's USA Olympic basketball team could've beaten the 1992 Dream Team.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12818-jordan-1992-dream-team-better-than-2012-usa-squad.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:99:"http://www.chicagodefender.com/article-12818-jordan-1992-dream-team-better-than-2012-usa-squad.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/a5af454836e4c297eccc237e0b418eb6";}s:7:"pubdate";i:1342202433;s:7:"dc:date";i:1342202433;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:41;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:42:"Biden to offer rebuttal to Romney at NAACP";s:11:"description";s:536:"WASHINGTON (AP) &mdash; Vice President Joe Biden is offering a rebuttal of Republican presidential candidate Mitt Romney before the nation's largest civil rights organization, defending President Barack Obama's record before black voters.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12813-biden-to-offer-rebuttal-to-romney-at-naacp.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:92:"http://www.chicagodefender.com/article-12813-biden-to-offer-rebuttal-to-romney-at-naacp.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/69a057000112ea5c4776183d74c75e5b";}s:7:"pubdate";i:1342098007;s:7:"dc:date";i:1342098007;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:42;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:64:"Jackson&#039;s &#039;mood disorder&#039; raising more questions?";s:11:"description";s:579:"CHICAGO (AP) &mdash; U.S. Rep. Jesse Jackson Jr.'s disclosure that he is suffering from a "mood disorder" still leaves many questions about his secretive medical leave and whether the Illinois congressman has satisfied mounting calls to be more open about his monthlong absence.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12812-jacksons-mood-disorder-raising-more-questions.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:95:"http://www.chicagodefender.com/article-12812-jacksons-mood-disorder-raising-more-questions.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/26e6050be97247a36e1e1e96e2405834";}s:7:"pubdate";i:1342098007;s:7:"dc:date";i:1342098007;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:43;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:43:"Bill helps minority, women-owned businesses";s:11:"description";s:392:"CHICAGO (AP) &mdash; Minority and women-owned businesses are getting some help in Cook County.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12814-bill-helps-minority-women-owned-businesses.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:92:"http://www.chicagodefender.com/article-12814-bill-helps-minority-women-owned-businesses.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/44bd2b34ce9185a4962fb7619bde1230";}s:7:"pubdate";i:1342098007;s:7:"dc:date";i:1342098007;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:44;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Former Bear Shaun Gayle latest player to sue NFL";s:11:"description";s:532:"CHICAGO (AP) &mdash; Former Chicago Bears defensive back Shaun Gayle has joined other former professional football players in suing the National Football League for injuries they say have resulted in debilitating health effects.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12815-former-bear-shaun-gayle-latest-player-to-sue-nfl.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:98:"http://www.chicagodefender.com/article-12815-former-bear-shaun-gayle-latest-player-to-sue-nfl.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/930db6e9e4012259d7bb8f8d52bf3db6";}s:7:"pubdate";i:1342098007;s:7:"dc:date";i:1342098007;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:45;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:47:"Prep hoops star Parker names 10 college choices";s:11:"description";s:517:"LOS ANGELES (AP) &mdash; Basketball star Jabari Parker has named the 10 college programs he is considering with a year to go before he graduates from high school, and DePaul in his hometown of Chicago made the cut.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12816-prep-hoops-star-parker-names-10-college-choices.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.chicagodefender.com/article-12816-prep-hoops-star-parker-names-10-college-choices.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/e74a7d2154dba65432b442b1c5de9f44";}s:7:"pubdate";i:1342098007;s:7:"dc:date";i:1342098007;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:46;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:49:"Obama calls for rise in small business write-offs";s:11:"description";s:452:"WASHINGTON (AP) &mdash; President Barack Obama is calling on Congress to increase the amount of investments small businesses can expense next year.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12808-obama-calls-for-rise-in-small-business-write-offs.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:99:"http://www.chicagodefender.com/article-12808-obama-calls-for-rise-in-small-business-write-offs.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/5cb0b2571dc6940de2376afb7ae68307";}s:7:"pubdate";i:1342024242;s:7:"dc:date";i:1342024242;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:47;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:48:"Collective effort needed to reduce crime numbers";s:11:"description";s:475:"There&rsquo;ve been more than 275 murders in the city within the last seven months. The number exceeds the number of soldiers killed in combat during the same time period.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12803-collective-effort-needed-to-reduce-crime-numbers.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:98:"http://www.chicagodefender.com/article-12803-collective-effort-needed-to-reduce-crime-numbers.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/05c799b2abb92338903a560cd1d66d0a";}s:7:"pubdate";i:1342024242;s:7:"dc:date";i:1342024242;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:48;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:50:"Romney to NAACP: Voting GOP good for your families";s:11:"description";s:474:"HOUSTON (AP) &mdash; Republican Mitt Romney will tell black voters during a speech to the NAACP that backing him for president is in the best interest of their families.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12807-romney-to-naacp-voting-gop-good-for-your-families.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:99:"http://www.chicagodefender.com/article-12807-romney-to-naacp-voting-gop-good-for-your-families.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/3be62bd6a00d2040ff2ea9aa39c1ea1d";}s:7:"pubdate";i:1342024242;s:7:"dc:date";i:1342024242;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}i:49;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:81:"&amp;lsquo;Big names&amp;rsquo; on tap for RPC&amp;rsquo;s 41st annual convention";s:11:"description";s:733:"Influential thought leaders are joining with the Rev. Jesse L. Jackson Sr. this week for the 41st national dialogue on civil rights sponsored by Rainbow PUSH Coalition and Citizenship Education Fund. Running through Saturday at the Chicago Hilton & Towers Hotel, the confab Is themed A More Perfect Union: Closing the Gap and Expanding the Tent. It will once again bring leaders together in an innovative exchange of ideas.
    
      <p>
    	<span>
        <a href="http://www.chicagodefender.com/article-12806-lsbig-namesrs-on-tap-for-rpcrss-41st-annual-convention.html">Originally</a>&nbsp; 
                    from <a href="http://www.chicagodefender.com/">Chicago Defender</a></span>
            
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:104:"http://www.chicagodefender.com/article-12806-lsbig-namesrs-on-tap-for-rpcrss-41st-annual-convention.html";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:44:"tag:sppfeed/613356a230cf40ec764ba1483201ddc7";}s:7:"pubdate";i:1342024242;s:7:"dc:date";i:1342024242;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:0:"";}}}}}s:3:"url";s:69:"http://www.sigmapiphi.org/feeds/out/rss.php?user=1&feedtag=Newspapers";}