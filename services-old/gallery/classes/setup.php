<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Setup the package
 *
 * @package svManager
 */
 class Setup
 {
  /**
   * Constructs Setup class
   * Unsets zend compatibility mode and sets error reporting
   * Strips slashes form $_POST and $_GET
   * It is best not to set the error level to 0 for security in auth.php
   */
   function Setup()
   {
     if( version_compare(phpversion(), '5.0', '>=' ) && version_compare(phpversion(), '6.0', '<=' )) @ini_set('zend.ze1_compatibility_mode', '0');
     if (DEBUG) @ini_set('display_errors', 1);
     @ini_set('html_errors', 1);
     $_GET = $this->rStripSlashes($_GET);
     $_POST = $this->rStripSlashes($_POST);
   }

  /**
   * recursive function to strip slashes
   * see www.php.net/stripslashes strip_slashes_deep function
   *
   * @access public
   * @return array
   * @parameter array
   */
   function rStripSlashes($value)
   {
     if (!get_magic_quotes_gpc()) return $value;
     $value = is_array($value) ? array_map(array($this, 'rStripSlashes'), $value) : stripslashes($value);
     return $value;
   }
   
  /**
   * Checks login and creates login screen if necessary
   *
   * @access public
   * @return void
   */
   function checkLogin(&$auth)
   {
     if (isset($_GET['logout']))
     {
       $auth->logout();
     }
     if (!$auth->login())
     {
       $page = new loginPage('svManager &ndash; log-in', 'login');
       print $page->getHtmlHead();
       print $page->getLoginContent(isset($_POST['loginsubmitted']));
       print $page->getFooter();
       exit;
     }
   }
   
  /**
   * Handle error condition when no current gallery
   *
   * @access public
   * @return void
   * @param object galleryFactory
   * @param object error handler
   */
   function noCurrentGallery(&$galleryFactory, &$errorHandler)
   {
     $page = $galleryFactory->makeErrorPage(); 
     print $page->getHtmlHead();
     print $page->getPageHeader();
     print $errorHandler->getMessages();
     print '<p>Please go to the <a href="svmanager.php">galleries page</a> and select a gallery to edit</p>';
     print $page->getFooter();
   }
 }
?>