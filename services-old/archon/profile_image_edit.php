<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchon = "-1";
if (isset($_POST['CUSTOMERID'])) {
  $colname_rsArchon = (get_magic_quotes_gpc()) ? $_POST['CUSTOMERID'] : addslashes($_POST['CUSTOMERID']);
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchon = sprintf("SELECT CUSTOMERID, IMAGE FROM spp_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname_rsArchon, "int"));
$rsArchon = mysql_query($query_rsArchon, $sigma_modx) or die(mysql_error());
$row_rsArchon = mysql_fetch_assoc($rsArchon);
$totalRows_rsArchon = mysql_num_rows($rsArchon);

//start Trigger_ImageUpload trigger
//remove this line if you want to edit the code by hand
function Trigger_ImageUpload(&$tNG) {
  $uploadObj = new tNG_ImageUpload($tNG);
  $uploadObj->setFormFieldName("IMAGE");
  $uploadObj->setDbFieldName("IMAGE");
  $uploadObj->setFolder("/opt/lampp/htdocs/modx_lisa/home/assets/images/archons/");
  $uploadObj->setResize("true", 190, 0);
  $uploadObj->setMaxSize(3072);
  $uploadObj->setAllowedExtensions("gif, jpg, jpe, jpeg, png");
  $uploadObj->setRename("custom");
  $uploadObj->setRenameRule("{POST.CUSTOMERID}.{KT_name}.{KT_ext}");
  return $uploadObj->Execute();
}
//end Trigger_ImageUpload trigger

// Make an update transaction instance
$upd_spp_archons = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$return = !empty( $_POST['RETURN'] ) ? $_POST['RETURN'] : '/home/personal-profile.php';
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
$upd_spp_archons->registerTrigger("AFTER", "Trigger_ImageUpload", 97);
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");
$upd_spp_archons->addColumn("IMAGE", "FILE_TYPE", "FILES", "IMAGE");
$upd_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$row_rsspp_archons = mysql_fetch_assoc($rsspp_archons);
$totalRows_rsspp_archons = mysql_num_rows($rsspp_archons);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">ADD YOUR IMAGE</h1>
<div class="contentBlock">
  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" enctype="multipart/form-data">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th">CURRENT IMAGE: </td>
        <td>
		  <?php
// Show IF Conditional region1
if (@$row_rsArchon['IMAGE'] != NULL) {
?>
		    <img src="/home/assets/images/archons/<?php echo $row_rsArchon['IMAGE']; ?>" class="border" />
		    <?php
// else Conditional region1
} else { ?>
		    <img src="../includes/tng/styles/img_not_found.gif" alt="Add your image." class="border" />
  <?php }
// endif Conditional region1
?>		</td>
      </tr>
      <tr>
        <td class="KT_th"><label for="IMAGE">IMAGE:</label></td>
        <td><input type="file" name="IMAGE" id="IMAGE" size="32" />
            <?php echo $tNGs->displayFieldError("spp_archons", "IMAGE"); ?> </td>
      </tr>

    </table>

      <div class="KT_bottombuttons">
        <input type="submit" name="KT_Update1" id="KT_Update1" value="Add" />
		<input type="button" name="Cancel" value="Cancel" onclick="history.go(-1)" />
      </div>

 	<input name="CUSTOMERID" type="hidden" id="CUSTOMERID" value="<?php echo KT_escapeAttribute($_POST['CUSTOMERID']); ?>" />
	<input name="UPDATEDBY" type="hidden" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($_POST['UPDATEDBY']); ?>" />
	<input name="RETURN" type="hidden" id="RETURN" value="<?php echo KT_escapeAttribute($_POST['RETURN']); ?>" />
 </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchon);
?>
