<?php
/*
Name: SigPiPhi Recent Articles & Extra Content
 * Type: standard
 * Some variables are already defined:
 *
 * - $theme_options An array with all theme options
 * - $theme_url Is the absolute URL to the theme folder used to reference images
 * - $theme_subject Will be the email subject if set by this theme
 *
 */

global $newsletter, $post;

$color = $theme_options['theme_color'];
if (empty($color)) $color = '#000000';

if (isset($theme_options['theme_posts'])) {
    $filters = array();
    
    if (empty($theme_options['theme_max_posts'])) $filters['posts_per_page'] = 10;
    else $filters['posts_per_page'] = (int)$theme_options['theme_max_posts'];
    
    if (!empty($theme_options['theme_categories'])) {
        $filters['category__in'] = $theme_options['theme_categories'];
    }
    
    if (!empty($theme_options['theme_tags'])) {
        $filters['tag'] = $theme_options['theme_tags'];
    }
    
    if (!empty($theme_options['theme_post_types'])) {
        $filters['post_type'] = $theme_options['theme_post_types'];
    }    
    
    $posts = get_posts($filters);
}

?><!DOCTYPE html>
<html>
    <head>
        <!-- Not all email client take care of styles inserted here -->
        <style type="text/css" media="all">
            a {
                text-decoration: none;
                color: <?php echo $color; ?>;
            }
			a:hover, a:visited, a:link {color: #0085ba;}
			
        </style>
    </head>
    <body style="background-color: #ddd; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 14px; color: #666; margin: 0 auto; padding: 0;">
        <br>
        <table align="center">
            <tr>
                <td valign="top" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 14px; color: #666;">
                    <div style="text-align: left; background-color: #fff; max-width: 500px;">
                       <div style="padding: 30px 20px; color: #000; font-size: 28px; background-color: #EFEFEF; border-bottom: 1px solid #ddd; text-align: center;">
								
                        <?php //HEADER
//                        if (!empty($theme_options['theme_banner'])) { 
//                            echo $theme_options['theme_banner'];

                        if (!empty($theme_options['theme_header_logo']['url'])) { ?>
                            <img style="max-width: 200px" alt="<?php echo $theme_options['main_header_title'] ?>" src="<?php echo $theme_options['theme_header_logo']['url'] ?>" />
                        <?php } elseif (!empty($theme_options['main_header_logo']['url'])) { ?>
                            <img style="max-width: 200px" alt="<?php echo $theme_options['main_header_title'] ?>" src="<?php echo $theme_options['main_header_logo']['url'] ?>" />
                        <?php } elseif (!empty($theme_options['main_header_title'])) { ?>
                             <div style="padding: 30px 0; color: #000; font-size: 28px; background-color: #EFEFEF; border-bottom: 1px solid #ddd; text-align: center;">
                                <?php echo $theme_options['main_header_title'] ?>
                            </div>
                            <?php if (!empty($theme_options['main_header_sub'])) { ?>
                            <div style="padding: 10px 0; color: #000; font-size: 16px; text-align: center;">
								
                                <?php echo $theme_options['main_header_sub'] ?>
                            </div>
                        <?php } ?>
                        <?php } else { ?>
                            
								<img style="max-width: 200px; text-align:center" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/header-logo-combined.png" />
                                <?php //echo get_option('blogname'); ?>
                            </div>
                            <?php if (!empty($theme_options['main_header_sub'])) { ?>
                            <div style="padding: 10px 0; color: #000; font-size: 16px; text-align: center;">
                                <?php echo $theme_options['main_header_sub'] ?>
                            </div>
                        <?php } ?>
                        <?php } ?>
                        </div>
                        
                            
                        <div style="padding: 10px 20px 20px 20px; background-color: #fff; line-height: 18px">
							
							<h2 style="text-align: center; font-weight: bold; margin: 20px 0 5px;line-height:25px" align="center"><font color="#595959" face="trebuchet ms,geneva">Archon Network News</font></h2>
							<p style="text-align: center; font-size: small;">July 20, 2016</p>

                            <p style="text-align: center; font-size: small;"><a target="_blank"  href="{email_url}">View this email online</a></p>

                            <p>Here you can start to write your message. Be polite with your readers. Don't forget the subject of this message.</p>
                            <?php if (!empty($posts)) { ?>
                            <table cellpadding="5">
                                <?php foreach ($posts as $post) { setup_postdata($post); ?>
                                    <tr>
                                        <?php if (isset($theme_options['theme_thumbnails'])) { ?>
                                        <td valign="top"><a target="_blank"  href="<?php echo get_permalink($post); ?>"><img width="175" src="<?php echo newsletter_get_post_image($post->ID); ?>" alt="image"></a></td>
                                        <?php } ?>
                                        <td valign="top">
                                            <a target="_blank"  href="<?php echo get_permalink(); ?>" style="font-size: 20px; line-height: 26px"><?php the_title(); ?></a>
                                            <?php if (isset($theme_options['theme_excerpts'])) newsletter_the_excerpt($post); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
							<hr />
							<h2 style="text-align: left; font-weight: bold; margin: 40px 0 10px;" align="left"><font color="#595959" face="trebuchet ms,geneva"><a href="#">Headline Article 1</a></font></h2>
							<p><font color="#595959">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin volutpat quam et semper condimentum. Mauris vel rutrum orci. Donec ut semper lacus. Aenean non tempus diam, sed tincidunt leo. Morbi quis metus velit. Donec fringilla lectus a neque elementum malesuada. Praesent sodales mi sapien, eu efficitur eros rutrum in.</font></p>
							<p align="right"><font size="1" color="#595959"><a href="#">MORE &gt;</a></font></p>
							<hr />
							<h2 style="text-align: left; font-weight: bold; margin: 40px 0 10px;" align="left"><font color="#595959"><strong>Headline Article Two</strong></span></font></h2>
							<p><font color="#595959">Lorem ipsum <img style="margin-right: 25px; margin-bottom: 20px;" src="http://dev3.sigmapiphi.org/boules/wp-content/uploads/2016/02/shutterstock_185931644-300x300.jpg" alt="" width="120" height="120" align="left" border="0" vspace="0" />dolor sit amet, consectetur adipiscing elit. Proin volutpat quam et semper condimentum. Mauris vel rutrum orci. Donec ut semper lacus. Aenean non tempus diam, sed tincidunt leo. Morbi quis metus velit. Donec fringilla lectus a neque elementum malesuada. Praesent sodales mi sapien, eu efficitur eros rutrum in. Quisque faucibus nunc ac rutrum bibendum. Integer dignissim ante vitae lacus gravida elementum. Morbi dictum orci ut nibh pretium finibus. Nam feugiat id lacus in elementum. Donec mollis rhoncus tempor. Pellentesque ac dui cursus, tristique neque ac, fermentum justo.</font></p>
							<p align="right"><span style="font-size: x-small; text-align: -webkit-right;"><a href="#">MORE &gt;</a></span></p>
							<hr />
							<h2 style="text-align: left; font-weight: bold; margin: 40px 0 10px;" align="left"><font color="#595959" face="trebuchet ms,geneva"><a href="#">Headline Article Three</a></font></h2>
							<p>Lorem ipsum dolor sit amet, quisque faucibus nunc ac rutrum bibendum. Integer dignissim ante vitae lacus gravida elementum. Morbi dictum orci ut nibh pretium finibus. Nam feugiat id lacus in elementum. Donec mollis rhoncus tempor. Pellentesque ac dui cursus, tristique neque ac, fermentum justo.</p>
							<p align="right"><font size="1" color="#595959"><a href="#">MORE &gt;</a></font></p>
							
							<table cellpadding="5" style="margin-top:15px; padding:30px 0 15px; border-top: 8px solid #CCC; border-bottom: 8px solid #CCC">
							<tbody>
							<tr>
							<td style="text-align: center;" valign="top" width="150"><strong><font face="trebuchet ms,geneva"><a style="font-size: 20px; line-height: 26px;" href="http://dev3.sigmapiphi.org/boules/recent-updates/" target="_blank"><img src="http://dev3.sigmapiphi.org/boules/wp-content/uploads/2016/03/statesround.png" alt="Regional Boulés" width="110" height="110" />Regional Boulés</a></font></strong>
							<p>News and correspondence from the five regions. Visit them today.</p>
							</td>
							<td style="text-align: center;" valign="top" width="150"><font color="#595959"><strong><font face="trebuchet ms,geneva"><a style="font-size: 20px; line-height: 26px;" href="http://dev3.sigmapiphi.org/boules" target="_blank"><img src="http://dev3.sigmapiphi.org/boules/wp-content/uploads/2016/03/archiveround.png" alt="Grand Boulé Archiving" width="110" height="110" />Grand Boulé Archiving</a></font></strong></font>
							<p><font color="#595959">A growing and ongoing initiative to preserve our historical documents. View the details.</font></p>
							</td>
							<td style="text-align: center;" valign="top" width="150">
							<strong style="color: #595959; line-height: 18px;"><font face="trebuchet ms,geneva"><a style="font-size: 20px; line-height: 26px; text-align: center;" href="http://dev3.sigmapiphi.org/boules/business-directory" target="_blank"><img src="http://dev3.sigmapiphi.org/boules/wp-content/uploads/2016/03/handround.png" alt="Business Directory" width="110" height="110" /></a></font></strong><strong style="color: #595959; line-height: 18px;"><font face="trebuchet ms,geneva"><a style="font-size: 20px; line-height: 26px; text-align: center;" href="http://dev3.sigmapiphi.org/boules/business-directory/" target="_blank">Business Directory</a></font></strong>
							<p><font style="text-align: center;" color="#595959">Share your business contact information here. Enter your listing today.</font></p>
							</td>
							</tr>
							</tbody>
							</table>
                            <?php } ?>
                            
                            <?php include WP_CONTENT_DIR . '/extensions/newsletter/emails/themes/SigPiPhiArticleCollection/footer.php'; ?>

                        </div>

                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>