<?php die('Unauthorized access.'); ?>a:103:{s:2:"id";s:5:"13701";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:17:"Boulé Foundation";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:16:"boule-foundation";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:10:"1286807828";s:10:"unpub_date";s:1:"0";s:6:"parent";s:1:"1";s:8:"isfolder";s:1:"1";s:9:"introtext";s:0:"";s:7:"content";s:10236:"<div style="display:visible;">

<div class="span-3"><div class="inner float-left">
<img src="assets/images/archons/TeamerCharlesC.jpg" alt="Archon Charles C. Teamer" border="0" class="float-left"><br />
<span class="block font-small">Charles C. Teamer</span>
</div></div>
<h2>The Vision</h2>
<p>
The vision for establishing the Boul&eacute; Foundation is reflective of the deep passion for education and excellence that have been the hallmarks of Sigma Pi Phi Fraternity since its founding. The celebration of its 25th anniversary is a testament to the care and dedication of Archons throughout the Fraternity in ensuring that it remains a strong financial entity for the provision of aid and assistance that are consistent with its mission and objectives. Its creation and mission have been one of the best kept secrets in the African American community! Because of its tremendous reach of influence, it is now known and celebrated.
</p>
<p style="display:none;">
Peyton Williams, Jr.<br />
Secretary, The Boul&eacute; Foundation
</p>
<p>
The idea for the Foundation was rooted in 1927 when then Grand Sire Archon William C. McNeil first proposed such an entity. The stated purpose of the Boul&eacute; Foundation was to &quot;use and apply the whole or any part of the income therefrom and the principal thereof exclusively for charitable, religious, scientific, literary or educational purposes.&quot; Past Grand Sire Archon Robert V. Franklin, a retired Ohio Supreme Court Judge, developed the legal basis for the Foundation that was eventually chartered in Ohio. The incorporators hammered out a mission distinct from the Fraternity&#39;s Social Action Committee, from which $100,000 had been transferred to fund the new Foundation. The distinction agreed upon was that the Foundation would serve as the Grand Boul&eacute;&#39;s grant-maker to programs recommended by the Social Action Committee. The Foundation is a 501(c) (3) organization and all contributions are tax deductable.
</p>
<div class="span-3"><div class="inner float-left">
<img src="assets/images/archons/anthony-w-hall.jpg" alt="Archon Anthony W. Hall, Jr." border="0" class="float-left"><br />
<span class="block font-small">Anthony W. Hall, Jr.</span>
</div></div>
<h2>A Message From the Chairman</h2>
<p>
Dear Archons and Archousai:
</p>
<p>
The Quartern is The Boul&eacute; Foundation&rsquo;s 2013 Annual Report. The Quartern is so named because it is the first quarter-of-a-century marker for your foundation. And what a journey these twenty-five years have been. 
</p>
<p>
The Quartern is dedicated to the Archons and Archousai of Sigma Pi Phi Fraternity, who planted the seeds for The Boul&eacute; Foundation and who have since moved to higher ground. It is in the inevitable ebb and flow of life that each of us, for however much time we are given, must press on toward the mark.
</p>
<p>
&ldquo;The Boul&eacute; Foundation was created to do the greatest good by building a corpus large enough that the members of Sigma Pi Phi Fraternity could contribute, collectively and generously, to many causes,&rdquo; said Archon Manford Byrd, the Foundation&rsquo;s treasurer. And in this next quartern, we press on to the mark with clarity of purpose and a track record of success in achieving our goals. But in 2005 you rose as well to the challenge of the national emergency created by hurricanes Katrina and Rita. Your giving, which was matched by your foundation, totaled more than $600,000, and in keeping with The Boul&eacute; Foundation&rsquo;s mission, that money has been designated for scholarships to students enrolled in New Orleans and East Texas colleges, whose educational pursuits were interrupted by the storms.
</p>
<p>
The Quartern is the record of The Boul&eacute; Foundation&rsquo;s stewardship during 2005. It is divided into four quarters: The Vision, Resource Generation, Design and Implementation, and Governance and Accountability. You will find on these pages lists of your names and the cumulative level of your legacy giving. Also, several of you responded when we asked why you give to the Boul&eacute; Foundation and why other Archons and Archousai should do the likewise. Several of you generously shared as well your ideas on ways that The Boul&eacute; Foundation might broaden its mission in response to changing times. Again and again, Archons and Archousai told us that they wanted their foundation to do more outreach to African American youths and especially young black males due to their overrepresentation in the nation&rsquo;s prisons, its foster care system, homicide statistics and numbers of absentee fathers.
</p>
<p>
The Boul&eacute; Foundation&rsquo;s compelling mission is &ldquo;to restore greatness to our young people and to our community,&rdquo; Archousa Carol Goss said. And I ask that you be generous so that your foundation can stand in the gap for our children. If we don&rsquo;t, who will?
</p>
<p>
Archon Anthony W. Hall, Jr.<br />
Chairman
</p>
</div>
<h3>Resource Generation</h3>
<p>
The Boul&eacute; Foundation currently generates income from a number of sources. They include return on investment, annual assessments from the Archons, proceeds from the Foundation&#39;s Gala benefit held during the Grand Boul&eacute;, and the personal contributions of Archons and Archousai. The cumulative giving of Archons and Archousai is encouraged through the Boul&eacute; Foundation&#39;s Legacy Awards Program, which began in 1992 with a $1,000 contribution in honor of Sigma Pi Phi&#39;s Founder Henry M. Minton.
</p>
<p>
The Legacy Awards are named for men of the Fraternity who have distinguished themselves in some field of national endeavor.
</p>
<p>
The Legacy Awards are:
</p>
<ul>
	<li><strong>John B. Clemmons, Sr.</strong> ($100,000)</li>
	<li><strong>Robert V. Franklin</strong> ($75,000)</li>
	<li><strong>William Sylvester &quot;Turk&#39; Thompson</strong> ($50,000)</li>
	<li><strong>Harvey C. Russell</strong> ($25,000)</li>
	<li><strong>Percy Julian</strong> ($10,000)</li>
	<li><strong>W.E.B. DuBois</strong> ($5,000)</li>
	<li><strong>James P. Brawley</strong> ($2,500)</li>
	<li><strong>Henry M. Minton Fellow</strong> ($1,000 non-cumulative giving)</li>
</ul>
<h3>Scholarships Sponsored by the Foundation</h3>
<p>
The Boul&eacute; Foundation distributes its funds primarily to educate new generations of black leaders, support others working to protect and advance the civil rights of African Americans and to improve the quality of life for African Americans. The Boul&eacute; Foundation meets its objectives in several ways:
</p>
<h3>Boul&eacute; Achievement Scholarship Program</h3>
<p>
The Foundation annually awards twelve $1,000 achievement scholarships in each of the four years that recipients are enrolled in college. At the Centennial Grand Boul&eacute; in Philadelphia in 2004 new scholarships and grant programs were awarded and announced.
</p>
<h3>The LeBaron Taylor Scholarship</h3>
<p>
Archon Taylor was a pioneer in the entertainment industry. And upon his death at age 65 in 2000, his friend, Archon Oldfield Dukes, raised about $55,000 that was matched by the Foundation in order to provide perpetual four-year scholarships to students aspiring to careers in business, music or the arts.
</p>
<h3>The Thomas Bailey Shropshire Matching Grants Program</h3>
<p>
This program honors Past Grand Sire Archon Thomas Bailey Shropshire. He helped establish The Boul&eacute; Foundation. The matching grants program in his name supports and encourages member boul&eacute;s to energize their social action programs.
</p>
<h3>Centennial Scholarship Fund</h3>
<p>
The fund was established with $100,000 as a one-year-only program with awards made during the 2005-2006 academic year. The program was then extended for an additional academic year. The fund was a call to member boul&eacute;s to share in those funds by applying to the Foundation for matching $1,000 grants for the scholarships they awarded locally. The target group for Centennial Scholarships were young minority males who completed at least one year of college, attained a 3.0 grade point average, and whose road to graduation was made rocky by personal financial pressures. The Boul&eacute; Foundation also seeks to fulfill its commitment to equality and social justice by contributing generously and regularly to other groups whose main focus also is centered around the social issues and advancement of African Americans. Some of those groups include the NAACP, Urban League, NAACP Legal Defense Fund and History Makers.
</p>
<h3>Board of Trustees</h3>
<p>Chairman, Anthony W. Hall, Jr., Esq. (Nu)<br>
Vice Chairman, James A. B. Lockhart, Esq. (Alpha Rho)<br>
Secretary, Dr. Charles C. Teamer, Sr. (Alpha Alpha)<br>
Treasurer, Dr. Manford Byrd, Jr. (Beta)</p>

<p>Mr. Samuel Bacote III (Kappa)*<br>
Mr. Maurice L. Barksdale (Delta Mu)<br>
Mr. Henry H. Brown (Nu)<br>
Mr. Michael B. Bruno (Alpha Alpha)*<br>
Mr. Khephra Burns (Alpha Sigma)*<br>
James O. Cole, Esq., (Alpha Rho)*<br>
Mr. Wesley A. Coleman (Xi)<br>
Mr. N. John Douglas (Gamma Chi)<br>
Mr. Donald Floyd (Delta Zeta)*<br>
Mr. Leon Fulton (Alpha Alpha)<br>
Mr. Robert Holmes (Beta Kappa)<br>
Mr. Darrell B. Jackson (Beta)<br>
Mr. Robert A. McNeely (Alpha Pi)<br>
Cornell Leverette Moore, Esq. (Omicron)<br>
Mr. Dwayne M. Murray (Alpha Xi)*<br>
James E. Payne, Esq. (Delta Zeta)*<br>
Dr. Rodney J. Reed (Alpha Gamma)*<br>
Dr. Enrique Riggs (Zeta)<br>
Mr. Roy S. Roberts (Iota)<br>
Mr. Calvin E. Tyler, Jr. (Epsilon Beta)<br>
Mrs. Jacqulyn C. Shropshire (Delta Theta)<br>
Dr. Gladys Hope Franklin White (Beta Epsilon)<br>
Dr. Eddie N. Williams (Epsilon)<br>
Mrs. Nola L. Whiteman (Alpha Sigma)</p>

<p><em>*Ex Officio members who serve by virtue of their office held in Sigma Pi Phi Fraternity.</em></p>

</div>
<p class="clear">
&nbsp;
</p>

<div style="display:none;">
<p>
<strong>Chairman Emeritus</strong><br />
The Hon. Robert V. Franklin (Alpha Phi)
</p>
<p>
<strong>Trustees Emeriti</strong><br />
Dr. Matthew G. Carter (Gamma Xi)<br />
Dr. Clinton Warner, Jr. (Kappa)<br />
Dr. Huel D. Perkins (Alpha Xi)
</p>
</div>";s:8:"richtext";s:1:"1";s:8:"template";s:3:"251";s:9:"menuindex";s:1:"8";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1286807889";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1451146588";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1286807828";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:10:"Foundation";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"1";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:17:"bouleJournalIssue";a:5:{i:0;s:17:"bouleJournalIssue";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:12:"FeatureImage";a:5:{i:0;s:12:"FeatureImage";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:17:"featureImageThumb";a:5:{i:0;s:17:"featureImageThumb";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:8:"setBoule";a:5:{i:0;s:8:"setBoule";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:7:"showTOC";a:5:{i:0;s:7:"showTOC";i:1;s:3:"Yes";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:10:"iBooksLink";a:5:{i:0;s:10:"iBooksLink";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:11:"AndroidLink";a:5:{i:0;s:11:"AndroidLink";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"file";}s:21:"slider-number-to-show";a:5:{i:0;s:21:"slider-number-to-show";i:1;s:1:"3";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:22:"slider-slide-1-caption";a:5:{i:0;s:22:"slider-slide-1-caption";i:1;s:55:"Caption text here. <span class="link">Read more.</span>";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:21:"callout-1-description";a:5:{i:0;s:21:"callout-1-description";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:20:"slider-slide-1-image";a:5:{i:0;s:20:"slider-slide-1-image";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:18:"slider-slide-1-url";a:5:{i:0;s:18:"slider-slide-1-url";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:12:"slider-start";a:5:{i:0;s:12:"slider-start";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:27:"slider-content-section-show";a:5:{i:0;s:27:"slider-content-section-show";i:1;s:2:"No";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:18:"spotlight-row-show";a:5:{i:0;s:18:"spotlight-row-show";i:1;s:2:"No";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:30:"content-display-featured-image";a:5:{i:0;s:30:"content-display-featured-image";i:1;s:4:"None";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:22:"content-featured-image";a:5:{i:0;s:22:"content-featured-image";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:31:"content-featured-image-location";a:5:{i:0;s:31:"content-featured-image-location";i:1;s:3:"Top";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:27:"content-featured-image-size";a:5:{i:0;s:27:"content-featured-image-size";i:1;s:6:"span-3";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:14:"callouts-start";a:5:{i:0;s:14:"callouts-start";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:17:"callout-1-caption";a:5:{i:0;s:17:"callout-1-caption";i:1;s:55:"Caption text here. <span class="link">Read more.</span>";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:15:"callout-1-image";a:5:{i:0;s:15:"callout-1-image";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:20:"slider-1-description";a:5:{i:0;s:20:"slider-1-description";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:24:"content-sidenav-location";a:5:{i:0;s:24:"content-sidenav-location";i:1;s:4:"Left";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:28:"content-featured-image-align";a:5:{i:0;s:28:"content-featured-image-align";i:1;s:10:"float-left";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:17:"main-nav-start-id";a:5:{i:0;s:17:"main-nav-start-id";i:1;s:10:"@INHERIT 1";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:27:"content-display-sidebar-nav";a:5:{i:0;s:27:"content-display-sidebar-nav";i:1;s:4:"none";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:32:"listing-featured-documents-title";a:5:{i:0;s:32:"listing-featured-documents-title";i:1;s:16:"Featured Listing";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:22:"listing-number-to-show";a:5:{i:0;s:22:"listing-number-to-show";i:1;s:1:"3";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:21:"listing-orderBy-field";a:5:{i:0;s:21:"listing-orderBy-field";i:1;s:9:"pagetitle";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:13:"listing-start";a:5:{i:0;s:13:"listing-start";i:1;s:14:"@INHERIT 38641";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:12:"listing-type";a:5:{i:0;s:12:"listing-type";i:1;s:7:"parents";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:17:"listing-sortOrder";a:5:{i:0;s:17:"listing-sortOrder";i:1;s:4:"DESC";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:29:"listing-event-documents-title";a:5:{i:0;s:29:"listing-event-documents-title";i:1;s:14:"Events Listing";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:19:"listing-event-start";a:5:{i:0;s:19:"listing-event-start";i:1;s:14:"@INHERIT 98,10";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:27:"listing-event-orderBy-field";a:5:{i:0;s:27:"listing-event-orderBy-field";i:1;s:9:"pagetitle";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:23:"listing-event-sortOrder";a:5:{i:0;s:23:"listing-event-sortOrder";i:1;s:4:"DESC";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:28:"listing-event-number-to-show";a:5:{i:0;s:28:"listing-event-number-to-show";i:1;s:1:"3";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:18:"listing-event-type";a:5:{i:0;s:18:"listing-event-type";i:1;s:7:"parents";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:24:"event-documents-row-show";a:5:{i:0;s:24:"event-documents-row-show";i:1;s:2:"No";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:16:"event-start-time";a:5:{i:0;s:16:"event-start-time";i:1;s:0:"";i:2;s:8:"unixtime";i:3;s:32:"&format=%A %d, %B %Y&default=Yes";i:4;s:4:"date";}s:14:"event-end-time";a:5:{i:0;s:14:"event-end-time";i:1;s:0:"";i:2;s:8:"unixtime";i:3;s:32:"&format=%A %d, %B %Y&default=Yes";i:4;s:4:"date";}s:16:"show-in-calendar";a:5:{i:0;s:16:"show-in-calendar";i:1;s:2:"No";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:14:"global-tagline";a:5:{i:0;s:14:"global-tagline";i:1;s:30:"Inspiring Our Youth to Succeed";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:15:"palette_default";a:5:{i:0;s:15:"palette_default";i:1;s:16:"@INHERIT Default";i:2;s:0:"";i:3;s:0:"";i:4;s:7:"listbox";}s:19:"slider-orderBy-sort";a:5:{i:0;s:19:"slider-orderBy-sort";i:1;s:3:"ASC";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:16:"palette_loggedin";a:5:{i:0;s:16:"palette_loggedin";i:1;s:37:"@INHERIT color-override-yellow-orange";i:2;s:0:"";i:3;s:0:"";i:4;s:7:"listbox";}s:22:"main-nav-level-to-show";a:5:{i:0;s:22:"main-nav-level-to-show";i:1;s:1:"2";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:21:"callout-orderBy-field";a:5:{i:0;s:21:"callout-orderBy-field";i:1;s:9:"menuindex";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:20:"callout-orderBy-sort";a:5:{i:0;s:20:"callout-orderBy-sort";i:1;s:3:"ASC";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:21:"sidebar-listing-start";a:5:{i:0;s:21:"sidebar-listing-start";i:1;s:15:"@INHERIT [*id*]";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:29:"sidebar-listing-orderBy-field";a:5:{i:0;s:29:"sidebar-listing-orderBy-field";i:1;s:9:"pagetitle";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:33:"sidebar-listing-listing-sortOrder";a:5:{i:0;s:33:"sidebar-listing-listing-sortOrder";i:1;s:4:"DESC";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:30:"sidebar-listing-levels-to-show";a:5:{i:0;s:30:"sidebar-listing-levels-to-show";i:1;s:1:"1";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"dropdown";}s:26:"sidebar-list-related-pages";a:5:{i:0;s:26:"sidebar-list-related-pages";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:30:"sidebar-listing-number-to-show";a:5:{i:0;s:30:"sidebar-listing-number-to-show";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:21:"content-display-video";a:5:{i:0;s:21:"content-display-video";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:23:"section-content-columns";a:5:{i:0;s:23:"section-content-columns";i:1;s:1:"2";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:34:"listing-featured-documents-title-2";a:5:{i:0;s:34:"listing-featured-documents-title-2";i:1;s:16:"Featured Listing";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:24:"section-content-column-1";a:5:{i:0;s:24:"section-content-column-1";i:1;s:74:"<h2>[*listing-featured-documents-title*]</h2> 
{{section-content-events}}";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"textarea";}s:24:"section-content-column-2";a:5:{i:0;s:24:"section-content-column-2";i:1;s:78:"<h2>[*listing-featured-documents-title-2*]</h2> 
{{section-content-articles}}";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"textarea";}s:24:"section-content-column-3";a:5:{i:0;s:24:"section-content-column-3";i:1;s:536:"<h2>[*listing-event-documents-title-3*]</h2> 
{{section-content-col3}}
<ul class="lines">
	[!Ditto? &[*listing-type*]=`[*listing-start*]` &extenders='summary' &depth='1' &tpl=`section-content-articles-tpl` &orderBy=`[*listing-orderBy-field*] [*listing-sortOrder*]` &display=`[*listing-number-to-show*]` &truncText=`Continue Reading This Article` &dateFormat=`%e %B %Y` &noResults='No Results' !]
	<li><p><strong><a href="#">More [*listing-featured-documents-title-2*] <i class="fa fa-chevron-right"></i></a></strong></p></li>
</ul>";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"textarea";}s:34:"listing-featured-documents-title-3";a:5:{i:0;s:34:"listing-featured-documents-title-3";i:1;s:16:"Featured Listing";i:2;s:0:"";i:3;s:0:"";i:4;s:4:"text";}s:22:"slider-autoplay-toggle";a:5:{i:0;s:22:"slider-autoplay-toggle";i:1;s:1:"0";i:2;s:0:"";i:3;s:0:"";i:4;s:6:"option";}s:17:"__MODxDocGroups__";s:3:"301";}<!--__MODxCacheSpliter__--><!DOCTYPE html>
<!--
 ____________________________________________________________
|                                                            |
|      DATE + 2006.11.14                                     |
| COPYRIGHT + Sigma Pi Phi 2006.  All Rights Reserved.       |
|____________________________________________________________|

-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>DEVELOPMENT 4 | Sigma Pi Phi | Boulé Foundation</title>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="index, follow, noarchive" />
<meta name="googlebot" content="noarchive" />
<meta name="viewport" content="width=device-width"> 
	<link rel="stylesheet" href="/home/assets/templates/archon/css/main.css">
<link rel="stylesheet" href="/home/assets/templates/archon/css/palette.css">

<link rel="stylesheet" href="/home/assets/templates/archon/css/plugins.css">
<link rel="stylesheet" href="/home/assets/templates/archon/css/ymp.css">


	<script src="assets/templates/archon/js/vendor/modernizr-2.8.1.min.js"></script>

<script type="text/javascript" src="/home/assets/js/jquery/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script type="text/javascript" src="/home/assets/js/jquery/js/jquery.cookie.js"></script>
<link type="text/css" href="/home/assets/js/jquery/css/custom-theme/jquery-ui-1.8.17.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="/home/assets/js/jquery/js/jquery-ui-1.8.17.custom.min.js"></script>



	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto+Slab%7CRaleway:400,700">
<!-- <link rel="icon" type="image/x-icon" href="assets/templates/archon/images/favicon.ico"> -->
<link rel="apple-touch-icon" sizes="57x57" href="assets/templates/archon/images/favicon/apple-touch-icon-57x57.png?v=yyy2mNWRBY">
<link rel="apple-touch-icon" sizes="60x60" href="assets/templates/archon/images/favicon/apple-touch-icon-60x60.png?v=yyy2mNWRBY">
<link rel="apple-touch-icon" sizes="72x72" href="assets/templates/archon/images/favicon/apple-touch-icon-72x72.png?v=yyy2mNWRBY">
<link rel="apple-touch-icon" sizes="76x76" href="assets/templates/archon/images/favicon/apple-touch-icon-76x76.png?v=yyy2mNWRBY">
<link rel="apple-touch-icon" sizes="114x114" href="assets/templates/archon/images/favicon/apple-touch-icon-114x114.png?v=yyy2mNWRBY">
<link rel="apple-touch-icon" sizes="120x120" href="assets/templates/archon/images/favicon/apple-touch-icon-120x120.png?v=yyy2mNWRBY">
<link rel="apple-touch-icon" sizes="144x144" href="assets/templates/archon/images/favicon/apple-touch-icon-144x144.png?v=yyy2mNWRBY">
<link rel="apple-touch-icon" sizes="152x152" href="assets/templates/archon/images/favicon/apple-touch-icon-152x152.png?v=yyy2mNWRBY">
<link rel="apple-touch-icon" sizes="180x180" href="assets/templates/archon/images/favicon/apple-touch-icon-180x180.png?v=yyy2mNWRBY">
<link rel="icon" type="image/png" href="assets/templates/archon/images/favicon/favicon-32x32.png?v=yyy2mNWRBY" sizes="32x32">
<link rel="icon" type="image/png" href="assets/templates/archon/images/favicon/favicon-194x194.png?v=yyy2mNWRBY" sizes="194x194">
<link rel="icon" type="image/png" href="assets/templates/archon/images/favicon/favicon-96x96.png?v=yyy2mNWRBY" sizes="96x96">
<link rel="icon" type="image/png" href="assets/templates/archon/images/favicon/android-chrome-192x192.png?v=yyy2mNWRBY" sizes="192x192">
<link rel="icon" type="image/png" href="assets/templates/archon/images/favicon/favicon-16x16.png?v=yyy2mNWRBY" sizes="16x16">
<link rel="manifest" href="assets/templates/archon/images/favicon/manifest.json?v=yyy2mNWRBY">
<link rel="shortcut icon" href="/assets/templates/archon/images/favicon/favicon.ico?v=yyy2mNWRBY">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="assets/templates/archon/images/favicon/mstile-144x144.png?v=yyy2mNWRBY">
<meta name="msapplication-config" content="assets/templates/archon/images/favicon/browserconfig.xml?v=yyy2mNWRBY">
<meta name="theme-color" content="#ffffff">
</head>

<body class="home blue-bar [!colorPalette? &palette_default=`Default` &palette_loggedin=`Default`!]">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
        <nav role="navigation">
	<div class="width relative">
		<h2 class="visuallyhidden">Site Menu</h2>
		<ul class="jetmenu Default">

<li class="secondary home quicklinks" style="display:visible; color:white;"><a class="nodup" href="javascript:void(0);">Home</a><ul class="dropdown Default">
			[!Wayfinder? &startId=`39041` &displayStart=`0` &excludeDocs='38371,38411' &level=`2` &ignoreHidden=`0` &outerTpl=`main-nav-outer-tpl`  &innerTpl=`main-nav-inner-tpl` &innerRowTpl=`main-nav-inner-row-tpl` &parentRowTpl=`main-nav-parent-row-tpl` &sortBy=`pagetitle` &sortOrder=`ASC`!]
</ul></li>

</ul>
		<ul class="jetmenu Default">



			[!Wayfinder? &startId=`1` &displayStart=`0` &excludeDocs='38371,38411'  &level=`2` &ignoreHidden=`0` &titleOfLinks=`menutitle` &outerTpl=`main-nav-outer-tpl`  &innerTpl=`main-nav-inner-tpl` &innerRowTpl=`main-nav-inner-row-tpl` &parentRowTpl=`main-nav-parent-row-tpl` &sortBy=`menuindex` &sortOrder=`ASC`!]
			<li class="searchtrigger"><a href="#" class="secondary">Search <i class="fa fa-search"></i></a><div class="headersearch"><form id="ajaxSearch_form" method="post" action="[~38631~]">
		<fieldset>
			<input type="hidden" value="oneword" name="advsearch">
			<label>
			<input id="ajaxSearch_input" class="cleardefault" type="text" placeholder="Search" value="" name="search">
			</label>
			<label>
				<input id="ajaxSearch_submit" type="submit" value="Go!" name="sub">
			</label>
		</fieldset>
		</form>
		</div></li>
			[!dowload_menu!]
			<li style="display: visible;">[!testLoggedIn!]</li>
		</ul>
	</div>
	
</nav>
			<div class="width">
				<div class="logo">
	<a href="/">
        <img src="assets/templates/archon/images/header-sphinx.png" alt="" class="sphinx">
        <img src="assets/templates/archon/images/header-logo.png" alt="Sigma Pi Phi Fraternity" class="text">
    </a>
    <em>Inspiring Our Youth to Succeed</em>
</div>

			</div>
    </header>
    <!-- large image slider content toggle start -->
		
		
		<!-- don't display slider section -->
    <!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- resource content row start -->
<div class="row grid-12">

	<section class="margin-bottom-medium">
	<div class="span-12">
		<div class="inner">
		<h1>Boulé Foundation</h1>
		<ol class="breadcrumbs" style="margin-top:-18px;"><li><span class="B_currentCrumb">Foundation</span></li></ol>
		<div>
	</div>
	</section>


<!-- holds the sidebar section -->				
	
	<!-- shows a featured image in the top of the sidebar area -->
	
					
						

		
		
		


	
	<!-- shows a fatured image in the bottom of the sidebar area -->
	
					
	
				

	
<!-- holds the content section -->
	<section class="margin-bottom-medium">
        <div class="span-12">

		<div class="inner">

	
	<div style="display:visible;">

<div class="span-3"><div class="inner float-left">
<img src="assets/images/archons/TeamerCharlesC.jpg" alt="Archon Charles C. Teamer" border="0" class="float-left"><br />
<span class="block font-small">Charles C. Teamer</span>
</div></div>
<h2>The Vision</h2>
<p>
The vision for establishing the Boul&eacute; Foundation is reflective of the deep passion for education and excellence that have been the hallmarks of Sigma Pi Phi Fraternity since its founding. The celebration of its 25th anniversary is a testament to the care and dedication of Archons throughout the Fraternity in ensuring that it remains a strong financial entity for the provision of aid and assistance that are consistent with its mission and objectives. Its creation and mission have been one of the best kept secrets in the African American community! Because of its tremendous reach of influence, it is now known and celebrated.
</p>
<p style="display:none;">
Peyton Williams, Jr.<br />
Secretary, The Boul&eacute; Foundation
</p>
<p>
The idea for the Foundation was rooted in 1927 when then Grand Sire Archon William C. McNeil first proposed such an entity. The stated purpose of the Boul&eacute; Foundation was to &quot;use and apply the whole or any part of the income therefrom and the principal thereof exclusively for charitable, religious, scientific, literary or educational purposes.&quot; Past Grand Sire Archon Robert V. Franklin, a retired Ohio Supreme Court Judge, developed the legal basis for the Foundation that was eventually chartered in Ohio. The incorporators hammered out a mission distinct from the Fraternity&#39;s Social Action Committee, from which $100,000 had been transferred to fund the new Foundation. The distinction agreed upon was that the Foundation would serve as the Grand Boul&eacute;&#39;s grant-maker to programs recommended by the Social Action Committee. The Foundation is a 501(c) (3) organization and all contributions are tax deductable.
</p>
<div class="span-3"><div class="inner float-left">
<img src="assets/images/archons/anthony-w-hall.jpg" alt="Archon Anthony W. Hall, Jr." border="0" class="float-left"><br />
<span class="block font-small">Anthony W. Hall, Jr.</span>
</div></div>
<h2>A Message From the Chairman</h2>
<p>
Dear Archons and Archousai:
</p>
<p>
The Quartern is The Boul&eacute; Foundation&rsquo;s 2013 Annual Report. The Quartern is so named because it is the first quarter-of-a-century marker for your foundation. And what a journey these twenty-five years have been. 
</p>
<p>
The Quartern is dedicated to the Archons and Archousai of Sigma Pi Phi Fraternity, who planted the seeds for The Boul&eacute; Foundation and who have since moved to higher ground. It is in the inevitable ebb and flow of life that each of us, for however much time we are given, must press on toward the mark.
</p>
<p>
&ldquo;The Boul&eacute; Foundation was created to do the greatest good by building a corpus large enough that the members of Sigma Pi Phi Fraternity could contribute, collectively and generously, to many causes,&rdquo; said Archon Manford Byrd, the Foundation&rsquo;s treasurer. And in this next quartern, we press on to the mark with clarity of purpose and a track record of success in achieving our goals. But in 2005 you rose as well to the challenge of the national emergency created by hurricanes Katrina and Rita. Your giving, which was matched by your foundation, totaled more than $600,000, and in keeping with The Boul&eacute; Foundation&rsquo;s mission, that money has been designated for scholarships to students enrolled in New Orleans and East Texas colleges, whose educational pursuits were interrupted by the storms.
</p>
<p>
The Quartern is the record of The Boul&eacute; Foundation&rsquo;s stewardship during 2005. It is divided into four quarters: The Vision, Resource Generation, Design and Implementation, and Governance and Accountability. You will find on these pages lists of your names and the cumulative level of your legacy giving. Also, several of you responded when we asked why you give to the Boul&eacute; Foundation and why other Archons and Archousai should do the likewise. Several of you generously shared as well your ideas on ways that The Boul&eacute; Foundation might broaden its mission in response to changing times. Again and again, Archons and Archousai told us that they wanted their foundation to do more outreach to African American youths and especially young black males due to their overrepresentation in the nation&rsquo;s prisons, its foster care system, homicide statistics and numbers of absentee fathers.
</p>
<p>
The Boul&eacute; Foundation&rsquo;s compelling mission is &ldquo;to restore greatness to our young people and to our community,&rdquo; Archousa Carol Goss said. And I ask that you be generous so that your foundation can stand in the gap for our children. If we don&rsquo;t, who will?
</p>
<p>
Archon Anthony W. Hall, Jr.<br />
Chairman
</p>
</div>
<h3>Resource Generation</h3>
<p>
The Boul&eacute; Foundation currently generates income from a number of sources. They include return on investment, annual assessments from the Archons, proceeds from the Foundation&#39;s Gala benefit held during the Grand Boul&eacute;, and the personal contributions of Archons and Archousai. The cumulative giving of Archons and Archousai is encouraged through the Boul&eacute; Foundation&#39;s Legacy Awards Program, which began in 1992 with a $1,000 contribution in honor of Sigma Pi Phi&#39;s Founder Henry M. Minton.
</p>
<p>
The Legacy Awards are named for men of the Fraternity who have distinguished themselves in some field of national endeavor.
</p>
<p>
The Legacy Awards are:
</p>
<ul>
	<li><strong>John B. Clemmons, Sr.</strong> ($100,000)</li>
	<li><strong>Robert V. Franklin</strong> ($75,000)</li>
	<li><strong>William Sylvester &quot;Turk&#39; Thompson</strong> ($50,000)</li>
	<li><strong>Harvey C. Russell</strong> ($25,000)</li>
	<li><strong>Percy Julian</strong> ($10,000)</li>
	<li><strong>W.E.B. DuBois</strong> ($5,000)</li>
	<li><strong>James P. Brawley</strong> ($2,500)</li>
	<li><strong>Henry M. Minton Fellow</strong> ($1,000 non-cumulative giving)</li>
</ul>
<h3>Scholarships Sponsored by the Foundation</h3>
<p>
The Boul&eacute; Foundation distributes its funds primarily to educate new generations of black leaders, support others working to protect and advance the civil rights of African Americans and to improve the quality of life for African Americans. The Boul&eacute; Foundation meets its objectives in several ways:
</p>
<h3>Boul&eacute; Achievement Scholarship Program</h3>
<p>
The Foundation annually awards twelve $1,000 achievement scholarships in each of the four years that recipients are enrolled in college. At the Centennial Grand Boul&eacute; in Philadelphia in 2004 new scholarships and grant programs were awarded and announced.
</p>
<h3>The LeBaron Taylor Scholarship</h3>
<p>
Archon Taylor was a pioneer in the entertainment industry. And upon his death at age 65 in 2000, his friend, Archon Oldfield Dukes, raised about $55,000 that was matched by the Foundation in order to provide perpetual four-year scholarships to students aspiring to careers in business, music or the arts.
</p>
<h3>The Thomas Bailey Shropshire Matching Grants Program</h3>
<p>
This program honors Past Grand Sire Archon Thomas Bailey Shropshire. He helped establish The Boul&eacute; Foundation. The matching grants program in his name supports and encourages member boul&eacute;s to energize their social action programs.
</p>
<h3>Centennial Scholarship Fund</h3>
<p>
The fund was established with $100,000 as a one-year-only program with awards made during the 2005-2006 academic year. The program was then extended for an additional academic year. The fund was a call to member boul&eacute;s to share in those funds by applying to the Foundation for matching $1,000 grants for the scholarships they awarded locally. The target group for Centennial Scholarships were young minority males who completed at least one year of college, attained a 3.0 grade point average, and whose road to graduation was made rocky by personal financial pressures. The Boul&eacute; Foundation also seeks to fulfill its commitment to equality and social justice by contributing generously and regularly to other groups whose main focus also is centered around the social issues and advancement of African Americans. Some of those groups include the NAACP, Urban League, NAACP Legal Defense Fund and History Makers.
</p>
<h3>Board of Trustees</h3>
<p>Chairman, Anthony W. Hall, Jr., Esq. (Nu)<br>
Vice Chairman, James A. B. Lockhart, Esq. (Alpha Rho)<br>
Secretary, Dr. Charles C. Teamer, Sr. (Alpha Alpha)<br>
Treasurer, Dr. Manford Byrd, Jr. (Beta)</p>

<p>Mr. Samuel Bacote III (Kappa)*<br>
Mr. Maurice L. Barksdale (Delta Mu)<br>
Mr. Henry H. Brown (Nu)<br>
Mr. Michael B. Bruno (Alpha Alpha)*<br>
Mr. Khephra Burns (Alpha Sigma)*<br>
James O. Cole, Esq., (Alpha Rho)*<br>
Mr. Wesley A. Coleman (Xi)<br>
Mr. N. John Douglas (Gamma Chi)<br>
Mr. Donald Floyd (Delta Zeta)*<br>
Mr. Leon Fulton (Alpha Alpha)<br>
Mr. Robert Holmes (Beta Kappa)<br>
Mr. Darrell B. Jackson (Beta)<br>
Mr. Robert A. McNeely (Alpha Pi)<br>
Cornell Leverette Moore, Esq. (Omicron)<br>
Mr. Dwayne M. Murray (Alpha Xi)*<br>
James E. Payne, Esq. (Delta Zeta)*<br>
Dr. Rodney J. Reed (Alpha Gamma)*<br>
Dr. Enrique Riggs (Zeta)<br>
Mr. Roy S. Roberts (Iota)<br>
Mr. Calvin E. Tyler, Jr. (Epsilon Beta)<br>
Mrs. Jacqulyn C. Shropshire (Delta Theta)<br>
Dr. Gladys Hope Franklin White (Beta Epsilon)<br>
Dr. Eddie N. Williams (Epsilon)<br>
Mrs. Nola L. Whiteman (Alpha Sigma)</p>

<p><em>*Ex Officio members who serve by virtue of their office held in Sigma Pi Phi Fraternity.</em></p>

</div>
<p class="clear">
&nbsp;
</p>

<div style="display:none;">
<p>
<strong>Chairman Emeritus</strong><br />
The Hon. Robert V. Franklin (Alpha Phi)
</p>
<p>
<strong>Trustees Emeriti</strong><br />
Dr. Matthew G. Carter (Gamma Xi)<br />
Dr. Clinton Warner, Jr. (Kappa)<br />
Dr. Huel D. Perkins (Alpha Xi)
</p>
</div>
        

	
	</div>
</div>
</section>

					
</div>			
<!-- resource content row end -->

			<!-- end showing the content, pagetitle, breadcrumb and sidebar -->
					
			<!-- 3 round images toggle start -->
            <!-- don't display spotlight row section -->
 			<!-- 3 round images toggle end -->
       </div>
    </div>
    <!-- main content body end -->	
	
	<!-- third section event and articles start -->
	<!-- don't display events/articles row section --> <div class="row" style="border-top: 10px #52B609 solid;"></div>
	<!-- third section event and articles end -->

    <footer class="width padding">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
            <section class="span-4">
    <div class="inner">
        <h2>Contact Us</h2>
        <p>
            <strong>ADDRESS</strong>
            <br> 
			260 Peachtree Street, NW<br/> Suite 1604 <br/>
Atlanta, Georgia 30303
        </p>
        <p>
            <strong>PHONE</strong>
            <br>
			<a href="tel:+14045299919">(404) 529-9919</a>
        </p>
        <p>
            <strong>EMAIL</strong>
            <br>
			<a href="mailto:info@sigmapiphi.org">info@sigmapiphi.org</a>
        </p>
    </div>
</section>
<section class="span-4">
    <div class="inner">
        <h2>Navigation</h2>
        <nav role="navigation">
			<div class="span-6">
				<ul class="lines">
	
[!Ditto? 
& parents=``
&documents=`13,38631,29,1,38621`
& IDs=``
& id=``
&depth=`1`
& dittobase=``
& language=``
& format=``
& config=``
& extenders=``
& placeholders=``
& filter=`field,No,2`
& filter=`unscheduled,No,2`
& filter=`field,criteria,mode` with pipe (“|”) delimiter for multiple filters`
& idType=``
& debug=``
& phx=``
&dateSource=`pub_date`
&dateFormat=`%B %e, %Y`
& display=`1`
& total=``
& language=``
& showPublishedOnly=``
&showInMenuOnly=`0`
& hideFolders=``
& hidePrivate=``
& seeThruUnpub=``
& where=``
& noResults=``
& removeChunk=``
&hiddenFields=`bouleJournalIssue,FeatureImage`
& start=``
& keywords=``
&orderBy=`pagetitle ASC`
& randomize=``
& save=``
&tpl=`footer-ditto-navigation-item`
& tplAlt=``
& tplFirst=`content-gallery-featured-tpl`
& tplLast=``
& tplCurrentDocument=``
!]
	
</ul>
			</div>
			<div class="span-6">
				<ul class="lines">
	<!--[ !Ditto? &documents='0' &includeDocs='1,821,13' &ignoreHidden='1' &level='0' &rowTpl=`footer-navigation-item` &sortOrder=`ASC`!]-->

[!Ditto? 
& parents=``
&documents=`26981,39101,41001,39091,40751`
& IDs=``
& id=``
&depth=`1`
& dittobase=``
& language=``
& format=``
& config=``
& extenders=``
& placeholders=``
& filter=`field,No,2`
& filter=`unscheduled,No,2`
& filter=`field,criteria,mode` with pipe (“|”) delimiter for multiple filters`
& idType=``
& debug=``
& phx=``
&dateSource=`pub_date`
&dateFormat=`%B %e, %Y`
& display=`1`
& total=``
& language=``
& showPublishedOnly=``
&showInMenuOnly=`0`
& hideFolders=``
& hidePrivate=``
& seeThruUnpub=``
& where=``
&noResults=` `
& removeChunk=``
&hiddenFields=`bouleJournalIssue,FeatureImage`
& start=``
& keywords=``
&orderBy=`pagetitle ASC`
& randomize=``
& save=``
&tpl=`footer-ditto-navigation-item`
& tplAlt=``
& tplFirst=`content-gallery-featured-tpl`
& tplLast=``
& tplCurrentDocument=``
!]







<!--[ !Wayfinder? &startId='0' &includeDocs='1,821,13' &ignoreHidden='1' &level='0' &rowTpl=`footer-navigation-item` &sortOrder=`ASC`!]-->
	
</ul>
			</div>
		</nav>
    </div>
</section>
<section class="span-4">
    <div class="inner">
        <h2>The Fraternity</h2>
		<p>
			Modeling leadership, education and civic responsibility by supporting these endeavors and by mentoring the young men who will advance the African American community in all facets of equality, mutual respect and devotion to democratic traditions.
		</p>
        <p class="align-center margin-top-large">
			<img src="assets/templates/archon/images/footer-sphinx.png" alt="" >
			<img src="assets/templates/archon/images/footer-logo.png" alt="Sigma Pi Phi Fraternity">
        </p>
    </div>
</section>
</div>
<p class="copyright">&copy;2006-2017, Sigma Pi Phi Fraternity</p>

        </div>
    </footer>

[!popup_profile_edit!]

<div class="popup_view_profile" id="popup_view_profile"></div>
        <div class="ymp_loader"></div>
        <div id="success_loader" class="ymp_loader_success"></div>
	<!--[if gte IE 9 | !IE ]><!-->
    <script src="assets/templates/archon/js/vendor/jquery-2.1.3.min.js"></script>
    <!--<![endif]-->
    <!--[if lt IE 9]><script src="assets/templates/archon/js/vendor/jquery-1.11.2.min.js"></script><![endif]-->
    <script src="assets/templates/archon/js/tinymce/tinymce.min.js"></script>
    <script src="assets/templates/archon/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="assets/templates/archon/js/plugins.js"></script>
    <script src="assets/templates/archon/js/main.js"></script>
    <script src="assets/templates/archon/js/ymp.js"></script>


</body>
</html>