a:4:{s:8:"feedinfo";a:3:{s:4:"type";s:3:"RSS";s:7:"version";s:3:"2.0";s:8:"encoding";s:5:"UTF-8";}s:4:"info";a:3:{s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:43:"http://www.sigmapiphi.org/feeds/out/?user=1";}}s:5:"title";s:40:"Items of Interest for Fraternity Members";s:11:"description";s:40:"Items of Interest for Fraternity Members";}s:5:"items";a:50:{i:0;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:22:"Accelerated Assistance";s:11:"description";s:681:"For a startup, the first few years of life are most critical. It’s a pivotal time that only a fraction of ventures have the capital, resources, and stamina to survive. Stepping in to help entrepreneurs are business incubators, which provide coaching, networking opportunities, and even affordable office space to newly minted entrepreneurs.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/entrepreneurs/2010/01/09/accelerated-assistance">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Margarette Burnette</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:78:"http://www.blackenterprise.com/entrepreneurs/2010/01/09/accelerated-assistance";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44112";}s:7:"pubdate";i:1263506403;s:7:"dc:date";i:1263506403;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:19:"Margarette Burnette";}}}}i:1;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:20:"Uncoventional Wisdom";s:11:"description";s:651:"How good a stock picker is Quintano Downes? The 13-year Wall Street veteran equities trader did such a stellar job for clients in his three years running Haven Financial Services’ equity and fixed income trading desk, that he was able to purchase a majority stake in the firm’s parent company, APB Financial Group, in 2009.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/uncoventional-wisdom">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>John Simons</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:71:"http://www.blackenterprise.com/magazine/2010/01/01/uncoventional-wisdom";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44091";}s:7:"pubdate";i:1263504606;s:7:"dc:date";i:1263504606;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:11:"John Simons";}}}}i:2;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:25:"Get a Jump on the Tax Man";s:11:"description";s:597:"As the clock strikes midnight on Jan. 1, it will also usher in the start of tax season. As everyone knows, you have until April 15th to file your state and federal income tax forms. But many tax experts recommend getting an early start to beat the mad deadline dash. 
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/get-a-jump-on-the-tax-man">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Renita Burns</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:76:"http://www.blackenterprise.com/magazine/2010/01/01/get-a-jump-on-the-tax-man";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44089";}s:7:"pubdate";i:1263504606;s:7:"dc:date";i:1263504606;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"Renita Burns";}}}}i:3;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:15:"Apocalypse Then";s:11:"description";s:649:"Life teaches its lessons not during easy times, but during difficult ones. As distance runners say: “Every race is won or lost on the uphill.” That’s why in October my entire firm, Ariel Investments, held a firm-wide retreat to reflect on the year that had just passed. In doing so, we came away with three big takeaways.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/apocalypse-then">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Mellody Hobson</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:66:"http://www.blackenterprise.com/magazine/2010/01/01/apocalypse-then";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44087";}s:7:"pubdate";i:1263504606;s:7:"dc:date";i:1263504606;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"Mellody Hobson";}}}}i:4;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:37:"The Best Offense: Defensive Investing";s:11:"description";s:536:"Why do wealthy investors and institutions pay millions of dollars in admission fees to get into hedge funds? Because hedging—or using investment strategies that reduce financial risk—works. 
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/02/the-best-offense-defensive-investing/">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Donald Jay Korn</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:85:"http://www.blackenterprise.com/magazine/2010/02/the-best-offense-defensive-investing/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44084";}s:7:"pubdate";i:1263504606;s:7:"dc:date";i:1263504606;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"Donald Jay Korn";}}}}i:5;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:15:"A Healing Touch";s:11:"description";s:975:"Growing up in the rural village of Lwala, Kenya, Milton and Frederick Ochieng’ were accustomed to traveling for two hours at a time and more than 20 miles on foot to get to the nearest hospital whenever they became ill. But when the teenage brothers witnessed a neighbor die in childbirth on her way to the hospital, the tragedy “planted the seed in our minds that we wanted to be doctors and grow up to do something for our village,” recalls Milton. A career decision spawned by a heartbreaking, avoidable incident would lead the two brothers out of Kenya to the United States and back, so their neighbor’s story need never be repeated again.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/a-healing-touch">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Tamara E. Holmes</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:66:"http://www.blackenterprise.com/magazine/2010/01/01/a-healing-touch";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44552";}s:7:"pubdate";i:1263501005;s:7:"dc:date";i:1263501005;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"Tamara E. Holmes";}}}}i:6;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:45:"Psychological Strategies Can Push Performance";s:11:"description";s:642:"“In a vastly diverse, global, and multicultural marketplace where all other factors are becoming increasingly equal, human performance serves as an enterprise’s predominant competitive differential,” says Rodney Stigall, senior recruiting manager for Florida-based Harris Corp.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/careers/2010/01/01/psychological-strategies-can-push-performance">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Marcia A. Reed-Woodard</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:95:"http://www.blackenterprise.com/careers/2010/01/01/psychological-strategies-can-push-performance";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44499";}s:7:"pubdate";i:1263501005;s:7:"dc:date";i:1263501005;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:22:"Marcia A. Reed-Woodard";}}}}i:7;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:27:"The Legal Face of Diversity";s:11:"description";s:906:"After practicing law for nearly eight years, by 2003, B. Seth Bryant had reached a crossroad. He was feeling stagnant after serving four years in the corporate department of Morrison &#038; Foerster L.L.P., a firm specializing in legal services in business and litigation. So he decided to branch out on his own. Bryant’s first attempt at entrepreneurship resulted in a seven-lawyer corporate boutique called Bryant Law Group P.C., but after Adorno &#038; Yoss, L.L.P., the largest certified minority-owned law firm in the U.S., acquired it, he returned to private law.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/the-legal-face-of-diversity">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Brittany Hutson</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:78:"http://www.blackenterprise.com/magazine/2010/01/01/the-legal-face-of-diversity";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44526";}s:7:"pubdate";i:1263501005;s:7:"dc:date";i:1263501005;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"Brittany Hutson";}}}}i:8;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:16:"Spirit of France";s:11:"description";s:844:"The Cognac region—birthplace of the esteemed brandy—resembles a page from a French novel: narrow cobblestone streets wind between 18th-century houses and spectacular churches embellished with striking sculptures and frescoes. Most cognac producers delight in providing tours, demos, and historical recreations. I’ve always associated the auburn hue of cognac with hard, throat-scorching liquor; I never noticed its distinction. The indulgences on my six-day itinerary certainly elevated my perception—and my palate.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/lifestyle/2010/01/01/spirit-of-france">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Sean Drakes</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:68:"http://www.blackenterprise.com/lifestyle/2010/01/01/spirit-of-france";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44586";}s:7:"pubdate";i:1263501005;s:7:"dc:date";i:1263501005;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:11:"Sean Drakes";}}}}i:9;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:30:"Charting The Growth Industries";s:11:"description";s:400:"This is our kickoff of a yearlong series on where the jobs are.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/careers/2010/01/01/charting-the-growth-industries">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Brittany Hutson</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:80:"http://www.blackenterprise.com/careers/2010/01/01/charting-the-growth-industries";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44488";}s:7:"pubdate";i:1263490205;s:7:"dc:date";i:1263490205;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"Brittany Hutson";}}}}i:10;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:29:"The Deal With Data Collection";s:11:"description";s:1086:"The decennial is a huge operation; it’s the largest domestic mobilization project that our country does. We collaborate across disciplines and departments within the Census Bureau. I look for individuals who have had some background training in interpersonal relations, individuals who either by design or default have been forced to work in environments where they have had to learn another technical difficulty beside their own. We have a number of people who are very deep in one area whether it’s math or demographics, however, to do a census you have to understand project management, budgeting, scheduling, and as we move through the process those disciplines actually become almost more important than the technical disciplines.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/careers/executive-suite/2010/01/01/the-deal-with-data-collection">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Annya Lott</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:95:"http://www.blackenterprise.com/careers/executive-suite/2010/01/01/the-deal-with-data-collection";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44478";}s:7:"pubdate";i:1263490205;s:7:"dc:date";i:1263490205;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:10:"Annya Lott";}}}}i:11;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:18:"Organize Your Life";s:11:"description";s:725:"Anyone using a smartphone already knows how great it is to have everything you want right in your pocket. Wouldn’t it be even better, though, if you could turn your device into a fully functional organizer, managing everything from employee lists to business inventory to customer information? Well, there are a couple of apps for that: Bento for iPhone and DDH Software’s HanDBase for smartphones.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/organize-your-life">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>David Hudson</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:69:"http://www.blackenterprise.com/magazine/2010/01/01/organize-your-life";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44465";}s:7:"pubdate";i:1263490205;s:7:"dc:date";i:1263490205;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"David Hudson";}}}}i:12;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:20:"The Past Is Prologue";s:11:"description";s:573:"How would you like to chat with Benjamin Franklin? Think it’s impossible? Not so, says Charles Palmer, executive director at Harrisburg University’s Center for Advanced Entertainment and Learning Technologies in Harrisburg, Pennsylvania. 
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/the-past-is-prologue">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Sonya A. Donaldson</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:71:"http://www.blackenterprise.com/magazine/2010/01/01/the-past-is-prologue";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44461";}s:7:"pubdate";i:1263488405;s:7:"dc:date";i:1263488405;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:18:"Sonya A. Donaldson";}}}}i:13;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:27:"From One Company to Another";s:11:"description";s:701:"When sisters Eve Lane and Holly Lane-Mixon launched Zebra Lane (www.zebralane.com) on a shoestring budget of $500 in 2003, they knew they wanted to expand their fraternity and sorority stationery and gift business beyond the retail realm into the world of business-to-business—conducting transactions between themselves and wholesalers or retailers.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/entrepreneurs/2010/01/01/from-one-company-to-another">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>BLACK ENTERPRISE MAGAZINE</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:83:"http://www.blackenterprise.com/entrepreneurs/2010/01/01/from-one-company-to-another";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44109";}s:7:"pubdate";i:1263483004;s:7:"dc:date";i:1263483004;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:25:"BLACK ENTERPRISE MAGAZINE";}}}}i:14;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:32:"Jesse Jackson Takes Aim at Banks";s:11:"description";s:511:"Jesse Jackson Sr. called for a second Stimulus Act to help main street and outlined the goals of the 13th Annual Wall Street Project Economic Summit in New York City. 
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/2010/01/14/jesse-jackson-takes-aim-at-banks">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Marcia Wade Talbert</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:83:"http://www.blackenterprise.com/business/2010/01/14/jesse-jackson-takes-aim-at-banks";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49515";}s:7:"pubdate";i:1263477604;s:7:"dc:date";i:1263477604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:19:"Marcia Wade Talbert";}}}}i:15;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:36:"Be Part of the Haitian Relief Effort";s:11:"description";s:425:"Where to send donations to help the earthquake-ravaged Port-au-Prince region of Haiti.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/2010/01/13/be-part-of-the-haitian-relief-effort">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Sonja Mack</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:87:"http://www.blackenterprise.com/business/2010/01/13/be-part-of-the-haitian-relief-effort";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49497";}s:7:"pubdate";i:1263429004;s:7:"dc:date";i:1263429004;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:10:"Sonja Mack";}}}}i:16;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:37:"Gaining Concensus on ‘Green Jobs’";s:11:"description";s:463:"During the Black Enterprise "Conversation on Energy" forum, panelists discussed strategies for creating and getting green jobs. 
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/blogs/2010/01/13/gaining-concensus-on-green-jobs">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Andre Williams</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:79:"http://www.blackenterprise.com/blogs/2010/01/13/gaining-concensus-on-green-jobs";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49482";}s:7:"pubdate";i:1263403804;s:7:"dc:date";i:1263403804;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"Andre Williams";}}}}i:17;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:38:"Getting the Green Conversation Started";s:11:"description";s:550:"Black Enterprise gathered top-level executives from businesses and organizations within the energy industry in Washington Tuesday to discuss how African Americans can participate in the green energy economy. 
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/blogs/2010/01/12/getting-the-green-conversation-started">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Andre Williams</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:86:"http://www.blackenterprise.com/blogs/2010/01/12/getting-the-green-conversation-started";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49470";}s:7:"pubdate";i:1263342603;s:7:"dc:date";i:1263342603;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:14:"Andre Williams";}}}}i:18;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:37:"Energy Experts to Blacks: Think Green";s:11:"description";s:534:"If African Americans hope to not only participate in the emerging green economy but also play a significant role in getting and creating jobs in that sector, they need to start talking.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/2010/01/12/energy-experts-to-blacks-you-must-think-green">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Joyce Jones</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:96:"http://www.blackenterprise.com/business/2010/01/12/energy-experts-to-blacks-you-must-think-green";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49458";}s:7:"pubdate";i:1263339004;s:7:"dc:date";i:1263339004;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:11:"Joyce Jones";}}}}i:19;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:41:"Met Museum Pays Tribute to Eunice Johnson";s:11:"description";s:569:"On Monday some of the biggest names in fashion, publishing, and politics paid tribute to Eunice W. Johnson, fashion icon, and a founder of Ebony magazine at New York’s Metropolitan Museum of Art in New York.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/lifestyle/2010/01/12/met-museum-pays-tribute-to-eunice-johnson">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Deborah Creighton Skinner</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:93:"http://www.blackenterprise.com/lifestyle/2010/01/12/met-museum-pays-tribute-to-eunice-johnson";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49431";}s:7:"pubdate";i:1263335405;s:7:"dc:date";i:1263335405;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:25:"Deborah Creighton Skinner";}}}}i:20;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:45:"Andersons Complete Year of ‘Buying Black’";s:11:"description";s:531:"The Empowerment Experiment's Anderson family discuss the successes that kept them empowered, the struggles that challenged them, and why they won’t be continuing the experiment.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/lifestyle/2010/01/12/andersons-complete-year-of-buying-black">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Marcia Wade Talbert</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:91:"http://www.blackenterprise.com/lifestyle/2010/01/12/andersons-complete-year-of-buying-black";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49422";}s:7:"pubdate";i:1263335405;s:7:"dc:date";i:1263335405;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:19:"Marcia Wade Talbert";}}}}i:21;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:42:"The Homebuyer’s Toolkit: Condo or Co-Op?";s:11:"description";s:683:"If shoveling snow, mowing the lawn, and general maintenance is not up your alley then purchasing a condo or a cooperative (co-op) might be a better option for you. They may appear to be one in the same, but there are some striking differences that you should make yourself aware of before you make your [...]
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/wealth-for-life/homeownership/2010/01/11/the-homebuyer%e2%80%99s-toolkit-condo-or-co-op">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>LaToya M. Smith</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:118:"http://www.blackenterprise.com/wealth-for-life/homeownership/2010/01/11/the-homebuyer%e2%80%99s-toolkit-condo-or-co-op";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49381";}s:7:"pubdate";i:1263243604;s:7:"dc:date";i:1263243604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"LaToya M. Smith";}}}}i:22;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:56:"Congressional Leaders Begin Reconciling Healthcare Bills";s:11:"description";s:682:"When members of the U.S. House of Representatives return to Washington this week, they will fully immerse themselves in the daunting task of reconciling the House and Senate healthcare reform bills into a final piece of legislation that President Barack Obama can sign into law before his first state-of-the-union address.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/2010/01/11/congressional-leaders-begin-reconciling-healthcare-bills">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Joyce Jones</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:107:"http://www.blackenterprise.com/business/2010/01/11/congressional-leaders-begin-reconciling-healthcare-bills";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49388";}s:7:"pubdate";i:1263243604;s:7:"dc:date";i:1263243604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:11:"Joyce Jones";}}}}i:23;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:10:"Letting Go";s:11:"description";s:889:"When corporate downsizing resulted in a job change from manufacturing supervisor to security guard, Mary Parker didn’t complain. Instead, she remained upbeat, embraced the new challenge, and quickly earned the moniker “Officer Friendly” in the building where she worked. N ow as president and CEO of ALL(n)1 Security Services (www.alln1security.com; 404-691-4915), a full-service security firm in Atlanta, Parker imparts that same commitment to compassionate security to the people she employs: eight full-time office staff and nearly 200 people in the field.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/entrepreneurs/2010/01/01/letting-go">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Maya Payne Smart</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:66:"http://www.blackenterprise.com/entrepreneurs/2010/01/01/letting-go";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44102";}s:7:"pubdate";i:1263227405;s:7:"dc:date";i:1263227405;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:16:"Maya Payne Smart";}}}}i:24;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:38:"Money Matters: Should I Ditch My Home?";s:11:"description";s:464:"I don’t have equity in the home I bought three years ago. When is it time to ditch a house, even if you are current on your mortgage payments?
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/qa-money-matters">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>John Simons</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:67:"http://www.blackenterprise.com/magazine/2010/01/01/qa-money-matters";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44098";}s:7:"pubdate";i:1263225603;s:7:"dc:date";i:1263225603;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:11:"John Simons";}}}}i:25;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:21:"5 Ways to Reduce Debt";s:11:"description";s:1122:"In 2002, Kristen Tate decided that debt free was the way to be. With a debt load of $28,000 that included student loans, credit cards, and a car note, Tate, who earned $65,000 as an account manager for a global public relations firm, began to pay triple the minimum payment of $200 each month. Tate eventually withdrew money from her savings account and paid off the balance—by 2008, she was completely debt free. Now a self-employed communications consultant, the 29-year-old says she met her goals by drawing on the values she was raised with. “My parents and grandparents stressed the importance of being financially responsible. Rather than buying everything I wanted, I lived frugally and paid down my debt. I wanted the peace of mind of knowing I didn’t owe anyone.” 		       — 

    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/blog/2010/02/15/5-ways-to-reduce-debt/">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Leslie E. Royal</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:69:"http://www.blackenterprise.com/blog/2010/02/15/5-ways-to-reduce-debt/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=44100";}s:7:"pubdate";i:1263225603;s:7:"dc:date";i:1263225603;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"Leslie E. Royal";}}}}i:26;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:29:"Backtalk with Anika Noni Rose";s:11:"description";s:477:"From a self-proclaimed dreamer to a world-renowned Dreamgirl, actress and singer Anika Noni Rose has seen many of her dreams come true.

    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/backtalk/2010/01/07/backtalk-with-anika-noni-rose">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Ed Gordon</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:89:"http://www.blackenterprise.com/magazine/backtalk/2010/01/07/backtalk-with-anika-noni-rose";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=47400";}s:7:"pubdate";i:1262995203;s:7:"dc:date";i:1262995203;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:9:"Ed Gordon";}}}}i:27;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:18:"The Paths to Power";s:11:"description";s:816:"It all began in 1980, when an 19-year-old Ursula Burns walked through the doors of Xerox Corp. to work as a summer intern. Over the next three decades she would put an indelible mark on the global leviathan. In July 2009, her prowess and performance led to a headline-grabbing milestone: Burns was installed as Xerox’s chief executive officer, becoming the first African American woman to take the helm of one of the nation’s largest publicly traded companies.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/about-this-issue/2010/01/07/the-paths-to-power">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>BLACK ENTERPRISE Editors</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:86:"http://www.blackenterprise.com/magazine/about-this-issue/2010/01/07/the-paths-to-power";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=47347";}s:7:"pubdate";i:1262993403;s:7:"dc:date";i:1262993403;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:24:"BLACK ENTERPRISE Editors";}}}}i:28;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:56:"Financial Illiteracy Will Make Us a Permanent Underclass";s:11:"description";s:963:"These days, I’ve become increasingly alarmed by the growing pattern of recklessness and neglect that seems to govern the management of our personal finances. Recent actions—or I should say inaction—by large numbers of African Americans have prompted me to suggest taking a bold step and declare a state of financial emergency. I make this assertion not for dramatic effect but to bring attention to the need for us to take corrective measures. If not, this self-destructive behavior will continue to threaten the future of our families for generations to come.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/executive-memo/2010/01/07/financial-illiteracy-will-make-us-a-permanent-underclass">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Earl &quot;Butch&quot; Graves Jr.</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:122:"http://www.blackenterprise.com/magazine/executive-memo/2010/01/07/financial-illiteracy-will-make-us-a-permanent-underclass";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=47951";}s:7:"pubdate";i:1262993403;s:7:"dc:date";i:1262993403;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:33:"Earl &quot;Butch&quot; Graves Jr.";}}}}i:29;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:31:"Taking the Reins of Opportunity";s:11:"description";s:606:"Whether you view the fact that in 2010 we are still celebrating major firsts in African American history as good news or bad is a matter of perspective. But the ascension of Ursula Burns to CEO of Xerox Corp. was unequivocally, spectacularly good!
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/publishers-page/2010/01/07/taking-the-reins-of-opportunity">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Earl G. Graves, Sr.</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:98:"http://www.blackenterprise.com/magazine/publishers-page/2010/01/07/taking-the-reins-of-opportunity";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=47383";}s:7:"pubdate";i:1262993403;s:7:"dc:date";i:1262993403;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:19:"Earl G. Graves, Sr.";}}}}i:30;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:51:"Black Broadcasting Network Launches on Verizon Fios";s:11:"description";s:479:"The Black Broadcasting Network has inked its first distribution deal with Verizon Fios TV Video on Demand. 

    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/business-news/2010/01/08/black-broadcasting-network-launches-on-verizon-fios">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Renita Burns</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:116:"http://www.blackenterprise.com/business/business-news/2010/01/08/black-broadcasting-network-launches-on-verizon-fios";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48902";}s:7:"pubdate";i:1262991603;s:7:"dc:date";i:1262991603;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"Renita Burns";}}}}i:31;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:34:"75 Most Powerful Women In Business";s:11:"description";s:879:"What does it take to be named one of Black Enterprise’s 75 Most Powerful Women in Business? To identify our senior corporate executives and leading entrepreneurs—women responsible for developing product lines, positioning brands, operating core business areas, generating revenues, and with profit &#038; loss oversight at the highest levels—our editorial and research teams pored over hundreds of biographies and résumés, as well as conducted interviews with potential candidates and leading trade associations for several months.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/08/75-most-powerful-women-in-business">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Sonia Alleyne</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:85:"http://www.blackenterprise.com/magazine/2010/01/08/75-most-powerful-women-in-business";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=47231";}s:7:"pubdate";i:1262986203;s:7:"dc:date";i:1262986203;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:13:"Sonia Alleyne";}}}}i:32;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:20:"Unduplicated Success";s:11:"description";s:729:"In July 2009, Ursula M. Burns was named CEO of Xerox Corp., becoming not only the first African American woman to run one of the largest publicly traded companies, but also being placed among the world’s most influential chief executives, male or female. This event may have signaled the beginning of a new era for black female representation in Corporate America.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/events/women-of-power/women-of-power-articles/2010/01/08/unduplicated-success">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Sonia Alleyne</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:108:"http://www.blackenterprise.com/events/women-of-power/women-of-power-articles/2010/01/08/unduplicated-success";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48762";}s:7:"pubdate";i:1262984405;s:7:"dc:date";i:1262984405;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:13:"Sonia Alleyne";}}}}i:33;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:32:"Black Unemployment Spikes to 16%";s:11:"description";s:531:"As the beleaguered economy hobbles along, employers shed 85,000 jobs last month holding the unemployment rate steady compared to November, but shooting up from 7.4% year-over-year. 
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/business-news/2010/01/08/black-unemployment-spikes-to-16">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Renita Burns</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:96:"http://www.blackenterprise.com/business/business-news/2010/01/08/black-unemployment-spikes-to-16";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49194";}s:7:"pubdate";i:1262980803;s:7:"dc:date";i:1262980803;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"Renita Burns";}}}}i:34;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:28:"New Attitude, New Generation";s:11:"description";s:906:"This issue signals the beginning of our 40th anniversary—and here, at black enterprise, we have a new attitude. Of course, we take great pride in our four decades of unmatched coverage of black business. The energy that crackles through each office and cubicle, however, comes from our renewed commitment to serve you, our audience, whether you’re getting the latest scoop from the magazine, our Website, a must-attend event, or an episode of one of our television shows,: Black Enterprise Business Report or Our World with Black Enterprise.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/about-this-issue/2010/01/01/new-attitude-new-generation">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>BLACK ENTERPRISE Editors</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:95:"http://www.blackenterprise.com/magazine/about-this-issue/2010/01/01/new-attitude-new-generation";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=43953";}s:7:"pubdate";i:1262916003;s:7:"dc:date";i:1262916003;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:24:"BLACK ENTERPRISE Editors";}}}}i:35;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:32:"Golf Pioneer William Powell Dies";s:11:"description";s:550:"William Powell, an African American visionary who designed, built, owned and operated his own golf course, died on Dec. 31 at Aultman Hospital in Canton, Ohio after suffering complications from a stroke.  He was 93.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/2010/01/07/golf-pioneer-william-powell-dies">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Sonja Mack</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:83:"http://www.blackenterprise.com/business/2010/01/07/golf-pioneer-william-powell-dies";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49070";}s:7:"pubdate";i:1262912403;s:7:"dc:date";i:1262912403;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:10:"Sonja Mack";}}}}i:36;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:23:"Boost Your Credit Score";s:11:"description";s:747:"Having the ability to obtain an affordable home loan is only one example of how it pays to have good credit. Your insurance premiums will also be lower if you have a high credit score. “An excellent credit score is 750 or higher; 680 to 740 is good. The lower 600s would be considered fair. Anything below that is poor or subprime,” says Ben Woolsey, director of marketing and consumer research for CreditCards.com.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/boost-your-credit-score-3">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Lauren Lea</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:76:"http://www.blackenterprise.com/magazine/2010/01/01/boost-your-credit-score-3";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48776";}s:7:"pubdate";i:1262892604;s:7:"dc:date";i:1262892604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:10:"Lauren Lea";}}}}i:37;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:27:"Remembering Percy E. Sutton";s:11:"description";s:557:""Mentor, media mogul, entrepreneur, friend, WWII fighter, “The Chairman,” or just simply Dad or Uncle Jimmy are all ways in which family and friends remembered Percy Ellis Sutton during his funeral service on Wednesday.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/2010/01/06/remembering-percy-e-sutton">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>LaToya M. Smith</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:77:"http://www.blackenterprise.com/business/2010/01/06/remembering-percy-e-sutton";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49014";}s:7:"pubdate";i:1262824204;s:7:"dc:date";i:1262824204;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"LaToya M. Smith";}}}}i:38;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:38:"BE Next: Making the January 2010 Cover";s:11:"description";s:387:"Behind the scenes at a BE Next cover shoot.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/blogs/2010/01/06/be-next-making-the-january-2010-cover-2">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>LaToya M. Smith</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:87:"http://www.blackenterprise.com/blogs/2010/01/06/be-next-making-the-january-2010-cover-2";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=49008";}s:7:"pubdate";i:1262820604;s:7:"dc:date";i:1262820604;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"LaToya M. Smith";}}}}i:39;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:41:"Help! My Dad Has a Credit Card in My Name";s:11:"description";s:1109:"When “Jonathan” was 14-years-old, his Dad opened a few credit cards and acquired a few utilities in his name—unbeknownst to him. Fast-forward four years and Jon, now with enough money saved for a down payment on a vehicle, is denied financing from a used car dealership. Frustrated but not discouraged he tries another—rejected again. Annoyed but still not discouraged he tries another—rejected a third time. But this time Jon asked why. The car salesman explained to Jon that he pulled his 25-page credit report and found that his credit was terrible. You see, Dad never paid any of the bills he racked up in Jon’s name and now lawyers, bill collectors, and a sheriff with a warrant are coming after Jon—while Dad cannot be found.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/wealth-for-life/credit-debt/2010/01/06/help-my-mom-has-a-credit-card-in-my-name">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Renita Burns</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:110:"http://www.blackenterprise.com/wealth-for-life/credit-debt/2010/01/06/help-my-mom-has-a-credit-card-in-my-name";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48980";}s:7:"pubdate";i:1262813404;s:7:"dc:date";i:1262813404;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"Renita Burns";}}}}i:40;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:42:"Poll: Christmas Spending Stayed Under $200";s:11:"description";s:569:"‘Twas the season to ... watch your spending. The yuletide spirit may have been in the air this holiday season, but shoppers were cautious about how they doled out the dollars according to a recent BlackEnterprise.com poll.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/2010/01/06/poll-christmas-spending-stayed-under-200">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Renita Burns</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:91:"http://www.blackenterprise.com/business/2010/01/06/poll-christmas-spending-stayed-under-200";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=47419";}s:7:"pubdate";i:1262809803;s:7:"dc:date";i:1262809803;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"Renita Burns";}}}}i:41;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:30:"Tiger Woods: Golfer or Inmate?";s:11:"description";s:524:"Was "Vanity Fair" magazine fair in its portrayal of Tiger Woods, or did the magazine go too far by portraying him as someone more comfortable in a prison yard than on a golf course?
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/blogs/2010/01/06/tigers-wood-golfer-or-inmate">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Deborah Creighton Skinner</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:76:"http://www.blackenterprise.com/blogs/2010/01/06/tigers-wood-golfer-or-inmate";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48850";}s:7:"pubdate";i:1262788204;s:7:"dc:date";i:1262788204;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:25:"Deborah Creighton Skinner";}}}}i:42;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:25:"Backtalk with Wyclef Jean";s:11:"description";s:676:"Growing up in Croix-de-Bouquets, Haiti, until the age of 9, Wyclef Jean’s music often sheds light on what it was like living on the poverty-stricken and war-torn island. Jean’s eclectic sound and pensive lyrics have helped raise awareness about the impoverished Caribbean nation, yet at the same time they’ve inspired hope for its youth.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/backtalk-with-wyclef-jean">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>LaToya M. Smith</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:76:"http://www.blackenterprise.com/magazine/2010/01/01/backtalk-with-wyclef-jean";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48207";}s:7:"pubdate";i:1262705404;s:7:"dc:date";i:1262705404;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:15:"LaToya M. Smith";}}}}i:43;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:36:"Ebony Co-Founder Eunice Johnson Dies";s:11:"description";s:485:"Eunice W. Johnson, widow of John H. Johnson and co-founder of Johnson Publishing Co., died Sunday. She was 93.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/business/business-news/2010/01/04/fashion-icon-ebony-co-founder-eunice-johnson-dies">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Marcia Wade Talbert</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:114:"http://www.blackenterprise.com/business/business-news/2010/01/04/fashion-icon-ebony-co-founder-eunice-johnson-dies";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48663";}s:7:"pubdate";i:1262647803;s:7:"dc:date";i:1262647803;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:19:"Marcia Wade Talbert";}}}}i:44;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:38:"BE Next: Making the January 2010 Cover";s:11:"description";s:532:"With an inside look at the January 2010 cover shoot, Black Enterprise introduces you to its latest franchise – BE Next.  Find out who they are, what they do, and how you can BE Next.

    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/television/features/2010/01/01/be-next-making-the-january-2010-cover">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>jahdy</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:99:"http://www.blackenterprise.com/television/features/2010/01/01/be-next-making-the-january-2010-cover";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48360";}s:7:"pubdate";i:1262363405;s:7:"dc:date";i:1262363405;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:5:"jahdy";}}}}i:45;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:59:"Making the Cover – BE Next: Young, Fearless Entrepreneurs";s:11:"description";s:352:"<p>
    	<span>
        <a href="http://www.blackenterprise.com/television/features/2010/01/01/making-the-cover-be-next-young-fearless-entrepreneurs-2">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>jahdy</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:117:"http://www.blackenterprise.com/television/features/2010/01/01/making-the-cover-be-next-young-fearless-entrepreneurs-2";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48356";}s:7:"pubdate";i:1262363405;s:7:"dc:date";i:1262363405;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:5:"jahdy";}}}}i:46;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:13:"Paul Williams";s:11:"description";s:485:"Paul Williams, Executive Director, Dormitory Authority State of New York.

    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/television/business-report/power-player-black-enterprise-business-report-tv-video/2009/12/06/paul-williams">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>BLACK ENTERPRISE Broadcast Group</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:137:"http://www.blackenterprise.com/television/business-report/power-player-black-enterprise-business-report-tv-video/2009/12/06/paul-williams";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=48317";}s:7:"pubdate";i:1262287803;s:7:"dc:date";i:1262287803;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:32:"BLACK ENTERPRISE Broadcast Group";}}}}i:47;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:46:"Obama on the Defense Against Black Politicians";s:11:"description";s:558:"Members of Congress and political pundits have grown louder in their frustration with the White House, asserting that President Barack Obama has not been doing enough to help African Americans.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/politics/politics-news/2009/12/30/obama-on-the-defense-against-black-politicians">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Renita Burns</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:111:"http://www.blackenterprise.com/politics/politics-news/2009/12/30/obama-on-the-defense-against-black-politicians";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=47718";}s:7:"pubdate";i:1262217605;s:7:"dc:date";i:1262217605;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"Renita Burns";}}}}i:48;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:10:"High Hopes";s:11:"description";s:936:"Jason Gray has big plans. He wants to be the first of his five siblings to graduate from college and start a business. At 25, he’s counting the days until he graduates from John Jay College of Criminal Justice in New York City, where he has about a year before receiving a degree in criminal justice. His plans include launching a career in law enforcement, starting a business, and buying his first home. He’s not short on dreams. What Gray needs is patience. “I’m trying to slow down. I have school, my job, and I want to get the business going,” says Gray, who is single and lives in Queens, New York.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2009/12/30/high-hopes-3">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Sheryl Nance Nash</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:63:"http://www.blackenterprise.com/magazine/2009/12/30/high-hopes-3";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=43961";}s:7:"pubdate";i:1262212204;s:7:"dc:date";i:1262212204;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:17:"Sheryl Nance Nash";}}}}i:49;O:14:"SimplePie_Item":1:{s:4:"data";a:7:{s:5:"title";s:22:"Living the Frugal Life";s:11:"description";s:701:"As a social worker, Philadelphia native Chad Dion Lassiter is used to encouraging people to take risks. He often pushes people to explore new ways to overcome obstacles in both their personal and professional lives. Lassiter’s view of risk-taking, however, doesn’t extend to how he handles money. He and his wife, Wanda, are careful and thrifty in their spending habits.
    
      <p>
    	<span>
        <a href="http://www.blackenterprise.com/magazine/2010/01/01/living-the-frugal-life">Originally</a>&nbsp; 
                    from <a href="http://www.blackenterprise.com">BLACK ENTERPRISE</a></span>
            
                    by <span>Glenn Townes</span>
        	</span>
        </p>";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:73:"http://www.blackenterprise.com/magazine/2010/01/01/living-the-frugal-life";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:39:"http://www.blackenterprise.com/?p=43937";}s:7:"pubdate";i:1262212204;s:7:"dc:date";i:1262212204;s:6:"author";a:1:{i:0;O:16:"SimplePie_Author":3:{s:4:"name";N;s:4:"link";N;s:5:"email";s:12:"Glenn Townes";}}}}}s:3:"url";s:68:"https://www.sigmapiphi.org/feeds/out/rss.php?feedtag=BlackEnterprise";}