<?php
require_once('../includes/admin/access.php');
require_once('../includes/admin/header.php');
?>

<h3>Sitemap Generator</h3>

Click <a href="admin_sitemap.php?action=renew" onClick="return confirm('<?php echo T_SURE;?>?');">Sitemap <?php echo round((time()-filemtime(SITEMAP_FILE))/60,1);?> <?php echo T_MINUTES?></a>


<?php 
if (cG("action")=="renew") {
	$sitemap=generateSitemap();
	echo "<br/><textarea cols=60 rows=30>$sitemap</textarea>";
}
?>

<br />
<br />
<a target="_blank" href="<?php echo SITE_URL;?>/sitemap.xml.gz">open Sitemap</a>



<?php
require_once('../includes/admin/footer.php');
?>
