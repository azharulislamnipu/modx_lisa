<?php
/*
Template Name: Full Width Page, No Sidebar
*/
?>

<?php get_header() ?>

	<div id="content" class="padding-top-minus-20">
		<div class="padder width padding-top-none-20">
<div class="width padding-top-none-20 divider">
		<?php do_action( 'sigma_before_blog_page' ) ?>
  <div class="row grid-12">
	  <div class="page" id="blog-page">
		    <div class="span-12"> 
		     <!-- <div class="span-12">  -->

		 <div class="inner">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php // Check that the user is allowed to view the content
					  $can_view_content = can_view_content(); ?>
				<?php if ( $can_view_content ) : ?>
					<h1 class="pagetitle"><?php the_title(); ?></h1>
					<div class="post" id="post-<?php the_ID(); ?>">
						<div class="contentBlock">
							<div class="entry">
								<?php the_content( __( '<p class="serif">Read the rest of this page &rarr;</p>', 'archon-network' ) ); ?>
								<?php wp_link_pages( array( 'before' => __( '<p><strong>Pages:</strong> ', 'archon-network' ), 'after' => '</p>', 'next_or_number' => 'number')); ?>
								<?php edit_post_link( __( 'Edit this entry.', 'archon-network' ), '<p>', '</p>'); ?>
							</div>
						</div>
					</div>

				<?php else: ?>
					<h2 class="pagetitle"><?php _e( 'Unauthorized', 'archon-network' ); ?></h2>
					<div class="post" id="post-<?php the_ID(); ?>">
						<div class="contentBlock">
							<div class="entry">
								<p><?php _e( 'The information on this page is available for members only. Please log in to view this content.', 'archon-network' ); ?></p>
							</div>
						</div>
					</div>

				<?php endif; ?>


			<?php endwhile; endif; ?>

			<?php if ( $can_view_content ) : ?>
				<?php //comments_template(); ?>
			<?php endif; ?>
		</div><!--inner-->
	</div><!--span-12-->


		</div><!-- .grid-12 -->

		<?php do_action( 'sigma_after_blog_page' ) ?>
	
		</div><!-- .divider -->
		
	</div><!-- .padder -->
	
    <!-- 3 round images -->
    <div class="row grid-12 margin-top-medium padding">
			<div class="width padding">
		<div class="round">
		
	<?php if ( is_active_sidebar( 'three_round_images' ) ) : ?>
		
		<?php dynamic_sidebar( 'three_round_images' ); ?>
		 
		<?php endif; ?>
		</div><!-- #width padding -->
	</div><!-- #round -->
		</div><!-- #row grid 12 round images  -->

    </div><!-- #content -->
	

<?php get_footer(); ?>
