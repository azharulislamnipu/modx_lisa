<?php require_once('../webassist/framework/framework.php'); ?>
<?php require_once('../webassist/framework/library.php'); ?>
<?php require_once('../Connections/sigma_modx2015_i.php'); ?>
<?php require_once("../webassist/database_management/wada_search.php"); ?>
<?php
//WA Database Search (Copyright 2005, WebAssist.com)
//Recordset: rsArchon;
//Searchpage: directory-search.php;
//Form: form1;
$WADbSearch1_DefaultWhere = "";
if (!session_id()) session_start();
if ((isset($_POST["WADbSearch1"])) && ($_POST["WADbSearch1"] != "")) {
  $WADbSearch1 = new FilterDef;
  $WADbSearch1->initializeQueryBuilder("MYSQL","1");
  //keyword array declarations
  $KeyArr0 = array("REGIONNAME", "BOULENAME", "FIRSTNAME", "MIDDLEINITIAL", "LASTNAME", "SUFFIX", "DESIGNATIONLST", "ORGNAME", "CITY", "STATECD", "BIO", "OCCUPATIONID", "OCCUPATION_TYPE", "TITLEID", "JOB_TITLE", "COMPANYNAME", "COMPANYCITY", "COMPANYSTCD", "JOB_DESCRIPTION", "DEGREE", "INSTITUTION", "MAJOR", "MINOR1", "CPOSITION", "JOINDATE", "COMMITTEESTATUSSTT");

  //comparison list additions
  $WADbSearch1->addComparisonFromEdit("REGIONNAME","regionSearch","AND","=",0);
  $WADbSearch1->addComparisonFromEdit("BOULENAME","bouleSearch","OR","=",0);
  $WADbSearch1->addComparisonFromEdit("LASTNAME","lnameSearch","OR","=",0);
  $WADbSearch1->addComparisonFromEdit("STATECD","stateSearch","OR","=",0);
  $WADbSearch1->keywordComparison($KeyArr0,"".((isset($_POST["searchAdvanced"]))?$_POST["searchAdvanced"]:"")  ."","OR","Includes",",%20","%20","%22","%22",0);

  //save the query in a session variable
  if (1 == 1) {
    $_SESSION["WADbSearch1_directoryresults"]=$WADbSearch1->whereClause;
  }
}
else     {
  $WADbSearch1 = new FilterDef;
  $WADbSearch1->initializeQueryBuilder("MYSQL","1");
  //get the filter definition from a session variable
  if (1 == 1)     {
    if (isset($_SESSION["WADbSearch1_directoryresults"]) && $_SESSION["WADbSearch1_directoryresults"] != "")     {
      $WADbSearch1->whereClause = $_SESSION["WADbSearch1_directoryresults"];
    }
    else     {
      $WADbSearch1->whereClause = $WADbSearch1_DefaultWhere;
    }
  }
  else     {
    $WADbSearch1->whereClause = $WADbSearch1_DefaultWhere;
  }
}
$WADbSearch1->whereClause = str_replace("\\''", "''", $WADbSearch1->whereClause);
$WADbSearch1whereClause = '';
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx2015_i, $sigma_modx2015_i);
$query_rsArchon = "SELECT CUSTOMERID, WEB_ID, REGIONNAME, REGIONCD, BOULENAME, CHAPTERID, BOULECITY, BOULESTATECD, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, HOMEPHONE, SUFFIX, DESIGNATIONLST, EMAIL, WORK_EMAIL, WEB_EMAIL, LASTUPDATED, UPDATEDBY, SKILLCDLST, OCCUPATIONCD, JOBTITLE, ORGNAME, CITY, STATECD, ZIP, BIO, SPOUSENAME, OCCUPATIONID, OCCUPATION_TYPE, TITLEID, JOB_TITLE, COMPANYNAME, COMPANYCITY, COMPANYSTCD, JOB_DESCRIPTION, DEGREE, INSTITUTION, MAJOR, MINOR1, MINOR2, `COMMENT`, OFFICERID, CHAPTERCD, CPOSITION, JOINDATE, COMMITTEESTATUSSTT, IMAGE FROM vw_search2";
setQueryBuilderSource($query_rsArchon,$WADbSearch1,false);
$rsArchon = mysql_query($query_rsArchon, $sigma_modx2015_i) or die(mysql_error());
$row_rsArchon = mysql_fetch_assoc($rsArchon);
$totalRows_rsArchon = mysql_num_rows($rsArchon);

mysql_select_db($database_sigma_modx2015_i, $sigma_modx2015_i);
$query_rsArchons = "SELECT CUSTOMERID, WEB_ID, REGIONNAME, REGIONCD, BOULENAME, CHAPTERID, BOULECITY, BOULESTATECD, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, HOMEPHONE, SUFFIX, DESIGNATIONLST, EMAIL, WORK_EMAIL, WEB_EMAIL, LASTUPDATED, UPDATEDBY, SKILLCDLST, OCCUPATIONCD, JOBTITLE, ORGNAME, CITY, STATECD, ZIP, BIO, SPOUSENAME, OCCUPATIONID, OCCUPATION_TYPE, TITLEID, JOB_TITLE, COMPANYNAME, COMPANYCITY, COMPANYSTCD, JOB_DESCRIPTION, DEGREE, INSTITUTION, MAJOR, MINOR1, MINOR2, `COMMENT`, OFFICERID, CHAPTERCD, CPOSITION, JOINDATE, COMMITTEESTATUSSTT, IMAGE FROM vw_search2";
setQueryBuilderSource($query_rsArchons,$WADbSearch1,false);
$rsArchons = mysql_query($query_rsArchons, $sigma_modx2015_i) or die(mysql_error());
$row_rsArchons = mysql_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysql_num_rows($rsArchons);

if("" == ""){
	$WA_spp_redesign_archon_1451841775342_Content_Empty = "";
	$WA_spp_redesign_archon_1451841775342_Content = new WA_Include(__FILE__); 
	$WA_spp_redesign_archon_1451841775342_Template = new WA_Include("../Templates{$WA_spp_redesign_archon_1451841775342_Content_Empty}/spp-redesign-archon.dwt.php");
	require($WA_spp_redesign_archon_1451841775342_Template->BaseName);
	$WA_spp_redesign_archon_1451841775342_Template->Initialize(true);
}
?>
<!DOCTYPE html>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"><!-- InstanceBegin template="/Templates/Themes/spp-redesign-archon_fw_theme.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" --><!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="pagetitle" --><!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="content" --><!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
if(isset($WA_spp_redesign_archon_1451841775342_Template)){
	$WA_spp_redesign_archon_1451841775342_Content->Initialize();
	$WA_spp_redesign_archon_1451841775342_Template->ReplaceTemplateRegions($WA_spp_redesign_archon_1451841775342_Content);
	print($WA_spp_redesign_archon_1451841775342_Template->Content);
}

mysql_free_result($rsArchon);

mysql_free_result($rsArchons);
?>
