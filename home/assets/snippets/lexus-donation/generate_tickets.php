<!DOCTYPE html> 
<html> 
    
<head> 
<title>Lexus Drawing Tickets</title> 

<style>

@media print, screen {
    body {
        font: 14px arial, sans-serif;
        color: #000;
        background: #fff;
    }

    p {
        padding: 10px;
        border-top: 1px solid #bbb;
        page-break-inside: avoid;
    }
}
    
</style>

</head>

<body>
        

<?php

/*
 * Output draw tickets from the contents of spp_lexus_drawing
 * Author: Pat Heard {http://fullahead.org}
 */

// Database details
$host = "localhost";
$user = "sigma-admin";
$password = "beaure";
$database = "modx_dev";

// Attempt to open a database connection
$con = mysql_connect($host, $user, $password); 
if (!$con) {
    die('Could not connect to database for draw tickets printing: ' . mysql_error());
}

// Select the MODX database
if(!mysql_select_db($database)){
    die('Could not select "' . $database . '" database: ' . mysql_error()); 
}

// Update any delegates that have lost their eligibility
$result = mysql_query("
    SELECT
        draw_numbers_start,
        draw_numbers_end,
        FIRSTNAME,
        LASTNAME,
        ADDRESS1,
        CITY,
        STATECD,
        ZIP,
        HOMEPHONE
    FROM 
        spp_lexus_drawing   
    ORDER BY
        draw_numbers_start,
        draw_numbers_end
");


// Foreach row returned, build a donation object
while($row = mysql_fetch_array($result)) {
  $donation = (object)array(
    'draw_numbers_start' => intval($row['draw_numbers_start']),
    'draw_numbers_end' => intval($row['draw_numbers_end']),
    'FIRSTNAME' => $row['FIRSTNAME'],
    'LASTNAME' => $row['LASTNAME'],
    'ADDRESS1' => $row['ADDRESS1'],
    'CITY' => $row['CITY'],
    'STATECD' => $row['STATECD'],
    'ZIP' => $row['ZIP'],
    'HOMEPHONE' => $row['HOMEPHONE']
  );      

  
  // Get the tickets details
  $start = $donation->draw_numbers_start;
  $details = 
        ucfirst($donation->FIRSTNAME) . ' ' . 
        ucfirst($donation->LASTNAME) . ', ' . 
        ucwords($donation->ADDRESS1) . ', ' . 
        ucwords($donation->CITY) . ', ' .
        ucwords($donation->STATECD) . ', ' .
        $donation->ZIP . '<br/>' .          
        formatPhoneNumber(cleanPhoneNumber($donation->HOMEPHONE));
  while($start <= $donation->draw_numbers_end): ?>
    <p>#<?php echo $start++ ?><br/><?php echo $details ?></p>
  <?php endwhile;
}

mysql_close($con);


function cleanPhoneNumber($n=''){
    return preg_replace('/[^0-9]/', '', $n);
}

function formatPhoneNumber($n=''){
    if(preg_match('/[0-9]{10}/', $n)){
        return '(' . substr($n, 0, 3) . ') ' . substr($n, 3, 3) . '-' . substr($n, 6, 4);
    }
    return $n;
    return preg_replace('/[^0-9]/', '', $n);
}

// C'est tout!
?>

</body>
</html>