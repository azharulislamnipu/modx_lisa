<?php
$data = $response = array();
parse_str($_POST['data'], $data);
array_walk($data, 'test_in_array');
$response['type'] = 'success';

$response['html'] = '';

if (empty($data['FIRSTNAME'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>First Name is not valid</p>';
}
if (empty($data['LASTNAME'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Last Name is not valid</p>';
}

if (empty($data['GENDER'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Gender is not valid</p>';
}
if (empty($data['BIRTHDATE'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Birth Date is not valid</p>';
}


$date = date_create($data['BIRTHDATE']);
$data['BIRTHDATE'] = date_format($date,"Y-m-d");



//die($data);
if($response['type'] !== 'error'){
    $db = get_db();
    $ndata = array (
        'FIRSTNAME'         => $data['FIRSTNAME'],
        'MIDDLEINITIAL'     => $data['MIDDLEINITIAL'],
        'LASTNAME'          => $data['LASTNAME'],
        'GENDER'            => $data['GENDER'],
        'BIRTHDATE'         => $data['BIRTHDATE'],
        'UPDATEDBY'         => $updated_by,
        'LASTUPDATE'        => date("Y-m-d H:i:s"),
    );

//    print_r($extra_val);
    $db_success = false;

    if(empty($data['CHILDID'])){
        $ndata['SPOUSEID'] = $spouse_id;
        $ndata['ARCHOUSAID'] = $archousa_id;
        $ndata['CUSTOMERID'] = $user_id;
        $db_success = $db->insert ('spp_children', $ndata);
        $response['action'] = 'INSERT';
        $response['CHILDID'] = $db_success;
    } else {
        $db->where ('CUSTOMERID', $user_id);
        $db->where ('CHILDID', $data['CHILDID']);
        $db_success = $db->update ('spp_children', $ndata);
        $response['action'] = 'UPDATE';
        $response['CHILDID'] = $data['CHILDID'];
    }

    if ($db_success){
        $response['type'] = 'success';
        $response['html'] = $response['TITLE'] = "{$ndata['FIRSTNAME']} {$ndata['MIDDLEINITIAL']} {$ndata['LASTNAME']}";
        SPP_ARCHON_DB::update_lastupdate_date($user_id);
    } else{
        $response['type'] = 'error';
//        $response['html'] = $db->getLastError();
        $response['html'] = '<p>Error occurred! Please try again.</p>';
    }
}

die(json_encode($response));