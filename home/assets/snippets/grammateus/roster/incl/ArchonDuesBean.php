<?php

/**
 * Holds an Archon's dues information
 * @author Pat Heard {http://fullahead.org}
 */

require_once 'ArchonDuesDatabaseManager.php';
class ArchonDuesBean {

	private $id = -1;
	private $transaction_id = '';
	private $customercd = -1;
	private $archon = null;
	private $dues_start_date = null;
	private $dues_amount = 0;
	private $dues_amount_additional = 0;
	private $comment = '';
	private $status_current = '';
	private $status_requested = '';
	private $created_date = null;
	private $created_by = -1;
	private $updated_date = null;
	private $updated_by = -1;


	public function __construct(array $args = null){

		// Set default arguments
		$args = array_merge(array(
                    'id' => -1,
                    'transaction_id' => '',
                    'customercd' => -1,
                    'archon' => null,
                    'dues_start_date' => null,
                    'dues_amount' => 0,
                    'dues_amount_additional' => 0,
                    'comment' => '',
                    'status_current' => '',
                    'status_requested' => '',
                    'created_date' => null,
                    'created_by' => -1,
                    'updated_date' => null,
                    'updated_by' => -1
		), $args);

                $this->id = $args['id'];
                $this->transaction_id = $args['transaction_id'];
                $this->customercd = $args['customercd'];
                $this->archon = $args['archon'];
                $this->dues_start_date = $args['dues_start_date'];
                $this->dues_amount = $args['dues_amount'];
                $this->dues_amount_additional = $args['dues_amount_additional'];
                $this->comment = $args['comment'];
                $this->status_current = trim($args['status_current']);
                $this->status_requested = trim($args['status_requested']);
                $this->created_date = $args['created_date'];
                $this->created_by = $args['created_by'];
                $this->updated_date = $args['updated_date'];
                $this->updated_by = $args['updated_by'];
	}

	public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
	}

        public function isDuesRecorded(){
            return $this->id !== -1;
        }

        public function isCompletable(){
            return $this->isDuesRecorded() && empty($this->transaction_id);
        }

        public function isSubmittable(){
            return $this->transaction_id === ArchonDuesDatabaseManager::COMPLETE_TRANSACTION_ID;
        }

        public function isFinalized(){
            return (!empty($this->transaction_id) && ($this->transaction_id === ArchonDuesDatabaseManager::SUBMIT_TRANSACTION_ID || $this->transaction_id !== ArchonDuesDatabaseManager::COMPLETE_TRANSACTION_ID));
        }

        public function isEditable(){
            return $this->isSubmittable();
        }

	public function __toString(){
            return
                ' id :' . $this->id .
                ' transaction_id :' . $this->transaction_id .
                ' customercd :' . $this->customercd .
                ' archon :' . $this->archon .
                ' dues_start_date :' . $this->dues_start_date.
                ' dues_amount :' . $this->dues_amount.
                ' dues_amount_additional :' . $this->dues_amount_additional .
                ' comment :' . $this->comment .
                ' status_current :' . $this->status_current .
                ' status_requested :' . $this->status_requested .
                ' created_date :' . $this->created_date .
                ' created_by :' . $this->created_by .
                ' updated_date :' . $this->updated_date .
                ' updated_by :' . $this->updated_by;
	}

}

?>
