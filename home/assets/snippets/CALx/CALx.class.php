<?php
/* -------------------------------------------------------------
:: Snippet: CALx
----------------------------------------------------------------
    Courte Description:
        Calendrier/Agenda �v�nementiel

    Version:
        0.7

    Cr�e par:
	    Gr�goire GENIER (Jabberwock - jabberwock@neuf.fr)
	    Anne PROVOST (Sa-Q - kuku.the.nut@gmail.com)

	    Utilisation:
				[!CALx? &getFolder=`2` &idMultiEvent=`34` &idDocYear=`19` &idDocMonthEvents=`26` 
				&dayStart=`0` &inactDay=`6` &startMonth=`08` &lang=`french` &useTV=`true` 
				&dateStartTVName=`startDateofEvent` &dateEndTVName=`endDateofEvent` &popupType=`2` 
				&popupSize=`200` &toolTipPosition=`CENTER` &showOtherMonth=`both` &showPrevNextLink=`false` 
				&cssId=`CALxContentFr` &TVCalxRepeat=`CALxRepeat` &TVstartDatePeriod=`startDatePeriod` 
				&TVendDatePeriod=`endDatePeriod`  &getTypeProcess=`createCal`!]
        
    Changelog :
    	v0.8.1 :
    		- v�rification si utilisation de la r�p�tition
    		- modification de l'affichage des infos de l'event (titre, longtext etc...)
    	v0.8 :
    		- Ajout de la balise <thead></thead>
				- Ajout <td><acronym title="lundi">lu</acronym></td>
				- Ajout d'un param�tre avec un nom d'id CSS, � l'utilisateur de changer si il y a plusieurs
					calendrier.
					Et par d�faut : "#CALxContent".
				- Ajout des liens pour les �v�nements apparaissant sur des jours des mois suivants/pr�c�dents
				- Ajout du param�tre pour choisir l'affichage des liens pr�c�dent/suivant
				- Rajout de la gestion des �v�nements r�p�t�s uniquement si on utilise les TV.
    	
    		Note : Avec la r�p�tition des �v�nements, il faut bien s�parer le r�le des deux dates.
    		Il y a les param�tres : &dateStartTVName / &dateEndTVName qui correspondent
    		dans le cas d'�v�nements r�p�t�s aux jours ou l'�v�nements a lieux,
    		et les param�tres &TVstartDatePeriod/&TVendDatePeriod qui correspondent � la 
    		p�riode de r�p�tition.
    		
    	v0.7 :
    		- modification de la gestion de la page des �v�nements multiples.
    			L'id du document qui re�oit l'appel du snippet pour l'affichage multiple
    			est d�sormais obligatoire et doit �tre cr�e avant l'utilisation du snippet.
    			Sans ce param�tre (&idMultiEvent), CALx ne s'affiche pas.
    			Cette page est mis � jour en fonction des param�tre de l'appel principal de CALx.
    		- correction d'un bug au passage � la nouvelle an�e via un changement de mois
    		ex : 12/2007 -> 01/2008
    		- CALx supporte les sous r�pertoires.
    			Le r�pertoire doit �tre publi� pour que les �v�nements contenus apparaissent
    		- ajout d'un param�tre pour choisir le mois de d�part � afficher : &startMonth
    		- ajout d'un param�tre pour choisir l'ann�e de d�part � afficher : &starYear
    		- Les liens vers l'affichage de l'ann�e enti�re et de tous les �v�nements du mois
    		ne sont plus cr�es.
    		A la place deux placeholder sont g�n�r�s :
    			[+CALx_ShowFullYear_&idDocYear+]
    			[+CALx_AllEventMonth_idDocMonthEvents+]
    			Exemple : 
						si &idDocYear=`21` le placeholder � la valeur : [+CALx_ShowFullYear_21+]
    				si &dDocMonthEvents=`22` le placeholder � la valeur : [+CALx_AllEventMonth_22+]
    				
    	v0.6:
    		- affichage d'un lien pour voir tous les �v�nements du mois en cours
    		- CALx permet d'afficher des �v�nements s'�talant sur plusieurs mois
    		- s'il le contenu ([*content*]) de l'�v�nement ne contient pas d'information
    		le lien "Cliquer ici pour plus de d�tail" dans l'info bulle n'apparait pas.
    		- ajout d'un param�tre &showOtherMonth=`string` qui permet l'affichage
				des jours du mois pr�c�dent, du mois suivant, les deux, ou aucun.
				Les �v�nements qui pourrait avoir lieu sur ces journ�es ne sont pas repris 
				pour ne pas alourdir le calendrier.
    		- ajout du param�tre &idDocMonthEvents=`�nt`qui indique le num�ro du document
    		qui contient l'appel de CALx avec le param�tre &getTypeProcess=`showAllMonthEvent`
    		- ajout d'un param�tre &chunkCSSName qui contient le nom du chunk contenant les classes
    		CSS (voir les indications d'installationpour plus de d�tail)
    		
    	v0.5:
    		- affiche un message d'erreur si les noms de TV indiqu�s ne sont pas trouv�s
    		- ajout d'un param�tre &idDocFullYear=`�nt` qui indique la page qui contient 
					l'appel du snippet de CALx avec le param�tre &getTypeProcess=`showFullYear`
				- correction de bugs majeurs emp�chant le focntionnement correct de CALx
					
			v0.4:
    		- correction du bug de choix de template pour le multi event
    		- correction de bugs pour l'affichage utf8
    		- am�lioration de la lisibilit� du code source lors de l'affichage par le navigateur
				- possibilit� de choisir entre une gestion des dates de publications ou
    			par TV.
    		- ajout d'un param�tre pour indiquer le nom de la TV correspondant
					� la date de d�part de l'�v�nement
    		- ajout d'un param�tre pour indiquer le nom de la TV correspondant
					� la date de fin de l'�v�nement
    		- XHTML 1.0 Transitional
    		
    	v0.3:
    		- ajout d'un param�tre pour le type d'info bulle :
    			0 : l'infobulle apparait sur le survol, pas de lien cliquable
    			1 : l'inobulle apparait sur un survol, mais permet de cliquer
    				sur un lien
    			2 : l'infobulle apparait sur un clic et permet de cliquer sur un lien
    		- ajout d'un param�tre pour la taille de l'infobulle
    		- ajout d'un param�tre pour la position de l'infobulle:
    			LEFT, RIGHT, CENTER, ABOVE, BELOW
    		- ajout d'un param�tre pour changer le premier jour de la semaine
    			ex : 0 pour commencer au lundi, 6 pour commencer au dimanche
    		- ajout d'un param�tre pour changer le jour d'inactivit� (en France
    		le dimanche par ex)
    		
     	v0.2:
    		- fonctionne avec et sans javascript
    		- modification de chemin pour fonctionner sur la 0.9.5
    		- ajout de la gestion des fichiers de langue
    		- ajout de fl�ches pour la navigation
    		- ajout d'infobulle de type 'title' 
    		- ajout de class CSS pour styler la barre de navigation
        
----------------------------------------------------------------
:: Description
----------------------------------------------------------------
    G�n�re un calendrier �v�nementiel, r�sup�rant les �v�nements
    se trouvant dans le r�pertoire `(et sous r�pertoire) de �d`
        
    Si une journ�e contient plusieurs �v�nements l'info-bulle
    indique le nombre d'�v�nements et un lien permettant d'acc�der 
    � la liste des �v�nements. Un lien sous chaque �v�nement
    permet d'acc�der au d�tail.

		Si on utilise la r�p�tition des �v�nements, un �v�nements peut �tre
		cr�er une seule fois puis r�p�t� selon les param�tres du calendrier.
		
----------------------------------------------------------------
:: Installation
----------------------------------------------------------------
    * Cr�er un r�pertoire aganda dans le r�pertoire assets/snippets/ de votre installation modx
    * Copier-coller le contenu de l'archive dans ce r�pertoire
    * Dans le manager cr�er un nouveau snippet : CALx
    * Copier le code suivant dedans :
			
		<?php
			$CALxPath = $modx->config['base_path']. 'assets/snippets/CALx/';
			include_once($CALxPath.'CALx.class.php');
			$objCALx = new CALx();
			
			$objCALx->getFolder=$getFolder;
			$objCALx->getTypeProcess=$getTypeProcess;
			$objCALx->idMultiEvent=$idMultiEvent;
			
			if(isset($cssId)){$objCALx->cssId=$cssId;}else{$objCALx->cssId='CALxContent';}
			if(isset($inactDay)){$objCALx->inactDay=$inactDay;}else{$objCALx->inactDay='6';}
			if(isset($dayStart)){$objCALx->dayStart=$dayStart;}else{$objCALx->dayStart='0';}
			if(isset($startMonth)){$objCALx->mois=$startMonth;}
			if(isset($startYear)){$objCALx->annee=$startYear;}
			if(isset($showOtherMonth)){$objCALx->showOtherMonth=$showOtherMonth;}else{$objCALx->showOtherMonth='none';}
			if(isset($lang)){$objCALx->lang=$lang;}else{$objCALx->lang='english';}
			if(isset($cssId)){$objCALx->cssId=$cssId;}else{$objCALx->cssId='CALx';}
			if(isset($showPrevNextLink)){$objCALx->showPrevNextLink=$showPrevNextLink;}else{$objCALx->showPrevNextLink=true;}
			if(isset($useTV)){
			  if(!isset($dateStartTVName) || !isset($dateEndTVName)){
			    $useTV='false';
			  }
			  $objCALx->useTV=$useTV;
			}else{
			  $objCALx->useTV='false';
			}
			if($useTV=='true'){
			$objCALx->dateStartTVName=$dateStartTVName;
			$objCALx->dateEndTVName=$dateEndTVName;
			$objCALx->TVCalxRepeat_name=$TVCalxRepeat;
			$objCALx->TVstartDatePeriod = $TVstartDatePeriod ;
			$objCALx->TVendDatePeriod = $TVendDatePeriod ;
			}
			if(isset($popupSize)){$objCALx->popupSize=$popupSize;}else{$objCALx->popupSize='300';}
			if(isset($toolTipPosition)){$objCALx->toolTipPosition=$toolTipPosition;}else{$objCALx->toolTipPosition='CENTER';}
			if(isset($popupType)){$objCALx->popupType=$popupType;}else{$objCALx->popupType='2';}
			if(isset($idDocYear)){$objCALx->idDocYear=$idDocYear;}
			if(isset($idDocMonthEvents)){$objCALx->idDocMonthEvents=$idDocMonthEvents;}
			$CALx = $objCALx->Run();
			$objCALx='';
			return $CALx;
		?>

		* Cr�er un nouveau document pour l'affichage des jours avec plusieurs �v�nement.
			Vous pouvez laisser le dicument vierge.
			indiquez le num�ro du document dans le param�tre : "idMultiEvent", de l'appel du snippet.
			
		* Pour utiliser les TV : Cr�er deux TV avec en :
			'Type d'entr�e : ' mettre 'Date'
			'Widget : ' mettre 'Date Formatter'
			*Ne changez pas le format
			'S�lectionner le template o� elles seront disponibles

		* Pour utiliser la r�p�tition, il faut cr�er trois TV :
			(Vous pouvez changer le nom des TV)
			
			CALxRepeat :
				'Type d'entr�e : ' mettre 'DropDown List Menu'
				'Valeurs optionnelles d'entr�e: ' mettre '==||toutes les semaines==week||tous les mois==month||tous les ans==year'
				NE PAS CHANGER LES 'WEEK', 'MONTH', 'YEAR' !!)
				'S�lectionner le template o� elles seront disponibles
			
			
			startDatePeriod :
				'Type d'entr�e : ' mettre 'Date'
				'Widget : ' mettre 'Date Formatter'
				*Ne changez pas le format
				'S�lectionner le template o� elles seront disponibles
			
			endDatePeriod :
				'Type d'entr�e : ' mettre 'Date'
				'Widget : ' mettre 'Date Formatter'
				*Ne changez pas le format
				'S�lectionner le template o� elles seront disponibles
				: &showPrevNextLink =true/false => d�pend de param�tre &showOtherMonth
				"repeatEvent" o� l'on choisirait "day", "week", "month", "year" pour r�p�ter l'�v�nement.			
----------------------------------------------------------------
:: Utilisation
----------------------------------------------------------------
			[[CALx? &getFolder=`id` &idMultiEvent=`�nt` &idDocYear=`�nt` &startMonth=�nt` 
				&startYear=`int` &idDocMonthEvents=`int` 
				&dayStart=`int` &inactDay=`int` &lang=`string` 
				&useTV=`bool` &dateStartTVName=`string` 
				&dateEndTVName=`string` &popupType=`int` &popupSize=`int` 
				&toolTipPosition=`string` &showOtherMonth=`string` &chunkCSSName=`string` 
				&getTypeProcess=`string`]]
			
----------------------------------------------------------------
:: Parametres
----------------------------------------------------------------
	&getFolder [int] (obligatoire)
		Le num�ro du r�pertoire contenant les �v�nements
	
	&idMultiEvent [int] (obligatoire)
		Le num�ro du document contenant l'appel du snippet avec
			&getTypeProcess=`showMultiEvent`
			
	&idDocYear [int] (facultatif)
		Le num�ro du document contenant l'appel de CALx avec &getTypeProcess=`showFullYear`
		Cr�e un lien qui pointe vers ce document en dessous du calendrier.
	
	&idDocMonthEvents [int] (facultatif)
		Le num�ro du document contenant l'appel de CALx avec &getTypeProcess=`showAllMonthEvent`
		Cr�e un lien qui pointe vers ce document en dessous du calendrier.
	
	&startMonth [int] (falcultatif)
		indique le mois � afficher dans le calandrier.
		Si rien n'est indiqu� c'est le mois en cours qui est pris en compte

	&startYear [int] (falcultatif)
		indique l'ann�e � afficher dans le calandrier.
		Si rien n'est indiqu� c'est l'ann�e en cours qui est prise en compte
						
	&lang [string] (par d�faut : english)
		Langue disponible de base : english, french, spanish, german (or dutch)
		
		Le nom de la langue � utiliser. C'est le pr�fixe au fichier de langue du
		r�pertoire 'lang'.
		Ex : 'french" pour le fichier 'lang/french.lang.php'
		
	&getTypeProcess [createCal | showMultiEvent | showFullYear | showAllMonthEvent]
		createCal -> cr�er le calendrier, cr�er les lien, colore les cellules
		showMultiEvent -> cr�e la page des liens vers les �v�nements
		showFullYear -> cr�e la liste des calendriers d'une ann�e compl�te
		showAllMonthEvent -> cr�e une liste de tous les �v�nements du mois
		
	&dayStart [0 | 1 | 2 | 3 | 4 | 5 | 6] (par d�faut : 0)
		Le jour de d�part de la semaine
		Valeurs possibles :
			0 : lundi
			1 : mardi
			2 : mercredi
			3 : jeudi
			4 : vendredi
			5 : samedi
			6 : dimanche
	
	&inactDay [0 | 1 | 2 | 3 | 4 | 5 | 6] (par d�faut : 6)
		Le jour d'inactivit� de la semaine
		Valeurs possibles :
			0 : lundi
			1 : mardi
			2 : mercredi
			3 : jeudi
			4 : vendredi
			5 : samedi
			6 : dimanche
	
	&useTV [true | false] (par d�faut : false)
		Si on souhaite utiliser les TV � la place des dates de publications
		mettre ce param�tre � `true`.
		Si vous utilisez les TV, les param�tres : dateStartTVName, et dateEndTVName
		sont obligatoires.
		Si vous omettez ces deux param�tres $useTV sera mis � `false`
	
	&dateStartTVName [string] Obligatoire si &useTV=`true`
		Indique le nom de la TV associ�e � la date de d�but d'�v�nement
	
	&dateEndTVName [string] Obligatoire si &useTV=`true`
		Indique le nom de la TV associ�e � la date de fin d'�v�nement		

	&popupType [0 | 1 | 2] (par d�faut : 2)
		Indique le type de popup � utiliser pour les info bulle:
		Valeurs possibles :
			0 : l'infobulle apparait sur le survol, mais disparait sur un mouvement de souris
				Pas de lien cliquable pour le d�tail
			1 : l'inobulle apparait sur un survol, permet de cliquer sur un lien pour le d�tail
			2 : l'infobulle apparait sur un clic, permet de cliquer sur un lien pour le d�tail
	
	&showOtherMonth [previous | both | next | none ] (par d�faut : none)
		Affiche les jours du mois pr�c�dent, suivant, les deux, ou aucun

	&showPrevNextLink [true | false ]
		Cr�er des liens sur les jours suivants/pr�c�dents s'il y a des �v�nements.
		Ce param�tre d�pend du param�tre &showOtherMonth.
		
	&TVCalxRepeat [string]
		Nom de la variable de template si on veut utiliser la r�p�tition des �v�nements.
		Cette variable correspond � la fr�quence de r�p�tition et doit �tre �gale �:
		week ou month ou year
	
	&TVstartDatePeriod [string]
		Nom de la variable de template qui contient la premi�re date de r�p�tition.
		
	&TVendDatePeriod [string]
			Nom de la variable de template qui contient la derni�re�re date de r�p�tition.
			
	&popupSize [int] (par d�faut : 300)
				Taille en pixel de la popup
				
	&toolTipPosition [CENTER | LEFT | RIGHT | ABOVE | BELOW] (par d�faut : CENTER)
		Emplacement de l'apparition de la popup
		
----------------------------------------------------------------
:: CSS
----------------------------------------------------------------	
	Copier-coller le contenu d'un des fichiers du r�pertoire style
	dans un chunk, et ajoutez le param�tre &chunkCSSName=`nomDuChunk`
	dans l'appel de CALx.
	
	Si aucun n'est chunk n'est indiqu� le th�me par d�faut est charg�
	Fichier : /CALX/Style/defaut_style.css.
	
	Important : Ne pas renomer les classes CSS.
------------------------------------------------------------- 

----------------------------------------------------------------
:: LE FICHIER DE LANGUE
----------------------------------------------------------------	
	Les fichiers de langues regroupent les traductions des quelques
	phrases du calendrier.
	Le fichier doit se trouver dans le r�pertoire 'lang' du snippet
	et le fichier doit se nommer comme cela :
		nomLangue.lang.php
	'nomLangue' sera utiliser lors de l'appel du snippet
------------------------------------------------------------- 
*/

class CALx{

	/*Variables avec la liste des mois, jour et nom de jour*/
	var $month= array();
	var $day= array();
	var $dayName= array();
	var $dayUS= array();
	var $nbjmonth = array();
	var $dayStart; // jour de d�part de l'affichage du calendrier
	var $inactDay; // jour d'inactivit� dans la semaine
	
	//Pile des �v�nements
	var $listEvents= array();
	var $tabEvenements = array();
	/*Est ce qu'on utilise les TV*/
	var $useTV, $dateStartTVName, $dateEndTVName;
	var $TVstartDatePeriod, $TVendDatePeriod;
	//TV de la r�p�tition
	var $TVCalxRepeat_name;
	
	var $error01, $msgCannotCreateCALx;
	/*Variables G�n�riques sur Modx et les tables*/
	var $idDocSource;
	var $dbase;
	var $tbl_prefix;	
	var $dbase_tbl;
	var $site_content;
	var $url_prefix;
	var $pathPrefix;   
	var $base_url;
	var $base_path;
	var $site_url;
	var $chunkCSSName, $chunkCSSID, $cssChunk;
	var $cssId;
	
	/*Variables sur les chemins des scripts java et CSS*/
	var $cssTheme;
	var $CALx;
	var $js;
	var $agenda;
	var $jour, $mois, $annee;
	var $lang;
	/*Variables pour les diff�rents messages*/
	var $startStringUniqEvent;
	var $endStringUniqEvent ;
	var $stringDetails;
	var $pagetitle;
	var $longtitle;
	var $description;
	var $introtext;
	var $eventOf;
	var $noEvent;
	var $previousYear;
  var $previousMonth;
  var $nextMonth;
  var $nextYear;
  var $msgShowMonthEvents;
  var $msgShowAYear;
  var $showOtherMonth;
  var $noEventMonth;
  
	/*Variables pour quelques propri�t�s d'Overlib*/
	var $popupSize, $closeText, $popupType;
	
	/*Variable du r�pertoire des �v�nements*/
	var $getFolder;
	/*Variable qui contient la liste des r�pertoires qui ont des �v�nements*/
	var $tabChild;
	/*Variable pour la gestion du multi event
	Valeurs de la variable $getType :
	showpageMultiEvent -> affiche la page des multi event
	createCal -> Construit le calendrier */
	var $getTypeProcess;
	/*id du document contenant l'appel du snippet pour l'ann�e enti�re*/
	var $idDocFullYear;
	/*Variable contenant la date lise dans le champs masqu�*/
	var $dateMultiEvent;
	/*id du document contenant l'appel du snippet pour les �v�nements du mois*/
	var $idDocMonthEvents;
	/*id du document qui recevra l'appel du snippet pour le multi event*/
	var $idMultiEvent;
	/*Variable qui indique si on doit montrer les liens sur les jours du mois pr�c�dent
	/suivant*/
	var $showPrevNextLink;
	
	function Run(){
		global $modx;
		   
		//Param�tre obligatoire
		if(empty($this->getFolder)){
			$msg = 'The parameter &getFolder is obligatory.';
			return utf8_encode($msg);
		}
		if(empty($this->getTypeProcess)){
			$msg = 'The parameter &getTypeProcess is obligatory.';
			return utf8_encode($msg);
		}
		if(empty($this->idMultiEvent)){
			$msg = 'The parameter &idMultiEvent is obligatory.';
			return utf8_encode($msg);
		}
		
		//initialise les variables du snippet
		$this->getDateParams();
		$this->init();
		
		if($this->getTypeProcess=='createCal'){
			if(!empty($this->cssChunk)){
				$output='<style type="text/css">'.$this->cssChunk.'</style>';
			}
			$output.= $this->buildCALx();
			return utf8_encode($output);
		}
		if($this->getTypeProcess=='showMultiEvent'){
			//Appel pour la version avec les dates de publications
			if($this->useTV=='false'){
				$output = $this->createLinkMultiEventPage($_GET['jour']);
			}else{
				$output = $this->createLinkMultiEventPageTV($_GET['jour']);
			}
			return utf8_encode($output);
		}
		if($this->getTypeProcess=='showFullYear'){
			$this->getDateParams();
			if(!empty($this->cssChunk)){
				$output='<style type="text/css">'.$this->cssChunk.'</style>';
			}
			$output= $this->getTabofYear();
			return utf8_encode($output);
		}
		if($this->getTypeProcess=='showAllMonthEvent'){
			$this->getDateParams();
			$output = $this->getAllMonthEvent();
			return utf8_encode($output);
		}
		return utf8_encode($this->msgCannotCreateCALx);
	}
	
	function init(){
		global $modx;
			
 		// configuration
 		$this->base_path = $modx->config['base_path'];
		$this->dbase = $modx->dbConfig['dbase'];
		$this->tbl_prefix = $modx->dbConfig['table_prefix'];	
		$this->dbase_tbl = $this->dbase.'.'.$this->tbl_prefix;
		$this->site_content = $this->dbase_tbl.'site_content';
		$this->tmpvar_content = $this->dbase_tbl.'site_tmplvar_contentvalues';
		$this->base_url=$modx->config['base_url'];
		$this->site_url=$modx->config['site_url'];
		$this->idDocSource=$modx->documentObject['id'];
		$this->url_prefix = "index.php?id=" . $this->idDocSource;
		$this->pathPrefix = 	$this->base_url.'assets/snippets/CALx';
		/*$this->cssTheme = $this->pathPrefix.'/style/'.$this->cssStyle.'_style.css';
		$modx->regClientCSS($this->cssTheme);*/
		
		//On cherche l'ID du chunk
  	/*if(!Empty($this->chunkCSSName)){
			$this->chunkCSSID=$this->getChunkId($this->chunkCSSName);
			$cssChunk=$this->getChunk($this->chunkCSSName);
			$this->cssChunk = str_replace('[<-chunkID->]', $this->chunkCSSID,$cssChunk);
		}else{	
			$this->cssTheme = $this->pathPrefix.'/style/'.$this->cssStyle.'_style.css';
			$modx->regClientCSS($this->cssTheme);
		}*/

		$this->js = $this->pathPrefix."/js/script.js";
		$modx->regClientScript('<script type="text/javascript" src="'.$this->js.'"></script>');
		$this->js = $this->pathPrefix."/js/overlib.js";
		$modx->regClientScript('<script type="text/javascript" src="'.$this->js.'"></script>');
		
		//Liste des jours en anglais pour le placement des dates
  	$this->dayUS[0]="Monday";
  	$this->dayUS[1]="Tuesday";
  	$this->dayUS[2]="Wednesday";
  	$this->dayUS[3]="Thursday";
  	$this->dayUS[4]="Friday";
  	$this->dayUS[5]="Saturday";
  	$this->dayUS[6]="Sunday";
  	
  	//Charge le fichier de langue
  	$this->loadLanguage();
  	
		//Nombre de jour dans le mois
		$this->nbjmonth[0] = 31 ;
  	//$this->nbjmonth[1] = ($this->annee%4==0?($this->annee%100==0?($this->annee%400?29:28):29):28);
  	$this->nbjmonth[1] = date("t",mktime( 0, 0, 0, $this->mois, 1, $this->annee ));
  	$this->nbjmonth[2] = 31 ;
  	$this->nbjmonth[3] = 30 ;
  	$this->nbjmonth[4] = 31 ;
  	$this->nbjmonth[5] = 30 ;
  	$this->nbjmonth[6] = 31;
  	$this->nbjmonth[7] = 31 ;
  	$this->nbjmonth[8] = 30 ;
  	$this->nbjmonth[9] = 31 ;
  	$this->nbjmonth[10] = 30 ;
  	$this->nbjmonth[11] = 31 ;
  	
		/*Pr�pare la liste des r�pertoires suceptibles d'avoir des �v�nements*/
		$this->getChildsArray($this->getFolder, $this->tabChild);
		$this->tabChild.='\''.$this->getFolder.'\'';
		
	}
	
	/*Retourne un tableau avec tous les fils de type r�pertoire du parent pass� en param�tre*/
	function getChildsArray($parent, &$childsArray){
		global $modx;
		
		$parentWhere_str = $this->site_content.".parent=".$parent;
		$sql = "SELECT "
			.$this->site_content.".id"
			." FROM ".$this->site_content
			." WHERE"
			." (".$parentWhere_str
			." AND ".$this->site_content.".isfolder=1"
			." AND ".$this->site_content.".published=1);";
	
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		for ($i = 0; $i < $rs_count; $i++){	
			$row = $modx->fetchRow($rs);
			$childsArray.='\''.$row['id'].'\',';
			$this->getChildsArray($row['id'],$childsArray);
			
		}
	}
	
	function getChunkId($chunkCSSName){
		global $modx;
		$chunckTbl = $this->dbase_tbl.'site_htmlsnippets';
		$sql = 'SELECT '.$chunckTbl.'.id
			FROM '.$chunckTbl.'
			WHERE ('.$chunckTbl.'.name=\''.$chunkCSSName.'\');';
		
		$rs = $modx->dbQuery($sql);	
		$chunkId='';
		for ($i = 0; $i < $modx->recordCount($rs); $i++){
			$row = $modx->fetchRow($rs);
			$chunkId=$row['id'];
		}
		return $chunkId;
	}
	
	function getChunk($chunkCSSName){
		global $modx;
		$chunckTbl = $this->dbase_tbl.'site_htmlsnippets';
		$sql = 'SELECT '.$chunckTbl.'.snippet
			FROM '.$chunckTbl.'
			WHERE ('.$chunckTbl.'.name=\''.$chunkCSSName.'\');';
		
		$rs = $modx->dbQuery($sql);	
		$chunk='';
		for ($i = 0; $i < $modx->recordCount($rs); $i++){
			$row = $modx->fetchRow($rs);
			$chunk=$row['snippet'];
		}
		return $chunk;
	}
		
	function getDateParams(){
		//Si on change de mois ou ann�e, les variables $_GET['mois'] et $_GET['annee']
		//existe
		if(isset($_GET['jour'])){$this->jour=$_GET['jour'];}
		if(isset($_GET['mois'])){$this->mois=$_GET['mois'];}
		if(isset($_GET['annee'])){$this->annee=$_GET['annee'];}
  	if(Empty($this->jour)){ $this->jour = date("j"); }
  	if(Empty($this->mois)){ $this->mois = date("m"); }
  	if(Empty($this->annee)){ $this->annee = date("Y");}

		//V�rifie si la date est valide
  	if(!checkdate($this->mois,"01",$this->annee)){
   		$this->jour = date("j") ;
   		$this->mois = date("m") ;
   		$this->annee = date("Y") ;
  	}
	}
	/* inclut le fichier de lanque et charge les variables*/
	function loadLanguage(){
		//On inclut la fichier de langue
		include($this->base_path.'assets/snippets/CALx/lang/'.$this->lang.'.lang.php');
		
		// On associe les variables globales des mois/ann�e avec les variable de langage
		$this->month=$month;
		$this->day=$day;
		$this->dayName=$dayName;
		
		// On associe les variables globales des mois/ann�e avec les variable de langage
  	$this->error01 = $error01;
  	$this->msgCannotCreateCALx = $msgCannotCreateCALx;
  	$this->closeText=$closeText;
		$this->titleText=$titleText;
		
		$this->startStringUniqEvent=$startStringUniqEvent;
		$this->endStringUniqEvent=$endStringUniqEvent ;
		$this->stringDetails=$stringDetails;
		$this->pagetitle=$pagetitle;
		$this->longtitle=$longtitle;
		$this->description=$description;
		$this->introtext=$introtext;
		$this->eventOf=$eventOf;
		$this->noEvent=$noEvent;
		
		$this->previousYear=$previousYear;
  	$this->previousMonth=$previousMonth;
  	$this->nextMonth=$nextMonth;
  	$this->nextYear=$nextYear;
  	
  	$this->msgShowAYear=$msgShowAYear;
  	$this->msgShowMonthEvents=$msgShowMonthEvents;
  	$this->noEventMonth=$noEventMonth;
	}
	
	function getTabofYear(){
		$output.= '<table class=\'CALxTabOfYearContent'.$this->chunkCSSID.'\'>'."\n";
		$output.='<tr>';
		for($i=1;$i<=12;$i++){
			$this->mois=$i;
			
			$output.='<td>'.$this->buildCALx().'</td>';
			if(($i+1)%2){
				$output.='</tr>';
				if($i!=12){$output.='<tr>';}
			}
		}
		$output.='</table>';
		return $output;
	}
	
	function getAllMonthEvent(){
			$output='';
			for($i=1;$i<$this->nbjmonth[($this->mois)-1]+1;$i++){
				if($this->useTV=='false'){
					$output.=$this->createLinkMultiEventPage($i);
				}else{
					//$output.=$this->createLinkMultiEventPageTV($i);
					$tmp_output='';
					$tempDom = $i;
					$tempMois = $this->mois;
					if($tempDom<10 and strlen($tempDom)==1 ){$tempDom='0'.$tempDom;}
					if($tempMois<10 and strlen($tempMois)==1 ){$tempMois='0'.$tempMois;}
					$tmp_output = $this->creerPageMultiEventTV($tempDom, $tempDom.'-'.$tempMois.'-'.$this->annee);
					if(!Empty($tmp_output)){
						$output.=$tmp_output;
					}
				}
			}
			if(Empty($output)){
				$output = '<strong>'.$this->noEventMonth.'</strong><br/>';
			}
			return $output;
	}
	
	function buildCALx(){
		global $modx;
			//Instruction sp�ciale pour mettre � jour le doc multi Event pour la langue
			//$this->createDocument(1, $this->readFileMultiEventsTXT());
			if(empty($this->idMultiEvent)){
				return '&idMultiEvent est obligatoire';
			}
			//$this->updateDocument($this->idMultiEvent);
			//On cr�e la barre de navigation
			$output=$this->createNavBar();
			
			//Creation du tableau inferieur
			$output.= '<div class="CALxContent">'."\n".'<table class=\'CALxTable'.$this->chunkCSSID.'\'>'."\n";
   		$output.='<thead>'."\n" ;
			$output.='<tr>';
			/*Affiche le nom des jours, en fonction du jour de d�part indiqu� en param�tre
			 du snippet.*/
    	for($i=$this->dayStart, $dayCount=0;$dayCount<7;$dayCount++){
				$output.= '<th>';
				if(!empty($this->dayName)){
						$output.= '<acronym title=\''.$this->dayName[$i].'\'>';
				}
				$output.=$this->day[$i];
				if(!empty($this->dayName)){
						$output.= '</acronym>';
				}				
				$output.='</th>'."\n" ;
				if($i==6){
					$i=0;
				}else{
					$i++;
				}
			}
			$output.= '</tr>'."\n" ;
			$output.='</thead>'."\n" ;
			$dom = 1 ;
			
  		//Affiche le reste du calendrier
  		for ($i=0;$i<6;$i++){
   			if($dom<=($this->nbjmonth[($this->mois-1)])){
					$output.= '<tr>'."\n";
   				for($j=$this->dayStart, $dayCount=0;$dayCount<7;$dayCount++){
						//Colorie le dimanche => $j_inac = position du dimanche dans le tableau $day
						$j_inac = ($j==$this->inactDay) ;
						$dateJourCalc=$this->mois.'/'.$dom.'/'.$this->annee;
						$dateJourCalc=strtotime($dateJourCalc);
						$nomJourCalc=strftime("%A",$dateJourCalc);
						/*Si le jour en cours a le m�me nom que le jour en position, on place le jour
						Exemple : si le 01/04/2007 est un dimanche on attend d'etre sur la bonne position pour se placer*/
						if($this->dayUS[$j]==$nomJourCalc && $dom!=($this->nbjmonth[($this->mois-1)]+1)){
								if($dom <= $this->nbjmonth[($this->mois-1)]) {
									$output.=$this->createCell($dom, $this->mois, $this->annee);
									$dom++;
								}
						}else{
							if($dom==1){/*Gestion des jours du mois pr�c�dent*/
								if($this->showOtherMonth=='previous' || $this->showOtherMonth=='both'){
									$tempYear='';
									$tempMonth='';
									$previousMonthNbDay='';
									if($this->mois==1){
										$tempYear=$this->annee-1;
										$tempMonth = 12;
										$previousMonthNbDay=$this->nbjmonth[($this->mois+10)];
										$dateTemp=($this->mois+11).'/'.$previousMonthNbDay.'/'.$tempYear;
									}else{
										$tempYear=$this->annee;
										$tempMonth = $this->mois-1;
										$previousMonthNbDay=$this->nbjmonth[($this->mois-2)];
										$dateTemp=($this->mois-1).'/'.$previousMonthNbDay.'/'.$tempYear;
									}
									$dateTemp2=strtotime($dateTemp);
									$numDay=strftime("%w",$dateTemp2);
								
									$jTemp = array(6 => 0, 5 => 2, 4 => 3, 3 => 4, 2 => 5, 1 => 6, 0 =>1);
									$day = $previousMonthNbDay-$numDay+$jTemp[$this->dayStart]+$dayCount;
									$outputCell=$this->createCell($day, $tempMonth, $tempYear, $this->showPrevNextLink);
									$output.=$outputCell."\n";
								}else{
									$output.='<td class=\'emptyDay'.$this->chunkCSSID.'\'></td>'."\n";
								}
							}
							/*Gestion des jours du mois suivant*/
							if(($dom-1)>=$this->nbjmonth[($this->mois-1)]){
								if($this->showOtherMonth=='next' || $this->showOtherMonth=='both'){
									$tempYear='';
									$tempMonth='';
									if($this->mois==12){
										$tempYear = $this->annee+1;
										$tempMonth = 1;
									}else{
										$tempYear = $this->annee;
										$tempMonth = $this->mois+1;									
									}
									$dateTemp=($this->mois+0).'/'.$this->nbjmonth[($this->mois-1)].'/'.$this->annee;
									$dateTemp2=strtotime($dateTemp);
									$numDay=strftime("%w",$dateTemp2);
									$day = $dayCount-$numDay-($dayCount-$j)+1;
									$outputCell=$this->createCell($day, $tempMonth, $tempYear, $this->showPrevNextLink);
									$output.=$outputCell."\n";
								}else{
									$output.='<td class=\'emptyDay'.$this->chunkCSSID.'\'></td>'."\n";
								}
							}
						}
						
						if($j==6){
							$j=0;
						}else{
							$j++;
						}
		   		}
		   		$output.= "</tr>\n" ;
	   		}
	   		/*$output.= "</tr>\n" ;*/
	  	}
	  	//$output.= "</tr>\n" ;
			$output.= "</table></div>\n" ;
			$output.='</div>'."\n";
			
			/*On affiche lien vers la page qui affiche une ann�e*/
			if(isset($this->idDocYear)and $this->getTypeProcess=='createCal'){
				$link ='<a href="index.php?id='.$this->idDocYear.'&amp;annee='.$this->annee.'">'.$this->msgShowAYear.'</a>';
				$modx->setPlaceholder('CALx_ShowFullYear_'.$this->idDocYear, utf8_encode($link));
			}
		/*On affiche lien vers la page qui affiche tous les �v�nements du mois*/
			if(isset($this->idDocMonthEvents)and $this->getTypeProcess=='createCal'){
				$link ='<a href="index.php?id='.$this->idDocMonthEvents.'&amp;mois='.$this->mois.'&amp;annee='.$this->annee.'">'.$this->msgShowMonthEvents.'</a>';
				$modx->setPlaceholder('CALx_AllEventMonth_'.$this->idDocMonthEvents, utf8_encode($link));
			}
			return $output;
	}
	
	/*Fonction qui cr�e une cellule compl�te pour le jour donn� en param�tres*/
	function createCell($dom, $mois, $annee, $createLink=true){
		$output='';
		//Si on est la date du jour on colorie dans une couleur sp�ciale id => today
		//Si on est dimanche on met la couleur du dimanche id => sunday
		/********************
		On v�rifie si des �v�nements sont planifi�s le jour que l'on est
		en train de mettre dans le tableau
		*********************/
								
		/*On test si on utilise les TV*/
		if($this->useTV=='false'){
			//Appels pour la version avec les date de publication
			$this->addEvents($mois.'/'.$dom.'/'.$annee);
		}else{
			//Appels pour la version avec TV's
			$tempDom = $dom;
			$tempMois=$mois;
			if($tempDom<10 and strlen($tempDom)==1 ){$tempDom='0'.$tempDom;}
			if($tempMois<10 and strlen($tempMois)==1 ){$tempMois='0'.$tempMois;}
			$returnMsg=$this->addEventsTV($tempDom.'-'.$tempMois.'-'.$annee);
			if(!empty($returnMsg)){return $returnMsg;}
		}
										
		$output.= '<td';
		//Ici on g�re si on a un �v�nement pour cette journ�e
		if(count($this->listEvents)>0 && $createLink==1){
			//COULEUR MULTI EVENEMENTS  										
			if(count($this->listEvents)>1){
				//On a au moins deux �v�nements pour cette journ�e ou en cours
				//CHANGEMENT DE LA COULEUR
				$output.=' class=\'multiple'.$this->chunkCSSID.'\'';
				//AJOUT DE L'INFOBULLE
				$output.='>'."\n";
				$msg = $this->startStringUniqEvent.'<strong>'.count($this->listEvents).'</strong> '.$this->endStringUniqEvent.'<br/>';
				//On lance la gestion du multi �v�nements
				//A la fin du traitement on re�oit le num�ro du document
				//r�sumant les liens vers les diff�rents �v�nements
				$this->launchMultiEvents();
				$moisCours=$mois+0;
				if($this->popupType!='0'){
					$msg.='<a href=index.php?id='.$this->idMultiEvent.'&amp;jour='.$dom.'&amp;mois='.$moisCours.'&amp;annee='.$annee.'>'.$this->stringDetails.'</a>';
				}
					$output.=$this->createTagEvents($dom, $moisCours, $annee, $msg, $this->idMultiEvent, $this->popupType);
				}else{
					//On a un seul �v�nement pour cette journ�e ou en cours
					//CHANGEMENT DE LA COULEUR
					$output.=' class=\'event'.$this->chunkCSSID.'\'';
					//AJOUT DE L'INFOBULLE
					$output.='>'."\n";
					foreach ($this->listEvents as $valeur) {
						$msg = $this->createMessageEvents($valeur);
					}
												
					$moisCours=$mois+0;
					$putLinkStringDetails=true;
					if($this->popupType!='0'){
						if(!empty($valeur['content'])){
							$msg.='<a href=index.php?id='.$valeur['id'].'&amp;jour='.$dom.'&amp;mois='.$moisCours.'&amp;annee='.$annee.'>'.$this->stringDetails.'</a>';
						}else{
							$putLinkStringDetails=false;
						}
					}
				$output.=$this->createTagEvents($dom, $moisCours, $annee, $msg, $valeur['id'], $this->popupType,$putLinkStringDetails);
				}
				$output.='</td>'."\n" ;
			}else{
				//COULEUR POUR LA DATE DU JOUR
				if($dom.(0+$mois).$annee==(date("d")).(0+date("m")).date("Y")){
					//CHANGEMENT DE LA COULEUR
					$output.=' class=\'today'.$this->chunkCSSID.'\'';
				}
				//COULEUR DU JOUR D'INACTIVITE
				else if($j_inac){
					//CHANGEMENT DE LA COULEUR
					$output.=' class=\'inactDay'.$this->chunkCSSID.'\'';
				}else{
					//JOUR NORMAL SANS RIEN
					$output.='';
				}
				//PAS D'INFOBULLE
				$output.='>';
				$output.= $dom.'</td>'."\n" ;
			}
			//On supprime les �v�nements se terminant aujourdhui
			//VERSION AVEC LES DATE DE PUBLICATION
		  if($this->useTV=='false'){
				$this->deleteEvents($mois.'/'.$dom.'/'.$annee);
		  }else{
				//VERSION AVEC LES TV's
		  	$returnMsg=$this->deleteEventsTV($tempDom.'-'.$tempMois.'-'.$annee);
		  	if(!empty($returnMsg)){return $returnMsg;}
		  }	  							
		return $output;
	}
	
	function createNavBar(){
			$navMonth=$_GET['mois'];
			$output='<div id=\''.$this->cssId.'\'>'."\n";
  		$previousYear=$this->annee-1;
  		$previousMonth=$this->mois-1;
  		$nextMonth=$this->mois+1;
  		$nextYear=$this->annee+1;
  		$strDayLink='';
			if(isset($_GET['jour'])){
				$strDayLink='&amp;jour='.$this->jour;
			}
			$tempNextYear=$this->annee;
			if($navMonth==12){
				$nextMonth=1;
				$tempNextYear = $nextYear;
			}
			$tempPrevYear=$this->annee;
			if($navMonth==1){
				$previousMonth=12;
				$tempPrevYear = $previousYear;
			}
			if($this->getTypeProcess!='showFullYear'){
				$output.='<div class=\'navPeriod'.$this->chunkCSSID.'\'>'."\n";
				$output.='<span class=\'previousYear'.$this->chunkCSSID.'\'><a href="'.$this->url_prefix.$strDayLink.'&amp;mois='.($this->mois).'&amp;annee='.$previousYear.'" title="'.$this->previousYear.'">&lt;&lt;</a></span>'."\n";
				$output.='<span class=\'previousMonth'.$this->chunkCSSID.'\'><a href="'.$this->url_prefix.$strDayLink.'&amp;mois='.$previousMonth.'&amp;annee='.$tempPrevYear.'" title="'.$this->previousMonth.'">&lt;</a></span>'."\n";
				$output.='<span class=\'month'.$this->chunkCSSID.'\'>'.$this->month[$this->mois-1].'</span>'."\n";
				$output.='<span class=\'year'.$this->chunkCSSID.'\'>'.$this->annee.'</span>'."\n";				
				$output.='<span class=\'nextMonth'.$this->chunkCSSID.'\'><a href="'.$this->url_prefix.$strDayLink.'&amp;mois='.$nextMonth.'&amp;annee='.$tempNextYear.'" title="'.$this->nextMonth.'">&gt;</a></span>'."\n";
				$output.='<span class=\'nextYear'.$this->chunkCSSID.'\'><a href="'.$this->url_prefix.$strDayLink.'&amp;mois='.($this->mois).'&amp;annee='.$nextYear.'" title="'.$this->nextYear.'">&gt;&gt;</a></span>'."\n";
				$output.='</div>'."\n";//End of navPeriod
			}else{
				if(isset($_GET['mois'])){
					$navMonth=$_GET['mois'];
				}else{
					$navMonth = date("m");
				}
				$output.='<div class=\'navPeriod'.$this->chunkCSSID.'\'>'."\n";
				$output.='<span class=\'previousYear'.$this->chunkCSSID.'\'><a href="'.$this->url_prefix.$strDayLink.'&amp;mois='.($navMonth).'&amp;annee='.$previousYear.'" title="'.$this->previousYear.'">&lt;&lt;</a></span>'."\n";
				$output.='<span class=\'month'.$this->chunkCSSID.'\'>'.$this->month[$this->mois-1].'</span>'."\n";
				$output.='<span class=\'year'.$this->chunkCSSID.'\'>'.$this->annee.'</span>'."\n";
				$output.='<span class=\'nextYear'.$this->chunkCSSID.'\'><a href="'.$this->url_prefix.$strDayLink.'&amp;mois='.($navMonth).'&amp;annee='.$nextYear.'" title="'.$this->nextYear.'">&gt;&gt;</a></span>'."\n";
				$output.='</div>'."\n";//End of navPeriod
			}
			
			return $output;
	}
	
	/*fonction qui met en forme le message*/
	function createMessageEvents($events){
		$output='';
		$output.= $events['pagetitle']. '<br/>';
		if($events['longtitle']){($output.= addslashes($events['longtitle']. '<br/>'));}
		if($events['introtext']){($output.= addslashes($events['introtext']. '<br/>'));}
		
		return $output;
	}
	/*fonction qui cr�er le lien pour les infos bulles*/
	function createTagEvents($dom, $mois, $annee, $msg, $id, $action=0, $putLinkStringDetails=true){
		/*Liste des valeurs de $action
		*	0 -> TOOLTIP POPUP
		* 1 -> STICKY POPUP
		* 2 -> CLICKABLE POPUP
		*/
		$output='';
		$link = '';
		if($putLinkStringDetails==true){
			$link = '"index.php?id='.$id.'&amp;jour='.$dom.'&amp;mois='.$mois.'&amp;annee='.$annee.'" title="'.$this->stringDetails.'"';
		}else{
			//$link = '"index.php?id=[*id*]&amp;mois='.$mois.'&amp;annee='.$annee.'"';
			$link = '"index.php?mois='.$mois.'&amp;annee='.$annee.'"';
		}
		switch($action){
			case 0:{
				  $output='<a href='.$link .' onmouseover="return overlib(\''.$msg.'\', 
						'.$this->toolTipPosition.', WIDTH, '.$this->popupSize.', CSSCLASS, TEXTFONTCLASS,\'textfontClass'.$this->chunkCSSID.'\', 
						FGCLASS,\'fgClass'.$this->chunkCSSID.'\',BGCLASS,\'bgClass'.$this->chunkCSSID.'\',
						CAPTIONFONTCLASS,\'capfontClass'.$this->chunkCSSID.'\', CLOSEFONTCLASS, \'closefontClass'.$this->chunkCSSID.'\');" onmouseout="return nd();">'.$dom.'</a>';
  			break;
  		}
			case 1:{
				$output= '<a href='.$link .' onmouseover="return overlib(\''.$msg.'\',
						'.$this->toolTipPosition.', CSSCLASS, TEXTFONTCLASS,\'textfontClass'.$this->chunkCSSID.'\', FGCLASS,\'fgClass'.$this->chunkCSSID.'\',BGCLASS,\'bgClass'.$this->chunkCSSID.'\',
						CAPTIONFONTCLASS,\'capfontClass'.$this->chunkCSSID.'\', CLOSEFONTCLASS, \'closefontClass'.$this->chunkCSSID.'\',
						STICKY, MOUSEOFF, WIDTH, '.$this->popupSize.');" onmouseout="return nd();">'.$dom."</a>\n" ; 
				break;
			}
			case 2:{
				$output='<a href='.$link.' onclick="return overlib(\''.$msg.'\', 
						'.$this->toolTipPosition.', CSSCLASS, TEXTFONTCLASS,\'textfontClass'.$this->chunkCSSID.'\', FGCLASS,\'fgClass'.$this->chunkCSSID.'\',BGCLASS,\'bgClass'.$this->chunkCSSID.'\',
						CAPTIONFONTCLASS,\'capfontClass'.$this->chunkCSSID.'\', CLOSEFONTCLASS, \'closefontClass'.$this->chunkCSSID.'\', 
						STICKY, CAPTION,\''.$this->titleText.'\', RIGHT, WIDTH, '.$this->popupSize.', 
						CLOSETEXT, \''.$this->closeText.'\');" onmouseout="nd();">'.$dom.'</a>';
				break;
			}
		}
		return $output;
	}
	

	
	/*Ajoutes les nouveaux �v�nements � la pile
	VERSION POUR LES DATES DEPUBLICATIONS*/
	function addEvents($dateEvent){
		global $modx;
		//En fonction du r�pertoire contenant les �v�nements
		$dateStartEvent=strtotime($dateEvent.' 00:00:00');
		$dateEndEvent=strtotime($dateEvent.' 23:59:59');
		
		//$parentWhere_str = $this->site_content.".parent=".$this->getFolder;
		$parentWhere_str = $this->site_content.".parent IN(".$this->tabChild.')';
		
		$sql = "SELECT "
		.$this->site_content.".id"
		.",".$this->site_content.".pagetitle"
		.",".$this->site_content.".longtitle"	
		.",".$this->site_content.".menutitle"
		.",".$this->site_content.".description"
		.",".$this->site_content.".introtext"
		.",".$this->site_content.".content"
		.",".$this->site_content.".menuindex"
		.",".$this->site_content.".pub_date"
		.",".$this->site_content.".unpub_date"		
		." FROM ".$this->site_content
		." WHERE"
		." (".$parentWhere_str.")"
		." AND ".$this->site_content.".isfolder=0"
		
		." AND ("
		." ".$this->site_content.".pub_date>=".$dateStartEvent
		." AND ".$this->site_content.".pub_date<=".$dateEndEvent
		.")"	
					
		." ORDER BY ".$this->site_content.".pub_date"
		.",".$this->site_content.".menuindex"	
		." ASC";
		
		//Ajoute les nouveaux �v�nements du jour � la pile des �v�nements
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		for ($i = 0; $i < $rs_count; $i++){
			$listEventsDetail = array();
			$row = $modx->fetchRow($rs);
			$listEventsDetail['id']=$row['id'];
			$listEventsDetail['pagetitle']=$row['pagetitle'];
			$listEventsDetail['longtitle']=$row['longtitle'];
			$listEventsDetail['menutitle']=$row['menutitle'];
			$listEventsDetail['description']=$row['description'];
			$listEventsDetail['introtext']=$row['introtext'];
			$listEventsDetail['content']=$row['content'];
			$listEventsDetail['menuindex']=$row['menuindex'];
			$listEventsDetail['pub_date']=$row['pub_date'];
			$listEventsDetail['unpub_date']=$row['unpub_date'];
			$this->listEvents[$row['id']]=$listEventsDetail;
			
			//UPDATE POUR FORCER LA PUBLICATION DES DOCUMENTS
			$sql= 'UPDATE '.$this->site_content.
			' SET '.$this->site_content.".published=1".
			' WHERE '.$this->site_content.'.id='.$row['id'].';';
			$modx->dbQuery($sql);
		}
		
		/*
		* CODE AJOUTE POUR LES EVENEMENTS SUR DEUX MOIS
		*/
		//On ajoute aussi les �v�nements qui se trouvent � cheval sur deux mois
		$sql = "SELECT "
		.$this->site_content.".id"
		.",".$this->site_content.".pagetitle"
		.",".$this->site_content.".longtitle"	
		.",".$this->site_content.".menutitle"
		.",".$this->site_content.".description"
		.",".$this->site_content.".introtext"
		.",".$this->site_content.".content"
		.",".$this->site_content.".menuindex"
		.",".$this->site_content.".pub_date"
		.",".$this->site_content.".unpub_date"		
		." FROM ".$this->site_content
		." WHERE"
		." (".$parentWhere_str.")"
		." AND ".$this->site_content.".isfolder=0"
		
		." AND ("
		." ".$this->site_content.".pub_date<=".$dateStartEvent
		." AND MONTH(FROM_UNIXTIME(".$this->site_content.".unpub_date))=MONTH(FROM_UNIXTIME(".$dateEndEvent."))
		AND ".$this->site_content.".unpub_date>=".$dateStartEvent.")"	
					
		." ORDER BY ".$this->site_content.".pub_date"
		.",".$this->site_content.".menuindex"	
		." ASC";
		
		
		$sql_2 = "SELECT "
		.$this->site_content.".id"
		.",".$this->site_content.".pagetitle"
		.",".$this->site_content.".longtitle"	
		.",".$this->site_content.".menutitle"
		.",".$this->site_content.".description"
		.",".$this->site_content.".introtext"
		.",".$this->site_content.".content"
		.",".$this->site_content.".menuindex"
		.",".$this->site_content.".pub_date"
		.",".$this->site_content.".unpub_date"		
		." FROM ".$this->site_content
		." WHERE"
		." (".$parentWhere_str.")"
		." AND ".$this->site_content.".isfolder=0"
		
		." AND ("
		." ".$this->site_content.".unpub_date>=".$dateEndEvent.")"	
					
		." ORDER BY ".$this->site_content.".pub_date"
		.",".$this->site_content.".menuindex"	
		." ASC";
				
		//Ajoute les nouveaux �v�nements du jour � la pile des �v�nements
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		for ($i = 0; $i < $rs_count; $i++){
		$row = $modx->fetchRow($rs);
			if($this->inList($sql_2, $row['contentid'])){
				$listEventsDetail = array();
				
				$listEventsDetail['id']=$row['id'];
				$listEventsDetail['pagetitle']=$row['pagetitle'];
				$listEventsDetail['longtitle']=$row['longtitle'];
				$listEventsDetail['menutitle']=$row['menutitle'];
				$listEventsDetail['description']=$row['description'];
				$listEventsDetail['introtext']=$row['introtext'];
				$listEventsDetail['content']=$row['content'];
				$listEventsDetail['menuindex']=$row['menuindex'];
				$listEventsDetail['pub_date']=$row['pub_date'];
				$listEventsDetail['unpub_date']=$row['unpub_date'];
				$this->listEvents[$row['id']]=$listEventsDetail;
			
				//UPDATE POUR FORCER LA PUBLICATION DES DOCUMENTS
				$sql= 'UPDATE '.$this->site_content.
				' SET '.$this->site_content.".published=1".
				' WHERE '.$this->site_content.'.id='.$row['id'].';';
				$modx->dbQuery($sql);
			}
		}
	}
	
	/*Supprime les �v�nements termin�s de la pile des �v�nements*/
	function deleteEvents($dateEvent){
		global $modx;
		$dateStartEvent=strtotime($dateEvent.' 00:00:00');
		$dateEndEvent=strtotime($dateEvent.' 23:59:59');
		
		$parentWhere_str = $this->site_content.".parent IN(".$this->tabChild.')';
		$sql = "SELECT "
		.$this->site_content.".id"
		.",".$this->site_content.".pagetitle"
		.",".$this->site_content.".longtitle"	
		.",".$this->site_content.".menutitle"
		.",".$this->site_content.".description"
		.",".$this->site_content.".introtext"
		.",".$this->site_content.".content"
		.",".$this->site_content.".menuindex"
		.",".$this->site_content.".pub_date"
		.",".$this->site_content.".unpub_date"		
		." FROM ".$this->site_content
		." WHERE"
		." (".$parentWhere_str.")"
		." AND ".$this->site_content.".isfolder=0"
		
		." AND ("
		." ".$this->site_content.".unpub_date>=".$dateStartEvent
		." AND ".$this->site_content.".unpub_date<=".$dateEndEvent
		.")"		
		." ORDER BY ".$this->site_content.".unpub_date"
		.",".$this->site_content.".menuindex"	
		." ASC";
		
		/*Supprime les nouveaux �v�nements du jour � la pile des �v�nements*/
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		for ($i = 0; $i < $rs_count; $i++){
			$row = $modx->fetchRow($rs);
			unset($this->listEvents[$row['id']]);
		}
	}
	
	/*PAGE MULTI EVENT PROBLEME DE DATES*/
	function creerPageMultiEventTV($dom, $dateEvent){
		$output = '';
		$titre='';
		$this->addEventsTV($dateEvent);
		if(count($this->listEvents)>0){
			$titre = '<strong>'.$this->eventOf.' '.$dom .' '.$this->month[$this->mois-1].' '.$this->annee.'</strong><br/>';
		}
		foreach ($this->listEvents as $valeur) {
			$output.= $valeur['pagetitle']. '<br/>';
			$output .= '<strong class=\'titleMulti'.'\'>'.$valeur['pagetitle'];
			if(!Empty($valeur['longtitle'])){
				$output .= ' - '.$valeur['longtitle'];
			}
			$output .= '</strong><br/>';
			if(!Empty($valeur['introtext'])){
				$output .= '<br/>';
			}
			if(!Empty($row['content'])){
				$output .='<span class=\'textfontClass'.'\'><a href="index.php?id='.$valeur['id'].'">'.$this->stringDetails.'</a></span><br/>';
			}
			$output .= '<br/>';
		}
		if(!Empty($output)){
			$output .= '<br/>';
		}
		$this->deleteEventsTV($dateEvent);
		return $titre.$output;
	}
	
	/*Ajoutes les nouveaux �v�nements � la pile
	VERSION POUR LES VARIABLES DE TEMPLATES*/
	function addEventsTV($dateEvent){
		global $modx;
		/*On s�lectionne tous les documents dont la TV de date de d�part
		est �gale � la date recherch�*/
		$TVstartdateid = $modx->getTemplateVar($this->dateStartTVName);
		
		if(Empty($TVstartdateid['id'])){return 'The TVid of the TVName : '.$this->dateStartTVName.' not found<br/>Check your syntaxe.';}
		$TVstartdateid=$TVstartdateid['id'];
		$sql = 	' SELECT * '.
						' FROM '.$this->tmpvar_content .
						' WHERE ('.$this->tmpvar_content.'.tmplvarid='.$TVstartdateid.' and 
								left('.$this->tmpvar_content.'.value,10) = \''.$dateEvent.'\');';
		
		$this->addEventsTVToTheStack($sql);
		
		/* Ajout des �v�nements sur deux mois */
		$TVenddateid = $modx->getTemplateVar($this->dateEndTVName);
		if(Empty($TVenddateid['id'])){return 'The TVid of the TVName : '.$this->dateEndTVName.' not found<br/>Check your syntaxe.';}
		$TVenddateid=$TVenddateid['id'];
		$dateEventMod = substr($dateEvent, 6,4).'-'. substr($dateEvent, 3,2).'-'.substr($dateEvent, 0,2);
       
		$sql = 	' SELECT * '.
						' FROM '.$this->tmpvar_content.
						' WHERE ('.$this->tmpvar_content.'.tmplvarid='.$TVenddateid.' and 
								MONTH(CONCAT(SUBSTRING('.$this->tmpvar_content.'.value,7,4),"-",
		SUBSTRING('.$this->tmpvar_content.'.value,4,2),"-",
		SUBSTRING('.$this->tmpvar_content.'.value,1,2)
		)) = MONTH(\''.$dateEventMod.'\')
		AND
		'.$this->tmpvar_content.'.tmplvarid='.$TVenddateid.' and '.
						'STR_TO_DATE('.$this->tmpvar_content.'.value, \'%d-%m-%Y\')>=\''.$dateEventMod.'\'
		
		
		);';      
		
		$sqlSartEvent=' SELECT * '.
						' FROM '.$this->tmpvar_content .
						' WHERE ('.$this->tmpvar_content.'.tmplvarid='.$TVstartdateid.' and 
								STR_TO_DATE('.$this->tmpvar_content.'.value, \'%d-%m-%Y\')<=\''.$dateEventMod.'\');';
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		for ($i = 0; $i < $rs_count; $i++){		
			$row = $modx->fetchRow($rs);
			if($this->inList($sqlSartEvent, $row['contentid'])){
				$this->addEventsTVToTheStackID($row['contentid']);
			}
		}

		/*Ajout des �v�nements r�p�t�s*/
		$this->addRepeatEventsTV($TVstartdateid, $dateEventMod, 'week');
		$this->addRepeatEventsTV($TVstartdateid, $dateEventMod, 'month');
		$this->addRepeatEventsTV($TVstartdateid, $dateEventMod, 'year');

		return '';
	}
	
	function addRepeatEventsTV($TVstartdateid, $dateEvent, $type_repeat){
		global $modx;
		/*************
		** Pour la gestion des r�p�titions
		** valeur : week, month, year
		*************/
		$TVCalxRepeat_id = $modx->getTemplateVar($this->TVCalxRepeat_name);
		if(Empty($TVCalxRepeat_id['id'])){return 'The TVid of the TVName : '.$this->TVCalxRepeat_name.' not found<br/>Check your syntaxe.';}
		$TVCalxRepeat_id = $TVCalxRepeat_id['id'];
		$sql = "SELECT ".$this->tmpvar_content.".contentid ".
						"FROM ".$this->tmpvar_content.",".$this->site_content.
						" WHERE ".$this->tmpvar_content.'.tmplvarid='.$TVCalxRepeat_id.
						" AND ".$this->tmpvar_content.".value='".$type_repeat."' ".
						" AND ".$this->tmpvar_content.".contentid=".$this->site_content.".id ";
		
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		for ($i = 0; $i < $rs_count; $i++){
			//Pour chaque document on recherche les infos
			$row = $modx->fetchRow($rs);
			$dateEvent_2 = substr($dateEvent, 8,2).'-'. substr($dateEvent, 5,2).'-'.substr($dateEvent, 0,4);
			$sql = $this->getRepeatEventsSql($TVstartdateid, 'start', $dateEvent_2, $type_repeat, $row['contentid']);
					//A changer pour avoir la valeur pass� en param�tre
			
				//$this->TVstartDatePeriod = 'startDatePeriod';
				$TVstartDatePeriod_id = $modx->getTemplateVar($this->TVstartDatePeriod);
				if(Empty($TVstartDatePeriod_id['id'])){return 'The TVid of the TVName : '.$this->TVstartDatePeriod.' not found<br/>Check your syntaxe.';}
				$TVstartDatePeriod_id = $TVstartDatePeriod_id['id'];
				
				//$this->TVendDatePeriod = 'endDatePeriod';
				$TVendDatePeriod_id = $modx->getTemplateVar($this->TVendDatePeriod);
				if(Empty($TVendDatePeriod_id['id'])){return 'The TVid of the TVName : '.$this->TVendDatePeriod.' not found<br/>Check your syntaxe.';}
				$TVendDatePeriod_id = $TVendDatePeriod_id['id'];
			
				$sql_2  = "SELECT * FROM ".$this->tmpvar_content;
				$sql_2 .= " WHERE ".$this->tmpvar_content.'.tmplvarid='.$TVstartDatePeriod_id;
				$sql_2 .= " AND STR_TO_DATE(".$this->tmpvar_content.".value, '%d-%m-%Y')<='".$dateEvent."'";
				$sql_2 .= " UNION ";
				$sql_2 .= " SELECT * FROM ".$this->tmpvar_content;
				$sql_2 .= " WHERE ".$this->tmpvar_content.'.tmplvarid='.$TVendDatePeriod_id;
				$sql_2 .= " AND STR_TO_DATE(".$this->tmpvar_content.".value, '%d-%m-%Y')>='".$dateEvent."'";
				
				$rs_2 = $modx->dbQuery($sql_2);	
				$rs_count_2 = $modx->recordCount($rs_2);
				
				if ($rs_count_2 == 2) {
					//Pour chaque document on recherche les infos
					$row_2 = $modx->fetchRow($rs_2);
					if($this->inList($sql, $row_2['contentid'])){
						$this->addEventsTVToTheStackID($row_2['contentid']);
				}
			}
		}
	}
			
	function addEventsTVToTheStack($sql){
		global $modx;
		//En fonction du r�pertoire contenant les �v�nements
		$parentWhere_str = $this->site_content.".parent IN(".$this->tabChild.')';
		//Ajoute les nouveaux �v�nements du jour � la pile des �v�nements
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		
		for ($i = 0; $i < $rs_count; $i++){
			//Pour chaque document on recherche les infos
			$row = $modx->fetchRow($rs);
			$sql = "SELECT "
				.$this->site_content.".id"
				.",".$this->site_content.".pagetitle"
				.",".$this->site_content.".longtitle"	
					.",".$this->site_content.".menutitle"
				.",".$this->site_content.".description"
				.",".$this->site_content.".introtext"
				.",".$this->site_content.".content"
				.",".$this->site_content.".menuindex"
				.",".$this->site_content.".pub_date"
				.",".$this->site_content.".unpub_date"		
				." FROM ".$this->site_content
				." WHERE"
				." (".$parentWhere_str.")"
				." AND ".$this->site_content.".isfolder=0"
				." AND ("
				." ".$this->site_content.".id=".$row['contentid'].');';
		
			//Ajoute les nouveaux �v�nements du jour � la pile des �v�nements
			$rs_1 = $modx->dbQuery($sql);	
			$rs_count_1 = $modx->recordCount($rs_1);
			for ($i_1 = 0; $i_1 < $rs_count_1; $i_1++){
				$listEventsDetail = array();
				$row_1 = $modx->fetchRow($rs_1);
				$listEventsDetail['id']=$row_1['id'];
				$listEventsDetail['pagetitle']=$row_1['pagetitle'];
				$listEventsDetail['longtitle']=$row_1['longtitle'];
				$listEventsDetail['menutitle']=$row_1['menutitle'];
				$listEventsDetail['description']=$row_1['description'];
				$listEventsDetail['introtext']=$row_1['introtext'];
				$listEventsDetail['content']=$row_1['content'];
				$listEventsDetail['menuindex']=$row_1['menuindex'];
				
				//UPDATE POUR FORCER LA PUBLICATION DES DOCUMENTS
				//On doit republier avant de r�cup�rer les valeurs des TV
				$sql= 'UPDATE '.$this->site_content.
				' SET '.$this->site_content.".published=1".
				' WHERE '.$this->site_content.'.id='.$row_1['id'].';';
				$modx->dbQuery($sql);	
		
				//On r�cup�re les date de d�but et de fin
				$tabTVEvent = $modx->getTemplateVarOutput(array($this->dateStartTVName,$this->dateEndTVName), $row_1['id'], 1);
				$listEventsDetail[$this->dateStartTVName]=$tabTVEvent[$this->dateStartTVName];
				$listEventsDetail[$this->dateEndTVName]=$tabTVEvent[$this->dateEndTVName];
				$this->listEvents[$row_1['id']]=$listEventsDetail;
			}
		}
	}
	
		function addEventsTVToTheStackID($id){
			global $modx;
			//En fonction du r�pertoire contenant les �v�nements
			$parentWhere_str = $this->site_content.".parent IN(".$this->tabChild.')';
			
			//Ajoute les nouveaux �v�nements du jour � la pile des �v�nements
			$row = $modx->fetchRow($rs);
			$sql = "SELECT "
				.$this->site_content.".id"
				.",".$this->site_content.".pagetitle"
				.",".$this->site_content.".longtitle"	
					.",".$this->site_content.".menutitle"
				.",".$this->site_content.".description"
				.",".$this->site_content.".introtext"
				.",".$this->site_content.".content"
				.",".$this->site_content.".menuindex"
				.",".$this->site_content.".pub_date"
				.",".$this->site_content.".unpub_date"		
				." FROM ".$this->site_content
				." WHERE"
				." (".$parentWhere_str.")"
				." AND ".$this->site_content.".isfolder=0"
				." AND ("
				." ".$this->site_content.".id=".$id.');';
		
			//Ajoute les nouveaux �v�nements du jour � la pile des �v�nements
			$rs_1 = $modx->dbQuery($sql);	
			$rs_count_1 = $modx->recordCount($rs_1);
			for ($i_1 = 0; $i_1 < $rs_count_1; $i_1++){
				$listEventsDetail = array();
				$row_1 = $modx->fetchRow($rs_1);
				$listEventsDetail['id']=$row_1['id'];
				$listEventsDetail['pagetitle']=$row_1['pagetitle'];
				$listEventsDetail['longtitle']=$row_1['longtitle'];
				$listEventsDetail['menutitle']=$row_1['menutitle'];
				$listEventsDetail['description']=$row_1['description'];
				$listEventsDetail['introtext']=$row_1['introtext'];
				$listEventsDetail['content']=$row_1['content'];
				$listEventsDetail['menuindex']=$row_1['menuindex'];
				
				//UPDATE POUR FORCER LA PUBLICATION DES DOCUMENTS
				//On doit republier avant de r�cup�rer les valeurs des TV
				$sql= 'UPDATE '.$this->site_content.
				' SET '.$this->site_content.".published=1".
				' WHERE '.$this->site_content.'.id='.$row_1['id'].';';
				$modx->dbQuery($sql);	
		
				//On r�cup�re les date de d�but et de fin
				$tabTVEvent = $modx->getTemplateVarOutput(array($this->dateStartTVName,$this->dateEndTVName), $row_1['id'], 1);
				$listEventsDetail[$this->dateStartTVName]=$tabTVEvent[$this->dateStartTVName];
				$listEventsDetail[$this->dateEndTVName]=$tabTVEvent[$this->dateEndTVName];
				$this->listEvents[$row_1['id']]=$listEventsDetail;
			}
	}
	
	/*Supprime les �v�nements termin�s de la pile des �v�nements
	VERSION POUR LES VARIABLES DE TEMPLATES*/
	function deleteEventsTV($dateEvent){
		global $modx;
		//En fonction du r�pertoire contenant les �v�nements
		$parentWhere_str = $this->site_content.".parent IN(".$this->tabChild.')';

		/*On s�lectionne tous les documents dont la TV de date de d�part
		est �gale � la date recherch�*/
		$TVenddateid = $modx->getTemplateVar($this->dateEndTVName);
		if(Empty($TVenddateid['id'])){return 'The TVid of the TVName : '.$this->dateEndTVName.' not found<br/>Check your syntaxe.';}
		$TVenddateid=$TVenddateid['id'];
		$sql = 	' SELECT * '.
						' FROM '.$this->tmpvar_content.
						' WHERE ('.$this->tmpvar_content.'.tmplvarid='.$TVenddateid.' and 
								left('.$this->tmpvar_content.'.value,10) = \''.$dateEvent.'\');';
							
		//Supprime les nouveaux �v�nements du jour � la pile des �v�nements
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		for ($i = 0; $i < $rs_count; $i++){
			//Pour chaque document on recherche les infos
			$row = $modx->fetchRow($rs);
			$sql = "SELECT "
				.$this->site_content.".id"
				.",".$this->site_content.".pagetitle"
				.",".$this->site_content.".longtitle"	
					.",".$this->site_content.".menutitle"
				.",".$this->site_content.".description"
				.",".$this->site_content.".introtext"
				.",".$this->site_content.".content"
				.",".$this->site_content.".menuindex"
				.",".$this->site_content.".pub_date"
				.",".$this->site_content.".unpub_date"		
				." FROM ".$this->site_content
				." WHERE"
				." (".$parentWhere_str.")"
				." AND ".$this->site_content.".isfolder=0"
				." AND ("
				." ".$this->site_content.".id=".$row['contentid'].');';
			
			/*Supprime les nouveaux �v�nements du jour � la pile des �v�nements*/
			$rs_1 = $modx->dbQuery($sql);	
			$rs_count_1 = $modx->recordCount($rs_1);
			for ($i_1= 0; $i_1 < $rs_count_1; $i_1++){
				$row_1 = $modx->fetchRow($rs_1);
				unset($this->listEvents[$row_1['id']]);
			}
		}
		
		/*Suppression des �v�nements r�p�t�s*/

		$this->deleteRepeatEventsTV($TVenddateid, $dateEvent, 'week');
		$this->deleteRepeatEventsTV($TVenddateid, $dateEvent, 'month');
		$this->deleteRepeatEventsTV($TVenddateid, $dateEvent, 'year');
		return '';
	}	
	
	/*Supprimes les �v�nements r�p�t�s de la pile*/
	function deleteRepeatEventsTV($TVenddateid, $dateEvent, $type_repeat){
		global $modx;
		/*************
		** Pour la gestion des r�p�titions
		** tvname pour les tests : CALxRepeat
		** valeur : week, month, year
		*************/
		$TVCalxRepeat_id = $modx->getTemplateVar($this->TVCalxRepeat_name);
		if(Empty($TVCalxRepeat_id['id'])){return 'The TVid of the TVName : '.$this->TVCalxRepeat_name.' not found<br/>Check your syntaxe.';}
		$TVCalxRepeat_id = $TVCalxRepeat_id['id'];
		/*Evenement r�p�tition : semaine, week*/
		$sql = "SELECT ".$this->tmpvar_content.".contentid ".
						"FROM ".$this->tmpvar_content.",".$this->site_content.
						" WHERE ".$this->tmpvar_content.'.tmplvarid='.$TVCalxRepeat_id.
						" AND ".$this->tmpvar_content.".value='".$type_repeat."' ".
						" AND ".$this->tmpvar_content.".contentid=".$this->site_content.".id ";
		
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		for ($i = 0; $i < $rs_count; $i++){
			//Pour chaque document on recherche les infos
			$row = $modx->fetchRow($rs);
			
			$sql = $this->getRepeatEventsSql($TVenddateid, 'end', $dateEvent, $type_repeat, $row['contentid']);
			
			/*Supprime les nouveaux �v�nements du jour � la pile des �v�nements*/
			$rs_1 = $modx->dbQuery($sql);	
			$rs_count_1 = $modx->recordCount($rs_1);
			for ($i_1= 0; $i_1 < $rs_count_1; $i_1++){
				$row_1 = $modx->fetchRow($rs_1);
				unset($this->listEvents[$row_1['contentid']]);
			}
		}
	}
	
	function getRepeatEventsSql($TVdateid, $type_date, $dateEvent, $type_repeat, $rowId){
		//global $modx;
		$dateEventMod = substr($dateEvent, 6,4).'-'. substr($dateEvent, 3,2).'-'.substr($dateEvent, 0,2);
		$sql = '';
		
		if($type_repeat == 'week'){
			$sql = "SELECT ".$this->tmpvar_content.".contentid, ".$this->tmpvar_content.".value ".
						" FROM ".$this->tmpvar_content.",".$this->site_content.
						" WHERE ".$this->tmpvar_content.".contentid =".$rowId.
						" AND ".$this->tmpvar_content.'.tmplvarid='.$TVdateid.
						" AND ".$this->tmpvar_content.".contentid=".$this->site_content.".id ".
						" AND DAYNAME(STR_TO_DATE(".$this->tmpvar_content.".value, '%d-%m-%Y'))=DAYNAME('".$dateEventMod."')";
		
		}
		if($type_repeat == 'month'){
			$sql = "SELECT ".$this->tmpvar_content.".contentid, ".$this->tmpvar_content.".value ".
						" FROM ".$this->tmpvar_content.",".$this->site_content.
						" WHERE ".$this->tmpvar_content.".contentid =".$rowId.
						" AND ".$this->tmpvar_content.'.tmplvarid='.$TVdateid.
						" AND ".$this->tmpvar_content.".contentid=".$this->site_content.".id ".
						" AND DATE_FORMAT(STR_TO_DATE(".$this->tmpvar_content.".value, '%d-%m-%Y'),'%d')=DATE_FORMAT('".$dateEventMod."','%d')";
		}
		if($type_repeat == 'year'){
			$sql = "SELECT ".$this->tmpvar_content.".contentid, ".$this->tmpvar_content.".value ".
						" FROM ".$this->tmpvar_content.",".$this->site_content.
						" WHERE ".$this->tmpvar_content.".contentid =".$rowId.
						" AND ".$this->tmpvar_content.'.tmplvarid='.$TVdateid.
						" AND ".$this->tmpvar_content.".contentid=".$this->site_content.".id ".
						" AND DATE_FORMAT(STR_TO_DATE(".$this->tmpvar_content.".value, '%d-%m-%Y'),'%d%m')=DATE_FORMAT('".$dateEventMod."','%d%m')";
		}
		
		$sql.=";";
		return $sql;
	}
	
	/*Fonctions qui g�rent le document pour le multi �v�nements
	Si le fichier texte avec le num�ro du document n'existe pas
	on le cr�er dans le r�pertoire parent et 
	on �crit le num�ro sur un fichier texte dans le r�pertoire d'agenda.
	Ensuite on lis ce fichier et on change le 'Content'*/
	function launchMultiEvents(/*$dom*/){
		global $modx;
		//UPDATE POUR FORCER LA PUBLICATION DES DOCUMENTS
		$sql= 'UPDATE '.$this->site_content.
			' SET '.$this->site_content.".published=1".
			' WHERE '.$this->site_content.'.id='.$this->idMultiEvent.';';
		$modx->dbQuery($sql);
	}
	
	function updateDocument($idDocMultiEvent){
		global $modx;
		//on calcul le contenu du document
		$content = '[[CALx? &amp;getFolder=`'.$this->getFolder.'` '.
			'&amp;lang=`'.$this->lang.'` '.
			'&amp;idMultiEvent=`'.$this->idMultiEvent.'` '.
			'&amp;useTV=`'.$this->useTV.'` '.
			'&amp;dateStartTVName=`'.$this->dateStartTVName.'` '.
			'&amp;dateEndTVName=`'.$this->dateEndTVName.'` '.
			'&amp;getTypeProcess=`showMultiEvent`]]';
		
		//Les diff�rentes valeurs � ins�rer/ou � mettre � jour
		$contentType='text/html';
		$typeDoc='document';
		$pagetitle=utf8_encode($this->pagetitle);
		$longtitle=utf8_encode($this->longtitle);
		$description=utf8_encode($this->description);
		$introtext=utf8_encode($this->introtext);
		$createdon=strtotime(date("d/m/Y h:i:s"));
		$editedon=strtotime(date("d/m/Y h:i:s"));
		$template = $modx->config['default_template'];
		$parent=$this->getFolder;
		$createdby='1';
		$isFolder='0';
		
		//UPDATE
		$sql='UPDATE '.$this->site_content.
				' SET '.$this->site_content.'.content='.'\''.$content.'\','.
				$this->site_content.'.template='.'\''.$modx->config['default_template'].'\','.
				$this->site_content.'.pagetitle='.'\''.$pagetitle.'\','.
				$this->site_content.'.longtitle='.'\''.$longtitle.'\','.
				$this->site_content.'.description='.'\''.$description.'\''.
				' WHERE ('.$this->site_content.'.id='.$this->idMultiEvent.');';
			$modx->dbQuery($sql);	
	}
	
	/*Cr�e la page des liens du multi event
	VERSION POUR LES DATES DE PUBLICATIONS*/
	function createLinkMultiEventPage($dom){
		global $modx;
		$dateEvent = $this->mois.'/'.$dom.'/'.$this->annee;
		
		//En fonction du r�pertoire contenant les �v�nements
		$dateStartEvent=strtotime($dateEvent.' 00:00:00');
		$dateEndEvent=strtotime($dateEvent.' 23:59:59');
		
		//$parentWhere_str = $this->site_content.".parent=".$this->getFolder;
		$parentWhere_str = $this->site_content.".parent IN(".$this->tabChild.')';
		$sql = "SELECT "
		.$this->site_content.".id"
		.",".$this->site_content.".pagetitle"
		.",".$this->site_content.".longtitle"	
		.",".$this->site_content.".menutitle"
		.",".$this->site_content.".description"
		.",".$this->site_content.".introtext"
		.",".$this->site_content.".content"
		.",".$this->site_content.".menuindex"
		.",".$this->site_content.".pub_date"
		.",".$this->site_content.".unpub_date"		
		." FROM ".$this->site_content
		." WHERE"
		." (".$parentWhere_str.")"
		." AND ".$this->site_content.".isfolder=0"
		
		." AND ("
		." ".$this->site_content.".pub_date<=".$dateEndEvent
		." AND ".$this->site_content.".unpub_date>=".$dateStartEvent
		.")"	
					
		." ORDER BY ".$this->site_content.".pub_date"
		.",".$this->site_content.".menuindex"	
		." ASC";
		
		//Ajoute les nouveaux �v�nements sur la page
		$rs = $modx->dbQuery($sql);	
		$rs_count = $modx->recordCount($rs);
		if ($rs_count>0){
			//Titre de la page
			$output = '<strong>'.$this->eventOf.' '.$dom .' '.$this->month[$this->mois-1].' '.$this->annee.'</strong><br/>';
		}else{
			//$output = '<strong>'.$this->noEvent.'</strong><br/>';
		}
		for ($i = 0; $i < $rs_count; $i++){
			$row = $modx->fetchRow($rs);
			$output .= '<strong class=\'titleMulti'.$this->chunkCSSID.'\'>'.$row['pagetitle'].' - '.$row['longtitle'].'</strong><br/>'.$row['introtext'].'<br/>';
			if(!Empty($row['content'])){
				$output .='<span class=\'textfontClass'.$this->chunkCSSID.'\'><a href="index.php?id='.$row['id'].'">'.$this->stringDetails.'</a></span><br/>';
			}
			$output.='<br/>';
		}
		//Si on a pas de chaine, c'est qu'il n'y a pas d'�v�nement
		if($this->getTypeProcess!='showAllMonthEvent'){
			if(Empty($output)){$output = '<strong>'.$this->noEvent.'</strong><br/>';}
		}
		
		return $output;		
	}
	
	/*Cr�e la page des liens du multi event
	VERSION POUR LES DATES DE PUBLICATIONS*/
	function createLinkMultiEventPageTV($dom){
		global $modx;
		
		$tempDom = $dom;
		$tempMois=$this->mois;
		if($tempDom<10 and strlen($tempDom)==1 ){$tempDom='0'.$tempDom;}
		if($tempMois<10 and strlen($tempMois)==1 ){$tempMois='0'.$tempMois;}
		$dateEvent = $this->annee.'-'.$tempMois.'-'.$tempDom;
									
		//En fonction du r�pertoire contenant les �v�nements
		$parentWhere_str = $this->site_content.".parent IN(".$this->tabChild.')';
		/*On s�lectionne tous les documents dont la TV de date de d�part
		est �gale � la date recherch�*/
		$TVstartdateid = $modx->getTemplateVar($this->dateStartTVName);
		
		$TVstartdateid=$TVstartdateid['id'];
		$TVenddateid = $modx->getTemplateVar($this->dateEndTVName);
		$TVenddateid=$TVenddateid['id'];
	
		$sqlStartEvent = 	' SELECT * '.
						' FROM '.$this->tmpvar_content.
						' WHERE ('.$this->tmpvar_content.'.tmplvarid='.$TVstartdateid.' and '.
						'STR_TO_DATE('.$this->tmpvar_content.'.value, \'%d-%m-%Y\')<=\''.$dateEvent.'\')';
							
		$sqlEndEvent = ' SELECT * '.
						' FROM '.$this->tmpvar_content.
						' WHERE ('.$this->tmpvar_content.'.tmplvarid='.$TVenddateid.' and '.
						'STR_TO_DATE('.$this->tmpvar_content.'.value, \'%d-%m-%Y\')>=\''.$dateEvent.'\')';
		
	
		$rsStartEvent = $modx->dbQuery($sqlStartEvent);
		$rs_count_start_event = $modx->recordCount($rsStartEvent);
		for ($i = 0; $i < $rs_count_start_event; $i++){
			//Pour chaque document on recherche les infos
			$row = $modx->fetchRow($rsStartEvent);
			
			if($this->inList($sqlEndEvent,$row['contentid'])){ 
				$sql = "SELECT "
					.$this->site_content.".id"
					.",".$this->site_content.".pagetitle"
					.",".$this->site_content.".longtitle"	
					.",".$this->site_content.".menutitle"
					.",".$this->site_content.".description"
					.",".$this->site_content.".introtext"
					.",".$this->site_content.".content"
					.",".$this->site_content.".menuindex"
					.",".$this->site_content.".pub_date"
					.",".$this->site_content.".unpub_date"		
					." FROM ".$this->site_content
					." WHERE"
					." (".$parentWhere_str.")"
					." AND ".$this->site_content.".isfolder=0"
					." AND ("
					." ".$this->site_content.".id=".$row['contentid'].');';	
				
					
				//Ajoute les nouveaux �v�nements sur la page
				$rs_2 = $modx->dbQuery($sql);	
				if(Empty($output)){
					$output = '<strong>'.$this->eventOf.' '.$dom .' '.$this->month[$this->mois-1].' '.$this->annee.' </strong><br/>';
				}
				$row_2 = $modx->fetchRow($rs_2);
				
				if(substr($row['value'],0,10)==$tempDom.'-'.$tempMois.'-'.$this->annee){
					$output .= '<strong class=\'titleMulti'.$this->chunkCSSID.'\'>'.$row_2['pagetitle'];
					if(!Empty($row_2['longtitle'])){
						$output .= ' - '.$row_2['longtitle'];
					}
					$output .= '</strong><br/>';
				}else{
					$output .= '<strong class=\'titleMulti'.$this->chunkCSSID.'\'>'.$row_2['pagetitle'];
					if(!Empty($row_2['longtitle'])){
						$output .= ' - '.$row_2['longtitle'];
					}
					$output .= '</strong><br/>';
				}
				if(!Empty($row_2['content'])){
					$output .='<span class=\'textfontClass'.$this->chunkCSSID.'\'><a href="index.php?id='.$row_2['id'].'">'.$this->stringDetails.'</a></span><br/>';
				}
				$output.='<br/>';
			}
		}
		
		/*Rajout gestion des �v�nements r�p�ter*/
		$output .= $this->addRepeatMultiEventsTV($TVstartdateid, $dateEvent, 'week', $output);
		$output .= $this->addRepeatMultiEventsTV($TVstartdateid, $dateEvent, 'month', $output);
		$output .= $this->addRepeatMultiEventsTV($TVstartdateid, $dateEvent, 'year', $output);
		
		//Si on a pas de chaine, c'est qu'il n'y a pas d'�v�nement
		if($this->getTypeProcess!='showAllMonthEvent'){
			if(Empty($output)){$output = '<strong>'.$this->noEvent.'</strong><br/>';}
		}
		return $output;		
	}
	
	function addRepeatMultiEventsTV($TVstartdateid, $dateEvent, $type_repeat, $stringHtml){
		global $modx;
		$parentWhere_str = $this->site_content.".parent IN(".$this->tabChild.')';
		
		/*************
		** Pour la gestion des r�p�titions
		** valeur : week, month, year
		*************/
		if(!Empty($this->TVCalxRepeat_name)){
			$TVCalxRepeat_id = $modx->getTemplateVar($this->TVCalxRepeat_name);
			if(Empty($TVCalxRepeat_id['id'])){return 'The TVid of the TVName : '.$this->TVCalxRepeat_name.' not found<br/>Check your syntaxe.<br/>';}
			$TVCalxRepeat_id = $TVCalxRepeat_id['id'];
			$sql = "SELECT ".$this->tmpvar_content.".contentid ".
							"FROM ".$this->tmpvar_content.",".$this->site_content.
							" WHERE ".$this->tmpvar_content.'.tmplvarid='.$TVCalxRepeat_id.
							" AND ".$this->tmpvar_content.".value='".$type_repeat."' ".
							" AND ".$this->tmpvar_content.".contentid=".$this->site_content.".id ";
			
			$rs = $modx->dbQuery($sql);	
			$rs_count = $modx->recordCount($rs);
			$oldContentIdAdd = '';
			for ($i = 0; $i < $rs_count; $i++){
				//Pour chaque document on recherche les infos
				$row = $modx->fetchRow($rs);
				
				$dateEvent_2 = substr($dateEvent, 8,2).'-'. substr($dateEvent, 5,2).'-'.substr($dateEvent, 0,4);
				$sql = $this->getRepeatEventsSql($TVstartdateid, 'start', $dateEvent_2, $type_repeat, $row['contentid']);
				
				//A changer pour avoir la valeur pass� en param�tre
				//$this->TVstartDatePeriod = 'startDatePeriod';
				$TVstartDatePeriod_id = $modx->getTemplateVar($this->TVstartDatePeriod);
				if(Empty($TVstartDatePeriod_id['id'])){return 'The TVid of the TVName : '.$this->TVstartDatePeriod.' not found<br/>Check your syntaxe.';}
				$TVstartDatePeriod_id = $TVstartDatePeriod_id['id'];
					
				//$this->TVendDatePeriod = 'endDatePeriod';
				$TVendDatePeriod_id = $modx->getTemplateVar($this->TVendDatePeriod);
				if(Empty($TVendDatePeriod_id['id'])){return 'The TVid of the TVName : '.$this->TVendDatePeriod.' not found<br/>Check your syntaxe.';}
				$TVendDatePeriod_id = $TVendDatePeriod_id['id'];
				
				$sql_2  = "SELECT * FROM ".$this->tmpvar_content;
				$sql_2 .= " WHERE ".$this->tmpvar_content.'.tmplvarid='.$TVstartDatePeriod_id;
				$sql_2 .= " AND STR_TO_DATE(".$this->tmpvar_content.".value, '%d-%m-%Y')<='".$dateEvent."'";
				$sql_2 .= " UNION ";
				$sql_2 .= " SELECT * FROM ".$this->tmpvar_content;
				$sql_2 .= " WHERE ".$this->tmpvar_content.'.tmplvarid='.$TVendDatePeriod_id;
				$sql_2 .= " AND STR_TO_DATE(".$this->tmpvar_content.".value, '%d-%m-%Y')>='".$dateEvent."'";
					
				$rs_2 = $modx->dbQuery($sql_2);	
				$rs_count_2 = $modx->recordCount($rs_2);
				
				if ($rs_count_2 == 2) {
				
					//Pour chaque document on recherche les infos
					$row_2 = $modx->fetchRow($rs_2);
					//PB : Les evenements ne s'affichent pas car la requete ne renvoi que le premier jour de la semaine
					//qui porte le m�me noms que celui de la TV
					//On parcourt la liste des �v�nements pour affichage
					
					//......
					//..... A FAIRE
					if($this->inList($sql,$row_2['contentid'])){
								$tmp = " AND ".$this->site_content.".id='".$row_2['contentid']."');";
								$sql_3 = "SELECT "
									.$this->site_content.".id"
									.",".$this->site_content.".pagetitle"
									.",".$this->site_content.".longtitle"	
									.",".$this->site_content.".menutitle"
									.",".$this->site_content.".description"
									.",".$this->site_content.".introtext"
									.",".$this->site_content.".content"
									.",".$this->site_content.".menuindex"
									.",".$this->site_content.".pub_date"
									.",".$this->site_content.".unpub_date"		
									." FROM ".$this->site_content
									." WHERE"
									." (".$parentWhere_str
									." AND ".$this->site_content.".isfolder=0"
									.$tmp;	
							
							
								//Ajoute les nouveaux �v�nements sur la page
								$rs_3 = $modx->dbQuery($sql_3);
								$dom = substr($dateEvent, 8,2)+0;	
								
								if(Empty($stringHtml)){
									$output .= '<strong>'.$this->eventOf.' '.$dom .' '.$this->month[$this->mois-1].' '.$this->annee.' </strong><br/>';
								}
								$row_3 = $modx->fetchRow($rs_3);
									
								if(substr($row_2['value'],0,10)==$tempDom.'-'.$tempMois.'-'.$this->annee){
									$output .= '<strong class=\'titleMulti'.$this->chunkCSSID.'\'>'.$row_3['pagetitle'];
									if(!Empty($row_3['longtitle'])){
										$output .= ' - '.$row_3['longtitle'];
									}
									$output .= '</strong><br/>';
								}else{
									$output .= '<strong class=\'titleMulti'.$this->chunkCSSID.'\'>'.' '.$row_3['pagetitle'];
									if(!Empty($row_3['longtitle'])){
										$output .= ' - '.$row_3['longtitle'];
									}
									$output .= '</strong><br/>';
								}
								if(!Empty($row_3['content'])){
									$output .='<span class=\'textfontClass'.$this->chunkCSSID.'\'><a href="index.php?id='.$row_3['id'].'">'.$this->stringDetails.'</a></span><br/>';
								}
								$output.='<br/>';
						}
				}
		}
	}
	return $output;
}
	
	function inList($sqlEndEvent, $contentid){
		global $modx;
		$rsEndEvent = $modx->dbQuery($sqlEndEvent); 
		$rs_count = $modx->recordCount($rsEndEvent);
		for ($i = 0; $i < $rs_count; $i++){
			$row = $modx->fetchRow($rsEndEvent);
			if($row['contentid']==$contentid){
				return true;
			}
		}
		return false;
	}
}
?>