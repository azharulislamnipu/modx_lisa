<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
  // DEFAULTS FOR NEW GALLERIES CAN BE SET BELOW
  
  // Default sort order
  // options are 'dragndrop' or 'alpha' or 'ralpha' or 'date' or 'rdate'
  define('SV_DEFAULT_SORT_ORDER', 'dragndrop');
  define('SV_DEFAULT_ADD_LINKS', true);
  define('SV_BACKGROUND_COLOR', '0x181818');
  define('SV_MAX_IMAGE_WIDTH', '640');
  define('SV_MAX_IMAGE_HEIGHT', '480');
  define('SV_TEXT_COLOR', '0xFFFFFF');
  define('SV_FRAME_COLOR', '0xFFFFFF');
  define('SV_FRAME_WIDTH', '20');
  define('SV_STAGE_PADDING', '40');
  define('SV_NAV_PADDING', '40');
  define('SV_NAV_POSITION', 'left');
  define('SV_THUMBNAIL_COLUMNS', '3');
  define('SV_THUMBNAIL_ROWS', '3');
  define('SV_ENABLE_RIGHT_CLICK_OPEN', 'true');
  define('SV_BACKGROUND_IMAGE_PATH', '');
  define('SV_V_ALIGN', 'center');
  define('SV_H_ALIGN', 'center');
  
  // OTHER SETTINGS
  
  // Specify type of urls to be stored in the gallery xml for new galleries
  // Options are 'RELATIVE' (default) for relative urls (images/myimage.jpg)
  // or 'ABSOLUTE' for absolute urls (/svmanager/g1/images/myimage.jpg)
  // or 'HTTP' for http urls (http://www.myserver.com/svmanager/g1/images/myimage.jpg)
  define('SV_URL_TYPE', 'RELATIVE');
  define('SV_XML_FILE', 'gallery.xml');
  define('SV_XML_SETTINGS_TAG', 'simpleviewergallery');
  define('SV_MASTER_PATH', 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svmaster'.DIRECTORY_SEPARATOR);
  define('SV_MASTER_FILECOUNT', 4);
  define('SV_DEFAULT_THUMB_PATH', 'thumbs'.DIRECTORY_SEPARATOR);
  define('SV_DEFAULT_IMAGE_PATH', 'images'.DIRECTORY_SEPARATOR);
  define('SV_THUMB_WIDTH', 65);
  define('SV_THUMB_HEIGHT', 65);
  define('SV_THUMB_QUALITY', 85);
  define('SV_DEFAULT_SWF', 'viewer.swf');
  define('SV_GALLERY_TITLE', 'New SimpleViewer Gallery');
  define ('SV_IMAGE_CAPTION', '');
  // Underline caption links when Auto links is on
  define('SV_UNDERLINE_CAPTION_LINKS', true);
  define('SV_CREATE_NEW', false);
?>