<?php
//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Snippet Name: 		MaxiGallery
// Short Description: 	Advanced imagegallery snippet
// Version: 			0.5 BETA 2
// Author: 				doze <dozebox@gmail.com>
// 
//////////////////////////////////////////////////////////////////////////////////////////////////

global $modx;

///////////////////////////////////////////////////////////////////////////////////////////////////
// Default values (can be overridden in the snippet call), 
// No need to edit unless you know what you're doing!
//
$mgconfig['debug'] = (isset($debug)) ? $debug : 0; //[0 | 1] display debug trace
$mgconfig['lang'] = (isset($lang)) ? $lang : "en"; // [ en | fi | de | fr ] (you can add more by your self, see lang_en.php for example) 
$mgconfig['manager_webgroups'] = (isset($manager_webgroups)) ? explode(",",$manager_webgroups) : array(); // [ array ]
$mgconfig['manager_webusers'] = (isset($manager_webusers)) ? explode(",",$manager_webusers) : array(); // [ array ]
$mgconfig['is_target'] = (isset($is_target)) ? $is_target : false; // [ true | false ]
$mgconfig['picture_target'] = (isset($picture_target)) ? $picture_target : ""; // [ number | empty ]
$mgconfig['manage_target'] = (isset($manage_target)) ? $manage_target : ""; // [ number | empty ]
$mgconfig['manage_gallery'] = (isset($manage_gallery)) ? $manage_gallery : ""; // [ number | empty ]
$mgconfig['view_gallery'] = (isset($view_gallery)) ? $view_gallery : ""; // [ number | empty ]
$mgconfig['pics_per_page'] = (isset($pics_per_page)) ? $pics_per_page : 0; // [ number | 0 for unlimited number ]
$mgconfig['pics_per_row'] = (isset($pics_per_row)) ? $pics_per_row : 0; // [ number | 0 for no auto break ]
$mgconfig['order_by'] = (isset($order_by)) ? $order_by : "pos,date"; // [ date | id | filename | title ] 
$mgconfig['order_direction'] = (isset($order_direction)) ? $order_direction : "DESC"; // [ ASC | DESC ] 
$mgconfig['gtable'] = (isset($gtable)) ? $gtable : "maxigallery"; // [ text ] 
$mgconfig['display'] = (isset($display)) ? $display : "normal"; // [ normal | embedded | childgalleries ]
$mgconfig['childgalleries_level_limit'] = (isset($childgalleries_level_limit)) ? $childgalleries_level_limit : 0; // [ number ]
$mgconfig['embedtype'] = (isset($embedtype)) ? $embedtype : ""; // [ slidebox | lightboxv2 | slimbox | slideshow | popup | external ]
$mgconfig['max_thumb_size'] = (isset($max_thumb_size)) ? $max_thumb_size : 130; // [ number ] 
$mgconfig['max_pic_size'] = (isset($max_pic_size)) ? $max_pic_size : 600; // [ number | zero for unlimited size ] 
$mgconfig['max_pic_number'] = (isset($max_pic_number)) ? $max_pic_number : 0; // [ number | 0 for unlimited number ] 
$mgconfig['quality_thumb'] = (isset($quality_thumb)) ? $quality_thumb : 70; // [ number between 0-100 ] 
$mgconfig['quality_pic'] = (isset($quality_pic)) ? $quality_pic : 70; // [ number between 0-100 ] 
$mgconfig['keep_bigimg'] = (isset($keep_bigimg)) ? $keep_bigimg : false; // [ true | false ] 
$mgconfig['max_big_size'] = (isset($max_big_size)) ? $max_big_size : 1024; // [ number | 0 for original image ] 
$mgconfig['quality_big'] = (isset($quality_big)) ? $quality_big : 100; // [ number between 0-100 ] 
$mgconfig['big_img_linkstyle'] = (isset($big_img_linkstyle)) ? $big_img_linkstyle : ""; // [ slidebox | lightboxv2 | popup | external ]
$mgconfig['keep_date'] = (isset($keep_date)) ? $keep_date : true; // [true | false ] 
$mgconfig['disable_rightclick'] = (isset($disable_rightclick)) ? $disable_rightclick : false; // [ true | false ]
$mgconfig['thumb_use_watermark'] = (isset($thumb_use_watermark)) ? $thumb_use_watermark : false; // [ true | false ]
$mgconfig['thumb_watermark_txt'] = (isset($thumb_watermark_txt)) ? $thumb_watermark_txt : "Copyright ".date("Y"); // [ text ]
$mgconfig['thumb_watermark_txt_color'] = (isset($thumb_watermark_txt_color)) ? $thumb_watermark_txt_color : "FFFFFF";	// [ RGB Hexadecimal ]
$mgconfig['thumb_watermark_font'] = (isset($thumb_watermark_font)) ? $thumb_watermark_font : 1; // [ 1 | 2 | 3 | 4 | 5 ]
$mgconfig['thumb_watermark_txt_vmargin'] = (isset($thumb_watermark_txt_vmargin)) ? $thumb_watermark_txt_vmargin : 2; // [ number ]
$mgconfig['thumb_watermark_txt_hmargin'] = (isset($thumb_watermark_txt_hmargin)) ? $thumb_watermark_txt_hmargin : 2;	// [ number ]
$mgconfig['thumb_watermark_img'] = (isset($thumb_watermark_img)) ? $thumb_watermark_img : "assets/snippets/maxigallery/watermark/watermark.png"; //path 
$mgconfig['thumb_watermark_type'] = (isset($thumb_watermark_type)) ? $thumb_watermark_type : "text"; // [ text | image ]
$mgconfig['thumb_watermark_valign'] = (isset($thumb_watermark_valign)) ? $thumb_watermark_valign : "bottom"; // [ top | center | bottom ]
$mgconfig['thumb_watermark_halign'] = (isset($thumb_watermark_halign)) ? $thumb_watermark_halign : "right"; // [ left | center | right ]
$mgconfig['pic_use_watermark'] = (isset($pic_use_watermark)) ? $pic_use_watermark : false; // [ true | false ]
$mgconfig['pic_watermark_txt'] = (isset($pic_watermark_txt)) ? $pic_watermark_txt : "Copyright ".date("Y")." ".$modx->config['site_name']; // [ text ]
$mgconfig['pic_watermark_txt_color'] = (isset($pic_watermark_txt_color)) ? $pic_watermark_txt_color : "FFFFFF";	// [ RGB Hexadecimal ]
$mgconfig['pic_watermark_font'] = (isset($pic_watermark_font)) ? $pic_watermark_font : 3; // [ 1 | 2 | 3 | 4 | 5 ]
$mgconfig['pic_watermark_txt_vmargin'] = (isset($pic_watermark_txt_vmargin)) ? $pic_watermark_txt_vmargin : 10; // [ number ]
$mgconfig['pic_watermark_txt_hmargin'] = (isset($pic_watermark_txt_hmargin)) ? $pic_watermark_txt_hmargin : 10;	// [ number ]
$mgconfig['pic_watermark_img'] = (isset($pic_watermark_img)) ? $pic_watermark_img : "assets/snippets/maxigallery/watermark/watermark.png"; //path 
$mgconfig['pic_watermark_type'] = (isset($pic_watermark_type)) ? $pic_watermark_type : "text"; // [ text | image ]
$mgconfig['pic_watermark_valign'] = (isset($pic_watermark_valign)) ? $pic_watermark_valign : "bottom"; // [ top | center | bottom ]
$mgconfig['pic_watermark_halign'] = (isset($pic_watermark_halign)) ? $pic_watermark_halign : "right"; // [ left | center | right ]
$mgconfig['big_use_watermark'] = (isset($big_use_watermark)) ? $big_use_watermark : false; // [ true | false ]
$mgconfig['big_watermark_txt'] = (isset($big_watermark_txt)) ? $big_watermark_txt : "Copyright ".date("Y")." ".$modx->config['site_name']; // [ text ]
$mgconfig['big_watermark_txt_color'] = (isset($big_watermark_txt_color)) ? $big_watermark_txt_color : "FFFFFF";	// [ RGB Hexadecimal ]
$mgconfig['big_watermark_font'] = (isset($big_watermark_font)) ? $big_watermark_font : 5; // [ 1 | 2 | 3 | 4 | 5 ]
$mgconfig['big_watermark_txt_vmargin'] = (isset($big_watermark_txt_vmargin)) ? $big_watermark_txt_vmargin : 15; // [ number ]
$mgconfig['big_watermark_txt_hmargin'] = (isset($big_watermark_txt_hmargin)) ? $big_watermark_txt_hmargin : 15;	// [ number ]
$mgconfig['big_watermark_img'] = (isset($big_watermark_img)) ? $big_watermark_img : "assets/snippets/maxigallery/watermark/watermark.png"; // [ path ]
$mgconfig['big_watermark_type'] = (isset($big_watermark_type)) ? $big_watermark_type : "text"; // [ text | image ]
$mgconfig['big_watermark_valign'] = (isset($big_watermark_valign)) ? $big_watermark_valign : "bottom"; // [ top | center | bottom ]
$mgconfig['big_watermark_halign'] = (isset($big_watermark_halign)) ? $big_watermark_halign : "right"; // [ left | center | right ]
$mgconfig['thumb_use_dropshadow'] = (isset($thumb_use_dropshadow)) ? $thumb_use_dropshadow : false; // [ true | false ] 
$mgconfig['thumb_shadow_bgcolor'] = (isset($thumb_shadow_bgcolor)) ? $thumb_shadow_bgcolor : "FFFFFF"; // [ RGB Hexadecimal ]
$mgconfig['thumb_shadow_path'] = (isset($thumb_shadow_path)) ? $thumb_shadow_path : "assets/snippets/maxigallery/dropshadow/"; // [ path ]
$mgconfig['pic_use_dropshadow'] = (isset($pic_use_dropshadow)) ? $pic_use_dropshadow : false; // [ true | false ]
$mgconfig['pic_shadow_bgcolor'] = (isset($pic_shadow_bgcolor)) ? $pic_shadow_bgcolor : "FFFFFF"; // [ RGB Hexadecimal ] 
$mgconfig['pic_shadow_path'] = (isset($pic_shadow_path)) ? $pic_shadow_path : "assets/snippets/maxigallery/dropshadow/"; // [ path ]
$mgconfig['big_use_dropshadow'] = (isset($big_use_dropshadow)) ? $big_use_dropshadow : false; // [ true | false ]
$mgconfig['big_shadow_bgcolor'] = (isset($big_shadow_bgcolor)) ? $big_shadow_bgcolor : "FFFFFF"; // [ RGB Hexadecimal ] 
$mgconfig['big_shadow_path'] = (isset($big_shadow_path)) ? $big_shadow_path : "assets/snippets/maxigallery/dropshadow/"; // [ path ]
$mgconfig['thumb_use_imagemask'] = (isset($thumb_use_imagemask)) ? $thumb_use_imagemask : false; // [ true | false ]
$mgconfig['thumb_mask_bgcolor'] = (isset($thumb_mask_bgcolor)) ? $thumb_mask_bgcolor : "FFFFFF"; // [ RGB Hexadecimal ]
$mgconfig['thumb_mask_position'] = (isset($thumb_mask_position)) ? $thumb_mask_position : "resize"; // [ top | topleft | topright | left | center | right | bottom | bottomleft | bottomright | resize ]
$mgconfig['thumb_mask_img'] = (isset($thumb_mask_img)) ? $thumb_mask_img : "assets/snippets/maxigallery/imagemask/demomask-frame1.png"; // [ path ]
$mgconfig['pic_use_imagemask'] = (isset($pic_use_imagemask)) ? $pic_use_imagemask : false; // [ true | false ]
$mgconfig['pic_mask_bgcolor'] = (isset($pic_mask_bgcolor)) ? $pic_mask_bgcolor : "FFFFFF"; // [ RGB Hexadecimal ]
$mgconfig['pic_mask_position'] = (isset($pic_mask_position)) ? $pic_mask_position : "resize"; // [ top | topleft | topright | left | center | right | bottom | bottomleft | bottomright | resize ]
$mgconfig['pic_mask_img'] = (isset($pic_mask_img)) ? $pic_mask_img : "assets/snippets/maxigallery/imagemask/demomask-frame2.png"; // [ path ]
$mgconfig['big_use_imagemask'] = (isset($big_use_imagemask)) ? $big_use_imagemask : false; // [ true | false ]
$mgconfig['big_mask_bgcolor'] = (isset($big_mask_bgcolor)) ? $big_mask_bgcolor : "FFFFFF"; // [ RGB Hexadecimal ]
$mgconfig['big_mask_position'] = (isset($big_mask_position)) ? $big_mask_position : "resize"; // [ top | topleft | topright | left | center | right | bottom | bottomleft | bottomright | resize ]
$mgconfig['big_mask_img'] = (isset($big_mask_img)) ? $big_mask_img : "assets/snippets/maxigallery/imagemask/demomask-frame2.png"; // [ path ]

// new in v05
$mgconfig['manageOuterTpl'] = (isset($manageOuterTpl)) ? $manageOuterTpl : "assets/snippets/maxigallery/templates/manageoutertpl.html"; // [ path | chunkname | text ]
$mgconfig['managePictureTpl'] = (isset($managePictureTpl)) ? $managePictureTpl : "assets/snippets/maxigallery/templates/managepicturetpl.html"; // [ path | chunkname | text ]
$mgconfig['manageUploadTpl'] = (isset($manageUploadTpl)) ? $manageUploadTpl : "assets/snippets/maxigallery/templates/manageuploadtpl.html"; // [ path | chunkname | text ]
$mgconfig['galleryOuterTpl'] = (isset($galleryOuterTpl)) ? $galleryOuterTpl : "assets/snippets/maxigallery/templates/galleryoutertpl.html"; // [ path | chunkname | text ]
$mgconfig['galleryPictureTpl'] = (isset($galleryPictureTpl)) ? $galleryPictureTpl : "assets/snippets/maxigallery/templates/gallerypicturetpl.html"; // [ path | chunkname | text ]
$mgconfig['childgalleryPictureTpl'] = (isset($childgalleryPictureTpl)) ? $childgalleryPictureTpl : "assets/snippets/maxigallery/templates/childgallerytpl.html"; // [ path | chunkname | text ]
$mgconfig['pictureTpl'] = (isset($pictureTpl)) ? $pictureTpl : "assets/snippets/maxigallery/templates/picturetpl.html"; // [ path | chunkname | text ]
$mgconfig['clearerTpl'] = (isset($clearerTpl)) ? $clearerTpl : "assets/snippets/maxigallery/templates/clearertpl.html"; // [ path | chunkname | text ]
$mgconfig['pageNumberTpl'] = (isset($pageNumberTpl)) ? $pageNumberTpl : "assets/snippets/maxigallery/templates/pagenumbertpl.html"; // [ path | chunkname | text ]
$mgconfig['css'] = (isset($css)) ? $css : "assets/snippets/maxigallery/css/default.css"; // [ text ]
$mgconfig['js'] = (isset($js)) ? $js : ""; // [ text ]
$mgconfig['slideshow_mode'] = (isset($slideshow_mode)) ? $slideshow_mode : "timed"; // [ timed | showcase ]
$mgconfig['slideshow_width'] = (isset($slideshow_width)) ? $slideshow_width : "600"; // [ number ]
$mgconfig['slideshow_height'] = (isset($slideshow_height)) ? $slideshow_height : "600"; // [ number ]
$mgconfig['slideshow_slidedelay'] = (isset($slideshow_slidedelay)) ? $slideshow_slidedelay : "9000"; // [ number ]
$mgconfig['slideshow_blendduration'] = (isset($slideshow_blendduration)) ? $slideshow_blendduration : "400"; // [ number ]
$mgconfig['slideshow_showinfo'] = (isset($slideshow_showinfo)) ? $slideshow_showinfo : true; // [ true | false ]
$mgconfig['slideshow_infodelay'] = (isset($slideshow_infodelay)) ? $slideshow_infodelay : "500"; // [ number ]

///////////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------------------------------------
//  SNIPPET LOGIC CODE STARTS HERE
//-------------------------------------------------------------------------------------------------

if (!class_exists('maxiGallery')) {
	$mgclass=$modx->config['base_path'].'assets/snippets/maxigallery/maxigallery.class.inc.php';
	if(file_exists($mgclass)){
		include_once($mgclass);
	} else {
		$output = 'Cannot find maxigallery class file! ('.$mgclass.')'; 
		return;
	}
}

// Initialize class
if (class_exists('maxiGallery')) {
   $mg = new maxiGallery($mgconfig);
} else {
	$output =  'MaxiGallery class not found'; 
	return;
}

if (!class_exists('CChunkie')) {
	$chunkieclass = $modx->config['base_path'].'assets/snippets/maxigallery/chunkie/chunkie.class.inc.php';
	if (file_exists($chunkieclass)) {
		include_once $chunkieclass;
	} else {
		$output = 'Cannot find chunkie class file! ('.$chunkieclass.')'; 
		return;
	}
}

//include lang file
$langfile = $modx->config['base_path'].'assets/snippets/maxigallery/lang/lang_'.$mg->mgconfig['lang'].'.php';
if(file_exists($langfile)){
	include($langfile);
} else {
	$defaultlang = $modx->config['base_path'].'assets/snippets/maxigallery/lang/lang_en.php';
	if (file_exists($defaultlang)){
		include($defaultlang);
	} else {
		$output = 'Default language file is missing!'; 
		return;
	}
}


//Initialize variables
$pics_tbl = $modx->db->config['dbase'].".".$modx->db->config['table_prefix'].$mg->mgconfig['gtable'];
$descvalid = 0; // assume descriptions not supported
$custposvalid = 0; // assume that custom image ordering not supported
if($mg->mgconfig['is_target']==true && $_REQUEST['gal_id']){
	$pageinfo=$modx->getPageInfo($_REQUEST['gal_id'],0, "id, pagetitle, longtitle, description, alias, createdby");
}else if($mg->mgconfig['manage_gallery']!=""){
	$pageinfo=$modx->getPageInfo($mg->mgconfig['manage_gallery'],0, "id, pagetitle, longtitle, description, alias, createdby");
}else if($mg->mgconfig['view_gallery']!=""){ 
	$pageinfo=$modx->getPageInfo($mg->mgconfig['view_gallery'],0, "id, pagetitle, longtitle, description, alias, createdby");
}else{
	$pageinfo=$modx->getPageInfo($modx->documentIdentifier,0, "id, pagetitle, longtitle, description, alias, createdby");
}
$path_to_gal=$path_to_galleries.$pageinfo['id']."/";

//validate gallery table
$query=mysql_query("DESC $pics_tbl");
if(!$query) {
	if($mg->createTable($pics_tbl)){
		$descvalid = 1; 
		$custposvalid = 1;
	}else{
		$output = $strings['database_error'];
		return;
	}
} else {
	if($mg->isDescValid($pics_tbl, $query)) {
		$descvalid=1;
	}
	if($mg->isPosValid($pics_tbl, $query)) {
		$custposvalid=1;
	}
	if(!$mg->isOwnIDValid($pics_tbl, $query)) {
		$output = $strings['database_error_ownid'];
		return;
	}
}

//manage pictures
if($mg->checkPermissions($_SESSION['mgrInternalKey'],$pageinfo['id'])) {
	if($_REQUEST['mode']=="admin" || ($mg->mgconfig['manage_gallery']!="" && $mg->mgconfig['manage_target']=="")) {
		// array to store the data for tpl
		$manageOuterTplData = array('messages' => '');
		
		// --- Add by Marc:
		if ($mg->mgconfig['debug']) echo"Debug >> ".__LINE__." - //if user is allowed to modify and has entered admin mode<br />\n";
		if ($mg->mgconfig['debug']) echo"|--- Action: ".$_REQUEST['action']."<br />\n";
		if ($mg->mgconfig['debug']) echo"|--- From_id: ".$_REQUEST['from_id']."<br />\n";
		if ($mg->mgconfig['debug']) echo"|--- Gal_id: ".$_REQUEST['gal_id']."<br />\n";
		if ($mg->mgconfig['debug']) echo"|--- max_pic_number: ".$mg->mgconfig['max_pic_number']."<br />\n";
		if ($mg->mgconfig['debug']) echo"|--- display: ".$mg->mgconfig['display']."<br />\n";
		if ($mg->mgconfig['debug']) echo"|--- embedtype: ".$mg->mgconfig['embedtype']."<br />\n";
		// --- End add by Marc
		
		//if user is allowed to modify and has entered admin mode:
		if($descvalid==0) {
			$manageOuterTplData['messages'] .= $strings['db_no_descr_support'];
		}
		if($custposvalid==0) {
			$manageOuterTplData['messages'] .= $strings['db_no_pos_support'];
		}
		//processors
		if($_REQUEST['action']=='upload_pics') { //if "upload pictures" has been used
			if(!file_exists($path_to_gal)) {
				mkdir($path_to_gal);
				chmod($path_to_gal,0777);
			}
			//if max picture limit is set, get current pictures in table
			if($mg->mgconfig['max_pic_number']!=0){
				$rsx=$modx->db->query("SELECT * FROM " . $pics_tbl . " WHERE gal_id='" . $pageinfo['id'] . "' ORDER BY " . $mg->mgconfig['order_by'] . " " . $mg->mgconfig['order_direction']);
			}
			$upload_error = false;
			for($i=0;$i<10;$i++) { //for each of the ten upload fields:
				//if max number of pics limit has been reached, break
				if($mg->mgconfig['max_pic_number']!=0 && ($mg->mgconfig['max_pic_number'] <= ($i + $modx->db->getRecordCount($rsx)))){
					$upload_error = true;
					$manageOuterTplData['messages'] .= $strings['max_pics_reached_some_discarded'];
					break;
				}

				$name=$mg->clearFilename($_FILES['file'.$i]['name']);

				if($name!="") {
					//check for existing filenames
					$ni = 1;
					while (file_exists($path_to_gal.$name)) { 
						$name=$ni.$name; 
						$ni++;
					}
					move_uploaded_file( $_FILES['file'.$i]['tmp_name'] , $path_to_gal.$name );
					chmod($path_to_gal.$name,0666);
					
					//upload file(s) to gallery folder
					if(substr(strtolower($name),-4)==".jpg" || substr(strtolower($name),-5)==".jpeg") {
						$mg->handlePics($pics_tbl, $path_to_gal, $pageinfo['id'], $name, "jpeg");
					}else if(substr(strtolower($name),-4)==".png"){
						$mg->handlePics($pics_tbl, $path_to_gal, $pageinfo['id'], $name, "png");
					}else if(substr(strtolower($name),-4)==".gif"){
						if(function_exists('imagecreatefromgif')){
							$mg->handlePics($pics_tbl, $path_to_gal, $pageinfo['id'], $name, "gif");
						}else{
							unlink($path_to_gal.$name);
							$upload_error = true;
							$manageOuterTplData['messages'] = $strings['gif_not_supported'];
						}
					}else if(substr(strtolower($name),-4)==".zip") {
						if(function_exists('zip_open')){
							$zip = zip_open($path_to_gal.$name);
							if ($zip) {
								//variable to count pics
								$zip_i=0;
								while ($zip_entry = zip_read($zip)) {
									//if max number of pics limit has been reached, break
									if($mg->mgconfig['max_pic_number']!=0 && ($mg->mgconfig['max_pic_number'] <= ($zip_i + mysql_num_rows($rsx)))){
										$upload_error = true;
										$manageOuterTplData['messages'] .= $strings['max_pics_reached_some_discarded'];
										break;
									}
									$zip_i++;
									$jpgfilename=$mg->clearFilename(zip_entry_name($zip_entry));
									$zi = 1;
									while (file_exists($path_to_gal.$jpgfilename)) { 
										$jpgfilename=$zi.$jpgfilename;
										$zi++;
									}
									if (zip_entry_open($zip, $zip_entry, "r")) {
										$jpgfile=fopen($path_to_gal.$jpgfilename,"a+b");
										fwrite($jpgfile,zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
										fclose($jpgfile);

										if(substr(strtolower($jpgfilename),-4)==".jpg" || substr(strtolower($jpgfilename),-5)==".jpeg") {
											handlePics($pics_tbl, $path_to_gal, $pageinfo['id'], $jpgfilename, "jpeg");
										}else if(substr(strtolower($jpgfilename),-4)==".png"){
											handlePics($pics_tbl, $path_to_gal, $pageinfo['id'], $jpgfilename, "png");
										}else if(substr(strtolower($jpgfilename),-4)==".gif"){
											if(function_exists('imagecreatefromgif')){
												$mg->handlePics($pics_tbl, $path_to_gal, $pageinfo['id'], $jpgfilename, "gif");
											}else{
												unlink($path_to_gal.$jpgfilename);
												$upload_error = true;
												$manageOuterTplData['messages'] .= $strings['gif_not_supported'];
											}
										}else{
											//encountered not supported filetype inside the zip
											$manageOuterTplData['messages'] .= $strings['supported_types_inzip'];
											$upload_error = true;
											unlink($path_to_gal.$jpgfilename);
										}
										zip_entry_close($zip_entry);
									}
								}
								zip_close($zip);
							}
						}else{
							$manageOuterTplData['messages'] .= $strings['zip_not_supported'];
							$upload_error = true;
							unlink($path_to_gal.$name);
						}
					}else{
						$manageOuterTplData['messages'] .= $strings['supported_types'];
						$upload_error = true;
					}
				}
			}
			if(!$upload_error){
				$manageOuterTplData['messages'] .= $strings['pictures_successfully_uploaded'];
			}
			//if display tyle is embedded and slidebox in use, create/update gallery xml
			if($mg->mgconfig['display']=="embedded" && $mg->mgconfig['embedtype']=="slidebox"){
				$mg->createGalleryXML($pics_tbl,$pageinfo['id'],$path_to_gal);
			}
		}

		if($_REQUEST['action']=="edit_pics") {    // If "save changes" has been used
			for($i=0;$i<$_REQUEST['number'];$i++) {
				if($_REQUEST['delete'.$i]=="yes") {
					$rs0=$modx->db->query("SELECT id,filename FROM $pics_tbl WHERE id='" . $_REQUEST['pic_id'.$i] . "'");
					$deletepic=$modx->fetchRow($rs0);
					if(file_exists($path_to_gal.$deletepic['filename'])) 
						unlink($path_to_gal.$deletepic['filename']);
					if(file_exists($path_to_gal."tn_".$deletepic['filename'])) 
						unlink($path_to_gal."tn_".$deletepic['filename']);
					if(file_exists($path_to_gal."big_".$deletepic['filename'])) 
						unlink($path_to_gal."big_".$deletepic['filename']);
					$rs1=$modx->db->query("DELETE FROM $pics_tbl WHERE id='" . $_REQUEST['pic_id'.$i] . "'");
				}
				if($_REQUEST['modified'.$i]=="yes") {
					// restructured for clarity and extended MF oct2005
					$updateQueryString= "UPDATE ".$pics_tbl." SET ";
					$updateQueryString.= "title='".addslashes($_REQUEST['title'.$i])."'"; //add title content
					if(!$mg->mgconfig['keep_date']) 
						$updateQueryString.=",date=NOW()";
					if($descvalid==1) 
						$updateQueryString.=", descr='".addslashes($_REQUEST['descr'.$i])."'"; //MF add descr content
					if($custposvalid==1 && is_numeric($_REQUEST['pos'.$i]))
						$updateQueryString.=", pos=".$_REQUEST['pos'.$i]; //add pos value
					$updateQueryString.=" WHERE id='" .$_REQUEST['pic_id'.$i]."'";
					$rs2=$modx->db->query( $updateQueryString );
				}
			}
			//if display style is embedded and slidebox in use, create/update gallery xml
			if($mg->mgconfig['display']=="embedded" && $mg->mgconfig['embedtype']=="slidebox"){
				$mg->createGalleryXML($pics_tbl,$pageinfo['id'],$path_to_gal);
			}else if(file_exists($path_to_gal."gallery.xml")){ //if gallery xml exists but not in use, delete it
				unlink($path_to_gal."gallery.xml");
			}
			//remove gallery directory if all images have been removed
			$res=$modx->db->query("SELECT * FROM " . $pics_tbl . " WHERE gal_id='" . $pageinfo['id'] . "' ORDER BY " . $mg->mgconfig['order_by'] . " " . $mg->mgconfig['order_direction']);
			$totalpics = $modx->db->getRecordCount($res);
			if($totalpics == 0 && file_exists($path_to_gal)){
				$mg->deldir($path_to_gal);
			}
			$manageOuterTplData['messages'] .= $strings['changes_have_been_saved'];
		}

		// Create manage template data 
		// Edit gallery and its pictures
		$urparams = "";
		if($mg->mgconfig['is_target']==true && $_REQUEST['gal_id'] ){
			$urparams = "gal_id=".$_REQUEST['gal_id'];
			$backtonormal = $modx->makeUrl($_REQUEST['gal_id'], '', '');
		}else if($mg->mgconfig['manage_gallery']!=""){
			$backtonormal = $modx->makeUrl($mg->mgconfig['manage_gallery'], '', '');
		}else if($_REQUEST['from_id']){
			$urparams = "from_id=".$_REQUEST['from_id'];
			$backtonormal = $modx->makeUrl($_REQUEST['from_id'], '', '');
		}else{
			$backtonormal = $modx->makeUrl($modx->documentIdentifier, '','');
		}
		
		$manageOuterTplData['urlback'] = $backtonormal; 
		$manageOuterTplData['urlaction'] = $modx->makeUrl($modx->documentIdentifier, '', $urparams);
		
		//if logged from backend and rights to edit this page, get all pics	
		if($_SESSION['mgrValidated'] && $_SESSION['mgrPermissions']['edit_document']){
			$sql = "SELECT * FROM " . $pics_tbl . " WHERE gal_id='" . $pageinfo['id'] . "' ORDER BY " . $mg->mgconfig['order_by'] . " " . $mg->mgconfig['order_direction'];
		}else{
			$sql = "SELECT * FROM " . $pics_tbl . " WHERE gal_id='" . $pageinfo['id'] . "' AND own_id='" . $modx->getLoginUserID() . "' ORDER BY " . $mg->mgconfig['order_by'] . " " . $mg->mgconfig['order_direction'];
		}
		$rs1=$modx->db->query($sql);
		$i=0;
		
		$manageOuterTplData['managepictures'] = '';
		
		while($pic=$modx->fetchRow($rs1)) {
			$file = $path_to_gal.$pic['filename'];
			$tn_file = $path_to_gal . "tn_" . $pic['filename'];
			
			$tpl = new CChunkie($mg->mgconfig['managePictureTpl']);
			
			$managePictureTplData = array();
			
			$pic['title'] = stripslashes($pic['title']);
			$pic['descr'] = stripslashes($pic['descr']);
			$managePictureTplData['picture'] = $pic;
			$managePictureTplData['path_to_gal'] = $path_to_gal;
			
			$managePictureTplData['strings'] = $strings;
			
			$managePictureTplFieldNameData = array();
			
			$managePictureTplFieldNameData['delete'] = 'delete'.$i;
			$managePictureTplFieldNameData['position'] = 'pos'.$i;
			$managePictureTplFieldNameData['title'] = 'title'.$i;
			$managePictureTplFieldNameData['pictureid'] = 'pic_id'.$i;
			$managePictureTplFieldNameData['modified'] = 'modified'.$i;
			$managePictureTplFieldNameData['description'] = 'descr'.$i;
			
			$managePictureTplData['fieldnames'] = $managePictureTplFieldNameData; 
			
			$tpl->addVar('maxigallery', $managePictureTplData);
			
			$manageOuterTplData['managepictures'] .= $tpl->Render();
			
			$i++;
			
		}
		
		$outerTplDataHiddenFields['mainform'] = '<input type="hidden" name="action" value="edit_pics" /><input type="hidden" name="mode" value="admin" /><input type="hidden" name="number" value="'.$i.'" />';
		 
		//check that max limit of pictures have not been reached, if it's set
		if(!isset($mg->mgconfig['max_pic_number']) || ($mg->mgconfig['max_pic_number']==0 || $mg->mgconfig['max_pic_number']>mysql_num_rows($rs1))){
			$outerTplDataHiddenFields['uploadform'] = '<input type="hidden" name="action" value="upload_pics" /><input type="hidden" name="mode" value="admin" />';
			for($i=0;$i<10;$i++) {
				$tpl = new CChunkie($mg->mgconfig['manageUploadTpl']);
				$uploadPictureTplData = array();
				
				$uploadPictureTplData['counter'] = $i+1;
				$uploadPictureTplData['fieldnames.file'] = 'file'.$i;
				
				$tpl->addVar('maxigallery', $uploadPictureTplData);
			
				$manageOuterTplData['uploadpictures'] .= $tpl->Render();
							
			}
			
		}else{
			$manageOuterTplData['messages'] .= $strings['max_pics_reached'];
		}
		
		$manageOuterTplData['hiddenfields'] = $outerTplDataHiddenFields;
		
		//Construct chunkie with the manager template
		$tpl = new CChunkie($mg->mgconfig['manageOuterTpl']);
		//add data to the template
		$manageOuterTplData['pageinfo'] = $pageinfo;
		$manageOuterTplData['strings'] = $strings;
		$tpl->addVar('maxigallery', $manageOuterTplData);
		//render template and return output
		$output = $tpl->Render();
		$mg->regScriptsAndCSS();
		return;
		
	}
}

//-------------------------------------------------------------------------------------------------
// Single picture
//-------------------------------------------------------------------------------------------------

if ($_REQUEST['pic']) {
	
	// --- Add by Marc:
	if ($mg->mgconfig['debug']) echo"Debug >> ". __LINE__." - // Show single Pic<br />\n";
	if ($mg->mgconfig['debug']) echo"|--- Pics: ".$_REQUEST['pic']."<br />\n";
	if ($mg->mgconfig['debug']) echo"|--- From_id: ".$_REQUEST['from_id']."<br />\n";
	if ($mg->mgconfig['debug']) echo"|--- Gal_id: ".$_REQUEST['gal_id']."<br />\n";
	if ($mg->mgconfig['debug']) echo"|--- Big_img_linkstyle: ".$mg->mgconfig['big_img_linkstyle']."<br />\n";
	if ($mg->mgconfig['debug']) echo"|--- Keep_bigimg: ".$mg->mgconfig['keep_bigimg']."<br />\n";
	if ($mg->mgconfig['debug']) echo"|--- Display_picno: ".$mg->mgconfig['display_picno']."<br />\n";
	// --- End add by Marc

	//retrieve all gallery pics
	$rs=$modx->db->select("*", $modx->db->config['table_prefix'].$mg->mgconfig['gtable'], "gal_id='" . $pageinfo['id'] . "'", $mg->mgconfig['order_by'].' '.$mg->mgconfig['order_direction']);
	$pics = array();
	while ($row = $modx->db->getRow($rs)) {
	 	array_push($pics, $row);
	}
	//find pic
	$i=0;
	foreach($pics as $pic) {
		if($pic['id']==$_REQUEST['pic']) {
			$current=$i;
			break;
		}
	$i++;
	}		
	
	$pic_number=$i+1;
	$total_pics=count($pics);
	
	$pictureTplData = array();
	$pictureTplData['pageinfo'] = $pageinfo;
	$pictureTplData['big_img_linkstyle'] = $mg->mgconfig['big_img_linkstyle'];
	$pictureTplData['keep_bigimg'] = intval($mg->mgconfig['keep_bigimg']);
	$pictureTplData['strings'] = $strings;
	$pictureTplData['path_to_gal'] = $path_to_gal;
	$pics[$i]['title'] = stripslashes($pics[$i]['title']);
	$pics[$i]['descr'] = stripslashes($pics[$i]['descr']);
	$pictureTplData['picture'] = $pics[$i];
	$pictureTplData['counter'] = $pic_number;
	$pictureTplData['total_pics_count'] = $total_pics;
	
	if(file_exists($modx->config['base_path'].$path_to_gal."big_".$pics[$i]['filename'])){
		$imagesize=getimagesize($modx->config['base_path'].$path_to_gal."big_".$pics[$i]['filename']);
		$pictureTplData['picture_height_big'] = $imagesize[1];
		$pictureTplData['picture_width_big'] = $imagesize[0];
		$pictureTplData['big_pic_exists'] = 1;
	} else {
		$pictureTplData['big_pic_exists'] = 0;
	}
	
	$imagesize=getimagesize($modx->config['base_path'].$path_to_gal.$pics[$i]['filename']);
	$pictureTplData['picture_height_normal'] = $imagesize[1];
	$pictureTplData['picture_width_normal'] = $imagesize[0];
	
	$imagesize=getimagesize($modx->config['base_path'].$path_to_gal."tn_".$pics[$i]['filename']);
	$pictureTplData['picture_height_thumb'] = $imagesize[1];
	$pictureTplData['picture_width_thumb'] = $imagesize[0];
	
	//build prev and next links
	$next_pic_querystr = "";
	$previous_pic_querystr = "";
	if($_REQUEST['from_id']){
		$next_pic_querystr .= "&from_id=".$_REQUEST['from_id'];
		$previous_pic_querystr .= "&from_id=".$_REQUEST['from_id'];
	}
	if($mg->mgconfig['is_target']==true && $_REQUEST['gal_id']){
		$next_pic_querystr .= "&gal_id=".$_REQUEST['gal_id'];
		$previous_pic_querystr .= "&gal_id=".$_REQUEST['gal_id'];
	}
	$next_pic = $modx->makeUrl($modx->documentIdentifier, '', "pic=".$pics[$i+1]['id'].$next_pic_querystr);
	$previous_pic = $modx->makeUrl($modx->documentIdentifier, '', "pic=".$pics[$i-1]['id'].$previous_pic_querystr);
	
	//build back to index link
	if($_REQUEST['from_id']){
		$back_to_index = $modx->makeUrl($_REQUEST['from_id'], '', '');
	}else if($mg->mgconfig['is_target']==true && $_REQUEST['gal_id']){
		$back_to_index = $modx->makeUrl($_REQUEST['gal_id'], '', '');
	}else{
		$back_to_index = $modx->makeUrl($modx->documentIdentifier, '', '');
	}
	
	$pictureTplData['index_url'] = $back_to_index;
	
	if($i+1!=1){
		$pictureTplData['previous_pic_url'] = $previous_pic;
	} else {
		$pictureTplData['previous_pic_url'] = $back_to_index;
	}
	
	if($i+1!=count($pics)){
		$pictureTplData['next_pic_url'] = $next_pic;
	} else {
		$pictureTplData['next_pic_url'] = $back_to_index;
	}
	
	$tpl = new CChunkie($mg->mgconfig['pictureTpl']);
	$tpl->addVar('maxigallery', $pictureTplData);
	$output =  $tpl->Render();
	$mg->regScriptsAndCSS();
	return;
	
}

$outerTplData = array();

if($mg->checkPermissions($_SESSION['mgrInternalKey'],$pageinfo['id']) && $mg->mgconfig['is_target']==false) {
	if(isset($mg->mgconfig['manage_target']) && $mg->mgconfig['manage_target'] != "") {
		$formaction= $modx->makeUrl($mg->mgconfig['manage_target'], '', "gal_id=".$pageinfo['id']);
	} else if(isset($mg->mgconfig['view_gallery']) && $mg->mgconfig['view_gallery'] != "") {
		$formaction= $modx->makeUrl($mg->mgconfig['view_gallery'], '', "from_id=".$modx->documentIdentifier);
	} else{
		$formaction= $modx->makeUrl($modx->documentIdentifier, '', '');
	} 
	$outerTplData['managebutton'] = '<form action="'.$formaction.'" method="post" name="activateadminform"><input type="hidden" name="mode" value="admin" /><input type="submit" value="'.$strings['manage_pictures'].'" /></form>';
}



//-------------------------------------------------------------------------------------------------
// Childgalleries
//-------------------------------------------------------------------------------------------------

if($mg->mgconfig['display']=="childgalleries") {
	$pageinfo=$modx->getPageInfo($modx->documentIdentifier,1, "id, pagetitle, description, alias, createdby");
	$children = $mg->getAllSubDocuments($pageinfo['id'], "menuindex", "asc", $mg->mgconfig['childgalleries_level_limit'], true, false);
	
	$counter = 1;
	if (count($children) > 0) {
		foreach($children as $child) {
		
			$rs = $modx->db->select("*", $modx->db->config['table_prefix'].$mg->mgconfig['gtable'], "gal_id='" . $child['id'] . "'",$mg->mgconfig['order_by'].' '.$mg->mgconfig['order_direction'],"1");
			$thumb = array();
			while ($row = $modx->db->getRow($rs)) {
		 		array_push($thumb, $row);
			}
			if($thumb[0]['filename']=="") continue;
		
			$childgalleryTplData = array();
			$childgalleryTplData['strings'] = $strings;
			$childgalleryTplData['pageinfo'] = $child;
			$thumb[0]['title'] = stripslashes($thumb[0]['title']);
			$thumb[0]['descr'] = stripslashes($thumb[0]['descr']);
			$childgalleryTplData['picture'] = $thumb[0]; 
			$childgalleryTplData['childurl'] = $modx->makeUrl($child['id'], '', '');
			$childgalleryTplData['path_to_gal'] = $path_to_galleries.$child['id']."/";
		
			$tpl = new CChunkie($mg->mgconfig['childgalleryPictureTpl']);
			$tpl->addVar('maxigallery', $childgalleryTplData);
			$outerTplData['childgalleries'] .= $tpl->Render();
		
			if($counter == $mg->mgconfig['pics_per_row']) {
				$outerTplData['childgalleries'] .= $tpl->getTemplate($mg->mgconfig['clearerTpl']);
				$counter = 0;
			}
		
			$counter++;
		}
	}
}


//-------------------------------------------------------------------------------------------------
// Display gallery thumbnails
//-------------------------------------------------------------------------------------------------

// --- Add by Marc:
if ($mg->mgconfig['debug']) echo"Debug >> ". __LINE__." - // Show gallery overview with thumbnails<br />\n";
if ($mg->mgconfig['debug']) echo"|--- Pics_in_a_row: ".$mg->mgconfig['pics_in_a_row']."<br />\n";
if ($mg->mgconfig['debug']) echo"|--- Pics per page: ".$mg->mgconfig['pics_per_page']."<br />\n";
if ($mg->mgconfig['debug']) echo"|--- Only_page_numbers: ".$mg->mgconfig['only_page_numbers']."<br />\n";
if ($mg->mgconfig['debug']) echo"|--- Display: ".$mg->mgconfig['display']."<br />\n";
if ($mg->mgconfig['debug']) echo"|--- Embedtype: ".$mg->mgconfig['embedtype']."<br />\n";
// --- End add by Marc

$rs=$modx->db->select("*", $modx->db->config['table_prefix'].$mg->mgconfig['gtable'], "gal_id='" . $pageinfo['id'] . "'",$mg->mgconfig['order_by'].' '.$mg->mgconfig['order_direction']);
$pics = array();
while ($row = $modx->db->getRow($rs)) {
	 array_push($pics, $row);
}
$offset = 0;
//if pictures per page limit is set, make the page navigator
if(isset($mg->mgconfig['pics_per_page']) && $mg->mgconfig['pics_per_page']!=0){
	// get pager values 
    if($_REQUEST['page']){
		$page = $_REQUEST['page']; 
	}else{
		$page = 1;
	}
    $total = count($pics);  
	$pager  = $mg->getPagerData($total, $mg->mgconfig['pics_per_page'], $page);
	
	$outerTplData['currentpage'] = $page;
	$outerTplData['pagecount'] = $pager['numPages'];
	 
	$outerTplData['previous_page_url'] = $mg->getPaginationUrl($modx->documentIdentifier, '', array("page" => $page-1));
	$outerTplData['next_page_url'] = $mg->getPaginationUrl($modx->documentIdentifier, '', array("page" => $page+1));

	//page numbers
	for ($i=1;$i<=$pager['numPages'];$i++) { 
		
		$pageNumberTplData = array();
		$pageNumberTplData['pageurl'] = $mg->getPaginationUrl($modx->documentIdentifier,'',array("page" => $i));
		$pageNumberTplData['pagenumber'] = $i;
		$pageNumberTplData['pagecount'] = $pager['numPages'];
		$pageNumberTplData['currentpage'] = $pager['page']; 
		
		$tpl = new CChunkie($mg->mgconfig['pageNumberTpl']);
		$tpl->addVar('maxigallery', $pageNumberTplData);
		$outerTplData['pagenumbers'] .= $tpl->Render();
		
	}

	if($_REQUEST['page']){
		$offset = $pager['offset']; 
	}
}
//figure out the limit value for pics to get
if(!isset($mg->mgconfig['pics_per_page']) || $mg->mgconfig['pics_per_page']==0 || ($offset+$mg->mgconfig['pics_per_page']>count($pics))){
	$limit=count($pics);
}else{
	$limit=$offset+$mg->mgconfig['pics_per_page'];
}
//print out gallery overview
$counter = 1;
for($i=$offset;$i<$limit;$i++){
	
	$pictureTplData = array();
	
	$pictureTplData['strings'] = $strings;
	$pictureTplData['pageinfo'] = $pageinfo;
	$pictureTplData['embedtype'] = $mg->mgconfig['embedtype'];
	$pictureTplData['path_to_gal'] = $path_to_gal;
	$pics[$i]['title'] = stripslashes($pics[$i]['title']);
	$pics[$i]['descr'] = stripslashes($pics[$i]['descr']);
	$pictureTplData['picture'] = $pics[$i];
	
	if(isset($mg->mgconfig['picture_target']) && $mg->mgconfig['picture_target'] != "") {
		$pictureTplData['picture_link_url'] = $modx->makeUrl($mg->mgconfig['picture_target'], '', "&gal_id=".$pageinfo['id']."&pic=".$pics[$i]['id']);
	} else if(isset($mg->mgconfig['view_gallery']) && $mg->mgconfig['view_gallery'] != "") {
		$pictureTplData['picture_link_url'] = $modx->makeUrl($pageinfo['id'], '', "&pic=".$pics[$i]['id']."&from_id=".$modx->documentIdentifier);
	} else {
		$pictureTplData['picture_link_url'] = $modx->makeUrl($pageinfo['id'], '', "&pic=".$pics[$i]['id']);
	}
	
	if(file_exists($modx->config['base_path'].$path_to_gal."big_".$pics[$i]['filename'])){
		$imagesize=getimagesize($modx->config['base_path'].$path_to_gal."big_".$pics[$i]['filename']);
		$pictureTplData['picture_height_big'] = $imagesize[1];
		$pictureTplData['picture_width_big'] = $imagesize[0];
		$pictureTplData['big_pic_exists'] = 1;
	} else {
		$pictureTplData['big_pic_exists'] = 0;
	}
	
	$imagesize=getimagesize($modx->config['base_path'].$path_to_gal.$pics[$i]['filename']);
	$pictureTplData['picture_height_normal'] = $imagesize[1];
	$pictureTplData['picture_width_normal'] = $imagesize[0];
	
	$imagesize=getimagesize($modx->config['base_path'].$path_to_gal."tn_".$pics[$i]['filename']);
	$pictureTplData['picture_height_thumb'] = $imagesize[1];
	$pictureTplData['picture_width_thumb'] = $imagesize[0];

	$tpl = new CChunkie($mg->mgconfig['galleryPictureTpl']);
	$tpl->addVar('maxigallery', $pictureTplData);
	$outerTplData['pictures'] .= $tpl->Render();
	
	if($counter == $mg->mgconfig['pics_per_row']) {
		$outerTplData['pictures'] .= $tpl->getTemplate($mg->mgconfig['clearerTpl']);
		$counter = 0;
	}
	
	$counter++;
}

$tpl = new CChunkie($mg->mgconfig['galleryOuterTpl']);
//add data to the template
$outerTplData['embedtype'] = $mg->mgconfig['embedtype'];
$outerTplData['pageinfo'] = $pageinfo;
$outerTplData['strings'] = $strings;
$tpl->addVar('maxigallery', $outerTplData);
//render template and return output
$output =  $tpl->Render();
//add slideshow div css
if ($mg->mgconfig['display'] == "embedded" && $mg->mgconfig['embedtype'] == "slideshow") {
	$mg->regSlideshowCSS($pageinfo['id']);
}
$mg->regScriptsAndCSS();
return;
?>
