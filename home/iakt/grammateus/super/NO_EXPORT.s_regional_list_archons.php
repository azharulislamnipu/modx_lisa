<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Filter
$tfi_listrsArchons2 = new TFI_TableFilter($sigma_modx, "tfi_listrsArchons2");
$tfi_listrsArchons2->addColumn("STATUS", "STRING_TYPE", "STATUS", "%");
$tfi_listrsArchons2->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listrsArchons2->addColumn("JOINDATE", "DATE_TYPE", "JOINDATE", "=");
$tfi_listrsArchons2->addColumn("LASTUPDATED", "DATE_TYPE", "LASTUPDATED", "=");
$tfi_listrsArchons2->Execute();

// Sorter
$tso_listrsArchons2 = new TSO_TableSorter("rsArchons", "tso_listrsArchons2");
$tso_listrsArchons2->addColumn("STATUS");
$tso_listrsArchons2->addColumn("FULLNAME");
$tso_listrsArchons2->addColumn("JOINDATE");
$tso_listrsArchons2->addColumn("LASTUPDATED");
$tso_listrsArchons2->setDefault("FULLNAME");
$tso_listrsArchons2->Execute();

// Navigation
$nav_listrsArchons2 = new NAV_Regular("nav_listrsArchons2", "rsArchons", "../../", $_SERVER['PHP_SELF'], 5);

// Begin List Recordset
$maxRows_rsArchons = $_SESSION['max_rows_nav_listrsArchons2'];
$pageNum_rsArchons = 0;
if (isset($_GET['pageNum_rsArchons'])) {
  $pageNum_rsArchons = $_GET['pageNum_rsArchons'];
}
$startRow_rsArchons = $pageNum_rsArchons * $maxRows_rsArchons;
$colname__rsArchons = '-1';
if (isset($_SESSION['REGIONNAME'])) {
  $colname__rsArchons = $_SESSION['REGIONNAME'];
}
// Defining List Recordset variable
$NXTFilter__rsArchons = "1=1";
if (isset($_SESSION['filter_tfi_listrsArchons2'])) {
  $NXTFilter__rsArchons = $_SESSION['filter_tfi_listrsArchons2'];
}
// Defining List Recordset variable
$NXTSort__rsArchons = "FULLNAME";
if (isset($_SESSION['sorter_tso_listrsArchons2'])) {
  $NXTSort__rsArchons = $_SESSION['sorter_tso_listrsArchons2'];
}
$query_rsArchons = sprintf("SELECT * FROM vw_web_elig WHERE vw_web_elig.REGIONNAME = %s AND {$NXTFilter__rsArchons} ORDER BY {$NXTSort__rsArchons} ", GetSQLValueString($colname__rsArchons, "text"));
$rsArchons = $sigma_modx->SelectLimit($query_rsArchons, $maxRows_rsArchons, $startRow_rsArchons) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsArchons'])) {
  $totalRows_rsArchons = $_GET['totalRows_rsArchons'];
} else {
  $all_rsArchons = $sigma_modx->SelectLimit($query_rsArchons) or die($sigma_modx->ErrorMsg());
  $totalRows_rsArchons = $all_rsArchons->RecordCount();
}
$totalPages_rsArchons = (int)(($totalRows_rsArchons-1)/$maxRows_rsArchons);
// End List Recordset

// begin Recordset
$query_rsStatusTypes = "SELECT    vw_gramm_archons.`STATUS` FROM   vw_gramm_archons GROUP BY   vw_gramm_archons.`STATUS`";
$rsStatusTypes = $sigma_modx->SelectLimit($query_rsStatusTypes) or die($sigma_modx->ErrorMsg());
$totalRows_rsStatusTypes = $rsStatusTypes->RecordCount();
// end Recordset

$nav_listrsArchons2->checkBoundries();

//PHP ADODB document - made with PHAkt 3.7.1
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_STATUS {width:126px; overflow:hidden;}
  .KT_col_FULLNAME {width:280px; overflow:hidden;}
  .KT_col_JOINDATE {width:70px; overflow:hidden;}
  .KT_col_LASTUPDATED {width:70px; overflow:hidden;}
.style4 {
	font-size: 18px;
	margin-bottom: 4px;
	margin-top: -4px;
}
.style6 {font-size: 12px}
.style8 {
	font-size: 11px;
	margin-top: -2px;
}
</style>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng" id="listrsArchons2">
  <h1><?php echo strtoupper($_GET['archonStatus']); ?> ARCHONS | Displaying 
    <?php
  $nav_listrsArchons2->Prepare();
  require("../../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listrsArchons2->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
            <?php 
  // Show IF Conditional region1 
  if (@$_GET['show_all_nav_listrsArchons2'] == 1) {
?>
              <?php echo $_SESSION['default_max_rows_nav_listrsArchons2']; ?>
              <?php 
  // else Conditional region1
  } else { ?>
              <?php echo NXT_getResource("all"); ?>
              <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>            </th>
            <th id="STATUS" class="KT_sorter KT_col_STATUS <?php echo $tso_listrsArchons2->getSortIcon('STATUS'); ?>"> <a href="<?php echo $tso_listrsArchons2->getSortLink('STATUS'); ?>">STATUS</a> </th>
            <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listrsArchons2->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listrsArchons2->getSortLink('FULLNAME'); ?>">FULL NAME</a> </th>
            <th id="JOINDATE" class="KT_sorter KT_col_JOINDATE <?php echo $tso_listrsArchons2->getSortIcon('JOINDATE'); ?>"> <a href="<?php echo $tso_listrsArchons2->getSortLink('JOINDATE'); ?>">JOIN DATE</a> </th>
            <th id="LASTUPDATED" class="KT_sorter KT_col_LASTUPDATED <?php echo $tso_listrsArchons2->getSortIcon('LASTUPDATED'); ?>"> <a href="<?php echo $tso_listrsArchons2->getSortLink('LASTUPDATED'); ?>">LAST UPDATED</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3 
  if (@$_SESSION['has_filter_tfi_listrsArchons2'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listrsArchons2_FULLNAME" id="tfi_listrsArchons2_FULLNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchons2_FULLNAME']); ?>" size="35" maxlength="20" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="submit" name="tfi_listrsArchons2" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsArchons == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="6"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsArchons > 0) { // Show if recordset not empty ?>
            <?php
  while (!$rsArchons->EOF) { 
?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_vw_gramm_archons" class="id_checkbox" value="<?php echo $rsArchons->Fields('CUSTOMERID'); ?>" />
                    <input type="hidden" name="CUSTOMERID" class="id_field" value="<?php echo $rsArchons->Fields('CUSTOMERID'); ?>" />                </td>
                <td><div class="KT_col_STATUS">
				<p class="style6"><strong><?php echo $rsArchons->Fields('BOULENAME'); ?></strong></p>
				<span class="style6"><?php echo KT_FormatForList($rsArchons->Fields('STATUS'), 18); ?></span><br />
                  <br />
                </div></td>
                <td><div class="KT_col_FULLNAME">
                  <p class="style4"><?php echo $rsArchons->Fields('L2R_FULLNAME'); ?></p>
                      <p class="style6"><?php echo $rsArchons->Fields('ADDRESS1'); ?><?php echo $rsArchons->Fields('ADDRESS2'); ?>, <?php echo $rsArchons->Fields('CITY'); ?>, <?php echo $rsArchons->Fields('STATECD'); ?> <?php echo $rsArchons->Fields('ZIP'); ?></p>
<p class="style8">
  <?php 
// Show IF Conditional region7 
if (@$rsArchons->Fields('HOMEPHONE') != NULL) {
?>
  HOME PHONE:   <span class="style6"><?php echo $rsArchons->Fields('HOMEPHONE'); ?></span>
    <?php 
// else Conditional region7
} else { ?>
    <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>&amp;KT_back=1#homephone">Add Home Phone</a>
  <?php } 
// endif Conditional region7
?><br>
EMAIL: <span class="style6"><a href="mailto:<?php echo $rsArchons->Fields('WEB_EMAIL'); ?>"><?php echo $rsArchons->Fields('WEB_EMAIL'); ?></a></span> [ <a href="../admin/edit_archon.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>">edit</a> ] </p>
<p class="style8"> 
  <?php 
// Show IF Conditional region8 
if (@$rsArchons->Fields('SPOUSENAME') != NULL) {
?>
    ARCHOUSA:
        <span class="style6"><?php echo $rsArchons->Fields('SPOUSENAME'); ?></span> [ <a href="/home/edit_archousa.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>&amp;KT_back=1">edit</a> ]
        <?php 
// else Conditional region8
} else { ?>
    <a href="/home/edit_archousa.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Add Archousa</a>
  <?php } 
// endif Conditional region8
?></p>

                  
                </div></td>
                <td><div class="KT_col_JOINDATE">
                  <p class="style8">JOIN DATE<br>
                      <?php 
// Show IF Conditional region11 
if (@$rsArchons->Fields('JOINDATE') != NULL) {
?>
                        <span class="style6"><?php echo KT_formatDate($rsArchons->Fields('JOINDATE')); ?></span>
                        <?php 
// else Conditional region11
} else { ?>
                        <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>&amp;KT_back=1#joindate">Add Date</a>
  <?php } 
// endif Conditional region11
?></p>
                  <p class="style8">BIRTH DATE<br>
					  <?php 
// Show IF Conditional region6 
if (@$rsArchons->Fields('BIRTHDATE') != NULL) {
?>
					    <span class="style6"><?php echo KT_formatDate($rsArchons->Fields('BIRTHDATE')); ?></span>
					    <?php 
// else Conditional region6
} else { ?>
					    <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>&amp;KT_back=1#birthdate">Add Date</a>
  <?php } 
// endif Conditional region6
?>
</p>
                  <?php  // Show IF Conditional region4 
if (@$_GET['archonStatus'] == "Deceased" || @$rsArchons->Fields('DECEASEDDATE') != NULL) {
?>
                  <p class="style8"> DECEASED<br>
                      <?php 
// Show IF Conditional region5 
if (@$rsArchons->Fields('DECEASEDDATE') != NULL) {
?>
                      <span class="style6"><?php echo $rsArchons->Fields('DECEASEDDATE'); ?></span>
                      <?php 
// else Conditional region5
} else { ?>
                      <a href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>&amp;KT_back=1#deceaseddate">Add Date</a>
                      <?php } 
// endif Conditional region5
?>
                  </p>
                  <?php } 
// endif Conditional region4
?>
                </div></td>
                <td><div class="KT_col_LASTUPDATED"><p class="style8">BY ARCHON<br><span class="style6"><?php echo KT_formatDate($rsArchons->Fields('LASTUPDATED')); ?></span></p>
				<p class="style8">BY GRAMM<br><span class="style6"><?php echo KT_formatDate($rsArchons->Fields('GRAMMUPDATE')); ?></span></p></div></td>
                <td><a class="KT_edit_link" href="/home/edit_archon.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Edit Archon Data</a><br />
				<?php 
// Show IF Conditional region3 
if (@$_GET['archonStatus'] != "Deceased") {
?>
                    <a href="/home/edit_deceased.php?CUSTOMERID=<?php echo $rsArchons->Fields('CUSTOMERID'); ?>&amp;KT_back=1">Mark Deceased</a>
                    <?php } 
// endif Conditional region3
?></td>
              </tr>
              <?php
    $rsArchons->MoveNext(); 
  }
?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsArchons2->Prepare();
            require("../../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a></div>
        <span>&nbsp;</span></div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchons->Close();

$rsStatusTypes->Close();
?>
