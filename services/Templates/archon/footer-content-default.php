            <section class="span-4">
    <div class="inner">
        <h2>Contact Us</h2>
        <p>
            <strong>ADDRESS</strong>
            <br> 
			260 Peachtree Street, NW<br/> Suite 1604 <br/>
Atlanta, Georgia 30303
        </p>
        <p>
            <strong>PHONE</strong>
            <br>
			(404) 529-9919
        </p>
        <p>
            <strong>EMAIL</strong>
            <br>
			info@sigmapiphi.org
        </p>
    </div>
</section>
<section class="span-4">
    <div class="inner">
        <h2>Navigation</h2>
        <nav role="navigation">
			<div class="span-6">
				<ul class="lines">
	
<li><a href="/home/boule-journal.php">Boul&eacute; Journal</a></li><li><a href="/home/contact_us.php">Contact Us</a></li><li><a href="/home/login.php">Login</a></li><li><a href="/home/search-results-new.php">Search</a></li><li><a href="/home/home.php">Visitors</a></li>
	
</ul>
			</div>
			<div class="span-6">
				<ul class="lines">

<li><a href="/home/53rd-grand-boule-1.php">53rd Grand Boul&#233;</a></li><li><a href="/home/advanced-search-4.php">Archon Directory</a></li><li><a href="/home/officers1.php">Officers</a></li><li><a href="/home/personal-profile.php">Personal Profile</a></li><li><a href="/home/the-grand-boule-nav.php">The Grand Boul&eacute;</a></li>
</ul>
			</div>
		</nav>
    </div>
</section>
<section class="span-4">
    <div class="inner">
        <h2>The Fraternity</h2>
		<p>
			Modeling leadership, education and civic responsibility by supporting these endeavors and by mentoring the young men who will advance the African American community in all facets of equality, mutual respect and devotion to democratic traditions.
		</p>
        <p class="align-center margin-top-large">
			<img src="/home/assets/templates/archon/images/footer-sphinx.png" alt="" >
			<img src="/home/assets/templates/archon/images/footer-logo.png" alt="Sigma Pi Phi Fraternity">
        </p>
    </div>
</section>
</div>

<p class="copyright">&copy;2006-<?php echo date("Y"); ?>, Sigma Pi Phi Fraternity</p>