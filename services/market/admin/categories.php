<?php
require_once('../includes/admin/access.php');
require_once('../includes/admin/header.php');
?>


<script language="JavaScript" >
		function newCategory(){
			d = document.category;
			d.cid.value = "";
			d.cname.value = "";
			d.corder.value = "";
			d.cparent.value = "";
			d.cdesc.value = "";
			d.action.value ="new";
			d.submitCat.value ="<?php echo T_ADD_CAT;?>";
			show("formCat");
		}	
		function editCategory(cid, cname, corder,cparent){
			d = document.category;
			d.cid.value = cid;
			d.cname.value = cname;
			d.corder.value = corder;
			d.cparent.value = cparent;
			d.cdesc.value = document.getElementById('desc-'+cid).innerHTML;//cdesc;
			d.action.value ="edit";
			d.submitCat.value ="<?php echo T_UPDATE_CAT;?>";
			show("formCat");
		}	
		function deleteCategory(category, label){
			if (confirm('<?php echo T_DELETE_CAT;?> "'+label+'"?'))
			window.location = "categories.php?action=delete&cid=" + category;
		}
</script>

<h3><?php echo T_CATEGORIES; ?></h3>

<?php

function catSlug($name){ //try to prevent repeated categories
	global $ocdb;
	$name=friendly_url($name);
	
	$res=$ocdb->getValue("SELECT friendlyName FROM ".TABLE_PREFIX."categories where friendlyName='$name' limit 1","none");
	if ($res!=false){//exists try adding by category
		$name.='-'.cP("cparent");  
		$res=$ocdb->getValue("SELECT friendlyName FROM ".TABLE_PREFIX."categories where friendlyName='$name' limit 1","none");//now with the new cat name
	}
	
	if ($res==false) return $name;
	else return false;	
}

//actions
if (cP("action")!=""||cG("action")!=""){
	$action=cG("action");
	if ($action=="")$action=cP("action");
	
	if ($action=="new"){
		$nameSlug=catSlug(cP("cname"));
		if ($nameSlug!=false){  //no exists insert
			$ocdb->insert(TABLE_PREFIX."categories (name,friendlyName,`order`,idCategoryParent,description)",
						"'".cP("cname")."','$nameSlug',".cP("corder").",".cP("cparent").",'".cP("cdesc")."'");
		}
		else echo "Category name repeated";
		
	}
	elseif ($action=="delete"){
		$ocdb->delete(TABLE_PREFIX."categories","idCategory=".cG("cid"));
		//echo "Deleted";
	}
	elseif ($action=="edit"){
		$nameSlug=catSlug(cP("cname"));
		if ($nameSlug!=false){  //no exists update
			$query="update ".TABLE_PREFIX."categories set name='".cP("cname")."',friendlyName='$nameSlug'
			,`order`=".cP("corder").",idCategoryParent=".cP("cparent").",description='".cP("cdesc")."' 
			where idCategory=".cP("cid");
			$ocdb->query($query);
		}
		else echo "Category name repeated";
		//echo "Edit: $query";
	}
	
	if (CACHE_DEL_ON_CAT) deleteCache();//delete cache on category if is activated
	if (SITEMAP_DEL_ON_CAT) generateSitemap();//new/update cat generate sitemap
	
}

?>
	
	  <p><?php echo T_CAT_DESC;?></p>
	  
	  <table>
	  
	    <tr> 
	      <td><strong><?php echo T_NAME;?></strong></td>
	      <td><strong><?php echo T_ORDER;?></strong></td>
	      <td><strong><?php echo T_PARENT;?></strong></td>
		  <td><strong><?php echo T_ACTIONS;?></strong></td>
	    </tr>

	  <?php 
		$result = $ocdb->query("SELECT *,(select name from ".TABLE_PREFIX."categories  where idCategory=C.idCategoryParent) ParentName 
							FROM ".TABLE_PREFIX."categories C order by  idCategoryParent,'order'");
			while ($row = mysql_fetch_array($result)){
				$name = $row["name"] ;
				$desc = $row["description"];
				$order =  $row["order"];
				$idCategory=$row["idCategory"];
				$parent=$row["idCategoryParent"];
				$parentName=$row["ParentName"];
				if ($parentName=="") $parentName="Home";
	
	?>
	    <tr>      
	      <td><?php echo $name;?></td>
	      <td><?php echo $order;?></td>
	      <td><?php echo $parentName;?></td>
		  <td><a href="" onClick="editCategory('<?php echo $idCategory; ?>', '<?php echo $name;?>', '<?php echo $order;?>','<?php echo $parent;?>');return false;"><?php echo T_EDIT;?></a> 
		    | <a href="" onClick="deleteCategory('<?php echo $idCategory;?>','<?php echo $name; ?>');return false;"  ><?php echo T_DELETE;?></a>
		    <div style="display:none;" id="desc-<?php echo $idCategory; ?>"><?php echo $desc;?></div>
		    </td>
	    </tr>
	    
		<?php } ?>
		<tr><td colspan="4" align="right"><a href="" onClick="newCategory();return false;"><?php echo T_ADD_NEW_CAT;?></a></td></tr>
	  </table>
	<br />
	<br />
	
	
	<div id='formCat' style="width:320px;border:2px solid #6699ff; display:none;">
		<form name="category" action="categories.php" method="post" onsubmit="return checkForm(this);">
		<table>
		<tr>
			<td><strong><?php echo T_NAME;?></strong></td>
			<td><strong><?php echo T_ORDER;?></strong></td>
			<td><strong><?php echo T_PARENT;?></strong></td>
			
		</tr>
		<tr>
			<td><input type="text" name="cname" size="12" onblur="validateText(this);" lang="false"></td>
			<td><input type="text" name="corder" value="" size="2" maxlength="5"  onblur="validateNumber(this);" onkeypress="return isNumberKey(event);" lang="false" ></td>
			<td><?php sqlOption("select idCategory,name from ".TABLE_PREFIX."categories where idCategoryParent=0","cparent","");?></td>
		</tr>
		<tr>
			<td colspan="3">
				<strong><?php echo T_DESCRIPTION;?>:</strong><br />
				<textarea cols="40" name="cdesc" ></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2"><a href="" onClick="hide('formCat');return false;" ><?php echo T_CANCEL;?></a></td>
			<td align="right"><input id="submitCat" type="submit" value=""></td>
		</tr>
		</table>
	 	<input type="hidden" name="cid" value="">
		<input type="hidden" name="action" value="">
		</form>
	
	</div>



<?php
require_once('../includes/admin/footer.php');
?>