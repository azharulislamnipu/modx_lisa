<?php get_header(); ?>

	<div id="content">
		<div class="padder width padding-top-none-20">
			
			<div class="width padding-top-none-20 divider">

		<?php do_action( 'sigma_before_blog_search' ); ?>
		
		<div class="row grid-12">

		<div class="page" id="blog-search" role="main">
			
	   	 <div class="span-9 float-right">
	   		 <div class="inner">

			<?php if (have_posts()) : ?>

				<h2 class="purple color"><?php _e( 'Search Results', 'archon-network' ); ?></h2>
				<div class="contentBlock">

					<ul class="link-list">
					<?php
					 	while (have_posts()) : the_post();
							if ( can_view_content() ) : ?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php _e('Read', 'archon-network') ?>">
										<strong><?php the_title(); ?></strong></a>
										<span class="excerpt"><?php the_excerpt(); ?></span>
									
								</li>
							<?php endif; ?>
						<?php endwhile; ?>
					</ul>

				</div>

			<?php else : ?>

				<h2 class="purple color"><?php _e( 'No matching content found...', 'archon-network' ); ?></h2>
				<div class="contentBlock">
				<?php get_search_form(); ?>
				</div>

			<?php endif; ?>
		

		</div>
		
			</div><!--inner-->
		</div><!--span-9-->
		
		 <div class="span-3 float-left">
			 <div class="inner">
				 <?php get_sidebar(); ?>
			</div><!-- .inner -->
		</div><!-- .span-3 -->
		
		</div><!-- .grid-12 -->

		<?php do_action( 'sigma_after_blog_search' ); ?>

		</div><!-- .padder -->
		
		</div><!-- .padder -->
		
	    <!-- 3 round images -->
	    <div class="row grid-12 margin-top-medium padding">
				<div class="width padding">
			<div class="round">
		
		<?php if ( is_active_sidebar( 'three_round_images' ) ) : ?>
		
			<?php dynamic_sidebar( 'three_round_images' ); ?>
		 
			<?php endif; ?>
			</div><!-- #width padding -->
		</div><!-- #round -->
			</div><!-- #row grid 12 round images  -->
	</div><!-- #content -->

	<?php //get_sidebar(); ?>

<?php get_footer(); ?>
