<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("BIO", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an update transaction instance
$upd_spp_archons = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/personal-profile.php");
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");
$upd_spp_archons->addColumn("BIO", "STRING_TYPE", "POST", "BIO");
$upd_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "VALUE", "{POST.UPDATEDBY}");
$upd_spp_archons->addColumn("IPADDRESS", "STRING_TYPE", "VALUE", "{SERVER.REMOTE_ADDR}");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$row_rsspp_archons = mysql_fetch_assoc($rsspp_archons);
$totalRows_rsspp_archons = mysql_num_rows($rsspp_archons);
?><!DOCTYPE html>
<html class="no-js"><!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --> <!--<![endif]-->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-meta.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-css.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-javascript.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-links.php"); ?>

    <!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>

<body class="home blue-bar color-override-yellow-orange">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/main-nav.php"); ?>
			<div class="width">
			    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/global-logo-tagline.php"); ?>
			</div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
	<!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- InstanceBeginEditable name="pagetitle" -->
        <section class="span-12 margin-bottom-medium">
          <div class="inner">
               <h2>Profile</h2>
              <ol class="breadcrumbs" style="margin-top:-18px;">
                <li><a class="B_homeCrumb" href="/home/archon-home.php" title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                <li><span class="B_currentCrumb">Administration</span></li>
              </ol>
         </div>
      </section>
           <!-- InstanceEndEditable -->
                               
   <section class="span-12 margin-bottom-medium">
      <div class="inner">
        <!-- InstanceBeginEditable name="content" -->
        <?php echo $tNGs->displayValidationRules();?>
          <h2 class="yellow">Edit Biography</h2>
          <div class="contentBlock">

            <?php
          	echo $tNGs->getErrorMsg();
            ?>
            <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
                <table cellpadding="2" cellspacing="0" class="KT_tngtable">
                  <tr>
                    <td ><!-- <label for="BIO">BIOGRAPHY : </label> --><h4> BIOGRAPHY <a style="color:red;">*</a> :</h4></td>
                    
                  </tr>
                  <tr>
                   <!--  <td class="KT_th"><h4> BIOGRAPHY <a style="color:red;">*</a> :</h4></td> -->
                    <td><textarea name="BIO" id="BIO" cols="90" rows="10"><?php echo KT_escapeAttribute($row_rsspp_archons['BIO']); ?></textarea>
                        <?php echo $tNGs->displayFieldHint("BIO");?> <?php echo $tNGs->displayFieldError("spp_archons", "BIO"); ?> </td>
                  </tr>

                  <tr class="KT_buttons">
                    <td colspan="6"><input type="submit" name="KT_Update1" id="KT_Update1" value="Update Bio"/>
                      <input type="button" name="Cancel" id="KT_Updatecancel1" value="Cancel" onclick="history.go(-1)"/>
                     <!--  <span class="font-small" ><a href="#" name="KT_Update1" id="KT_Update1"  class="button">Update Bio</a></span>
                       <span class="font-small" ><a href="#" name="Cancel"  class="button">Cancel</a></span> -->
            		       <!--  <input type="button" name="Cancel" value="Cancel" onclick="history.go(-1)" />      -->   
                    </td>
                  </tr>

                </table>
                    
                <input type="hidden" name="CUSTOMERID" id="CUSTOMERID" value="<?php echo KT_escapeAttribute($_POST['CUSTOMERID']); ?>" />
            </form>
            <p>&nbsp;</p>
          </div>
            <!-- InstanceEndEditable -->
        </div>
    </section>
           
           <!-- end showing the content, pagetitle, breadcrumb and sidebar -->
             <!-- 3 round images toggle start -->
             <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/row-spotlight-grid-12.php"); ?>
             <!-- 3 round images toggle end -->
       </div>
    </div>

            <!-- main content body end -->	
	
	<!-- third section event and articles start -->
  	 <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
	<!-- third section event and articles end -->
	
    <footer class="width padding" style="border-top: 10px #52B609 solid;">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-content-default.php"); ?>
      </div>
    </footer>
	
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-scripts.php"); ?>
</body>
<!-- InstanceEnd --></html>
