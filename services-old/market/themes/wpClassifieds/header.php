<script type="text/javascript" src="<?php echo SITE_URL; ?>/themes/wpClassifieds/jsclass.js"></script>
<?php 
//getting the title in two parts/colors
$pos=strpos(SITE_NAME," ");
$firstH=substr(SITE_NAME,0,$pos);//first part of the site name in green
$secondH=substr(SITE_NAME,$pos);//second part of the name un blue
?>

<div class="container_12" id="wrap">
  <div class="grid_12" id="header">
    <div id="logo"> 
      <a href="<?php echo SITE_URL; ?>" title="<?php echo SITE_NAME; ?>">
       <h4><span class="firstH"><?php echo $firstH; ?></span><span class="secondH"><?php echo $secondH; ?></span></h4>
      </a> 
      <p><?php echo '<a title="'.T_NEW_ITEM.'" href="'.SITE_URL.newURL().'">'.T_NEW_ITEM.'</a>';?></p>
      <div class="clear"></div>
    </div>
  </div>
  <div class="clear"></div>
  <div class="grid_12" id="top_dropdown">
    <ul id="nav">
    <?php generateMenuJS($selectedCategory);?>
    </ul>
  </div>
  <div class="clear"></div>
  <div class="grid_12" style="position:static;" id="top_cats">
    <?php generateSubMenuJS($idCategoryParent,$categoryParent,$currentCategory);  ?>
  </div>
  <div class="clear"></div>
  <div id="content">
     <div class="grid_12">
      <div class=" breadcrumb">
            <?php if(isset($categoryName)&&isset($categoryDescription)){ ?>
			    <?php echo $categoryDescription;?>
			    <a title="<?php echo T_POST_IN;?> <?php echo $categoryName;?>" href="<?php echo SITE_URL.newURL();?>?category=<?php echo $currentCategory;?>"><?php echo T_POST_IN;?> <?php echo $categoryName;?></a> 
	        <?php }
	            else echo date("l d, F Y");
	        ?>
            <div style="float:right;"><b><?php echo T_FILTER;?></b>:
		    <?php generatePostType($currentCategory,$type); ?>
		    </div>
		</div>
    </div>
    <div class="clear"></div>
       <div class="grid_8" id="content_main">
   
