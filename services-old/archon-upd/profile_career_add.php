<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("OCCUPATIONID", true, "numeric", "", "", "", "");
$formValidation->addField("TITLEID", true, "numeric", "", "", "", "");
$formValidation->addField("COMPANYNAME", true, "text", "", "", "", "");
$formValidation->addField("COMPANYCITY", true, "text", "", "", "", "");
$formValidation->addField("COMPANYSTCD", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsProfession = "SELECT OCCUPATIONID, `DESCRIPTION` FROM spp_occupation ORDER BY `DESCRIPTION` ASC";
$rsProfession = mysql_query($query_rsProfession, $sigma_modx) or die(mysql_error());
$row_rsProfession = mysql_fetch_assoc($rsProfession);
$totalRows_rsProfession = mysql_num_rows($rsProfession);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsTitles = "SELECT SKILLID, `DESCRIPTION` FROM spp_skillref ORDER BY `DESCRIPTION` ASC";
$rsTitles = mysql_query($query_rsTitles, $sigma_modx) or die(mysql_error());
$row_rsTitles = mysql_fetch_assoc($rsTitles);
$totalRows_rsTitles = mysql_num_rows($rsTitles);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = mysql_query($query_rsStates, $sigma_modx) or die(mysql_error());
$row_rsStates = mysql_fetch_assoc($rsStates);
$totalRows_rsStates = mysql_num_rows($rsStates);

// Make an insert transaction instance
$ins_spp_prof = new tNG_insert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_prof);
// Register triggers
$ins_spp_prof->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_prof->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_prof->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/personal-profile.php");

// Add columns
$ins_spp_prof->setTable("spp_prof");
$ins_spp_prof->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID", "{POST.CUSTOMERID}");
$ins_spp_prof->addColumn("OCCUPATIONID", "NUMERIC_TYPE", "POST", "OCCUPATIONID");
$ins_spp_prof->addColumn("TITLEID", "NUMERIC_TYPE", "POST", "TITLEID");
$ins_spp_prof->addColumn("COMPANYNAME", "STRING_TYPE", "POST", "COMPANYNAME");
$ins_spp_prof->addColumn("COMPANYCITY", "STRING_TYPE", "POST", "COMPANYCITY");
$ins_spp_prof->addColumn("COMPANYSTCD", "STRING_TYPE", "POST", "COMPANYSTCD");
$ins_spp_prof->addColumn("DESCRIPTION", "STRING_TYPE", "POST", "DESCRIPTION");
$ins_spp_prof->addColumn("COMPANYJOBSTART", "DATE_TYPE", "POST", "COMPANYJOBSTART");
$ins_spp_prof->addColumn("COMPANYJOBEND", "DATE_TYPE", "POST", "COMPANYJOBEND");
$ins_spp_prof->addColumn("CREATEDATE", "DATE_TYPE", "POST", "CREATEDATE", "{now_dt}");
$ins_spp_prof->addColumn("CREATEDBY", "STRING_TYPE", "POST", "CREATEDBY", "{POST.UPDATEDBY}");
$ins_spp_prof->setPrimaryKey("PROFID", "NUMERIC_TYPE");

// Execute all the registered transactions
// echo $ins_spp_prof;
$tNGs->executeTransactions();


// Get the transaction recordset
//$rsspp_prof = $tNGs->getRecordset();

 $rsspp_prof = $tNGs->getRecordset("spp_prof");
// $row_rsspp_prof = mysql_fetch_assoc($rsspp_prof);
// $totalRows_rsspp_prof = mysql_num_rows($rsspp_prof);

?><!DOCTYPE html>
<html class="no-js"><!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --> <!--<![endif]-->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-meta.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-css.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-javascript.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-links.php"); ?>

    <!-- InstanceBeginEditable name="head" -->
<script>
$MXW_relPath = '../';
</script>
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/JSRecordset.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/DynamicInput.js"></script>
<?php
//begin JSRecordset
$jsObject_rsProfession = new WDG_JsRecordset("rsProfession");
echo $jsObject_rsProfession->getOutput();
//end JSRecordset
?>
<?php
//begin JSRecordset
$jsObject_rsTitles = new WDG_JsRecordset("rsTitles");
echo $jsObject_rsTitles->getOutput();
//end JSRecordset
?>
<script type="text/javascript" src="../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../includes/resources/calendar.js"></script>
<!-- InstanceEndEditable -->
</head>

<body class="home blue-bar color-override-yellow-orange">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/main-nav.php"); ?>
			<div class="width">
			    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/global-logo-tagline.php"); ?>
			</div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
	<!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- InstanceBeginEditable name="pagetitle" -->
        <section class="span-12 margin-bottom-medium">
        <div class="inner">
                 <h1>Profile</h1>
                <ol class="breadcrumbs" style="margin-top:-18px;">
                  <li><a class="B_homeCrumb" href="/home/archon-home.php" title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                  <li><span class="B_currentCrumb">Administration</span></li>
                </ol>
                   <div>
           </section>
           <!-- InstanceEndEditable -->
           
           <section class="span-12 margin-bottom-medium">
         <div class="inner">
         <!-- InstanceBeginEditable name="content" -->
         <?php echo $tNGs->displayValidationRules();?>
        <h1 class="yellow">ADD PRIOR EMPLOYMENT</h1>
        <div class="contentBlock">

      <?php
    	echo $tNGs->getErrorMsg();
    ?>
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
        <td class="KT_th"><label for="OCCUPATIONID">PROFESSION:</label></td>
        <td><select name="OCCUPATIONID" id="OCCUPATIONID" wdg:subtype="DynamicInput" wdg:type="widget" wdg:recordset="rsProfession" wdg:displayfield="DESCRIPTION" wdg:valuefield="OCCUPATIONID" wdg:norec="50" wdg:singleclickselect="no" wdg:defaultoptiontext="no" wdg:restrict="No" wdg:ext="php" wdg:selected="<?php echo $row_rsspp_prof['OCCUPATIONID'] ?>" wdg:fakeattribute="<?php WDG_registerRecordInsert("sigma_modx", "rsProfession", "OCCUPATIONID", "DESCRIPTION");?>">
          </select>
            <?php echo $tNGs->displayFieldError("spp_prof", "OCCUPATIONID"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="TITLEID">TITLE:</label></td>
        <td><select name="TITLEID" id="TITLEID" wdg:subtype="DynamicInput" wdg:type="widget" wdg:recordset="rsTitles" wdg:displayfield="DESCRIPTION" wdg:valuefield="SKILLID" wdg:norec="50" wdg:singleclickselect="no" wdg:defaultoptiontext="no" wdg:restrict="No" wdg:ext="php" wdg:selected="<?php echo $row_rsspp_prof['TITLEID'] ?>" wdg:fakeattribute="<?php WDG_registerRecordInsert("sigma_modx", "rsTitles", "SKILLID", "DESCRIPTION");?>">
                          </select>
            <?php echo $tNGs->displayFieldError("spp_prof", "TITLEID"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="COMPANYNAME">COMPANY NAME:</label></td>
        <td><input type="text" name="COMPANYNAME" id="COMPANYNAME" value="<?php echo KT_escapeAttribute($row_rsspp_prof['COMPANYNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("COMPANYNAME");?> <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYNAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="COMPANYCITY">COMPANY CITY:</label></td>
        <td><input type="text" name="COMPANYCITY" id="COMPANYCITY" value="<?php echo KT_escapeAttribute($row_rsspp_prof['COMPANYCITY']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("COMPANYCITY");?> <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYCITY"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="COMPANYSTCD">COMPANY STATE:</label></td>
        <td><select name="COMPANYSTCD" id="COMPANYSTCD">
            <?php 
            do {  
            ?>
            <option value="<?php echo $row_rsStates['st_cd']?>"<?php if (!(strcmp($row_rsStates['st_cd'], $row_rsspp_prof['COMPANYSTCD']))) {echo "SELECTED";} ?>><?php echo $row_rsStates['st_nm']?></option>
            <?php
              } while ($row_rsStates = mysql_fetch_assoc($rsStates));
                $rows = mysql_num_rows($rsStates);
                if($rows > 0) {
                    mysql_data_seek($rsStates, 0);
              	  $row_rsStates = mysql_fetch_assoc($rsStates);
                }
              ?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYSTCD"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="DESCRIPTION">DESCRIPTION:</label></td>
        <td><textarea name="DESCRIPTION" id="DESCRIPTION" cols="50" rows="5"><?php echo KT_escapeAttribute($row_rsspp_prof['DESCRIPTION']); ?></textarea>
            <?php echo $tNGs->displayFieldHint("DESCRIPTION");?> <?php echo $tNGs->displayFieldError("spp_prof", "DESCRIPTION"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="COMPANYJOBSTART">COMPANY DATE START:</label></td>
        <td><input name="COMPANYJOBSTART" id="COMPANYJOBSTART" value="<?php echo KT_formatDate($row_rsspp_prof['COMPANYJOBSTART']); ?>" size="32" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
            <?php echo $tNGs->displayFieldHint("COMPANYJOBSTART");?> <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYJOBSTART"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="COMPANYJOBEND">COMPANY DATE END:</label></td>
        <td><input name="COMPANYJOBEND" id="COMPANYJOBEND" value="<?php echo KT_formatDate($row_rsspp_prof['COMPANYJOBEND']); ?>" size="32" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
            <?php echo $tNGs->displayFieldHint("COMPANYJOBEND");?> <?php echo $tNGs->displayFieldError("spp_prof", "COMPANYJOBEND"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Insert1" id="KT_Insert1" value="ADD PRIOR EMPLOYMENT" /> <input type="button" name="Cancel" value="Cancel" onclick="history.go(-1)" /></td>
      </tr>
    </table>
    <input type="hidden" name="CUSTOMERID" id="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsspp_prof['CUSTOMERID']); ?>" />
    <input type="hidden" name="CREATEDATE" id="CREATEDATE" value="<?php echo KT_formatDate($row_rsspp_prof['CREATEDATE']); ?>" />
    <input type="hidden" name="CREATEDBY" id="CREATEDBY" value="<?php echo KT_escapeAttribute($row_rsspp_prof['CREATEDBY']); ?>" />
  </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
      <div>
       </section>
           <!-- end showing the content, pagetitle, breadcrumb and sidebar -->
           <!-- 3 round images toggle start -->
           <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/row-spotlight-grid-12.php"); ?>
           <!-- 3 round images toggle end -->
     </div>
    </div>

            <!-- main content body end -->	
	
	<!-- third section event and articles start -->
  	 <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
	<!-- third section event and articles end -->
	
    <footer class="width padding" style="border-top: 10px #52B609 solid;">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-content-default.php"); ?>
      </div>
    </footer>
	
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-scripts.php"); ?>
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsProfession);

mysql_free_result($rsTitles);

mysql_free_result($rsStates);
?>
