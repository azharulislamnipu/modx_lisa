<?php
// English language file
// If you are going to make your own language file and are usign the lighbox, 
// make a language file for it too, f.ex. /maxigallery/slidebox/slidebox_lang_en.js
$strings['previous']="&laquo; previous";
$strings['next']="next &raquo;";
$strings['click_to_zoom']=" &raquo; Click to zoom ->";
$strings['click_to_go_back']="<- Click to go back";
$strings['click_to_open_original']= "Click to open big image";
$strings['back_to_normal_view'] = "Back to normal view";
$strings['index']="index";
$strings['go_to_gallery']="Go to gallery ->";
$strings['pictures_successfully_uploaded']="<b>Pictures uploaded successfully.</b>";
$strings['changes_have_been_saved']="<b>Changes have been saved.</b>";
$strings['delete']="Delete";
$strings['title']="Title";
$strings['save_changes']="Save changes";
$strings['upload_pictures']="Upload pictures";
$strings['check_to_delete_this_picture']="Check to delete this picture";
$strings['manage_pictures']="Manage pictures";
$strings['date']="Date";
$strings['description']="Description";
$strings['position']="Position";
$strings['gallery_description']="Gallery description";
$strings['db_no_descr_support']="<em>Warning: Gallery database does not support descriptions!</em><br />";
$strings['db_no_pos_support']="<em>Warning: Gallery database does not support picture positions!</em><br />";
$strings['picture'] = "Picture:";
$strings['max_pics_reached'] = "You have reached maximum pictures limit for this gallery, so you can't send more pictures.";
$strings['supported_types'] = "<b>Incorrect filetype!</b> This gallery supports the following filetypes: jpg/jpeg, png, gif and zip (which has aforementioned filetypes inside)";
$strings['gif_not_supported'] = "<b>The GD-library on this server does not support .gif images</b> You must use only jpg and png images.";
$strings['supported_types_inzip'] = "Error: zip archive contained incorrect filetypes which were ignored.";
$strings['max_pics_reached_some_discarded'] = "<b>Maximum pictures limit reached!</b> Some of the pictures sent might got discarded.";
$strings['zip_not_supported'] = "<b>Your PHP-installation does not support .zip archives!</b> You have to install the <a href=\"http://www.php.net/manual/ref.zip.php\">PHP zip module and ZZIPlib library.</a>";
$strings['database_error'] = "Error: Gallery database could not be created!";
$strings['database_error_ownid'] = "Error: Could not create own_id field to database table!";
$strings['invalid_class'] = "Error: Cannot find maxigallery class file. Please check it! ";
$strings['invalid_name_class'] = "Error: Maxigallery class name is invalid.";
?>