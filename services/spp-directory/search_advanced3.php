<?php
require_once('../Connections/sigma_modx.php');

$errorMsg = null;
$hasErrors = true;
//$errorMsg = 'You must specify at least one field!';

$currentpage = 0;
$sort = 'LASTNAME';
$sortdir = 'ASC';
if(isset($_GET['currentpage'])) {
    $currentpage = intval($_GET['currentpage']);
}
if(isset($_GET['sort'])) {
    $sort = $_GET['sort'];
}
if(isset($_GET['sortdir'])) {
    $sortdir = $_GET['sortdir'];
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsIndustry = "SELECT * FROM spp_occupation ORDER BY `DESCRIPTION` ASC";
$rsIndustry = mysql_query($query_rsIndustry, $sigma_modx) or die(mysql_error());
$totalRows_rsIndustry = mysql_num_rows($rsIndustry);

// built to allow key -> values relations for industries in the "You searched for" text
/*$industries = array();
if ($totalRows_rsIndustry)
{
    while ($row_rsIndustry = mysql_fetch_assoc($rsIndustry))
    {
      $industry = mysql_fetch_assoc($rsIndustry);
      $industries[$industry['OCCUPATIONID']] = $industry['DESCRIPTION'];
    }
    mysql_data_seek($rsIndustry, 0);
}*/

//$row_rsIndustry = mysql_fetch_assoc($rsIndustry);


$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = mysql_query($query_rsStates, $sigma_modx) or die(mysql_error());
$row_rsStates = mysql_fetch_assoc($rsStates);
$totalRows_rsStates = mysql_num_rows($rsStates);

$query_rsBoule = "SELECT * FROM spp_boule ORDER BY ORGCD ASC";
$rsBoule = mysql_query($query_rsBoule, $sigma_modx) or die(mysql_error());
$row_rsBoule = mysql_fetch_assoc($rsBoule);
$totalRows_rsBoule = mysql_num_rows($rsBoule);

$query_rsRegions = "SELECT DISTINCT REGIONCD, REGIONNAME FROM spp_boule ORDER BY REGIONNAME ASC";
$rsRegions = mysql_query($query_rsRegions, $sigma_modx) or die(mysql_error());
$row_rsRegions = mysql_fetch_assoc($rsRegions);
$totalRows_rsRegions = mysql_num_rows($rsRegions);

$query_rsOfficers = "SELECT * FROM spp_cposition WHERE CPOSITION NOT LIKE '%Grand%' AND CPOSITION NOT LIKE '%past regional sire archons council%' ORDER BY CPOSITIONID ASC";
$rsOfficers = mysql_query($query_rsOfficers, $sigma_modx) or die(mysql_error());
$totalRows_rsOfficers = mysql_num_rows($rsOfficers);


//SEARCH SQL

function keyPars($sql, $values, $fields) {
    //$fields = Array('FIRSTNAME','LASTNAME');
    if(strlen($sql) > 0) {
        $sql .= ' AND ';
    }
    $vals = null;
    if(is_array($values)) {
        $vals = $values;
    }
    else {
        $vals = Array();
        $vals[0] = $values;
    }
    $insql = '';
    $insql2 = '';
    foreach($vals as $val) {
        $insql2 = '';
        foreach($fields as $fld) {
            if(strlen($insql2) > 0) {
                $insql2 .= ' OR ';
            }
            $insql2 .= $fld.' LIKE "%'.$val.'%"';
        }
        if(strlen($insql) > 0) {
            $insql .= ' AND ';
        }
        $insql .= '('.$insql2.')';
    }
    $sql .= '('.$insql.')';
    return $sql;
}

function testParm($in, $fld) {
    if(isset($_GET[$in])) {
        $val = $_GET[$in];
        if(is_array($val)) {
            return true;
        }
        else {
            $val = $val.'';
            if(strlen($val) > 0) {
                return true;
            }
        }
    }
    return false;
}

$searchpars_out;

function addSearchPar($sql, $in, $field, $like) {
    if(!testParm($in, $field)) {
        return $sql;
    }
    global $searchpars_out;
    $searchpars_out .= $in.' = ';
    $values = $_GET[$in];
    if(strlen($sql) > 0) {
        $sql .= ' AND ';
    }
    $vals = null;
    if(is_array($values)) {
        $vals = $values;
    }
    else {
        $vals = Array();
        $vals[0] = $values;
    }
    $insql = '';
    foreach($vals as $val) {
        $searchpars_out .= $val.' ';
        if(strlen($insql) > 0) {
            $insql .= ' OR ';
        }
        if($like) {
            $insql .= $field.' LIKE "%'.$val.'%"';
        }
        else {
            $insql .= $field.' = "'.$val.'"';
        }
    }
    $sql .= '('.$insql.')';
    if($field == 'OFFICERID') {
        $sql .= ' AND COMMITTEESTATUSSTT = "Active"';
    }
    $searchpars_out .= '<br/>';
    return $sql;
}

$searchsql = '';

if(isset($_GET['q'])) {
    $keys = $_GET['q'].'';
    if(strlen($keys) > 0) {
        $searchpars_out .= 'Keywords = '.$keys;
        $vals = explode(' ', $keys);
        $fieldssql = 'DESC vw_search2';
        $res = mysql_query($fieldssql, $sigma_modx) or die(mysql_error());
        $fields = Array();
        while($row = mysql_fetch_assoc($res)) {
            array_push($fields, $row['Field']);
        }
        $searchsql = keyPars($searchsql, $vals, $fields);
        $hasErrors = false;
    }
}

$searchsql = addSearchPar($searchsql,'firstname', 'FIRSTNAME',true);
$searchsql = addSearchPar($searchsql,'lastname', 'LASTNAME', true);
$searchsql = addSearchPar($searchsql,'boule', 'CHAPTERID', false);
$searchsql = addSearchPar($searchsql,'city', 'CITY', true);
$searchsql = addSearchPar($searchsql,'state', 'STATECD', true);
$searchsql = addSearchPar($searchsql,'region', 'REGIONCD', false);
$searchsql = addSearchPar($searchsql,'office', 'CPOSITION', false);
$searchsql = addSearchPar($searchsql,'title', 'JOBTITLE', true);
$searchsql = addSearchPar($searchsql,'company', 'COMPANYNAME', true);
$searchsql = addSearchPar($searchsql,'school', 'INSTITUTION', true);
$searchsql = addSearchPar($searchsql,'industry', 'OCCUPATIONCD', false);
$searchsql = addSearchPar($searchsql,'company', 'COMPANYNAME', true);

if(strlen($searchsql) > 0) {
    $hasErrors = false;
}
if(!$hasErrors) {
    $searchressql = 'SELECT WEB_ID, FIRSTNAME, LASTNAME, BOULENAME, CITY, STATECD, EMAIL, HOMEPHONE, CPOSITION, JOB_TITLE, IMAGE FROM vw_search2 WHERE '.$searchsql.' GROUP BY WEB_ID ORDER BY '.$sort.' '.$sortdir.' LIMIT '.($currentpage * 25).', 25';
    $rsArchons = mysql_query($searchressql, $sigma_modx) or die(mysql_error());
    $searchpagessql = 'SELECT COUNT(*) as cnt FROM vw_search2 WHERE '.$searchsql.' GROUP BY WEB_ID';
    //echo $searchressql;
    $rsPageArchons = mysql_query($searchpagessql, $sigma_modx) or die(mysql_error());
    //$row = mysql_fetch_assoc($rsPageArchons);
    $totalRows_rsSearch = mysql_num_rows($rsPageArchons);
}
else {
    $errorMsg = 'You must specify at least one field!';
}

function urlWithParam($par, $value) {
    $gets = '';
    $found = false;
    foreach($_GET as $c=>$v) {  
        //echo "LOOOOK" . $c. " = > ".$v."<br>";
        if(strlen($gets) > 0) {
            $gets .='&';
        }
        if($c == $par) {
            $gets .= $c.'='.$value;
            $found = true;
        }
        else {
            if(is_array($v)) {
                $nv = "";
                foreach($v as $vv) {
                    if(strlen($nv) > 0) {
                        $nv.="&";
                    }
                    $nv .= $c.'[]='.$vv;
                }
                $gets.=$nv;
            }
            else {
                $gets .= $c .'='.$v;
            }                
        }
    }    
    if(!$found) {
        if(strlen($gets) > 0) {
            $gets .='&';
        }
        $gets .= $par.'='.$value;
    }
    //echo 'http://'.getenv("HTTP_HOST").'/services/directory/search_advanced3.php?'.$gets;
    echo '?'.$gets;
}  

function changePage($p, $current, $total) {
    $fp = $current + $p;
    urlWithParam('currentpage', $fp);
}

function changeOrder($value, $curr, $dir) {
    if($curr == $value) {
        if($dir == 'ASC') {
            urlWithParam('sortdir', 'DESC');
        }
        else {
            urlWithParam('sortdir', 'ASC');
        }
    }
    else {
        urlWithParam('sort', $value);
    }
}

function makeImageLink($img) {
    if(isset($img)) {
        echo '<a href="http://'.getenv("HTTP_HOST").'/home/assets/images/archons/'.$img.'">link for image</a>';
    }
}

?>
<!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --><!DOCTYPE html>
<!--
 ____________________________________________________________
|                                                            |
|      DATE + 2006.11.14                                     |
| COPYRIGHT + Sigma Pi Phi 2006.  All Rights Reserved.       |
|____________________________________________________________|

-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity |</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-meta.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-css.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-javascript.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-links.php"); ?>

    <!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body class="home blue-bar [!colorPalette? &palette_default=`[*palette_default*]` &palette_loggedin=`[*palette_loggedin*]` !]">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/main-nav.php"); ?>
			<div class="width">
			    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/global-logo-tagline.php"); ?>
			</div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
	<!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- InstanceBeginEditable name="pagetitle" -->
                            <section class="span-12 margin-bottom-medium">
                            <div class="inner">
                                     <h1>Profile</h1>
                                    <ol class="breadcrumbs" style="margin-top:-18px;">
                                      <li><a class="B_homeCrumb" href="/home/archon-home.php" title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                                      <li><span class="B_currentCrumb">Administration</span></li>
                                    </ol>
                     <div>
             </section>
             <!-- InstanceEndEditable -->
             
             <!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">ADVANCED SEARCH</h1>
<div class="contentBlock">
  <table width="100%" cellpadding="2">
    <tr valign="top">
      <td width="50%">
      <form name="MX_Search_Form" method="GET" action="">
      <table width="100%" cellpadding="2">
          <tr>
            <td><div align="right">Keywords:              </div></td>
            <td><input name="q" type="text" id="q" size="45" value="" /></td>
          </tr>
          <tr>
            <td><div align="right">State:</div></td>
            <td><select name="state" id="state"><option value="">Anywhere</option>
                <?php
do {  
    //$extra = ((isset($_GET['state']) && $row_rsStates['st_cd'] == $_GET['state'])?'selected="selected"':'');
?>
                <option value="<?php echo $row_rsStates['st_cd']?>"><?php echo $row_rsStates['st_nm']?></option>
                <?php
} while ($row_rsStates = mysql_fetch_assoc($rsStates));
  $rows = mysql_num_rows($rsStates);
  if($rows > 0) {
      mysql_data_seek($rsStates, 0);
      $row_rsStates = mysql_fetch_assoc($rsStates);
  }
?>
            </select></td>
          </tr>
          <tr>
            <td><div align="right">City:</div></td>
            <td><input name="city" type="text" id="city" size="45" value="" /></td>
          </tr>
        </table></td>
      <td><table width="100%" cellpadding="2">
        <tr>
          <td><div align="right">First Name:</div></td>
          <td><input name="firstname" type="text" id="firstname" size="45" value="" /></td>
        </tr>
        <tr>
          <td><div align="right">Last Name:</div></td>
          <td><input name="lastname" type="text" id="lastname" size="45" value="" /></td>
        </tr>
        <tr>
          <td><div align="right">Boul&eacute;:</div></td>
          <td><select name="boule" id="boule">
            <option value="">Anywhere</option>
            <?php
do {
    //$extra = ((isset($_GET['boule']) && $row_rsBoule['CHAPTERID'] == $_GET['boule'])?'selected="selected"':'');  
?>
            <option value="<?php echo $row_rsBoule['CHAPTERID']?>"><?php echo $row_rsBoule['BOULENAME']?></option>
            <?php
} while ($row_rsBoule = mysql_fetch_assoc($rsBoule));
  $rows = mysql_num_rows($rsBoule);
  if($rows > 0) {
      mysql_data_seek($rsBoule, 0);
      $row_rsBoule = mysql_fetch_assoc($rsBoule);
  }
?>
          </select></td>
        </tr>
        <tr>
          <td><div align="right">Region:</div></td>
          <td>
            <select name="region" id="region">
                <option value="">Anywhere</option>
                <?php
                do {
                    //$extra = ((isset($_GET['region']) && $row_rsRegions['REGIONCD'] == $_GET['region'])?'selected="selected"':'');
                ?>
                            <option value="<?php echo $row_rsRegions['REGIONCD']?>"><?php echo $row_rsRegions['REGIONNAME']?></option>
                            <?php
                } while ($row_rsRegions = mysql_fetch_assoc($rsRegions));
                  $rows = mysql_num_rows($rsRegions);
                  if($rows > 0) {
                      mysql_data_seek($rsRegions, 0);
                      $row_rsRegions = mysql_fetch_assoc($rsRegions);
                  }
                ?>
            </select>
          </td>
        </tr>
        <tr>
          <td><div align="right">Office:</div></td>
          <td>
            <select name="office[]" size="5" multiple="multiple" id="office">
            <?php
while ($row_rsOfficers = mysql_fetch_assoc($rsOfficers)) {  
    //$extra = ((isset($_GET['office']) && in_array($row_rsOfficers['OFFICERID'], $_GET['office']))?'selected="selected"':'');
?>
            <option value="<?php echo $row_rsOfficers['CPOSITION']?>"><?php echo $row_rsOfficers['CPOSITION']?></option>
            <?php
} 
?>
          </select>
          </td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="2"><hr noshade /></td>
      </tr>
    <tr valign="top">
      <td width="50%"><table width="100%" cellpadding="2">
        <tr>
          <td><div align="right">Title:</div></td>
          <td><input name="title" type="text" id="title" size="45" value="" /></td>
        </tr>
        <tr>
          <td><div align="right">Company:</div></td>
          <td><input name="company" type="text" id="company" size="45" value="" /></td>
        </tr>
        <tr>
          <td><div align="right">School:</div></td>
          <td><input name="school" type="text" id="school" size="45" value="" /></td>
        </tr>
      </table></td>
      <td><table width="100%" cellpadding="2">
        <tr>
          <td valign="top"><div align="right">Occupation:</div></td>
          <td><select name="industry[]" size="5" multiple="multiple" id="industry">
            <?php
while ($row_rsIndustry = mysql_fetch_assoc($rsIndustry)) {  
   // $extra = ((isset($_GET['industry']) && in_array($row_rsIndustry['OCCUPATIONID'], $_GET['industry']))?'selected="selected"':'');
    //$extra = ''
?>
            <option value="<?php echo $row_rsIndustry['DESCRIPTION']?>"><?php echo $row_rsIndustry['DESCRIPTION']?></option>
            <?php
} 
?>
          </select></td>
        </tr>
        <tr>
          <td valign="top"><div align="right"></div></td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    </tr>

<!-- ERROR MESSAGES -->
    
      <?php 
// Show IF Conditional region1 
if (@$_GET['q'] == NULL) {
?>
<tr>
        <td>&nbsp;</td>
</tr>
  <?php } 
// endif Conditional region1

if (isset($_GET['q']))
{
    //echo 'results '. $totalRows_rsSearch;
    if ($hasErrors && !is_null($errorMsg)) echo '<tr><td colspan="2"><p style="font-size:2em; font-weight:bold; color:red;" align="center">'.$errorMsg.'</p></td>';
    else if (($totalRows_rsSearch == 0)){ // Show if recordset empty
?>
<tr>
          <td colspan="2"><p style="font-size:2em; font-weight:bold; color:red;" align="center">No records found matching your criteria!</p></td>
</tr>
<?php }} // Show if recordset empty ?>
    
<!-- END ERROR MESSAGES -->
    
    <tr>
      <td colspan="2"><div align="center">
        <input type="submit" name="search" id="search" value="Search" />
</div></td>
      </tr>
  </table>
</div>
  
  <?php 
    if($totalRows_rsSearch > 0) { 
    ?>
  <div id="search_therms">  
        <strong>Searching For:</strong>
        <p>
    <?php
        echo $searchpars_out;
    ?>
    </p>
    <p>
        Showing 
            <?php 
                $showingto = $currentpage * 25 + 25;
                if($showingto > $totalRows_rsSearch) {
                    $showingto = $totalRows_rsSearch;
                }
                echo ($currentpage * 25 + 1).'-'.$showingto.' of '.$totalRows_rsSearch;
            ?> 
    </p>
  </div>
  <h1 class="yellow">Search Results</h1>
<div class="contentBlock">
<table cellspacing="4" cellpadding="4">
  <tr>
    <td bgcolor="#65A9DC"><span class="style1"><a href="<?php changeOrder('FIRSTNAME', $sort, $sortdir) ?>">FIRST NAME</a></span></td>
    <td bgcolor="#65A9DC"><span class="style1"><a href="<?php changeOrder('LASTNAME', $sort, $sortdir) ?>">LAST NAME</a></span></td>
    <td bgcolor="#65A9DC"><span class="style1"><a href="<?php changeOrder('BOULENAME', $sort, $sortdir) ?>">BOUL&Eacute;</a></span></td>
    <td bgcolor="#65A9DC"><span class="style1"><a href="<?php changeOrder('CITY', $sort, $sortdir) ?>">CITY</a></span></td>
    <td bgcolor="#65A9DC"><span class="style1"><a href="<?php changeOrder('STATECD', $sort, $sortdir) ?>">STATE</a></span></td>
    <td bgcolor="#65A9DC"><span class="style1">EMAIL</span></td>
    <td bgcolor="#65A9DC"><span class="style1">PHONE</span></td>
    <!--td bgcolor="#65A9DC"><span class="style1"><a href="<?php //changeOrder('OFFICERID', $sort, $sortdir) ?>">OFFICE</a></span></td-->
    <td bgcolor="#65A9DC"><span class="style1"><a href="<?php changeOrder('CPOSITION', $sort, $sortdir) ?>">OFFICE</a></span></td>
    <td bgcolor="#65A9DC"><span class="style1">JOB TITLE</span></td>
    <td bgcolor="#65A9DC"><span class="style1">PROFILE</span></td>
  </tr>
  <?php while ($row_rsArchons = mysql_fetch_assoc($rsArchons)) { ?>
    <tr valign="top">
      <td bgcolor="#FFFFFF" class="style1"><?php echo $row_rsArchons['FIRSTNAME']; ?></span></td>
      <td bgcolor="#FFFFFF" class="style1"><?php echo $row_rsArchons['LASTNAME']; ?></span></td>
      <td bgcolor="#FFFFFF" class="style1"><?php echo $row_rsArchons['BOULENAME']; ?></td>
      <td bgcolor="#FFFFFF" class="style1"><?php echo $row_rsArchons['CITY']; ?></td>
      <td bgcolor="#FFFFFF" class="style1"><?php echo $row_rsArchons['STATECD']; ?></td>
      <td bgcolor="#FFFFFF" class="style1"><a href="mailto:<?php echo $row_rsArchons['EMAIL']; ?>"><?php echo $row_rsArchons['EMAIL']; ?></a></td>
      <td bgcolor="#FFFFFF" class="style1"><?php echo $row_rsArchons['HOMEPHONE']; ?></td>
      <td bgcolor="#FFFFFF" class="style1"><?php echo $row_rsArchons['CPOSITION']; ?></td>
      <td bgcolor="#FFFFFF" class="style1"><?php echo $row_rsArchons['JOB_TITLE']; ?></td>
      <td bgcolor="#FFFFFF" class="style1"><a href="results.php?id=<?php echo $row_rsArchons['WEB_ID'] ?>">View</a></td>
    </tr>
    <?php } ?>
</table>
<p>&nbsp;

<?php if($currentpage > 0) { ?>
        <a href="<?php urlWithParam('currentpage', 0) ?>">First</a> <a href="<?php changePage(-1, $currentpage, $totalRows_rsSearch) ?>">Previous</a> 
    <?php } 
    else { ?>
        First Previous
    <?php }
    if($currentpage < ceil($totalRows_rsSearch / 25) - 1) { ?>
        <a href="<?php changePage(1, $currentpage, $totalRows_rsSearch)?>">Next</a> <a href="<?php urlWithParam('currentpage', floor(($totalRows_rsSearch-1)/25))?>">Last</a>
    <?php }
    else {?>
        Next Last
    <?php } ?>

</p>
</div>

<?php
 }
?>
  
  
  
<!-- InstanceEndEditable -->
            <!-- main content body end -->	
	
	<!-- third section event and articles start -->
  	 <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
	<!-- third section event and articles end -->
	
    <footer class="width padding" style="border-top: 10px #52B609 solid;">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-content-default.php"); ?>
      </div>
    </footer>
	
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-scripts.php"); ?>
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsIndustry);

mysql_free_result($rsStates);

mysql_free_result($rsBoule);

mysql_free_result($rsRegions);
?>