<?php
////////////////////////////////////////////////////////////
//Common header for all the themes
////////////////////////////////////////////////////////////
require_once('functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANGUAGE;?>" lang="<?php echo LANGUAGE;?>">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET;?>" />
		<title><?php echo $html_title;?></title>
		<meta name="title" content="<?php echo $html_title;?>" />
		<meta name="description" content="<?php echo $html_description;?>" />
		<meta name="keywords" content="<?php echo $html_keywords;?>" />		
		<meta name="generator" content="Open Classifieds <?php echo VERSION;?>" />
		<link rel="shortcut icon" href="<?php echo SITE_URL;?>/favicon.ico" />
	<?php if (isset($type)){?>
		<link rel="alternate" type="application/rss+xml" title="<?php echo T_RSS_ADS;?> <?php echo ucwords(getTypeName($type));?> <?php echo ucwords($currentCategory);?>" href="<?php echo SITE_URL;?>/rss/?type=<?php echo $type;?>&amp;category=<?php echo $currentCategory;?>" />
	<?php }?>
	<?php if (isset($currentCategory)){?>
		<link rel="alternate" type="application/rss+xml" title="<?php echo T_RSS_ADS;?> <?php echo ucwords($currentCategory);?>" href="<?php echo SITE_URL;?>/rss/?category=<?php echo $currentCategory;?>" />
	<?php }?>
		<link rel="alternate" type="application/rss+xml" title="<?php echo T_RSS_ADS;?>" href="<?php echo SITE_URL;?>/rss/" />
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>/themes/<?php echo THEME;?>/style.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>/themes/wordcloud.css" media="screen" />
	<?php if (isset($idItem)||( is_numeric(cG("post")) ) ) {//only in the item the css and in the edit?>
		<script type="text/javascript">var GB_ROOT_DIR = "<?php echo SITE_URL;?>/includes/greybox/";</script>
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL;?>/includes/greybox/gb_styles.css" media="screen" />
	<?php }?>
		<script type="text/javascript" src="<?php echo SITE_URL;?>/includes/js/common.js"></script>
</head>
<body>
<?php require_once(SITE_ROOT.'/themes/'.THEME.'/header.php');?>
<!--googleoff: index-->
<noscript>
	<div style="height:30px;border:3px solid #6699ff;text-align:center;font-weight: bold;padding-top:10px">
		Your browser does not support JavaScript!
	</div>
</noscript>
<!--googleon: index-->
