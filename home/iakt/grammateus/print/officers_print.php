<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// begin Recordset
$colname__rsOfficers = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsOfficers = $_SESSION['BOULENAME'];
}
$query_rsOfficers = sprintf("SELECT OFFICERID, CPOSITIONID, CPOSITION, L2R_FULLNAME, ADDRESS1, ADDRESS2, CITY, STATECD, ZIP, HOMEPHONE, EMAIL, WORKPHONE, ALTEMAIL, WEB_EMAIL FROM vw_boule_officers WHERE COMMITTEESTATUSSTT = 'Active' AND BOULENAME = 
%s ORDER BY CPOSITIONID ASC", GetSQLValueString($colname__rsOfficers, "text"));
$rsOfficers = $sigma_modx->SelectLimit($query_rsOfficers) or die($sigma_modx->ErrorMsg());
$totalRows_rsOfficers = $rsOfficers->RecordCount();
// end Recordset
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<style type="text/css">
<!--
.style4 {
	font-size: 14px;
	font-weight: bold;
}
-->
</style><!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->
<div class="KT_tng">
<h1><?php echo date("Y"); ?> Officer Information</h1>
<div style="padding: 2px 12px; font-size: 16px;">
<p>Print this document and return it with your 2009 Membership Reporting documents.</p>
<p>[*print_doc*]</p>
</div>
<table cellspacing="0" cellpadding="4" class="KT_tngtable">
  <tr>
    <td></td>
    <td></td>
  </tr>
  <?php
  while (!$rsOfficers->EOF) { 
?>
    <tr>
      <td valign="top"><h3><?php echo $rsOfficers->Fields('CPOSITION'); ?></h3></td>
      <td valign="top"><p><span class="style4"><?php echo $rsOfficers->Fields('L2R_FULLNAME'); ?></span><br>
            <?php echo $rsOfficers->Fields('ADDRESS1'); ?><br>
            <?php echo $rsOfficers->Fields('CITY'); ?>&nbsp;<?php echo $rsOfficers->Fields('STATECD'); ?>&nbsp;<?php echo $rsOfficers->Fields('ZIP'); ?></p>
        <p>Home: <?php echo $rsOfficers->Fields('HOMEPHONE'); ?><br />
          Office: <?php echo $rsOfficers->Fields('WORKPHONE'); ?><br />
          Email: <?php echo $rsOfficers->Fields('WEB_EMAIL'); ?></p></td>
    </tr>
    <?php
    $rsOfficers->MoveNext(); 
  }
?>
</table>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsOfficers->Close();
?>
