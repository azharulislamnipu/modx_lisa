<?php get_header(); ?>

	<div id="content">
		<div class="padder one-column">
			<?php do_action( 'sigma_before_404' ); ?>
			<div id="post-0" class="post page-404 error404 not-found" role="main">
				<h2 class="purple color"><?php _e( "Page not found", 'archon-network' ); ?></h2>
				<div class="contentBlock">
					<p><?php _e( "We're sorry, but we can't find the page that you're looking for. Perhaps searching will help.", 'archon-network' ); ?></p>
					<?php get_search_form(); ?>
				</div>

				<?php do_action( 'sigma_404' ); ?>
			</div>

			<?php do_action( 'sigma_after_404' ); ?>
		</div><!-- .padder -->
	</div><!-- #content -->

<?php get_footer(); ?>