<?php
// English language file
// If you are going to make your own language file and are usign the lighbox, 
// make a language file for it too, f.ex. /maxigallery/slidebox/slidebox_lang_en.js
$strings['previous']="&laquo; anterior";
$strings['next']="siguiente &raquo;";
$strings['click_to_zoom']=" &raquo; Clic para magnificar ->";
$strings['click_to_go_back']="<- Clic para regresar";
$strings['click_to_open_original']= "Clic para ver la versi&oacute;n grande";
$strings['back_to_normal_view'] = "Volver a vista normal";
$strings['index']="Indice";
$strings['go_to_gallery']="Ir a la galer&iacute;a ->";
$strings['pictures_successfully_uploaded']="<b>Im&aacute;genes subidas con &eacute;xito.</b>";
$strings['changes_have_been_saved']="<b>Los cambios han sido guardados.</b>";
$strings['delete']="Eliminar";
$strings['title']="T&iacute;tulo";
$strings['save_changes']="Guardar cambios";
$strings['upload_pictures']="Subir im&aacute;genes";
$strings['check_to_delete_this_picture']="Marcar para eliminar esta imagen";
$strings['manage_pictures']="Administrar im&aacute;genes";
$strings['date']="Fecha";
$strings['description']="Descripci&oacute;n";
$strings['position']="Posici&oacute;n";
$strings['gallery_description']="Descripci&oacute;n de la galer&iacute;a";
$strings['db_no_descr_support']="<em>�Advertencia: La base de datos empleada por esta galer&iacute;a no acepta descripciones!</em><br />";
$strings['db_no_pos_support']="<em>�Advertencia: La base de datos empleada por esta galer&iacute;a no acepta posiciones de im&aacute;genes!</em><br />";
$strings['picture'] = "Im&aacute;gen:";
$strings['max_pics_reached'] = "Ha alcanzado el l&iacute;mite m&aacute;ximo de im&aacute;genes para esta galer&iacute;a, no puede enviar m&aacute;s im&aacute;genes.";
$strings['supported_types'] = "<b>�Tipo de fichero incorrecto!</b> Esta galer&iacute;a soporta los siguientes formatos: jpg/jpeg, png, gif y zip (conteniendo los formatos citados anteriormente)";
$strings['gif_not_supported'] = "<b>La librer&iacute;a GD en este servidor no soporta im&aacute;genes .gif</b> Debe utilizar solamente im&aacute;genes jpg y png.";
$strings['supported_types_inzip'] = "Error: el archivo zip conten&iacute;a tipos de ficheros incorrectos que fueron ignorados.";
$strings['max_pics_reached_some_discarded'] = "<b>�L&iacute;mite m&aacute;ximo de im&aacute;genes alcanzado!</b> Algunas de las im&aacute;genes pueden haber sido descartadas.";
$strings['zip_not_supported'] = "<b>�Su instalaci�n PHP no soporta archivos .zip!</b> Debe instalar el <a href=\"http://www.php.net/manual/ref.zip.php\">m&oacute;dulo zip PHP y la librer&iacute;a ZZIPlib.</a>";
$strings['database_error'] = "Error: �No se pudo crear la base de datos para la galer&iacute;a!";
$strings['database_error_ownid'] = "Error: �No se pudo crear el campo own_id en la tabla de la base de datos!";
$strings['invalid_class'] = "Error: Cannot find maxigallery class file. Please check it! ";
$strings['invalid_name_class'] = "Error: Maxigallery class name is invalid.";
?>
