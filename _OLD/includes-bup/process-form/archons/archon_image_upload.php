<?php

$config = array();
//$main_dir = str_replace('includes','', $include_path);
//$config["destination_folder"]		=  $main_dir. 'home/assets/images/archons/';
$config["destination_folder"]		= $main_path . 'home/assets/images/archons/';
$config["upload_url"] 				= "/home/assets/images/archons/";
$config["file_name_prefix"]         = empty($user_id) ? '' : $user_id . '.';
$fname = empty($_POST['UPDATEDBY']) ? '' : $_POST['UPDATEDBY'];

$fname = explode(' ', $fname);

$config['user_name'] = strtolower($fname[0]);

//include sanwebe impage resize class
include $pform_dir . "image-upload/crop.php";

//create class instance
$crop = new CropAvatar(
    $config,
    isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
    isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
    isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
);
$response = array(
    'type'  => 'success',
    'err_msg' => $crop -> getMsg(),
    'img_url' => $crop -> getResult()
);
if(empty($response['err_msg'])){
    try{
        // Save image to db
        $db = get_db();
        $data = array (
            'IMAGE'         => $crop->getFileName(),
            'LASTUPDATED'   => date("Y-m-d H:i:s"),
            'UPDATEDBY'   => $updated_by,
        );
        $db->where ('CUSTOMERID', $user_id);
        if ($db->update ('spp_archons', $data)){
            $response['type'] = 'success';
        } else{
            $response['type'] = 'error';
            $response['err_msg'] = $db->getLastError();
        }
    } catch(Exception $e){
        $response['type'] = 'error';
        $response['err_msg'] = $response['err_msg'] . '<p>' . $e->getMessage() . '</p>';
    }
} else {
    $response['type'] = 'error';
}

die(json_encode($response));
