<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("DEGREE", true, "text", "", "", "", "");
$formValidation->addField("INSTITUTION", true, "text", "", "", "", "");
$formValidation->addField("DEGREEYEAR", true, "numeric", "", "", "", "");
$formValidation->addField("MAJOR", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an insert transaction instance
$ins_spp_educ = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_educ);
// Register triggers
$return = !empty( $_POST['RETURN'] ) ? $_POST['RETURN'] : '/home/personal-profile.php';
$ins_spp_educ->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_educ->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_educ->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$ins_spp_educ->setTable("spp_educ");
$ins_spp_educ->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID", "{POST.CUSTOMERID}");
$ins_spp_educ->addColumn("DEGREE", "STRING_TYPE", "POST", "DEGREE");
$ins_spp_educ->addColumn("INSTITUTION", "STRING_TYPE", "POST", "INSTITUTION");
$ins_spp_educ->addColumn("DEGREEYEAR", "NUMERIC_TYPE", "POST", "DEGREEYEAR");
$ins_spp_educ->addColumn("MAJOR", "STRING_TYPE", "POST", "MAJOR");
$ins_spp_educ->addColumn("MINOR1", "STRING_TYPE", "POST", "MINOR1");
$ins_spp_educ->addColumn("MINOR2", "STRING_TYPE", "POST", "MINOR2");
$ins_spp_educ->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$ins_spp_educ->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY", "{SESSION.webShortname}");
$ins_spp_educ->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "LASTUPDATED", "{NOW_DT}");
$ins_spp_educ->setPrimaryKey("DEGREEID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_educ = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_educ);
// Register triggers
$upd_spp_educ->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_educ->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_educ->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$upd_spp_educ->setTable("spp_educ");
$upd_spp_educ->addColumn("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");
$upd_spp_educ->addColumn("DEGREE", "STRING_TYPE", "POST", "DEGREE");
$upd_spp_educ->addColumn("INSTITUTION", "STRING_TYPE", "POST", "INSTITUTION");
$upd_spp_educ->addColumn("DEGREEYEAR", "NUMERIC_TYPE", "POST", "DEGREEYEAR");
$upd_spp_educ->addColumn("MAJOR", "STRING_TYPE", "POST", "MAJOR");
$upd_spp_educ->addColumn("MINOR1", "STRING_TYPE", "POST", "MINOR1");
$upd_spp_educ->addColumn("MINOR2", "STRING_TYPE", "POST", "MINOR2");
$upd_spp_educ->addColumn("COMMENT", "STRING_TYPE", "POST", "COMMENT");
$upd_spp_educ->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "{SESSION.webShortname}");
$upd_spp_educ->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "{NOW_DT}");
$upd_spp_educ->setPrimaryKey("DEGREEID", "NUMERIC_TYPE", "POST", "DEGREEID");

// Make an instance of the transaction object
$del_spp_educ = new tNG_multipleDelete($conn_sigma_modx);
$tNGs->addTransaction($del_spp_educ);
// Register triggers
$del_spp_educ->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_educ->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$del_spp_educ->setTable("spp_educ");
$del_spp_educ->setPrimaryKey("DEGREEID", "NUMERIC_TYPE", "POST", "DEGREEID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_educ = $tNGs->getRecordset("spp_educ");
$row_rsspp_educ = mysql_fetch_assoc($rsspp_educ);
$totalRows_rsspp_educ = mysql_num_rows($rsspp_educ);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<h1 class="yellow">ADD EDUCATION</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">

    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      	<input type="hidden" name="RETURN" id="RETURN" value="<?php echo KT_escapeAttribute($_POST['RETURN']); ?>" />
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php
// Show IF Conditional region1
if (@$totalRows_rsspp_educ > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php }
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <tr>
              <td class="KT_th"><label for="DEGREE_<?php echo $cnt1; ?>">DEGREE:</label></td>
              <td><input type="text" name="DEGREE_<?php echo $cnt1; ?>" id="DEGREE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_educ['DEGREE']); ?>" size="25" maxlength="25" />
                  <?php echo $tNGs->displayFieldHint("DEGREE");?> <?php echo $tNGs->displayFieldError("spp_educ", "DEGREE", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="INSTITUTION_<?php echo $cnt1; ?>">INSTITUTION:</label></td>
              <td><input type="text" name="INSTITUTION_<?php echo $cnt1; ?>" id="INSTITUTION_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_educ['INSTITUTION']); ?>" size="32" maxlength="150" />
                  <?php echo $tNGs->displayFieldHint("INSTITUTION");?> <?php echo $tNGs->displayFieldError("spp_educ", "INSTITUTION", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="DEGREEYEAR_<?php echo $cnt1; ?>">DEGREE YEAR:</label></td>
              <td><input type="text" name="DEGREEYEAR_<?php echo $cnt1; ?>" id="DEGREEYEAR_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_educ['DEGREEYEAR']); ?>" size="7" />
                  <?php echo $tNGs->displayFieldHint("DEGREEYEAR");?> <?php echo $tNGs->displayFieldError("spp_educ", "DEGREEYEAR", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="MAJOR_<?php echo $cnt1; ?>">MAJOR:</label></td>
              <td><input type="text" name="MAJOR_<?php echo $cnt1; ?>" id="MAJOR_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_educ['MAJOR']); ?>" size="32" maxlength="125" />
                  <?php echo $tNGs->displayFieldHint("MAJOR");?> <?php echo $tNGs->displayFieldError("spp_educ", "MAJOR", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="MINOR1_<?php echo $cnt1; ?>">MINOR 1:</label></td>
              <td><input type="text" name="MINOR1_<?php echo $cnt1; ?>" id="MINOR1_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_educ['MINOR1']); ?>" size="32" maxlength="125" />
                  <?php echo $tNGs->displayFieldHint("MINOR1");?> <?php echo $tNGs->displayFieldError("spp_educ", "MINOR1", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="MINOR2_<?php echo $cnt1; ?>">MINOR 2:</label></td>
              <td><input type="text" name="MINOR2_<?php echo $cnt1; ?>" id="MINOR2_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_educ['MINOR2']); ?>" size="32" maxlength="125" />
                  <?php echo $tNGs->displayFieldHint("MINOR2");?> <?php echo $tNGs->displayFieldError("spp_educ", "MINOR2", $cnt1); ?> </td>
            </tr>
            <tr>
              <td class="KT_th"><label for="COMMENT_<?php echo $cnt1; ?>">COMMENT:</label></td>
              <td><textarea name="COMMENT_<?php echo $cnt1; ?>" id="COMMENT_<?php echo $cnt1; ?>" cols="50" rows="5"><?php echo KT_escapeAttribute($row_rsspp_educ['COMMENT']); ?></textarea>
                  <?php echo $tNGs->displayFieldHint("COMMENT");?> <?php echo $tNGs->displayFieldError("spp_educ", "COMMENT", $cnt1); ?> </td>
            </tr>
          </table>
          <input type="hidden" name="kt_pk_spp_educ_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_educ['kt_pk_spp_educ']); ?>" />
          <input type="hidden" name="CUSTOMERID_<?php echo $cnt1; ?>" id="CUSTOMERID_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_educ['CUSTOMERID']); ?>" />
          <input type="hidden" name="UPDATEDBY_<?php echo $cnt1; ?>" id="UPDATEDBY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_educ['UPDATEDBY']); ?>" />
          <input type="hidden" name="LASTUPDATED_<?php echo $cnt1; ?>" id="LASTUPDATED_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_educ['LASTUPDATED']); ?>" />
          <?php } while ($row_rsspp_educ = mysql_fetch_assoc($rsspp_educ)); ?>
        <div class="KT_bottombuttons">
          <div>
            <?php
      // Show IF Conditional region1
      if (@$_POST['DEGREEID'] == "") {
      ?>
              <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Add"); ?>" />
              <?php
      // else Conditional region1
      } else { ?>
              <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <?php }
      // endif Conditional region1
      ?>
            <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '<?php echo $return ?>')" />
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
