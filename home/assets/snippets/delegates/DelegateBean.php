<?php

/**
 * Holds a delegate's information
 * @author Pat Heard {http://fullahead.org}
 */
 
class DelegateBean implements Serializable {

	const TYPE_DELEGATE = 'D';
	const TYPE_ALTERNATE = 'A';

        private $id = -1;               // delegate_id on spp_delegates
	private $name = '';             // Delegate name
	private $memberNumber = '';     // Member number
	private $type = '';             // Delegate or alternate
        
        private $appointed = false;     // Has this delegate been appointed?
        private $certified = false;     // Has this delegate been certified?
        private $certifiedComment = ''; // Is there a certified comment?
        private $financiallyEligible = false;   // Is this delegate financially eligible (from spp_archons)
        private $ineligible = false;    // Has this delegate been marked as ineligible by cron database job? (spp_delegates.APPOINTED = '2')
	
        private $isModified = false;    // Has the delegate data been modified?
        
	public function __construct(array $args = null){

		// Set default arguments
		$args = array_merge(array(
                        'id'                    => -1,
			'name'                  => '',
			'memberNumber'          => '',
			'type'                  => '',
                        'appointed'             => false,
                        'certified'             => false,
                        'certifiedComment'      => '',
                        'financiallyEligible'   => false,                 
                        'ineligible'            => false
		), $args);
	
                $this->id = $args['id'];
		$this->name = ucwords($args['name']);
		$this->memberNumber = $args['memberNumber'];
		$this->type = $args['type'];
                $this->appointed = $args['appointed'];
                $this->certified = $args['certified'];
                $this->certifiedComment = $args['certifiedComment'];
                $this->financiallyEligible = $args['financiallyEligible'];
                $this->ineligible = $args['ineligible'];
	}
        
        public function serialize(){  
            return serialize(array(  
                'id'                    => $this->id, 
                'name'                  => $this->name,  
                'memberNumber'          => $this->memberNumber,  
                'type'                  => $this->type,  
                'appointed'             => $this->appointed,  
                'certified'             => $this->certified,
                'certifiedComment'      => $this->certifiedComment,
                'financiallyEligible'   => $this->financiallyEligible,
                'ineligible'            => $this->ineligible  
            )); 
        }  

        public function unserialize($serializedData){  
            $data = unserialize($serializedData);
            
            $this->id = isset($data['id']) ? $data['id'] : -1;
            $this->name = isset($data['name']) ? $data['name'] : '';	
            $this->memberNumber = isset($data['memberNumber']) ? $data['memberNumber'] : '';
            $this->type = isset($data['type']) ? $data['type'] : '';
            $this->appointed = isset($data['appointed']) ? $data['appointed'] : false; 
            $this->certified = isset($data['certified']) ? $data['certified'] : false; 
            $this->certifiedComment = isset($data['certifiedComment']) ? $data['certifiedComment'] : ''; 
            $this->financiallyEligible = isset($data['financiallyEligible']) ? $data['financiallyEligible'] : false; 
            $this->ineligible = isset($data['ineligible']) ? $data['ineligible'] : false;
        }         

        public function validate(){
            $messages = array();
            if(empty($this->name)){
                $messages[] = array('error', 'Delegate name cannot be empty.');
            }
            if(empty($this->memberNumber)){
                $messages[] = array('error', 'Delegate member number cannot be empty.');
            }
            if($this->type !== self::TYPE_DELEGATE && $this->type !== self::TYPE_ALTERNATE){
                $messages[] = array('error', 'Delegate type is not valid.  Must be "D" or "A".');
            }  
            return $messages;
        }
    
	public function setModified($isModified){
		$this->isModified = $isModified;
	}
    
	public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
	}
	
	public function __toString(){
            return
                ' id :' . $this->id .
                ' name :' . $this->name .
                ' memberNumber :' . $this->memberNumber .
                ' type :' . $this->type . 
                ' appointed :' . $this->appointed .
                ' certified :' . $this->certified .
                ' certifiedComment :' . $this->certifiedComment .
                ' financiallyEligible :' . $this->financiallyEligible .
                ' ineligible :' . $this->ineligible;
	}
}

?>