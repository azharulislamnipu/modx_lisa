<?php
add_filter( 'kc_post_settings', 'sigma_post_options' );
function sigma_post_options( $groups ) {
	$my_group = array(
		'post'	=> array(		// post_type
			'sample_section' => array(
				'id'				=> 'post_options',		// section ID for each metabox
				'title'			=> 'Post Options',		// section title
				'desc'			=> '',	// section description (optional, default null)
				'priority'	=> 'high',							// section priority, low|high (optional, default high)
				'role'			=> array('administrator', 'editor'),			// user role, only user in this role will get this metabox. use an array for more than one role (optional, default none)
				'fields'		=> array(								// here are the options for this metabox
					'title_bar_color' => array(
						'id'			=> 'title_bar_color',
						'title'		=> 'Title color',
						'desc'		=> 'The color of the page\'s title bar',
						'type'		=> 'select',
						'options'	=> array(
							'red'		=> 'Red',
							'dark_red'	=> 'Dark Red',
							'yellow'	=> 'Yellow',
							'blue'		=> 'Blue',
							'green'		=> 'Green',
							'purple'	=> 'Purple',
							'black'		=> 'Black',
							'brown'		=> 'Brown',
							'gray'		=> 'Gray'
						)
					)
				)
			)
		),
		'page'	=> array(		// post_type
			'sample_section' => array(
				'id'				=> 'page_options',		// section ID for each metabox
				'title'			=> 'Page Options',		// section title
				'desc'			=> '',	// section description (optional, default null)
				'priority'	=> 'high',							// section priority, low|high (optional, default high)
				'role'			=> array('administrator', 'editor'),			// user role, only user in this role will get this metabox. use an array for more than one role (optional, default none)
				'fields'		=> array(								// here are the options for this metabox
					'title_bar_color' => array(
						'id'			=> 'title_bar_color',
						'title'		=> 'Title color',
						'desc'		=> 'The color of the page\'s title bar',
						'type'		=> 'select',
						'options'	=> array(
							'red'		=> 'Red',
							'dark_red'	=> 'Dark Red',
							'yellow'	=> 'Yellow',
							'blue'		=> 'Blue',
							'green'		=> 'Green',
							'purple'	=> 'Purple',
							'black'		=> 'Black',
							'brown'		=> 'Brown',
							'gray'		=> 'Gray'
						)
					)
				)
			)
		)
	);

	$groups[] = $my_group;
	return $groups;
}

?>