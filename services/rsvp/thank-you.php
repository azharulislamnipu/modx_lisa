<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchon = "-1";
if (isset($_GET['WEB_ID'])) {
  $colname_rsArchon = $_GET['WEB_ID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArchon = sprintf("SELECT CUSTOMERID, REGIONNAME, REGIONCD, BOULENAME, CHAPTERID, BOULECITY, BOULESTATECD, STATUS, STATUSSTT, CUSTOMERCLASSSTT, CUSTOMERTYPE, JOINDATE, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, SUFFIX, DESIGNATIONLST, L2R_FULLNAME, FULLNAME, FULLNAME2, L2R_FULLNAME2, ADDRESS1, ADDRESS2, CITY, STATECD, ZIP, HOMEPHONE, EMAIL, JOBTITLE, OCCUPATIONCD, SKILLCDLST, ORGNAME, ALTADDRESS1, ALTADDRESS2, ALTCITY, ALTSTATE, ALTZIP, WORKPHONE, ALTEMAIL, BIRTHDATE, SPOUSENAME, LASTUPDATED, UPDATEDBY, GRAMMUPDATE, GRAMM, IMAGE, DECEASEDDATE, WEB_EMAIL, CPOSITION, WEB_ID FROM vw_web_elig WHERE WEB_ID = %s", GetSQLValueString($colname_rsArchon, "int"));
$rsArchon = mysql_query($query_rsArchon, $sigma_modx) or die(mysql_error());
$row_rsArchon = mysql_fetch_assoc($rsArchon);
$totalRows_rsArchon = mysql_num_rows($rsArchon);

$colname_rsRSVP = "-1";
if (isset($_GET['rsvp_id'])) {
  $colname_rsRSVP = $_GET['rsvp_id'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsRSVP = sprintf("SELECT rsvp_id, guest_name, `number` FROM spp_rsvp WHERE rsvp_id = %s", GetSQLValueString($colname_rsRSVP, "int"));
$rsRSVP = mysql_query($query_rsRSVP, $sigma_modx) or die(mysql_error());
$row_rsRSVP = mysql_fetch_assoc($rsRSVP);
$totalRows_rsRSVP = mysql_num_rows($rsRSVP);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">INVITATION REQUEST CONFIRMATION</h1>
<div class="contentBlock">
<h2>Thank You!</h2>
<p style="width:599px;">
Archon <?php echo $row_rsArchon['LASTNAME']; ?>,</p>
<p style="width:475px;"> Your  request for <strong><?php echo $row_rsRSVP['number']; ?></strong> invitation(s) to attend the Inaugural Reception has been submitted. You will receive an email at <strong><?php echo $row_rsArchon['EMAIL']; ?></strong> which will confirm your request. </p>
<p style="width:475px;">The Office of the Grand Boul&#233; will inform you  of the status of your request shortly.</p>
<p>&nbsp;</p>

</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchon);

mysql_free_result($rsRSVP);
?>
