<?php
#
# Snippet PPP v 0.3 beta
# By Uncle68 
#
# This release: 2-Jan-2007.
#
# Demo site: http://www.learningmodx.com
# Usage and examples - see documentation attached to this release
# Requires: eForm 1.4.1+
#
# Warning! This add-on might not be ready for public use
#

// \\ FUNCTIONALITY TODO // \\

//TODO: Let the user style his/her own page by choosing from a list of predefined (by you) css and/or pictures.
//TODO: Make is possible to delete custom field /custom table
//TODO: Langugae file 
//TODO: User statistics: Online today/week/month, new today/week/month, latest member, online now, total registered users and more
//TODO: Ability to work with more than one custom table

// \\ CODING TODO // \\

//TODO: Security and error handling
//TODO: Sanitize fields and table names before trying to add to database
//TODO: Set defaults values to better values and add error check.
//TODO: Adjust coding to instructions at http://wiki.modxcms.com/index.php/Creating_Snippets
//TODO: Remove any styling from admin modules
//TODO: Before adding a new table, check to see that a table with that name doesn't already exist, otherwise it will generate an error.
//TODO: Check new created custom fields not beeing named with same name as any filed in standard tables - would create problems when updating fileds.
//TODO: Make usage of the settings table possible
//TODO: The different parts of admin modules should be placed in placeholders to enable custom designed admin pages.


GLOBAL $ppp_userId, $ppp_editOtherUser, $ppp_useTable, $ppp_storeTo;
GLOBAL $arrayPhCustom, $arrayPhSettings, $arrayPhAttributes, $ppp_allowCustomColumns;

$ppp_userId = "";
$ppp_showId = "";
$ppp_editId = "";
$ppp_editOtherUser = "";
$ppp_showOwnOnly = "";
$ppp_useTable = "";
$ppp_adminListPH = "";
$ppp_adminAddFields = "";
$ppp_allowCustomColumns = "";

/* Get current web user id or, if set, web  user id from the snippet call */
$ppp_userId = isset($webuserid) ? $webuserid : $modx->getLoginUserID();

	// NEW in 0.3
	
		/* Set id from GET[]  */
		$ppp_showId = isset($_GET[WID]) ? $_GET[WID] : '';

		//Id to edit. Use on public admin page with editOtherUser set to one in ppp-call
		$ppp_editId = isset($_GET[WEDIT])  ? $_GET[WEDIT] : '';

		//If to allow to edit other users (should only be available for trusted front end admins)
		$ppp_editOtherUser = isset($editOtherUser) ? $editOtherUser : 0 ;

		//Prevent personal page from being accessed by using WID of another user
		$ppp_showOwnOnly = isset($showOwnOnly) ? $showOwnOnly : 0;
		
	// END NEW
		
/* Name of custom table */
$ppp_useTable = isset($useTable) ? $useTable : 0;

/* If to show the list of avalible placeholders */
$ppp_adminListPH = isset($adminListPH)? $adminListPH : 0;

/*If to show the add field form for the custom table*/
$ppp_adminAddFields = isset($addFields) ? $addFields : 0;

/*Set to '1' to enable a list of users. Creats a placeholder named "ppp_ListUsers" which must be present on page to show list*/
$ppp_listUsers = isset($listUsers) ? $listUsers : 0;

/* Limits number of users to be listed  Defaults to 10 */
$ppp_listUsersLimit = isset($listUsersLimit) ? $listUsersLimit : 10;

/* Sets the page number to where the user profile is shown (must be a page with a ppp call) */
// TODO - What to set as default??? If not set, an errorr message should be displayed.
$ppp_listUsersDirectTo = isset($listUsersDirectTo) ? $listUsersDirectTo : 0; 

//Not used in this version
$ppp_listUsersOrderBy = isset($listUsersOrderBy) ? $listUsersOrderBy : 0;

//Which groups should be listed? Not used in this version
$ppp_listUsersGroups = isset($listUsersGroups) ? $listUsersGroups : 0;


	//NEW IN 0.2//
	
	// A comma delimited list of fields that must be filled in for the user to show up in the"simple mode" user links list
	$ppp_listUsersWithFields = isset($listUsersWithFields) ? $listUsersWithFields : false;
	
	// Set to path where you want files uploaded by user to be stored. Defaults to assets/images/ (set enough rights in server)
	$ppp_fileStoreDir = isset($fileStoreDir) ? $fileStoreDir : 'assets/images/';
	
	// Set to 1 to automatically create seperate directory for each user uploading a file (recommended). - Note: Set rights to 777 (?)
	$ppp_fileUseUserDir = isset($fileUseUserDir) ? $fileUseUserDir : false ;
	
	// NOT YET VERIFIED - Use at own risk
	/*Set different types of columns*/ ### Thanks to Chuck (ProWebscape) ###
	$ppp_allowCustomColumns = isset($allowCustomColumns) ? $allowCustomColumns : 0;
	
	//TEST - Not in use in this version - a listMaker less heavy on recourses
	$ppp_listMaker = isset($listMaker) ? $listMaker : false;
	
	// Advanced list creator
	$ppp_listUsersAdv = isset($listUsersAdv) ? $listUsersAdv : false;

// \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ 	

require_once('ppp.resources.inc.php');

	//Determine what web user Id should be used
	if($ppp_editOtherUser==1) {
		$ppp_userId = !EMPTY($ppp_editId) ? $ppp_editId : $ppp_userId;
	} else if($ppp_showOwnOnly != 1) {
		$ppp_userId = !EMPTY($ppp_showId) ? $ppp_showId : $ppp_userId;
	}

	//If this is from an edit for a different user, get webuser id value from hidden field to be used for file store path
	if($_POST[wuid]) $ppp_userId = $_POST[wuid];
	
	//Sets dir for storing files and ctreates a pleceholder
	if ($ppp_fileUseUserDir) {$ppp_useDir = 'webuser/' .$ppp_userId. '/';} else {$ppp_useDir = '';}
	$ppp_storeTo = $ppp_fileStoreDir . $ppp_useDir ;
	$modx->setPlaceholder('ppp_fileDir',$ppp_storeTo);


/* Now, let's do something  */

	// If not logged in - do nothing. (User will still be able to see other profiles.)
	if ($ppp_userId > 0) {
		if (!$ppp_useTable==0) ppp_createTbl(); //Create table if needed
		if (!$ppp_useTable==0) ppp_makePhCustomTbl(); //If custom table specified - create placeholders
		ppp_makePhStandardTbl();  //Create ph for standard tables
		if ($ppp_adminAddFields==1) ppp_addCustomField(); //Set a new field (column) in selected custom table
		if ($ppp_adminListPH==1) ppp_listPlaceholders($arrayPhSettings, $arrayPhCustom, $arrayPhAttributes); //List ph on admin page
	}  
	
	// Simple user list - Get a  list of users which can be clicked and show user info. 
	if ($ppp_listUsers==1) ppp_showListOfUsers($ppp_listUsersLimit, $ppp_listUsersDirectTo, $ppp_listUsersWithFields); 

	// Advances user listing - allows multiple list per page and lots of different configurations
	if (!EMPTY($ppp_listUsersAdv)) ppp_listUsersAdvanced($ppp_listUsersAdv);
 
?>