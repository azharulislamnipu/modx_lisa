<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <?php
      /* Leave this block of php code intact if customizing this page */
      error_reporting(0);
      $preferences = file_get_contents('preferences.txt');
      $preferences = unserialize($preferences);
      $title = htmlspecialchars($preferences['htmlTitle'], ENT_QUOTES, 'UTF-8');
      $useFlickr = $preferences['useFlickr'] ? 'true' : 'false';
      $showTakenByText = $preferences['flickrShowTakenByText'] ? 'true' : 'false';
      $useReloadButton = $preferences['useReloadButton'] ? 'true' : 'false';
      $showFlipButton = $preferences['showFlipButton'] ? 'true' : 'false';
      $showLinkButton = $preferences['showLinkButton'] ? 'true' : 'false';
      $bkgndTransparent = $preferences['bkgndTransparent'] ? 'true' : 'false';
      $showFullscreenOption = $preferences['showFullscreenOption'] ? 'true' : 'false';
      $enableSounds = $preferences['enableSounds'] ? 'true' : 'false';
      print '<title>'.$title.'</title>';
    ?>
    <script type="text/javascript" src="swfobject.js"></script>
    <style type="text/css">	
    	/* If you are adding your own headers and footers then set these heights in pixels */
    	html {
    		height: 100%;
    		overflow: hidden;
    	}	
    	#flashcontent {
    		height: 100%;
    	}
    	body {
    		height: 100%;
    		margin: 0;
    		padding: 0;
    		background-color: #000000;
    		color:#ffffff;
    		font-family:sans-serif;
    		font-size:40;
    	}
    	a {	
    		color:#cccccc;
    	}
    </style>
  </head>
  <body>
  	<div id="flashcontent">
  	  TiltViewer requires JavaScript and the latest Flash player. <a href="http://get.adobe.com/flashplayer/">Get Flash here.</a>
  	</div>
  	<script type="text/javascript">
  	
  		var fo = new SWFObject("TiltViewer.swf", "viewer", "100%", "100%", "9.0.28", "#000000");			
  		
  		// TILTVIEWER CONFIGURATION OPTIONS
  		// For a description of config options, go to: 
  		// http://www.airtightinteractive.com/projects/tiltviewer/config_options.html		
  		
  		// XML GALLERY OPTIONS	
  		fo.addVariable("useFlickr", "<?php print $useFlickr; ?>");
  		fo.addVariable("xmlURL", "gallery.xml?nocache=<?php print rand(); ?>");
  		
  		// FlICKR GALLERY OPTIONS
  		fo.addVariable("user_id", "<?php print $preferences['flickrUserId']; ?>");
  		fo.addVariable("tags", "<?php print $preferences['flickrTags']; ?>");
  		fo.addVariable("tag_mode", "<?php print $preferences['flickrTagMode']; ?>");
  		fo.addVariable("showTakenByText", "<?php print $showTakenByText; ?>");
  		
  		
  		// GENERAL OPTIONS
  		fo.addVariable("maxJPGSize","<?php print $preferences['maxJPGSize']; ?>");
  		fo.addVariable("useReloadButton", "<?php print $useReloadButton; ?>");
  		fo.addVariable("columns", "<?php print $preferences['columns']; ?>");
  		fo.addVariable("rows", "<?php print $preferences['rows']; ?>");
  		fo.addVariable("showFlipButton", "<?php print $showFlipButton; ?>");
  		fo.addVariable("showLinkButton", "<?php print $showLinkButton; ?>");		
  		fo.addVariable("linkLabel", "<?php print $preferences['linkLabel']; ?>");
  		fo.addVariable("frameColor", "<?php print $preferences['frameColor']; ?>");
  		fo.addVariable("backColor", "<?php print $preferences['backColor']; ?>");
  		fo.addVariable("bkgndInnerColor", "<?php print $preferences['bkgndInnerColor']; ?>");
  		fo.addVariable("bkgndOuterColor", "<?php print $preferences['bkgndOuterColor']; ?>");				
  		fo.addVariable("langGoFull", "<?php print $preferences['langGoFull']; ?>");
  		fo.addVariable("langExitFull", "<?php print $preferences['langExitFull']; ?>");
  		fo.addVariable("langAbout", "<?php print $preferences['langAbout']; ?>");
  		
  		// PRO OPTIONS
  		fo.addVariable("bkgndTransparent", "<?php print $bkgndTransparent; ?>");
  		fo.addVariable("showFullscreenOption", "<?php print $showFullscreenOption; ?>");
  		fo.addVariable("textColor", "<?php print $preferences['textColor']; ?>");
  		fo.addVariable("linkTextColor", "<?php print $preferences['linkTextColor']; ?>");
  		fo.addVariable("linkBkgndColor", "<?php print $preferences['linkBkgndColor']; ?>");
  		fo.addVariable("frameColor", "<?php print $preferences['frameColor']; ?>");
  		fo.addVariable("navButtonColor", "<?php print $preferences['navButtonColor']; ?>");
  		fo.addVariable("flipButtonColor", "<?php print $preferences['flipButtonColor']; ?>");
  		fo.addVariable("frameWidth", "<?php print $preferences['frameWidth']; ?>");
  		fo.addVariable("zoomedInDistance", "<?php print $preferences['zoomedInDistance']; ?>");
  		fo.addVariable("zoomedOutDistance", "<?php print $preferences['zoomedOutDistance']; ?>");
  		fo.addVariable("fontName", "<?php print $preferences['fontName']; ?>");
  		fo.addVariable("titleFontSize", "<?php print $preferences['titleFontSize']; ?>");
  		fo.addVariable("descriptionFontSize", "<?php print $preferences['descriptionFontSize']; ?>");
  		fo.addVariable("linkFontSize", "<?php print $preferences['linkFontSize']; ?>");
  		fo.addVariable("linkTarget", "<?php print $preferences['linkTarget']; ?>");
  		fo.addVariable("enableSounds", "<?php print $enableSounds; ?>");
  		fo.addVariable("tiltAmountIn", "<?php print $preferences['tiltAmountIn']; ?>");
  		fo.addVariable("tiltAmountOut", "<?php print $preferences['tiltAmountOut']; ?>");
  		
  		// END TILTVIEWER CONFIGURATION OPTIONS
  		
  		fo.addParam("allowFullScreen","true");
  		fo.write("flashcontent");			
  	</script>	
  </body>
</html>