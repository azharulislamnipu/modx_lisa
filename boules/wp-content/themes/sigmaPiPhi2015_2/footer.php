     <section id="events">
         <div class="width padding">

             <div class="row grid-12">
				
 				<?php if ( is_active_sidebar( 'event_block' ) ) : ?>
				
 					<?php dynamic_sidebar( 'event_block' ); ?>
				 
 					<?php endif; ?>
 				</div><!-- row grid12-->

    <!-- <div class="row grid-12"> -->
               
			
              

		
 		    <?php //if( !is_multisite_primary() && !is_regional_site() ) :

 				//the_widget('RegionalBoules_Widget', array('show_regional' => 'no'), array('before_title' => '<h3 class="widgettitle">', 'after_title' => '</h3>'));

 		    	//$chapter_name = get_bloginfo('name'); ?>
 		    	<!-- <div class="widget">
 			        <h2 class="color yellow" style="clear: both"><?php //echo $chapter_name . ' ' . __('Officers', 'archon-network');?></h2>
 			        <div class="contentBlock">
 			            <?php //get_boule_leadership( $chapter_name ) ?>
 			        </div>
 		        </div> -->
 		    <?php //endif; ?>
		
             <!-- </div> -->

         </div>
     </section>
	
		

		<footer class="width padding">
			   <div class="row grid-12">
				   <div class="inner">
					   
			<?php if ( is_active_sidebar( 'footer_block' ) ) : ?>
			
				<?php dynamic_sidebar( 'footer_block' ); ?>
			 
				<?php endif; ?>

			<?php //do_action( 'sigma_footer' ) ?>

	</div><!-- #inner -->
	<!-- </div> --><!-- #width padding -->
		</footer><!-- #footer -->
		

		<?php do_action( 'sigma_after_footer' ) ?>

		<?php wp_footer(); ?>
		<!--[if gte IE 9 | !IE ]><!--><script src="<?php bloginfo('template_directory') ?>/assets/js/vendor/jquery-2.1.3.min.js"></script><!--<![endif]--> 
		<!--[if lt IE 9]><script src="assets/js/vendor/jquery-1.11.2.min.js"></script><![endif]-->
		<script src="<?php bloginfo('template_directory') ?>/assets/js/main.js"></script>

	</body>

</html>