<?php
//Connection statement
require_once('../Connections/sigma_modx.php');

//Aditional Functions
require_once('../includes/functions.inc.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Filter
$tfi_listrsArchonList1 = new TFI_TableFilter($sigma_modx, "tfi_listrsArchonList1");
$tfi_listrsArchonList1->addColumn("FULLNAME", "STRING_TYPE", "FULLNAME", "%");
$tfi_listrsArchonList1->addColumn("STATUS", "STRING_TYPE", "STATUS", "%");
$tfi_listrsArchonList1->addColumn("JOINDATE", "DATE_TYPE", "JOINDATE", "=");
$tfi_listrsArchonList1->addColumn("EMAIL", "STRING_TYPE", "EMAIL", "%");
$tfi_listrsArchonList1->addColumn("LASTUPDATED", "DATE_TYPE", "LASTUPDATED", "=");
$tfi_listrsArchonList1->Execute();

// Sorter
$tso_listrsArchonList1 = new TSO_TableSorter("rsArchonList", "tso_listrsArchonList1");
$tso_listrsArchonList1->addColumn("FULLNAME");
$tso_listrsArchonList1->addColumn("STATUS");
$tso_listrsArchonList1->addColumn("JOINDATE");
$tso_listrsArchonList1->addColumn("EMAIL");
$tso_listrsArchonList1->addColumn("LASTUPDATED");
$tso_listrsArchonList1->setDefault("FULLNAME");
$tso_listrsArchonList1->Execute();

// Navigation
$nav_listrsArchonList1 = new NAV_Regular("nav_listrsArchonList1", "rsArchonList", "../", $_SERVER['PHP_SELF'], 10);

// Begin List Recordset
$maxRows_rsArchonList = $_SESSION['max_rows_nav_listrsArchonList1'];
$pageNum_rsArchonList = 0;
if (isset($_GET['pageNum_rsArchonList'])) {
  $pageNum_rsArchonList = $_GET['pageNum_rsArchonList'];
}
$startRow_rsArchonList = $pageNum_rsArchonList * $maxRows_rsArchonList;
$colname__rsArchonList = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsArchonList = $_SESSION['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter__rsArchonList = "1=1";
if (isset($_SESSION['filter_tfi_listrsArchonList1'])) {
  $NXTFilter__rsArchonList = $_SESSION['filter_tfi_listrsArchonList1'];
}
// Defining List Recordset variable
$NXTSort__rsArchonList = "FULLNAME";
if (isset($_SESSION['sorter_tso_listrsArchonList1'])) {
  $NXTSort__rsArchonList = $_SESSION['sorter_tso_listrsArchonList1'];
}
$query_rsArchonList = sprintf("SELECT * FROM vw_gramm_archons WHERE BOULENAME = %s AND  {$NXTFilter__rsArchonList}  ORDER BY  {$NXTSort__rsArchonList} ASC ", GetSQLValueString($colname__rsArchonList, "text"));
$rsArchonList = $sigma_modx->SelectLimit($query_rsArchonList, $maxRows_rsArchonList, $startRow_rsArchonList) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsArchonList'])) {
  $totalRows_rsArchonList = $_GET['totalRows_rsArchonList'];
} else {
  $all_rsArchonList = $sigma_modx->SelectLimit($query_rsArchonList) or die($sigma_modx->ErrorMsg());
  $totalRows_rsArchonList = $all_rsArchonList->RecordCount();
}
$totalPages_rsArchonList = (int)(($totalRows_rsArchonList-1)/$maxRows_rsArchonList);
// End List Recordset

$nav_listrsArchonList1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: false,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_FULLNAME {width:175px; overflow:hidden;}
  .KT_col_STATUS {width:70px; overflow:hidden;}
  .KT_col_JOINDATE {width:70px; overflow:hidden;}
  .KT_col_EMAIL {width:105px; overflow:hidden;}
  .KT_col_LASTUPDATED {width:105px; overflow:hidden;}
</style><!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->

<div class="KT_tng" id="listrsArchonList1">
  <h1>[*pagetitle*]
    <?php
  $nav_listrsArchonList1->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listrsArchonList1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
        <?php 
  // Show IF Conditional region1 
  if (@$_GET['show_all_nav_listrsArchonList1'] == 1) {
?>
          <?php echo $_SESSION['default_max_rows_nav_listrsArchonList1']; ?>
          <?php 
  // else Conditional region1
  } else { ?>
          <?php echo NXT_getResource("all"); ?>
          <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                            <?php 
  // Show IF Conditional region2 
  if (@$_SESSION['has_filter_tfi_listrsArchonList1'] == 1) {
?>
                              <a href="<?php echo $tfi_listrsArchonList1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                              <?php 
  // else Conditional region2
  } else { ?>
                              <a href="<?php echo $tfi_listrsArchonList1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                              <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>
            </th>
            <th id="FULLNAME" class="KT_sorter KT_col_FULLNAME <?php echo $tso_listrsArchonList1->getSortIcon('FULLNAME'); ?>"> <a href="<?php echo $tso_listrsArchonList1->getSortLink('FULLNAME'); ?>">NAME</a> </th>
            <th id="STATUS" class="KT_sorter KT_col_STATUS <?php echo $tso_listrsArchonList1->getSortIcon('STATUS'); ?>"> <a href="<?php echo $tso_listrsArchonList1->getSortLink('STATUS'); ?>">STATUS</a> </th>
            <th id="JOINDATE" class="KT_sorter KT_col_JOINDATE <?php echo $tso_listrsArchonList1->getSortIcon('JOINDATE'); ?>"> <a href="<?php echo $tso_listrsArchonList1->getSortLink('JOINDATE'); ?>">JOIN<br />
              DATE</a> </th>
            <th id="EMAIL" class="KT_sorter KT_col_EMAIL <?php echo $tso_listrsArchonList1->getSortIcon('EMAIL'); ?>"> <a href="<?php echo $tso_listrsArchonList1->getSortLink('EMAIL'); ?>">EMAIL</a> </th>
            <th id="LASTUPDATED" class="KT_sorter KT_col_LASTUPDATED <?php echo $tso_listrsArchonList1->getSortIcon('LASTUPDATED'); ?>"> <a href="<?php echo $tso_listrsArchonList1->getSortLink('LASTUPDATED'); ?>">LAST<br />
              UPDATE</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3 
  if (@$_SESSION['has_filter_tfi_listrsArchonList1'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listrsArchonList1_FULLNAME" id="tfi_listrsArchonList1_FULLNAME" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchonList1_FULLNAME']); ?>" size="22" maxlength="15" /></td>
              <td><input type="text" name="tfi_listrsArchonList1_STATUS" id="tfi_listrsArchonList1_STATUS" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchonList1_STATUS']); ?>" size="10" maxlength="15" /></td>
              <td><input type="text" name="tfi_listrsArchonList1_JOINDATE" id="tfi_listrsArchonList1_JOINDATE" value="<?php echo @$_SESSION['tfi_listrsArchonList1_JOINDATE']; ?>" size="10" maxlength="10" /></td>
              <td><input type="text" name="tfi_listrsArchonList1_EMAIL" id="tfi_listrsArchonList1_EMAIL" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listrsArchonList1_EMAIL']); ?>" size="22" maxlength="15" /></td>
              <td><input type="text" name="tfi_listrsArchonList1_LASTUPDATED" id="tfi_listrsArchonList1_LASTUPDATED" value="<?php echo @$_SESSION['tfi_listrsArchonList1_LASTUPDATED']; ?>" size="10" maxlength="10" /></td>
              <td><input type="submit" name="tfi_listrsArchonList1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsArchonList == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="7"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsArchonList > 0) { // Show if recordset not empty ?>
            <?php
  while (!$rsArchonList->EOF) { 
?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_vw_gramm_archons" class="id_checkbox" value="<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>" />
                    <input type="hidden" name="CUSTOMERID" class="id_field" value="<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>" />
                </td>
                <td><div class="KT_col_FULLNAME"><strong><?php echo KT_FormatForList($rsArchonList->Fields('FULLNAME'), 25); ?></strong><br />
                      <?php echo $rsArchonList->Fields('ADDRESS1'); ?> <?php echo $rsArchonList->Fields('ADDRESS2'); ?><br>
                      <?php echo $rsArchonList->Fields('CITY'); ?> <?php echo $rsArchonList->Fields('STATECD'); ?> <?php echo $rsArchonList->Fields('ZIP'); ?></div></td>
                <td><div class="KT_col_STATUS"><?php echo KT_FormatForList($rsArchonList->Fields('STATUS'), 10); ?></div></td>
                <td><div class="KT_col_JOINDATE"><?php echo KT_FormatForList(KT_formatDate($rsArchonList->Fields('JOINDATE')), 10); ?></div></td>
                <td><div class="KT_col_EMAIL"><?php echo KT_FormatForList($rsArchonList->Fields('EMAIL'), 15); ?></div></td>
                <td><div class="KT_col_LASTUPDATED"><?php echo KT_FormatForList(KT_formatDate($rsArchonList->Fields('LASTUPDATED')), 10); ?></div></td>
                <td><a class="KT_edit_link" href="/home/archons_detail_personal.php?CUSTOMERID=<?php echo $rsArchonList->Fields('CUSTOMERID'); ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> thisthafgdfgt</td>
              </tr>
              <?php
    $rsArchonList->MoveNext(); 
  }
?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listrsArchonList1->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a></div>
</div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchonList->Close();
?>
