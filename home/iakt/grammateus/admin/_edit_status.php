<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("req_cmt", true, "text", "", "", "", "Please enter your request.");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("info@sigmapiphi.org");
  $emailObj->setCC("");
  $emailObj->setBCC("katrinaspp@bellsouth.net,lisa.jeter@gmail.com");
  $emailObj->setSubject("Status Change Request");
  //WriteContent method
  $emailObj->setContent("<p>Archon {SESSION.webFullname} of {SESSION.BOULENAME} has requested the status of the following Archon be updated:</p>\n\n<p>CUSTOMERCD: {GET.CUSTOMERID}<br />\nCHANGE STATUS TO: {POST.archonStatusType}</p>\n\n<p>EXPLANATION:</p>\n<p>{req_cmt}</p>");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

//start Trigger_SendEmail1 trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail1(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("{SESSION.webEmail}");
  $emailObj->setCC("");
  $emailObj->setBCC("");
  $emailObj->setSubject("Status Change Confirmation");
  //WriteContent method
  $emailObj->setContent("<p>Your change request has been sent to the Office of the Grand Boul&eacute;.</p>");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail1 trigger

// begin Recordset
$colname__rsArchonInfo = '-1';
if (isset($_GET['CUSTOMERID'])) {
  $colname__rsArchonInfo = $_GET['CUSTOMERID'];
}
$query_rsArchonInfo = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname__rsArchonInfo, "int"));
$rsArchonInfo = $sigma_modx->SelectLimit($query_rsArchonInfo) or die($sigma_modx->ErrorMsg());
$totalRows_rsArchonInfo = $rsArchonInfo->RecordCount();
// end Recordset

// begin Recordset
$query_rsStatusTypes = "SELECT    vw_gramm_archons.`STATUS` FROM   vw_gramm_archons GROUP BY   vw_gramm_archons.STATUS ORDER BY STATUS ASC";
$rsStatusTypes = $sigma_modx->SelectLimit($query_rsStatusTypes) or die($sigma_modx->ErrorMsg());
$totalRows_rsStatusTypes = $rsStatusTypes->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_request = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_request);
// Register triggers
$ins_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_request->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
$ins_spp_request->registerTrigger("AFTER", "Trigger_SendEmail", 98);
$ins_spp_request->registerTrigger("AFTER", "Trigger_SendEmail1", 98);
// Add columns
$ins_spp_request->setTable("spp_request");
$ins_spp_request->addColumn("req_cmt", "STRING_TYPE", "POST", "req_cmt");
$ins_spp_request->addColumn("type_id", "NUMERIC_TYPE", "POST", "type_id", "1");
$ins_spp_request->addColumn("req_date", "DATE_TYPE", "POST", "req_date", "{now_dt}");
$ins_spp_request->addColumn("req_by", "NUMERIC_TYPE", "POST", "req_by", "{SESSION.webInternalKey}");
$ins_spp_request->addColumn("archon_ref_id", "STRING_TYPE", "VALUE", "{GET.CUSTOMERID}");
$ins_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_request = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_request);
// Register triggers
$upd_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_request->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$upd_spp_request->setTable("spp_request");
$upd_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE", "GET", "req_id");

// Make an instance of the transaction object
$del_spp_request = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_request);
// Register triggers
$del_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$del_spp_request->setTable("spp_request");
$del_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE", "GET", "req_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_request = $tNGs->getRecordset("spp_request");
$totalRows_rsspp_request = $rsspp_request->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<style type="text/css">
<!--
.style2 {
	font-size: 18px;
}
.style4 {
	font-size: 18px;
}
.style6 {
	font-size: 14px;
	margin-bottom: 6px;
}
-->
</style>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" --><?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    [*pagetitle*] </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_request->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_request > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <?php 
// Show IF Conditional show_req_cmt_on_insert_only 
if (@$_GET['req_id'] == "") {
?>
              <tr>
                <td colspan="2" style="padding:6px;"><p class="style6" style="margin-right:200px;">Complete the information below in order  to request a change in<strong> Archon <?php echo $rsArchonInfo->Fields('L2R_FULLNAME'); ?></strong>'s Grand Boul&eacute; status. This request will be forwarded to the Office of the Grand Boul&eacute;.</p>
                </td>
              </tr>
              <tr>
                <td class="KT_th">&nbsp;</td>
                <td><span class="style2"><?php echo $rsArchonInfo->Fields('L2R_FULLNAME'); ?></span></td>
              </tr>
              <tr>
                <td class="KT_th">CURRENT STATUS: </td>
                <td><span class="style4"><?php echo $rsArchonInfo->Fields('STATUS'); ?></span></td>
              </tr>
              <tr>
                <td class="KT_th">REQUESTED STATUS:</td>
                <td>
<input name="archonStatusType" type="radio" value="Active"><span class="style6">Active</span><br />
<input name="archonStatusType" type="radio" value="Active"><span class="style6">Deceased</span><br />
<input name="archonStatusType" type="radio" value="Active"><span class="style6">Delinquent</span><br />
<input name="archonStatusType" type="radio" value="Active"><span class="style6">Dropped</span><br />
<input name="archonStatusType" type="radio" value="Active"><span class="style6">Emeritus</span>
</td>
              </tr>
            <tr>
              <td class="KT_th"><label for="req_cmt_<?php echo $cnt1; ?>">REQUEST:</label><br>
<br>
Please explain the status change in the text box.</td>
              <td><textarea name="req_cmt_<?php echo $cnt1; ?>" id="req_cmt_<?php echo $cnt1; ?>" cols="80" rows="15"><?php echo KT_escapeAttribute($rsspp_request->Fields('req_cmt')); ?></textarea>
              <?php echo $tNGs->displayFieldHint("req_cmt");?> <?php echo $tNGs->displayFieldError("spp_request", "req_cmt", $cnt1); ?> </td></tr>
            <?php } 
// endif Conditional show_req_cmt_on_insert_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_request_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_request->Fields('kt_pk_spp_request')); ?>" />
        <input type="hidden" name="type_id_<?php echo $cnt1; ?>" id="type_id_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_request->Fields('type_id')); ?>" />
        <input type="hidden" name="req_date_<?php echo $cnt1; ?>" id="req_date_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_request->Fields('req_date')); ?>" />
        <input type="hidden" name="req_by_<?php echo $cnt1; ?>" id="req_by_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($rsspp_request->Fields('req_by')); ?>" />
        <?php
    $rsspp_request->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['req_id'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
  <!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchonInfo->Close();

$rsStatusTypes->Close();
?>
