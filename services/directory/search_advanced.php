<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//Search functions include
require_once('../includes/MXSearch/MX_Search.inc.php');

//Start Search Configuration
$KT_MxSearchTables = new KT_MXSearchConfig();

// Start vw_search
$KT_MxSearchTables->addTable("vw_search", 5, "L2R_FULLNAME", "LOCATION", "/services/directory/results.php?id=", "WEB_ID", "");
$KT_MxSearchTables->addField("vw_search", "REGIONNAME", 3);
$KT_MxSearchTables->addField("vw_search", "REGIONCD", 3);
$KT_MxSearchTables->addField("vw_search", "BOULENAME", 3);
$KT_MxSearchTables->addField("vw_search", "CHAPTERID", 3);
$KT_MxSearchTables->addField("vw_search", "BOULECITY", 3);
$KT_MxSearchTables->addField("vw_search", "BOULESTATECD", 3);
$KT_MxSearchTables->addField("vw_search", "LOCATION", 3);
$KT_MxSearchTables->addField("vw_search", "STATUS", 3);
$KT_MxSearchTables->addField("vw_search", "FULLNAME", 3);
$KT_MxSearchTables->addField("vw_search", "L2R_FULLNAME", 5);
$KT_MxSearchTables->addField("vw_search", "PREFIX", 3);
$KT_MxSearchTables->addField("vw_search", "FIRSTNAME", 3);
$KT_MxSearchTables->addField("vw_search", "MIDDLEINITIAL", 3);
$KT_MxSearchTables->addField("vw_search", "LASTNAME", 4);
$KT_MxSearchTables->addField("vw_search", "HOMEPHONE", 3);
$KT_MxSearchTables->addField("vw_search", "SUFFIX", 3);
$KT_MxSearchTables->addField("vw_search", "DESIGNATIONLST", 3);
$KT_MxSearchTables->addField("vw_search", "EMAIL", 3);
$KT_MxSearchTables->addField("vw_search", "WORK_EMAIL", 3);
$KT_MxSearchTables->addField("vw_search", "WEB_EMAIL", 3);
$KT_MxSearchTables->addField("vw_search", "UPDATEDBY", 3);
$KT_MxSearchTables->addField("vw_search", "SKILLCDLST", 3);
$KT_MxSearchTables->addField("vw_search", "OCCUPATIONCD", 3);
$KT_MxSearchTables->addField("vw_search", "JOBTITLE", 5);
$KT_MxSearchTables->addField("vw_search", "ORGNAME", 5);
$KT_MxSearchTables->addField("vw_search", "CITY", 3);
$KT_MxSearchTables->addField("vw_search", "STATECD", 3);
$KT_MxSearchTables->addField("vw_search", "ZIP", 3);
$KT_MxSearchTables->addField("vw_search", "BIO", 5);
$KT_MxSearchTables->addField("vw_search", "SPOUSENAME", 3);
$KT_MxSearchTables->addField("vw_search", "OCCUPATION_TYPE", 4);
$KT_MxSearchTables->addField("vw_search", "JOB_TITLE", 4);
$KT_MxSearchTables->addField("vw_search", "COMPANYNAME", 5);
$KT_MxSearchTables->addField("vw_search", "COMPANYCITY", 3);
$KT_MxSearchTables->addField("vw_search", "COMPANYSTCD", 3);
$KT_MxSearchTables->addField("vw_search", "JOB_DESCRIPTION", 3);
$KT_MxSearchTables->addField("vw_search", "DEGREE", 3);
$KT_MxSearchTables->addField("vw_search", "INSTITUTION", 5);
$KT_MxSearchTables->addField("vw_search", "MAJOR", 4);
$KT_MxSearchTables->addField("vw_search", "MINOR1", 3);
$KT_MxSearchTables->addField("vw_search", "MINOR2", 3);
$KT_MxSearchTables->addField("vw_search", "COMMENT", 3);
$KT_MxSearchTables->addField("vw_search", "CPOSITION", 5);
// End vw_search

$currentPage = $_SERVER["PHP_SELF"];

// Begin Search Recordset

mysql_select_db($database_sigma_modx, $sigma_modx);

$KTSE_rsSearch = new KT_MXSearch();
$KTSE_rsSearch->setSearchType("boolean fulltext");
$KTSE_rsSearch->setConnection($sigma_modx, "MySQL");
$KTSE_rsSearch->setCache("src_cache_cah", 1);
$KTSE_rsSearch->setTempTable("src_temp_tmp");
$KTSE_rsSearch->setTables($KT_MxSearchTables);
$KTSE_rsSearch->computeAll("q");

$maxRows_rsSearch = 10;
$pageNum_rsSearch = 0;
if (isset($HTTP_GET_VARS['pageNum_rsSearch'])) {
	$pageNum_rsSearch = $HTTP_GET_VARS['pageNum_rsSearch'];
}
$startRow_rsSearch = $pageNum_rsSearch * $maxRows_rsSearch;
$rsSearch = $KTSE_rsSearch->getRecordset($startRow_rsSearch, $maxRows_rsSearch);
$row_rsSearch = mysql_fetch_assoc($rsSearch);
if (isset($HTTP_GET_VARS['totalRows_rsSearch'])) {
	$totalRows_rsSearch = $HTTP_GET_VARS['totalRows_rsSearch'];
} else {
	$totalRows_rsSearch = $KTSE_rsSearch->getTotalRows();
}
$totalPages_rsSearch = (int)(($totalRows_rsSearch-1)/$maxRows_rsSearch);
// End Search Recordset
?>
<?php $KT_MXSearchKeywords = $KTSE_rsSearch->getKeywords(); ?>
<?php
$queryString_rsSearch = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsSearch") == false && 
        stristr($param, "totalRows_rsSearch") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsSearch = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsSearch = sprintf("&totalRows_rsSearch=%d%s", $totalRows_rsSearch, $queryString_rsSearch);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  dir="ltr" lang="en-EN"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | Advanced Search</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<link rel="stylesheet" href="../includes/MXSearch/styles/MXSearch.css" type="text/css">
<h1 class="yellow">ADVANCED SEARCH</h1>
<div class="contentBlock">
<table border="0" cellspacing="4" cellpadding="4">
  <tr valign="top">
    <td width="66%"><form name="MX_Search_Form" method="GET" action="">
  <strong>Search</strong>
  <input name="q" type="text" value="<?php echo $KT_MXSearchKeywords; ?>" size="75" />
  <input type="submit" name="Submit" value="Go">
</form>
<br>
<table width="90%" border="0">
  <?php if ($totalRows_rsSearch > 0) { // Show if recordset not empty ?>
    <tr>
      <td><p>Your search for <strong><?php echo $KT_MXSearchKeywords; ?></strong> returned
        the following results:</p>
        <table width="100%"  border="0" align="left" cellpadding="2" cellspacing="0">
            <?php $offset = 0; ?>
            <?php do { ?>
              <tr>
                <td><div style="margin: 0 0 0 18px;">
                    <p><?php //echo (min($startRow_rsSearch + 1, $totalRows_rsSearch)) + $offset?><a href="<?php echo $row_rsSearch['url_cah']; ?>" target="_self"><?php echo $row_rsSearch['title_cah']; ?></a><br>
                          <font color="#339900"><?php echo $KTSE_rsSearch->formatDescription($row_rsSearch['shortdesc_cah']); ?></font> </p>
                </div></td>
              </tr>
              <?php $offset++; ?>
              <?php } while ($row_rsSearch = mysql_fetch_assoc($rsSearch)); ?>
        </table></td>
    </tr>
    <tr>
      <td><p>Showing matches <?php echo (min($startRow_rsSearch + 1, $totalRows_rsSearch)) ?> - <?php echo min($startRow_rsSearch + $maxRows_rsSearch, $totalRows_rsSearch) ?> from <?php echo $totalRows_rsSearch ?></p></td>
    </tr>
    <tr>
      <td><?php if ($totalRows_rsSearch > 10) { // Show if recordset not empty ?>
          <table border="0" align="left">
              <tr>
                <td align="center"><?php if ($pageNum_rsSearch > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_rsSearch=%d%s", $currentPage, 0, $queryString_rsSearch); ?>">First</a>
                      <?php } // Show if not first page ?>
                  <?php if ($pageNum_rsSearch == 0) { // Show if first page ?>
                    First
                    <?php } // Show if first page ?>                </td>
                <td align="center"><?php if ($pageNum_rsSearch > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_rsSearch=%d%s", $currentPage, max(0, $pageNum_rsSearch - 1), $queryString_rsSearch); ?>">Prev</a>
                      <?php } // Show if not first page ?>
                  <?php if ($pageNum_rsSearch == 0) { // Show if first page ?>
                    Prev
                    <?php } // Show if first page ?>                </td>
                <td align="center"><?php if ($pageNum_rsSearch < $totalPages_rsSearch) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_rsSearch=%d%s", $currentPage, min($totalPages_rsSearch, $pageNum_rsSearch + 1), $queryString_rsSearch); ?>">Next</a>
                      <?php } // Show if not last page ?>
                  <?php if ($pageNum_rsSearch >= $totalPages_rsSearch) { // Show if last page ?>
                    Next
                    <?php } // Show if last page ?>                </td>
                <td align="center"><?php if ($pageNum_rsSearch < $totalPages_rsSearch) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_rsSearch=%d%s", $currentPage, $totalPages_rsSearch, $queryString_rsSearch); ?>">Last</a>
                      <?php } // Show if not last page ?>
                  <?php if ($pageNum_rsSearch >= $totalPages_rsSearch) { // Show if last page ?>
                    Last
                    <?php } // Show if last page ?>                </td>
              </tr>
                </table>
          <?php } // Show if recordset not empty ?></td>
    </tr>
    <?php } // Show if recordset not empty ?>
      <?php 
// Show IF Conditional region1 
if (@$_GET['q'] == NULL) {
?>
<tr>
        <td>&nbsp;</td>
</tr>
  <?php } 
// endif Conditional region1
?>
<?php if (($totalRows_rsSearch == 0) and (@$_GET['q'] != NULL) ){ // Show if recordset empty ?>
<tr>
          <td>No records found matching your criteria! </td>
</tr>
<?php } // Show if recordset empty ?>
</table>
</td>
    <td bgcolor="#E7E7E7"><p><b>Search Options </b><br /> Searching always ignores upper and lower case.<br />
  Search results automatically match suffixes such as &quot;-s&quot; and &quot;-ed&quot;. </p>
<p><em>Search examples</em>:</p>

  <p><code class="literal">apple  banana<br />
  </code><code>apple or banana<br />
  </code><code>apple | banana <br />
  </code>Finds archons that have the word &ldquo;<span class="quote">apple</span>&rdquo; or &ldquo;banana&rdquo; in their profile.</p>
  <p><code class="literal">+apple +juice<br />
  </code><code>apple and juice<br />
  </code><code>apple &amp; juice <br />
  </code>Finds archons that have the word &ldquo;<span class="quote">apple</span>&rdquo; and the word &ldquo;banana&rdquo; in their profile. </p>
  <p><code class="literal">+apple macintosh<br />
  </code>Finds archons that have the word &ldquo;<span class="quote">apple</span>&rdquo; in their profile. The archon's profile is displayed higher in the results list if the archon also has the word
      &ldquo;<span class="quote">macintosh</span>&rdquo; in their profile.</p>
  <p><code>+apple -macintosh<br />
  </code><code>apple not macintosh<br />
  </code>Finds archons that have the word &ldquo;<span class="quote">apple</span>&rdquo; in their profile. Only archon profiles that also have  the word
      &ldquo;<span class="quote">macintosh</span>&rdquo; in their profile will be displayed.</p>
  <p><code>+apple ~macintosh<br />
  </code>Finds archons that have the word &ldquo;<span class="quote">apple</span>&rdquo; in their profile. The archon's profile is displayed lower in the results list if the archon has the word
      &ldquo;<span class="quote">macintosh</span>&rdquo; in their profile.</p>
  <p><code>apple*<br />
  </code>Find profiles that contain words such as &ldquo;<span class="quote">apple</span>&rdquo;,
      &ldquo;<span class="quote">apples</span>&rdquo;, &ldquo;<span class="quote">applesauce</span>&rdquo;, or
      &ldquo;<span class="quote">applet</span>&rdquo;.      </li>    
</p>
  <p><b>Search for Phrases</b><br>
In addition to searching for individual words, you can search for exact phrases of indexed words by enclosing them in double-quotes. </p>
<p><em>Search phrase examples</em>:</p>

  <p><code>books and "romance novels"</code><br />
  <code>books or "romance novels"</code><br />
  <code>books not "romance novels"</code></p>
  </td>
</tr>
</table>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
