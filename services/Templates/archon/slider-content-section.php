<!-- large image slider start -->
    <div class="full-width margin-bottom-medium" style="display:visible;">
        <div class="row">
            <div class="span-8">
                <div class="inner">
						<a href="[~[+slider-slide-1-url+]~]" class="slide">
						<img src="[+slider-slide-1-image+]" alt="[+pagetitle+]">
						<div class="overlay">
							<h2>[+pagetitle+]</h2>
							[+slider-slide-1-caption+]  <span class="link">[+introtext+]</span> 
						</div>
					</a>
                </div>
            </div>
        </div>
    </div>
<!-- large image slider end -->