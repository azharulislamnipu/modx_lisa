<?php

/**
 * Holds an Archon's information
 * @author Pat Heard {http://fullahead.org}
 */

class ArchonBean {

        private $customerid = -1;
        private $customertype = '';
        private $chapterid = '';
        private $regioncd = '';
        private $status = '';

        private $fullname = '';
        private $prefix = '';
        private $first_name = '';
        private $middle_name = '';
        private $last_name = '';
        private $suffix = '';
        private $birth_date = '';
        private $join_date = '';

        private $address1 = '';
        private $address2 = '';
        private $city = '';
        private $state = '';
        private $zip = '';

        private $phone_home = '';
        private $phone_work = '';
        private $phone_mobile = '';
        private $email_home = '';
        private $email_work = '';

        private $officer_position = '';
        private $officer_status = '';

	public function __construct(array $args = null){

		// Set default arguments
		$args = array_merge(array(
                    'customerid' => -1,
                    'customertype' => '',
                    'chapterid' => '',
                    'regioncd' => '',
                    'status' => '',
                    'fullname' => '',
                    'prefix' => '',
                    'first_name' => '',
                    'middle_name' => '',
                    'last_name' => '',
                    'suffix' => '',
                    'address1' => '',
                    'address2' => '',
                    'birth_date' => '',
                    'join_date' => '',
                    'city' => '',
                    'state' => '',
                    'zip' => '',
                    'phone_home' => '',
                    'phone_work' => '',
                    'phone_mobile' => '',
                    'email_home' => '',
                    'email_work' => '',
                    'officer_position' => '',
                    'officer_status' => ''
		), $args);

                $this->customerid = $args['customerid'];
                $this->customertype = $args['customertype'];
                $this->chapterid = $args['chapterid'];
                $this->regioncd = $args['regioncd'];
                $this->status = trim($args['status']);

                $this->fullname = $args['fullname'];
                $this->prefix = $args['prefix'];
                $this->first_name = $args['first_name'];
                $this->middle_name = $args['middle_name'];
                $this->last_name = $args['last_name'];
                $this->suffix = $args['suffix'];
                $this->birth_date = $args['birth_date'];
                $this->join_date = $args['join_date'];

                $this->address1 = $args['address1'];
                $this->address2 = $args['address2'];
                $this->city = $args['city'];
                $this->state = $args['state'];
                $this->zip = $args['zip'];

                $this->phone_home = $args['phone_home'];
                $this->phone_work = $args['phone_work'];
                $this->phone_mobile = $args['phone_mobile'];
                $this->email_home = $args['email_home'];
                $this->email_work = $args['email_work'];

                $this->officer_position = $args['officer_position'];
                $this->officer_status = $args['officer_status'];
	}

	public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
	}

        public function isEmeritus() {
            return $this->customertype === 'Emeritus';
        }

	public function __toString(){
            return
                ' customerid: ' . $this->customerid .
                ' customertype: ' . $this->customertype .
                ' chapterid: ' . $this->chapterid .
                ' regioncd: ' . $this->regioncd .
                ' status: ' . $this->status .
                ' fullname: ' . $this->fullname .
                ' prefix: ' . $this->prefix .
                ' first_name: ' .$this->first_name .
                ' middle_name: ' . $this->middle_name .
                ' last_name: ' . $this->last_name .
                ' suffix: ' . $this->suffix .
                ' address1: ' . $this->address1 .
                ' address2: ' . $this->address2 .
                ' birth_date: ' . $this->birth_date .
                ' join_date: ' . $this->join_date .
                ' city: ' . $this->city .
                ' state: ' . $this->state .
                ' zip: ' . $this->zip .
                ' phone_home: ' . $this->phone_home .
                ' phone_work: ' . $this->phone_work .
                ' phone_mobile: ' . $this->phone_mobile .
                ' email_home: ' . $this->email_home .
                ' email_work: ' . $this->email_work .
                ' officer_position: ' . $this->officer_position .
                ' officer_status: ' . $this->officer_status;
	}

}

?>
