<?php

/*
 * Purpose:
 *   1. Create WordPress user accounts from CSV file.
 *   2. Assign created users to Archon Network and Boule sites with given role.
 * Author:
 *   Pat Heard
 *   http://fullahead.org
 * Date:
 *   2014-02-18
 */

// Change this to "dev2.sigmapiphi.org" or "www.sigmapiphi.org" for loading DEV and PROD
define( "DOMAIN", "dev2.sigmapiphi.org" );
define( "PATH", "boules" );
define( "CSV_INPUT", "accounts.csv" );

// Number of accounts to add at once, and the delay to wait before adding another chunk
define( "CHUNK_SIZE", 10 );
define( "CHUNK_DELAY", 1 );

define( "MAX_EXECUTION_TIME", ini_get( "max_execution_time" ) );
define( "ARCHON_NETWORK_ID", 1 );
define( "WP_USE_THEMES", false );

// Enable wordpress function use in the scripts
require( "../wp-blog-header.php" );






// Read in the CSV file and create an associative array of accounts
$accounts = csv_to_array( CSV_INPUT );
$bouleIds = array();
$failed = array();
$missingBoules = array();
$chunk = 0;
$count = 0;

// Loop over the accounts and add them
foreach( $accounts as $a ){

	// Pause the script every set number of accounts.  This prevents hammering
	// the database with 11K+ inserts/updates.
	++$chunk;
	if ( $chunk === CHUNK_SIZE ) {
		$chunk = 0;

		// Sleep and reset the script execution timeout
		sleep( CHUNK_DELAY );
		set_time_limit( MAX_EXECUTION_TIME );
	}

	// Create the account
	$user_id = wp_insert_user(array(
		"user_login"	=> $a[ "username" ],
		"user_pass"		=> $a[ "PASSWORD" ]
	));

	// Check if there was an error about an existing account.
	// Lookup the existing account's ID if yes.
	if( is_wp_error( $user_id ) && $user_id->get_error_code() === "existing_user_login" ) {
		$user = get_user_by( "login", $a[ "username" ] );
		$user_id = $user ? $user->ID : false;

		// Update the user's password
		if ( $user_id !== false ) {
			wp_set_password( $a[ "PASSWORD" ], $user_id );
		}
	}

	// Add user to the Boule sites if they were created successfully
	if( $user_id !== false && !is_wp_error( $user_id ) ) {

		// Lookup the Boule ID
		$boule = $a["boule-site"];
		$bouleId = isset( $bouleIds[ $boule ] ) ? $bouleIds[ $boule ] : 0;
		if ( $bouleId === 0 ) {
			$bouleId = get_blog_id_from_url( DOMAIN, "/" . PATH . "/" . $boule . "/" );
			$bouleIds[ $boule ] = $bouleId;
		}

		// Get the user's role, defaulting to Archon if not specified
		$role = !empty( $a[ "ROLE" ] ) ? strtolower( $a[ "ROLE" ] ) : "archon";

		// Add to Archon Network and Boule site
		add_user_to_blog( ARCHON_NETWORK_ID, $user_id, $role );
		if ( $bouleId !== 0 ) {
			add_user_to_blog( $bouleId, $user_id, $role );

		// Store the missing member boule site
		} else if ( !in_array( $boule, $missingBoules ) ) {
			$missingBoules[] = $boule;
		}
		++$count;

	} else {
		$failed[] = $a[ "username" ];
	}
}

echo "Successfully added " . $count . " of " . count( $accounts ) . " accounts.<br>";

// Account usernames that weren't added
if ( !empty( $failed ) ) {
	echo "The following accounts were not added: <br>";
	print_r( $failed );
}

// Boule sites that don't exist
if ( !empty( $missingBoules ) ) {
	echo "The following Member Boule sites were not found: <br>";
	print_r( $missingBoules );
}

// Converts a CSV file to an associative array
function csv_to_array( $filename = "", $delimiter = "," ) {
    if( !file_exists( $filename ) || !is_readable( $filename ) ) {
        return FALSE;
	}

    $header = NULL;
    $data = array();
    if ( ( $handle = fopen( $filename, "r" ) ) !== FALSE ) {
        while ( ( $row = fgetcsv( $handle, 1000, $delimiter ) ) !== FALSE ) {
            if( !$header ) {
                $header = $row;
			} else {
                $data[] = array_combine( $header, $row );
			}
        }
        fclose( $handle );
    }
    return $data;
}

?>