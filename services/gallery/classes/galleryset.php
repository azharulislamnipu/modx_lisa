<?php
/**
 * SimpleViewer admin package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

/**
 * GallerySet class
 *
 * @access public
 * @package svManager
 */
Class GallerySet
{
 /**
  * @var array gallery names and paths
  * galleriesData is a 2-D array of the form
  * array(0=>array('galleryRef'=>1, 'title'=>'Nature Shots', 'path'=>'g1/', 'galleryType'=>'simpleviewer'), 1=>array('galleryRef'=>2, 'title'=>'Landscapes', 'path'=>'g2/', 'galleryType'=>'tiltviewer'))
  * 'title' contains raw unencoded html tags.
  * Note that 'path' is really a path (backslashes on Windows)
  * The index of the first dimension (galleryIndex) is arbitrary and will change on sort
  * As the user deletes galleries, gaps will appear in the sequence of gallery refs.
  * The addGallery function fills these gaps in ascending order
  */
  var $galleriesData = array();
  
 /**
  * @var array preferences for all galleries. Sort order on galleries page.
  * 
  */
  var $galleriesPrefs = array('sortColumn' => 'galleryRef', 'sortAscending' => true);
  

 /**
  * @var object instance of PathParser class
  */
  var $pathParser;

 /**
  * @var object instance of XmlParser class
  */
  var $xmlParser;
  
 /**
  * @var object instance of HtmlEntityDecode class
  */
  var $htmlEntityDecode;
  
  /**
   * Constructs GallerySet
   */
  function GallerySet()
  {
    $this->pathParser = new PathParser();
    $this->xmlParser = new xmlParser();
    $this->htmlEntityDecode = new HtmlEntityDecode();
    $this->rCopy = new RCopy();
    $galleriesData = $this->readGalleriesData();
    if ($galleriesData !== false)
    {
      // upgrade from older versions
      foreach ($galleriesData as $key=>$galleryData)
      {
        if (!isset($galleryData['galleryType']))
        {
          $galleriesData[$key]['galleryType'] = 'simpleviewer';
        }
      }
      $this->galleriesData = $galleriesData;
      $this->saveGalleriesData();
    }
    $galleriesPrefs = $this->readGalleriesPrefs();
    if ($galleriesPrefs !== false)
    {
      $this->galleriesPrefs = array_merge($this->galleriesPrefs, $galleriesPrefs);
    }
    $this->saveGalleriesPrefs();
  }
  
 /**
  * Read gallery data from file
  *
  * Note that there is no error message for missing gallery data file - normal condition on install
  * @access private
  * @return array
  */
  function readGalleriesData()
  {
    if (!file_exists(GALLERY_DATA_FILE)) return false;
    $galleriesData = @file_get_contents(GALLERY_DATA_FILE);
    if ($galleriesData === false)
    {
      trigger_error('cannot read '.GALLERY_DATA_FILE, E_USER_ERROR);
    }
    $data = @unserialize($galleriesData);
    if ($data === false)
    {
      trigger_error(GALLERY_DATA_FILE.' file is corrupted, see user manual', E_USER_ERROR);
      return false;
    }
    if (count($data) == 0) return $data;
    foreach ($data as $key => $galleryData)
    {
      if (!isset($galleryData['galleryRef']))
      {
        $data[$key]['galleryRef'] = $key;
      }
    }
    return $data;    
  }
  
 /**
  * Read galleries preferences file
  *
  * @access private
  * @return array
  */
  function readGalleriesPrefs()
  {
    if (!file_exists(GALLERIES_PREFS_FILE)) return false;
    {
      $galleriesPrefs = @file_get_contents(GALLERIES_PREFS_FILE);
      if ($galleriesPrefs === false) return false;
      $data = @unserialize($galleriesPrefs);
      if ($data === false) return false;
      return $data;
    }
  }
 
 /**
  * returns gallery data
  *
  * @access public
  * @return array
  */
  function getGalleriesData()
  {
    return $this->galleriesData;
  }
  
 /** returns gallery title
  *
  * @access public
  * @return string
  * @param string gallery index
  */
  function getGalleryTitle($key)
  {
    return $this->galleriesData[$key]['title'];
  }
  
 /** returns gallery reference
  *
  * @access public
  * @return string
  * @param string gallery index
  */
  function getGalleryRef($key)
  {
    return $this->galleriesData[$key]['galleryRef'];
  }
  
 /** returns gallery path (backslashes in Windows)
  *
  * @access public
  * @return string
  * @param string gallery reference
  */
  function getGalleryPath($key)
  {
    return $this->galleriesData[$key]['path'];
  }
  
 /** returns gallery type
  *
  * @access public
  * @return string
  * @param string gallery reference
  */
  function getGalleryType($key)
  {
    return $this->galleriesData[$key]['galleryType'];
  }
  
 /** set gallery type
  *
  * @access public
  * @return void
  * @param string gallery reference
  * @param string new value
  */
  function setGalleryType($key, $value)
  {
    $this->galleriesData[$key]['galleryType'] = $value;
  }
  
 /**
  * returns galleries prefs
  *
  * @access public
  * @return array
  */
  function getGalleriesPrefs()
  {
    return $this->galleriesPrefs;
  }
  
 /**
  * Sets galleriesPrefs['newGalleryPref']
  *
  * @access public
  * @return void
  * @param string gallery type
  */
  function setNewGalleryPref($galleryType)
  {
    $this->galleriesPrefs['newGalleryPref'] = $galleryType;
  }
    
 /**
  * set galleriesData['galleryRef']
  *
  * @access public
  * @return integer index in galleriesData array
  * @param string galleryRef
  */
  function addGalleriesDataRecord(&$gallery, $galleryType)
  {
    $this->galleriesData[] = array('galleryRef'=>$gallery->getGalleryRef(), 'path'=>$gallery->getGalleryPathRelSvm(), 'title'=>$gallery->getGalleryTitle(), 'galleryType'=>$galleryType);
    return key($this->galleriesData);
  }

 /**
  * set gallery data['path']
  *
  * @access public
  * @return void
  * @param string gallery index
  * @param string new value for gallery data item
  */
  function setGalleriesDataPath($galleryIndex, $path)
  {
    $this->galleriesData[$galleryIndex]['path'] = $path;
    return;
  }
  
 /**
  * set galleriesData[$ref]['title']
  *
  * @access public
  * @return void
  * @param string gallery ref
  * @param string gallery title with or without html
  */
  function setGalleriesDataTitle($index, $title)
  {
    $this->galleriesData[$index]['title'] = $title;
  }
  
 /**
  * set galleriesData['galleryType']
  *
  * @access public
  * @return void
  * @param string gallery ref
  * @param string gallery type
  */
  function setGalleriesDataGalleryType($ref, $galleryType)
  {
    $this->galleriesData[$ref]['galleryType'] = $galleryType;
  }
  
 /* save gallery data to file
  *
  * @access public
  * @return boolean success
  */
  function saveGalleriesData()
  {
    if (@file_put_contents(GALLERY_DATA_FILE, serialize($this->galleriesData), FPC_LOCK))
    {
      return true;
    }
    else
    {
      trigger_error('could not save gallery data to '.GALLERY_DATA_FILE, E_USER_WARNING);
      return false;
    }
  }
  
 /* save galleries preferences to file
  *
  * @access public
  * @return boolean success
  */
  function saveGalleriesPrefs()
  {
    
    if (@file_put_contents(GALLERIES_PREFS_FILE, serialize($this->galleriesPrefs), FPC_LOCK))
    {
      return true;
    }
    else
    {
      trigger_error('could not save gallery preferences to '.GALLERIES_PREFS_FILE, E_USER_NOTICE);
      return false;
    }
  }

 /**
  * Find next available gallery reference number
  *
  * @access public
  * @return integer gallery reference number
  */
  /*function nextGalleryRef()
  {
    $galleryRef = 1;
    while(file_exists(GALLERY_PREFIX.$galleryRef) || $this->galleryRefExists($galleryRef))
    {
      $galleryRef++;
    }
    return $galleryRef;
  }*/
  
 /**
  * Find next available gallery reference number
  *
  * @access public
  * @return integer gallery reference number
  */
  function nextGalleryRef()
  {
    $nextRef = 1;
    if (count($this->galleriesData) > 0)
    {
      foreach ($this->galleriesData as $key=>$gallery)
      {
        $ref[$key] = $gallery['galleryRef'];
      }
      $nextRef = 1 + max($ref);
    }
    while(file_exists(GALLERY_PREFIX.$nextRef))
    {
      $nextRef++;
    }
    return $nextRef;
  }
  
 /**
  * Find next available gallery reference number for import
  * Similar to nextGalleryRef but omits check for already exists
  *
  * @access public
  * @return integer gallery reference number
  */
  function nextImportedGalleryRef()
  {
    if (count($this->galleriesData) == 0) return 1;
    foreach ($this->galleriesData as $key=>$gallery)
    {
      $ref[$key] = $gallery['galleryRef'];
    }
    return 1 + max($ref);
  }

  
 /**
  * Check for presence of existing gallery reference
  * Currently not in use
  *
  * @access private
  * @return boolean reference already exists
  * @param string reference
  */
  function galleryRefExists($ref)
  {
    foreach ($this->galleriesData as $key=>$gallery)
    {
      if ($gallery['galleryRef'] == $ref) return true;
    }
    return false;
  }

 /**
  * Convert root url from import page to relative path
  * Uses $_SERVER['PHP_SELF'] and pathparser
  * 
  * @access public
  * @return string path relative to svManager
  * @param string root url
  */
  function relPathFromRootUrl($rootUrl)
  {
    $rootUrl = trim($rootUrl);
    $rootUrl = '/'.ltrim($rootUrl, '/');
    $rootUrl = rtrim($rootUrl, '/');
    $relPath = rtrim($this->pathParser->findRelativePath($_SERVER['PHP_SELF'], $rootUrl), '/');
    $galleryPath = $relPath.DIRECTORY_SEPARATOR;
    return $galleryPath;
  }
  
 /**
  * Import existing gallery
  *
  * @access public
  * @return string gallery type
  * @param string url for import folder as entered by user
  */
  function importGallery($rootUrl, $supportedGalleries)
  {
    // convert any user-entered backslashes to forward slashes
    $rootUrl = str_replace('\\', '/', $rootUrl);
    $rootUrl = trim($rootUrl);
    $rootUrl = '/'.trim($rootUrl, '/');
    // findRelativePath always returns forward slashes
    $relPath = rtrim($this->pathParser->findRelativePath($_SERVER['PHP_SELF'], $rootUrl), '/');
    if ($relPath == '')
    {
      trigger_error('no import &ndash; import folder not specified', E_USER_NOTICE);
      return false;
    }
    // Note that realpath will return backslashes on Windows servers
    $absPath = realpath($relPath);
    if (!file_exists($absPath))
    {
      trigger_error('no import &ndash; cannot find folder '.$relPath, E_USER_NOTICE);
      return false;
    }
    $galleryType = $this->findGalleryType($absPath);
    if ($galleryType === false) return false;
    $xmlFileName = 'gallery.xml';
    switch($galleryType)
    {
      case 'simpleviewer' :
        $masterPath = SV_MASTER_PATH;
        break;
      case 'simpleviewer2' :
        $masterPath = SV2_MASTER_PATH;
        break;
      case 'tiltviewer' :
        $masterPath = TV_MASTER_PATH;
        break;
      case 'autoviewer' :
        $masterPath = AV_MASTER_PATH;
        break;
      case 'postcardviewer' :
        $masterPath = PV_MASTER_PATH;
        break;
      default :
        trigger_error('no import &ndash; '.$galleryType.' not supported', E_USER_NOTICE);
        return false;
    }
    
    $galleryPath = $relPath.DIRECTORY_SEPARATOR;
    
    foreach ($this->galleriesData as $gallery)
    {
      if ($gallery['path'] == $galleryPath)
      {
        trigger_error('no import &ndash; gallery is already on the svManager list', E_USER_NOTICE);
        return false;
      }
    }
    if (@!is_writable($absPath))
    {
      trigger_error('no import &ndash; cannot write to import gallery folder', E_USER_NOTICE);
      return false;
    }
    if (file_exists($absPath.DIRECTORY_SEPARATOR.'gallery.xml') && @!is_writable($absPath.DIRECTORY_SEPARATOR.'gallery.xml'))
    {
      trigger_error('no import &ndash; cannot write to gallery.xml file; please check permissions');
      return false;
    }
    if (@!copy($absPath.DIRECTORY_SEPARATOR.$xmlFileName, $absPath.DIRECTORY_SEPARATOR.'gallerysvmbak.xml'))
    {
      trigger_error('could not backup the gallery.xml file', E_USER_NOTICE);
    }
    if (!file_exists($absPath.DIRECTORY_SEPARATOR.'index.php'))
    {
      if (@!copy($masterPath.'index.php', $absPath.DIRECTORY_SEPARATOR.'index.php'))
      {
        trigger_error('no import &ndash; could not copy index.php; please check permissions', E_USER_NOTICE);
        return false;
      }
    }
    if (($galleryType == 'simpleviewer2') && !file_exists($absPath.DIRECTORY_SEPARATOR.'svcore'))
    {
      $fileCount = $this->rCopy->doCopy($masterPath.DIRECTORY_SEPARATOR.'svcore', $absPath.DIRECTORY_SEPARATOR.'svcore');
      if ($fileCount == 0)
      {
        trigger_error( 'svcore file copy incomplete '.$fileCount.' files copied', E_USER_NOTICE);
      }
    }
    return $galleryType;
  }
  
 /**
  * Distinguish between gallery types based on name of xml file and gallery tag
  *
  * @access public
  * @returns string gallery type
  * @param string absolute path to gallery directory
  */
  function findGalleryType($absPath)
  {
    if (file_exists($absPath.DIRECTORY_SEPARATOR.'svcore'))
    {
      // version 2.1.0+
      return 'simpleviewer2';
    }
    if (file_exists($absPath.DIRECTORY_SEPARATOR.'simpleviewer.swf'))
    {
      // version 2.0.0+
      return 'simpleviewer2';
    }
    if (file_exists($absPath.DIRECTORY_SEPARATOR.'autoviewer.swf'))
    {
      return 'autoviewer';
    }
    if (file_exists($absPath.DIRECTORY_SEPARATOR.'imagedata.xml'))
    {
      trigger_error('svManager cannot import older versions of postcardviewer', E_USER_NOTICE);
      return false;
    }
    if (!file_exists($absPath.DIRECTORY_SEPARATOR.'gallery.xml'))
    {
      trigger_error('no import cannot find the gallery xml file', E_USER_NOTICE);
      return false;
    }
    $xmlSource = @file_get_contents($absPath.DIRECTORY_SEPARATOR.'gallery.xml');
    if ($xmlSource === false)
    {
      trigger_error('no import, cannot read gallery xml file ('.$absPath.DIRECTORY_SEPARATOR.'gallery.xml)', E_USER_NOTICE);
      return false;
    }
    $check = new XML_check;
    if(!$check->check_string($xmlSource))
    {
      trigger_error('no import, XML in '.$absPath.DIRECTORY_SEPARATOR.'gallery.xml is not well-formed. '.$check->get_full_error(), E_USER_NOTICE);
      return false;
    }
    $p = xml_parser_create('UTF-8');
    xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($p, XML_OPTION_SKIP_WHITE, 1);
    if (0 === @xml_parse_into_struct($p, $xmlSource, $vals, $index))
    {
      trigger_error('no import, failed to parse the xml file ('.$xmlSource.').', E_USER_NOTICE);
      return false;
    }       
    xml_parser_free($p);
    $indexKeys = array_keys($index);
    $galleryTag = strtolower($indexKeys[0]);
    switch ($galleryTag)
    {
      case 'gallery' :
        $xmlStruct = array('vals'=>$vals, 'index'=>$index);
        $attributes = $this->xmlParser->parseAttributes($xmlStruct, 'gallery');
        $attributes = array_change_key_case($attributes, CASE_LOWER);
        if (array_key_exists('celldimension', $attributes))
        {
          $galleryType = 'postcardviewer';
          break;
        }
        $galleryType = 'autoviewer';
        break;
      case 'simpleviewergallery' :
        $galleryType = 'simpleviewer';
        break;
      case 'tiltviewergallery' :
        $galleryType = 'tiltviewer';
        break;
      default :
        trigger_error('no import, cannot recognise gallery type '.$galleryType, E_USER_NOTICE);
        return false;
    }
    return $galleryType;
  }

 /**
  * Remove gallery from the svManager database without deleting it
  *
  * @access public
  * @return boolean success
  * @param integer key to gallery in galleriesData
  */
  function removeGallery($key)
  {
    if (!isset($this->galleriesData[$key]))
    {
      trigger_error('could not find a gallery to remove', E_USER_NOTICE);
      return false;
    }
    @unlink(THUMB_CACHE.INDEX_THUMB_PREFIX.$this->galleriesData[$key]['galleryRef'].'.jpg');
    if (!$this->rChmod($this->galleriesData[$key]['path'], DIR_MODE, FILE_MODE))
    {
      trigger_error('could not change folder and file permissions', E_USER_NOTICE);
    }
    unset($this->galleriesData[$key]);
    $this->saveGalleriesData();
    return true;
  }

 /**
   * Recursive chmod
   *
   * @access private
   * @return boolean success
   * @param string directory name. Also works on files but there are easier ways to chmod a file
   * @param integer mode to set directories
   * @param integer mode to set files
   */
  function rChmod($start, $dirMode, $fileMode)
  {
    $start = rtrim($start, '\\/');
    if(!file_exists($start)) return false;
    if (!is_dir($start)) return @chmod($start, $fileMode);
    elseif(is_readable($start))
    {
      $handle = opendir($start);
      while (false !== ($item = readdir($handle)))
      {
        if($item[0] == '.') continue;
        $path = $start.DIRECTORY_SEPARATOR.$item;
        if(is_dir($path)) 
        {
          $this->rChmod($path, $dirMode, $fileMode);
        }
        else
        {
          @chmod($path, $fileMode);
        }
      }
      closedir($handle);  
    }
    clearstatcache();
    return @chmod($start, $dirMode);
  }
  
 /**
  * Public interface to sortGalleries function
  *
  * @access public
  * @return boolean success
  * @param string sort column ['galleryTitle' | 'galleryCTime' | 'galleryType']
  */
  function setGallerySortOrder($sortColumn)
  {
    if ($sortColumn != 'reSort')
    {
      if ($sortColumn == $this->galleriesPrefs['sortColumn'])
      {
        $this->galleriesPrefs['sortAscending'] = !$this->galleriesPrefs['sortAscending'];
      }
      else
      {
        $this->galleriesPrefs['sortAscending'] = true;
      }
      $this->galleriesPrefs['sortColumn'] = $sortColumn;
    }
    $this->galleriesData = $this->sortGalleries($this->galleriesData, $this->galleriesPrefs);
    $this->saveGalleriesPrefs();
    $this->saveGalleriesData();
    return true;
  }

  
 /**
  * Sort galleries. Note that sort by age is included but not currently in use
  *
  * @access private
  * @return array sorted image data;
  * @param array to sort
  * @param array galleries preferences
  * @param string sort column ['galleryTitle' | 'galleryCTime' | 'galleryType']
  */
  function sortGalleries($galleriesData, $galleriesPrefs)
  {
    $galleryRef = array();
    $galleryTitle = array();
    $galleryCTime = array();
    $galleryType = array();
    $sortOrder = $galleriesPrefs['sortAscending'] ? SORT_ASC : SORT_DESC;
    foreach ($galleriesData as $key => $row)
    {
      if(!is_dir(rtrim($row['path'], '\\/')))
      {
        trigger_error('cannot find gallery No. '.$row['galleryRef'].' folder '.rtrim($row['path'], '\\/'), E_USER_NOTICE);
        $galleryRef[$key] = $row['galleryRef'];
        $galleryTitle[$key]  = $row['title'];
        $galleryType[$key] = $row['galleryType'];
        $galleryCTime[$key] = 0;
        continue;
      }
      $galleryRef[$key] = $row['galleryRef'];
      $galleryTitle[$key]  = $row['title'];
      $galleryType[$key] = $row['galleryType'];
      $galleryCTime[$key] = filectime(rtrim($row['path'], '\\/'));
    }
    switch($galleriesPrefs['sortColumn'])
    {
      case 'galleryRef' :
        array_multisort($galleryRef, $sortOrder, $galleriesData);
        break;
      case 'galleryTitle' :
        array_multisort($galleryTitle, $sortOrder, $galleriesData);
      break;
      case 'galleryType' :
        array_multisort($galleryType, $sortOrder, $galleriesData);
      break;
      case 'galleryCTime' :
        array_multisort($galleryCTime, $sortOrder, SORT_NUMERIC, $galleriesData);
      break;
    }
    return $galleriesData;
  }
}

?>
