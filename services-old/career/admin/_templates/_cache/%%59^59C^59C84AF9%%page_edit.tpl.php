<?php /* Smarty version 2.6.26, created on 2010-05-13 11:43:52
         compiled from page_edit.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		
				<div id="sidebar">
					<div class="sidebar_page_edit">
						<h3>Links</h3>
						<p>Make a selection in the editor or select an existing link and click on one of the links bellow.</p>
						<dl id="links_list">
							<dt><strong>Internal</strong></dt>
							<dd><a href="#" rel="<?php echo $this->_tpl_vars['BASE_URL']; ?>
post/">Post a job</a></dd>
							<dd><a href="#" rel="<?php echo $this->_tpl_vars['BASE_URL']; ?>
rss/">RSS Feed</a></dd>
							<dd><a href="#" rel="<?php echo $this->_tpl_vars['BASE_URL']; ?>
<?php echo $this->_tpl_vars['URL_COMPANIES']; ?>
/">Companies</a></dd>
							<dt><strong>Categories</strong></dt>
							<?php unset($this->_sections['tmp']);
$this->_sections['tmp']['name'] = 'tmp';
$this->_sections['tmp']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['tmp']['show'] = true;
$this->_sections['tmp']['max'] = $this->_sections['tmp']['loop'];
$this->_sections['tmp']['step'] = 1;
$this->_sections['tmp']['start'] = $this->_sections['tmp']['step'] > 0 ? 0 : $this->_sections['tmp']['loop']-1;
if ($this->_sections['tmp']['show']) {
    $this->_sections['tmp']['total'] = $this->_sections['tmp']['loop'];
    if ($this->_sections['tmp']['total'] == 0)
        $this->_sections['tmp']['show'] = false;
} else
    $this->_sections['tmp']['total'] = 0;
if ($this->_sections['tmp']['show']):

            for ($this->_sections['tmp']['index'] = $this->_sections['tmp']['start'], $this->_sections['tmp']['iteration'] = 1;
                 $this->_sections['tmp']['iteration'] <= $this->_sections['tmp']['total'];
                 $this->_sections['tmp']['index'] += $this->_sections['tmp']['step'], $this->_sections['tmp']['iteration']++):
$this->_sections['tmp']['rownum'] = $this->_sections['tmp']['iteration'];
$this->_sections['tmp']['index_prev'] = $this->_sections['tmp']['index'] - $this->_sections['tmp']['step'];
$this->_sections['tmp']['index_next'] = $this->_sections['tmp']['index'] + $this->_sections['tmp']['step'];
$this->_sections['tmp']['first']      = ($this->_sections['tmp']['iteration'] == 1);
$this->_sections['tmp']['last']       = ($this->_sections['tmp']['iteration'] == $this->_sections['tmp']['total']);
?>
							<dd><a href="#" rel="<?php echo $this->_tpl_vars['BASE_URL']; ?>
<?php echo $this->_tpl_vars['URL_JOBS']; ?>
/<?php echo $this->_tpl_vars['categories'][$this->_sections['tmp']['index']]['var_name']; ?>
/"><?php echo $this->_tpl_vars['categories'][$this->_sections['tmp']['index']]['name']; ?>
</a></dd>
							<?php endfor; endif; ?>
							<dt><strong>Pages</strong></dt>
							<?php $_from = $this->_tpl_vars['pages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['page']):
?>
							<dd><a href="#" rel="<?php echo $this->_tpl_vars['BASE_URL']; ?>
<?php echo $this->_tpl_vars['URL_JOBS']; ?>
/<?php echo $this->_tpl_vars['page']['url']; ?>
/"><?php echo $this->_tpl_vars['page']['title']; ?>
</a></dd>
							<?php endforeach; endif; unset($_from); ?>
						</dl>
					</div>
				</div>
		<div id="content">
				<h2>Pages</h2>
				<?php if ($this->_tpl_vars['current_page']): ?>
				<h3>Editing page &quot;<?php echo $this->_tpl_vars['current_page']['title']; ?>
&quot;</h3>
				<?php else: ?>
				<h3>Add new page</h3>
				<?php endif; ?>
				<form id="publish_form" action="<?php echo $_SERVER['REQUEST_URI']; ?>
" method="post">
					<p>
						<button type="submit" class="submit_button"><?php if ($this->_tpl_vars['current_page']): ?>Modify page<?php else: ?>Add page<?php endif; ?></button>
					</p>
					<fieldset>
						<legend>SEO</legend>
						<table cellspacing="2" cellpadding="2" border="0">
							<tr>
								<td colspan="2"><label for="page_url">URL</label></td>
							</tr>
							<tr>
								<td colspan="2">
									<?php if ($this->_tpl_vars['errors']['page_url']): ?><em class="form_error"><?php echo $this->_tpl_vars['errors']['page_url']; ?>
</em><?php endif; ?>
									<input type="text" name="page_url" id="page_url" class="text_field" value="<?php echo $this->_tpl_vars['defaults']['page_url']; ?>
" />
								</td>
							</tr>
							<tr>
								<td colspan="2"><label for="page_page_title">Page title</label></td>
							</tr>
							<tr>
								<td colspan="2">
									<?php if ($this->_tpl_vars['errors']['page_page_title']): ?><em class="form_error"><?php echo $this->_tpl_vars['errors']['page_page_title']; ?>
</em><?php endif; ?>
									<input type="text" name="page_page_title" id="page_page_title" class="text_field" value="<?php echo $this->_tpl_vars['defaults']['page_page_title']; ?>
" />
								</td>
							</tr>
							<tr>
								<td width="50%"><label for="page_keywords">Keywords</label></td>
								<td width="50%"><label for="page_description">Description</label></td>
							</tr>
							<tr>
								<td width="50%"><textarea id="page_keywords" name="page_keywords" class="textarea_field_small" rows="6"><?php echo $this->_tpl_vars['defaults']['page_keywords']; ?>
</textarea></td>
								<td width="50%"><textarea id="page_description" name="page_description" class="textarea_field_small" rows="6"><?php echo $this->_tpl_vars['defaults']['page_description']; ?>
</textarea></td>
							</tr>
						</table>
					</fieldset>
					<p>
						<button type="submit" class="submit_button"><?php if ($this->_tpl_vars['current_page']): ?>Modify page<?php else: ?>Add page<?php endif; ?></button>
					</p>
					<fieldset>
						<legend>Content</legend>
						<table cellspacing="2" cellpadding="2" border="0">
							<tr>
								<td colspan="2"><label for="page_title">Title</label></td>
							</tr>
							<tr>
								<td colspan="2">
									<?php if ($this->_tpl_vars['errors']['page_title']): ?><em class="form_error"><?php echo $this->_tpl_vars['errors']['page_title']; ?>
</em><?php endif; ?>
									<input type="text" name="page_title" id="page_title" class="text_field" value="<?php echo $this->_tpl_vars['defaults']['page_title']; ?>
" />
								</td>
							</tr>
							<tr>
								<td colspan="2"><label for="page_content">Content</label></td>
							</tr>
							<tr>
								<td colspan="2"><textarea id="page_content" name="page_content" class="textarea_field mceEditor" rows="40"><?php echo $this->_tpl_vars['defaults']['page_content']; ?>
</textarea></td>
							</tr>
							<tr>
								<td colspan="2"><label><input type="checkbox" id="page_has_form" name="page_has_form" value="1"<?php if ($this->_tpl_vars['defaults']['page_has_form'] == '1'): ?> checked="checked"<?php endif; ?> /> Has contact form?</label></td>
							</tr>
							<tr<?php if ($this->_tpl_vars['defaults']['page_has_form'] != '1'): ?> class="hidden"<?php endif; ?>>
								<td colspan="2"><label for="page_form_message">Form message</label></td>
							</tr>
							<tr<?php if ($this->_tpl_vars['defaults']['page_has_form'] != '1'): ?> class="hidden"<?php endif; ?>>
								<td colspan="2"><textarea id="page_form_message" name="page_form_message" class="textarea_field mceEditor" rows="20"><?php echo $this->_tpl_vars['defaults']['page_form_message']; ?>
</textarea></td>
							</tr>
						</table>
					</fieldset>
					<p>
						<button type="submit" class="submit_button"><?php if ($this->_tpl_vars['current_page']): ?>Modify page<?php else: ?>Add page<?php endif; ?></button>
					</p>
				</form>
		</div><!-- #content -->
<?php echo '
	<script type="text/javascript">
		jobberBase.editor();
	</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>