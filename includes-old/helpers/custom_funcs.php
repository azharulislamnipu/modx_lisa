<?php


/**
 * Test input data
 * @param $data
 * @return string
 */
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/**
 * @param $helper
 * @param $current
 * @param $echo
 * @param $type
 * @return string
 */
function __checked_selected_helper( $helper, $current, $echo, $type ) {
    if ( (string) $helper === (string) $current )
        $result = " $type='$type'";
    else
        $result = '';

    if ( $echo )
        echo $result;

    return $result;
}

function selected( $selected, $current = true, $echo = true ) {
    return __checked_selected_helper( $selected, $current, $echo, 'selected' );
}

function checked( $checked, $current = true, $echo = true ) {
    return __checked_selected_helper( $checked, $current, $echo, 'checked' );
}

if (!function_exists('array_column')) {
    function array_column($input, $column_key, $index_key = null) {
        $arr = array_map(function($d) use ($column_key, $index_key) {
            if (!isset($d[$column_key])) {
                return null;
            }
            if ($index_key !== null) {
                return array($d[$index_key] => $d[$column_key]);
            }
            return $d[$column_key];
        }, $input);

        if ($index_key !== null) {
            $tmp = array();
            foreach ($arr as $ar) {
                $tmp[key($ar)] = current($ar);
            }
            $arr = $tmp;
        }
        return $arr;
    }
}

function test_in_array(&$value, $key){
    if(is_array($value)){
        $value = join(',', $value);
    }
    $value = test_input($value);
}


/**
 * Combine two arrays and keep all values
 * @param $keys
 * @param $values
 * @return array
 */
function array_combine_($keys, $values) {
    $result = array();
    foreach ($keys as $i => $k) {
        $result[$k][] = $values[$i];
    }
    array_walk($result, create_function('&$v', '$v = (count($v) == 1)? array_pop($v): $v;'));
    return $result;
}


/*Get Ip address*/
function getip(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = test_input($_SERVER['HTTP_CLIENT_IP']);
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = test_input($_SERVER['HTTP_X_FORWARDED_FOR']);
    } else {
        $ip = test_input($_SERVER['REMOTE_ADDR']);
        if ($ip == '::1') {
            $ip = '127.0.0.1';
        }
    }
    return $ip;
}

function create_suffix_prefix_from_array($arr){
    ob_start();
    foreach ($arr as $item){
        echo "<option value='$item'>$item</option>";
    }
    return ob_get_clean();
}

function is_ajax(){
	return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
}

function tr_for_search_result($agu, $position){
	return <<<DDD
<tr data-customerid="{$agu['CUSTOMERID']}" data-updatedBy="[[svVars? &var=`webInternalKey`]]" data-updaterEmail="[[svVars? &var=`webEmail`]]" data-webId="{$agu['WEB_ID']}">
	<td>
		<div id="{$agu['WEB_ID']}" class="balloonstyle">
			<span style="display: none;">{$agu['LASTNAME']}</span>
			<strong>{$agu['PREFIX']} {$agu['L2R_FULLNAME']}</strong><br />
			{$agu['EMAIL']}<br />
			{$agu['HOMEPHONE']}, {$agu['MOBILEPHONE']}
		</div>
	</td>
	<td>{$agu['STATUS']}</td>
	<td>{$position}</td>
	<td class="action_td">
		<a href="/services/spp-directory/search-results-json.php?id={$agu['CUSTOMERID']}" class="view" id="view_{$agu['WEB_ID']}">View</a>,
		<a href="#" class="edit" id="edit_{$agu['CUSTOMERID']}">Edit</a>
		<a href="/services/grammateus/member/status_request.php?CUSTOMERID={$agu['CUSTOMERID']}&amp;UPDATEDBY=[[svVars? &var=`webInternalKey`]]&amp;DM_EMAIL=[[svVars? &var=`webEmail`]]&amp;KT_back=1">Request Status Change</a>
	</td>
</tr>
DDD;
}

function get_tel_link($phone){
	return "<a href='tel:{$phone}'>{$phone}</a>";
}

function tr_for_search_result2($agu, $position){
	$mobile = isset($agu['MOBILEPHONE']) ? ',' . get_tel_link($agu['MOBILEPHONE']) : '';
	$home_phone = get_tel_link($agu['HOMEPHONE']);
	return <<<DDD
<tr data-customerid="{$agu['CUSTOMERID']}" data-updatedBy="[[svVars? &var=`webInternalKey`]]" data-updaterEmail="[[svVars? &var=`webEmail`]]" data-webId="{$agu['WEB_ID']}">
	<td>	
		<div id="{$agu['WEB_ID']}" class="balloonstyle">
			<span style="display: none;">{$agu['LASTNAME']}</span>
			<strong>{$agu['PREFIX']} {$agu['L2R_FULLNAME']}</strong><br />
			{$agu['BOULENAME']}<br />
			<a href="mailto:{$agu['EMAIL']}" target="_top">{$agu['EMAIL']}</a><br />
			{$home_phone} {$mobile}
		</div>
	</td>
	<td>{$position}</td>
	<td class="action_td">
		<a href="/services/spp-directory/search-results-noedit-json.php?id={$agu['CUSTOMERID']}" class="view" id="view_{$agu['WEB_ID']}">View</a>
	</td>
</tr>
DDD;
}
//SEARCH SQL

function keyPars($sql, $values, $fields)
{
//$fields = Array('FIRSTNAME','LASTNAME');
	if (strlen($sql) > 0) {
		$sql .= ' AND ';
	}
	$vals = null;
	if (is_array($values)) {
		$vals = $values;
	} else {
		$vals = Array();
		$vals[0] = $values;
	}
	$insql = '';
	$insql2 = '';
	foreach ($vals as $val) {
		$insql2 = '';
		foreach ($fields as $fld) {
			if (strlen($insql2) > 0) {
				$insql2 .= ' OR ';
			}
			$insql2 .= $fld . ' LIKE "%' . $val . '%"';
		}
		if (strlen($insql) > 0) {
			$insql .= ' AND ';
		}
		$insql .= '(' . $insql2 . ')';
	}
	$sql .= '(' . $insql . ')';
	return $sql;
}

function testParm($in, $fld)
{
	if (isset($_GET[$in])) {
		$val = $_GET[$in];
		if (is_array($val)) {
			return true;
		} else {
			$val = $val . '';
			if (strlen($val) > 0) {
				return true;
			}
		}
	}
	return false;
}

function addSearchPar($sql, $in, $field, $like)
{
	if (!testParm($in, $field)) {
		return $sql;
	}
	global $searchpars_out;
	$searchpars_out .= ucfirst($in) . ' = ';
	$values = $_GET[$in];
	if (strlen($sql) > 0) {
		$sql .= ' AND ';
	}
	$vals = null;
	if (is_array($values)) {
		$vals = $values;
	} else {
		$vals = Array();
		$vals[0] = $values;
	}
	$insql = '';
	foreach ($vals as $val) {
		$searchpars_out .= $val . ', ';
		if (strlen($insql) > 0) {
			$insql .= ' OR ';
		}
		if ($like) {
			$insql .= $field . ' LIKE "%' . $val . '%"';
		} else {
			$insql .= $field . ' = "' . $val . '"';
		}
	}
	$sql .= '(' . $insql . ')';
//	$sql .= ' ' . $insql . ' ';
	if ($field == 'OFFICERID') {
		$sql .= ' AND COMMITTEESTATUSSTT = "Active"';
	}
//    $searchpars_out .= ', ';
	return $sql;
}

function urlWithParam($par, $value)
{
	$gets = '';
	$found = false;
	foreach ($_GET as $c => $v) {
		//echo "LOOOOK" . $c. " = > ".$v."<br>";
		if (strlen($gets) > 0) {
			$gets .= '&';
		}
		if ($c == $par) {
			$gets .= $c . '=' . $value;
			$found = true;
		} else {
			if (is_array($v)) {
				$nv = "";
				foreach ($v as $vv) {
					if (strlen($nv) > 0) {
						$nv .= "&";
					}
					$nv .= $c . '[]=' . $vv;
				}
				$gets .= $nv;
			} else {
				$gets .= $c . '=' . $v;
			}
		}
	}
	if (!$found) {
		if (strlen($gets) > 0) {
			$gets .= '&';
		}
		$gets .= $par . '=' . $value;
	}
	//echo 'http://'.getenv("HTTP_HOST").'/services/directory/search_advanced3.php?'.$gets;
	echo '?' . $gets;
}

function changePage($p, $current, $total)
{
	$fp = $current + $p;
	urlWithParam('currentpage', $fp);
}

function changeOrder($value, $curr, $dir)
{
	if ($curr == $value) {
		if ($dir == 'ASC') {
			urlWithParam('sortdir', 'DESC');
		} else {
			urlWithParam('sortdir', 'ASC');
		}
	} else {
		urlWithParam('sort', $value);
	}
}

function makeImageLink($img)
{
	if (isset($img)) {
		echo '<a href="http://' . getenv("HTTP_HOST") . '/home/assets/images/archons/' . $img . '">link for image</a>';
	}
}

function create_search_query_from_all($keys, $table, $db) {
	$vals = explode(' ', $keys);
	$fieldssql = 'DESC ' . $table;
	$res = $db->rawQuery($fieldssql);
	$fields = Array();
	foreach ($res as $row) {
		array_push($fields, $row['Field']);
	}
	return keyPars('', $vals, $fields);
//	$searchsql = keyPars($searchsql, $vals, $fields);
}

function create_custom_search_query($searchsql){

}

function checkExistSirArchon ($data){

    $demo = false;

    foreach ( $rslt as $data) {


        if ($demo = true && $data == "Sire Archon") {
            continue;
        }

        if($data == "Sire Archon"){
            $demo = true;
        }


    }
    return $demo;

}