<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Models a PostcardViewer gallery
 *
 * @package svManager
 */
class PvGallery extends Gallery
{  
 /**
  * @var array gallery settings to be stored in preferences file
  */
  var $storedInPreferences = array(
  'sortOrder' => '',
  'galleryTitle' => '',
  'htmlTitle' => '',
  'backgroundColor' => '',
  'maxImageWidth' => 0,
  'maxImageHeight' => 0,
  'langOpenImage' => '',
  'langAbout' => '',
  'urlType'=>'');
  
 /**
  * @var array gallery settings from xml file and preferences
  * 'cellDimension' integer 800 cell size
  * 'columns' integer 4 number of columns
  * 'zoomOutPerc integer 15 zoom-out percentage
  * 'zoomInPerc integer 100 zoom-in percentage
  * 'frameColor' string 0xFFFFFF color of gallery frame
  * 'captionColor' string 0xFFFFFF caption color
  * 'frameWidth' integer 10 frame width in px
  * 'enableRightClickOpen' string true/false display right-click menu option.
  * 'sortOrder' string image sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  * 'galleryTitle' string gallery title including permitted html
  * 'htmlTitle' string gallery title or preset
  * 'backgroundColor' string 0x181818 gallery background color
  * 'maxImageWidth' integer 640
  * 'maxImageHeight' integer 480
  * 'langOpenImage' string, "Open Image in New Window", language option
  * 'langAbout', string, "About", language option 
  */
  var $preferences = array();
  
 /**
  * @var string name of image class
  */
  var $imageClass = 'PvImage';
  
 /**
  * @var array of image objects
  */
  var $imageObjects = array();
  
 /**
  * @var string default path to master gallery
  */
  var $masterPath = PV_MASTER_PATH;
  
 /**
  * @var string to prepend to paths
  */
  var $pathScheme = '';
  
 /**
  * @var integer count of files in master gallery
  */
  var $masterFileCount = PV_MASTER_FILECOUNT;
  
 /**
  * @var string default image path
  */
  var $imageDirPathRelGallery = PV_DEFAULT_IMAGE_PATH;
  
 /**
  * @var string default thumb path
  */
  var $thumbDirPathRelGallery = PV_DEFAULT_THUMB_PATH;
  
 /**
  * @var integer thumb width
  */ 
  var $thumbWidth = PV_THUMB_WIDTH;

 /**
  * @var integer thumb height
  */
  var $thumbHeight = PV_THUMB_HEIGHT;
  
 /**
  * @var integer thumb quality
  */
  var $thumbQuality = PV_THUMB_QUALITY;
  
 /**
  * @var string name of viewer swf file
  */
  var $viewerSwf = PV_DEFAULT_SWF;
  
 /**
  * @var string acceptable html tags in captions
  */
  var $okTags = '<a><u><font><br><br />';
  
 /**
  * Constructs PvGallery
  *
  * Attempts to read local copies of preferences.txt and gallery.xml
  * Parses gallery.xml file into class settings and imageObjects array.
  * Note that the parsing translates html entities (e.g. in the title) back to html
  * If local file has been corrupted then default values are used.
  * 
  * @access public
  * @param string gallery reference (permanent reference, not the  galleriesData index)
  * @param string gallery title, empty string gives default
  * @param string gallery path
  * @param boolean new gallery
  *
  */  
  function PvGallery($ref, $path, $newGallery = false)
  {
    parent::Gallery();
    $this->preferences['frameColor'] = PV_FRAME_COLOR;
    $this->preferences['captionColor'] = PV_CAPTION_COLOR;
    $this->preferences['frameWidth'] = PV_FRAME_WIDTH;
    $this->preferences['cellDimension'] = PV_CELL_DIMENSION;
    $this->preferences['columns'] = PV_COLUMNS;
    $this->preferences['zoomInPerc'] = PV_ZOOM_IN_PERC;
    $this->preferences['zoomOutPerc'] = PV_ZOOM_OUT_PERC;
    $this->preferences['enableRightClickOpen'] = PV_ENABLE_RIGHT_CLICK_OPEN;
    $this->preferences['backgroundColor'] = PV_BACKGROUND_COLOR;
    $this->preferences['maxImageWidth'] = PV_MAX_IMAGE_WIDTH;
    $this->preferences['maxImageHeight'] = PV_MAX_IMAGE_HEIGHT;
    $this->preferences['sortOrder'] = PV_SORT_ORDER;
    $this->preferences['galleryTitle'] = PV_GALLERY_TITLE;
    $this->preferences['htmlTitle'] = HTML_TITLE;
    $this->preferences['langOpenImage'] = PV_LANG_OPEN_IMAGE;
    $this->preferences['langAbout'] = PV_LANG_ABOUT;
    $this->preferences['urlType'] = PV_URL_TYPE;
    $this->galleryRef = $ref;
    $this->galleryPathRelSvm = $path;
    $this->pathScheme = ($this->pathScheme == '') ? '' : rtrim($this->pathScheme, '\\/').'/';
    $this->imageDirPathRelGallery = rtrim($this->imageDirPathRelGallery, '\\/').DIRECTORY_SEPARATOR;
    $this->thumbDirPathRelGallery = rtrim($this->thumbDirPathRelGallery, '\\/').DIRECTORY_SEPARATOR;
    
    if ($newGallery)
    {
      $this->addGallery($ref);
    }
    if (!file_exists($this->trimSeparator($this->galleryPathRelSvm)))
    {
      // exit
      trigger_error('cannot find gallery folder '.$this->galleryPathRelSvm, E_USER_ERROR);
    }
    $prefs = $this->readPreferences();
    if ($prefs !== false)
    {
      $this->preferences = array_merge($this->preferences, $prefs);
    }
    $this->setPathScheme();
    $xmlStruct = $this->parseXml($this->getXmlPath());
    if ($xmlStruct !== false)
    {
      $att = $this->xmlParser->parseAttributes($xmlStruct, PV_XML_SETTINGS_TAG);
      if ($att !== false)
      {
        $this->preferences = array_merge($this->preferences, $att);
      }
      $this->imageObjects = $this->parseImageTags($xmlStruct);
    }
    $this->preferences['htmlTitle'] = (USE_HTML_TITLE === false) ? strip_tags($this->htmlEntityDecode->decode($this->preferences['galleryTitle'])) : HTML_TITLE;
    $this->saveGallery();
  }
  
 /** 
   * Get stored image data from class
   *
   * @access public
   * @return array
   */
   function getImageObjects()
   {
     return $this->imageObjects;
   }

 /**
  * Get xml path
  *
  * @access private
  * @return string calculated xml path
  */
  function getXmlPath()
  {
    return str_replace('/', DIRECTORY_SEPARATOR, $this->pathParser->fix($this->galleryPathRelSvm.PV_XML_PATH_REL_GALLERY));
  } 
  
 /**
  * Get gallery title
  *
  * @access public
  * @return string
  */
  function getGalleryTitle()
  {
    return $this->preferences['galleryTitle'];
  }
  
 /**
  * Set gallery title
  * @access public
  * @return void
  * @param string
  */
  function setGalleryTitle($title)
  {
    $this->preferences['galleryTitle'] = $title;
    return;
  }
  
 /**
  * Get gallery title stripped of html
  *
  * @access public
  * @return string
  */
  function getGalleryTitleText()
  {
    return strip_tags($this->htmlEntityDecode->decode($this->preferences['galleryTitle']));
  }
  
 /**
  * Get sort order
  *
  * @access public
  * @return string
  */
  function getSortOrder()
  {
    return $this->preferences['sortOrder'];
  }
  
 /**
  * Set sort order in $this->settings
  *
  * @access public
  * @return void
  * @param string sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  */
  function setSortOrder($sortOrder)
  {
    $this->preferences['sortOrder'] = $sortOrder;
    return;
  }

  /**
   * Set captions in image data
   *
   * @access public
   * @return  boolean true on success
   * @param array captions
   */
  function setImageCaptions($post)
  {
    $captions = $post['cap'];
    $i=0;
    foreach (array_keys($this->imageObjects) as $key)
    {
      $this->imageObjects[$key]->setCaption($captions[$key], $i, $this->okTags);
      $i++;
    }
    return true;
  }

 /**
  * Extract file names and captions from xml structured array
  *
  * Any empty image tags are silently ignored
  * @access private
  * @return array containing filenames and captions
  * @param values array as generated by xml_parse_into_struct
  */
  function parseImageTags($xmlStruct)
  {
    $vals = $xmlStruct['vals'];
    $imageObjects = array();
    $imageTagOpen = false;
    $imageUrl = '';
    $imageCaption = '';
    foreach ($vals as $tagInfo)
    {
      $imageTagOpen = ((strtolower($tagInfo['tag']) == 'image' && $tagInfo['type'] == 'open') || $imageTagOpen);
      if (strtolower($tagInfo['tag']) == 'url')
      {
        if (isset($tagInfo['value']))
        {
          $imageUrl = $tagInfo['value'];
          $imagePathRelGallery = $this->getPathRelGallery($imageUrl);
          $fileName = basename($imagePathRelGallery);
        }
        else continue;
      }
      if (strtolower($tagInfo['tag']) == 'caption')
      {
        if (isset($tagInfo['value']))
        {
          $imageCaption = strip_tags($this->htmlEntityDecode->decode($tagInfo['value']), $this->okTags);
        }
        else $imageCaption = PV_IMAGE_CAPTION;
      }
      if ($imageTagOpen && strtolower($tagInfo['tag']) == 'image' && $tagInfo['type'] == 'close')
      {
        {
          $imageSize = @getimagesize($this->galleryPathRelSvm.$imagePathRelGallery);
          if ($imageSize != false)
          {
            $imageObjects[] = new $this->imageClass($this->galleryPathRelSvm, $imagePathRelGallery, $this->thumbDirPathRelGallery.$fileName, $imageCaption);
          }
          $photoTagOpen = false;
          $imageUrl = '';
          $imageCaption = '';
        }
      }
    }
    return $imageObjects;
  }

 /**
  * Construct xml string and write to file
  * htmlspecialchars is used in case any xml reserved characters sneak into attributes
  * The photo url is built afresh from the paths and the file name
  *
  * @access private
  * @param string file path
  * @return boolean
  */
  function writeXml($xmlPath)
  {
    $imageObjects = $this->imageObjects;
    $attributes = array_diff_key($this->preferences, $this->storedInPreferences);
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>
  <'.PV_XML_SETTINGS_TAG;
    foreach ($attributes as $name => $value)
    {
       $xml .= ' '.$name.' = "'.htmlspecialchars($value, ENT_QUOTES, 'UTF-8').'"';
    }
    $xml .= '>';
    foreach ($imageObjects as $key=>$imageObject)
    {
      $imageUrl = $this->convertRelToScheme($imageObject->getImagePathRelGallery(), $this->galleryPathRelSvm, $this->pathScheme);
      $imageUrl = htmlspecialchars($imageUrl, ENT_QUOTES, 'UTF-8');
      $imageCaption = $imageObject->getCaption();
      $xml .= ' 
    <image>
     <url>'.$imageUrl.'</url>
     <caption><![CDATA['.$imageCaption.']]></caption>
    </image>';
    }
    $xml .= '
  </'.PV_XML_SETTINGS_TAG.'>
';
    
    if(@!file_put_contents($xmlPath, $xml, FPC_LOCK))
    {
      trigger_error('unable to write xml to file '.$xmlPath, E_USER_NOTICE);
      return false;
    }
    return true; 
  }

 /** Clean data from customize form and update class properties
  *
  * @access public
  * @return array
  * @param array as in preferences file
  */
  function customize($newSettings)
  {
    $newSettings = array_map('trim', $newSettings);
    $this->preferences['backgroundColor'] = $this->cleanHex($newSettings['backgroundColor'], 6);    
    $this->preferences['maxImageWidth'] = intval(max(0, $newSettings['maxImageWidth']));
    $this->preferences['maxImageHeight'] = intval(max(0, $newSettings['maxImageHeight']));
    $this->preferences['galleryTitle'] = strip_tags($newSettings['galleryTitle']);
    $this->preferences['htmlTitle'] = (USE_HTML_TITLE === false) ? $this->preferences['galleryTitle'] : HTML_TITLE;
    $this->preferences['cellDimension'] = intval(max(0, $newSettings['cellDimension']));
    $this->preferences['frameWidth'] = intval(max(0, $newSettings['frameWidth']));
    $this->preferences['enableRightClickOpen'] = (isset($newSettings['enableRightClickOpen'])) ? 'true' : 'false';
    $this->preferences['frameColor'] = $this->cleanHex($newSettings['frameColor'], 6);
    $this->preferences['captionColor'] = $this->cleanHex($newSettings['captionColor'], 6);
    $this->preferences['frameWidth'] = intval(max(0, $newSettings['frameWidth']));
    $this->preferences['columns'] = intval(max(0, $newSettings['columns']));
    $this->preferences['zoomInPerc'] = intval(max(0, $newSettings['zoomInPerc']));
    $this->preferences['zoomOutPerc'] = intval(max(0, $newSettings['zoomOutPerc']));
    $this->preferences['urlType'] = $newSettings['urlType'];
    return true;
  }
}


?>