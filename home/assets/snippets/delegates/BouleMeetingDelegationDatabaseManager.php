<?php

/**
 * Handles the database interactions for the Boule regional delegate administration 
 * @author Pat Heard {http://fullahead.org}
 */

require_once 'BouleMeetingBean.php';
require_once '/opt/lampp/htdocs/modx_lisa/home/index.php';
define('MODX_API_MODE', true); // Allows this class to use the MODx API

class BouleMeetingDelegationDatabaseManager {
        
    private $modx = null;

    /** Initialize the database manager */
    public function __construct(&$modx = null){
        if($modx == null){
            die('You must pass a valid $modx object reference to the BouleRegionalDelegationDatabaseManager.');
        }
        $this->modx = $modx; 
    } 
  
    /** Loads all boule delegations for a given meeting */
    public function getBoulesForMeeting($meeting_id = -1){
        
        $boules = array(); 
        $where = $meeting_id > -1 ? 'WHERE meeting_id = '.$meeting_id : '';
        
        // Get the delegation counts for the meeting
        $results = $this->modx->db->query('
            SELECT 
                *
            FROM 
                vw_delegation_matrix
            '.$where . '
            ORDER BY
                BOULENAME
        ');

        // Got results?  Load up the beans
        if($this->modx->db->getRecordCount($results) >= 1) {
            while($row = $this->modx->db->getRow($results)){
                $boules[] = new BouleMeetingBean(array(
                    'id'                        => $row['meeting_id'],
                    'code'                      => $row['BOULECD'],
                    'name'                      => $row['BOULENAME'], 
                    'region'                    => $row['REGIONNAME'], 
                    'location'                  => $row['LOCATION'], 
                    'count_archons'             => $row['CENSUS'], 
                    'count_delegates'           => $row['DEL_TOTAL'], 
                    'count_delegates_appointed' => $row['DEL_APPT'], 
                    'count_delegates_certified' => $row['CERTIFIED'], 
                    'count_alternates'          => $row['ALT_TOTAL'], 
                    'count_alternates_appointed'=> $row['ALT_APPT'] 
                ));                   
            }
        } 
             
        return $boules;
        
    }    
    
}

?>
