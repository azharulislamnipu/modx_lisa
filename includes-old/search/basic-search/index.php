<?php
$user_id = $_SESSION['webInternalKey'];
$user_email = $_SESSION['webEmail'];
if(isset($_GET['Submit'])) {
	$type = $_GET['Submit'];
	$type_text = '';
	switch ($type){
		case 'LASTNAME':
			$type_text = 'Last Name';
			break;
		case 'CITY':
			$type_text = 'City';
			break;
		case 'BOULENAME':
			$type_text = 'Boulé';
			break;
		case 'STATECD':
			$type_text = 'State';
			break;
	}
	?>
	<h3>
		Search Result for "<?php echo $type_text;?>" = "<?php echo $_GET[$type];?>"
	</h3>
	<div class="amu_table search-table" data-visitor="<?php echo $user_id; ?>"
	     data-dmemail="<?php echo $user_email; ?>">
		<table width="100%" cellpadding="0" cellspacing="0" id="archon_list_search_table" class="display">
			<thead>
			<tr>
				<td>ARCHON</td>
				<td>Office Title</td>
				<td>Action</td>
			</tr>
			</thead>
			<tbody>
			<?php// include strtolower($type) . '.php';?>
			<?php include 'search.php';?>
			</tbody>
		</table>
	</div>
<?php
}