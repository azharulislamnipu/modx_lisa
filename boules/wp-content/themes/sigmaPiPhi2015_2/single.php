<?php get_header() ?>

	<div id="content">
		<div class="padder width padding-top-none-20">
			<div class="width divider">

		<?php do_action( 'sigma_before_blog_single_post' ) ?>
  <div class="row grid-12">
		<div class="page" id="blog-single">
	   	 <div class="span-9 float-right">
	   		 <div class="inner">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="post" id="post-<?php the_ID(); ?>">

					<div class="post-content">

						<?php // Check that the user is allowed to view the content
							  $can_view_content = can_view_content();
							  $can_view_boule_only = can_view_boule_only(); ?>
						<?php if ( $can_view_content ) : ?>
							<?php echo the_post_thumbnail();?>
							<h2 class="posttitle"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( 'Permanent Link to', 'archon-network' ) ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
							<span class="icon archive"><?php echo get_the_date(); ?> | <?php echo "By: ".get_the_author(); ?> | <?php _e( 'Posted in', 'archon-network' ) ?> <?php the_category(', ') ?> </span> 
							<div class="contentBlock">
								<div class="entry">
									<?php the_content(); ?>

									<?php wp_link_pages(array('before' => __( '<p><strong>Pages:</strong> ', 'archon-network' ), 'after' => '</p>', 'next_or_number' => 'number')); ?>
								</div>
							</div>
							<div class="contentBlockFooter">
								
								
								<?php
									// Only Boule members can comment
									// if ( $can_view_boule_only ) {
// 										comments_popup_link( __( 'No Comments', 'archon-network' ), __( '1 Comment', 'archon-network' ), __( '% Comments', 'archon-network' ), 'icon comment' );
// 									}
								?>
							</div>

						<?php else: ?>
							<h2 class="posttitle"><?php _e( 'Unauthorized', 'archon-network' ); ?></h2>
							<div class="contentBlock">
								<div class="entry">
									<p><?php _e( 'The information on this page is available for members only. Please log in to view this content.', 'archon-network' ); ?></p>
								</div>
							</div>
						<?php endif; ?>
						
						

					</div><!-- post content -->

				</div><!-- post -->

			<?php if ( $can_view_boule_only ) : ?>
				<?php //comments_template(); ?>
			<?php endif; ?>

			<?php endwhile; else: ?>

				<p><?php _e( 'Sorry, no posts matched your criteria.', 'archon-network' ) ?></p>

			<?php endif; ?>
			
				</div><!--inner-->
			</div><!--span-9-->
			
			 <div class="span-3 float-left">
				 <div class="inner">
					 <?php get_sidebar(); ?>
				</div><!-- .inner -->
			</div><!-- .span-3 -->

		</div><!-- .page -->
		
		</div><!-- .grid-12 -->

		<?php do_action( 'sigma_after_blog_single_post' ) ?>
				</div><!-- .divider -->

		</div><!-- .padder -->
		
			<?php locate_template( array( 'sidebar.php' ), true ) ?>
		
	    <!-- 3 round images -->
	    <div class="row grid-12 margin-top-medium padding">
			<div class="width padding">
			<div class="round">
		
		<?php if ( is_active_sidebar( 'three_round_images' ) ) : ?>
		
			<?php dynamic_sidebar( 'three_round_images' ); ?>
		 
			<?php endif; ?>
			</div><!-- width padding-->
		</div><!-- #round -->
			</div><!-- #row grid 12 round images  -->
	</div><!-- #content -->


	
	

<?php get_footer() ?>