<?php

/**
 * Represents a user's info required by an Authorize.net transaction
 * Author: Pat Heard {http://fullahead.org}
 */
 
class AuthorizeNetECheckInfo {

	const CHECKING = 'CHECKING';
	const BUSINESSCHECKING = 'BUSINESSCHECKING';
	const SAVINGS = 'SAVINGS';
	
	const METHOD = 'ECHECK';
	const ECHECK_TYPE_WEB = 'WEB';
        const ECHECK_TYPE_CCD = 'CCD';

	private $x_amount = null;
	private $x_bank_aba_code = null;
	private $x_bank_acct_num = null;
	private $x_bank_acct_type = '';
	private $x_bank_name = '';
	private $x_bank_acct_name = '';
	private $x_recurring_billing = '';
        
        private $year = 0;
        private $officer = null;
	
	private $messages = array();
	
	/** Construction powers activate! */
    public function __construct(array $args = null){
        if(isset($args)){

            $args = array_merge(array(
                'year' => 0,
                'x_amount' => null,
                'x_bank_aba_code' => null,
                'x_bank_acct_num' => null,
                'x_bank_acct_type' => '',
                'x_bank_name' => '',
                'x_bank_acct_name' => '',
                'x_recurring_billing' => 'NO',
                'officer' => null
            ), $args);

            $this->year = $args['year'];
            $this->x_amount = $args['x_amount'];
            $this->x_bank_aba_code = $args['x_bank_aba_code'];
            $this->x_bank_acct_num = $args['x_bank_acct_num'];
            $this->x_bank_acct_type = $args['x_bank_acct_type'];
            $this->x_bank_name = $args['x_bank_name'];
            $this->x_bank_acct_name = $args['x_bank_acct_name'];
            $this->x_recurring_billing = $args['x_recurring_billing'];
            $this->officer = $args['officer'];
        }
    }	
	

	
	/** Returns the transaction fields for an Authorize.net payment */
	public function getTransactionFields(){
            return
                array(
                    // Payment info
                    'method'		=> AuthorizeNetECheckInfo::METHOD,
                    'echeck_type'	=> $this->getECheckType(),
                    'amount'		=> $this->x_amount,
                    'bank_aba_code'	=> $this->cleanNumber($this->x_bank_aba_code),
                    'bank_acct_num'	=> $this->cleanNumber($this->x_bank_acct_num),
                    'bank_acct_type'	=> $this->x_bank_acct_type,
                    'bank_name'		=> $this->cleanString($this->x_bank_name, 50),
                    'bank_acct_name'	=> $this->cleanString($this->x_bank_acct_name, 50),				
                    'recurring_billing' => $this->x_recurring_billing,
                    'delim_data'        => true,
                    'relay_response'	=> false,
                    
                    // Submitting Officer's info
                    'description'	=> $this->year . ' Dues Year',
                    'invoice_num'       => $this->cleanString($this->officer->chapterid . ' Roster', 20),
                    'first_name'        => $this->cleanString($this->officer->first_name, 50),
                    'last_name'         => $this->cleanString($this->officer->last_name, 50),
                    'address'           => $this->cleanString($this->officer->address1, 60),
                    'city' 		=> $this->cleanString($this->officer->city, 40),
                    'state'             => $this->cleanString($this->officer->state, 40),
                    'zip' 		=> $this->cleanString($this->officer->zip, 20),
                    'phone' 		=> $this->cleanString($this->officer->phone_home, 25),
                    'email' 		=> $this->cleanString($this->officer->email_home, 255),
                    'company'           => $this->cleanString($this->officer->officer_position . ', ' . $this->officer->chapterid, 50),
                    'cust_id'           => $this->cleanString($this->officer->chapterid, 20)

                );		
	}
	
	/** Makes sure the data is correct for a transaction */
	public function validate($is_transaction_validate = true){
		$msgs = array();		
                    
		// Amount
		if(empty($this->x_amount) || !is_numeric($this->x_amount) || intval($this->x_amount) <= 0){
			$this->messages[] = 'There was a problem calculating the payment amount. Please reload the page and try again.';
		}  

		// ABA Code
		if(empty($this->x_bank_aba_code) || !$this->isValidRoutingNumber($this->x_bank_aba_code)){
				$this->messages[] = 'You must enter a valid, 9 digit routing code.';
		} 

		// Account Number
		if(empty($this->x_bank_acct_num) || !$this->isValidDashedNumber($this->x_bank_acct_num, '{5,17}')){
				$this->messages[] = 'You must enter a valid bank acount number (up to 20 digits only).';
		}  		
		
		// Bank acount type
		if(empty($this->x_bank_acct_type)){
				$this->messages[] = 'You must select a bank account type.';
		} else {
			switch($this->x_bank_acct_type) {
				case AuthorizeNetECheckInfo::CHECKING:
				case AuthorizeNetECheckInfo::BUSINESSCHECKING:
				case AuthorizeNetECheckInfo::SAVINGS:
					break;
				default:
					$this->messages[] = 'You must select a valid bank account type (checking, business checking or savings)';
			}				
		} 	

		// Bank Name
		if(empty($this->x_bank_name)){
				$this->messages[] = 'You must enter the bank\'s name.';
		}  

		// Bank Account Name
		if(empty($this->x_bank_acct_name)){
				$this->messages[] = 'You must enter the name associated with the bank account.';
		} 	

		// Recurring billing
		if($this->x_recurring_billing !== 'YES' && $this->x_recurring_billing !== 'NO'){
				$this->messages[] = 'Recurring payment must be set to "YES" or "NO".';
		} 	
                
		// Officer Info
		if($this->officer === null){
				$this->messages[] = 'Officer info was not found.  Please reload the page and try again.';
		}                 
		
		return !$this->isMessages();
	}
	
	/** Are there messages */
	public function isMessages(){
		return !empty($this->messages);
	}	

	/** Clearout old messages */
	public function clearMessages(){
		$this->messages = array();
	}	    
	
        private function getECheckType() {
            return $this->x_bank_acct_type === AuthorizeNetECheckInfo::BUSINESSCHECKING ? AuthorizeNetECheckInfo::ECHECK_TYPE_CCD: AuthorizeNetECheckInfo::ECHECK_TYPE_WEB;
        }
        
	/** Validate number */
	private function isValidDashedNumber($dashedNumber='', $length){
                $number = preg_replace('/[\D]/', '', $dashedNumber);  // Remove dashes
		return preg_match('/^[0-9]'.$length.'$/', $number); // Check length
	}
        
        /** Validate routing number */
        private function isValidRoutingNumber($routingNumber = 0) {
            $routingNumber = preg_replace('/[\D]/', '', $routingNumber); //only digits
            if(strlen($routingNumber) != 9) {
                return false;   
            }

            $checkSum = 0;
            for ($i = 0, $j = strlen($routingNumber); $i < $j; $i+= 3 ) {
                $checkSum += ($routingNumber[$i] * 3);
                $checkSum += ($routingNumber[$i+1] * 7);
                $checkSum += ($routingNumber[$i+2]);
            }

            if($checkSum != 0 and ($checkSum % 10) == 0) {
                return true;
            } else { 
                return false;
            }
        }  
        
        private function cleanString($s, $length = false){
            $s = preg_replace('/[^\w\-\,\.\(\) @]/', '', $s);
            return $length !== false ? substr($s, 0, $length) : $s;
        }
        
        private function cleanNumber($n){   
            return preg_replace('/[\D]/', '', $n);
        }
	
        public function setAmount($amount){
            $this->x_amount = $amount;
        }
        
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
}

?>