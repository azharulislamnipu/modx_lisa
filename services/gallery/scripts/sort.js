/**
 * SimpleViewer admin package.
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
var snapshot = '';
var submitted = false;

// this is the prototype version of formSnapshot
Event.observe(window, 'load', formSnapshot);
Event.observe(window, 'beforeunload', confirmExit.bindAsEventListener());
Event.observe(window, 'load', function()
{
  var forms = $$('form.public');
  if (forms.length == 0) return;
  Event.observe(forms[0], 'submit', function()
  {
    submitted = true;
  });
});

Event.observe(window, 'load', function()
{
  if (!$("sortable") || !$("sortoutput")) return;
  Position.includeScrollOffsets = true;
  Sortable.create("sortable",
  {
    tag:'img',overlap:'horizontal',constraint:false, scroll: window, 
    onUpdate: function()
    {  
      $("sortoutput").value = Sortable.serialize("sortable", {name: "sortable", tag: "img"});
    }  
  });
});

function formSnapshot()
{
  var forms = $$('form.public');
  if (forms.length == 0) return;
  snapshot = Form.serialize(forms[0]);
}

function confirmExit(e)
{
  if ($$('body')[0].id == 'login') return;
  if (submitted) return;
  var forms = $$('form.public');
  if (forms.length == 0) return;
  var snap2 = Form.serialize(forms[0]);
  if (snap2 != snapshot)
  {
    e.returnValue = 'You have not saved your changes';
  }
}
