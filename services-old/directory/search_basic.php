<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoules = "SELECT    spp_boule.CHAPTERID,   concat_ws('',BOULENAME, ' (', CITY, ', ', STATECD, ')') AS LOCATION FROM   spp_boule ORDER BY BOULENAME ASC";
$rsBoules = mysql_query($query_rsBoules, $sigma_modx) or die(mysql_error());
$row_rsBoules = mysql_fetch_assoc($rsBoules);
$totalRows_rsBoules = mysql_num_rows($rsBoules);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = mysql_query($query_rsStates, $sigma_modx) or die(mysql_error());
$row_rsStates = mysql_fetch_assoc($rsStates);
$totalRows_rsStates = mysql_num_rows($rsStates);

// Make a custom transaction instance
$customTransaction = new tNG_custom($conn_sigma_modx);
$tNGs->addTransaction($customTransaction);
// Register triggers
$customTransaction->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Custom1");
$customTransaction->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
// Add columns
$customTransaction->addColumn("FIRSTNAME", "STRING_TYPE", "GET", "FIRSTNAME");
$customTransaction->addColumn("LASTNAME", "STRING_TYPE", "GET", "LASTNAME");
$customTransaction->addColumn("CHAPTERID", "STRING_TYPE", "GET", "CHAPTERID");
$customTransaction->addColumn("CITY", "STRING_TYPE", "GET", "CITY");
$customTransaction->addColumn("STATECD", "STRING_TYPE", "GET", "STATECD");
// End of custom transaction instance

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rscustom = $tNGs->getRecordset("custom");
$row_rscustom = mysql_fetch_assoc($rscustom);
$totalRows_rscustom = mysql_num_rows($rscustom);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_map.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts_map.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">Basic Membership Search</h1>
<div class="contentBlock">
<div id="map"
          onmouseover="document.getElementById('img').src='images/chapterMap/usa_on.gif'"
          onmouseout="document.getElementById('img').src='images/chapterMap/usa_off.gif'">

        <img id="img" src="images/chapterMap/usa_off.gif" alt="Boul&eacute; Chapter Map" title="Boul&eacute; Chapter Map"/>

        <ul>

          <li id="pacific"><a href="#nogo" class="tl" onclick="map.showPopup('pPacific')" title="View Pacific Region Boul&eacute;s">Pacific Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pPacific" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Pacific Region</h1>
                <p><a href="javascript:map.hidePopup('pPacific')" title="Close Popup" class="close">Close Pacific Popup</a></p>
                <div>
                                      <a href="boule_results.php?BOULENAME=Alpha Omicron">Alpha Omicron (Seattle, WA)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Pi">Alpha Pi (San Diego, CA)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Upsilon">Beta Upsilon (San Francisco, CA)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Phi">Delta Phi (Portland, OR)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Rho">Delta Rho (Anchorage, AK)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Theta">Delta Theta (Las Vegas, NV)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Xi">Delta Xi (Diamond Bar, CA)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Alpha">Gamma Alpha (Tucson, AZ)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Chi">Gamma Chi (San Jose, CA)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Epsilon">Gamma Epsilon (Sacramento, CA)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Mu">Gamma Mu (Phoenix, AZ)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Zeta">Gamma Zeta (Pasadena, CA)</a>
                                      <a href="boule_results.php?BOULENAME=Xi">Xi (Los Angeles, CA)</a>
                </div>
              </div>
              <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="western"><a href="#nogo" class="tl" onclick="map.showPopup('pWestern')" title="View Western Region Boul&eacute;s">Western Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pWestern" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Western Region</h1>
                <p><a href="javascript:map.hidePopup('pWestern')" title="Close Popup" class="close">Close Western Popup</a></p>
                <div>
                                      <a href="boule_results.php?BOULENAME=Alpha Epsilon">Alpha Epsilon (Dallas/Ft. Worth, TX)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Nu">Alpha Nu (Wichita, KS)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Theta">Alpha Theta (State of OK, OK)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Eta">Delta Eta (Denver, CO)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Gamma">Delta Gamma (Tyler, TX)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Mu">Delta Mu (Fort Worth, TX)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Zeta">Delta Zeta (Beaumont, TX)</a>
                                      <a href="boule_results.php?BOULENAME=Epsilon Beta">Epsilon Beta (Contra Costa, CA)</a>
                                      <a href="boule_results.php?BOULENAME=Eta">Eta (St. Louis, MO)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Gamma">Gamma Gamma (Austin, TX)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Phi">Gamma Phi (San Antonio, TX)</a>
                                      <a href="boule_results.php?BOULENAME=Nu">Nu (Houston, TX)</a>
                                      <a href="boule_results.php?BOULENAME=Pi">Pi (Little Rock, AR)</a>
                                      <a href="boule_results.php?BOULENAME=Theta">Theta (Kansas City, MO)</a>
                </div>
              </div>
              <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="central"><a href="#nogo" class="tl" onclick="map.showPopup('pCentral')" title="View Central Region Boul&eacute;s">Central Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pCentral" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Central Region</h1>
                <p><a href="javascript:map.hidePopup('pCentral')" title="Close Popup" class="close">Close Central Popup</a></p>
                <div>
                                      <a href="boule_results.php?BOULENAME=Alpha Chi">Alpha Chi (Lansing, MI)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Delta">Alpha Delta (Cincinnati, OH)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Eta">Alpha Eta (Indianapolis, IN)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Phi">Alpha Phi (Toledo, OH)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Upsilon">Alpha Upsilon (Bluefield, WV)</a>
                                      <a href="boule_results.php?BOULENAME=Beta">Beta (Chicago, IL)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Alpha">Beta Alpha (Milwaukee, WI)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Omicron">Beta Omicron (Grand Rapids, MI)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Rho">Beta Rho (Akron/Kent, OH)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Alpha">Delta Alpha (Northern Illinois, IL)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Sigma">Delta Sigma (S. Suburban Chicago, IL)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Tau">Delta Tau (Frankfort, KY)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Delta">Gamma Delta (Flint, MI)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Eta">Gamma Eta (Des Moines, IA)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Rho">Gamma Rho (Ann Arbor, MI)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Tau">Gamma Tau (Racine/Kenosha, WI)</a>
                                      <a href="boule_results.php?BOULENAME=Iota">Iota (Detroit, MI)</a>
                                      <a href="boule_results.php?BOULENAME=Lambda">Lambda (Columbus, OH)</a>
                                      <a href="boule_results.php?BOULENAME=Omicron">Omicron (Minneapolis/St. Paul, MN)</a>
                                      <a href="boule_results.php?BOULENAME=Psi">Psi (Louisville, KY)</a>
                                      <a href="boule_results.php?BOULENAME=Sigma">Sigma (Dayton, OH)</a>
                                      <a href="boule_results.php?BOULENAME=Tau">Tau (Cleveland, OH)</a>
                                      <a href="boule_results.php?BOULENAME=Upsilon">Upsilon (Charleston, WV)</a>
                </div>
              </div>
              <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="northeast"><a href="#nogo" class="tl" onclick="map.showPopup('pNortheast')" title="View Northeast Region Boul&eacute;s">North East Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pNortheast" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Northeast Region</h1>
                <p><a href="javascript:map.hidePopup('pNortheast')" title="Close Popup" class="close">Close Northeast Popup</a></p>
                <div>
                                      <a href="boule_results.php?BOULENAME=Alpha">Alpha (Philadelphia, PA)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Gamma">Alpha Gamma (Oakland/Berkeley, CA)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Kappa">Alpha Kappa (Buffalo, NY)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Psi">Alpha Psi (Hartford, CT)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Sigma">Alpha Sigma (Brooklyn/Long Island, NY)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Beta">Beta Beta (Boston, MA)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Eta">Beta Eta (State of DE, DE)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Iota">Beta Iota (Waterbury, CT)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Mu">Beta Mu (Suburban MD, MD)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Nu">Beta Nu (Northern VA, VA)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Pi">Beta Pi (Harrisburg, PA)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Psi">Beta Psi (Albany, NY)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Sigma">Beta Sigma (Springfield, MA)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Tau">Beta Tau (New Haven, CT)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Zeta">Beta Zeta (Westchester Co., NY)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Epsilon">Delta Epsilon (Southern NJ, NJ)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Nu">Delta Nu (Fairfield, CT)</a>
                                      <a href="boule_results.php?BOULENAME=Epsilon">Epsilon (Washington, DC)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma">Gamma (Baltimore, MD)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Iota">Gamma Iota (Rochester, NY)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Theta">Gamma Theta (Princess Anne, MD)</a>
                                      <a href="boule_results.php?BOULENAME=Mu">Mu (Northern NJ, NJ)</a>
                                      <a href="boule_results.php?BOULENAME=Rho">Rho (Pittsburgh, PA)</a>
                                      <a href="boule_results.php?BOULENAME=Zeta">Zeta (New York, NY)</a>
                </div>
              </div>
              <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

          <li id="southeast"><a href="#nogo" class="tl" onclick="map.showPopup('pSoutheast')" title="View Southeast Region Boul&eacute;s">South East Region<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div id="pSoutheast" class="xsnazzy">
              <b class="xb1"></b><b class="xb2"></b><b class="xb3"></b><b class="xb4"></b><b class="xb5"></b><b class="xb6"></b><b class="xb7"></b>
              <div class="xboxcontent">
                <h1>Southeast Region</h1>
                <p><a href="javascript:map.hidePopup('pSoutheast')" title="Close Popup" class="close">Close Southeast Popup</a></p>
                <div>
                                      <a href="boule_results.php?BOULENAME=Alpha Alpha">Alpha Alpha (New Orleans, LA)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Beta">Alpha Beta (Richmond, VA)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Iota">Alpha Iota (Columbia, SC)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Lambda">Alpha Lambda (Savannah, GA)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Mu">Alpha Mu (Augusta, GA)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Rho">Alpha Rho (Miami, FL)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Tau">Alpha Tau (Durham, NC)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Xi">Alpha Xi (Baton Rouge, LA)</a>
                                      <a href="boule_results.php?BOULENAME=Alpha Zeta">Alpha Zeta (Tallahassee, FL)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Chi">Beta Chi (Fort Valley, GA)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Delta">Beta Delta (Charlotte, NC)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Epsilon">Beta Epsilon (Greensboro, NC)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Gamma">Beta Gamma (Jackson, MS)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Kappa">Beta Kappa (Birmingham, AL)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Lambda">Beta Lambda (Hampton/Norfolk, VA)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Phi">Beta Phi (Roanoke/Lynchburg, VA)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Theta">Beta Theta (Knoxville, TN)</a>
                                      <a href="boule_results.php?BOULENAME=Beta Xi">Beta Xi (Central Florida, FL)</a>
                                      <a href="boule_results.php?BOULENAME=Chi">Chi (Nashville, TN)</a>
                                      <a href="boule_results.php?BOULENAME=Delta">Delta (Memphis, TN)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Beta">Delta Beta (Greenville, SC)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Delta">Delta Delta (Albany, GA)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Iota">Delta Iota (Mobile, AL)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Kappa">Delta Kappa (Shreveport, LA)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Lambda">Delta Lambda (Nassau, Ba)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Omicron">Delta Omicron (Gulfport, MS)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Pi">Delta Pi (Pensacola, FL)</a>
                                      <a href="boule_results.php?BOULENAME=Delta Upsilon">Delta Upsilon (North Atlanta, GA)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Beta">Gamma Beta (Jacksonville, FL)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Kappa">Gamma Kappa (Winson-Salem, NC)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Lambda">Gamma Lambda (Charleston, SC)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Nu">Gamma Nu (Northern Mississippi, MS)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Omicron">Gamma Omicron (Tampa Bay, FL)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Pi">Gamma Pi (Chattanooga, TN)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Psi">Gamma Psi (Columbus, GA)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Sigma">Gamma Sigma (Raleigh, NC)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Upsilon">Gamma Upsilon (Asheville, NC)</a>
                                      <a href="boule_results.php?BOULENAME=Gamma Xi">Gamma Xi (Sarasota, FL)</a>
                                      <a href="boule_results.php?BOULENAME=Kappa">Kappa (Atlanta, GA)</a>
                                      <a href="boule_results.php?BOULENAME=Phi">Phi (Montgomery, AL)</a>

                </div>
              </div>
              <b class="xb7"></b><b class="xb6"></b><b class="xb5"></b><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b class="xb1"></b>
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
          </li>

        </ul>

        </div>

<p>here</p>
<p>&nbsp;
  <?php
	echo $tNGs->getErrorMsg();
?>
<form action="results_basic.php" method="get" name="form1" id="form1">
  <table align="center" cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr>
      <td class="KT_th"><label for="FIRSTNAME">FIRST NAME:</label></td>
      <td><input type="text" name="FIRSTNAME" id="FIRSTNAME" value="<?php echo KT_escapeAttribute($row_rscustom['FIRSTNAME']); ?>" size="32" />
          <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("custom", "FIRSTNAME"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="LASTNAME">LAST NAME:</label></td>
      <td><input type="text" name="LASTNAME" id="LASTNAME" value="<?php echo KT_escapeAttribute($row_rscustom['LASTNAME']); ?>" size="32" />
          <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("custom", "LASTNAME"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="CHAPTERID">BOUL&#201;:</label></td>
      <td><select name="CHAPTERID" id="CHAPTERID">
	  <option value="">Select</option> 
        <?php 
do {  
?>
        <option value="<?php echo $row_rsBoules['CHAPTERID']?>"<?php if (!(strcmp($row_rsBoules['CHAPTERID'], $row_rscustom['CHAPTERID']))) {echo "SELECTED";} ?>><?php echo $row_rsBoules['LOCATION']?></option>
        <?php
} while ($row_rsBoules = mysql_fetch_assoc($rsBoules));
  $rows = mysql_num_rows($rsBoules);
  if($rows > 0) {
      mysql_data_seek($rsBoules, 0);
	  $row_rsBoules = mysql_fetch_assoc($rsBoules);
  }
?>
      </select>
        <?php echo $tNGs->displayFieldError("custom", "CHAPTERID"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="CITY">CITY:</label></td>
      <td><input type="text" name="CITY" id="CITY" value="<?php echo KT_escapeAttribute($row_rscustom['CITY']); ?>" size="32" />
          <?php echo $tNGs->displayFieldHint("CITY");?> <?php echo $tNGs->displayFieldError("custom", "CITY"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="STATECD">STATE/COUNTRY:</label></td>
      <td><select name="STATECD" id="STATECD">
	  <option value="">Select</option>
        <?php
do {  
?>
        <option value="<?php echo $row_rsStates['st_cd']?>"<?php if (!(strcmp($row_rsStates['st_cd'], KT_escapeAttribute($row_rscustom['STATECD'])))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsStates['st_nm']?></option>
        <?php
} while ($row_rsStates = mysql_fetch_assoc($rsStates));
  $rows = mysql_num_rows($rsStates);
  if($rows > 0) {
      mysql_data_seek($rsStates, 0);
	  $row_rsStates = mysql_fetch_assoc($rsStates);
  }
?>
      </select>
        <?php echo $tNGs->displayFieldError("custom", "STATECD"); ?> </td></tr>
    <tr class="KT_buttons">
      <td colspan="2"><input type="submit" name="KT_Custom1" id="KT_Custom1" value="Search" />      </td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsBoules);

mysql_free_result($rsStates);
?>
