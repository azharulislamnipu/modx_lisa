<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Customize page
 *
 * @package svManager
 */
class AvCustomizePage extends Page
{
 /**
  * @var string overrides some of head content in Page
  */
  var $customHeadHtml = '
       <link href="colorjack/plugin.css" rel="stylesheet" type="text/css" />
       <style type="text/css">
         #plugin {
           color: #FFFFFF;
           background-color: #999999;
         }
         #H {
           border-color: #999999;
         }
         #plugCLOSE {
           color: #FFFFFF;
           background-color: #999999;
         }
       </style>
       <script src="scripts/mootools-core.js" type="text/javascript"></script>
       <script src="scripts/mootools-more.js" type="text/javascript"></script>
       <script src="colorjack/plugin.js" type="text/javaScript"></script>
       <script src="scripts/customize.js" type="text/javascript"></script>
       <script src="scripts/mooformcheck.js" type="text/javascript"></script>
';
 
 /**
  * constructs CusomizePage class
  * @param string contains text for html <title></title> tags
  * @param string html id for body tag
  */
  function AvCustomizePage()
  {
    parent::Page('svManager &ndash; customize', 'avcustomize', 'navcustomize customize autoviewer');
  }

 /**
  * get html for customize page
  *
  * Note hack in conditional comments from
  * @link http://www.hedgerwow.com/360/bugs/css-select-free.html
  * to prevent select elements showing above the colorpicker in IE6Win
  * @access public
  * @return string html
  * @param object gallery set
  * @param object gallery
  */
  function getCustomizeHtml(&$gallerySet, &$gallery)
  {
    $preferences = $gallery->getPreferences();
    $rootUrl = $gallery->getRootUrl();
    $title = htmlspecialchars($gallery->getGalleryTitleText());
    $serverName = 'http://'.$_SERVER['SERVER_NAME'].'/';
    $svManagerUrl = substr(dirname($_SERVER['PHP_SELF']), 1);
    $frameColor = strtoupper(substr($preferences['frameColor'], 2, strlen($preferences['frameColor']) - 2));
    $backgroundColor = strtoupper(substr($preferences['backgroundColor'], 2, strlen($preferences['backgroundColor']) - 2));
    $enableRightClickOpenChecked = ($preferences['enableRightClickOpen'] == 'true') ? 'checked="checked"' : '';
    $relativePathChecked = '';
    $absolutePathChecked = '';
    $httpPathChecked = '';
    switch (strtoupper($preferences['urlType']))
    {
      case 'ABSOLUTE' :
      $absolutePathChecked = 'checked="checked"';
      break;
      case 'HTTP' :
      $httpPathChecked = 'checked="checked"';
      break;
      default :
      $relativePathChecked = 'checked="checked"';
    }

    
$html = <<<EOD

<div id="plugin" class="selectfree">
 <div id="plugHEX" onmousedown="stop=0; setTimeout('stop=1',100);">F1FFCC</div>
 <!-- Note change of function name from toggle to avoid namespace conflict -->
 <div id="plugCLOSE" onmousedown="toggleDisplay('plugin')">X</div><br />
 <div id="SV" onmousedown="HSVslide('SVslide','plugin',event)" title="Saturation + Value">
  <div id="SVslide" style="TOP: -4px; LEFT: -4px;"><br /></div>
 </div>
 <form id="H" action="" onmousedown="HSVslide('Hslide','plugin',event)" title="Hue">
  <div id="Hslide" style="TOP: -7px; LEFT: -8px;"><br /></div>
  <div id="Hmodel"></div>
 </form>
 <!--[if lte IE 6.5]><iframe></iframe><![endif]-->
</div>
<form class="public" action = "{$_SERVER['PHP_SELF']}" id="customizeform" method="post">
  <div class="elementwrap">
    <h3 class="togglers">Basic settings</h3>
    <div class="accordionElements">
      <table id="settings1" cellspacing="0">
        <tr id="titleentry">
          <td class="label"><label for="title">Gallery title</label></td><td><input type="text" id="title" tabindex="1" class="text" name="galleryTitle" value="{$title}" /></td>
        </tr>
      </table>
    
      <table id="settings2" cellspacing="0">
        <tr id="framecolorentry">
          <td class="label"><label for="framecolor">Frame color</label></td><td><input type="text" id="framecolor" tabindex="3" class="colorpicker" name="frameColor" value="{$frameColor}" /></td>
        </tr>
        <tr id="backgroundcolorentry">
          <td class="label"><label for="backgroundcolor">Background color</label></td><td><input type="text" id="backgroundcolor" tabindex="4" class="colorpicker" name="backgroundColor" value="{$backgroundColor}" /></td>
        </tr>
        <tr id="imagepaddingentry">
          <td class="label"><label for="imagepadding">Image padding px</label></td><td><input type="text" id="imagepadding" tabindex="6" class="text" name="imagePadding" value="{$preferences['imagePadding']}" /></td>
        </tr>
        <tr id="displaytimeentry">
          <td class="label"><label for="displaytime">Display time sec</label></td><td><input type="text" id="displaytime" tabindex="7" class="text" name="displayTime" value="{$preferences['displayTime']}" /></td>
        </tr>
      </table>
    
      <table id="settings3" cellspacing="0">
        <tr id="framewidthentry">
          <td class="label"><label for="framewidth">Frame width px</label></td><td><input type="text" id="framewidth" tabindex="5" class="text" name="frameWidth" value="{$preferences['frameWidth']}" /></td>
        </tr>
        <tr id="imagewidthentry">
          <td class="label"><label for="maximagewidth">Max resize width px</label></td><td><input type="text" id="maximagewidth" tabindex="10" class="text" name="maxImageWidth" value="{$preferences['maxImageWidth']}" /></td>
        </tr>
        <tr id="imageheightentry">
          <td class="label"><label for="maximageheight">Max resize height px</label></td><td><input type="text" id="maximageheight" tabindex="11" class="text" name="maxImageHeight" value="{$preferences['maxImageHeight']}" /></td>
        </tr>
        <tr id="enablerightclickopenentry">
          <td class="label"><label for="enablerightclickopen">Right-click download</label></td><td><input type="checkbox" class="checkbox" id="enablerightclickopen" tabindex="8" {$enableRightClickOpenChecked} name="enableRightClickOpen" /></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="elementwrap" id="advanced">
    <h3 class="togglers">Advanced settings</h3>
    <div class="accordionElements">
      <table id="settingspaths" cellspacing="0">
        <tr id="absurlentry">
          <td class="label"><label for="absurl">Path from web root</label></td><td><input type="text" id="absurl" tabindex="2" class="text" name="absurl" value="{$rootUrl}" /></td>
        </tr>
        <tr id="urltypeentry">
            <td class="label">Save paths as</td>
            <td><input id="relativepath" tabindex="12" type="radio" name="urlType" value="RELATIVE" {$relativePathChecked} /><label for="relativepath">Relative</label>
            <input id="absolutepath" tabindex="13" type="radio" name="urlType" value="ABSOLUTE" {$absolutePathChecked} /><label for="absolutepath">Absolute</label>
            <input id="httppath" tabindex="13" type="radio" name="urlType" value="HTTP" {$httpPathChecked} /><label for="httppath">http</label></td>
          </tr>
      </table>
    </div>
  </div>
  <div id="submitinputs">
    <input type="hidden" name="customizesubmitted" value="true" /><input type="submit" name="submit" class="formbutton" value="Update" />
  </div>
</form>
EOD;
    return $html;
  }

}
