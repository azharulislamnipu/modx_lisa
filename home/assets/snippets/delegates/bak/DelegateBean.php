<?php

/**
 * Holds a delegate's information
 * @author Pat Heard {http://fullahead.org}
 */
 
class DelegateBean implements Serializable {

	const TYPE_DELEGATE = 'D';
	const TYPE_ALTERNATE = 'A';

	private $name = '';
	private $memberNumber = '';
	private $type = '';
        private $appointed = false;
        private $ineligible = false;
	
	public function __construct(array $args = null){

		// Set default arguments
		$args = array_merge(array(
			'name'		=> '',
			'memberNumber'	=> '',
			'type'		=> '',
                        'appointed'	=> false,
                        'ineligible'	=> false
		), $args);
	
		$this->name = ucwords($args['name']);
		$this->memberNumber = $args['memberNumber'];
		$this->type = $args['type'];
                $this->appointed = $args['appointed'];
                $this->ineligible = $args['ineligible'];
	}
        
        public function serialize(){  
            return serialize(array(  
                'name'          => $this->name,  
                'memberNumber'  => $this->memberNumber,  
                'type'          => $this->type,  
                'appointed'     => $this->appointed,  
                'ineligible'    => $this->ineligible  
            )); 
        }  

        public function unserialize($serializedData){  
            $data = unserialize($serializedData);
            
            $this->name = isset($data['name']) ? $data['name'] : '';	
            $this->memberNumber = isset($data['memberNumber']) ? $data['memberNumber'] : '';
            $this->type = isset($data['type']) ? $data['type'] : '';
            $this->appointed = isset($data['appointed']) ? $data['appointed'] : false; 
            $this->ineligible = isset($data['ineligible']) ? $data['ineligible'] : false;
        }         

        public function validate(){
            $messages = array();
            if(empty($this->name)){
                $messages[] = array('error', 'Delegate name cannot be empty.');
            }
            if(empty($this->memberNumber)){
                $messages[] = array('error', 'Delegate member number cannot be empty.');
            }
            if($this->type !== self::TYPE_DELEGATE && $this->type !== self::TYPE_ALTERNATE){
                $messages[] = array('error', 'Delegate type is not valid.  Must be "D" or "A".');
            }  
            return $messages;
        }
        
	public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
	}
	
	public function __toString(){
            return
                ' name :' . $this->name .
                ' memberNumber :' . $this->memberNumber .
                ' type :' . $this->type . 
                ' appointed :' . $this->appointed;
                ' ineligible :' . $this->ineligible;
	}
}

?>