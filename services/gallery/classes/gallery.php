<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Models a generalised gallery
 *
 * @package svManager
 */
class Gallery
{
 /**
  * @var integer unique gallery reference
  */
  var $galleryRef;
  
 /**
  * @var string gallery path relative to svManager
  */
  var $galleryPathRelSvm;


 /**
  * @var object instance of pathParser class
  */
  var $pathParser;
  
 /**
  * @var object instance of XmlParser class
  */
  var $xmlParser;

 /**
  * @var object instance of HtmlEntityDecode class
  */
  var $htmlEntityDecode;
   
 /**
  * Constructs Gallery
  *
  * 
  * @access public
  *
  */  
  function Gallery()
  {
    $this->pathParser = new PathParser();
    $this->xmlParser = new XmlParser();
    $this->htmlEntityDecode = new HtmlEntityDecode();
    $this->rCopy = new RCopy();
  }
  
 /**
  * Get gallery ref
  *
  * @access public
  * @return integer
  */
  function getGalleryRef()
  {
    return $this->galleryRef;
  }
  
 /**
  * Get gallery path
  *
  * @access public
  * @return string
  */
  function getGalleryPathRelSvm()
  {
    return $this->galleryPathRelSvm;
  }
  
 /**
  * Translate a path rel gallery to a url rel svm
  *
  * @access public
  * @return string
  * @param string path or url rel gallery
  */
  function getUrlRelSvm($pathRelGallery)
  {
    return $this->pathParser->fix($this->galleryPathRelSvm.$pathRelGallery);
  }
  
 /* Get image path relative to svManager
  * Image directory paths that start with ../ are fixed with pathparser
  *
  * @access private
  * @return string
  */
  function getImageDirPathRelSvm()
  {
    return str_replace(
      '/', DIRECTORY_SEPARATOR, $this->pathParser->fix(
        $this->galleryPathRelSvm.$this->imageDirPathRelGallery
      )
    );
  }
  
 /* Get thumb path relative to svManager
  *
  * @access private
  * @return string
  */
  function getThumbDirPathRelSvm()
  {
    return str_replace(
      '/', DIRECTORY_SEPARATOR, $this->pathParser->fix(
        $this->galleryPathRelSvm.$this->thumbDirPathRelGallery
      )
    );
  }
  
 /* Get image url relative to svManager
  * pathParser->fix gives forward slashes and fixes image paths that start ../
  *
  * @access public
  * @return string
  */
  function getImageDirUrlRelSvm()
  {
    return $this->pathParser->fix($this->galleryPathRelSvm.$this->imageDirPathRelGallery);
  }
  
 /* Get thumb url relative to svManager
  * pathParser->fix gives forward slashes and fixes thumb paths that start ../
  *
  * @access public
  * @return string
  */
  function getThumbDirUrlRelSvm()
  {
    return $this->pathParser->fix($this->galleryPathRelSvm.$this->thumbDirPathRelGallery);
  }
  
 /**
   * Save all gallery data to xml file and preferences file
   *
   * @access public
   * @return boolean true on success
   */
  function saveGallery()
  {
    $this->savePreferences();
    $xmlPath = $this->getXmlPath();
    if(!$this->writeXml($xmlPath))
    {
      trigger_error('unable to save gallery data to '.$this->getXmlPath().' file', E_USER_NOTICE);
      return false;
    }
    $thumbSaved = $this->saveIndexThumb();
    return $thumbSaved;
  }
  
 /**
   * Update preferences and save
   *
   * @access public
   * @return boolean success
   */
  function savePreferences()
   {
     $newPrefs = array_intersect_key($this->preferences, $this->storedInPreferences);
     if (@!file_put_contents($this->galleryPathRelSvm.PREFERENCES_PATH, serialize($newPrefs), FPC_LOCK))
     {
       trigger_error('unable to save preferences to '.$this->galleryPathRelSvm.PREFERENCES_PATH, E_USER_NOTICE);
       return false;
     }
     else
     {
       return true;
     }
   }
   
 /**
  * Update pathScheme
  * @access public
  * @return void
  */
  function setPathScheme()
  {
    $absPath = dirname($_SERVER['PHP_SELF']).'/';
    if (!isset($this->preferences['urlType']))
    {
      $this->pathScheme = '';
      return;
    }
    switch (strtoupper($this->preferences['urlType']))
    {
      case 'ABSOLUTE' :
        $this->pathScheme = $absPath;
        break;
      case 'HTTP' :
        $this->pathScheme = 'http://'.$_SERVER['SERVER_NAME'].'/'.ltrim($absPath, '\\/');
        break;
      default :
        $this->pathScheme = ''; 
    }
  }
  
 /**
  * Create new gallery from master copy
  * Creates empty image and thumb directories if necessary
  *
  * @access public
  * @return boolean success.
  * 
  */
  function addGallery($galleryRef)
  {
    $galleryPathRelSvm = GALLERY_PREFIX.$galleryRef.DIRECTORY_SEPARATOR;
    $rootUrl = '/'.ltrim($this->pathParser->fix(dirname($_SERVER['PHP_SELF']).'/'.$galleryPathRelSvm),'/');
    $fileCount = $this->rCopy->doCopy(rtrim($this->masterPath, '\\/'), rtrim($galleryPathRelSvm, '\\/'));
    if ($fileCount < $this->masterFileCount)
    {
      trigger_error( 'file copy incomplete '.$fileCount.' out of '.$this->masterFileCount.' copied', E_USER_NOTICE);
    }
    $imageDir = rtrim($galleryPathRelSvm.$this->imageDirPathRelGallery, '\\/');
    $thumbDir = rtrim($galleryPathRelSvm.$this->thumbDirPathRelGallery, '\\/');
    if (!file_exists($imageDir))
    {
      if (!@mkdir($imageDir))
      {
        trigger_error('unable to create image directory ', E_USER_NOTICE);
      }
    }
    if (!file_exists($thumbDir))
    {
      if (!@mkdir($thumbDir))
      {
        trigger_error('unable to create thumb directory ', E_USER_NOTICE);
      }
    }
    $this->saveGallery();
    return true;
  }
  
 /**
  * Move gallery and update image and thumb paths and background image path
  *
  * @access private
  * @return string new gallery path relative to SVM
  * @param string new root url from customize form
  */
  function moveGallery($rootUrl)
  {
    $imagesData = array();
    if (!@$realGalleryPath = realpath($this->galleryPathRelSvm))
    {
      trigger_error('Cannot find gallery path '.$this->galleryPathRelSvm, E_USER_WARNING);
      return false;
    }
    if (!@$realImagePath = realpath($this->getImageDirPathRelSvm()))
    {
      trigger_error('Cannot find image path '.$this->getImageDirPathRelSvm());
      return false;
    }
    if (!@$realThumbPath = realpath($this->getThumbDirPathRelSvm()))
    {
      trigger_error('Cannot find thumb path '.$this->getThumbDirPathRelSvm());
      return false;
    }
    if (isset($this->preferences['backgroundImagePath']) && ($this->preferences['backgroundImagePath'] != ''))
    {
      $backgroundImagePath = $this->preferences['backgroundImagePath'];
      $backgroundImagePathRelGallery = $this->getPathRelGallery($backgroundImagePath);
      @$realBackgroundImagePath = realpath($this->galleryPathRelSvm.$backgroundImagePathRelGallery);
      $backgroundImageInGallery = (strpos($realBackgroundImagePath, $realGalleryPath) !== false);
    }
    $imageDirInGallery = (strpos($realImagePath, $realGalleryPath) !== false);
    $thumbDirInGallery = (strpos($realThumbPath, $realGalleryPath) !== false);
    $imageObjects = $this->imageObjects;
    foreach($imageObjects as $key=>$imageObject)
    {
      $imagesData[$key]['imagePathRelGallery'] = $imageObject->getImagePathRelGallery();
      $imagesData[$key]['realImagePath'] = realPath($this->galleryPathRelSvm.$imageObject->getImagePathRelGallery());
      $imagesData[$key]['imageInGallery'] = (strpos($imagesData[$key]['realImagePath'], $realGalleryPath) !== false);
      $imagesData[$key]['thumbPathRelGallery'] = $imageObject->getThumbPathRelGallery();
      $imagesData[$key]['realThumbPath'] = realPath($this->galleryPathRelSvm.$imageObject->getThumbPathRelGallery());
      $imagesData[$key]['thumbInGallery'] = (strpos($imagesData[$key]['realThumbPath'], $realGalleryPath) !== false);
    }
    unset($key);
    unset($imageObject);
    // Move gallery directory here
    $newGalleryPathRelSvm = $this->moveGalleryDir($rootUrl);
    if ($newGalleryPathRelSvm === false)  return false;
    $this->galleryPathRelSvm = $newGalleryPathRelSvm;
    if (!@$realGalleryPath = realpath($this->galleryPathRelSvm))
    {
      trigger_error('Cannot find gallery path '.$this->galleryPathRelSvm, E_USER_WARNING);
      return false;
    }
    if(!$imageDirInGallery)
    {
      // absolute paths are invariant
      $this->imageDirPathRelGallery = $this->pathParser->findRelativePath($realGalleryPath, $realImagePath);
    }
    if(!$thumbDirInGallery)
    {
      // absolute paths are invariant
      $this->thumbDirPathRelGallery = $this->pathParser->findRelativePath($realGalleryPath, $realThumbPath);
    }
    if (isset($this->preferences['backgroundImagePath']) && ($this->preferences['backgroundImagePath'] != ''))
    {
      if(!$backgroundImageInGallery)
      {
        $backgroundImagePathRelGallery = $this->pathParser->findRelativePath($realGalleryPath, $realBackgroundImagePath); 
      }
      $this->preferences['backgroundImagePath'] = $this->convertRelToScheme($backgroundImagePathRelGallery, $this->galleryPathRelSvm, $this->pathScheme);
    }
    $imagesDirUrl = $this->convertRelToScheme($this->imageDirPathRelGallery, $this->galleryPathRelSvm, $this->pathScheme);
    foreach ($imagesData as $key=>$imageArray)
    {
      if(!$imageArray['imageInGallery'])
      {
        // absolute paths are invariant
        $imagesData[$key]['imagePathRelGallery'] = $this->pathParser->findRelativePath($realGalleryPath, $imageArray['realImagePath']);
      }
      $this->imageObjects[$key]->setImagePathRelGallery($imagesData[$key]['imagePathRelGallery']);
      if(!$imageArray['thumbInGallery'])
      {
        // absolute paths are invariant
        $imagesData[$key]['thumbPathRelGallery'] = $this->pathParser->findRelativePath($realGalleryPath, $imageArray['realThumbPath']);
      }
      $this->imageObjects[$key]->setThumbPathRelGallery($imagesData[$key]['thumbPathRelGallery']);
    }
    return $this->galleryPathRelSvm;
  }

 /**
  * Move gallery directory
  *
  * @access public 
  * @return relative gallery path
  * @param string galleryRef
  * @param string new path
  */
  function moveGalleryDir($rootUrl)
  {
    $rootUrl = trim($rootUrl);
    $rootUrl = '/'.ltrim($rootUrl, '/');
    $rootUrl = rtrim($rootUrl, '/');
    $newRelPath = $this->pathParser->findRelativePath($_SERVER['PHP_SELF'], $rootUrl);
    $newRelPath = str_replace('/', DIRECTORY_SEPARATOR, $newRelPath);
    $newFolder = rtrim($newRelPath, '\\/');
    $galleryFolder = rtrim($this->galleryPathRelSvm, '\\/');
    if (!$this->isEmpty($newFolder))
    {
      trigger_error('new folder is not empty &ndash; no move', E_USER_NOTICE);
      return false;
    }
    if (!@rename($galleryFolder, $newFolder))
    {
      trigger_error('unable to move '.$galleryFolder.' to '.$newFolder, E_USER_NOTICE);
      return false;
    }
    return $newRelPath;
  }

 /**
  * Check if it's ok to rename to this directory name
  *
  * @access private
  * @param string path to directory
  * @return boolean true if directory does NOT exist or exists and is empty
  */
  function isEmpty($dir)
  {
    if (!file_exists($dir)) return true;
    if (count(glob("$dir/*")) === 0) return true;
    return false;
  }

 /**
  * Rebuild the local xml file
  *
  * @access public
  * @return boolean success
  */
  function rebuild()
  {
    $sortOrder = $this->getSortOrder();
    $relThumbPath = $this->getThumbDirPathRelSvm();
    foreach ($this->imageObjects as $key=>$imageObject)
    {
      if(!file_exists($this->galleryPathRelSvm.$imageObject->getimagePathRelGallery()))
      {
        unset($this->imageObjects[$key]);
      }
    }
    $scannedImageObjects = $this->scanImageData();
    $newImageObjects = $this->extractNewImageData($this->imageObjects, $scannedImageObjects);
    $this->imageObjects = array_merge($this->imageObjects, $newImageObjects);
    $this->imageObjects = $this->sortImages($this->imageObjects, $sortOrder);
    if ($this->getImageDirPathRelSvm() != $this->getThumbDirPathRelSvm())
    {
      $this->makeThumbs($relThumbPath, $this->imageObjects, OVERWRITE_THUMBNAILS);
    }
    $this->saveGallery();
    /*
    if (!copy ($this->masterPath.$this->viewerSwf, $this->galleryPathRelSvm.$this->viewerSwf))
    {
      trigger_error('failed to copy '.$this->viewerSwf.' file', E_USER_NOTICE);
    }
    if (!copy ($this->masterPath.'swfobject.js', $this->galleryPathRelSvm.'swfobject.js'))
    {
      trigger_error('failed to copy swfobject.js file', E_USER_NOTICE);
    }
    */
    return true;
  }

 /**
  * Add new images
  *
  * Similar to rebuild but does not overwrite existing thumbs
  * retains existing captions etc
  *
  * @access public
  * @return boolean success
  */
  function addNewImages()
  {
    $sortOrder = $this->getSortOrder();
    $relThumbPath = $this->getThumbDirPathRelSvm();
    $scannedImageObjects = $this->scanImageData();
    $newImageObjects = $this->extractNewImageData($this->imageObjects, $scannedImageObjects);
    $this->imageObjects = array_merge($this->imageObjects, $newImageObjects);
    $this->imageObjects = $this->sortImages($this->imageObjects, $sortOrder);
    if ($this->getImageDirPathRelSvm() != $this->getThumbDirPathRelSvm())
    {
      $this->makeThumbs($relThumbPath, $newImageObjects, OVERWRITE_THUMBNAILS);
    }
    $this->saveGallery();
    return true;
  }
  
 /**
  * returns array of imageObjects for files found in default image location
  *
  * @access private
  * @return array
  */
  function scanImageData()
  {
    $imageObjects = array();
    $relImagePath = $this->getImageDirPathRelSvm();        
    if (@!is_dir($relImagePath))
    {
      trigger_error('the image directory '.$relImagePath.' cannot be found', E_USER_ERROR);
    }
    $handle = @opendir($relImagePath);
    if ($handle === false)
    {
      trigger_error('cannot open the '.$relImagePath.' directory &ndash; check the file permissions', E_USER_ERROR);
    }
    while(false !== ($fileName = readdir($handle)))
    {
    	if (!$this->isImage($fileName)) {continue;}
    	$imageSize = @getimagesize($relImagePath.$fileName);
    	$imageObjects[] = new $this->imageClass($this->galleryPathRelSvm, $this->imageDirPathRelGallery.$fileName, $this->thumbDirPathRelGallery.$fileName);
    }
    closedir($handle);
    return $imageObjects;
  }

  
 /**
   * Extract new image data from scan containing all current image data
   *
   * @access private
   * @return array new image data
   * @param array old image data
   * @param array scanned image data
   */
  function extractNewImageData($oldImageObjects, $scannedImageObjects)
  {
    $newImageObjects = array();
    foreach ($scannedImageObjects as $key=>$imageObject)
    {
      if($this->fileInImageData($imageObject->getImageFileName(), $oldImageObjects) === false)
      {
        $newImageObjects[] = $imageObject;
      }
    }
    return $newImageObjects;
  }

 /**
  * Merge old and new image data without overwriting existing captions etc
  *
  * @access private
  * @return array merged image data
  * @param array old image data
  * @param array new image data
  */
  /*
  function mergeImageData($oldImageData, $newImageData)
  {
    $additions = array();
    foreach($newImageData as $key=>$imageArray)
    {
      if (!$this->fileInImageData($imageArray['fileName'], $oldImageData))
      {
        $additions[] = $imageArray;
      }
    }
    return array_merge($oldImageData, $additions);
  }
  */
  
 /** Test if fileName already present in the imageData array
  *
  * @access private
  * @return integer array key
  * @param string needle
  * @param array haystack
  */
  function fileInImageData($fileName, $imageObjects)
  {
    
    foreach ($imageObjects as $key=>$imageObject)
    {
      if ($imageObject->getImageFileName() === $fileName) return $key;
    }
    return false;
  }

 /**
  * Get root url
  *
  * @access public
  * @return string calculated root url with forward slashes
  */
  function getRootUrl()
  {
  	return '/'.ltrim($this->pathParser->fix(dirname($_SERVER['PHP_SELF']).'/'.$this->galleryPathRelSvm),'/');
  }
  
 /**
  * Set gallery path
  * 
  * @access public
  * @return boolean
  * @param string path
  */
  function setGalleryPath($path)
  {
    $this->galleryPathRelSvm = $path;
  }
  
 /**
  *
  * Get Gallery preferences
  * 
  * @access public
  * @return array
  */
  function getPreferences()
  {
    return $this->preferences;
  }
  
 /**
  * Set preferences
  *
  * @access public
  * @return void
  * @param array
  */
  function setPreferences($preferences)
  {
    $this->preferences = $preferences;
  }

  
 /**
  * Convert relative image or thumb path to url according to path scheme
  *
  * @access private
  * @return string url
  * @param string path relative to gallery
  * @param string gallery path relative to svmanager
  * @param path scheme
  */
  function convertRelToScheme($pathRelGallery, $galleryPathRelSvm, $pathScheme)
  {
    if ($pathScheme == '') return str_replace('\\', '/', $pathRelGallery);
    $pathParts = $this->pathParser->parse($pathScheme);
    if ($pathParts['root'] == '/' || $pathParts['scheme'] !== '')
    {
      return $this->pathParser->fix($pathScheme.$galleryPathRelSvm.$pathRelGallery);
    }
    // relative path scheme
    return str_replace('\\', '/', $pathRelGallery);
  }
    
  /**
  * Read preferences file
  * 
  * @access private
  * @return array as in preferences file or false on failure
  */
  function readPreferences()
  {
    if (!file_exists($this->galleryPathRelSvm.PREFERENCES_PATH))
    {
      // no error here since it is confusing when importing galleries
      return false;
    }    
    $prefs = @file_get_contents($this->galleryPathRelSvm.PREFERENCES_PATH);
    if ($prefs === false)
    {
      trigger_error('cannot read preferences file '.$this->galleryPathRelSvm.PREFERENCES_PATH.', default values will be used', E_USER_NOTICE);
      return false;
    }
    $prefs = unserialize($prefs);
    if ($prefs === false)
    {
      trigger_error('preferences file '.$this->galleryPathRelSvm.PREFERENCES_PATH.', is corrupt; default values will be used', E_USER_NOTICE);
      return false;
    }
    return $prefs;         
  }
  
  /**
  * Read xml file and parse into structure
  *
  * @access private
  * @returns array of $vals and $index created by xml_parse_into_struct
  * @param string path to xml file
  */
  function parseXml($xmlPath)
  {
    $rebuildMessage = isset($_GET['rebuild']) ? '' : '<br />Rebuild may be necessary.';
    if (!file_exists($xmlPath))
    {
      trigger_error('cannot find gallery xml file ('.$xmlPath.'), default settings will be used.'.$rebuildMessage, E_USER_NOTICE);
      return false;
    }
    $xmlSource = @file_get_contents($xmlPath);
    if ($xmlSource === false)
    {
      trigger_error('cannot read gallery xml file ('.$xmlPath.'), default settings will be used.'.$rebuildMessage, E_USER_NOTICE);
      return false;
    }
    $check = new XML_check;
    if(!$check->check_string($xmlSource))
    {
      trigger_error('XML in '.$xmlPath.' is not well-formed. '.$check->get_full_error().'. '.$rebuildMessage, E_USER_NOTICE);
      return false;
    }
    $p = xml_parser_create('UTF-8');
    xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($p, XML_OPTION_SKIP_WHITE, 0);
    if (0 === @xml_parse_into_struct($p, $xmlSource, $vals, $index))
    {
      trigger_error('failed to parse the xml file ('.$xmlSource.') default settings will be used. '.$rebuildMessage, E_USER_NOTICE);
      return false;
    }       
    xml_parser_free($p);
    return array('vals'=>$vals, 'index'=>$index);
  }
  
  /**
  * test for jpg
  *
  * Note that preg_match_all returns a number and false for badly formed utf-8
  * version including swf is (0 == preg_match_all('(.*\.((jpe?g)|(swf)|(png)|(gif))$)ui', $fileName, $matches))
  *
  * @return boolean true if filename ends in jpg, jpeg, png or gif (case insensitive)
  * @parameter string file name
  * @access private
  */
  function isImage($fileName)
  {
    return (0 != preg_match_all('(.*\.((jpe?g)|(png)|(gif))$)ui', $fileName, $matches));
  }
  
  /**
  * creates new thumbnails
  *
  * @access private
  * @param string path with trailing separator
  * @param string path with trailing separator
  * @param array file names and captions
  * @param boolean overwrite existing thumbnails (only relevant for rebuild)
  * @return integer number of thumbnails created or zero on error
  */
  function makeThumbs($thumbPath, $imageObjects, $overwrite = true)
  {
    $thumbCount = 0;
    $memoryLimit = (ini_get('memory_limit') == '') ? MEMORY_LIMIT_FALLBACK : ini_get('memory_limit');
    $maxImageBytes = (MEMORY_LIMIT == 0) ? $this->getBytes($memoryLimit) : MEMORY_LIMIT * pow(2,20);
    $thumbDir = $this->trimSeparator($thumbPath);
    if (!is_dir($thumbDir))
    {
    // Note: mkdir($this->thumbDir, 0777) will not work reliably because of umask (www.php.net/umask)
      if(@!mkdir($thumbDir))
      {
        trigger_error('the thumbnail directory '.$thumbDir.' cannot be created', E_USER_WARNING);
        return 0;
      }
      if (@!chmod($thumbDir, THUMB_DIR_MODE))
      {
        trigger_error('the thumbnail directory '.$thumbDir.' permission may be incorrect &ndash; please see user manual', E_USER_NOTICE);
        return 0;
      }
    }
    $gdVersion = $this->getGDversion();
    if (version_compare($gdVersion, '2.0', '<'))
    {
      trigger_error('the GD imaging library was not found on this server or it is an old version that does not support jpeg images. Thumbnails will not be created. Either upgrade to a later version of GD or create the thumbnails yourself in a graphics application such as Photoshop.', E_USER_NOTICE);
      return 0;
    }
    foreach ($imageObjects as $key=>$imageObject)
    {
      $fileName = $imageObject->getImageFileName();
      $image = $this->galleryPathRelSvm.$imageObject->getImagePathRelGallery();
      $thumb = $thumbPath.$fileName;
      if (@file_exists($thumb) && !$overwrite) {continue;}
      if (@!file_exists($image))
      {
        trigger_error('image '.$image.' cannot be found', E_USER_NOTICE);
        continue;
      }
      if ($this->createThumb($image, $thumb, $this->thumbWidth, $this->thumbHeight, $this->thumbQuality, $maxImageBytes))
      {
        $thumbCount ++;
      }
      else
      {
        trigger_error('Thumbnail for '.$fileName.' could not be created', E_USER_NOTICE);
      }
    }
    return $thumbCount;
  }
  
  /**
  * function createThumb creates and saves one thumbnail image.
  *
  * @access private
  * @return boolean success
  * @param string $filePath path to source image
  * @param string $thumbPath path to new thumbnail
  */

  function createThumb($filePath, $thumbPath, $thumbWidth, $thumbHeight, $thumbQuality, $maxImageBytes)
  {
    if($thumbWidth <= 0) $thumbWidth = THUMB_DISPLAY_SIZE;
    if($thumbHeight <= 0) $thumbHeight = THUMB_DISPLAY_SIZE;
    $success = false;
    $dimensions = @getimagesize($filePath);
    if ($dimensions === false)
    {
      trigger_error('cannot calculate size of image '.$filePath, E_USER_NOTICE);
    	return false;
    }
    // $imageInfo['channels'] is not set for png images so just guess at 3
    $channels = 3;
    $memoryNeeded = Round(($dimensions[0] * $dimensions[1] * $dimensions['bits'] * $channels / 8 + Pow(2, 16)) * MEMORY_SAFETY_FACTOR);
    if ($memoryNeeded > $maxImageBytes)
    {
    	trigger_error('image '.$filePath.' is too large to create a thumbnail', E_USER_NOTICE);
    	return false;
    }
  	$imageWidth		= $dimensions[0];
  	$imageHeight		= $dimensions[1];
  	if ($dimensions[0] == 0 || $dimensions[1] == 0)
    {
      trigger_error('zero width or height for '.$filePath, E_USER_NOTICE);
      return false;
    }
    $imageAspect = $dimensions[1]/$dimensions[0];
    $thumbAspect = ($thumbWidth == 0) ? 1 : $thumbHeight/$thumbWidth;
    if ($imageAspect >= $thumbAspect)
    {
      // thumbnail is full-width
      $cropWidth = $imageWidth;
      $cropHeight = $imageWidth * $thumbAspect;
      $deltaX = 0;
      $deltaY = ($imageHeight - $cropHeight)/2;
    }
    else
    {
      // thumbnail is full-height
      $cropWidth = $imageHeight / $thumbAspect;
      $cropHeight = $imageHeight;
      $deltaX = ($imageWidth - $cropWidth)/2;
      $deltaY = 0;
    }
    // get image identifier for source image
    switch($dimensions[2])
    {
      case IMAGETYPE_GIF :
        $imageSrc  = @imagecreatefromgif($filePath);
        break;
      case IMAGETYPE_JPEG :
        $imageSrc = @imagecreatefromjpeg($filePath);
        break;
      case IMAGETYPE_PNG :
        $imageSrc = @imagecreatefrompng($filePath);
        break;
      default :
        trigger_error('unidentified image type '.$filePath, E_USER_NOTICE);
        return false;
    }
    if ($imageSrc === false)
    {
      trigger_error('could not get image identifier for '.$filePath, E_USER_NOTICE);
      return false;
    }
    // Create an empty thumbnail image. 
    $imageDest = @imagecreatetruecolor($thumbWidth, $thumbHeight);
    if ($imageDest === false)
    {
      trigger_error('could not create true color image', E_USER_NOTICE);
      return false;
    }
    if(!$success = @imagecopyresampled($imageDest, $imageSrc, 0, 0, $deltaX, $deltaY, $thumbWidth, $thumbHeight, $cropWidth, $cropHeight))
    {
      trigger_error('could not create thumbnail using imagecopyresampled', E_USER_NOTICE);
      @imagedestroy($imageSrc);
		  @imagedestroy($imageDest);
      return false;
    }
    // save the thumbnail image into a file.
		if (!$success = @imagejpeg($imageDest, $thumbPath, $thumbQuality))
    {
      trigger_error('could not save thumbnail', E_USER_NOTICE);
    }
		// Delete both image resources.
		@imagedestroy($imageSrc);
		@imagedestroy($imageDest);
    unset ($imageSrc, $imageDest);
  	return $success;
  }

 /**
  * Get which version of GD is installed, if any.
  *
  * @access private
  * @return string version vector or '0' if GD not installed
  */
  function getGdVersion()
  {
    if (! extension_loaded('gd')) { return '0'; }
    // Use the gd_info() function if possible.
    if (function_exists('gd_info'))
    {
      $versionInfo = gd_info();
      preg_match("/[\d\.]+/", $versionInfo['GD Version'], $matches);
      return $matches[0];
    }
    // If phpinfo() is disabled return false...
    if (preg_match('/phpinfo/', ini_get('disable_functions')))
    {
      return '0';
    }
    // ...otherwise use phpinfo().
    ob_start();
    @phpinfo(8);
    $moduleInfo = ob_get_contents();
    ob_end_clean();
    if (preg_match("/\bgd\s+version\b[^\d\n\r]+?([\d\.]+)/i", $moduleInfo,$matches))
    {
      $gdVersion = $matches[1];
    }
    else
    {
      $gdVersion = '0';
    }
    return $gdVersion;
  }
  
  /**
  * Get maximum byte size to plug into java uploader
  *
  * @access public
  * @return integer file size in MBytes
  */
  function getMaxUploadMBytes()
  {
    if (POST_MAX_SIZE !== 0) return POST_MAX_SIZE;
    $postMaxSize = (ini_get('post_max_size') == '') ? POST_MAX_SIZE_FALLBACK : ini_get('post_max_size');
    $uploadMaxFilesize = (ini_get('upload_max_filesize') == '') ? UPLOAD_MAX_FILESIZE_FALLBACK : ini_get('upload_max_filesize');
    $postMaxMBytes = intval($this->getBytes($postMaxSize)/pow(2,20));
    $uploadMaxMBytes = intval($this->getBytes($uploadMaxFilesize)/pow(2,20));
    return min($postMaxMBytes, $uploadMaxMBytes);
  }

 /**
  * Convert ini-style G, M, kbytes to bytes
  * Note that switch statement drops through without breaks
  *
  * @access private
  * @return integer bytes
  * @param string
  */
  function getBytes($val)
  {
    $val = trim($val);
    $last = strtolower($val{strlen($val)-1});
    switch($last)
    {
      case 'g':
        $val *= 1024;
      case 'm':
        $val *= 1024;
      case 'k':
        $val *= 1024;
    }
    return $val;
  }
  
  /**
   * return a properly formatted hex color string
   *
   * @access private
   * @return string
   * @param string containing hex color
   * @param integer required length of hex number in characters
   */
   function cleanHex($hex, $length = 6)
   {
     $hex = trim(strtolower($hex));
     $hex = ltrim($hex, '#');
     $hex = str_replace('0x', '', $hex);
     return '0x'.str_pad(strtoupper(dechex(hexdec(  substr(trim($hex), 0, $length)  ))), $length, '0', STR_PAD_LEFT);
   }
  
  /**
  * handle correctly formatted path with trailing slash
  *
  * @return string path without trailing slash
  * @access private
  * @parameter string directory path
  */
  function trimSeparator($path)
  {
    return rtrim($path, '\\/');
  }
  
  /**
   * Get path relative to gallery unix or windows style
   *
   * @access public
   * @return string directory (with trailing slash) or file, relative to gallery
   * @param string relative url, absolute url, or http url
   */
   function getPathRelGallery($url)
   {
     $separators = array('/', '\\');
     // empty string
     if ($url == '') return '';
     $pathParts = $this->pathParser->parse($url);
     // absolute path
     if ($pathParts['root'] == '/')
     {
       $relPath = $this->pathParser->findRelativePath($this->getRootUrl(), $url);
       return str_replace($separators, DIRECTORY_SEPARATOR, $relPath);
     }
     // http
     if ($pathParts['scheme'] !== '')
     {
       $relPath = $this->pathParser->findRelativePath($this->getRootUrl(), '/'.$pathParts['dir'].$pathParts['file']);
       return str_replace($separators, DIRECTORY_SEPARATOR, $relPath);
     }
     // file name only
     if ($pathParts['dir'] === '')
     {
       return $url;
     }
     // relative path
     $relPath = $pathParts['dir'].$pathParts['file'];
     return str_replace($separators, DIRECTORY_SEPARATOR, $relPath);
   }

 /**
  * Remove image from this->imageData, delete image and thumbnail
  *
  * @access private
  * @return true on success
  * @param array of images to delete in format filename=>'on'
  */
  function deleteImages($delFiles)
  {
    $imageObjects = $this->imageObjects;
    $relThumbPath = $this->getThumbDirPathRelSvm();
    foreach ($delFiles as $key=>$checked)
    {
      if (array_key_exists($key, $imageObjects))
      {
        $filePath = $this->galleryPathRelSvm.$imageObjects[$key]->getImagePathRelGallery();
        $fileName = basename($filePath);
        //$file = $imageData[$key]['fileName'];
        unset ($imageObjects[$key]);
        if (file_exists($filePath))
        {
          if(@!unlink($filePath))
          {
            trigger_error('unable to delete image file '.$filePath, E_USER_NOTICE);
          }
        }
        if (file_exists($relThumbPath.$fileName))
        {
          if (@!unlink($relThumbPath.$fileName))
          {
            trigger_error('unable to delete thumbnail '.$fileName, E_USER_NOTICE);
          }
        }
      }
    }
    // re-index
    $this->imageObjects = array_values($imageObjects);
  }
   
 /** Check path for characters not acceptable in a url
  *
  * Based on http://www.designerstalk.com/forums/programming/17819-php-remove-punctuation.html
  * Regular expression modified by adding /_-. characters to acceptable set
  * Some versions of php are compiled without utf-8 support for posix named classes see
  * http://framework.zend.com/fisheye/browse/Zend_Framework/trunk/library/Zend/Filter/Alnum.php?r=5468
  * @access public
  * @return integer - 0 if no invalid characters otherwise 1
  * @param string to be checked
  */
  function checkPath()
  {
    $path = trim(str_replace('\\', '/', $this->getRootUrl()));
    // Checks if PCRE is compiled with UTF-8 and Unicode support
    if (!@preg_match('/\pL/u', 'a'))
    {
      // POSIX named classes are not supported, use alternative a-zA-Z0-9 match
      $pattern = '#[^a-zA-Z0-9/~_.\-]+#';
    }
    else
    {
    // Unicode safe filter for the value
    $pattern = '#[^\p{L}\p{N}/~_.\-]+#u';
    }
    return preg_match($pattern, $path);
  }
  
 /**
  * Public interface to sortImages function
  *
  * @access public
  * @return boolean success
  * @param string sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  */
  function sortGalleryImages()
  {
    $sortOrder = $this->getSortOrder(); 
    $this->imageObjects = $this->sortImages($this->imageObjects, $sortOrder);
    return true;
  }
  
 /**
  * Sort images
  *
  * @access private
  * @return array sorted image data;
  * @param array to sort
  * @param string sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  */
  function sortImages($imageObjects, $sortOrder = 'rdate')
  {
    if ($sortOrder == 'dragndrop') return $imageObjects;
    $relImagePath = $this->getImageDirPathRelSvm();
    $fileName = array();
    $fileMTime = array();
    foreach ($imageObjects as $key => $row)
    {
      $fileName[$key]  = $row->getImageFileName();
      $fileMTime[$key] =filemtime($relImagePath.$fileName[$key]);
    }
    switch($sortOrder)
    {
      case 'alpha':
        array_multisort($fileName, SORT_ASC, $imageObjects);
      break;
      case 'ralpha':
        array_multisort($fileName, SORT_DESC, $imageObjects);
      break;
      case 'date':
        array_multisort($fileMTime, SORT_ASC, SORT_NUMERIC, $imageObjects);
      break;
      case 'rdate':
        array_multisort($fileMTime, SORT_DESC, SORT_NUMERIC, $imageObjects);
      break;
    }
    return $imageObjects;
   }

 /**
  * Update imageData from sort page
  *
  * @access public
  * @return array
  * @param sort output from sort screen $_POST
  */
  function setDragnDrop($sortOutput)
  {
    parse_str($sortOutput, $temp);
    $newIndex = $temp['sortable'];
    foreach ($this->imageObjects as $key=>$imageObject)
    {
      $newImageObjects[$key] = $this->imageObjects[$newIndex[$key]];
    }
    $this->imageObjects = $newImageObjects;
  }

 /** Save thumbnail for galleries index page
  *
  * @access private
  * @return boolean success
  */
  function saveIndexThumb()
  {
    $memoryLimit = (ini_get('memory_limit') == '') ? MEMORY_LIMIT_FALLBACK : ini_get('memory_limit');
    $maxImageBytes = (MEMORY_LIMIT == 0) ? $this->getBytes($memoryLimit) : MEMORY_LIMIT * pow(2,20);
    $relThumbPath = $this->getThumbDirPathRelSvm();
    // need to check why the older array version checked that filename was set
    $thumbCache = rtrim(THUMB_CACHE, '\\/');
    $destThumb = $thumbCache.DIRECTORY_SEPARATOR.INDEX_THUMB_PREFIX.$this->galleryRef.'.jpg';
    if (!file_exists($thumbCache))
    {
      if (!@mkdir($thumbCache))
      {
        trigger_error('unable to create thumbnail cache for index page ', E_USER_NOTICE);
        return false;
      }
    }
    if (!isset($this->imageObjects[0]))
    {
      if(@!copy(IMAGE_SRC.'nothumb.gif', $destThumb))
      {
        trigger_error('unable to update thumbnail cache for index page', E_USER_NOTICE);
      }
      return false;
    }
    $fileName = $this->imageObjects[0]->getImageFileName();
    $image = $this->galleryPathRelSvm.$this->imageObjects[0]->getImagePathRelGallery();
    if (!file_exists($image))
    {
      if(@!copy(IMAGE_SRC.'nothumb.gif', $destThumb))
      {
        trigger_error('unable to update thumbnail cache for index page', E_USER_NOTICE);
      }
      return false;
    }
    if (!$this->createThumb($image, $destThumb, INDEX_THUMB_WIDTH, INDEX_THUMB_HEIGHT, $this->thumbQuality, $maxImageBytes))
    {
      if(@!copy(IMAGE_SRC.'nothumb.gif', $destThumb))
      {
        trigger_error('unable to update thumbnail cache for index page', E_USER_NOTICE);
      }
      return false;
    }
    return true;
  }
}
?>
