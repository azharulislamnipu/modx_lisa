<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("req_cmt", true, "text", "", "", "", "Please enteryour comment.");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("webmaster@sigmapiphi.org");
  $emailObj->setTo("TAC@sigmapiphi.org");
  $emailObj->setCC("");
  $emailObj->setBCC("");
  $emailObj->setSubject("Feedback Submitted");
  //WriteContent method
  $emailObj->setContent("<p>A feedback item has been submitted.</p>\n\n<p><a href=\"http://www.sigmapiphi.org/home/display_feedback.php?req_id={req_id}\">Feedback Item</a></p>");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

// Make an insert transaction instance
$ins_spp_request = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_request);
// Register triggers
$ins_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_request->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
$ins_spp_request->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_request->setTable("spp_request");
$ins_spp_request->addColumn("req_cmt", "STRING_TYPE", "POST", "req_cmt");
$ins_spp_request->addColumn("feedback_page", "STRING_TYPE", "POST", "feedback_page", "{SERVER.HTTP_REFERER}");
$ins_spp_request->addColumn("req_date", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_request->addColumn("type_id", "NUMERIC_TYPE", "POST", "type_id", "7");
$ins_spp_request->addColumn("req_by", "NUMERIC_TYPE", "POST", "req_by", "{SESSION.webInternalKey}");
$ins_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_request = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_request);
// Register triggers
$upd_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_request->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
$upd_spp_request->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$upd_spp_request->setTable("spp_request");
$upd_spp_request->addColumn("req_cmt", "STRING_TYPE", "POST", "req_cmt");
$upd_spp_request->addColumn("feedback_page", "STRING_TYPE", "POST", "feedback_page");
$upd_spp_request->addColumn("req_date", "DATE_TYPE", "CURRVAL", "");
$upd_spp_request->addColumn("type_id", "NUMERIC_TYPE", "POST", "type_id");
$upd_spp_request->addColumn("req_by", "NUMERIC_TYPE", "POST", "req_by");
$upd_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE", "GET", "req_id");

// Make an instance of the transaction object
$del_spp_request = new tNG_multipleDelete($conn_sigma_modx);
$tNGs->addTransaction($del_spp_request);
// Register triggers
$del_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/back.php");
// Add columns
$del_spp_request->setTable("spp_request");
$del_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE", "GET", "req_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_request = $tNGs->getRecordset("spp_request");
$row_rsspp_request = mysql_fetch_assoc($rsspp_request);
$totalRows_rsspp_request = mysql_num_rows($rsspp_request);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<style type="text/css">
<!--
.style1 {font-size: 14px}
-->
</style>
<!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
    <h1 class="yellow">ARCHON FEEDBACK</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_request > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php } 
// endif Conditional region1
?>
            <table cellpadding="2" cellspacing="0" class="KT_tngtable">
              <tr>
                <td colspan="2"><span class="style1">Please enter your feedback below.</span></td>
              </tr>
              <tr>
                <td class="KT_th"><label for="req_cmt_<?php echo $cnt1; ?>">COMMENT:</label></td>
                <td><textarea name="req_cmt_<?php echo $cnt1; ?>" id="req_cmt_<?php echo $cnt1; ?>" cols="80" rows="12"><?php echo KT_escapeAttribute($row_rsspp_request['req_cmt']); ?></textarea>
                    <?php echo $tNGs->displayFieldHint("req_cmt");?> <?php echo $tNGs->displayFieldError("spp_request", "req_cmt", $cnt1); ?> </td>
              </tr>
              <tr>
                <td class="KT_th"><label for="req_date_<?php echo $cnt1; ?>">DATE SUBMITTED:</label></td>
                <td><?php echo KT_formatDate($row_rsspp_request['req_date']); ?></td>
              </tr>
              <tr>
                <td class="KT_th">SUBMITTED BY:</td>
                <td><strong><?php echo $_SESSION['webFullname']; ?></strong></td>
              </tr>
            </table>
          <input type="hidden" name="feedback_page_<?php echo $cnt1; ?>" id="feedback_page_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_SERVER['HTTP_REFERER']); ?>" />
<input type="hidden" name="kt_pk_spp_request_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_request['kt_pk_spp_request']); ?>" />
          <input type="hidden" name="type_id_<?php echo $cnt1; ?>" id="type_id_<?php echo $cnt1; ?>" value="7" />
          <input type="hidden" name="req_by_<?php echo $cnt1; ?>" id="req_by_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($_SESSION['webInternalKey']); ?>" />
          <?php } while ($row_rsspp_request = mysql_fetch_assoc($rsspp_request)); ?>
        <div class="KT_bottombuttons">
          <div>
            <?php 
      // Show IF Conditional region1
      if (@$_GET['req_id'] == "") {
      ?>
              <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
              <?php 
      // else Conditional region1
      } else { ?>
              <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
              <?php }
      // endif Conditional region1
      ?>
            <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/back.php')" />
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  </div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
