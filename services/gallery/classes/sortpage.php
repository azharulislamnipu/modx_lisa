<?php
 /**
  * Part of Airtight Interactive gallery management package.
  *
  * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
  * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
  *
  * @package svManager
  * @author Jack Hardie {@link http://www.jhardie.com}
  * @version 1.6.3 build 091231
  * @copyright Copyright (c) 2007 - 2009, Airtight Interactive
  */
 
 /**
  * Images page
  *
  * @package svManager
  */
  class SortPage extends Page
  {
   /**
    * @var string overrides some of html in parent
    */
    var $customHeadHtml = '
    <script type="text/javascript" src="scripts/prototype.js"></script>
    <script type="text/javascript" src="scripts/scriptaculous/scriptaculous.js?load=effects,dragdrop"></script>
    <script type="text/javascript" src="scripts/sort.js"></script>
    ';
   /**
    * constructs SortPage class
    * @param string gallery type
    */
    function SortPage($galleryType)
    {
      parent::Page('svManager &ndash; sort', 'sort', $galleryType);
    }

   /**
    * get html for images page
    * Note that some class names are significant for manager.js
    *
    * @access public
    * @return string html
    * @param string gallery reference number
    * @param array image data
    */
    function getSortHtml(&$gallery)
    {
      $imageObjects = $gallery->getImageObjects();
      $sortOrder = $gallery->getSortOrder();
      $options = array('dragndrop'=>'drag&rsquo;n&rsquo;drop', 'alpha'=>'file name A&hellip;z', 'ralpha'=>'file name Z&hellip;a', 'date'=>'oldest first', 'rdate'=>'newest first');
      $sortOrderHtml = $this->htmlSelect('sortOrder', 'sortorder', '8', $options, $sortOrder);
      $galleryRef = $gallery->getGalleryRef();
      $html = '      <form class="public" action="'.$_SERVER['PHP_SELF'].'" method="post">
        <div id="sortable">
';
      $i = 0;
      foreach ($imageObjects as $key => $imageObject)
      {
        $fileName = $imageObject->getImageFileName();
        if (file_exists($gallery->getThumbDirPathRelSvm().$fileName))
        {
        	$src = $gallery->getThumbDirUrlRelSvm().$fileName;
        }
        else
        {
        	$src = rtrim(IMAGE_SRC, '\\/').'/nothumb.gif';
        }
        $html .= '          <img src="'.$src.'" id="image_'.(string)$i.'" alt="thumbnail" height="'.THUMB_DISPLAY_SIZE.'"/>
';
        $i ++;
      }
      $html .= '        </div>
      <div id="submitinputs">
        <input type="hidden" name="sortoutput" id="sortoutput" value="nosort"/><input type="hidden" name="sortsubmitted" value="true" /><label for="sortorder">Sort order</label>'.$sortOrderHtml.'<input class="formbutton" type="submit" value="Update" name="submit" id="submit" />     
    </div>
  </form>';
      return $html;
    }
  }
?>
