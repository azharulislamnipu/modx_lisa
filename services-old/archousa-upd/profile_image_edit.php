<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_ImageUpload trigger
//remove this line if you want to edit the code by hand 
function Trigger_ImageUpload(&$tNG) {
  $uploadObj = new tNG_ImageUpload($tNG);
  $uploadObj->setFormFieldName("IMAGE");
  $uploadObj->setDbFieldName("IMAGE");
  $uploadObj->setFolder("/opt/lampp/htdocs/modx_lisa/home/assets/images/archousa/");
  $uploadObj->setResize("true", 200, 0);
  $uploadObj->setMaxSize(1500);
  $uploadObj->setAllowedExtensions("gif, jpg, jpe, jpeg, png");
  $uploadObj->setRename("custom");
  $uploadObj->setRenameRule("{ARCHOUSAID}.{KT_name}.{KT_ext}");
  return $uploadObj->Execute();
}
//end Trigger_ImageUpload trigger

// Make an update transaction instance
$upd_spp_archousa = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archousa);
// Register triggers
$upd_spp_archousa->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archousa->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archousa->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-profile.php");
$upd_spp_archousa->registerTrigger("AFTER", "Trigger_ImageUpload", 97);
// Add columns
$upd_spp_archousa->setTable("spp_archousa");
$upd_spp_archousa->addColumn("IMAGE", "FILE_TYPE", "FILES", "IMAGE");
$upd_spp_archousa->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_archousa->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "LASTUPDATED");
$upd_spp_archousa->addColumn("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");
$upd_spp_archousa->setPrimaryKey("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archousa = $tNGs->getRecordset("spp_archousa");
$row_rsspp_archousa = mysql_fetch_assoc($rsspp_archousa);
$totalRows_rsspp_archousa = mysql_num_rows($rsspp_archousa);
?><!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --><!DOCTYPE html>
<!--
 ____________________________________________________________
|                                                            |
|      DATE + 2006.11.14                                     |
| COPYRIGHT + Sigma Pi Phi 2006.  All Rights Reserved.       |
|____________________________________________________________|

-->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-meta.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-css.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-javascript.php"); ?>
	<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/head-links.php"); ?>

    <!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>

<body class="home blue-bar [!colorPalette? &palette_default=`[*palette_default*]` &palette_loggedin=`[*palette_loggedin*]` !]">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/main-nav.php"); ?>
			<div class="width">
			    <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/global-logo-tagline.php"); ?>
			</div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
	<!-- large image slider content toggle end-->

    <!-- main content body start -->	
	<div id="main" role="main">
        <div class="width padding">
			<!-- shows the content, pagetitle, breadcrumb and sidebar -->
			<!-- InstanceBeginEditable name="pagetitle" -->
                            <section class="span-12 margin-bottom-medium">
                            <div class="inner">
                                     <h2>Add/Update Your Profile Image</h2>
                                     <ol class="breadcrumbs" style="margin-top:-18px;">
                                      <li><a class="B_homeCrumb" href="/home/archon-home.php" title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                                      <li><span class="B_currentCrumb">Administration</span></li>
                                    </ol>
                     <div>
             </section>
<!-- InstanceEndEditable -->
             
             <!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" enctype="multipart/form-data">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="IMAGE">IMAGE:</label></td>
        <td><input type="file" name="IMAGE" id="IMAGE" size="32" />
            <?php echo $tNGs->displayFieldError("spp_archousa", "IMAGE"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Upload Image" /> 
        <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onClick="return UNI_navigateCancel(event, '/home/archousa-profile.php')" />
        </td>
      </tr>
    </table>
    <input type="hidden" name="UPDATEDBY" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['UPDATEDBY']); ?>" />
    <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo KT_formatDate($row_rsspp_archousa['LASTUPDATED']); ?>" />
    <input type="hidden" name="ARCHOUSAID" id="ARCHOUSAID" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ARCHOUSAID']); ?>" />
  </form>


<!-- InstanceEndEditable -->
            <!-- main content body end -->	
	
	<!-- third section event and articles start -->
  	 <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
	<!-- third section event and articles end -->
	
    <footer class="width padding" style="border-top: 10px #52B609 solid;">
        <!-- 3 text content blocks -->
        <div class="row grid-12">
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-content-default.php"); ?>
      </div>
    </footer>
	
   	 <?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/footer-scripts.php"); ?>
</body>
<!-- InstanceEnd --></html>
