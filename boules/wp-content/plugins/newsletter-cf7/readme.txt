=== Contact Form 7 for Newsletter ===

== Changelog ==

= 4.0.0 =

* Added list support
* Compatible with Newsletter 4

= 1.1.0 =

* Added name and last name management
* Added Newsletter 4 menu support

= 1.0.0 =

* First release

