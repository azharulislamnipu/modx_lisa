<?php get_header(); ?>

<div id="content">
    <div class="padder">

    <?php
        do_action( 'sigma_before_blog_home' );
        $bouleName = get_bloginfo( 'name' );
        $bouleUrl = get_bloginfo( 'url' );
		$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    ?>


	<?php if( $page === 1 && !is_multisite_primary() && !is_regional_site() ) :
		if(is_page_content('history')): ?>
			<!-- Boule history -->
		    <h2 class="yellow color"><?php echo $bouleName . ' ' .  __('History', 'archon-network')?></h2>
		    <div class="contentBlock marginBottomLarge">
		        <?php get_page_content_by_slug( 'history' ) ?>
		    </div>
		<?php endif;
	endif;

	// Recent posts
	locate_template( array( 'home_aggregate.php' ), true );

	do_action( 'sigma_after_blog_home' ) ?>

    </div>
</div>

<?php locate_template( array( 'sidebar.php' ), true ) ?>

<?php get_footer(); ?>