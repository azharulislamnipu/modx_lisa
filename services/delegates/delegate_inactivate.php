<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an update transaction instance
$upd_spp_delegates = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_delegates);
// Register triggers
$upd_spp_delegates->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_delegates->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);

// Where should this page return to
$returnTo = isset($_POST['returnTo']) ? $_POST['returnTo'] : '/home/delegates_dashboard.php';
$upd_spp_delegates->registerTrigger("END", "Trigger_Default_Redirect", 99, $returnTo);

// Add columns
$upd_spp_delegates->setTable("spp_delegates");
$upd_spp_delegates->addColumn("delegate_status", "STRING_TYPE", "POST", "delegate_status");
$upd_spp_delegates->addColumn("APPTBY", "STRING_TYPE", "POST", "APPTBY");
$upd_spp_delegates->addColumn("APPTDATE", "DATE_TYPE", "VALUE", "{NOW_DATE}");
$upd_spp_delegates->setPrimaryKey("delegate_id", "NUMERIC_TYPE", "POST", "delegate_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_delegates = $tNGs->getRecordset("spp_delegates");
$row_rsspp_delegates = mysql_fetch_assoc($rsspp_delegates);
$totalRows_rsspp_delegates = mysql_num_rows($rsspp_delegates);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<div class="contentBlock">
  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Update record" />
        </td>
      </tr>
    </table>
     <input type="hidden" name="delegate_status" id="delegate_status" value="<?php echo KT_escapeAttribute($_POST['delegate_status']); ?>" />
    <input type="hidden" name="APPTBY" id="APPTBY" value="<?php echo KT_escapeAttribute($_SESSION['webShortname']); ?>" />
    <input type="hidden" name="APPTDATE" id="APPTDATE" value="<?php echo KT_formatDate($row_rsspp_delegates['APPTDATE']); ?>" />
  </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
