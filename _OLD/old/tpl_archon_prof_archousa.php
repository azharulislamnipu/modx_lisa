<div class="span-12 archons-info">
    <h3>Archousa Information</h3>

    <div class="span-4 width float-left archousa_profile_img">
        [+phx:if=`[+IMAGE+]`:is=``:then=`
        <img id="profile_img" src="/home/assets/images/icons/archon-default.png" alt="Default Archon Image Not Found picture."/>
        `:else=`
        <img id="profile_img" src="/home/assets/images/archousa/[+IMAGE+]" alt="Image of [+FIRSTNAME+] [+MIDDLEINITIAL+] [+LASTNAME+] [+SUFFIX+]"/>
        `+]
    </div>


    <div class="span-8 inner">
        <p><span id="ar_fname">[+FIRSTNAME+]</span> [+MIDDLEINITIAL+] [+LASTNAME+] [+SUFFIX+]</p>
        <p class="font-small">
            [[LDataDisplay? &table=`spp_prof` &display=`1` &tpl=`tpl_archon_prof_archousa_company` &filter=`ARCHOUSAID,[[getPlaceholder? &name=`archousaid`]],1`]]
        </p>
        <p class="btn_p"><a class="button small_button" id="view_archousa" href="/home/personal-profile.php">View</a>
        </p>
    </div>
</div>