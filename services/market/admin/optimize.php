<?php
require_once('../includes/admin/access.php');
require_once('../includes/admin/header.php');
?>

<h3>Tools to Optimize</h3>

<?php
if (cG("action")=="db"){
    $result  = $ocdb->query('SHOW TABLE STATUS FROM '. DB_NAME); 
    while ($row = mysql_fetch_array($result)) $tables[]=$row[0];  
    $tables=implode(", ",$tables); //echo $tables;
    $ocdb->query('OPTIMIZE TABLE '.$tables);
    echo "<h3>Optimized all the tables found in the database: $tables</h3>";
}
elseif (cG("action")=="cache") {
	deleteCache();
	echo "<h3>".T_CACHE_DELETED."</h3>";
}

elseif (cG("action")=="notconfirmed") {
	echo "<h3>".T_ERASE_OLD_ADS."</h3>";
	$query="SELECT idPost FROM ".TABLE_PREFIX."posts where isConfirmed=0  and TIMESTAMPDIFF(DAY,insertDate,now())>=3";
	$result =	$ocdb->query($query);
	while ($row=mysql_fetch_assoc($result))//delete posible images and folders
	{	
		$idPost=$row['idPost'];
		removeRessource(IMG_UPLOAD_DIR.$idPost);//delete images and folder for the ad
		echo T_DELETED." $idPost<br />";
	}
	//delete from db
	$ocdb->delete(TABLE_PREFIX."posts","isConfirmed=0  and TIMESTAMPDIFF(DAY,insertDate,now())>=3");		
}

?>

<ul>
<li><a href="<?php echo SITE_URL;?>/admin/optimize.php?action=cache" onClick="return confirm('<?php echo T_SURE;?>?');"><?php echo T_CACHE_OLD;?> <?php echo round((time()-filemtime(CACHE_DATA_FILE))/60,1);?> <?php echo T_MINUTES?></a></li>
<li><a href="<?php echo SITE_URL;?>/admin/optimize.php?action=notconfirmed" onClick="return confirm('<?php echo T_SURE;?>?');"><?php echo T_ERASE_OLD_ADS;?></a></li>
<li><a href="<?php echo SITE_URL;?>/admin/optimize.php?action=db" onClick="return confirm('<?php echo T_SURE;?>?');">Optmize  all the tables found in the database now</a></lli>
<li><a href="<?php echo SITE_URL;?>/admin/phpinfo.php" >PHP Info</a></li>
</ul>
 
<?php
require_once('../includes/admin/footer.php');
?>
