<?php
/**
 * MODx API Library 
 * Use this api to connect to the MODx Content Managment System
 * 
 * @author: Raymond Irving, 2006
 * Example : to initialise modx in an autonomous php file 
 //create a local modx to test :-)
  if (!isset($modx)) 
  {
    $modx = new MODxAPI();
    $modx->connect();
    $modx->getSettings();  
  }
  $html = $modx->executeDocument(3);
  echo $html;

 */
 
 
$_api_path = dirname(__FILE__).'/';

global $database_type;
global $database_server;
global $database_user;
global $database_password;
global $database_connection_charset;
global $database_connection_method;
global $dbase;
global $table_prefix;

// include config file
include_once($_api_path.'manager/includes/config.inc.php');

// include modx parser file
include_once($base_path.'manager/includes/document.parser.class.inc.php');

class MODxAPI extends DocumentParser {

	function MODxAPI() {
	
		$this->startSession();
		parent::DocumentParser();
		
		// set some parser options
		$this->minParserPasses = 1;		// min number of parser recursive loops or passes
		$this->maxParserPasses = 10;	// max number of parser recursive loops or passes
		$this->dumpSQL = false;
		$this->dumpSnippets = false;
		
		// set start time
		$this->tstart = $this->getMicroTime();	// feed the parser the execution start time	

	}

	// execute parser and return results - To be finalized
	function executeDocument($docid = 0) {
		ob_start();
			ob_start();
				$tmp = $_REQUEST['id']; // save old id
				$_REQUEST['id'] = $docid;
				$this->executeParser();
		$html = ob_get_contents();
		ob_end_clean();
		$_REQUEST['id'] = $tmp; // restore old id
		return $html;		
	}
	
	// connect to MODx database - use $modx->db->query();
	function connect() {
		 $this->db->connect();
	}
	
	function startSession() {
		startCMSSession();
	}		
}

?>