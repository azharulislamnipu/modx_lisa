/**
 * Region map popup code
 */
var map = {
  
  popups : ['pPacific','pCentral','pWestern','pNortheast','pSoutheast'],
  
  // Display a popup after hiding all others
  showPopup : function(id){
   
    // Hide any other popups
    var elem;
    for(var i = 0; i < this.popups.length; i++){
      elem = document.getElementById(this.popups[i]);
      if(elem && elem.style)
        elem.style.visibility = "hidden";
    }  
    
    // Hide form search element
    var elemSearch = document.getElementById('searchForm');
    if(elemSearch && elemSearch.style)
      elemSearch.style.visibility = "hidden";    
    
    // Display requested popup
    elem = document.getElementById(id);
    if(elem && elem.style)
      elem.style.visibility = "visible";        
  },
  
  // Hides a popup
  hidePopup : function(id){  
    elem = document.getElementById(id);
    if(elem && elem.style)
      elem.style.visibility = "hidden";    
      
    // Redisplay hidden search elements
    var elemSearch = document.getElementById('searchForm');
    if(elemSearch && elemSearch.style)
      elemSearch.style.visibility = "visible";       
  }
} 