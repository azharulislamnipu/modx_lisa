/**
 * SimpleViewer admin package.
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
window.addEvent('load', function()
{
  if (!$('selectall')) return;
  $('selectall').addEvent('click', selectAllCopy);
});

window.addEvent('load', function()
{
  if (!$('selectalldescriptions')) return;
  $('selectalldescriptions').addEvent('click', selectAllDescriptionsCopy);
});

window.addEvent('load', function()
{
  if (!$('selectallurls')) return;
  $('selectallurls').addEvent('click', selectAllURLsCopy);
});

window.addEvent('load', function()
{
  if (!$('selectalldelete')) return;
  $('selectalldelete').addEvent('click', selectAllDeleteCopy);
});

window.addEvent('load', function()
{
  if (!$('selectallshowflip')) return;
  $('selectallshowflip').addEvent('click', selectAllShowFlipCopy);
});

window.addEvent('load', function()
{
  if (!$('copy')) return;
  $('copy').addEvent('click', copyTo);
});


function selectAllCopy()
{
  $$('input.selectcaption').set('checked', $('selectall').get('checked'));
}

function selectAllDescriptionsCopy()
{
  $$('input.selectdescription').set('checked', $('selectalldescriptions').get('checked'));
}
function selectAllURLsCopy()
{
  $$('input.selecturl').set('checked', $('selectallurls').get('checked'));
}
function selectAllDeleteCopy()
{
  $$('input.selectdelete').set('checked', $('selectalldelete').get('checked'));
}
function selectAllShowFlipCopy()
{
  $$('input.selectshowflip').set('checked', $('selectallshowflip').get('checked'));
}

function copyTo()
{
  copyText = $('copytext').value;
  var captions = $$('#imagelisttable td.ilcaption input');
  var checkboxes = $$('#imagelisttable td.ilselect input');
  var nCaptions = captions.length;
  for (var i=0; i<nCaptions; i++)
  {
    if (checkboxes[i].checked)
    {
      captions[i].value = copyText;
      checkboxes[i].checked = false;
    }
  }
  if ($('selectall')) $('selectall').checked = false;
  if ($('selectalldescriptions')) $('selectalldescriptions').checked = false;
  if ($('selectallurls')) $('selectallurls').checked = false;
}

function allowImageDelete(form)
{
  var checkboxes = $$('input.selectdelete');
  var deletions = 0;
  for (var i=0; i<checkboxes.length; i++)
  {
    if (checkboxes[i].checked) deletions++;
  }
  if (deletions < 1) return true;
  if (deletions == 1) return window.confirm('Deleting ' + deletions + ' image');
  return window.confirm('Deleting ' + deletions + ' images');
}
