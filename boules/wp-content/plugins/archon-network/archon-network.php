<?php
/*
Plugin Name: Archon Network
Plugin URI: http://sigmapiphi.org/boules
Description: Manages the global requirements of the Archon Network.  This allows sites to use their own themes while still enforcing access restrictions and network settings.
Version: 2.0
Author: Pat Heard
Author URI: http://fullahead.org
*/

/**
 * Initializes existing Boule's when the plugin is activated
 */
function plugin_init () {
	$blogs = get_blogs_of_user(1);
	foreach ($blogs AS $blog) {
		init_boule_site($blog->userblog_id);
	}
}
register_activation_hook(__FILE__, 'plugin_init');



/**
 * Adds the Arcon Network roles
 */
function add_user_roles ($blog_id) {

	add_role('archon', 'Archon', array(
	    'read' => true,
	    'read_private_pages' => true,
	    'read_private_posts' => true
	));

	add_role('archousa', 'Archousa', array(
	    'read' => true
	));

	remove_role('author');
	remove_role('contributor');
	remove_role('subscriber');

	update_option('default_role', 'archon');

	// Look for any users without a defined role and give them the default
	$users = get_users(array(
		'blog_id' => $blog_id
	));
	if (!empty($users)) {
		foreach ($users as $u) {
			if (empty($u->roles)) {
				$user = new WP_user($u->ID);
				$user->add_role('archon');
			}
		}
	}
}

/**
 * Insert default categories
 */
function add_default_categories () {
	$cats = array(
		// Post categories
		array('name' => 'Documents and Resources', 'slug' => 'documents-resources', 'taxonomy' => 'category'),
		array('name' => 'Notices', 'slug' => 'notices', 'taxonomy' => 'category'),
		array('name' => 'Press Releases', 'slug' => 'press-releases', 'taxonomy' => 'category'),
		array('name' => 'Social Action', 'slug' => 'social-action', 'taxonomy' => 'category'),
		array('name' => 'Featured', 'slug' => 'featured', 'taxonomy' => 'category'),

		// Business directory categories
		array('name' => 'Listing Category 1', 'slug' => 'listing-category-1', 'taxonomy' => 'wpbdm-category'),
		array('name' => 'Listing Category 2', 'slug' => 'listing-category-2', 'taxonomy' => 'wpbdm-category')
	);
	foreach($cats as $c) {
		if ( taxonomy_exists( $c['taxonomy'] ) && get_term_by( 'name', $c['name'], $c['taxonomy'] ) === false) {
			wp_insert_category(array(
				'cat_name' => $c['name'],
				'category_description' => '',
				'category_nicename' => $c['slug'],
				'category_parent' => '',
				'taxonomy' => $c['taxonomy']
			));
		}
	}
}

/**
 * Initializes a new Boule when it's created
 */
function init_boule_site ($blog_id) {
    switch_to_blog($blog_id);

	add_user_roles($blog_id);
	add_default_categories();

	// Update the default media sizes
	update_option('thumbnail_size_w', 150);
	update_option('thumbnail_size_h', 150);
	update_option('medium_size_w', 640);
	update_option('medium_size_h', 640);
	update_option('large_size_w', 1024);
	update_option('large_size_h', 1024);

    restore_current_blog();
}
add_action('wpmu_new_blog', 'init_boule_site');



/**
 * Prevent dashboard access for non-admin users
 */
function sigma_limit_dashboard() {
	if (!is_editor() && $_SERVER['DOING_AJAX'] != '/wp-admin/admin-ajax.php') {
		wp_redirect(network_site_url());
		exit;
	}
}
add_action('admin_init', 'sigma_limit_dashboard');


/**
 * Prevent password reset
 */
function sigma_password_reset($text) {
	return str_replace( array('Lost your password?', 'Lost your password'), '', trim($text, '?') );
}
add_filter( 'gettext', 'sigma_password_reset' );


/**
 * Only show the password fields to administrators
 */
add_filter( 'show_password_fields', 'is_administrator');

/**
 * Only allow administrators to change their passwords
 */
function is_allow_password_reset($is_allowed, $user_id = -1) {

	$is_allowed = false;
	if ($user_id !== -1) {
		$user = get_userdata($user_id);
		if ($user) {
			$is_allowed = $user->has_cap('manage_options');
		}
	}
	return $is_allowed;
}
add_filter( 'allow_password_reset', 'is_allow_password_reset',1 ,2);


/**
 * Disables access to sections of the WP admin dashboard
 */
function sigma_disable_dashboard_sections() {
    if(is_admin()) {
		if(!is_network_administrator()) {
            wp_die( 'You do not have access to this section of the ' . get_site_option('site_name') . ' dashboard.' );
        }
    }
}
$restricted = array('users', 'user-new', 'profile', 'themes', 'plugins', 'link-add', 'link-manager', 'tools', 'import', 'expert', 'ms-delete-site', 'options-general', 'options-writing', 'options-reading', 'options-discussion', 'options-media', 'options-permalinks');
foreach($restricted as $r) {
	add_action('load-' . $r . '.php', 'sigma_disable_dashboard_sections');
}


/**
 * Disables dashboard menu items
 */
function sigma_remove_admin_menus() {
	remove_menu_page('edit.php?post_type=sp_organizer');
	if(!is_network_administrator()) {
		remove_menu_page('users.php');
		remove_menu_page('plugins.php');
		remove_menu_page('profile.php');
		remove_menu_page('tools.php');
		remove_menu_page('link-manager.php');
		remove_menu_page('options-general.php');
		remove_submenu_page('themes.php','themes.php');
    }
}
add_action('admin_menu', 'sigma_remove_admin_menus');



// Remove the admin footer text from the dashbaord
function sigma_admin_footer_text($text){
    return '';
}
add_filter('admin_footer_text', 'sigma_admin_footer_text');



// Redirect after login based on user capabilities
function sigma_login_redirect( $redirect_to ) {

    // Users that can edit posts should go to their respective admin console
    if(is_editor()) {
        return get_blog_details(get_userdata($user->ID)->primary_blog)->siteurl . '/wp-admin/';
	}
    return !empty($redirect_to) ? $redirect_to : network_site_url();

}
add_filter( 'login_redirect', 'sigma_login_redirect' );



/**
 * Adds all custom post types and the 'inherit' and 'private' post_status to feed and category requests
 */
function sigma_request($qv) {

	//http://wordpress.org/support/topic/custom-post-type-tagscategories-archive-page

	// Add all post types to RSS feeds
	if (isset($qv['feed'])){
		$qv['post_type'] = array("post", "page", "attachment", "documents", "sp_events");
	}
	// Add all post types and states to categories
	else if ( isset($qv['category_name']) ){
		$qv['post_type'] = array("post", "page", "attachment", "documents", "sp_events");
		$qv['post_status'] = 'publish' . (is_user_logged_in() ? ',private' : '');
	}
	return $qv;
}
add_filter('request', 'sigma_request');



/**
 * Custom login logo
 */
function sigma_logo_login() {
	echo '
		<style type="text/css">
		.login h1 a { background-image: url('.get_bloginfo('stylesheet_directory').'/images/logo-login.png) !important; background-position: 6px 10px; width: auto; background-size: 99%; margin: 0 auto }
		</style>';
}
add_action('login_head', 'sigma_logo_login');


// Removes "Private: " prefix from private post titles
function remove_private_title_prefix($title) {
	return str_replace('Private: ', '' ,$title);
}
add_filter('the_title','remove_private_title_prefix');


/**
 * Adds category support to Pages
 */
function sigma_admin_init() {
	register_taxonomy_for_object_type('category', 'page');
}
add_action('admin_init', 'sigma_admin_init');


/**
 * Adds category submenu items to Media and Pages sections
 */
function sigma_admin_menu() {
	// Media menu
	add_submenu_page( 'upload.php', 'Categories', 'Categories', 'manage_categories', 'edit-tags.php?taxonomy=category&post_type=attachment' ) ;
	// Pages menu
	add_submenu_page( 'edit.php?post_type=page', 'Categories', 'Categories', 'manage_categories', 'edit-tags.php?taxonomy=category&post_type=page' ) ;


}
add_action( 'admin_menu', 'sigma_admin_menu' );


/**
 * Custom dashboard logo, favicon and icons
 */
function sigma_admin_head() {

	// Custom header logo and post type icon ?>
	<style type="text/css">
		#header-logo { width: 0; height: 0; margin-top:0; background: none !important; }
		#wp-admin-bar-member-boules .ab-sub-wrapper { overflow-y: auto; overflow-x: hidden; max-height: 20em; }
		#adminmenu #menu-posts-documents div.wp-menu-image{background:transparent url(<?php bloginfo('url') ?>/wp-admin/images/menu.png) no-repeat scroll -151px -33px;}
		#adminmenu #menu-posts-documents:hover div.wp-menu-image,#adminmenu #menu-posts-documents.wp-has-current-submenu div.wp-menu-image{background:transparent url(<?php bloginfo('url') ?>/wp-admin/images/menu.png) no-repeat scroll -151px -1px;}
		.visible-to:before { content: '\f177'; color: #888; font: 400 20px/1 dashicons;	speak: none; display: inline-block;	padding: 0 2px 0 0;	top: 0;	left: -1px;	position: relative;	vertical-align: middle;	-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-decoration: none!important;
	} <?php

	// Large icon when in "Documents" section
	global $post_type;
	if (($_GET['post_type'] == 'documents') || ($post_type == 'documents')) :
		echo '#icon-edit { background:transparent url('.get_bloginfo('url').'/wp-admin/images/icons32.png) no-repeat -312px -5px; }';
	elseif (($_GET['post_type'] == 'links') || ($post_type == 'links')) :
		echo '#icon-edit { background:transparent url('.get_bloginfo('url').'/wp-admin/images/icons32.png) no-repeat -190px -5px; }';
	endif;

	echo '</style>';
	echo '<link rel="shortcut icon" href="' . get_bloginfo( 'template_directory' ) . '/favicon.ico" />';
}
add_action('admin_head', 'sigma_admin_head');


/*
 * Enable the Category field for Attachments
 */
function media_categories() {
   register_taxonomy_for_object_type('category', 'attachment');
   add_post_type_support('attachment', 'category');
}
add_action('admin_init', 'media_categories');


/*
 * Global Boule site settings
 */
function sigma_site_settings () {
    global $blog_prefix, $wpdb;

	// Define the default option values
	$options = array(
		'chapter_visible' => '1',
		'regional_site' => '0'
	);

	// Get the blog options
    $results = $wpdb->get_results( "SELECT option_name, option_value FROM {$blog_prefix}options WHERE option_name IN ('chapter_visible','regional_site')" );
	foreach ( $results as $r ) {
		$options[$r->option_name] = $r->option_value;
	}

    ?>

    <table class="form-table"><tbody><tr>
        <th>Show in "The Member Boul&eacute;s" dropdown:</th>
        <td>
        <select name="option[chapter_visible]">
            <option value="1"<?php if( $options['chapter_visible'] === '1' ) echo ' selected="selected"'?>>Yes</option>
            <option value="0"<?php if( $options['chapter_visible'] === '0' ) echo ' selected="selected"'?>>No</option>
        </select>
        </td>
    </tr></tbody></table>

    <table class="form-table"><tbody><tr>
        <th>Regional Site:</th>
        <td>
        <select name="option[regional_site]" id="regional_site_dropdown">
            <option value="1"<?php if( $options['regional_site'] === '1' ) echo ' selected="selected"'?>>Yes</option>
            <option value="0"<?php if( $options['regional_site'] === '0' ) echo ' selected="selected"'?>>No</option>
        </select>
        </td>
    </tr></tbody></table>

    <?php
}
add_action('wpmueditblogaction', 'sigma_site_settings', 999);



/**
 * Sets the default post/page visibility to "Private - All Archons"
 */
add_action( 'post_submitbox_misc_actions' , 'default_post_visibility' );
function default_post_visibility(){
	global $post;
	
	if ( 'publish' == $post->post_status ) {
		$visibility = 'public';
		$visibility_trans = __( 'Public' );
	} elseif ( !empty( $post->post_password ) ) {
		$visibility = 'password';
		$visibility_trans = __( 'Password protected' );
	} elseif ( $post_type == 'post' && is_sticky( $post->ID ) ) {
		$visibility = 'public';
		$visibility_trans = __( 'Public, Sticky' );
	} else {
		$post->post_password = '';
		$visibility = 'private';
		$visibility_trans = __( 'Private' );
	} ?>

	<script type="text/javascript">
		(function( $ ){
			try {
				$( '#post-visibility-display' ).text( '<?php echo $visibility_trans; ?>' );
				$( '#hidden-post-visibility' ).val( '<?php echo $visibility; ?>' );
				$( '#visibility-radio-<?php echo $visibility; ?>' ).attr( 'checked', true );
			} catch(err){}
		})( jQuery );
	</script> <?php
}

/**
 * Adds the "Visible To" post meta field to the content create/edit screens.
 * This allows for a sub type of "private" posts: "All Archons" or "Boule only".
 * TODO: Update to use add_post_status once WP has fixed
 */
add_action( 'post_submitbox_misc_actions', 'post_meta_visible_to' );
add_action( 'save_post', 'save_post_meta_visible_to' );
add_action( 'admin_footer-post.php', 'post_meta_visible_to_javascript' );
add_action( 'admin_footer-post-new.php', 'post_meta_visible_to_javascript' );

function post_meta_visible_to() {
    global $post;

    $value = get_post_meta( $post->ID, '_visible_to', true ) ? get_post_meta( $post->ID, '_visible_to', true ) : 'archons'; ?>
    <div class="misc-pub-section visible-to" style="<?php echo $post->post_status !== 'private' ? 'display: none' : '' ?>">
    <?php wp_nonce_field( plugin_basename(__FILE__), 'visible_to_nonce' ); ?>
	    Visible to:
	    <select name="visible_to" id="visible_to">
	    	<option value="archons" <?php echo $value === 'archons' ? 'selected="selected"' : '' ?>>All Archons</option>
	    	<option value="boule" <?php echo $value === 'boule' ? 'selected="selected"' : '' ?>>Boul&eacute; members</option>
	    </select>
	</div>
    <?php
}
function save_post_meta_visible_to( $post_id ) {
	global $post;

    if ( !isset($_POST[ 'post_type' ] ) ) {
        return $post_id;
	}
    if ( !wp_verify_nonce( $_POST[ 'visible_to_nonce' ], plugin_basename( __FILE__ ) ) ) {
        return $post_id;
	}
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
	}
    if ( !current_user_can( 'edit_post', $post_id ) ) {
    	return $post_id;
    }

    if ( !isset( $_POST[ 'visible_to' ] ) || get_post_status( $post_id ) !== 'private' ) {
        delete_post_meta( $post_id, '_visible_to' );
	} else {
		update_post_meta( $post_id, '_visible_to', $_POST[ 'visible_to' ], get_post_meta( $post_id, '_visible_to', true ) );
	}
}
function post_meta_visible_to_javascript() { ?>

	<script>
	jQuery( document ).ready( function( $ ) {
		var $visibleTo = $( '.visible-to' ).detach();
		$visibleTo.insertAfter( $( '#visibility' ) );
		$( 'input[name="visibility"], #post_status' )
			.on( 'change', function() {
				// Determine if the visibility "private" radio button is selected or the post status dropdown is "private"
				var $field = $( this ),
					display = ( $field.is( ':checked' ) || $field.is( 'select' ) ) && $field.val() === 'private' ? 'block' : 'none';
				$visibleTo.css( 'display', display );
			})
			.trigger( 'change' );
	});
    </script><?php
}


/*
 * Determines if a category is private.  Any category with "privacy_control" = yes or a parent with this is considered private
 */
function is_private_category( $category ){

	$isPrivate = false;
	while( !$isPrivate && isset( $category ) && isset( $category->cat_ID ) ){
		$isPrivate = strcmp( get_metadata( 'term', $category->cat_ID, 'privacy_control', true ), 'yes' ) === 0;
		$category = get_category( $category->category_parent );
	}
	return $isPrivate;
}

/**
 * Logic that determines visibility of all content within the site
 */
function can_view( $post_status, $post_visible_to, $is_parent_check = false ) {

	$can_view = false;

	switch( $post_status ) {
		// Everyone can see published content
		case "publish":
			$can_view = true;
			break;

		// Inherit needs the parent content checked
		case "inherit":
			// We don't support multiple levels of inherited status (shouldn't happen anyway)
			$can_view = !$is_parent_check ? can_view_parent() : false;;
			break;

		// If visibility is limited to Boule, must be able to view "Boule only", otherwise must be logged in
		default:
			$can_view = $post_visible_to === 'boule' ? can_view_boule_only() : can_view_all_archons();
	}
	return $can_view;
}

/**
 * Determines if a user can view the given content.
 */
function can_view_content(){
	global $post;

	if ( isset( $post ) ) {
		return can_view( $post->post_status, get_post_meta( $post->ID, '_visible_to', true ) );
	}
	return false;
}

/**
 * Determines if a user can view the given content's parent .
 * This is needed for items that inherit their parent's status.
 */
function can_view_parent() {
	global $post;

	if ( isset( $post ) ) {
		$parent_post_status = get_post_status( $post->ID );
		$parent_visible_to = isset( $post->post_parent ) ? get_post_meta( $post->post_parent, '_visible_to', true ) : "";
		return can_view( $parent_post_status, $parent_visible_to, true );
	}
	return false;
}

/**
 * Determines if a user can view private content for "All Archons"
 */
function can_view_all_archons(){
	return is_user_logged_in();
}

/**
 * Determines if a user can view private content for "Boule only"
 */
function can_view_boule_only(){
	global $user_ID, $blog_id;
	$can_view = false;

	if( can_view_all_archons() ) {

		// Get logged in user
		get_currentuserinfo();

		// 1st test: super admins can view all
		$can_view = is_super_admin( $user_ID );

		// 2nd test: is the user a member of the site?
		if( !$can_view ){

			$blogs = get_blogs_of_user( $user_ID );
			foreach( $blogs as $blog ){
				if( $blog->userblog_id == $blog_id ){
					$can_view = true;
					break;
				}
			}
		}
	}
	return $can_view;
}

/**
 * See WP_User::has_cap() in wp-includes/capabilities.php
 * Automatically grants read private posts/pages to any logged in, non Archousa user
 *
 * @param  array  $allcaps Existing capabilities for the user
 * @param  string $caps    Capabilities provided by map_meta_cap()
 * @param  array  $args    Arguments for current_user_can()
 * @return array
 */
add_filter( 'user_has_cap', 'add_capability_read_private', 10, 3 );
function add_capability_read_private( $allcaps, $caps, $args ) {

	// TODO: prevent these from being added to Archousa's
	if ( is_user_logged_in() ) {
		$allcaps['read_private_posts'] = 1;
		$allcaps['read_private_pages'] = 1;
	}
	return $allcaps;
}


/**
 * Determine user's role
 */
function is_network_administrator() {
	$user = wp_get_current_user();
	return $user && $user->has_cap('manage_network');
}

function is_administrator() {
	$user = wp_get_current_user();
	return $user && $user->has_cap('manage_options');
}

function is_editor() {
	$user = wp_get_current_user();
	return $user && $user->has_cap('edit_posts');
}

function is_editor_any_site() {

	if ( !is_user_logged_in() ) {
		return false;
	}

	$userid = get_current_user_id();
	$is_editor = get_site_transient( 'is_editor_' . $userid );

	if ( $is_editor === false ) {

		$allSites = get_blogs_of_user( $userid );
		foreach ( $allSites as $site ) {
			$is_editor = current_user_can_for_blog( $site->userblog_id, 'edit_posts' );
			if ( $is_editor === true ) {
				break;
			}
		}
		$is_editor = $is_editor ? 1 : 0;
		set_site_transient( 'is_editor_' . $userid,  $is_editor, (60 * 60) );
	}
	return $is_editor === '1';
}

function is_archousa_any_site() {

	if ( !is_user_logged_in() ) {
		return false;
	}

	$userid = get_current_user_id();
	$is_archousa = get_site_transient( 'is_archousa_' . $userid );

	if ( $is_archousa === false ) {

		$allSites = get_blogs_of_user( $userid );
		foreach ( $allSites as $site ) {
			$is_archousa = current_user_can_for_blog( $site->userblog_id, 'archousa' );
			if ( $is_archousa === true ) {
				break;
			}
		}
		$is_archousa = $is_archousa ? 1 : 0;
		set_site_transient( 'is_archousa_' . $userid, $is_archousa, (60 * 60) );
	}
	return $is_archousa === '1';
}

/*
 * Gets a page ID from a given page path
 */
function get_page_id($slug){
    $page = get_page_by_path( $slug );
    return $page ? $page->ID : null;
}

/*
 * Returns true if the user is currently viewing the primary multisite
 */
function is_multisite_primary(){
    return is_main_site();
}

/*
 * Retrieves the region code option of a site
 */
function get_region_code () {
	return get_site_option('region_code', '');
}

/*
 * Is this a regional aggregate site
 */
function is_regional_site () {
	return get_option('regional_site', '0') === '1';
}

/*
 * Checks if a chapter site is visible based on the 'wp_options' table value 'chapter_visible'
 */
function is_regional_site_query($blog, $blog_prefix){
    global $wpdb;
    return $wpdb->get_var( "SELECT option_value FROM {$blog_prefix}options WHERE option_name = 'regional_site'" ) === '1';
}

/*
 * Checks if a chapter site is visible based on the 'wp_options' table value 'chapter_visible'
 */
function is_chapter_visible_query( $blog, $blog_prefix ){
    global $wpdb;
    return $wpdb->get_var( "SELECT option_value FROM {$blog_prefix}options WHERE option_name = 'chapter_visible'" ) !== '0';
}

/**
 * Returns a MODx database connection
 */
function get_modx_database () {
	$modx = IS_DEV ?
		new wpdb( 'sigma-admin-dev3', '4dMz6saRXUJN', 'modx_dev', 'localhost' ) :
		new wpdb( 'wp-user', 'YrynVig1', 'modx', 'localhost' );
	$modx->suppress_errors(!IS_DEV);
	$modx->show_errors(IS_DEV);

	return $modx;
}

/*
 * Listing of all multisite blogs
 */
function sigma_get_multisites( $user = 1, $get_unwanted = false, $sort = true ) {
	global $wpdb;

	$sites = get_transient('archon_network_sites' . ($sort ? '_sorted' : ''));
	if (false === $sites) {

		$sites = array();

		// Get all blogs and sort in alphabetical order by blog name
		$allSites = wp_get_sites(array(	'limit'=> 999 ) );

		foreach ($allSites as $site) {

			// Get the blog details
			$site = get_blog_details( (int) $site[ 'blog_id' ] );
			$blog_prefix = $wpdb->get_blog_prefix($site->blog_id);

			// Only list visible, Member Boules
			if( is_chapter_visible_query( $site, $blog_prefix ) && !is_regional_site_query( $site, $blog_prefix ) && $site->blog_id !== 1 ){
				$sites[rtrim($site->blogname)] = $site;
			}
		}
		if (!empty($sites)) {

			// Sort by ORGCD from spp_boule
			if ( $sort ) {
				$sites = get_sites_orgcd($sites);
				usort($sites, 'sigma_sort_by_orgcd');
			}

			set_transient('archon_network_sites' . ($sort ? '_sorted' : ''), $sites, (60 * 60));
		}
	}
	return $sites;
}

/**
 * Retrieve the ORGCD for a given set of WP sites
 */
function get_sites_orgcd( $sites ) {

	if (!empty($sites)) {

		// Get all Boule ORGCDs
		$modx = get_modx_database();
		$boules = $modx->get_results("select BOULENAME, ORGCD from spp_boule");
		if (!empty($boules)) {
			foreach ($boules as $boule) {

				// Do we have a matching WP Boule site?
				if (isset($sites[$boule->BOULENAME])) {

					// Store the ORGCD on the matching WP site
					$sites[$boule->BOULENAME]->ORGCD = $boule->ORGCD;
				}
			}
		}
	}
	return $sites;
}


/** Adds a section to the page/post screen's help tab */
add_action( 'load-post.php', 'add_content_help' );
add_action( 'load-post-new.php', 'add_content_help' );
function add_content_help(){
	$screen = get_current_screen();

	if ( $screen ) {
		$screen->add_help_tab( array(
			'id'       => 'formatting-help',
			'title'    => 'Adding your content',
			'content'  => '
				<p>The following links will help you create and edit you page\'s content:</p>
				<ol>
				<li><a href="http://www.textfixer.com/html/" target="_blank">Code cleaner</a></li>
				<li><a href="http://tinyurl.com/" target="_blank">Shortening URLS</a></li>
				<li><a href="http://www.ascii.cl/htmlcodes.htm" target="_blank">HTLM Codes</a></li>
				<li><a href="http://support.microsoft.com/kb/290938" target="_blank">Keyboard Shortcuts, general</a></li>
				<li><a href="http://office.microsoft.com/en-us/word-help/keyboard-shortcuts-for-international-characters-HP001230378.aspx" target="_blank">The accented &eacute; and other letters</a></li>
				<li><a href="http://office.microsoft.com/en-us/word-help/keyboard-shortcuts-for-microsoft-word-HP010370109.aspx" target="_blank">MS Formatting shortcuts</a></li>
				<li><a href="http://support.apple.com/kb/ht1343" target="_blank">MAC general shortcuts</a></li>
				<li><a href="http://symbolcodes.tlt.psu.edu/accents/codemac.html" target="_blank">MAC accent shortcuts</a></li>
				<li><a href="http://fsymbols.com/keyboard/mac/" target="_blank">MAC symbols shortcuts</a></li>
				</ol>'
		) );
		$screen->add_help_tab( array(
			'id'       => 'image-help',
			'title'    => 'Image editing',
			'content'  => '
				<p>Learn about editing images before adding them to your page:</p>
				<ol>
				<li><a href="http://www.investintech.com/articles/mscustomization/" target="_blank">MS Office</a></li>
				<li><a href="http://office.microsoft.com/en-us/excel-help/accessibility-tips-when-working-with-a-smartart-graphic-HA010099846.aspx" target="_blank">Office SmartArt</a></li>
				<li><a href="http://office.microsoft.com/en-us/publisher-help/tips-for-working-with-images-HA001218940.aspx" target="_blank">Office Tips for working with images</a></li>
				<li><a href="http://office.microsoft.com/en-us/help/crop-or-edit-multiple-pictures-at-once-in-picture-manager-HA001077145.aspx" target="_blank">Crop or edit multiple pictures at once in Picture Manager</a></li>
				<li><a href="http://www.flyingmeat.com/acorn/" target="_blank">MAC image editing software</a></li>
				<li><a href="http://www.gimp.org/" target="_blank">FREE MAC image editing software</a></li>
				<li><a href="http://www.youtube.com/watch?v=Fvy9DaUE5LM" target="_blank">iPhoto Tutorial</a></li>
				<li><a href="http://www.youtube.com/watch?v=kmc3NDCL87k" target="_blank">iPhoto 11 Full Tutorial</a></li>
				</ol>'
		) );
		$screen->add_help_tab( array(
			'id'       => 'video-help',
			'title'    => 'Adding videos',
			'content'  => '
				<p>Links to help you add Youtube and Vimeo links in your pages:</p>
				<ol>
				<li><a href="http://en.support.wordpress.com/videos/youtube/" target="_blank">Wordpress embedding help</a></li>
				<li><a href="http://codex.wordpress.org/Embeds" target="_blank">Embedded videos basics</a></li>
				<li><a href="http://www.youtube.com/watch?v=z6TIrM6gZQk" target="_blank">Embed videos in wordpress--no plugin required</a></li>
				<li><a href="http://www.youtube.com/watch?v=-1WYXrGOAA0" target="_blank">Wordpress basic tutorial for everything</a><br>
				</li>
				</ol>'
		) );
	}
}



/**
 * Update the admin bar
 */
function sigma_admin_bar() {
    global $wp_admin_bar, $blog_id;

	// Remove default links
    $wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->remove_menu('site-name');
	$wp_admin_bar->remove_menu('new-content');
	$wp_admin_bar->remove_menu('edit');
	$wp_admin_bar->remove_menu('updates');
	$wp_admin_bar->remove_menu('comments');
	$wp_admin_bar->remove_menu('my-account');
	$wp_admin_bar->remove_menu('ngg-menu');


	/**
	 * Left side of admin bar
	 */

	// Dashbaord links
	if( is_administrator() || is_editor_any_site() ) {
	    $wp_admin_bar->add_menu(array(
			'title' => __( 'Dashboard', 'archon-network' ),
			'parent' => $isUser ? 'account' : false,
			'id' => 'my-sites'
		));

	    foreach ( (array)$wp_admin_bar->user->blogs as $blog ) {
	        $menu_id  = 'blog-' . $blog->userblog_id;
	        $wp_admin_bar->add_menu( array(
	            'parent'    => 'my-sites-list',
	            'id'    => $menu_id,
	            'title' => $blog->blogname,
	            'href'  => $blog->siteurl
			) );

			// Check if the new post link exists since this will only be available to editors.
			// If it doesn't, remove the dashboard link.
			if ( !$wp_admin_bar->get_node( $menu_id . '-n' ) ) {
				$wp_admin_bar->remove_menu( $menu_id . '-d' );
			}
	    }

	} else {
		$wp_admin_bar->remove_menu('my-sites');
	}

	// Grand Boule
    $wp_admin_bar->add_menu(array(
		'title' => __('The Grand Boul&eacute;', 'archon-network'),
		'href' => 'http://www.sigmapiphi.org/home/',
		'parent' => false
	));

	// Member Boule dropdowns
    $wp_admin_bar->add_menu(array(
		'title' => __( 'The Member Boul&eacute;s', 'archon-network' ),
		'parent' => false,
		'id' => 'member-boules'
	));
	$sites = sigma_get_multisites();
	foreach($sites as $site) {
	    $wp_admin_bar->add_menu(array(
			'title' => $site->blogname,
			'href' => $site->siteurl,
			'parent' => 'member-boules'
		));
	}

	// Archon Network link
    $wp_admin_bar->add_menu(array(
		'title' => get_site_option('site_name'),
		'href' => '/boules',
		'parent' => false
	));


	/**
	 * Right side of admin bar
	 */

	// User menu
	$current_user = wp_get_current_user();
	$isUser = 0 !== $current_user->ID;
	if ($isUser) {
		$wp_admin_bar->add_menu(array(
			'id' => 'account',
			'parent' => 'top-secondary',
			'title' => $current_user->user_login,
			'href' => '/home/private_home.php?webloginmode=lo'
			)
		);
	    $wp_admin_bar->add_menu(array(
			'title' => __( 'Log Out', 'archon-network' ),
			'parent' => $isUser ? 'account' : false,
			'href' => '/home/private_home.php?webloginmode=lo'
		));
	}

}
add_action('wp_before_admin_bar_render', 'sigma_admin_bar', 0);

// Turn the admin bar on for everyone
add_filter( 'show_admin_bar', '__return_true' , 1000 );


/**
 * Remove all WP upgrade notices for non-admins
 */
function remove_core_updates( $last_checked ){
	global $wp_version;
	
	if ( !is_network_administrator() ) {
	
		// Remove auto update notices
		delete_site_option( "auto_core_update_failed" );
		remove_action( "load-update-core.php", "wp_update_plugins" );
		
		// Makes it look like the version has just been checked
		$last_checked = ( object ) array (
			"last_checked"		=> time(),
			"version_checked" 	=> $wp_version
		);
		
	}
	return $last_checked;
}
add_filter('pre_site_transient_update_core','remove_core_updates');
add_filter('pre_site_transient_update_plugins','remove_core_updates');
add_filter('pre_site_transient_update_themes','remove_core_updates');
	

?>