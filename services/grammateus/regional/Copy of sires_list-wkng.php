<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsSiresByRegion = "-1";
if (isset($_POST['REGIONNAME'])) {
  $colname_rsSiresByRegion = $_POST['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsSiresByRegion = sprintf("SELECT * FROM vw_boule_officers WHERE REGIONNAME = %s AND COMMITTEESTATUSSTT = 'Active' AND    (vw_boule_officers.CPOSITION LIKE 'Sire%%' OR vw_boule_officers.CPOSITION LIKE 'Immed%%') ORDER BY BOULENAME ASC", GetSQLValueString($colname_rsSiresByRegion, "text"));
$rsSiresByRegion = mysql_query($query_rsSiresByRegion, $sigma_modx) or die(mysql_error());
$row_rsSiresByRegion = mysql_fetch_assoc($rsSiresByRegion);
$totalRows_rsSiresByRegion = mysql_num_rows($rsSiresByRegion);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --> 


<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_regional_grammateus.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow"><?php echo $_POST['REGIONNAME']; ?> Region Sire Archons</h1>
<div class="contentBlock">
<table width="100%" cellpadding="2">
  <tr>
    <td>BOUL&Eacute;</td>
    <td>ARCHON</td>
    <td>OFFICE</td>
    <td>START DATE</td>
    <td>EDIT</td>
  </tr>
  <?php do { ?>
    <tr valign="top">
      <td><?php echo $row_rsSiresByRegion['BOULENAME']; ?></td>
      <td><a href="archon_overview_regional.php?WEB_ID=<?php echo $row_rsSiresByRegion['WEB_ID']; ?>" rel="<?php echo $row_rsSiresByRegion['WEB_ID']; ?>"><?php echo $row_rsSiresByRegion['FULLNAME']; ?></a>
          <div id="<?php echo $row_rsSiresByRegion['WEB_ID']; ?>" class="balloonstyle">
            <?php echo $row_rsSiresByRegion['ADDRESS1']; ?><br />
            <?php echo $row_rsSiresByRegion['CITY']; ?>, <?php echo $row_rsSiresByRegion['STATECD']; ?> <?php echo $row_rsSiresByRegion['ZIP']; ?><br />
            HOME PHONE: <?php echo $row_rsSiresByRegion['HOMEPHONE']; ?><br />
            EMAIL: <?php echo $row_rsSiresByRegion['EMAIL']; ?><br />
            BUSINESS PHONE: <?php echo $row_rsSiresByRegion['WORKPHONE']; ?><br />
            BUSINESS EMAIL: <?php echo $row_rsSiresByRegion['ALTEMAIL']; ?>    </div></td>
      <td><?php echo $row_rsSiresByRegion['CPOSITION']; ?></td>
      <td><?php echo $row_rsSiresByRegion['STARTDATE']; ?></td>
      <td>Change Start Date<br />
        Remove from Office</td>
    </tr>
    <?php } while ($row_rsSiresByRegion = mysql_fetch_assoc($rsSiresByRegion)); ?>
</table>
</div>
<!-- InstanceEndEditable --></div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsSiresByRegion);
?>
