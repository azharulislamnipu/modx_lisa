<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the KT_back class
require_once('../../includes/nxt/KT_back.php');
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsRhetoricosByRegion = "-1";
if (isset($_COOKIE['REGIONNAME'])) {
  $colname_rsRhetoricosByRegion = $_COOKIE['REGIONNAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsRhetoricosByRegion = sprintf("SELECT * FROM vw_boule_officers WHERE REGIONNAME = %s AND COMMITTEESTATUSSTT = 'Active' AND    (vw_boule_officers.CPOSITION LIKE '%%hetor%%') ORDER BY BOULENAME ASC", GetSQLValueString($colname_rsRhetoricosByRegion, "text"));
$rsRhetoricosByRegion = mysql_query($query_rsRhetoricosByRegion, $sigma_modx) or die(mysql_error());
$row_rsRhetoricosByRegion = mysql_fetch_assoc($rsRhetoricosByRegion);
$totalRows_rsRhetoricosByRegion = mysql_num_rows($rsRhetoricosByRegion);

$colname_result = "-1";
if (isset($_COOKIE['GRAMMID'])) {
  $colname_result = $_COOKIE['GRAMMID'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_result = sprintf("SELECT REGIONNAME, BOULENAME, CHAPTERID FROM vw_web_elig_no_officers WHERE CUSTOMERID = %s", GetSQLValueString($colname_result, "int"));
$result = mysql_query($query_result, $sigma_modx) or die(mysql_error());
$row_result = mysql_fetch_assoc($result);
$totalRows_result = mysql_num_rows($result);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_regional_grammateus.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow"><?php echo $_COOKIE['REGIONNAME']; ?> Region Rhetoricos</h1>
<div class="contentBlock">
<table width="100%" cellpadding="2">
<tr>
  <td><a href="/home/reg_gramm_mgmt.php">Back to Officer's Listing</a></td>
  <td><form id="rhetoricos" name="rhetoricos" method="post" action="">
    <input name="rhetoricos_export" type="hidden" id="rhetoricos" value="rhetoricos" />
    <input name="export" type="submit" value="Click to Export Rhetoricos Data" />
    </form></td>
  <td align="right">&nbsp;</td>
  <td colspan="2" align="right"><a href="add_officer.php?KT_back=1">Add New Officer</a></td>
  </tr>
  <tr>
    <td><strong>BOUL&Eacute;</strong></td>
    <td><strong>ARCHON</strong></td>
    <td><strong>OFFICE</strong></td>
    <td><strong>START DATE</strong></td>
    <td><strong>EDIT</strong></td>
  </tr>
  <?php do { ?>
    <tr valign="top">
      <td><?php echo $row_rsRhetoricosByRegion['BOULENAME']; ?></td>
      <td>
<script language="JavaScript">
    function edit_<?php echo $row_rsRhetoricosByRegion['CUSTOMERCD'];?>(){
        document.forms.form_<?php echo $row_rsRhetoricosByRegion['CUSTOMERCD']; ?>.submit();
    }
</script>
  <form name="form_<?php echo $row_rsRhetoricosByRegion['CUSTOMERCD']; ?>" action="/services/grammateus/regional/rhetoricos_edit.php" method="get">
  <input type="hidden" name="CUSTOMERID" value="<?php echo KT_escapeAttribute($row_rsRhetoricosByRegion['CUSTOMERCD']); ?>">
  <input type="hidden" name="GRAMMID" value="<?php echo KT_escapeAttribute($_COOKIE['GRAMMID']); ?>">
  <input type="hidden" name="UPDATEDBY" value="<?php echo KT_escapeAttribute($_COOKIE['UPDATEDBY']); ?>">
  <input type="hidden" name="BOULENAME" value="<?php echo KT_escapeAttribute($_COOKIE['BOULENAME']); ?>">
  <input type="hidden" name="REGIONNAME" value="<?php echo KT_escapeAttribute($_COOKIE['REGIONNAME']); ?>">
  </form>
   
      
      <a href="#" rel="<?php echo $row_rsRhetoricosByRegion['WEB_ID']; ?>" onclick="edit_<?php echo $row_rsRhetoricosByRegion['CUSTOMERCD'];?>();return(FALSE);"><?php echo $row_rsRhetoricosByRegion['FULLNAME']; ?></a>
          <div id="<?php echo $row_rsRhetoricosByRegion['WEB_ID']; ?>" class="balloonstyle">
            <?php echo $row_rsRhetoricosByRegion['ADDRESS1']; ?><br />
            <?php echo $row_rsRhetoricosByRegion['CITY']; ?>, <?php echo $row_rsRhetoricosByRegion['STATECD']; ?> <?php echo $row_rsRhetoricosByRegion['ZIP']; ?><br />
            HOME PHONE: <?php echo $row_rsRhetoricosByRegion['HOMEPHONE']; ?><br />
            EMAIL: <?php echo $row_rsRhetoricosByRegion['EMAIL']; ?><br />
            BUSINESS PHONE: <?php echo $row_rsRhetoricosByRegion['WORKPHONE']; ?><br />
            BUSINESS EMAIL: <?php echo $row_rsRhetoricosByRegion['ALTEMAIL']; ?>    </div></td>
      <td><?php echo $row_rsRhetoricosByRegion['CPOSITION']; ?></td>
      <td><?php echo $row_rsRhetoricosByRegion['STARTDATE']; ?></td>
      <td><a href="change_date.php?OFFICERID=<?php echo $row_rsRhetoricosByRegion['OFFICERID']; ?>&amp;CUSTOMERCD=<?php echo $row_rsRhetoricosByRegion['CUSTOMERCD']; ?>&amp;KT_back=1">Change Start Date</a><br />
        <a href="remove_officer.php?OFFICERID=<?php echo $row_rsRhetoricosByRegion['OFFICERID']; ?>&amp;CUSTOMERCD=<?php echo $row_rsRhetoricosByRegion['CUSTOMERCD']; ?>&amp;KT_back=1">Remove from Office</a></td>
    </tr>
    <?php } while ($row_rsRhetoricosByRegion = mysql_fetch_assoc($rsRhetoricosByRegion)); ?>
<tr>
  <td><a href="/home/reg_gramm_mgmt.php">Back to Officer's Listing</a></td>
  <td align="right">&nbsp;</td>
  <td align="right">&nbsp;</td>
  <td colspan="2" align="right"><a href="add_officer.php?KT_back=1">Add New Officer</a></td>
  </tr>
</table>
</div>
<!-- InstanceEndEditable --></div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsRhetoricosByRegion);

mysql_free_result($result);
?>
