<?php
/**
 * DokuWiki mainscript
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Andreas Gohr <andi@splitbrain.org>
 *
 *
 * Modified by EPO - for integration into ModX cms - august 2010
 * * 2011/08/03 - update to comply with dokuwiki-2011-05-25a �Rincewind� version
 */

// update message version
$updateVersion = 34;

//  xdebug_start_profiling();


//define ('MODX_BASE_PATH', '../../../');
//include_once(MODX_BASE_PATH.'modxapi.php');
//print_r($_REQUEST);

define ('DOKUWIKI_RELBASE_PATH','assets/snippets/dokuwiki/');
define ('DOKUWIKI_BASE_PATH', MODX_BASE_PATH.DOKUWIKI_RELBASE_PATH);
//include_once(MODX_BASE_PATH.'assets/snippets/rightsCheck/rightsControl.php');


function modx_unfilter($aText)
{
	$aText  = str_replace('{ {','{{',str_replace('} }','}}',cleanText($aText)));
	$aText  = str_replace('[ [','[[',str_replace('] ]',']]',$aText));
	$aText  = str_replace('[ !','[!',str_replace('! ]','!]',$aText));
	$aText  = str_replace('\\ \\','\\\\',$aText);
	return $aText;
}

function modx_filter($aText)
{
	$aText=preg_replace("@([^'])\]\]@", '$1] ]', $aText); 
	$aText=preg_replace("@([^'])\[\[@", '$1[ [', $aText); 
	$aText=preg_replace("@([^'])!\]@", '$1! ]', $aText); 
	$aText=preg_replace("@([^'])\[!@", '$1[ !', $aText); 
	$aText=preg_replace("@([^'])}}@", '$1} }', $aText); 
	$aText=preg_replace("@([^']){{@", '$1{ {', $aText); 
	$aText=preg_replace("@([^'])\\\\@", '$1\\ \\', $aText); 
	return $aText;
}


global $modx;
global $ACT;
global $QUERY;
global $ID;
global $NS;
global $REV;
global $IDX;
global $DATE;
global $RANGE;
global $HIGH;
global $TEXT;
global $PRE;
global $SUF;
global $SUM;
global $INFO;
global $JSINFO;

if(!defined('DOKU_INC')) define('DOKU_INC',dirname(__FILE__).'/');

if(!defined('DOKU_REL')) define('DOKU_REL',MODX_BASE_URL.DOKUWIKI_RELBASE_PATH);
if(!defined('DOKU_URL')) define('DOKU_URL',MODX_SITE_URL.DOKUWIKI_RELBASE_PATH);

//if(!defined('DOKU_BASE'))


if (isset($_SERVER['HTTP_X_DOKUWIKI_DO'])){
    $ACT = trim(strtolower($_SERVER['HTTP_X_DOKUWIKI_DO']));
} elseif (!empty($_REQUEST['idx'])) {
    $ACT = 'index';
} elseif (isset($_REQUEST['do'])) {
    $ACT = $_REQUEST['do'];
} else {
    $ACT = 'show';
}

// load and initialize the core system
require_once(DOKU_INC.'inc/init.php');

//import variables
$_REQUEST['id'] = str_replace("\xC2\xAD",'',$_REQUEST['id']); //soft-hyphen
$QUERY = trim($_REQUEST['id']);
$ID    = getID();

// deprecated 2011-01-14
$NS    = getNS($ID);

$REV   = $_REQUEST['rev'];
$IDX   = $_REQUEST['idx'];
$DATE  = $_REQUEST['date'];
$RANGE = $_REQUEST['range'];
$HIGH  = $_REQUEST['s'];
if(empty($HIGH)) $HIGH = getGoogleQuery();
//echo $_POST['wikitext'];exit;
//les {{}} sont filtr�s par ModX lorsque pass�s en param�tres Post�s... 
// pour passer le filtre on ajoute/retire des blancs entre les {{}}
// (cf. lib/tpl/openskies/main.php - fonction javascript modx_unfiltering_wikitext)
//$TEXT  = cleanText($_POST['wikitext']);

if (isset($_POST['wikitext'])) {
	$TEXT  = modx_unfilter(cleanText($_POST['wikitext']));
}
//$TEXT  = str_replace('{ {','{{',str_replace('} }','}}',cleanText($_POST['wikitext'])));
//$TEXT  = str_replace('[ [','[[',str_replace('] ]',']]',$TEXT));
//$TEXT  = str_replace('\\ \\','\\\\',$TEXT);
//echo "$TEXT<br />".$_POST['wikitext'];exit;

$PRE   = modx_unfilter(cleanText(substr($_POST['prefix'], 0, -1)));
$SUF   = modx_unfilter(cleanText($_POST['suffix']));

$SUM   = $_REQUEST['summary'];

//sanitize revision
$REV = preg_replace('/[^0-9]/','',$REV);

//make infos about the selected page available
$INFO = pageinfo();

//export minimal infos to JS, plugins can add more
$JSINFO['id']        = $ID;
$JSINFO['namespace'] = (string) $INFO['namespace'];


// handle debugging
if($conf['allowdebug'] && $ACT == 'debug'){
    html_debug();
    exit;
}

//send 404 for missing pages if configured or ID has special meaning to bots
if(!$INFO['exists'] &&
  ($conf['send404'] || preg_match('/^(robots\.txt|sitemap\.xml(\.gz)?|favicon\.ico|crossdomain\.xml)$/',$ID)) &&
  ($ACT == 'show' || (!is_array($ACT) && substr($ACT,0,7) == 'export_')) ){
    header('HTTP/1.0 404 Not Found');
}

//prepare breadcrumbs (initialize a static var)
if ($conf['breadcrumbs']) breadcrumbs();

// check upstream
checkUpdateMessages();

$tmp = array(); // No event data
trigger_event('DOKUWIKI_STARTED',$tmp);

//close session
session_write_close();

//do the work

//on va stocker le r�sultat de dokuwiki / we store the result of dokuwiki

    // au cas o� il y aurait un ob_start de d�marr� avant...
    if ($prev_content=ob_get_contents()) ob_clean();
    else ob_start();
    
    act_dispatch($ACT);
    
    $outputdoku = ob_get_contents();
    if($prev_content === false) ob_end_clean();
    else
    {
        ob_clean();
        ob_start();
    }
    
    // si en mode �dition, on �carte toutes les s�quences qui pourraient �tre interpr�t�es par ModX dans le parser
	// if we are in edit mode, we escape any sequences that could be parsed and filtered by ModX
    // ex : '{{'
	
    //echo $ACT; //action en cours
    if ( ($ACT == 'edit') || ($ACT = 'preview')  || ($ACT = 'draft'))
    {
      // li� au TextArea de main.php correspondant � l'�dition du texte et des liens wiki
      // on modifie tous les crochets du textarea afin qu'ils passent le parser de ModX
      // Penser � remettre correctement les crochets dans le textarea avec du javascript (cf. lib/tpl/openskies/main.php - init_modx_unfiltering) quand le navigateur interpr�te le contenu renvoy� par ModX
	  // linked to TextArea in main.php managing the text edition and the wiki links
	  // We modify every wiki tag in order that they will not be filtered by the Modx parser
	  // then we will have to think to put them back in the textarea with javascript (cf. lib/tpl/openskies/main.php - init_modx_unfiltering) when the browser will read the content sent back by ModX

      preg_match('@<textarea.*>(.*)</textarea>@si',$outputdoku,$matchta);
	  $matchta = modx_filter($matchta[1]);
      $outputdoku=preg_replace('@<textarea(.*>)(.*)</textarea>@si', "<textarea$1$matchta</textarea>", $outputdoku); 

	  // Faire la m�me op�ration pour la partie suffix et prefix si on �dite une portion du texte... 
	  // Do the same operation for the "suffix" and "prefix" if we edit a part of text
      preg_match('@<input type="hidden" name="prefix" value="(.*)" />@Usi',$outputdoku,$matchta); //@Usi : ungreedy+new line+case insensitive
	  $matchta = modx_filter($matchta[1]);
      $outputdoku=preg_replace('@<input type="hidden" name="prefix" value="(.*)" />@Usi', '<input type="hidden" name="prefix" value="'.$matchta.'" />', $outputdoku); 

      preg_match('@<input type="hidden" name="suffix" value="(.*)" />@Usi',$outputdoku,$matchta);
	  $matchta = modx_filter($matchta[1]);
      $outputdoku=preg_replace('@<input type="hidden" name="suffix" value="(.*)" />@Usi', '<input type="hidden" name="suffix" value="'.$matchta.'" />', $outputdoku); 

      // li� au pb dans la g�n�ration du tableau de l'ACL dans lequel il y a des [*] dans des inputs qui sont filtr�s car sur une seule ligne (cf. lib/plugins/acl/main.php)
	  // linked to the problem of generating the ACL table in which there are [*] in the input that are filtered because they are in a single line (cf. lib/plugins/acl/main.php)
      $outputdoku=str_replace('<input', "\n<input", $outputdoku);

      //$outputdoku=preg_replace('@\[\*(.*?)\*\]@', '[ *$1* ]', $outputdoku);

	  }

	//et on remet tout en place...
	//and we put everything in place
    echo $prev_content.$outputdoku;

$tmp = array(); // No event data
trigger_event('DOKUWIKI_DONE', $tmp);
//exit;

//  xdebug_dump_function_profile(1);
