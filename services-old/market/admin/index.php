<?php
require_once('../includes/admin/access.php');
require_once('../includes/admin/header.php');
?>


<h3><?php echo T_ADMIN;?></h3>

<b>Quick view:</b>
<ul>
<li>Version: <?php echo VERSION;?></li>
<li><?php echo T_LANGUAGE;?>: <?php echo LANGUAGE;?></li>
<li><?php echo T_THEME;?>: <?php echo THEME;?></li>
<li><?php echo T_TOTAL_ADS.': '.totalAds();?>
<li><?php echo T_VIEWS.': '.totalViews();?></li>
</ul>


<?php
   echo '<br /><b><a href="http://open-classifieds.com/blog/" target="_blank">Blog Updates:</a></b><ul>'.rssReader('http://open-classifieds.com/feed/',5,CACHE_ACTIVE,'<li>','</li>').'</ul>';
   echo '<br /><b><a href="http://open-classifieds.com/forum" target="_blank">Support forum:</a></b><ul>'.rssReader('http://open-classifieds.com/forum/rss/topics',5,CACHE_ACTIVE,'<li>','</li>').'</ul>';
?>


<?php
require_once('../includes/admin/footer.php');
?>
