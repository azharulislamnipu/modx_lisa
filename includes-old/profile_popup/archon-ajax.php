<?php
if(!(defined('LoggedIn') && LoggedIn)){
	die('not permitted!');
}

$user_id = isset($_GET['user_id']) && is_numeric($_GET['user_id']) ? $_GET['user_id'] : false;
$visitor_id = isset($_GET['visitor']) && is_numeric($_GET['visitor']) ? $_GET['visitor'] : false;
$dm_email = isset($_GET['DM_EMAIL']) ? $_GET['DM_EMAIL'] : false;
if(empty($user_id) || !$user_id){
    die('no user id given!');
}
//$user_id = $_SESSION['CUSTOMERCD'];

$user_data = SPP_ARCHON_DB::get_user_profile_data($user_id)['data'];
if($visitor_id){
    $visitor_data = SPP_ARCHON_DB::get_user_profile_data($visitor_id)['data'];
} else {
    $visitor_data = $user_data;
}

if(empty($dm_email)){
	$dm_email = $visitor_data['EMAIL'];
}

$user_img = (empty($user_data['IMAGE'])) ? '/home/assets/images/icons/archon-default.png' :
'/home/assets/images/archons/' .$user_data['IMAGE'];

$submit_dv = <<<DDD
<div class="sb_div">
    <button type="button" class="pp_submit button">Update</button> &nbsp;
    <button type="button" class="pp_submit button new_elem_submit">Submit</button> &nbsp;
    <button type="button" class="pp_delete button hide" onclick="if(confirm('Delete this?')) {delete_this_pp(jQuery(this)); } return false;">Delete</button> &nbsp;
    <button type="reset" class="pp_cancel button">Done</button> &nbsp;

</div>
DDD;

/* Career Information Block */
$user_prof_data = SPP_ARCHON_DB::get_all_user_prof($user_id)['data'];

$all_prof_name = SPP_ARCHON_DB::get_all_profession()['data'];
$all_prof_name = array_combine_(array_column($all_prof_name, 'OCCUPATIONID'), $all_prof_name);

$all_prof_title = SPP_ARCHON_DB::get_all_prof_title()['data'];
$all_prof_title = array_combine_(array_column($all_prof_title, 'SKILLID'), $all_prof_title);
/* Career Information Block */

/* Education Information Block */
$user_edu_data = SPP_ARCHON_DB::get_all_user_edu($user_id)['data'];
/* Education Information Block */

/* Spouse Information Block */
$all_spouse = SPP_ARCHON_DB::get_all_user_spouse($user_id);
$all_spouse_data = $all_spouse['data'];
/* Spouse Information Block */

/* Children Information Block */
$all_children = SPP_ARCHON_DB::get_all_user_children($user_id);
$all_children_data = $all_children['data'];
/* Children Information Block */

?>

<?php ob_start();?>
<div id="popup_ymp" class="popup-ymp">

    <div class="profile_title_header profile_title_header_edit clearfix">
        <h1 id="profile_title">
            <?php echo "{$user_data['FIRSTNAME']} {$user_data['MIDDLEINITIAL']} {$user_data['LASTNAME']} {$user_data['SUFFIX']}";?>
        </h1>
    </div>
    <div class="view_edit_block clearfix">
        <a href="#" class="view_this_archon button">View <?php echo $user_data['FIRSTNAME'];?> Profile</a> &nbsp;
        <a href="#" class="edit_next_archon button">Edit Next Archon</a>
    </div>

    <form action="" method="post" id="pupup_form">
        <div class="message">

        </div>
        <div id="archon_popup_accordion" data-accordion-group>

            <?php include_once $pp_dir . "inc/archon/edit_contact.php";?><!-- edit contact  -->

            <?php include_once $pp_dir . "inc/archon/edit_img.php";?> <!--edit image-->

            <?php include_once $pp_dir . "inc/archon/edit_education.php";?><!-- end edit education  -->

            <?php include_once $pp_dir . "inc/archon/edit_career.php";?><!-- end edit career archon  -->

            <?php include_once $pp_dir . "inc/archon/edit_children.php";?><!-- edit Children  -->

            <?php include_once $pp_dir . "inc/archon/edit_bio.php";?><!-- end edit bio  -->
            <?php include_once $pp_dir . "inc/archon/request_status_change.php";?><!-- end Request Status Change  -->

        </div>

        <?php
        $full_name = $user_data['FIRSTNAME'] . ' ' . $user_data['MIDDLEINITIAL'] . ' ' . $user_data['LASTNAME'];
        $visitor_full_name = $visitor_data['FIRSTNAME'] . ' ' . $visitor_data['MIDDLEINITIAL'] . ' ' . $visitor_data['LASTNAME'];
        ?>
        <input type="hidden" name="CUSTOMERID" value="<?php echo $user_id;?>">
        <input type="hidden" name="UPDATEDBY" value="<?php echo $visitor_full_name;?>">
        <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo $user_data['LASTUPDATED'];?>">
        <input type="hidden" name="RETURN" value="<?php echo $_SESSION['webValidated'];?>">
        <input type="hidden" name="SPOUSEID" value="<?php if(isset($all_spouse_data[0]['SPOUSEID'])) echo
        $all_spouse_data[0]['SPOUSEID'];?>">
        <input type="hidden" name="ARCHOUSAID" value="<?php if(isset($all_spouse_data[0]['ARCHOUSAID'])) echo $all_spouse_data[0]['ARCHOUSAID'];?>">
        <input type="hidden" name="PROFILETYPE" value="ARCHON">
        <input type="hidden" name="image_name" value="<?php echo $full_name;?>">
    </form>
    <a href="#" class="popup_close">X</a>
</div>

<?php ob_get_flush();
die();