<?php get_header(); ?>

	<div id="content">
		<div class="padder">

		<?php do_action( 'sigma_before_blog_home' ); ?>

		<?php do_action( 'template_notices' ); ?>

		<div class="page" id="blog-latest" role="main">

			<?php if ( have_posts() ) : ?>

				<?php while (have_posts()) : the_post(); ?>

					<?php do_action( 'sigma_before_blog_post' ); ?>

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<div class="author-box">
							<?php echo get_avatar( get_the_author_meta( 'user_email' ), '50' ); ?>

							<?php if ( is_sticky() ) : ?>
								<span class="activity sticky-post"><?php _ex( 'Featured', 'Sticky post', 'archon-network' ); ?></span>
							<?php endif; ?>
						</div>

						<div class="post-content">
							<h2 class="posttitle"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php _e( 'Permanent Link to', 'archon-network' ); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

							<p class="date"><?php printf( __( '%1$s <span>in %2$s</span>', 'archon-network' ), get_the_date(), get_the_category_list( ', ' ) ); ?></p>

							<div class="entry">
								<?php the_content( __( 'Read the rest of this entry &rarr;', 'archon-network' ) ); ?>
								<?php wp_link_pages( array( 'before' => '<div class="page-link"><p>' . __( 'Pages: ', 'archon-network' ), 'after' => '</p></div>', 'next_or_number' => 'number' ) ); ?>
							</div>

							<p class="postmetadata"><?php the_tags( '<span class="tags">' . __( 'Tags: ', 'archon-network' ), ', ', '</span>' ); ?> <span class="comments"><?php comments_popup_link( __( 'No Comments &#187;', 'archon-network' ), __( '1 Comment &#187;', 'archon-network' ), __( '% Comments &#187;', 'archon-network' ) ); ?></span></p>
						</div>

					</div>

					<?php do_action( 'sigma_after_blog_post' ); ?>

				<?php endwhile; ?>

			<?php else : ?>

				<h2 class="center"><?php _e( 'Not Found', 'archon-network' ); ?></h2>
				<p class="center"><?php _e( 'No posts found...', 'archon-network' ); ?></p>

				<?php get_search_form(); ?>

			<?php endif; ?>
		</div>

		<?php do_action( 'sigma_after_blog_home' ); ?>

		</div><!-- .padder -->
	</div><!-- #content -->

	<?php get_sidebar(); ?>

<?php get_footer(); ?>
