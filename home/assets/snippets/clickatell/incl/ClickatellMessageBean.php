<?php

require_once 'ClickatellText.php';
require_once 'ClickatellUtils.php';

/**
 * Holds a single Clickatell message's data
 * Author: Pat Heard {http://fullahead.org}
 */
 
class ClickatellMessageBean {
	
	private $id = -1;
	private $date_format = '';
	private $active_ind = '0';	
	
	private $api_id = '';
	private $moMsgId = '';
	private $moMsgId_parent = '';
	private $moMsgId_linked = array();
	
	private $to = '';	
	private $from = '';	
	private $text = '';
        private $name = '';
	
	private $charset = '';
	private $udh = '';
	private $timestamp = '';
	
	/** Build the bean */
	public function __construct(array $args = array()){
		
		// Merge the default values with passed values	
		$args = array_merge(array(
			'id'			=> -1,
			'active_ind'	=> '0',	// Is this message active?
			'date_format' 	=> 'M j, Y H:i',	// Format for display of timestamp
			'from_post'		=> false,	// Is this bean being created from $_POST values?
			'api_id'		=> '',	// Clickatell API
			'moMsgId'		=> '',	// Clickatell message ID
			'moMsgId_parent'=> '',	// Clickatell message ID this message is in reply to
			'to'			=> '',	// To phone number (international format)
			'from'			=> '',	// From phone number (international format)
                        'name'                  => '',  // Name of the person who sent the text
			'text'			=> '',	// Text message
			'charset'		=> '',	// Charater set of text message
			'udh'			=> '',	// UDH headers - used to multiple text message together.  First 10 digits for related message will match, remaining 2 digits will give sequence of message
			'timestamp'		=> ''	// Timestamp of message
		), $args);		

		/* Check if this bean is being created by a Clickatell POST callback */
		if($args['from_post'] === true){
			$args = array_merge($args, $_POST);
		}
		
		$this->id = $args['id'];
		$this->date_format = $args['date_format'];
		$this->active_ind = $args['active_ind'];
		
		$this->api_id = $args['api_id'];
		$this->moMsgId = $args['moMsgId'];
		$this->moMsgId_parent = $args['moMsgId_parent'];
		
		$this->to = $args['to'];	
		$this->from = $args['from'];
                $this->name = urldecode($args['name']);
		$this->text = urldecode($args['text']);	
		
		$this->charset = $args['charset'];
		$this->udh = $args['udh'];		
		$this->timestamp = $args['timestamp'];	
				
	}
	
	/** Does this bean contain enough data to insert to the database? */
	public function isInsertable(){		
		return 
			!empty($this->api_id) &&
			!empty($this->moMsgId) &&
			!empty($this->to) &&
			!empty($this->from) &&
			!empty($this->text) &&
			!empty($this->charset) &&
			!empty($this->active_ind) &&
			!empty($this->timestamp);
	}
	
	/** Do we have a valid from phone number to reply to? */
	public function isFromValid(){
		return !empty($this->from) && ClickatellUtils::isValidNumber(ClickatellUtils::cleanPhoneNumber($this->from));
	}

	/** Return the text message, truncated as required */
	public function getText($count = -1){
		if($count > -1){
			$words = explode(' ', $this->text);
			if(count($words) > $count + 1){
				return implode(' ', array_slice($words, 0, $count + 1)) . '...';
			}
		}
		return $this->text;
	}
	
	/** Return the formated timestamp */
	public function getTimestamp(){
		$time = strtotime($this->timestamp);
		if($time !== false){
			return date($this->date_format, $time);
		}
		return '';
	}
        
	/** Return the formated date */
	public function getDate(){
		$time = strtotime($this->timestamp);
		if($time !== false){
			return date('l, F j, Y', $time);
		}
		return '';
	}            
        
	/** Return the formated time */
	public function getTime(){
		$time = strtotime($this->timestamp);
		if($time !== false){
			return date('g:i a', $time);
		}
		return '';
	}  
	
	/** Adds other messages that are linked to this one.  Occurs when UDH headers link extended messages together. */
	public function addLinkedMessage($other = null){
		if($other instanceof ClickatellMessageBean){
			$this->moMsgId_linked[] = $other->id;
			$this->text .= ' ' . $other->text;
			$other->setInactive();			
		}
	}
	
	/** Returns message IDs that are linked to this message  */
	public function getLinkedMessageIds(){			
		$ids = $this->id;
		if(!empty($this->moMsgId_linked)){
			$ids .= ',' . implode(',', $this->moMsgId_linked);
		}
		return $ids;
	}
	
	/** Marks a message as inactive */
	public function setInactive(){
		$this->active_ind = '0';
	}	
	
	/** Identify a UDH header match between 2 messages */
	public function isUdhMatch($other){
		if($other instanceof ClickatellMessageBean){
			if(strlen($this->udh) > 10 && strlen($other->udh) > 10){
				return substr($this->udh, 0, 10) === substr($other->udh, 0, 10);
			}
		}
	}
	
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
        public function setName($n){
            $this->name = $n;
        }
        
        public function getName(){
            $isName = !empty($this->name);
            return ($isName ? $this->name .  '<br/><span style="font-size: 12px">' : '') . ClickatellUtils::format($this->from) . ($isName ? '</span>' : '');
        }
        
	public function __toString() {
		return 
			'ClickatellMessageBean: ' . 
			' id: ' .$this->id . 
			' date_format: ' .$this->date_format . 
			' active_ind: ' .$this->active_ind . 
			
			' api_id: ' .$this->api_id . 
			' moMsgId: ' .$this->moMsgId . 
			' moMsgId_parent: ' .$this->moMsgId_parent . 
			' moMsgId_linked: ' .implode(',', $this->moMsgId_linked) . 
			
			' to: ' .$this->to . 
			' from: ' .$this->from . 
                        ' name: ' .$this->name . 
			' text: ' .$this->text . 
			
			' charset: ' .$this->charset . 
			' udh: ' .$this->udh . 
			' timestamp: ' .$this->timestamp;		
	}	
};


?>