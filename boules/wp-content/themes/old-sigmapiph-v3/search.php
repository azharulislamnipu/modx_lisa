<?php get_header(); ?>

	<div id="content">
		<div class="padder">

		<?php do_action( 'sigma_before_blog_search' ); ?>

		<div class="page" id="blog-search" role="main">

			<?php if (have_posts()) : ?>

				<h2 class="purple color"><?php _e( 'Search Results', 'archon-network' ); ?></h2>
				<div class="contentBlock">

					<ul class="link-list">
					<?php
					 	while (have_posts()) : the_post();
							if ( can_view_content() ) : ?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php _e('Read', 'archon-network') ?>">
										<strong><?php the_title(); ?></strong>
										<span class="exceprt"><?php the_excerpt(); ?></span>
									</a>
								</li>
							<?php endif; ?>
						<?php endwhile; ?>
					</ul>

				</div>

			<?php else : ?>

				<h2 class="purple color"><?php _e( 'No matching content found...', 'archon-network' ); ?></h2>
				<div class="contentBlock">
				<?php get_search_form(); ?>
				</div>

			<?php endif; ?>

		</div>

		<?php do_action( 'sigma_after_blog_search' ); ?>

		</div><!-- .padder -->
	</div><!-- #content -->

	<?php get_sidebar(); ?>

<?php get_footer(); ?>
