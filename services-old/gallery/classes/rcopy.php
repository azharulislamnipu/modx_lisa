<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Recursive copy
 *
 * @package svManager
 */
class RCopy
{
   /**
  * Copy files from one directory to another one recursively. Function returns number of files copied.
  * Optionally attempts to set directory and file permissions
  *
  * @access public
  * @param string source directory path
  * @param string destination directory path
  * @return integer number of files copied
  */

  function doCopy($srcdir, $dstdir)
  {
    $n = 0;
    if(!is_dir($dstdir))
    {
      if (!@mkdir($dstdir))
      {
        trigger_error('unable to create directory '.$dstdir, E_USER_ERROR);
        return 0;
      }
      if (NEW_GALLERY_CHMOD === true)
      {
        @chmod($dstdir, NEW_GALLERY_DIR_MODE);
      }
    }
    if(!@($curdir = opendir($srcdir)))
    {
      trigger_error('failed to open directory'.rtrim(SV_MASTER_PATH, '\\/'), E_USER_ERROR);
      return 0;
    }
    while(false !== ($file = readdir($curdir)))
    {
      if($file[0] == '.') continue;
      $srcfile = $srcdir . DIRECTORY_SEPARATOR . $file;
      $dstfile = $dstdir . DIRECTORY_SEPARATOR . $file;
      if(is_file($srcfile))
      {
        $n++;
        if(!@copy($srcfile, $dstfile))
        {
          trigger_error('failed to copy '.$file, E_USER_WARNING);
        }
        else if (NEW_GALLERY_CHMOD === true)
        {
          @chmod($dstfile, NEW_GALLERY_FILE_MODE);
        }
      }
      else if(is_dir($srcfile))
      {
        $n += $this->doCopy($srcfile, $dstfile);
      }
    }
    closedir($curdir);
    return $n;
  }
}