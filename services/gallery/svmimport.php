<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */

header('Content-Type: text/html; charset=utf-8');
require 'includes'.DIRECTORY_SEPARATOR.'constants.php';
error_reporting(DEBUG ? E_ALL : E_ERROR);
require 'classes'.DIRECTORY_SEPARATOR.'errorhandler.php';
require 'classes'.DIRECTORY_SEPARATOR.'setup.php';
require 'classes'.DIRECTORY_SEPARATOR.'page.php';
require 'classes'.DIRECTORY_SEPARATOR.'importpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'loginpage.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryfactory.php';
require 'classes'.DIRECTORY_SEPARATOR.'galleryset.php';
require 'classes'.DIRECTORY_SEPARATOR.'gallery.php';
require 'classes'.DIRECTORY_SEPARATOR.'image.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlcheck.php';
require 'classes'.DIRECTORY_SEPARATOR.'auth.php';
require 'includes'.DIRECTORY_SEPARATOR.'phpcompat.php';
require 'classes'.DIRECTORY_SEPARATOR.'htmlentitydecode.php';
require 'classes'.DIRECTORY_SEPARATOR.'rcopy.php';
require 'classes'.DIRECTORY_SEPARATOR.'pathparser.php';
require 'classes'.DIRECTORY_SEPARATOR.'xmlparser.php';
$supportedGalleries = array();
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer'.DIRECTORY_SEPARATOR.'svimage.php';
  $supportedGalleries[] = 'simpleviewer';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2gallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2image.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'sv2imagespage.php';
  $supportedGallery[] = 'simpleviewer2';
}

if (file_exists('plugins'.DIRECTORY_SEPARATOR.'tiltviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvimage.php';
  $supportedGalleries[] = 'tiltviewer';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'autoviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avimage.php';
  $supportedGalleries[] = 'autoviewer';
}
if (file_exists('plugins'.DIRECTORY_SEPARATOR.'postcardviewer'))
{
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'settings.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvgallery.php';
  require 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvimage.php';
  $supportedGalleries[] = 'postcardviewer';
}
$setup = new Setup();
$errorHandler = & new ErrorHandler();
$auth = new Auth();
$galleryFactory = new GalleryFactory();
$setup->checkLogin($auth);
$page = new ImportPage();
print $page->getHtmlHead();
print $page->getPageHeader();
$gallerySet = new GallerySet();
if (isset($_POST['importpath']))
{
  $galleryRef = $gallerySet->nextImportedGalleryRef();
  $galleryType = $gallerySet->importGallery($_POST['importpath'], $supportedGalleries);
  if ($galleryType !== false)
  { 
    $galleryTitle = 'Imported '.$galleryType.' gallery';
    $galleryPath = $gallerySet->relPathFromRootUrl($_POST['importpath']);
    $gallery = $galleryFactory->makeGallery($galleryType, $galleryRef, $galleryPath, false);
    if ($galleryType != 'simpleviewer'  && $galleryType != 'simpleviewer2')
    {
      $gallery->rebuild(); //create thumbnails
    }
    $galleryIndex = $gallerySet->addGalleriesDataRecord($gallery, $galleryType);
    $galleryTitle = $gallery->getGalleryTitle();
    $gallerySet->setGalleriesDataTitle($galleryIndex, $galleryTitle);
    $gallerySet->saveGalleriesData();
    $gallery->saveGallery();
    trigger_error($galleryType.' gallery has been imported', E_USER_NOTICE);
  }
}
ob_start();
print '<h2>Import Gallery</h2>';
print $page->getImportHtml();
$mainOutput = ob_get_clean();
print $errorHandler->getMessages();
print $mainOutput;

print $page->getFooter();
?>