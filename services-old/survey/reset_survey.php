<?php require_once('../Connections/limesurvey.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_limesurvey = new KT_connection($limesurvey, $database_limesurvey);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

// Start trigger
$formValidation1 = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation1);
// End trigger

//start Trigger_LinkTransactions trigger
//remove this line if you want to edit the code by hand 
function Trigger_LinkTransactions(&$tNG) {
	global $upd_lime_survey_51728;
  $linkObj = new tNG_LinkedTrans($tNG, $upd_lime_survey_51728);
  $linkObj->setLink("token");
  return $linkObj->Execute();
}
//end Trigger_LinkTransactions trigger

// Make an update transaction instance
$upd_lime_survey_51728 = new tNG_update($conn_limesurvey);
$tNGs->addTransaction($upd_lime_survey_51728);
// Register triggers
$upd_lime_survey_51728->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update2");
$upd_lime_survey_51728->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_lime_survey_51728->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/strategic-plan-reporting.php");
// Add columns
$upd_lime_survey_51728->setTable("lime_survey_51728");
$upd_lime_survey_51728->addColumn("submitdate", "DATE_TYPE", "POST", "submitdate");
$upd_lime_survey_51728->addColumn("token", "STRING_TYPE", "CURRVAL", "token");
$upd_lime_survey_51728->setPrimaryKey("token", "STRING_TYPE", "GET", "token");

// Make an update transaction instance
$upd_lime_tokens_51728 = new tNG_update($conn_limesurvey);
$tNGs->addTransaction($upd_lime_tokens_51728);
// Register triggers
$upd_lime_tokens_51728->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update2");
$upd_lime_tokens_51728->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation1);
$upd_lime_tokens_51728->registerTrigger("AFTER", "Trigger_LinkTransactions", 98);
$upd_lime_tokens_51728->registerTrigger("ERROR", "Trigger_LinkTransactions", 98);
// Add columns
$upd_lime_tokens_51728->setTable("lime_tokens_51728");
$upd_lime_tokens_51728->addColumn("completed", "STRING_TYPE", "POST", "completed");
$upd_lime_tokens_51728->setPrimaryKey("token", "STRING_TYPE", "GET", "token");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rslime_survey_51728 = $tNGs->getRecordset("lime_survey_51728");
$row_rslime_survey_51728 = mysql_fetch_assoc($rslime_survey_51728);
$totalRows_rslime_survey_51728 = mysql_num_rows($rslime_survey_51728);

// Get the transaction recordset
$rslime_tokens_51728 = $tNGs->getRecordset("lime_tokens_51728");
$row_rslime_tokens_51728 = mysql_fetch_assoc($rslime_tokens_51728);
$totalRows_rslime_tokens_51728 = mysql_num_rows($rslime_tokens_51728);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
  <?php
	echo $tNGs->getErrorMsg();
?>
<p>Click the button below to reset your Strategic Plan report. This will allow you to edit your existing report.</p>
<form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
  <table cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr class="KT_buttons">
  <input type="hidden" name="submitdate" id="submitdate" value="<?php $a = NULL; echo $a; ?>" />
  <input type="hidden" name="completed" id="completed" value="<?php echo KT_escapeAttribute('N'); ?>" />
      <td colspan="2"><input type="submit" name="KT_Update2" id="KT_Update2" value="Reset Strategic Plan Report" />
      </td>
    </tr>
  </table>
</form>
 <!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
