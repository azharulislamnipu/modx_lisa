<?php


/***************************************************************
  Name: FdmDocmanager
  Description: Class for editing/creating/duplicating/deleting FdmDocuments
	Based on the work of ur001,DocumentManager
  Version 0.5.4m modified for Front-End Document Manager (FDM) by Soda
  Author: ur001
  e-mail: ur001@mail.ru
**************************************************************/

class FdmDocument{
	var $fields;	// doc fields array
	var $tvs;		// TV array
	var $tvNames;	// TV names array
	var $oldTVs;	// TV values array
	var $isNew;		// true - new doc, false - existing doc

	/***********************************************
	  Initializing class
	  $id   - existing doc id or 0 for new doc
	  $fields - comma delimited field list
	************************************************/
	function FdmDocument($id=0,$fields="*"){
		global $modx;
		$this->isNew = $id==0;
		if(!$this->isNew){
			$this->fields = $modx->getPageInfo($id,0,$fields);
			$this->fields['id']=$id;
		} else {
			$this->fields = array(
				'type' =>'document',
				'contentType' =>'text/html',
				'pagetitle'	=> ' ',
				'longtitle'	=> '',
				'description' => '',
				'alias'		=> '',
				'link_attributes' => '',
				'published' =>'0',
				'pub_date' =>'0',
 				'unpub_date' =>'0',
				'parent'	=> 0,
				'isfolder' =>'0',
				'introtext' =>'',
				'content' 	=> '',
				'richtext' =>'0',
				'template' => $modx->config['default_template'],
				'menuindex' =>'0',
				'searchable' => $modx->config['search_default'],
				'cacheable'=> $modx->config['cache_default'],
				'createdby'=>'0',
				'createdon'=> time(),
				'editedby' =>'0',
				'editedon'=>'0',
				'deleted'=> '0',
				'deletedon'=> '0',
				'deletedby'=> '0',
				'publishedon'=> '0',
				'publishedby'=> '0',
				'menutitle'=> '',
				'donthit'=> '0',
				'haskeywords'=> '0',
				'hasmetatags'=> '0',
				'privateweb'=> '0',
				'privatemgr'=> '0',
				'content_dispo'=> '0',
				'hidemenu'=> '0'
			);
		}
	}


	function SetA($tabFieldValue){
		foreach 	($tabFieldValue as $field=> $value){
			$this->Set($field,$value);
		}
	}


	/***********************************************
	  Receiving doc values or TV
	  $field - doc value or TV with 'tv' prefix
	  Result: doc value, TV or null
	************************************************/
	function Get($field){
		switch(1){
			case substr($field,0,2)=='tv': return $this->GetTV(substr($field,2));
			default: return isset($this->fields[$field]) ? $this->fields[$field] : null;
		}
	}


	/***********************************************
	  Receiving TV
	  $name - TV name
	************************************************/
	function GetTV($tv){
		if(!is_array($this->tvs)){
			if($this->isNew) return null;
			$this->tvs=array();
		}
		if(isset($this->tvs[$tv])) return $this->tvs[$tv];
		if(!is_array($this->oldTVs)){
			if($this->isNew) return null;
			$this->oldTVs=$this->fillOldTVValues();
		}
		if(isset($this->oldTVs[$tv])) return $this->oldTVs[$tv];
		return null;
	}

	/****************************************************************
	  Result: doc value and TV in array tv with 'tv' before key name
	*****************************************************************/

	function Gets(){
		if (!is_array ($this->oldTVs)) $this->oldTVs=$this->fillOldTVValues();
		$tvs=is_array($tv)?$tvs:array();
		if (is_array ($this->oldTVs)){
			foreach ($this->oldTVs as $k=>$v){
				$t='tv'.$k;
  			$tvs[$t]=$v;
			}
		}
		return array_merge ($tvs, $this->fields);
	}

		function Affiche($t=''){
			echo"\n<h3>FdmDocument - $t -</h3>\n<pre>\n";
			echo htmlentities(print_r($this));
			echo"\n</pre>\n";
	}
	/***********************************************
	  Setting doc or TV value
	  $field - doc or TV (with prefix 'tv') name
	  $value - value
	  Result: true or false
	  Added: Set only if fields exist
	************************************************/
	function Set($field, $value){
		global $modx;
		switch(1){
			case $field=='id':return false;
			case substr($field,0,2)=='tv':		return $this->SetTV(substr($field,2), $value);
			case $field=='template':		return $this->SetTemplate($value);
			default:
			if (array_key_exists($field, $this->fields)){
				return $this->fields[$field]=$value;
			}
			return false;
		}
	}
	function SetAlias($alias=''){
		global $modx;
		$charset=$modx->config['modx_charset'];
		$alias= mb_strtolower($this->stripAlias(trim($alias)),$charset);
		$alias=$alias?$alias:$this->fields['pagetitle'];
		return $alias;

		/*
		$id=$this->fields['id'] ?$this->fields['id']:0;
		//if ($modx->config['friendly_urls']==1) {// auto assign alias
		//TODO can set a TV in alias, field must be 'tv'tvname
			$alias =empty($alias)?$this->fields['pagetitle']: $alias;
			//$alias=strtolower($this->stripAlias(trim($alias))); // break with utf-8
			$charset=$modx->config['modx_charset'];
$alias=mb_strtolower($this->stripAlias(trim($alias)),$charset); //need php 4.3.0
			$parent=$this->fields['parent'];
			// WHERE id<>'$id'
			if ($modx->config['use_alias_path']=='1')$sqlw=" AND parent='$parent'";
			// Check for duplicates on the same level if alias_path is on  else in all the tree(netnoise 2006/08/14)
			$cnt = $modx->db->getValue("SELECT COUNT(*) FROM " . $modx->getFullTableName("site_content") . " WHERE id<>'$id' AND alias = '$alias'".$sqlw);
			if ($cnt != 0){
				$cpt=0;
				do{
					$cpt+=1;
					$t=$alias.$cpt;
					$cnt = $modx->db->getValue("SELECT COUNT(*) FROM " . $modx->getFullTableName("site_content") . " WHERE id<>'$id' AND alias = '$t'".$sqlw);
					//$this->Set('description',$this->Get('description')."<p>id: $id parent: $parent cnt:$cnt alias: $alias  cpt: $cpt -$t</p>\n".print_r($cnt));
				}	while ($cnt>0 || $cpt>50);

				$alias.=$cpt;
			}
		//} else {
		//$alias=$id;
		//}
		return $this->fields['alias']=$alias;
		*/
	}

	function makeUrl(){
		global $modx;
		$siteUrl='';//str_replace ('/assets/','',$modx->config['rb_base_url']);
		if ($modx->config['friendly_urls'] == 1 ) {
			$docalias= $modx->config['friendly_url_prefix'] . $this->fields['alias'] . $modx->config['friendly_url_suffix'];
			if ($modx->config['friendly_alias_urls'] == 1) {
				// $tpath=array_keys($modx->getParentIds($this->fields['id'])); // grrrr don't work
				$path=$this->getPath();
			}
			$relUrl=$path.'/'.$docalias;
			$url= $siteUrl.str_replace ('//','/',$relUrl);
		} else {
				$url= 'index.php?id=' . $this->fields['id'] ;
		}
	return $url;
	}

	function getPath(){
		global $modx;
		$id2=$this->fields['parent'];
		$cpt=0;
		while ($parent!=0 || $cpt<10){
			$result = $modx->db->select('alias,parent', $modx->getFullTableName('site_content'), "id='$id2'");
   		if($modx->db->getRecordCount( $result ) >= 1 ) {
				$row = $modx->db->getRow( $result );
				$tpath[]=$row['alias'];
				$id2=$parent=$row['parent'];
			}
		$cpt+=1;
		}
		if (is_array($tpath)) return '/'.join('/',array_reverse($tpath)); else return '';

	}

	function stripAlias($alias) {// core fct
		global $modx;
		if (strtoupper($modx->config['modx_charset']) == 'UTF-8'){
			$replace_array = array(' ' => '-','&' => 'and','\'' => '','À' => 'A','À' => 'A','Á' => 'A','Á' => 'A','Â' => 'A','Â' => 'A','Ã' => 'A','Ã' => 'A','Ä' => 'e','Ä' => 'A','Å' => 'A','Å' => 'A','Æ' => 'e','Æ' => 'E','Ā' => 'A','Ą' => 'A','Ă' => 'A','Ç' => 'C','Ç' => 'C','Ć' => 'C','Č' => 'C','Ĉ' => 'C','Ċ' => 'C','Ď' => 'D','Đ' => 'D','È' => 'E','È' => 'E','É' => 'E','É' => 'E','Ê' => 'E','Ê' => 'E','Ë' => 'E','Ë' => 'E','Ē' => 'E','Ę' => 'E','Ě' => 'E','Ĕ' => 'E','Ė' => 'E','Ĝ' => 'G','Ğ' => 'G','Ġ' => 'G','Ģ' => 'G','Ĥ' => 'H','Ħ' => 'H','Ì' => 'I','Ì' => 'I','Í' => 'I','Í' => 'I','Î' => 'I','Î' => 'I','Ï' => 'I','Ï' => 'I','Ī' => 'I','Ĩ' => 'I','Ĭ' => 'I','Į' => 'I','İ' => 'I','Ĳ' => 'J','Ĵ' => 'J','Ķ' => 'K','Ľ' => 'K','Ĺ' => 'K','Ļ' => 'K','Ŀ' => 'K','Ñ' => 'N','Ñ' => 'N','Ń' => 'N','Ň' => 'N','Ņ' => 'N','Ŋ' => 'N','Ò' => 'O','Ò' => 'O','Ó' => 'O','Ó' => 'O','Ô' => 'O','Ô' => 'O','Õ' => 'O','Õ' => 'O','Ö' => 'e','Ö' => 'e','Ø' => 'O','Ø' => 'O','Ō' => 'O','Ő' => 'O','Ŏ' => 'O','Œ' => 'E','Ŕ' => 'R','Ř' => 'R','Ŗ' => 'R','Ś' => 'S','Ş' => 'S','Ŝ' => 'S','Ș' => 'S','Ť' => 'T','Ţ' => 'T','Ŧ' => 'T','Ț' => 'T','Ù' => 'U','Ù' => 'U','Ú' => 'U','Ú' => 'U','Û' => 'U','Û' => 'U','Ü' => 'e','Ū' => 'U','Ü' => 'e','Ů' => 'U','Ű' => 'U','Ŭ' => 'U','Ũ' => 'U','Ų' => 'U','Ŵ' => 'W','Ŷ' => 'Y','Ÿ' => 'Y','Ź' => 'Z','Ż' => 'Z','à' => 'a','á' => 'a','â' => 'a','ã' => 'a','ä' => 'e','ä' => 'e','å' => 'a','ā' => 'a','ą' => 'a','ă' => 'a','å' => 'a','æ' => 'e','ç' => 'c','ć' => 'c','č' => 'c','ĉ' => 'c','ċ' => 'c','ď' => 'd','đ' => 'd','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e','ē' => 'e','ę' => 'e','ě' => 'e','ĕ' => 'e','ė' => 'e','ƒ' => 'f','ĝ' => 'g','ğ' => 'g','ġ' => 'g','ģ' => 'g','ĥ' => 'h','ħ' => 'h','ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ī' => 'i','ĩ' => 'i','ĭ' => 'i','į' => 'i','ı' => 'i','ĳ' => 'j','ĵ' => 'j','ķ' => 'k','ĸ' => 'k','ł' => 'l','ľ' => 'l','ĺ' => 'l','ļ' => 'l','ŀ' => 'l','ñ' => 'n','ń' => 'n','ň' => 'n','ņ' => 'n','ŉ' => 'n','ŋ' => 'n','ò' => 'o','ó' => 'o','ô' => 'o','õ' => 'o','ö' => 'e','ö' => 'e','ø' => 'o','ō' => 'o','ő' => 'o','ŏ' => 'o','œ' => 'e','ŕ' => 'r','ř' => 'r','ŗ' => 'r','ù' => 'u','ú' => 'u','û' => 'u','ü' => 'e','ū' => 'u','ü' => 'e','ů' => 'u','ű' => 'u','ŭ' => 'u','ũ' => 'u','ų' => 'u','ŵ' => 'w','ÿ' => 'y','ŷ' => 'y','ż' => 'z','ź' => 'z','ß' => 's','ſ' => 's','Α' => 'A','Ά' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D','Ε' => 'E','Έ' => 'E','Ζ' => 'Z','Η' => 'I','Ή' => 'I','Θ' => 'TH','Ι' => 'I','Ί' => 'I','Ϊ' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M','Ν' => 'N','Ξ' => 'KS','Ο' => 'O','Ό' => 'O','Π' => 'P','Ρ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Ύ' => 'Y','Ϋ' => 'Y','Φ' => 'F','Χ' => 'X','Ψ' => 'PS','Ω' => 'O','Ώ' => 'O','α' => 'a','ά' => 'a','β' => 'b','γ' => 'g','δ' => 'd','ε' => 'e','έ' => 'e','ζ' => 'z','η' => 'i','ή' => 'i','θ' => 'th','ι' => 'i','ί' => 'i','ϊ' => 'i','ΐ' => 'i','κ' => 'k','λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'ks','ο' => 'o','ό' => 'o','π' => 'p','ρ' => 'r','σ' => 's','τ' => 't','υ' => 'y','ύ' => 'y','ϋ' => 'y','ΰ' => 'y','φ' => 'f','χ' => 'x','ψ' => 'ps','ω' => 'o','ώ' => 'o',
//russian chars
			'а'=>'a', 'б'=>'b','в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e','ё'=>'e', 'ж'=>'j', 'з'=>'z', 'и'=>'i','й'=>'y', 'к'=>'k', 'л'=>'l', 'м'=>'m','н'=>'n', 'о'=>'o', 'п'=>'p', 'р'=>'r','с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f','х'=>'h', 'ц'=>'ts' ,'ч'=>'ch', 'ш'=>'sh','щ'=>'sch', 'ъ'=>"'" ,'ы'=>'i', 'ь'=>"'",'э'=>'е', 'ю'=>'yu', 'я'=>'ya', 'А'=>'A','Б'=>'B', 'В'=>'V', 'Г'=>'G', 'Д'=>'D','Е'=>'E', 'Ё'=>'E', 'Ж'=>'J', 'З'=>'Z','И'=>'I', 'Й'=>'Y', 'К'=>'K', 'Л'=>'L','М'=>'M', 'Н'=>'N', 'О'=>'O', 'П'=>'P','Р'=>'R', 'С'=>'S', 'Т'=>'T', 'У'=>'U','Ф'=>'F', 'Х'=>'H', 'Ц'=>'TS', 'Ч'=>'CH','Ш'=>'SH' ,'Щ'=>'SCH', 'Ъ'=>"'", 'Ы'=>'I','Ь'=>"'", 'Э'=>'Е','Ю'=>'YU', 'Я'=>'YA');
    $alias = strtr($alias, $replace_array);  //added bug 874
    }
    $alias = strip_tags($alias);
    $alias = preg_replace('/&.+?;/', '', $alias); // kill entities
    $alias = preg_replace('/[^\.%A-Za-z0-9 _-]/', '', $alias);
    $alias = preg_replace('/\s+/', '_', $alias);
    $alias = preg_replace('|-+|', '_', $alias);
    $alias = trim($alias, '_');
		if (!$alias) $alias=$this->fields['id'];
    return $alias;
}
	/***********************************************
	  Setting TV value
	  Added: Set only if Tv is available
	************************************************/
	function SetTV($tv,$value){
		if(!is_array($this->tvs)) $this->tvs=array();
		if(!is_array($this->tvNames)) $this->fillTVNames();

		// http://modxcms.com/forums/index.php/topic,14977.msg108314.html#msg108314 by swit4er

		if (isset($this->tvNames[$tv]))
		{
			if (is_array($value)) {
			$this->tvs[$tv]=implode("||",$value);
			}
			else {$this->tvs[$tv]=$value;}
		}
	}


	/***********************************************
	  Setting doc template
	  $tpl - template name or id
	************************************************/
	function SetTemplate($tpl=''){
		global $modx;
		// on  0.9.6 default template is set to 1 in system_setting, but minimal template has id 3 .Reselect it in config !
//		if (!$tpl) $tpl=$modx->config['default_template'];
		if(!is_numeric($tpl)) {
			$tablename=$modx->getFullTableName('site_templates');
			$tpl = $modx->db->getValue("SELECT id FROM $tablename WHERE templatename='$tpl' LIMIT 1");
			if(empty($tpl)) return false;
		}
		$this->fields['template']=$tpl;
		return true;
	}


	/************************************************************
	  Duplicatig doc with TVs
	  Added: User
	*************************************************************/
	function Duplicate($user='0',$parent='0'){
		global $modx;
		//$oid=$this->Get('id');
    //$pid=$this->Get('parent');

		if($this->isNew) return;

		$all_tvs=$this->fillOldTVValues();
		foreach($all_tvs as $tv=>$value)
			if(!isset($this->tvs[$tv])) $this->tvs[$tv]=$value;
		$this->oldTVs=array();
		$this->isNew=true;
		unset($this->fields['id']);

		if ($parent) $this->fields['parent']=$parent;
		if ($user)   $this->fields['createdby']=$user;
    $this->fields['createdon']=time();

	}

 	/***************************************************
	  Saving/Updating FdmDocument (escape fields added)
	****************************************************/
	function Save(){ //escape added
		global $modx;
		$this->fields['editedon']=time();
		$tablename=$modx->getFullTableName('site_content');
	  	$escapedFields = array_map(create_function('$n', 'global $modx;if (get_magic_quotes_gpc()) { $n = stripslashes($n);	}return $modx->db->escape($n);'), $this->fields);
		if($this->isNew){
			$this->fields['id']=$modx->db->insert($escapedFields, $tablename);
			$this->isNew = false;
		} else {
			$id=$this->fields['id'];
			$modx->db->update($escapedFields, $tablename, "id=$id");
		}
		unset($escapedFields);// free the array
		if(is_array($this->tvs)) $this->saveTVs();
	}

	/************************************************************
	  Saving TV values, maintenance function. Only $tvNames values are saved,
        If a TV exists in oldTVs, then updating, else inserting
	*************************************************************/
	function saveTVs(){
		global $modx;
		if(!is_array($this->tvNames))$this->fillTVNames();
		if(!is_array($this->oldTVs) && !$this->isNew)
			$this->oldTVs=$this->fillOldTVValues();
//		else 							//http://modxcms.com/forums/index.php/topic,6334.msg92335.html#msg92335
//			$this->oldTVs = array();
		elseif ( !is_array( $this->oldTVs ) )
    	$this->oldTVs = array();
		$tvc = $modx->getFullTableName('site_tmplvar_contentvalues');
		$id=$this->fields['id'];
		foreach($this->tvs as $tv=>$value){
			if (get_magic_quotes_gpc()) { // anti sql injection
  				$value = stripslashes($value);
			}
			$escapedValue= $modx->db->escape($value); // sql escape the value
			if(isset($this->tvNames[$tv])){
				$tmplvarid=$this->tvNames[$tv];
				if(isset($this->oldTVs[$tv])){
					if($this->oldTVs[$tv]==$this->tvNames[$tv]) continue;
					$sql="UPDATE $tvc SET value='$escapedValue' WHERE tmplvarid=$tmplvarid AND contentid=$id";
				}else{
					$sql="INSERT INTO $tvc (tmplvarid,value,contentid) VALUES ($tmplvarid,'$escapedValue',$id)";
				}
			$modx->db->query($sql);
			}
		}
	}


	function Delete($user='',$real=false){
		if($this->isNew) return;
		global $modx;
		$pid=$this->fields['parent'];
		$id=$this->fields['id'];
		if ($real){
			$id=$this->fields['id'];
			$modx->db->delete($modx->getFullTableName('site_content'),"id=$id");
			$modx->db->delete($modx->getFullTableName('site_tmplvar_contentvalues'),"contentid=$id");
			// add gallery ?
			$this->isNew=true;
		} else {
			$this->Set('deleted','1');
			$this->Set('deletedon',time());
			$this->Set('deletedby',$user);
			$this->Save();
		}
		return ($pid);
	}

	function Publish($user=''){
		global $modx;
		$this->Set('published',1);
		$this->Set('publishedon',time());
		$this->Set('publishedby',$user);
		$this->Save();
		$pid=$this->Get('parent');
		$id=$this->Get('id');
    }
	function UnPublish($user=''){
		global $modx;
		$this->Set('published',0);
		$this->Save();
		$pid=$this->Get('parent');
		$id=$this->Get('id');

		return ($pid);
    }

	/************************************************************
	  Filling TV array ($oldTVs), maintenance function.
	  Differs from $modx->getTemplateVars
	*************************************************************/
	function fillOldTVValues(){
		global $modx;
		$tvc = $modx->getFullTableName('site_tmplvar_contentvalues');
		$tvs = $modx->getFullTableName('site_tmplvars');
		$sql = 'SELECT tvs.name as name, tvc.value as value '.
		       "FROM $tvc tvc INNER JOIN $tvs tvs ".
			   'ON tvs.id=tvc.tmplvarid WHERE tvc.contentid ='.$this->fields['id'];
		$result = $modx->db->query($sql);
		$TVs = array();
		while ($row = mysql_fetch_assoc($result))$TVs[$row['name']] = $row['value'];
		return $TVs;
	}

	/************************************************************
	  Fillin TV names array ($tvNames)), maintenance function.
	*************************************************************/
	function fillTVNames(){
		global $modx;
		$this->tvNames = array();
		$result = $modx->db->select('id, name', $modx->getFullTableName('site_tmplvars'));
		while ($row = mysql_fetch_assoc($result)) $this->tvNames[$row['name']] = $row['id'];
	}

	function flushcache(){
		global $modx;
		include_once $modx->config['base_path'].'manager/processors/cache_sync.class.processor.php';
    $sync = new synccache();
    $sync->setCachepath($modx->config['base_path'].'assets/cache/');
    $sync->setReport(false);
    $sync->emptyCache(); // first empty the cache
//$modx->clearCache();
	}

}// end Class


?>
