<?php
/*
	Copyright (c) InterAKT Online 2000-2006. All rights reserved.
*/

class XML_Export {
	var $filename;
	var $columns;
	var $recordset;
	var $maxRecords;
	var $dbEncoding;
	var $xmlEncoding;
	var $xmlFormat;
	
	function XML_Export() {
		$this->maxRecords = 'ALL';
		$this->xmlFormat = 'NODES';
		$this->columns = array();
		$this->filename = '';
	}

	function setRecordset(&$recordset) {
		if (is_resource($recordset)) {
			$localRs = new KT_Recordset($recordset);
		} else {
			$localRs = &$recordset;
		}
		$this->recordset = $localRs;
	}
	
	function setMaxRecords($maxRecords) {
		$this->maxRecords = trim($maxRecords);
	}
	
	function addColumn($column, $colType = '', $label = '') {
		$this->columns[trim($column)] = array('type' => trim($colType), 'label' => trim($label));
	}
	
	function setFilename($filename) {
		$this->filename = trim($filename);
	}
	
	function setDBEncoding($dbEncoding) {
		$this->dbEncoding = trim($dbEncoding);
	}
	
	function setXMLEncoding($xmlEncoding) {
		$this->xmlEncoding = trim($xmlEncoding);
	}
	
	function setXMLFormat($xmlFormat) {
		$this->xmlFormat = strtoupper(trim($xmlFormat));
		if ($this->xmlFormat != 'ATTRIBUTES') {
			$this->xmlFormat == 'NODES';
		}
	}
	
	
	function Execute($method, $reference) {
		$ret = KT_getRealValue($method, $reference);
		if (!isset($ret)) {
			return;
		}

		if ( !isset($this->recordset) || !isset($this->recordset->fields) ) {
			die('<strong>XML_Export Error.</strong><br/>Passed argument is not a valid recordset.');
			return;
		}
		
		if (count($this->columns) < 1) {
			die('<strong>XML_Export Error.</strong><br/>No columns defined!');
			return;
		}
		
		$trans = array_flip(get_html_translation_table(HTML_ENTITIES));
		$numRow = 1;
		$document = '';
		$document .= '<'.'?xml version="1.0" encoding="' . $this->xmlEncoding . '"?'.'>';
		$document .= "\r\n";
		$document .= '<export>';
		$document .= "\r\n";
		while (!$this->recordset->EOF) {
			if ($this->maxRecords != 'ALL' && $this->maxRecords < $numRow) {
				break;
			}
			$row = "\t";
			$row .= '<row';
			if ($this->xmlFormat != 'ATTRIBUTES') {
				$row .= '>' . "\r\n";
			}
			foreach ($this->columns as $column => $details) {
				$colName = $column;
				if ($details['label'] != '') {
					$colName = $details['label'];
				}
				$value = $this->recordset->Fields($column);
				if ($details['type'] == 'DATE_TYPE') {
					$value = KT_formatDate($value);
				}
				
				$value = strtr($value, $trans);
				if ($this->xmlEncoding != $this->dbEncoding) {
					if (!function_exists('mb_convert_encoding')) {
						die('<strong>XML_Export Error.</strong><br/>Could not perform characters conversion. Function <strong>mb_convert_encoding</strong> is not available! Please enable the <strong>mbstring</strong> extension!');
					}
					$value = mb_convert_encoding($value, $this->xmlEncoding, $this->dbEncoding);
				}
				$value = str_replace(array('&', '>', '<', '"'), array('&amp;', '&gt;', '&lt;', '&quot;'), $value);
				
				if ($this->xmlFormat == 'ATTRIBUTES') {
					$row .= ' ' . $colName . '="';
					$row .= $value;
					$row .= '"';
				} else {
					$row .= "\t\t";
					$row .= '<' . $colName . '>';
					$row .= $value;
					$row .= '</' . $colName . '>';
					$row .= "\r\n";
				}
			}
			if ($this->xmlFormat == 'ATTRIBUTES') {
				$row .= ' />';
			} else {
				$row .= "\t" . '</row>';
			}
			$document .= $row;
			$document .= "\r\n";
			$numRow++;
			$this->recordset->MoveNext();
		}
		$document .= '</export>';
		$size = strlen($document);
		$this->sendHeaders($size);
		echo $document;
		exit;
	}
	
	function sendHeaders($size) {
		if (headers_sent()) {
			die('Headers already sent! The XML cannot be exported');
		}
		header('Content-type: text/xml');
		header('Pragma: public');
		header('Cache-control: private');
		header('Expires: -1');
		header('Content-Length: ' . $size);
		if ($this->filename != '') {
			header('Content-disposition: attachment; filename="' . $this->filename . '";');
		}
	}
	
}

?>