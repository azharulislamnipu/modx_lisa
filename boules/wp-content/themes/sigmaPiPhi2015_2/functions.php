<?php

/*
 * Name:       functions.php
 * Purpose:    custom functions for the Sigma Pi Phi
 */


// Template action and filter definitions
require_once('_incl/functions/actions-filters.php');
require_once('_incl/functions/template-functions.php');


// Custom metadata for posts and terms
require_once('_incl/kc-settings/kc-settings.php');
require_once('_incl/sigma_post_settings.php');
require_once('_incl/sigma_term_settings.php');

// Classes
require_once('_incl/privacy_walker.php');


 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Templates content area width
if ( ! isset( $content_width ) ){
	$content_width = 610;
}

//add custom function 
require_once('custom_functions.php');

?>