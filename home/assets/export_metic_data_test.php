<?php

echo "Dumping session variable. <pre>";
var_dump($_SESSION);

die();


$base_path = str_replace('/home', '', $_api_path);

$boulcd = $_SESSION['BOULECD'];
$user_id = $_SESSION['CUSTOMERCD'];

$boulcd = preg_replace('/([A-Z])/', ' $1', $boulcd);
$boulcd = trim($boulcd);

$include_folder = $base_path . "includes";

include_once $include_folder . "/functions.php";


$eligible_values = array(
    'CHAPTERID' => 'BOULE NAME',
    'JOINDATE' => 'JOIN DATE',
    'PREFIX' => 'PREFIX',
    'FIRSTNAME' => 'FIRST NAME',
    'MIDDLEINITIAL' => 'MIDDLE INITIAL',
    'LASTNAME' => 'LAST NAME',
    'SUFFIX' => 'SUFFIX',
    'DESIGNATIONLST' => 'HONORIFIC',
    'ADDRESS1' => 'ADDRESS1',
    'ADDRESS2' => 'ADDRESS2',
    'CITY' => 'CITY',
    'STATECD' => 'STATE',
    'ZIP' => 'ZIP',
    'HOMEPHONE' => 'HOME PHONE',
    'MOBILEPHONE' => 'MOBILE PHONE',
    'EMAIL' => 'EMAIL',
    'MARITALSTT' => 'MARITAL STATUS',
    'BIRTHDATE' => 'BIRTH DATE',
    'DECEASEDDATE' => 'DECEASED DATE',
    'SPOUSENAME' => 'SPOUSE NAME',
);

$db = get_db();
$db->where ("CHAPTERID", $boulcd);
$bdata = $db->get('spp_archons', null, array_keys($eligible_values));


$referrer = empty($_SERVER['HTTP_REFERER']) ? '/home' : $_SERVER['HTTP_REFERER'];

if($db->count == 0){
    header('Location:  '.  $referrer);
    exit;
}


header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename='.$boulcd.' Data.csv');
$output = fopen("php://output", "w");
fputcsv($output, array_values($eligible_values));
$result = mysqli_query($con, $query);
foreach ($bdata as $index=>$data) {
    fputcsv($output, $data);
}
fclose($output);


die();