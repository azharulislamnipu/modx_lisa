<?php
$status = $user_data['STATUSSTT'];
?>
<div id="archon_request_status_change" class="accordion parent-elem request_status_change" data-accordion>
    <div data-control><h3 class="text-center">Request Status Change</h3></div>
    <div class="ymp_form_input" data-content>
        <div class="data-content-inner request_status_change_inner clearfix ymp_wrapper grid-12">
            <div class="ymp-row">
                <div class="input_wrapper clearfix">
                    <label class="span-6">Current Status:</label>
                    <span class="span-6"><?php echo $status;?></span>
                </div>
                <div class="input_wrapper clearfix">
                    <label class="span-6">Requested Status:  <span class="required_ind">*</span> <br> <span class="font_small">Please select the correct status for this Archon.</span></label>
                    <div class="span-6 radio_inputs">
                        <label>
                            <input type="radio" name="chnge_stat_to" value="Active" <?php checked('Active',$status);?>> Active
                        </label>
                        <label>
                            <input type="radio" name="chnge_stat_to" value="Deceased" <?php checked('Deceased',$status);?>> Deceased
                        </label>
                        <label>
                            <input type="radio" name="chnge_stat_to" value="Delinquent" <?php checked('Delinquent',$status);?>> Delinquent
                        </label>
                        <label>
                            <input type="radio" name="chnge_stat_to" value="Dropped" <?php checked('Dropped',$status);?>> Dropped
                        </label>
                        <label>
                            <input type="radio" name="chnge_stat_to" value="Emeritus" <?php checked('Emeritus',$status);?>> Emeritus
                        </label>
                        <label>
                            <input type="radio" name="chnge_stat_to" value="Involuntary Inactive" <?php checked('Involuntary Inactive',$status);?>> Involuntary Inactive (You will be asked if the related paper work has been submitted.)
                        </label>
                        <label>
                            <input type="radio" name="chnge_stat_to" value="Voluntary Inactive" <?php checked('Voluntary Inactive',$status);?>> Voluntary Inactive (You will be asked if the related paper work has been submitted.)
                        </label>
                        <label>
                            <input type="radio" name="chnge_stat_to" value="Widow" <?php checked('Widow',$status);?>> Widow (If the Archon has passed and leaves a widow, please select this status and list her name below.)
                        </label>
                    </div>
                </div>
                <div class="input_wrapper clearfix">
                    <label class="" for="req_cmt">REQUEST/COMMENT:   <span class="required_ind">*</span> <br> <span class="font_small">Please select the correct status for this Archon.</span></label>
                        <div class="clearfix">
                            <textarea class="input-required input-validated tiny-editable" cols="80" rows="10" name="req_cmt" id="req_cmt" ></textarea>
                        </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="KT_Insert1" value="SUBMIT REQUEST">
        <input type="hidden" name="visitor_id" value="<?php echo $visitor_id;?>">
        <input type="hidden" name="DM_EMAIL" value="<?php echo $dm_email;?>">
        <?php echo $submit_dv;?>
    </div>

</div>
<?php //echo htmlspecialchars_decode($user_data['BIO'], ENT_NOQUOTES);?>