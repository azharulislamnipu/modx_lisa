<?php
//Connection statement
require_once('../Connections/sigma_modx.php');

//Aditional Functions
require_once('../includes/functions.inc.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("JOINDATE", false, "date", "date", "", "", "Please enter the date the archon was installed.");
$formValidation->addField("TERMINATIONDATE", true, "date", "date", "", "", "Please enter the date the archon left office.");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("{KT_defaultSender}");
  $emailObj->setTo("info@sigmapiphi.org");
  $emailObj->setCC("{SESSION.webEmail}");
  $emailObj->setBCC("");
  $emailObj->setSubject("{SESSION.BOULENAME} Boule Officer Has Been Removed");
  //WriteContent method
  $emailObj->setContent("Archon {SESSION.webFullname}, Grammateus of {SESSION.BOULENAME} has made the following update: \n\nArchon {rsBouleOfficerDetail.FIRSTNAME} {rsBouleOfficerDetail.LASTNAME} of {rsBouleOfficerDetail.BOULENAME} no longer holds the office of {CPOSITION}.\n\nDate Processed: {NOW_DT}");
  $emailObj->setEncoding("ISO-8859-1");
  $emailObj->setFormat("Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

// begin Recordset
$colname__rsBouleMembers = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsBouleMembers = $_SESSION['BOULENAME'];
}
$query_rsBouleMembers = sprintf("SELECT * FROM vw_gramm_archons WHERE BOULENAME = %s", GetSQLValueString($colname__rsBouleMembers, "text"));
$rsBouleMembers = $sigma_modx->SelectLimit($query_rsBouleMembers) or die($sigma_modx->ErrorMsg());
$totalRows_rsBouleMembers = $rsBouleMembers->RecordCount();
// end Recordset

// begin Recordset
$query_rsBouleOffices = "SELECT * FROM spp_cposition WHERE CPOSITION not like '%grand%' and CPOSITION not like '%regional%' ORDER BY CPOSITION ASC";
$rsBouleOffices = $sigma_modx->SelectLimit($query_rsBouleOffices) or die($sigma_modx->ErrorMsg());
$totalRows_rsBouleOffices = $rsBouleOffices->RecordCount();
// end Recordset

// begin Recordset
$colname__rsBouleOfficerDetail = '-1';
if (isset($_GET['OFFICERID'])) {
  $colname__rsBouleOfficerDetail = $_GET['OFFICERID'];
}
$query_rsBouleOfficerDetail = sprintf("SELECT * FROM vw_boule_officers WHERE OFFICERID = %s", GetSQLValueString($colname__rsBouleOfficerDetail, "int"));
$rsBouleOfficerDetail = $sigma_modx->SelectLimit($query_rsBouleOfficerDetail) or die($sigma_modx->ErrorMsg());
$totalRows_rsBouleOfficerDetail = $rsBouleOfficerDetail->RecordCount();
// end Recordset

// Make an insert transaction instance
$ins_spp_officers = new tNG_multipleInsert($sigma_modx);
$tNGs->addTransaction($ins_spp_officers);
// Register triggers
$ins_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_spp_officers->setTable("spp_officers");
$ins_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "VALUE", "");
$ins_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "VALUE", "");
$ins_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_officers = new tNG_multipleUpdate($sigma_modx);
$tNGs->addTransaction($upd_spp_officers);
// Register triggers
$upd_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_officers->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
$upd_spp_officers->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$upd_spp_officers->setTable("spp_officers");
$upd_spp_officers->addColumn("CUSTOMERCD", "NUMERIC_TYPE", "CURRVAL", "");
$upd_spp_officers->addColumn("CPOSITION", "STRING_TYPE", "CURRVAL", "");
$upd_spp_officers->addColumn("JOINDATE", "DATE_TYPE", "CURRVAL", "");
$upd_spp_officers->addColumn("TERMINATIONDATE", "DATE_TYPE", "POST", "TERMINATIONDATE");
$upd_spp_officers->addColumn("COMMITTEESTATUSSTT", "STRING_TYPE", "POST", "COMMITTEESTATUSSTT");
$upd_spp_officers->addColumn("UPDATEUSERCD", "STRING_TYPE", "CURRVAL", "");
$upd_spp_officers->addColumn("UPDATETMS", "DATE_TYPE", "CURRVAL", "");
$upd_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE", "GET", "OFFICERID");

// Make an instance of the transaction object
$del_spp_officers = new tNG_multipleDelete($sigma_modx);
$tNGs->addTransaction($del_spp_officers);
// Register triggers
$del_spp_officers->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_spp_officers->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_spp_officers->setTable("spp_officers");
$del_spp_officers->setPrimaryKey("OFFICERID", "NUMERIC_TYPE", "GET", "OFFICERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_officers = $tNGs->getRecordset("spp_officers");
$totalRows_rsspp_officers = $rsspp_officers->RecordCount();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
</head>
<body>

<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['OFFICERID'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    [*pagetitle*] </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php
  while (!$rsspp_officers->EOF) { 
?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_officers > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <?php 
// Show IF Conditional show_CUSTOMERCD_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
            <tr>
              <td class="KT_th">ARCHON:</td>
              <td><strong><?php echo $rsBouleOfficerDetail->Fields('FULLNAME'); ?></strong></td>
            </tr>
            <?php } 
// endif Conditional show_CUSTOMERCD_on_update_only
?>
<tr>
            <td class="KT_th">OFFICE:</td>
            <td><?php echo KT_escapeAttribute($rsspp_officers->Fields('CPOSITION')); ?></td>
          </tr>
          <tr>
            <td class="KT_th">START DATE:</td>
            <td><?php echo KT_formatDate($rsspp_officers->Fields('JOINDATE')); ?></td>
          </tr>
          <?php 
// Show IF Conditional show_TERMINATIONDATE_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="TERMINATIONDATE_<?php echo $cnt1; ?>">END DATE:</label></td>
              <td><input type="text" name="TERMINATIONDATE_<?php echo $cnt1; ?>" id="TERMINATIONDATE_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($rsspp_officers->Fields('TERMINATIONDATE')); ?>" size="10" maxlength="22" />
                  <?php echo $tNGs->displayFieldHint("TERMINATIONDATE");?> <?php echo $tNGs->displayFieldError("spp_officers", "TERMINATIONDATE", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_TERMINATIONDATE_on_update_only
?>
          <?php 
// Show IF Conditional show_COMMITTEESTATUSSTT_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
            <tr>
              <td class="KT_th"><label for="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>_1">STATUS:</label></td>
              <td><div>
                  <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_officers->Fields('COMMITTEESTATUSSTT')),"Active"))) {echo "CHECKED";} ?> type="radio" name="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" id="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>_1" value="Active" />
                  <label for="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>_1">Active</label>
                </div>
                  <div>
                    <input <?php if (!(strcmp(KT_escapeAttribute($rsspp_officers->Fields('COMMITTEESTATUSSTT')),"Inactive"))) {echo "CHECKED";} ?> type="radio" name="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>" id="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>_2" value="Inactive" />
                    <label for="COMMITTEESTATUSSTT_<?php echo $cnt1; ?>_2">Inactive</label>
                  </div>
                <?php echo $tNGs->displayFieldError("spp_officers", "COMMITTEESTATUSSTT", $cnt1); ?> </td>
            </tr>
            <?php } 
// endif Conditional show_COMMITTEESTATUSSTT_on_update_only
?>
          <?php 
// Show IF Conditional show_UPDATEUSERCD_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
            <tr>
              <td class="KT_th">UPDATEUSERCD:</td>
              <td><?php echo KT_escapeAttribute($rsspp_officers->Fields('UPDATEUSERCD')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_UPDATEUSERCD_on_update_only
?>
          <?php 
// Show IF Conditional show_UPDATETMS_on_update_only 
if (@$_GET['OFFICERID'] != "") {
?>
            <tr>
              <td class="KT_th">UPDATETMS:</td>
              <td><?php echo KT_formatDate($rsspp_officers->Fields('UPDATETMS')); ?></td>
            </tr>
            <?php } 
// endif Conditional show_UPDATETMS_on_update_only
?>
        </table>
        <input type="hidden" name="kt_pk_spp_officers_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($rsspp_officers->Fields('kt_pk_spp_officers')); ?>" />
        <?php
    $rsspp_officers->MoveNext(); 
  }
?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1 
      if (@$_GET['OFFICERID'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php } 
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</body>
</html>
<?php
$rsBouleMembers->Close();

$rsBouleOffices->Close();

$rsBouleOfficerDetail->Close();
?>
