<?php /* Smarty version 2.6.26, created on 2010-03-15 15:12:03
         compiled from city_edit.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<form id="city_form" action="<?php echo $this->_tpl_vars['BASE_URL_ADMIN']; ?>
cities/<?php if ($this->_tpl_vars['action'] == 'add'): ?>add/<?php else: ?>edit/<?php endif; ?>" method="post">
		<fieldset>
			<legend><?php if ($this->_tpl_vars['action'] == 'add'): ?>New city<?php else: ?>Edit city<?php endif; ?></legend>
			<?php if ($this->_tpl_vars['errors']['name']): ?><em class="form_error"><?php echo $this->_tpl_vars['errors']['name']; ?>
</em><?php endif; ?>
			<label><span>Name:</span><input type="text" size="60" name="name" value="<?php echo $this->_tpl_vars['city']['name']; ?>
" /></label>
			<?php if ($this->_tpl_vars['errors']['asciiName']): ?><em class="form_error"><?php echo $this->_tpl_vars['errors']['asciiName']; ?>
</em><?php endif; ?>
			<label><span>Ascii name:</span><input type="text" size="60" name="ascii_name" value="<?php echo $this->_tpl_vars['city']['ascii_name']; ?>
"/></label>
			<?php if ($this->_tpl_vars['action'] == 'edit'): ?>
				<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['city']['id']; ?>
" />
			<?php endif; ?>
		</fieldset>
		<p>
			<button type="submit" class="submit_button">Save</button>
		</p>
	</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>