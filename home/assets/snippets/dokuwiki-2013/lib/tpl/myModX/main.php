<?php
/**
 * DokuWiki Default Template
 *
 * This is the template you need to change for the overall look
 * of DokuWiki.
 *
 * You should leave the doctype at the very top - It should
 * always be the very first line of a document.
 *
 * @link   http://dokuwiki.org/templates
 * @author Andreas Gohr <andi@splitbrain.org>
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();

global $modx;
?>

  <?php
    if ($prev_content=ob_get_contents()) ob_clean();
    else ob_start();
    
    tpl_metaheaders();
    
    $outputheader = ob_get_contents();
    if($prev_content === false) ob_end_clean();
    else
    {
        ob_clean();
        ob_start();
        echo $prev_content;
    }

  $modx->regClientStartupHTMLBlock($outputheader);
   ?>

<div class="dokuwiki">
  <?php html_msgarea()?>

  <div class="stylehead">

    <?php if($conf['breadcrumbs']){?>
    <div class="breadcrumbs">
      <?php tpl_breadcrumbs()?>
      <?php //tpl_youarehere() //(some people prefer this)?>
      <div style="float:right">[ [<?php tpl_link(wl($ID,'do=backlink'),tpl_pagetitle($ID,true),'title="'.$lang['btn_backlink'].'"')?>] ]</div>
    </div>
    <?php }?>

    <?php if($conf['youarehere']){?>
    <div class="breadcrumbs">
      <?php tpl_youarehere() ?>
    </div>
    <?php }?>

  </div>

  <div class="page">
    <!-- wikipage start -->
    <?php tpl_content()?>
    <!-- wikipage stop -->
  </div>

  <div class="clearer">&nbsp;</div>

  
  <div class="stylefoot">

    <div class="bar" id="bar__bottom">
      <div class="bar-left" id="bar__bottomleft">
        <?php tpl_button('edit')?>
        <?php tpl_button('history')?>
        <?php tpl_button('revert')?>
      </div>
      <div class="bar-right" id="bar__bottomright">
        <?php tpl_button('subscribe')?>
        <?php tpl_button('subscribens')?>
        <?php tpl_button('admin')?>
        <?php tpl_button('profile')?>
        <?php tpl_button('login')?>
        <?php tpl_searchform()?>&nbsp;
        <?php tpl_button('index')?>
        <?php //tpl_button('top')?>&nbsp;
      </div>
      <div class="clearer"></div>
    </div>

  </div>

</div>

<div class="no"><?php /* provide DokuWiki housekeeping, required in all templates */ tpl_indexerWebBug()?></div>

<script>
  addInitEvent(function(){ init_modx_unfiltering();} );
  
  // In order to bypass the ModX filtering on posted variables 
  // with ModX special characters used for the chunks, snippets, ...
  // that will be interpreted as hack injections
  // we put blank spaces 
  function init_modx_unfiltering()
  { 
    var btn_save        = document.getElementById('edbtn__save');
    if (btn_save) btn_save.onmouseup    = modx_unfiltering_wikitext;
    var btn_preview        = document.getElementById('edbtn__preview');
    if (btn_preview) btn_preview.onmouseup    = modx_unfiltering_wikitext;

    var btn_recover_draft        = document.getElementsByName('do[recover]')[0];
    if (btn_recover_draft) btn_recover_draft.onmouseup    = modx_unfiltering_wikitext;

    var wiki_text  = document.getElementById('wiki__text');
    if (wiki_text) 
    {
        var aTextVal = wiki_text.value;
        //alert(aTextVal);
        aTextVal=aTextVal.replace(/} }/g, '}}');
        aTextVal=aTextVal.replace(/{ {/g, '{{');
        aTextVal=aTextVal.replace(/\] \]/g, ']]');//!! take care to let this lines in this order as ModX could filter them! and nothing will work :-s
        aTextVal=aTextVal.replace(/\[ \[/g, '[[');
        aTextVal=aTextVal.replace(/! \]/g, '!]');
        aTextVal=aTextVal.replace(/\[ !/g, '[!');
        aTextVal=aTextVal.replace(/\\ \\/g, '\\\\');
        wiki_text.value = aTextVal;
        //alert(aTextVal);
    }
    return true;
  }
  
  function modx_unfiltering_wikitext()
  { 
    var wiki_text = document.getElementById('wiki__text');
    var aTextVal = wiki_text.value;
    aTextVal=aTextVal.replace(/{{/g, '{ {');
    aTextVal=aTextVal.replace(/}}/g, '} }');
    aTextVal=aTextVal.replace(/\[\[/g, '[ [');
    aTextVal=aTextVal.replace(/\]\]/g, '] ]');
    aTextVal=aTextVal.replace(/\[!/g, '[ !');
    aTextVal=aTextVal.replace(/!\]/g, '! ]');
    aTextVal=aTextVal.replace(/\\\\/g, '\\ \\');
    wiki_text.value = aTextVal;
    return true;
  }     
</script>