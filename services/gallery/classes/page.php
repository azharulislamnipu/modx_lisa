<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Creates html headers and footers including css.
 *
 * @package svManager
 */
class Page
{
  /**
   * @var string contains text for html <title></title> tags
   */
   var $title;

  /**
   * @var string id for body tag
   */
   var $bodyId;
   
  /**
   * @var string class for body tag. Used to differentiate viewers
   */
   var $bodyClass;

  /**
   * @var string page specific html to be added to the bottom of the <head> element
   */
   var $customHeadHtml;

  /**
   * Constructs Page
   *
   * @access public
   * @return void
   * @param string content for html title tag
   * @param string body id
   */
   function Page($title, $bodyId, $bodyClass='allviewers')
   {
     $this->title = $title;
     $this->bodyId = $bodyId;
     $this->bodyClass = $bodyClass;
   }

  /**
   * Returns string containing html header, css styles and page heading
   *
   * Rules for heredoc ( <<<EOD...EOD; ) are on {@link http://www.php.net/heredoc the php web site}
   * in particular note the rules for white space in closing heredoc tags
   *
   * @return string
   * @access public
   */  
  function getHtmlHead()
  {
    $header = <<<EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>{$this->title}</title>
<link href="css/dummy.css" rel="stylesheet" type="text/css" media="all" />
<!-- Conditional comment to fix bugs in IE5 Win -->
<!--[if IE 5]>
<link href="css/ie5.css" rel="stylesheet" type="text/css" media="all" />
<![endif]-->
<!-- Conditional comment to fix bugs in IE5 and 6 Win -->
<!--[if lte IE 6]>
<link href="css/ielte6.css" rel="stylesheet" type="text/css" media="all" />
<![endif]-->
<script src="scripts/allpages.js" type="text/javascript"></script>
{$this->customHeadHtml}
</head>

EOD;
  return $header;
}

 /**
  * Returns string containing html header, css styles and page heading for transitional doctype
  *
  * @return string
  * @access public
  */  
  function getHtmlHeadTransitional()
  {
    $header = <<<EOD
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>{$this->title}</title>
    <link href="css/dummy.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Conditional comment to fix bugs in IE5 Win -->
    <!--[if IE 5]>
    <link href="css/ie5.css" rel="stylesheet" type="text/css" media="all" />
    <![endif]-->
    <!-- Conditional comment to fix bugs in IE5 and 6 Win -->
    <!--[if lte IE 6]>
    <link href="css/ielte6.css" rel="stylesheet" type="text/css" media="all" />
    <![endif]-->
    <script src="scripts/allpages.js" type="text/javascript"></script>
    {$this->customHeadHtml}
    </head>

EOD;
  return $header;
}


 /**
  * Returns string containing navigation
  *
  @return string
  @param object gallery data
  @access public
  */
  function getPageHeader()
  { 
    $indexLink = (file_exists(INDEX_URL)) ? '<a href="'.INDEX_URL.'" title="link to gallery index page">Gallery&nbsp;Index</a>&nbsp;&nbsp;|&nbsp;&nbsp;' : '';    
    	$navigation = <<<EOD
  <body id="{$this->bodyId}" class="{$this->bodyClass}">
  	<div id="wrapper">
      <div id="header">
        <div id="externalnav">
          <ul>
            <li id="indexlink">{$indexLink}</li>
            <li id="navlogout"><a href="svmanager.php?logout=true" title="log out of svManager">Log-out</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
            <li id="help"><a href="http://www.simpleviewer.net/svmanager/documentation.html" rel="external_blank" title="on-line documentation">Help</a></li>
          </ul>
        </div>
        <div id="navigation">
        <ul>
          <li id="navgalleries"><a href="svmanager.php" title="svManager gallery list">Galleries</a></li>
          <li id="navimport"><a href="svmimport.php" title="import existing gallery">Import</a></li>
          <li id="navadmin"><a href="svmadmin.php" title="svManager administration">Admin</a></li>
          
        </ul>
        
      </div>
      <br class="clearboth" />
      </div>
      
      <div id="content">
  
EOD;
    return $navigation;
  }
  /**
   * Returns string containing button bar html
   *
   * @return string
   * @param array gallery data (needed for view)
   */
   function getButtonbar(&$gallery)
   {
     $titleString = '<h2>'.$gallery->getGalleryTitleText().'</h2>';
     $href = str_replace('\\', '/', $gallery->getGalleryPathRelSvm());
     $html = <<<EOD
     <div id="closegallery"><a href="svmanager.php" title="Close gallery screen">Close</a></div>
     {$titleString}
     <div id="buttonbar">
       <ul>
         <li id="navcustomize"><a href="svmcustomize.php"><span>Customize</span></a></li>
         <li id="navpro"><a href="svmpro.php"><span>Pro</span></a></li>
         <li id="navupload"><a href="svmupload.php"><span>Add&nbsp;images</span></a></li>
         <li id="navimages"><a href="svmimages.php"><span>Images</span></a></li>
         <li id="navsort"><a href="svmsort.php"><span>Sort</span></a></li>
         <li id="navview"><a rel="external" href="{$href}"><span>View</span></a></li>
       </ul>
     </div>
     <br class="clearboth" />

EOD;
     return $html;
   }

  /**
   * Returns string containing closing html tags
   *
   * Could also be used for a page footer, such as a copyright message
   * Rules for heredoc ( <<<EOD...EOD; ) are on {@link http://www.php.net/heredoc the php web site}
   * in particular note the rules for white space in closing heredoc tags
   *
   * @return string
   * @access public
   */  
  function getFooter()
  {
    $version = VERSION;
    $footer = <<<EOD
  </div>
  <div id="footer">
    <p id="copyright">&copy; 2011 Airtight Interactive</p>
    <br class="clearboth" />
  </div>
  </div>
</body>
</html>
EOD;
  return $footer;
  }
  
 /**
  * Returns html for colorjack
  *
  * @access private
  * @return string
  */
  function getColorjackHtml()
  {
    $html = <<<EOD

<div id="plugin" class="selectfree">
 <div id="plugHEX" onmousedown="stop=0; setTimeout('stop=1',100);">F1FFCC</div>
 <!-- Note change of function name from toggle to avoid namespace conflict -->
 <div id="plugCLOSE" onmousedown="toggleDisplay('plugin')">X</div><br />
 <div id="SV" onmousedown="HSVslide('SVslide','plugin',event)" title="Saturation + Value">
  <div id="SVslide" style="TOP: -4px; LEFT: -4px;"><br /></div>
 </div>
 <form id="H" action="" onmousedown="HSVslide('Hslide','plugin',event)" title="Hue">
  <div id="Hslide" style="TOP: -7px; LEFT: -8px;"><br /></div>
  <div id="Hmodel"></div>
 </form>
 <!--[if lte IE 6.5]><iframe></iframe><![endif]-->
</div>
EOD;
    return $html;
  }

 /**
  * Formats html for select form element
  *
  * @access private
  * @param string form element name
  * @param string form element id
  * @param array form element options as value=>option
  * @param string value for selected element
  */
  function htmlSelect($name, $id, $tabindex, $options, $selected)
  {
    $selected = strtolower($selected);
    $html = '';
    $html .= '<select name="'.$name.'" id="'.$id.'" tabindex="'.$tabindex.'">';
    foreach ($options as $value=>$option)
    {
      if (strtolower($value) == $selected)
      {
        $selectString = 'selected="selected"';
      }
      else
      {
        $selectString = '';
      }
      $html .= '<option value="'.$value.'" '.$selectString.'>'.$option.'</option>';
    }
    $html .= '</select>';
    return $html;
  }

 /**
  * strip html entities and tags from  string
  *
  * @access public
  * @return string
  * @param string
  */
  function stripHtml($string)
  {
    $htmlEntityDecode = new HtmlEntityDecode();
    return strip_tags($htmlEntityDecode->decode ($string));    
  }
}

?>