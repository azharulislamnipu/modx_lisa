<?php

/*
 * Update spp_delegates with any appointed delegates that have become ineligible
 * Author: Pat Heard {http://fullahead.org}
 */

// Database details
$host = "localhost";
$user = "sigma-admin";
$password = "beaure";
$database = "modx_dev";

// Attempt to open a database connection
$con = mysql_connect($host, $user, $password); 
if (!$con) {
    die('Could not connect to database for delegates eligibility batch process: ' . mysql_error());
}

// Select the MODX database
if(!mysql_select_db($database)){
    die('Could not select "' . $database . '" database: ' . mysql_error()); 
}

// Update any delegates that have lost their eligibility
$result = mysql_query("
    UPDATE 
        spp_delegates D
    SET
        D.APPOINTED = '2',
        D.APPTBY = 'INELIGIBLE_BATCH',
        D.APPTDATE = NOW()
    WHERE
        APPOINTED IN ('0', '1')
        AND delegate_status = 'A'
        AND NOT EXISTS (
            SELECT 
                1 
            FROM
                spp_archons A
            WHERE
                A.CUSTOMERID = D.CUSTOMERCD
            AND (
                A.CUSTOMERTYPE = _latin1'Member' 
                AND A.STATUSSTT = _latin1'Active'
                AND ISNULL(A.CUSTOMERCLASSSTT)
            )
        )      
");

// Query failed
if($result === false){
    die('Failed to update ineligible delegates: ' . mysql_error());
}

// Update any delegates that have regained their eligibility
$result = mysql_query("
    UPDATE 
        spp_delegates D
    SET
        D.APPOINTED = '0',
        D.APPTBY = 'ELIGIBLE_BATCH',
        D.APPTDATE = NOW()
    WHERE
        APPOINTED = '2'
        AND delegate_status = 'A'
        AND EXISTS (
            SELECT 
                1 
            FROM
                spp_archons A
            WHERE
                A.CUSTOMERID = D.CUSTOMERCD
            AND (
                A.CUSTOMERTYPE = _latin1'Member' 
                AND A.STATUSSTT = _latin1'Active'
                AND ISNULL(A.CUSTOMERCLASSSTT)
            )
        )      
");


// Query failed
if($result === false){
    die('Failed to update eligible delegates: ' . mysql_error());
}

// C'est tout!
?>
