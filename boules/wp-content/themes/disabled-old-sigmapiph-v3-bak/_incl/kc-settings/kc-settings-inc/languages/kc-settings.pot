# Copyright (C) 2010 KC Settings
# This file is distributed under the same license as the KC Settings package.
msgid ""
msgstr ""
"Project-Id-Version: KC Settings 1.2.1\n"
"Report-Msgid-Bugs-To: http://wordpress.org/tag/kc-settings\n"
"POT-Creation-Date: 2011-02-28 13:15:28+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2010-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: kc-settings-inc/form.php:171 kc-settings-inc/form.php:182
#: kc-settings-inc/helper.php:115
msgid "Select"
msgstr ""

#: kc-settings-inc/form.php:263
msgid "Delete"
msgstr ""

#: kc-settings-inc/form.php:279
msgid "Add new row"
msgstr ""

#: kc-settings-inc/theme.php:23
msgid "My Settings"
msgstr ""

#: kc-settings-inc/theme.php:117
msgid "Save Changes"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "KC Settings"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://kucrut.org/2010/10/kc-settings/"
msgstr ""

#. Description of the plugin/theme
msgid "Easily create plugin/theme settings page and custom fields metaboxes"
msgstr ""

#. Author of the plugin/theme
msgid "Dzikri Aziz"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://kucrut.org/"
msgstr ""
