<?php
require_once('../includes/admin/access.php');
require_once('../includes/admin/header.php');

if ($_POST){//if there's post action
        
        $succeed=false;
        //generationg the config.php
		$config_content = "<?php\n//Open Classifieds Installation Config ".date("d/m/Y G:i:s")."\n";
		foreach  ($_POST AS $key => $value){
				if ($key!="submit" and $key!="TIMEZONE"){
				    if ($value=="TRUE") $config_content.="define('$key',true);\n";	
				    elseif ($value=="FALSE") $config_content.="define('$key',false);\n";	
					else $config_content.="define('$key','$value');\n";		
				}
		}
		$config_content.="date_default_timezone_set('".$_POST["TIMEZONE"]."');\n";
		$config_content.="?>";//	echo $config_content;
		
		//writting the config.php
		if(is_writable('../includes')){
			$file = fopen('../includes/config.php' , "w+");
			if (fwrite($file, $config_content)=== FALSE) {
				$msg="Cannot write to the configuration file '/includes/config.php'";
				$succeed=false;
			}else $succeed=true;
			fclose($file);		
		}
		else $msg="The configuration file '/includes/config.php' is not writable. Change the permissions.";
	
	    //succeded writting the config.php
	    if ($succeed){
	        generateSitemap();
		    if (LANGUAGE!=$_POST["LANGUAGE"]){//changing the language generating new .htaccess
		        jsRedirect(SITE_URL."/admin/htaccess.php?lang=".$_POST["LANGUAGE"]);
		    }//end if language
			else { 
			    echo "Updated";
			    require_once('../includes/admin/footer.php');
			    die();
			}
	    }
	    else echo $msg;
}//end post
?>

<script type="text/javascript" src="../includes/js/common.js"></script>
		
		
<h3>Settings v<?php echo VERSION;?></h3>

<form id="install" action="settings.php" method="post" onsubmit="return checkForm(this);">


<div class="settingsTitle" onclick="openClose('bconf');">Basic configuration</div>
 
<table class="settingsTable" id="bconf">
<tr>
	<td>Site Name:</td>
	<td><input  type="text" name="SITE_NAME" value="<?php echo SITE_NAME;?>" lang="false" onblur="validateText(this);" /></td>
</tr>
<tr>
	<td>Email address:</td>
	<td><input  type="text" name="NOTIFY_EMAIL"  value="<?php echo NOTIFY_EMAIL;?>" lang="false" onblur="validateEmail(this);"/>for notifications</td>
</tr>
<tr>
	<td>Site language:</td>
	<td>
	<select name="LANGUAGE" >
	    <option value="<?php echo LANGUAGE;?>"><?php echo LANGUAGE;?></option>
	    <?php
	    $languages = scandir("../languages");
	    foreach ($languages as $lang) {
		    $lang=substr($lang,0,-4);
		    if($lang!=""&&$lang!=LANGUAGE){
			    echo "<option value=\"$lang\">$lang</option>";
		    }
	    }
	    ?>
	</select>
	</td>
</tr>
<tr>
	<td>Time Zone:</td>
	<td>
	<select id="TIMEZONE" name="TIMEZONE">
	<?php
	$timezone_identifiers = DateTimeZone::listIdentifiers();
	foreach( $timezone_identifiers as $value ){
	    if ( preg_match( '/^(America|Antartica|Arctic|Asia|Atlantic|Europe|Indian|Pacific)\//', $value ) ){
	    	$ex=explode("/",$value);//obtain continent,city
	    	
	    	
	    	if ($continent!=$ex[0]){
	    		if ($continent!="") echo '</optgroup>';
	    		echo '<optgroup label="'.$ex[0].'">';
	    	}
	    	
	    	$city=$ex[1];
	    	$continent=$ex[0];
	    	if (date_default_timezone_get()==$value) echo '<option selected=selected value="'.$value.'" >'.$city.'</option>';
	    	else echo '<option value="'.$value.'">'.$city.'</option>';	    	
	    	
	    }
	}
	?>
		</optgroup>
	</select>
	</td>
</tr>

<tr>
	<td>Admin Name:</td>
	<td><input type="text" name="ADMIN" value="<?php echo ADMIN;?>" lang="false" onblur="validateText(this);" /></td>
</tr>
<tr>
	<td>Admin Password:</td>
	<td><input type="password" name="ADMIN_PWD" value="<?php echo ADMIN_PWD;?>" /></td>		
</tr>
</table>



<div class="settingsTitle" onclick="openClose('dbconf');">Database configuration</div>
 
<table class="settingsTable" id="dbconf">
<tr>
	<td>Host Name:</td>
	<td><input  type="text" name="DB_HOST" value="<?php echo DB_HOST;?>" lang="false" onblur="validateText(this);" /></td>
</tr>
<tr>
	<td>Database Username:</td>
	<td><input  type="text" name="DB_USER"  value="<?php echo DB_USER;?>" lang="false" onblur="validateText(this);" /></td>
</tr>
<tr>
	<td>Database Password:</td>
	<td><input type="password" name="DB_PASS" value="<?php echo DB_PASS;?>"  /></td>		
</tr>
<tr>
	<td>Database Name:</td>
	<td><input type="text" name="DB_NAME" value="<?php echo DB_NAME;?>" lang="false" onblur="validateText(this);" /></td>
</tr>
<tr>
	<td>Database Charset:</td>
	<td><input type="text" name="DB_CHARSET" value="<?php echo DB_CHARSET;?>" lang="false" onblur="validateText(this);" />IMPORTANT: maybe you need to change it in the HTML Charset as well</td>
</tr>
<tr>
	<td>Table Prefix:</td>
	<td><input type="text" name="TABLE_PREFIX" value="<?php echo TABLE_PREFIX;?>"  /> Multiple installations in one database if you give each a unique prefix. Only numbers, letters, and underscores.
	</td>	
</tr>
</table>


<div class="settingsTitle" onclick="openClose('iniconf');">Initial sets</div>
 
<table class="settingsTable" id="iniconf">
<tr>
	<td>Site URL:</td>
	<td><input  type="text" name="SITE_URL" value="<?php echo SITE_URL;?>" lang="false"  /></td>
</tr>
<tr>
	<td>Site Full Path:</td>
	<td><input  type="text" name="SITE_ROOT" value="<?php echo SITE_ROOT;?>" lang="false"  /> Path in the server, IMPORTANT</td>
</tr>
<tr>
	<td>HTML Charset:</td>
	<td><input  type="text" name="CHARSET" value="<?php echo CHARSET;?>" lang="false"  />IMPORTANT: maybe you need to change it in your DB Charset as well</td>
</tr>
<tr>
	<td>Date Format:</td>
	<td><input  type="text" name="DATE_FORMAT" value="<?php echo DATE_FORMAT;?>" lang="false"  />Use a date format!</td>
</tr>
<tr>
	<td>Posts counter:</td>
	<td>
	<select name="COUNT_POSTS" >
	<option <?php if(COUNT_POSTS)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!COUNT_POSTS)  echo "selected=selected";?> >FALSE</option>
	</select> Count the visitors per post, uses DB!
	</td>
</tr>
<tr>
	<td>Minimal length to search:</td>
	<td><input  type="text" name="MIN_SEARCH_CHAR" value="<?php echo MIN_SEARCH_CHAR;?>" lang="false"  />Search less than this number will not be performed</td>
</tr>
<tr>
	<td>Password Size:</td>
	<td><input type="text" name="PASSWORD_SIZE" value="<?php echo PASSWORD_SIZE;?>" lang="false" size=3  />Size for the paswrod used in new Posts. Only numbers!</td>
</tr>
<tr>
	<td>Currency:</td>
	<td><input type="text" name="CURRENCY" value="<?php echo CURRENCY;?>" lang="false" size=3  />Price currency</td>
</tr>
<tr>
	<td>Currency Format:</td>
	<td><input type="text" name="CURRENCY_FORMAT" value="<?php echo CURRENCY_FORMAT;?>" lang="false"  />Format to display price ie: CURRENCYAMOUNT</td>
</tr>
<tr>
	<td>Type offer:</td>
	<td><input type="text" name="TYPE_OFFER" value="<?php echo TYPE_OFFER;?>" lang="false" size=3  />Offer value on DB</td>
</tr>
<tr>
	<td>Type need:</td>
	<td><input type="text" name="TYPE_NEED" value="<?php echo TYPE_NEED;?>" lang="false" size=3  />Need value on DB</td>
</tr>
</table>


<div class="settingsTitle" onclick="openClose('look');">Look and Feel</div>
 
<table class="settingsTable" id="look">
<tr>
<tr>
	<td>Friendly URL's:</td>
	<td>
	<select name="FRIENDLY_URL" >
	<option <?php if(FRIENDLY_URL)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!FRIENDLY_URL)  echo "selected=selected";?> >FALSE</option>
	</select> Disabled doesn't use the .htaccess, and the URLS they would look uglier, also worst SEO
	</td>
</tr>
	<td>Default Theme:</td>
	<td>
	<select name="DEFAULT_THEME" >
	<option value="<?php echo DEFAULT_THEME;?>"><?php echo DEFAULT_THEME;?></option>
	<?php
	$themes = scandir("../themes");
	foreach ($themes as $theme) {
		if($theme!="" && $theme!=DEFAULT_THEME && $theme!="." && $theme!=".." && $theme!="wordcloud.css"){
			echo "<option value=\"$theme\">".$theme."</option>";
		}
	}
	?>
	</select> For more themes please go to <a href="http://www.open-classifieds.com/">open-classifieds</a>
	</td>
</tr>
<tr>
	<td>Theme Selector:</td>
	<td>
	<select name="THEME_SELECTOR" >
	<option <?php if(THEME_SELECTOR)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!THEME_SELECTOR)  echo "selected=selected";?> >FALSE</option>
	</select> If you enable this you allow the user to select theme
	</td>
</tr>
<tr>
	<td>Mobile Theme:</td>
	<td>
	<select name="THEME_MOBILE" >
	<option <?php if(THEME_MOBILE)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!THEME_MOBILE)  echo "selected=selected";?> >FALSE</option>
	</select> Displays the mobile version of your site  to mobile devices if its enabled, (uses neoMobile theme)
	</td>
</tr>
<tr>
	<td>Items to display per page:</td>
	<td><input type="text" name="ITEMS_PER_PAGE" value="<?php echo ITEMS_PER_PAGE;?>" lang="false" size=3  />Only numbers!</td>
</tr>
<tr>
	<td>Pages to display:</td>
	<td><input type="text" name="DISPLAY_PAGES" value="<?php echo DISPLAY_PAGES;?>" lang="false" size=3  />How many pages are displayed. Only numbers!</td>
</tr>
<tr>
	<td>HTML Editor:</td>
	<td>
	<select name="HTML_EDITOR" >
	<option <?php if(HTML_EDITOR)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!HTML_EDITOR)  echo "selected=selected";?> >FALSE</option>
	</select> Disable HTML editor nicEdit, in the post.
	</td>
</tr>
<tr>
	<td>HTML Separator:</td>
	<td><input type="text" name="SEPARATOR" value="<?php echo SEPARATOR;?>" lang="false" size=3  />Separator used at few places</td>
</tr>
<tr>
	<td>Widgets Sidebar:</td>
	<td><textarea  name="SIDEBAR" lang="false" rows=3 cols=55><?php echo SIDEBAR;?></textarea><br />Each "," defines a new widget in the sidebar, remove the ones you don't want</td>
</tr>
<tr>
	<td>Widget Advertisement:</td>
	<td><textarea  name="ADVERT"  rows=3 cols=55><?php echo ADVERT;?></textarea><br />HTML advertisement that appears in the sidebar. Be carefull the code you paste in here, single quotes not recommended</td>
</tr>
</table>

<div class="settingsTitle" onclick="openClose('imgconf');">Image settings</div>
 
<table class="settingsTable" id="imgconf">
<tr>
	<td>Number of images:</td>
	<td><input type="text" name="MAX_IMG_NUM" value="<?php echo MAX_IMG_NUM;?>" lang="false" size=3 />Number of images that can be psoted, 0 disable all the images</td>
</tr>
<tr>
	<td>Image size:</td>
	<td><input type="text" name="MAX_IMG_SIZE" value="<?php echo MAX_IMG_SIZE;?>" lang="false" />b Max image size allowed</td>
</tr>
<tr>
	<td>Image folder:</td>
	<td><input type="text" name="IMG_UPLOAD" value="<?php echo IMG_UPLOAD;?>" lang="false" />Image upload directory name</td>
</tr>
<tr>
	<td>Image full path:</td>
	<td><input type="text" name="IMG_UPLOAD_DIR" value="<?php echo IMG_UPLOAD_DIR;?>" lang="false" />Fullpath where the images would be</td>
</tr>
<tr>
	<td>Image types:</td>
	<td><input type="text" name="IMG_TYPES" value="<?php echo IMG_TYPES;?>" lang="false" />Type of images allowed, separated by coma</td>
</tr>
<tr>
	<td>Image resize:</td>
	<td><input type="text" name="IMG_RESIZE" value="<?php echo IMG_RESIZE;?>" lang="false" size=3 />px Size of the images uploaded</td>
</tr>
<tr>
	<td>Thumbs resize:</td>
	<td><input type="text" name="IMG_RESIZE_THUMB" value="<?php echo IMG_RESIZE_THUMB;?>" lang="false" size=3/>px Size of the thumbs generated</td>
</tr>
</table>

<div class="settingsTitle" onclick="openClose('Sitemap');">Rss & Sitemap</div>
 
<table class="settingsTable" id="Sitemap">

<tr>
	<td>RSS Items:</td>
	<td><input type="text" name="RSS_ITEMS" value="<?php echo RSS_ITEMS;?>" lang="false" size=3/>number of items to display in RSS</td>
</tr>
<tr>
	<td>RSS Images:</td>
	<td>
	<select name="RSS_IMAGES" >
	<option <?php if(RSS_IMAGES)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!RSS_IMAGES)  echo "selected=selected";?> >FALSE</option>
	</select>Display images on RSS</td>
</tr>
<tr>
	<td>Sitemap File Path:</td>
	<td><input type="text" name="SITEMAP_FILE" value="<?php echo SITEMAP_FILE;?>" lang="false" />Path for the sitemap</td>
</tr>
<tr>
	<td>Sitemap Expires:</td>
	<td><input type="text" name="SITEMAP_EXPIRE" value="<?php echo SITEMAP_EXPIRE;?>" lang="false" />seconds, generates new sitemap after expire</td>
</tr>
<tr>
	<td>Sitemap delete on post:</td>
	<td>
	<select name="SITEMAP_DEL_ON_POST" >
	<option <?php if(SITEMAP_DEL_ON_POST)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!SITEMAP_DEL_ON_POST)  echo "selected=selected";?> >FALSE</option>
	</select>On new post generate new sitemap</td>
</tr>
<tr>
	<td>Sitemap on category:</td>
	<td>
	<select name="SITEMAP_DEL_ON_CAT" >
	<option <?php if(SITEMAP_DEL_ON_CAT)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!SITEMAP_DEL_ON_CAT)  echo "selected=selected";?> >FALSE</option>
	</select>On new/updated category generate new sitemap</td>
</tr>
<tr>
	<td>Sitemap ping to google:</td>
	<td>
	<select name="SITEMAP_PING" >
	<option <?php if(SITEMAP_PING)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!SITEMAP_PING)  echo "selected=selected";?> >FALSE</option>
	</select>On any update will ping google about this changes, before activate register your site at <a href="http://www.google.com/webmasters/tools/">Google webmaster tools</a></td>
</tr>
</table>

<div class="settingsTitle" onclick="openClose('cache');">Cache configuration</div>
 
<table class="settingsTable" id="cache">
<tr>
	<td>Cache Active:</td>
	<td>
	<select name="CACHE_ACTIVE" >
	<option <?php if(CACHE_ACTIVE)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!CACHE_ACTIVE)  echo "selected=selected";?> >FALSE</option>
	</select>sets on/off the cache system</td>
</tr>
<tr>
	<td>Cache File Path:</td>
	<td><input type="text" name="CACHE_DATA_FILE" value="<?php echo CACHE_DATA_FILE;?>" lang="false" />Path for the cache</td>
</tr>
<tr>
	<td>Cache Expires:</td>
	<td><input type="text" name="CACHE_EXPIRE" value="<?php echo CACHE_EXPIRE;?>" lang="false" />seconds, cache expires</td>
</tr>
<tr>
	<td>Cache delete on post:</td>
	<td>
	<select name="CACHE_DEL_ON_POST" >
	<option <?php if(CACHE_DEL_ON_POST)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!CACHE_DEL_ON_POST)  echo "selected=selected";?> >FALSE</option>
	</select>On new post generate deletes the cache</td>
</tr>
<tr>
	<td>Cache delete on category:</td>
	<td>
	<select name="CACHE_DEL_ON_CAT" >
	<option <?php if(CACHE_DEL_ON_CAT)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!CACHE_DEL_ON_CAT)  echo "selected=selected";?> >FALSE</option>
	</select>On new/updated category deletes the cache</td>
</tr>
</table>

<div class="settingsTitle" onclick="openClose('etools');">External Tools</div>
 
<table class="settingsTable" id="etools">
<tr>
	<td><a href="http://www.google.com/analytics/">Google Analytics</a>:</td>
	<td><input type="text" name="ANALYTICS" value="<?php echo ANALYTICS;?>" />code in the footer, ie: UA-4562297-13, empty disabled.</td>
</tr>
<tr>
	<td><a href="http://www.twitter.com/">Twitter</a>:</td>
	<td><input type="text" name="TWITTER" value="<?php echo TWITTER;?>" />User Name, any new post would be tweetted with this account.</td>
</tr>
<tr>
	<td>Twitter Password:</td>
	<td><input type="password" name="TWITTER_PWD" value="<?php echo TWITTER_PWD;?>" />Password for your account.</td>
</tr>
<tr>
	<td><a href="http://j.mp">Bit.ly/J.mp</a>:</td>
	<td><input type="text" name="BIT_USER" value="<?php echo BIT_USER;?>" />User Name, short URL for twitter.</td>
</tr>
<tr>
	<td>Bit.ly/j.mp API:</td>
	<td><input type="text" name="BIT_API" value="<?php echo BIT_API;?>" />API  for your account.</td>
</tr>
<tr>
	<td><a href="http://wordpress.com/api-keys/">Akismet KEY</a>:</td>
	<td><input type="text" name="AKISMET" value="<?php echo AKISMET;?>" />Prevent spam.</td>
</tr>
<tr>
	<td><a href="http://code.google.com/apis/maps/signup.html">Google Maps KEY</a>:</td>
	<td><input type="text" name="MAP_KEY" value="<?php echo MAP_KEY;?>" />MAP KEY,Displays google maps with posts.</td>
</tr>
<tr>
	<td>Center Maps at:</td>
	<td><input type="text" name="MAP_INI_POINT" value="<?php echo MAP_INI_POINT;?>" />Map would be centered here. ie: barcelona,spain.</td>
</tr>
<tr>
	<td>Comments:</td>
	<td><input type="text" name="DISQUS" value="<?php echo DISQUS;?>" />Account name enables comnents for posts threads with <a href="http://disqus.com/comments/register/">Disqus</a>.</td>
</tr>
<tr>
	<td>GMAIL:</td>
	<td>
	<select name="GMAIL" >
	<option <?php if(GMAIL)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!GMAIL)  echo "selected=selected";?> >FALSE</option>
	</select>Uses GMAIL for SMTP, perfect if you don' have email server or you can't configure it</td>
</tr>
<tr>
	<td>GMAIL Username:</td>
	<td><input type="text" name="GMAIL_USER" value="<?php echo GMAIL_USER;?>" />Account name</td>
</tr>
<tr>
	<td>GMAIL Password:</td>
	<td><input type="password" name="GMAIL_PASS" value="<?php echo GMAIL_PASS;?>" />Account password</td>
</tr>
<tr>
	<td>VIDEO:</td>
	<td>
	<select name="VIDEO" >
	<option <?php if(VIDEO)  echo "selected=selected";?> >TRUE</option>
	<option <?php if(!VIDEO)  echo "selected=selected";?> >FALSE</option>
	</select>Allows youtube videos on posts using [youtube=URLVIDEO]</td>
</tr>
</table>
<br />
<input type="submit" name="submit" id="submit" value="Save settings"  />
</form>

<?php
require_once('../includes/admin/footer.php');
?>
