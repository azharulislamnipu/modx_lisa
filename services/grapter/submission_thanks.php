<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArticles = "-1";
if (isset($_GET['art_id'])) {
  $colname_rsArticles = $_GET['art_id'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsArticles = sprintf("SELECT * FROM vw_articles_all_v2 WHERE art_id = %s", GetSQLValueString($colname_rsArticles, "int"));
$rsArticles = mysql_query($query_rsArticles, $sigma_modx) or die(mysql_error());
$row_rsArticles = mysql_fetch_assoc($rsArticles);
$totalRows_rsArticles = mysql_num_rows($rsArticles);

$colname_rsPhotos = "-1";
if (isset($_GET['art_id'])) {
  $colname_rsPhotos = $_GET['art_id'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsPhotos = sprintf("SELECT photo_id, photo_file, photo_nm, ph_caption, art_id, art_title, iss_cd, iss_nm, dept_cd, dept_nm, dept_img_limit, CHAPTERID, SUBMITBY, FULLNAME, date_submit, date_modified, photo_file_name, iss_id FROM vw_photos_all WHERE art_id = %s", GetSQLValueString($colname_rsPhotos, "int"));
$rsPhotos = mysql_query($query_rsPhotos, $sigma_modx) or die(mysql_error());
$row_rsPhotos = mysql_fetch_assoc($rsPhotos);
$totalRows_rsPhotos = mysql_num_rows($rsPhotos);

// Show Dynamic Thumbnail
$objDynamicThumb1 = new tNG_DynamicThumbnail("../", "KT_thumbnail1");
$objDynamicThumb1->setFolder("images/{rsPhotos.iss_cd}/{rsPhotos.CHAPTERID}/");
$objDynamicThumb1->setRenameRule("{rsPhotos.photo_file}");
$objDynamicThumb1->setResize(200, 0, true);
$objDynamicThumb1->setWatermark(false);

// Show Dynamic Thumbnail
$objDynamicThumb1 = new tNG_DynamicThumbnail("../", "KT_thumbnail1");
$objDynamicThumb1->setFolder("../grapter/images/{rsPhotos.iss_cd}/{rsPhotos.CHAPTERID}/");
$objDynamicThumb1->setRenameRule("{rsPhotos.photo_file}");
$objDynamicThumb1->setResize(200, 0, true);
$objDynamicThumb1->setWatermark(false);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/grapter_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<h1 class="yellow">THANK YOU FOR YOUR SUBMISSION</h1>
<div class="contentBlock">
<p style="font-size: 24px; margin: 0 0 6px 6px; line-height:125%; border-bottom: 2px solid black;"><a href="https://www.sigmapiphi.org/home/updated-submissions.php">Submit Another Article</a></p>
<p style="font-size: 24px; margin: 0 0 6px 6px; line-height:125%">Below is a copy of the information you submitted. <br />
Please print and keep a copy  for your records.</p>
<table cellspacing="4" cellpadding="4">
  <tr>
    <td valign="top" style="font-size: 14px; margin: 0 0 6px 6px; line-height:125%">ISSUE</td>
    <td valign="top" style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%"><?php echo $row_rsArticles['iss_nm']; ?></td>
  </tr>
  <tr>
    <td valign="top" style="font-size: 14px; margin: 0 0 6px 6px; line-height:125%">DEPARTMENT</td>
    <td valign="top" style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%"><?php echo $row_rsArticles['dept_nm']; ?></td>
  </tr>
  <tr>
    <td valign="top" style="font-size: 14px; margin: 0 0 6px 6px; line-height:125%">EVENT DATE</td>
    <td valign="top" style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%"><?php echo KT_formatDate($row_rsArticles['event_date']); ?></td>
  </tr>
  <tr>
    <td valign="top" style="font-size: 14px; margin: 0 0 6px 6px; line-height:125%">EVENT LOCATION</td>
    <td valign="top" style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%"><?php echo $row_rsArticles['event_loc']; ?></td>
  </tr>
  <tr>
    <td valign="top" style="font-size: 14px; margin: 0 0 6px 6px; line-height:125%">TITLE</td>
    <td valign="top" style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%"><strong><?php echo $row_rsArticles['art_title']; ?></strong></td>
  </tr>
  <tr>
    <td valign="top" style="font-size: 14px; margin: 0 0 6px 6px; line-height:125%">FILE NAME</td>
    <td valign="top" style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%"><?php echo $row_rsArticles['art_file']; ?></td>
  </tr>
  <tr>
    <td valign="top" style="font-size: 14px; margin: 0 0 6px 6px; line-height:125%">SUBMITTED BY</td>
    <td valign="top" style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%"><?php echo $row_rsArticles['L2R_FULLNAME']; ?><br />
      <?php echo $row_rsArticles['BOULENAME']; ?></td>
  </tr>
  <tr>
    <td valign="top" style="font-size: 14px; margin: 0 0 6px 6px; line-height:125%">SUBMIT DATE</td>
    <td valign="top" style="font-size: 18px; margin: 0 0 6px 6px; line-height:125%"><?php echo KT_formatDate($row_rsArticles['date_submit']); ?></td>
  </tr>
</table>
<p style="border-top:2px black dotted;">&nbsp;</p>
<h2 style="font-size: 24px; margin: 0 0 8px 6px; line-height:125% ">Associated Images</h2>



<?php if ($totalRows_rsPhotos > 0) { // Show if recordset not empty ?>
  <table cellspacing="4" cellpadding="4">
    <?php do { ?>
      <tr valign="top">
        <td><img src="<?php echo $objDynamicThumb1->Execute(); ?>" border="0" />      </td>
        <td><p><strong><?php echo $row_rsPhotos['photo_file']; ?></strong></p>            <p><?php echo $row_rsPhotos['ph_caption']; ?></p></td>
      </tr>
      <?php } while ($row_rsPhotos = mysql_fetch_assoc($rsPhotos)); ?>
  </table>
  <?php } // Show if recordset not empty ?>
  
  
  <?php if ($totalRows_rsPhotos == 0) { // Show if recordset empty ?>
    <p style="font-size: 24px; margin: 0 0 6px 6px; line-height:125%">There were no images uploaded with this article. </p>
    <?php } // Show if recordset empty ?>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArticles);

mysql_free_result($rsPhotos);
?>
