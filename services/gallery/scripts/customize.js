/**
 * SimpleViewer admin package.
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
var global_target; 
var global_colorSetter;

window.addEvent('domready', initAccordion);
window.addEvent('domready', initColorInputs);

function initColorInputs()
{
  var colorInputs = $$('input.colorpicker');
  if (colorInputs.length == 0) return;
  colorInputs.each(setColors);
  // load the slider
  loadSV();
  // set the hue
  updateH('F1FFCC');
  colorInputs.addEvent('focus', function(event)
  {
    global_target = event.target.id;
    var picker = $('plugin');
    var tweenPicker = new Fx.Tween(picker);
    if (picker.getStyle('display') != 'block')
    {
      picker.setStyle('top', event.target.getPosition().y);
      picker.setStyle('display', 'block');
    }   
    else
    {
      tweenPicker.start('top', event.target.getPosition().y);
    }
    // Update starting color and hsv slider (these two lines have been moved from the html body script)
    loadSV();
    updateH('F1FFCC');
    global_colorSetter = $clear(global_colorSetter);
    global_colorSetter = setColors.periodical(300, event.target, event.target);
  }
  );
  // Slider fires blur event in IE so can't use it
  $$('input.text', 'select', 'input.checkbox').addEvent('focus', function()
  {
    $('plugin').setStyle('display', 'none');
    global_colorSetter = $clear(global_colorSetter);
  });
}

function setColors(e)
{
  var thisColor = e.get('value');
  if (thisColor.charAt(0) == '#') thisColor = thisColor.slice(1);
  if (thisColor.toLowerCase().substring(0,2) == '0x') thisColor = thisColor.slice(2);
  if (!(thisColor.test('^([0-9a-f]{6}){1,2}$', 'i')))
  {
    e.setStyle('background', '#FFFFFF');
    e.setStyle('color', '#FF0000');
    return;
  }
  e.setStyle('background', '#' + thisColor);
  var backgroundColor = new Color(thisColor); 
  var foregroundColor = (backgroundColor.hsb[2] < 70) ? '#FFFFFF' : '#000000';
  e.setStyle('color', foregroundColor);
}

//Called by colorjack plugin
function mkColor(v)
{
  $(global_target).value = v;
}

function initAccordion()
{
  var accordionElements = $$('.accordionElements');
  if (accordionElements.length == 0) return;
  var togglerColor = '#F9F9F9';
  var hoverColor = '#CCD8DF';
  var bgColor = '#F9F9F9';
  var allTogglers = $$('h3.togglers'); 
  var proAccordion = new Fx.Accordion($$('.togglers'), accordionElements, {
    initialDisplayFx: false,
    opacity: false,
    alwaysHide: true
  });
  allTogglers[0].setStyle('background-color', hoverColor);
  allTogglers.addEvent('mouseenter', function(){
    bgColor = this.getStyle('background-color');
    this.setStyle('background-color', hoverColor);
  });
  allTogglers.addEvent('mouseleave', function(){
    this.setStyle('background-color', bgColor);
  });
  allTogglers.addEvent('click', function()
  {
    $$('h3.togglers').setStyle('background-color', togglerColor);
    bgColor = hoverColor;
    this.setStyle('background-color', bgColor);
    $('plugin').setStyle('display', 'none');
    global_colorSetter = $clear(global_colorSetter);
  });
  /* open on tab
  $$('input', 'select').addEvent('focus', function()
  {
    containerElement = this.getParents('.accordionElements')[0];
    if (containerElement.getStyle('height').toInt() == 0) proAccordion.display(containerElement);
  });
  */
  $$('input', 'select', 'checkbox').addEvent('focus', function()
  {
    var thisContainer = this.getParents('.accordionElements')[0];
    if(!$defined(thisContainer)) return; /* focus is submit input outside accordion */
    var thisToggler = thisContainer.getPrevious();
    if (thisContainer.getStyle('height').toInt() == 0) thisToggler.fireEvent('click');
  });
}
