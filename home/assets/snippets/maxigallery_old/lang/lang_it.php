<?php
// Italian language file
// If you are going to make your own language file and are usign the lighbox,
// make a language file for it too, f.ex. /maxigallery/slidebox/slidebox_lang_en.js
$strings['previous']="&laquo; previous";
$strings['next']="next &raquo;";
$strings['click_to_zoom']=" &raquo; Clicca per ingrandire ->";
$strings['click_to_go_back']="<- Clicca per tornare indietro";
$strings['click_to_open_original']= "Clicca per aprire l'immagine grande";
$strings['back_to_normal_view'] = "Torna alla visualizzazione normale";
$strings['index']="index";
$strings['go_to_gallery']="Vai alla Galleria ->";
$strings['pictures_successfully_uploaded']="<b>Immagini caricate con successo.</b>";
$strings['changes_have_been_saved']="<b>Cambiamenti salvati.</b>";
$strings['delete']="Cancella";
$strings['title']="Titolo";
$strings['save_changes']="Salva cambiamenti";
$strings['upload_pictures']="Carica immagini";
$strings['check_to_delete_this_picture']="Seleziona per eliminare questa immagine";
$strings['manage_pictures']="Gestisci immagini";
$strings['date']="Data";
$strings['description']="Descrizione";
$strings['position']="Posizione";
$strings['gallery_description']="Descrizione Galleria";
$strings['db_no_descr_support']="<em>Attenzione! il Database non supporta le descrizioni!</em><br />";
$strings['db_no_pos_support']="<em>Attenzione! il Database non supporta la posizione delle immagini!</em><br />";
$strings['picture'] = "Immagine:";
$strings['max_pics_reached'] = "Hai raggiunto il limite massimo di immagini per questa galleria, quindi non puoi caricare altre immagini.";
$strings['supported_types'] = "<b>Tipo di file non supportato!</b> Questa galleria supporta i seguenti tipi di immagini: jpg/jpeg, png, gif and zip (which has aforementioned filetypes inside)";
$strings['gif_not_supported'] = "<b>Le librerie grafiche di questo server non supportano i file .gif images</b>Puoi usare solo immagini .jpg e .png.";
$strings['supported_types_inzip'] = "Errore: il file zip contiene un file incorretto che � stato ignorato.";
$strings['max_pics_reached_some_discarded'] = "<b>Limite massimo di immagini raggiunto!</b> Alcune delle immagini inviate sono state scartate.";
$strings['zip_not_supported'] = "<b>La tua installazione php non supporta archivi .zip !</b> Devi installare <a href=\"http://www.php.net/manual/ref.zip.php\">Il modulo PHP zip e le librerie ZZIPlib.</a>";
$strings['database_error'] = "Errore: Impossibile creare il database per la Galleria!";
$strings['database_error_ownid'] = "Errore: Non posso creare il campo nella tabella del database!";
$strings['invalid_class'] = "Error: Cannot find maxigallery class file. Please check it! ";
$strings['invalid_name_class'] = "Error: Maxigallery class name is invalid.";
?>
