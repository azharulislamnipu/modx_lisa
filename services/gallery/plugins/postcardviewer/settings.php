<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
  //GALLERY DEFAULT SETTINGS
  
  // image sort order
  define ('PV_SORT_ORDER', 'dragndrop');
  // image path as in gallery.xml with trailing separator
  define ('PV_GALLERY_TITLE', 'New PostcardViewer Gallery');
  // default image caption
  define ('PV_IMAGE_CAPTION', '');
  // default image frame color
  define ('PV_FRAME_COLOR', '0xFFFFFF');
  // default image frame width px
  define ('PV_FRAME_WIDTH', 20);
  // default image caption color
  define ('PV_CAPTION_COLOR', '0xFFFFFF');
  // default cell size px
  define ('PV_CELL_DIMENSION', 800);
  // number of display columns
  define ('PV_COLUMNS', 4);
  // default zoom-out percentage
  define ('PV_ZOOM_OUT_PERC', 15);
  // default zoom-in percentage
  define ('PV_ZOOM_IN_PERC', 100);
  // default show right click menu option
  define ('PV_ENABLE_RIGHT_CLICK_OPEN', 'true');
  // language options
  define ('PV_LANG_OPEN_IMAGE', 'Open Image in New Window');
	define ('PV_LANG_ABOUT', 'About');
  
  // OTHER SETTINGS
  
  // Specify type of urls to be stored in the gallery xml for new galleries
  // Options are 'RELATIVE' (default) for relative urls (images/myimage.jpg)
  // or 'ABSOLUTE' for absolute urls (/svmanager/g1/images/myimage.jpg)
  // or 'HTTP' for http urls (http://www.myserver.com/svmanager/g1/images/myimage.jpg)
  define('PV_URL_TYPE', 'RELATIVE');
  define('PV_BACKGROUND_COLOR', '0x181818');
  define('PV_MAX_IMAGE_WIDTH', 640);
  define('PV_MAX_IMAGE_HEIGHT', 480);
  define('PV_THUMB_WIDTH', 65);
  define('PV_THUMB_HEIGHT', 65);
  define('PV_THUMB_QUALITY', 85);
  // Url of gallery xml file relative to gallery folder
  define('PV_XML_PATH_REL_GALLERY', 'gallery.xml');
  define('PV_XML_SETTINGS_TAG', 'gallery');
  define('PV_MASTER_PATH', 'plugins'.DIRECTORY_SEPARATOR.'postcardviewer'.DIRECTORY_SEPARATOR.'pvmaster'.DIRECTORY_SEPARATOR);
  define('PV_MASTER_FILECOUNT', 4);
  define('PV_DEFAULT_THUMB_PATH', 'thumbs'.DIRECTORY_SEPARATOR);
  define('PV_DEFAULT_IMAGE_PATH', 'images'.DIRECTORY_SEPARATOR);
  define('PV_DEFAULT_SWF', 'viewer.swf');
  define('PV_CREATE_NEW', true);
?>