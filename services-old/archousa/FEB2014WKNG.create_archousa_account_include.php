<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("PREFIX", true, "text", "", "", "", "");
$formValidation->addField("FIRSTNAME", true, "text", "", "", "", "");
$formValidation->addField("LASTNAME", true, "text", "", "", "", "");
$formValidation->addField("EMAIL", true, "text", "", "", "", "You must provide your Archousa's email address.");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("{SESSION.webEmail}");
  $emailObj->setTo("{EMAIL}");
  $emailObj->setCC("");
  $emailObj->setBCC("lisa.jeter@gmail.com");
  $emailObj->setSubject("Your Invitation the Archousa Area of the Sigma Pi Phi Website");
  //FromFile method
  $emailObj->setContentFile("archousa-invite.html");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

//start Trigger_SendEmail1 trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail1(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("webmaster@sigmapiphi.org");
  $emailObj->setTo("{SESSION.webEmail}");
  $emailObj->setCC("");
  $emailObj->setBCC("lisa.jeter@gmail.com");
  $emailObj->setSubject("Invitation to the Archousa Area Sent");
  //FromFile method
  $emailObj->setContentFile("archon-confirm.html");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail1 trigger

// Make an update transaction instance
$upd_spp_archousa = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archousa);
// Register triggers
$upd_spp_archousa->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archousa->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archousa->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-home.php");
$upd_spp_archousa->registerTrigger("AFTER", "Trigger_SendEmail", 98);
$upd_spp_archousa->registerTrigger("AFTER", "Trigger_SendEmail1", 98);
// Add columns
$upd_spp_archousa->setTable("spp_archousa");
$upd_spp_archousa->addColumn("PREFIX", "STRING_TYPE", "POST", "PREFIX");
$upd_spp_archousa->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$upd_spp_archousa->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$upd_spp_archousa->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$upd_spp_archousa->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$upd_spp_archousa->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_archousa->addColumn("LASTUPDATED", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archousa->addColumn("INVITECD", "NUMERIC_TYPE", "POST", "INVITECD");
$upd_spp_archousa->addColumn("INVITECOUNT", "NUMERIC_TYPE", "POST", "INVITECOUNT");
$upd_spp_archousa->addColumn("INVITELASTDATE", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_archousa->setPrimaryKey("CUSTOMERCD", "NUMERIC_TYPE", "SESSION", "webInternalKey");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archousa = $tNGs->getRecordset("spp_archousa");
$row_rsspp_archousa = mysql_fetch_assoc($rsspp_archousa);
$totalRows_rsspp_archousa = mysql_num_rows($rsspp_archousa);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/spp_include.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet_include.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="/services/includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="/services/includes/common/js/base.js" type="text/javascript"></script>
<script src="/services/includes/common/js/utility.js" type="text/javascript"></script>
<script src="/services/includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="content" -->
<?php if ($totalRows_rsspp_archousa == 0) { // Show if recordset empty ?>
  <div id="single" style="background-color:#CCCCCC;  width: 50%; padding: 8px;">
    <h2>Marital Status</h2>
    <p>You are currently listed in the Grand Boul&eacute; database as single. If your marital status has changed, please <a href="[~6771~]">update your profile</a> and <a href="mailto:sigmapiphi.webmaster@sigmapiphi.org">contact the webmaster</a> for assistance. </p>
  </div>
  <?php } // Show if recordset empty ?>
<?php if ($totalRows_rsspp_archousa > 0) { // Show if recordset not empty ?>
  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">
    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
            <td class="KT_th"><label for="PREFIX">SALUTATION:</label></td>
          <td><input type="text" name="PREFIX" id="PREFIX" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['PREFIX']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("PREFIX");?> <?php echo $tNGs->displayFieldError("spp_archousa", "PREFIX"); ?> </td>
        </tr>
          <tr>
            <td class="KT_th"><label for="FIRSTNAME">FIRST NAME:</label></td>
          <td><input type="text" name="FIRSTNAME" id="FIRSTNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['FIRSTNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("spp_archousa", "FIRSTNAME"); ?> </td>
        </tr>
          <tr>
            <td class="KT_th"><label for="MIDDLEINITIAL">MIDDLE INITIAL:</label></td>
          <td><input type="text" name="MIDDLEINITIAL" id="MIDDLEINITIAL" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['MIDDLEINITIAL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("MIDDLEINITIAL");?> <?php echo $tNGs->displayFieldError("spp_archousa", "MIDDLEINITIAL"); ?> </td>
        </tr>
          <tr>
            <td class="KT_th"><label for="LASTNAME">LAST NAME:</label></td>
          <td><input type="text" name="LASTNAME" id="LASTNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['LASTNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("spp_archousa", "LASTNAME"); ?> </td>
        </tr>
          <tr>
            <td class="KT_th"><label for="EMAIL">EMAIL:</label></td>
          <td><input type="text" name="EMAIL" id="EMAIL" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['EMAIL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("EMAIL");?> <?php echo $tNGs->displayFieldError("spp_archousa", "EMAIL"); ?> </td>
        </tr>
          <tr class="KT_buttons">
            <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Send Invitation" />            </td>
        </tr>
        </table>
      <input type="hidden" name="UPDATEDBY" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($_SESSION['webShortname']); ?>" />
        <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo KT_formatDate($row_rsspp_archousa['LASTUPDATED']); ?>" />
        <input name="INVITECD" type="hidden" id="INVITECD" value="1" />
        <input name="INVITECOUNT" type="hidden" value="<?php echo $row_rsspp_archousa['INVITECOUNT']+1; ?>" />
        <input type="hidden" name="INVITELASTDATE" id="INVITELASTDATE" value="<?php echo KT_formatDate($row_rsspp_archousa['INVITELASTDATE']); ?>" />
      </form>
     </div>
  </div>
  <?php } // Show if recordset not empty ?>
<p>&nbsp;</p>
<br class="clear"/>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
