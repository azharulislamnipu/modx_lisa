<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

// Load the CSV classes
require_once('../../includes/csv/CSV.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// begin Recordset
$colname__rsArchonExport = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsArchonExport = $_SESSION['BOULENAME'];
}
$query_rsArchonExport = sprintf("SELECT BOULENAME, STATUS, JOINDATE, `PREFIX`, FIRSTNAME, MIDDLEINITIAL, LASTNAME, SUFFIX, DESIGNATIONLST, ADDRESS1, ADDRESS2, CITY, STATECD, ZIP, HOMEPHONE, EMAIL, JOBTITLE, ORGNAME, ALTADDRESS1, ALTADDRESS2, ALTCITY, ALTSTATE, ALTZIP, WORKPHONE, ALTEMAIL, BIRTHDATE, DECEASEDDATE, SPOUSENAME FROM vw_gramm_archons WHERE BOULENAME = %s ORDER BY LASTNAME ASC", GetSQLValueString($colname__rsArchonExport, "text"));
$rsArchonExport = $sigma_modx->SelectLimit($query_rsArchonExport) or die($sigma_modx->ErrorMsg());
$totalRows_rsArchonExport = $rsArchonExport->RecordCount();
// end Recordset

// Begin CSVExport 	
$csvExportObj = new CSV_Export($rsArchonExport);
$csvExportObj->addColumn("BOULENAME", "STRING_TYPE", "BOULE");
$csvExportObj->addColumn("STATUS", "STRING_TYPE", "STATUS");
$csvExportObj->addColumn("JOINDATE", "DATE_TYPE", "JOIN DATE");
$csvExportObj->addColumn("PREFIX", "STRING_TYPE", "PREFIX");
$csvExportObj->addColumn("FIRSTNAME", "STRING_TYPE", "FIRSTNAME");
$csvExportObj->addColumn("MIDDLEINITIAL", "STRING_TYPE", "MIDDLEINITIAL");
$csvExportObj->addColumn("LASTNAME", "STRING_TYPE", "LASTNAME");
$csvExportObj->addColumn("SUFFIX", "STRING_TYPE", "SUFFIX");
$csvExportObj->addColumn("DESIGNATIONLST", "STRING_TYPE", "HONORIFIC");
$csvExportObj->addColumn("ADDRESS1", "STRING_TYPE", "ADDRESS1");
$csvExportObj->addColumn("ADDRESS2", "STRING_TYPE", "ADDRESS2");
$csvExportObj->addColumn("CITY", "STRING_TYPE", "CITY");
$csvExportObj->addColumn("STATECD", "STRING_TYPE", "STATECD");
$csvExportObj->addColumn("ZIP", "STRING_TYPE", "ZIP");
$csvExportObj->addColumn("HOMEPHONE", "STRING_TYPE", "HOME PHONE");
$csvExportObj->addColumn("WORKPHONE", "STRING_TYPE", "WORK PHONE");
$csvExportObj->addColumn("EMAIL", "STRING_TYPE", "EMAIL");
$csvExportObj->addColumn("BIRTHDATE", "DATE_TYPE", "BIRTHDATE");
$csvExportObj->addColumn("SPOUSENAME", "STRING_TYPE", "ARCHOUSA NAME");
$csvExportObj->addColumn("DECEASEDDATE", "DATE_TYPE", "DECEASEDDATE");
$csvExportObj->setFilename("BOULE_ARCHONS.csv");
$csvExportObj->setHeaders(true);
$csvExportObj->setDelimiter(",");
$csvExportObj->setEnclosure("\"");
$csvExportObj->Execute("VALUE", "1");
// End CSVExport 	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" --> 

<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->

<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsArchonExport->Close();
?>
