<?php
require_once('includes/header.php');

if (file_exists(SITE_ROOT.'/themes/'.THEME.'/index.php')){//index from the theme!
	require_once(SITE_ROOT.'/themes/'.THEME.'/index.php'); 
}
else{//default not found in theme
	
?>

	<?php if ($advs) echo '<div class="category">'.advancedSearchForm().'</div>';?>
	
	<?php if(isset($categoryName)&&isset($categoryDescription)){ ?>
	<div class="category">
	    <h1><?php echo $categoryName;?></h1> 
		<p>
			 <?php echo $categoryDescription;?>
			 <a title="<?php echo T_POST_IN;?> <?php echo $categoryName;?>" href="<?php echo SITE_URL.newURL();?>?category=<?php echo $currentCategory;?>"><?php echo T_POST_IN;?> <?php echo $categoryName;?></a> 
		</p>
	</div>
	<?php }?>
	
<div class="item">
<?php 
	if ($resultSearch){
	foreach ( $resultSearch as $row ){
		$idPost=$row['idPost'];
		$postType=$row['type'];
		$postTypeName=getTypeName($postType);
		$postTitle=$row['title'];
		$postPrice=$row['price'];
		$postDesc= substr(strip_tags(html_entity_decode($row['description'], ENT_QUOTES, CHARSET)), 0, 200)."...";
		$category=$row['category'];//real category name
		$fcategory=$row['fcategory'];//frienfly name category
		$idCategoryParent=$row['idCategoryParent'];
		$fCategoryParent=$row['parent'];
		$postImage=$row['image'];
		$postPassword=$row['password'];
		if ($postImage=="") $postImage=SITE_URL."/images/no_pic.png";
		else $postImage=SITE_URL.IMG_UPLOAD.$idPost."/thumb_".$postImage;
		
		if ($insertDate!=setDate($row['insertDate'])){
			$insertDate=setDate($row['insertDate']);
			echo "<h3>".$insertDate."</h3>";
		}

		$postUrl=itemURL($idPost,$fcategory,$postTypeName,$postTitle,$fCategoryParent);
		 	
		?>
		<div class="post" style="float:left;">
			<?php if (MAX_IMG_NUM>0){?>
				<img style="float:left;margin-right:10px;" class="thumb" title="<?php echo $postTitle." ".$postTypeName." ".$fcategory;?>"  alt="<?php echo $postTitle." ".$postTypeName." ".$fcategory;?>"  src="<?php echo $postImage;?>" />
			<?php }?>
				
			<a title="<?php echo $postTitle." ".$postTypeName." ".$fcategory;?>" href="<?php echo SITE_URL.$postUrl;?>" ><h2><?php echo $postTitle;?></h2></a>		
			<br />		
			<p>
			    <?php echo $postTypeName;?>
                <?php echo '<a href="'.SITE_URL.catURL($fcategory,$fCategoryParent).'" title="'.$category.' '.$idCategoryParent.'">'.$category.'</a>';?>
			    <a title="<?php echo $postTitle." ".$postTypeName." ".$category;?>" href="<?php echo SITE_URL.$postUrl;?>" >
			        <?php echo $postTitle;?> 	
				</a>
				<?php if ($postPrice!=0) echo " - ".getPrice($postPrice);?>
		    </p>
			<p><?php echo $postDesc;?></p>
			
			<?php if(isset($_SESSION['admin'])){?><br />
				<a href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idPost;?>&amp;pwd=<?php echo $postPassword;?>&amp;action=edit">
						<?php echo T_EDIT;?></a><?php echo SEPARATOR;?>
				<a onClick="return confirm('<?php echo T_DEACTIVATE;?>?');" 
					href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idPost;?>&amp;pwd=<?php echo $postPassword;?>&amp;action=deactivate">
						<?php echo T_DEACTIVATE;?></a><?php echo SEPARATOR;?>
				<a onClick="return confirm('<?php echo T_SPAM;?>?');"
					href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idPost;?>&amp;pwd=<?php echo $postPassword;?>&amp;action=spam">
						<?php echo T_SPAM;?></a><?php echo SEPARATOR;?>
				<a onClick="return confirm('<?php echo T_DELETE;?>?');"
					href="<?php echo SITE_URL;?>/manage/?post=<?php echo $idPost;?>&amp;pwd=<?php echo $postPassword;?>&amp;action=delete">
						<?php echo T_DELETE;?></a>
			<?php }?>
				
		</div>
		<?php 
	}
}//end if check there's results
else echo "<h2>".T_NOTHING_FOUND."</h2>";
?>
</div>
	
	<div class="item">&nbsp;<br />
	<?php //page numbers
		if ($total_pages>1){
			
			//if is a search
			if (strlen(cG("s"))>=MIN_SEARCH_CHAR) $search="&s=".cG("s");
			
			$pag_title=$html_title." ".T_PAGE." ";

			//getting the url
			if(strlen(cG("s"))>=MIN_SEARCH_CHAR){//home with search
				$pag_url='?s='.cG("s").'&category='.$currentCategory.'&page=';
			}
			elseif ($advs){//advanced search
				$pag_url="?category=$currentCategory&title=".cG("title")."&desc=".cG("desc")."&price=".cG("price")."&place=".cG("place")."&sort=".cG("sort")."&page=";
			}
			elseif (isset($type)){ //only set type in the home
				$pag_url=typeURL($type,$currentCategory).'&page=';
			}
			elseif (isset($currentCategory)){//category
				$pag_url=catURL($currentCategory,$selectedCategory);//only category
				if(!FRIENDLY_URL) $pag_url.='&page=';
			}
			else {
			    $pag_url="/";//home
			    if(!FRIENDLY_URL) $pag_url.='?page=';
			}
			//////////////////////////////////
		
			if ($page>1){
				echo "<a title='$pag_title' href='".SITE_URL.$pag_url."1'>&laquo;&laquo;</a>".SEPARATOR;//First
				echo "<a title='".T_PREVIOUS." $pag_title".($page-1)."' href='".SITE_URL.$pag_url.($page-1)."'>".T_PREVIOUS."</a>";//previous
			}
			//pages loop
			for ($i = $page; $i <= $total_pages && $i<=($page+DISPLAY_PAGES); $i++) {//for ($i = 1; $i <= $total_pages; $i++) {
		        if ($i == $page) echo SEPARATOR."<b>$i</b>";//not printing link current page
		        else echo SEPARATOR."<a title='$pag_title$i' href='".SITE_URL."$pag_url$i'>$i</a>";//print the link
		    }
		    
		    if ($page<$total_pages){
		    	echo SEPARATOR."<a href='".SITE_URL.$pag_url.($page+1)."' title='".T_NEXT." $pag_title".($page+1)."' >".T_NEXT."</a>";//next
		    	echo  SEPARATOR."<a title='$pag_title$total_pages' href='".SITE_URL."$pag_url$total_pages'>&raquo;&raquo;</a>";//End
		    }
		}	
	?>
	</div>

<?php
}//if else

require_once('includes/footer.php');
?>
