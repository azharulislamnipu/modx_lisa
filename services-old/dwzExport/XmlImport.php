<?php
ob_start();
if(!isset($_SESSION)) 
{ 
	session_start(); 
}  


include('dwzDataBase.php');
include('SetTempFolder.php');


//'**********************************
//' http://www.DwZone.it
//' Xml Import
//' Copyright (c) DwZone.it 2000-2005
//'**********************************
class dwzXmlImport
{
	
	var $xmlContent,	
	$root,
	$debug,
	$startOnEvent,
	$startOnValue,
	$redirectPage,
	$displayErrors,
	$importFrom,
	$filePath,
	$fileUrl,
	$fullFilePath,
	$progress_key,
	
	$hostname, 
	$database, 
	$username, 
	$password,
	$db,
	$table,
	$tableUniqueKey,
	$colIsNum,
	$onDuplicateEntry,
	$xmlUniqueKey,
	$xmlRepeatItem,
	$xmlData,
	
	$itemFieldRec,
	$itemFormat,
	$itemFromValue,
	$itemReference,
	
	$progressBar,
	$progressUpdate,
	$progressPagePath,
	$startTime,
	$totalLineNumber,
	
	$totalRows,
	$inErr,
	$errMsg;
	

			
	function SetProgressBar($param){
		$tmp = preg_split("/@_@/", $param);
		$this->progressBar = $tmp[0];
		if(floatval($tmp[1]) == 0){
			$this->progressUpdate = 10;			
		}else{
			$this->progressUpdate = intval($tmp[1]);
		}
		$this->progressPagePath = $this->root ."dwzExport/";
	}


	function Additem($rec, $from, $format, $reference){
		$this->itemFieldRec[] = $rec;
		$this->itemFromValue[] = $from;
		$this->itemFormat[] = $format;
		$this->itemReference[] = $reference;
	}
	
	function SetExtraData($param){
		$tmp = preg_split("/@_@/", $param);
		$this->root = $tmp[0];
	}
	
	function SetStartOn($param1,$param2){
		$this->startOnEvent = $param1;
		$this->startOnValue = $param2;
	}
	
	function SetRedirectPage($param){
		$this->redirectPage = $param;
	}
	
	function SetDisplayErrors($param){
		if(strtolower($param) == "true"){
			$this->displayErrors = true;
		}else{
			$this->displayErrors = false;
		}
	}
	
	function SetImportFrom($param){
		$this->importFrom = $param;
	}
	
	function SetFilePath($param){
		$this->filePath = $param;
	}
	
	function SetFileUrl($param){
		$this->fileUrl = $param;
	}
	
		
	function SetConnection($host, $db, $user, $pwd){
		$this->hostname = $host;
		$this->database = $db;
		$this->username = $user;
		$this->password = $pwd;
	}
	
	function SetTable($param){
		$this->table = $param;
	}
	
	function SetTableUniqueKey($param){
		$this->tableUniqueKey = $param;
	}
	
	function SetColIsNum($param){
		if(strtolower($param) == "true"){
			$this->colIsNum = true;
		}else{
			$this->colIsNum = false;
		}
	}
	
	function SetOnDuplicateEntry($param){
		$this->onDuplicateEntry = $param;
	}
	
	function SetXmlUniqueKey($param){
		$this->xmlUniqueKey = $param;
	}
		
	function GetProgressKey(){
		return $this->progress_key;
	}
	
	
	function SetXmlRepeatItem($param){
		$tmp = preg_split("/\|_\|/", $param);
		$this->xmlRepeatItem = $tmp[1];
		if(substr($this->xmlRepeatItem , -1) == "/"){
			$this->xmlRepeatItem = substr($this->xmlRepeatItem , 0, -1);
		}
	}
	
	function Init (){
		$this->itemFieldRec = array();
		$this->itemFormat = array();
		$this->itemFromValue = array();
		$this->itemReference = array();
		$this->itemColumn = array();
		$this->csvData = array();
		$this->inErr = false;
		$this->debug = true;
		$this->totalRows = -1;
		$this->errMsg = array();
		
		if(isset($_GET["xml_import_progress_key"]) && $_GET["xml_import_progress_key"] != ""){
			$this->progress_key = $_GET["xml_import_progress_key"];
			
		}elseif(isset($_POST["xml_import_progress_key"]) && $_POST["xml_import_progress_key"] != ""){
			$this->progress_key = $_POST["xml_import_progress_key"];
			
		}else{
			$this->progress_key = uniqid("");
		}
		$this->DeleteProgressFile();
	}	
	
	
	function Start(){
		$retStr = false;
		switch(strtoupper($this->startOnEvent)){
		case "GET":
			if(isset($_GET[$this->startOnValue]) && $_GET[$this->startOnValue] != ""){
				$retStr = true;
			}
			break;
		case "POST":
			if(isset($_POST[$this->startOnValue]) && $_POST[$this->startOnValue] != ""){
				$retStr = true;
			}
		case "SESSION":
			session_start();
			if(isset($_SESSION[$this->startOnValue]) && $_SESSION[$this->startOnValue] != ""){
				$retStr = true;
			}
			break;
		case "COOKIE":
			if(isset($_COOKIE[$this->startOnValue]) && $_COOKIE[$this->startOnValue] != ""){
				$retStr = true;
			}
		case "ONLOAD":
			$retStr = true;
		}
		return $retStr;
	}
	
	
	function Execute(){
		if(!$this->Start()){
			return;
		}
		//If Not isObject(recordset) Then
		//	Response.write "<strong>DwZone - XML Export Error.</strong><br/>The recordset is not valid"
		//	Exit sub
		//End If
		if(count($this->itemFieldRec) < 1){
			ob_clean();
			echo "<strong>DwZone - CSV Import Error.</strong><br/>No item defined";
			exit();
		}
		
		if($this->filePath == ""){
			ob_clean();
			echo "<strong>DwZone - CSV Import Error.</strong><br/>The file path is void";
			exit();
		}
		
		$this->fullFilePath = $this->GetFilePath();
		
		if(!file_exists($this->fullFilePath)){
			$this->AddError( "101", "The file: " .$this->fullFilePath ." is not find", "getfilePath");
		}
		
		if(count($this->errMsg) > 0){
			$this->ResponseError();
			exit();
		}
				
		$this->ReadXmlContent();
		
		//$this->Debug();
		
		if(count($this->errMsg) > 0){
			$this->ResponseError();
			exit();
		}
		
		$this->ImportData();

		if(count($this->errMsg) > 0){
			$this->ResponseError();
			exit();
		}
		
		if($this->redirectPage != ""){
			$aaa = $this->redirectPage;
			header("location: " .$this->redirectPage);
			exit();
		}		
	}
	
	function ResponseError(){
		if($this->displayErrors){			
			$retStr = "<table border=1>";
			$retStr .= "<tr><td colspan=3></td></tr>";
			$retStr .= "<tr>";
			$retStr .= "<td>Number</td>";
			$retStr .= "<td>Description</td>";
			$retStr .= "<td>Position</td>";
			$retStr .= "</tr>";
			foreach($this->errMsg as $msg){
				$tmp = "<tr>";
				$tmp .= "<td>" .$msg['Number'] ."</td>";
				$tmp .= "<td>" .$msg['Description'] ."</td>";
				$tmp .= "<td>" .$msg['Position'] ."</td>";
				$tmp .= "</tr>";
				$retStr .= $tmp;
			}
			$retStr .= "</table>";
			ob_clean();
			echo $retStr;
			exit();
		}else{
			if($this->redirectPage != ""){
				header("location: " .$this.redirectPage);
				exit();
			}
		}
	}
	
	function ImportData(){
		
		$this->startTime = mktime();
        
		$this->db = new dwzDataBase();
		
		$this->db->SetConn($this->hostname,
							$this->database,
							$this->username,
							$this->password);
		
		for($i=0; $i<=$this->totalRows; $i++){
			if($this->progressBar != ""){
				if($i == 0 || $i % $this->progressUpdate == 0){
					$sStato = "1";
					$sN_Line = $i + 1;
					$sT_Line = $this->totalRows + 1;
					$sstartTime =  mktime() - $this->startTime;
					$oper = "Import data";
					$this->WriteProgressBarInfo( $sStato, $sN_Line, $sT_Line, $sstartTime, $oper);
				}
			}
			
			if($this->onDuplicateEntry == "NoVerify"){
				$result = $this->CreateInsertQuery($i);
			}else{
				$recExists = $this->RecordExists($i);
				if($recExists){
					if($this->onDuplicateEntry == "Update"){
						$result = $this->CreateUpdateQuery($i);
					}elseif($this->onDuplicateEntry == "Skip"){
						$result = true;
					}else{
						$result = false;
						$this->AddError("110", "Row: " .($i + 1) ."- Err: Duplicate entry", "ImportData");
					}
				}else{
					$result = $this->CreateInsertQuery($i);
				}
			}			
		}
		
		$this->db->Close();
		
		if($this->progressBar != ""){
			$sStato = "DONE";
			$sN_Line = $this->totalRows + 1;
			$sT_Line = $this->totalRows + 1;
			$sstartTime =  mktime() - $this->startTime;
			$oper = "Import data completed";
			$this->WriteProgressBarInfo( $sStato, $sN_Line, $sT_Line, $sstartTime, $oper);
		}			
	}
	
	function AddError($n, $d, $p){
		$this->errMsg[] = array("Number" => $n, "Description" => $d, "Position" => $p);
	}
	
	function CreateUpdateQuery($index){
				
		$update["table_name"] = $this->table;
		
		for($i=0; $i<count($this->itemReference); $i++){
			$value = $this->GetXmlData($index, $this->itemReference[$i], $this->itemFromValue[$i]);
			$value = $this->FormatValue($value, $this->itemFormat[$i]);
			$type = $this->GetFieldType($this->itemFormat[$i]);
			
			$def_value = "";
			$not_def_value = "";
			if($type == "defined"){
				$tmp = preg_split("/,/", $this->itemFormat[$i]);
				$def_value = $tmp[1];
				$not_def_value = $tmp[2];
			}
			
			$update["fields"][$i] = array(
									"name" => $this->itemFieldRec[$i],
									"value" => $value,
									"type" => $type,
									"def_value" => $def_value,
									"not_def_value" => $not_def_value									
									);
		}
		
		$update["where"][0] = array(
								"name" => $this->tableUniqueKey,
								"value" => $this->GetXmlData($index, $this->xmlUniqueKey, "xml"),
								"type" => ($this->colIsNum ? "int" : "text")
								);
		
		$result = $this->db->Update($update);
						
		if($result !== true){
			$this->AddError("126", "Error in sql: " .$this->db.GetSql() , "CreateUpdateQuery");
			$this->AddError("126", "Error message: " .$result , "CreateUpdateQuery");
		}
		
		return $result;
	}
	
	
	function CreateInsertQuery($index){
			
		$insert["table_name"] = $this->table;
		
		for($i=0; $i<count($this->itemReference); $i++){
			$value = $this->GetXmlData($index, $this->itemReference[$i], $this->itemFromValue[$i]);
			$value = $this->FormatValue($value, $this->itemFormat[$i]);
			$type = $this->GetFieldType($this->itemFormat[$i]);
			
			$def_value = "";
			$not_def_value = "";
			if($type == "defined"){
				$tmp = preg_split("/,/", $this->itemFormat[$i]);
				$def_value = $tmp[1];
				$not_def_value = $tmp[2];
			}
			
			$insert["fields"][$i] = array(
									"name" => $this->itemFieldRec[$i],
									"value" => $value,
									"type" => $type,
									"def_value" => $def_value,
									"not_def_value" => $not_def_value									
									);
		}
		
		$result = $this->db->Insert($insert);
				
		if($result !== true){
			$this->AddError("127", "Error in sql: " .$this->db->GetSql() , "CreateInsertQuery");
			$this->AddError("127", "Error message: " .$result , "CreateInsertQuery");
		}
		
		return $result;		
	}
	
	function RecordExists($index){
		
		$select["table_name"] = $this->table;
		
		$select["fields"][0] = array(
								"name" => $this->tableUniqueKey,
								"value" => "",
								"type" => ($this->colIsNum ? "int" : "text")
								);
		
		$select["where"][0] = array(
								"name" => $this->tableUniqueKey,
								"value" => $this->GetXmlData($index, $this->xmlUniqueKey, "xml"),
								"type" => ($this->colIsNum ? "int" : "text")
								);
		
		$recordset = $this->db->Select($select);
		
		if($recordset === false){
			$this->AddError("128", "Error in sql: " .$this->db->GetSql() , "RecordExists");
			$this->AddError("128", "Error message: " .$result , "RecordExists");
		}		
				
		$result = false;
		if(mysql_num_rows($recordset) > 0){
			$result = true;
		}
		
		return $result;
	}
		
	
	function GetXmlData($index, $strValue, $valueFrom){		
		if($valueFrom == ""){
			for($i=0; $i<count($this->itemReference); $i++){
				if(strtolower($this->itemReference[$i]) == strtolower($strValue)){
					$valueFrom = strtolower($this->itemFromValue[$i]);
					break;
				}
			}
		}else{
			$valueFrom = strtolower($valueFrom);
		}
		
		$retStr = "";
		switch($valueFrom){
		case "xml":
			$tmp = preg_split("/\|_\|/", $strValue);
			$retStr = @$this->xmlData[$index][$tmp[0]];
			break;
		case "get":
			$retStr = @$_GET[$strValue];
			break;
		case "post":
			$retStr = @$_POST[$strValue];
			break;		
		case "session":
			$retStr = @$_SESSION[$strValue];
			break;
		case "cookie":
			$retStr = @$_COOKIE[$strValue];
			break;
		case "entered":
			$retStr = $strValue;
			break;
		}
		return $retStr;
		
		
		
		/*dim K
		ValueFrom = ""
		for K=0 to ItemCount
			if lcase(ItemReference(K)) = lcase(strValue) then
				ValueFrom = lcase(ItemFromValue(K))
				exit for
			end if
		next
		
		retStr = ""
		select case ValueFrom
		case "xml"
			err.clear
			on error resume next
			tmp = split(ItemReference(K), "|_|")
			retStr = XmlData(index)(lcase(tmp(0)))
			if err.number <> 0 then
				ErrMsg.add Err.number,Err.Description,"getXmlData - index: " & index & " - strValue: " & strValue
			end if
			on error goto 0
			err.clear
		case "get"
			retStr = request.QueryString(strValue)
		case "post"
			retStr = request.Form(strValue)
		case "request"
			retStr = request(strValue)
		case "application"
			retStr = Application(strValue)
		case "session"
			retStr = Session(strValue)
		case "cookie"
			retStr = request.Cookies(strValue)
		case "entered"
			retStr = strValue
		end select
		getXmlData = retStr
	end function*/
	}
	
	function ReadXmlContent(){				
		$this->startTime = mktime();
		
		if($this->importFrom == "File"){
			$xml = simplexml_load_file($this->fullFilePath, 'SimpleXMLElement', LIBXML_NOCDATA);
		}else{
			$xml = simplexml_load_file($this->fileUrl, 'SimpleXMLElement', LIBXML_NOCDATA);
		}
		
		$xPath = substr($this->xmlRepeatItem, strpos($this->xmlRepeatItem, "/") + 1);
		$nodes = $xml->xpath($xPath);
		
		$this->totalLineNumber = count($nodes);
		$this->totalRows = -1;
		
		foreach($nodes as $node){			
			$row = array();
			for($i=0; $i<count($this->itemFieldRec); $i++){
				$xml = new SimpleXMLElement($node->asXML());
				
				$tmp = preg_split("/\|_\|/", $this->itemReference[$i]);
				$name = $tmp[0];
				$xPath = $this->GerRealXPath($tmp[1]);
				$res = $xml->xpath($xPath);
				$value = "";
				if(count($res)){
					$value = implode("", $res);
				}
				$row[$name] = $value;
			}
			
			//'Verify if the XmlUniqueKey is in the XmlData object
			$tmp = preg_split("/\|_\|/", $this->xmlUniqueKey);
			$name = $tmp[0];
			$xPath = $this->GerRealXPath($tmp[1]);
			if(!array_key_exists($name, $row)){
				$res = $xml->xpath($xPath);
				$value = "";
				if(count($res)){
					$value = implode("", $res);
				}
				$row[$name] = $value;
			}			
			
			$this->totalRows ++;
			$this->xmlData[$this->totalRows] = $row;
			
			if($this->progressBar != ""){
				if( $this->totalRows == 0 || $this->totalRows % $this->progressUpdate == 0){
					$sStato = "1";
					$sN_Line = $this->totalRows + 1;
					$sT_Line = $this->totalLineNumber;
					$sStartTime =  mktime() - $this->startTime;
					$oper = "Read xml data";
					$this->WriteProgressBarInfo( $sStato, $sN_Line, $sT_Line, $sStartTime, $oper);
				}
			}
		}
		
		
		
		/*
		For each objNode in nodes
			If objNode.nodeType = NODE_ELEMENT Then
				TotalRows = TotalRows + 1
				ReDim Preserve XmlData(TotalRows)
				
				if ProgressBar<>"" then
					if TotalRows=1 or TotalRows Mod ProgressUpdate = 0 then
						sStato = "1"
						sN_Line = TotalRows
						sT_Line = TotalLineNumber
						sStartTime =  Clng((Now() - StartTime) * 86400 * 1000)
						oper = "Read xml data"
						WriteProgressBarInfo sStato, sN_Line, sT_Line, sStartTime, oper
					end if
				end if
												
				set XmlData(TotalRows) = Server.CreateObject("Scripting.Dictionary")
				for J=0 to ItemCount					
					if lcase(ItemFromValue(J)) = "xml" then
						tmp = split(ItemReference(J), "|_|")
						nName = tmp(0)
						xPath = GerRealXPath(tmp(1))
						nValue = ""
												
						set objChildNode = objNode.selectSingleNode(xPath)
						If objChildNode.nodeType = NODE_ATTRIBUTE Then
							nValue = objChildNode.Text
						elseif objChildNode.hasChildNodes then
							if objChildNode.ChildNodes(0).nodeType = NODE_CDATA then
								nValue = objChildNode.ChildNodes(0).Text
							else
								nValue = objChildNode.Text
							end if
						else
							nValue = objChildNode.Text
						end if
						XmlData(TotalRows)(lcase(nName)) = nValue
					end if
				next
				
				//'Verify if the XmlUniqueKey is in the XmlData object
				tmp = split(XmlUniqueKey, "|_|")
				nName = tmp(0)
				xPath = GerRealXPath(tmp(1))
				if not XmlData(TotalRows).Exists(lcase(nName)) then
					nValue = ""
					set objChildNode = objNode.selectSingleNode(xPath)
					If objChildNode.nodeType = NODE_ATTRIBUTE Then
						nValue = objChildNode.Text
					elseif objChildNode.hasChildNodes then
						if objChildNode.ChildNodes(0).nodeType = NODE_CDATA then
							nValue = objChildNode.ChildNodes(0).Text
						else
							nValue = objChildNode.Text
						end if
					else
						nValue = objChildNode.Text
					end if
					XmlData(TotalRows)(lcase(nName)) = nValue
				end if
				
			end if
		next
		*/
	}
	
	function GerRealXPath($str){
		$retVal = substr($str, strlen($this->xmlRepeatItem) + 1);
		if(substr($retVal, 1) == "/"){
			$retVal = substr($retVal, 2);
		}
		return $retVal;
	}
	
	function GetRandomNumber(){
		return uniqid("");
	}
	
	function GetSiteRoot(){
		$path = @$_SERVER['DOCUMENT_ROOT'];
		if(strlen($path) == 0){
			$path = @$HTTP_SERVER_VARS['DOCUMENT_ROOT'];
		}
		if(strlen($path) == 0){
			$path = @$_SERVER['APPL_PHYSICAL_PATH'];		
		}
		if(strlen($path) == 0 && isset($_SESSION)){
			$path = @$_SESSION['SITE_ROOT'];
			if(strlen($path) != 0){
				if(substr($path, -1) == "/" || substr($path, -1) == "\\"){
					$path = substr($path, 0, -1);
				}
			}
		}
		return $path;
	}
	
	function GetFilePath(){
		if($this->filePath == ""){
			$this->AddError("100","The file is missing","getfilePath");
			break;
		}else{
			if(is_file($this->filePath)){
				return $this->filePath;
			}
			$root = $this->GetSiteRoot() ;	
			$this->filePath = str_replace("\\", "/", $this->filePath);
			if(substr($this->filePath, 0, 1) == "/"){				
				if(strpos($root, "/") !== false){
					//	/folder/
					if(substr($root, -1) == "/"){
						$this->filePath = substr($this->filePath, 1);
					}
					return $root .$this->filePath;		
				}else{
					//	c:\folder\
					if(substr($root, -1) == "\\"){
						$this->filePath = substr($this->filePath, 1);
					}
					return $root .str_replace("/", "\\", $this->filePath);
				}
			}else{												
				if(substr($this->filePath, 1) != "." && substr($this->filePath, 1) != "/"){
					$root = realpath('.') ;
					if(strpos($root, "/") !== false){
						//	/folder/			
						if(substr($root, -1) != "/"){
							$root .= "/";
						}
						return $root .$this->filePath;		
					}else{
						//	c:\folder\
						if(substr($root, -1) != "\\"){
							$root .= "\\";
						}
						return $root .str_replace("/", "\\", $this->filePath);
					}
				}else{
					if(!is_dir(realpath($this->filePath))){
						$this->AddError("621","The path " .$this->filePath ."is missing","getfilePath");
						return $this->filePath;
					}
					return realpath($this->filePath);
				}				
			}
		}
	}
	
	function GetFieldType($format){
		if(strtolower(substr($format, 0, 1)) == "s"){
			return "text";
			
		}elseif(strtolower(substr($format, 0, 2)) == "ni"){
			return "int";
		
		}elseif(strtolower(substr($format, 0, 1)) == "n"){
			return "double";
			
		}elseif(strtolower(substr($format, 0, 1)) == "d"){
			return "date";
			
		}elseif(strtolower(substr($format, 0, 1)) == "b"){
			return "defined";
			
		}else{
			return "text";
		}
	}

	
	
	function FormatValue($strValue, $format){
		if(strtolower(substr($format, 0, 1)) == "s"){
			return $strValue;
			
		}elseif(strtolower(substr($format, 0, 1)) == "n"){
			return $this->FormatAsNumber($strValue, $format);
			
		}elseif(strtolower(substr($format, 0, 1)) == "d"){
			return $this->FormatAsDate($strValue, substr($format, 2));
			
		}elseif(strtolower(substr($format, 0, 1)) == "b"){
			return $this->FormatAsBoolean($strValue, $format);
			
		}else{
			return $strValue;
		}
	}
			
	function FormatAsBoolean($strValue, $format){
		if($strValue == ""){
			return "";
		}
		$tmp = preg_split("/,/", $format);
		if(strtolower($strValue) == strtolower($tmp[1])){
			return $strValue;
		}else{
			return "";
		}
	}
	
	function FormatAsDate($strValue, $format){
		if($strValue == ""){
			return "";
		}
		
		$day = "00";
		$month = "00";
		$year = "00";
		$hours = "00";
		$minutes = "00";
		$seconds = "00";
		
		$tmp = preg_split("/\s/", $strValue);		
		$data = $tmp[0];
		if(count($tmp) > 1){
			$time = $tmp[1];
		}else{
			$time = "00:00:00";
		}
		$tmp = preg_split("/:/", $time);
		$hours = $tmp[0];
		$minutes = $tmp[1];
		$seconds = $tmp[2];
		
		switch($format){
		case "DD/MM/YYYY":
		case "DD-MM-YYYY":
		case "DD.MM.YYYY":
		case "DD/MM/YY":
		case "DD-MM-YY":
		case "DD.MM.YY":
		case "DD/MM/YYYY hh:mm:ss":
		case "DD-MM-YYYY hh:mm:ss":
		case "DD.MM.YYYY hh:mm:ss":		 
		case "DD/MM/YY hh:mm:ss":
		case "DD-MM-YY hh:mm:ss":
		case "DD.MM.YY hh:mm:ss":
		 	if(substr($data, 2, 1) == "/" || substr($data, 2, 1) == "."){
				$tmp = preg_split("/\\" .substr($data, 2, 1) ."/", $data);
			}else{
				$tmp = preg_split("/" .substr($data, 2, 1) ."/", $data);
			}
			$day = $tmp[0];
			$month = $tmp[1];
			$year = $tmp[2];
			
			break;
		case "MM/DD/YYYY":
		case "MM-DD-YYYY":
		case "MM.DD.YYYY":																				 
		case "MM/DD/YY":
		case "MM-DD-YY":
		case "MM.DD.YY":
		case "MM/DD/YYYY hh:mm:ss":
		case "MM-DD-YYYY hh:mm:ss":
		case "MM.DD.YYYY hh:mm:ss":
		case "MM/DD/YY hh:mm:ss":
		case "MM-DD-YY hh:mm:ss":
		case "MM.DD.YY hh:mm:ss":
			if(substr($data, 2, 1) == "/" || substr($data, 2, 1) == "."){
				$tmp = preg_split("/\\" .substr($data, 2, 1) ."/", $data);
			}else{
				$tmp = preg_split("/" .substr($data, 2, 1) ."/", $data);
			}
			$day = $tmp[1];
			$month = $tmp[0];
			$year = $tmp[2];
			
			break;
		case "YYYYMMDD":
			$day = substr($data, 6, 2);
			$month = substr($data, 4, 2);
			$year = substr($data, 0, 4);
			break;
		case "YYMMDD":
			$day = substr($data, 4, 2);
			$month = substr($data, 2, 2);
			$year = substr($data, 0, 2);
			break;
		}
		
		if(strlen($year) == 2){
			$year = "20" .$year;
		}		
		
		$d = mktime(intval($hours), intval($minutes), intval($seconds), intval($month), intval($day), intval($year));
                $mysqldate = date( 'Y-m-d H:i:s', $d );
		return $mysqldate;
	}
	
	function GetDecPoint(){
		$tmp = number_format(0.1,1);
		return substr($tmp, 1, 1);
	}
	
	function GetThousandSep(){
		$tmp = number_format(1000,0);
		return substr($tmp, 1, 1);
	}
	
	function FormatAsNumber($strValue, $format){	
		if(strtolower(substr($format, 0, 2)) == "ni"){
			return intval($strValue);
		}else{
			$thouand = substr($format, 1, 1);
			$decimal = substr($format, 2, 1);		
			if($thouand == "."){
				$thouand = "\\" .$thouand;
			}
			if($decimal == "."){
				$decimal = "\\" .$decimal;
			}
			$strValue = preg_replace("/" .$thouand ."/", "", $strValue);
			$strValue = preg_replace("/" .$decimal ."/", $this->GetDecPoint(), $strValue);			
			$val = floatval($strValue);
			return $val;
		}
	}
	
	function InsertProgressJs(){
		$vbcrlf = "\n";
		
		$retStr = "";
		$retStr .= "<script language=\"javascript\">" .$vbcrlf;
		$retStr .= "var GoUpload" .$vbcrlf;
		$retStr .= "var isMAC = (navigator.userAgent.toLowerCase().indexOf(\"mac\") != -1);" .$vbcrlf;
		$retStr .= "var isIE = document.all;" .$vbcrlf;
		$retStr .= "var isNS6 = (!document.all && document.getElementById ? true : false);" .$vbcrlf;
		$retStr .= "var isNS7 = (navigator.userAgent.toLowerCase().indexOf(\"netscape/7\") != -1);" .$vbcrlf;
		$retStr .= "var dwz_SendForm = false" .$vbcrlf;

		$retStr .= "function dwzImportProgressBar(){	" .$vbcrlf;
		$retStr .= "	if(dwz_SendForm){" .$vbcrlf;
		$retStr .= "		return true" .$vbcrlf;
		$retStr .= "	}	" .$vbcrlf;		
		if( $this->progressBar == "mac" ){ 
			$retStr .= "	dimW = 350" .$vbcrlf;
			$retStr .= "	dimH = 100" .$vbcrlf;
		}else{
			$retStr .= "	dimW = 350" .$vbcrlf;
			$retStr .= "	dimH = 210" .$vbcrlf;
		}		
		$retStr .= "	PosL = (screen.width-dimW)/2" .$vbcrlf;
		$retStr .= "	PosT = (screen.height-dimH)/2" .$vbcrlf;
				
		$retStr .= "	progressURL = '" .$this->progressPagePath ."ProgressBar/ProgressBar.php?ProgressPage=" .$this->progressBar ."&progress_key=" .$this->progress_key ."'" .$vbcrlf;
		$retStr .= "	mailWin = window.open(progressURL,'progressImportBarPage','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width='+dimW+',height='+dimH+',top='+PosT+',left='+PosL)" .$vbcrlf;
		$retStr .= "	dwz_SendForm = true" .$vbcrlf;
		$retStr .= "	setTimeout('dwzSendForm()',500)" .$vbcrlf;
		$retStr .= "	return false" .$vbcrlf;
		$retStr .= "}" .$vbcrlf;
		$retStr .= "function dwzSendForm(){" .$vbcrlf;
		$retStr .= "	var el = document.getElementsByTagName('FORM')" .$vbcrlf;
		$retStr .= "	el[0].submit()" .$vbcrlf;
		$retStr .= "}" .$vbcrlf;
		$retStr .= "</scr" ."ipt>" .$vbcrlf;
		
		echo $retStr;
		
	}
	
	function DeleteProgressFile(){
		$filename = GetTempFolder() .$this->progress_key .".xml";
		if(file_exists($filename)){
			unlink($filename);
		}
	}
	
	function WriteProgressBarInfo($stato, $n_Line, $t_Line, $startTime, $oper){
		//$linea = $stato ."," .$n_Line ."," .$t_Line ."," .$startTime ."," .$oper;
		
		$xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
		$xml .= "<root>";
		$xml .= "<total>" .$t_Line ."</total>";
		$xml .= "<current>" .$n_Line ."</current>";
		$xml .= "<work>" .$oper ."</work>";
		$xml .= "<done>" .$stato ."</done>";
		$xml .= "<start_time>" .$startTime ."</start_time>";
		$xml .= "</root>";
		
		$filename = GetTempFolder() .$this->progress_key .".xml";
		
		//if (is_writable($filename)) {
			if (!$handle = fopen($filename, 'w')) {
				 exit();
			}		
			if (fwrite($handle, $xml) === FALSE) {
				exit();
			}
			fclose($handle);		
		//}
		
		//$_SESSION['dwzCsvImport'] = $linea;		
	}
	
	function Debug(){
		
		ob_clean();
		
		echo "<table border=1 cellspacing=2 >";
		
		echo "<td>";
		echo "Row index";
		echo "</td>";
		
		echo "<td>";
		echo "Xml Unique Key";
		echo "</td>";
		
		for($i=0; $i<count($this->itemReference); $i++){
			$tmp = preg_split("/\|_\|/", $this->itemReference[$i]);
			$name = $tmp[0];
			echo "<td>";
			echo $name;
			echo "</td>";
		}
		$aaa = $this->totalLineNumber;
		for($i=0; $i<$this->totalLineNumber; $i++){
			echo "<tr>";
			
			echo "<td>";
			echo $i;
			echo "</td>";
				
			echo "<td>";
			echo $this->GetXmlData($i, $this->xmlUniqueKey, "xml");
			echo "</td>";
			
			for($ii=0; $ii<count($this->itemReference); $ii++){
				$tmp = preg_split("/\|_\|/", $this->itemReference[$ii]);
				$name = $tmp[0];
				echo "<td>";
				echo $this->GetXmlData($i, $this->itemReference[$ii], "") ."&nbsp;";
				echo "</td>";
			}
			echo "</tr>";
		}		
		echo "</table>";
		exit();		
	}
	
}

?>
