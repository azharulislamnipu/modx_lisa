<footer class="width padding">

        <!-- 3 text content blocks -->
        <div class="row grid-12">
            <section class="span-4"><div class="inner">
                <h2>Contact Us</h2>
                <p>
                    <strong>MAIL</strong><br>
                    260 Peachtree Street, NW, Suite 1604<br>Atlanta, Georgia 30303
                </p>
                <p>
                    <strong>MAIL</strong><br>
                    (404) 529-9919
                </p>
                <p>
                    <strong>EMAIL</strong><br>
                    <a href="mailto:hello@sigmapiphi.org">hello@sigmapiphi.org</a>
                </p>
            </div></section>
            <section class="span-4"><div class="inner">
                <h2>Navigation</h2>
                <nav role="navigation">
                    <div class="span-6">
                        <ul class="lines">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Boulé Foundation</a></li>
                            <li><a href="#">Boulé Jorunal</a></li>
                        </ul>
                    </div>
                    <div class="span-6 float-right">
                        <ul class="lines">
                            <li><a href="#">Login</a></li>
                            <li><a href="#">Search</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Help &amp; Support</a></li>
                        </ul>
                    </div>
                </nav>
            </div></section>
            <section class="span-4"><div class="inner">
                <h2>The Fraternity</h2>
                <p>Brief mission statement or description of Sigma Pi Phi with a link where people can <a href="#">read more</a> about the fraternity.</p>
                <p class="align-center margin-top-large">
                    <img src="assets/images/footer-sphinx.png" alt="" >
q
                </p>
            </div></section>
        </div>

        <p class="copyright">Copyright &copy; 2015 Sigma Pi Phi Fraternity.  All rights reserved</p>

    </footer>

    <!--[if gte IE 9 | !IE ]><!--><script src="<?php echo get_template_directory_uri();?>/assets/js/vendor/jquery-2.1.3.min.js"></script><!--<![endif]-->
    <!--[if lt IE 9]><script src="assets/js/vendor/jquery-1.11.2.min.js"></script><![endif]-->
    <script src="<?php echo get_template_directory_uri();?>/assets/js/main.js"></script>

</body>
</html>