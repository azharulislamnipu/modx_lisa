<?php
/**
 * Widget template. This template can be overriden using the "sp_template_image-widget_widget.php" filter.
 * See the readme.txt file for more info.
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

echo '<section class="span-4 align-center roundwidget">
	<div class="inner">
		<div class="rounded">';

echo $before_widget;

echo $this->get_image_html( $instance, true );

if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }

if ( !empty( $description ) ) {
	echo '<div class="'.$this->widget_options['classname'].'-description" >';
	echo wpautop( $description );
	echo "</div>";
}
echo $after_widget;
echo '</div>
	</div>
		</section>';
?>