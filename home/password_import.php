<?php
/**
 * password_import.php
 * User: Marco Cotrufo <info@devncode.it>
 * Date: 23/02/14
 *
 * @package modx
 */

require_once('manager/includes/config.inc.php');
require_once('manager/includes/document.parser.class.inc.php');

$modx = new DocumentParser();
$modx->loadExtension('DBAPI');
$modx->loadExtension('ManagerAPI');

define('INTERNAL_ID', 0);
define('USERNAME', 1);
define('PASSWORD', 2);
define('ROLE', 3);

/**
 * Parse the csv file and returns the content as array
 * @param string $file path to the file
 * @throws Exception
 * @return array
 */
function parseCSV($file)
{
    $result = array();

    if (!file_exists($file)) {
        throw new Exception("CSV doesn't exists");
    }

    $firstLine = true;
    $lines = explode("\n", str_replace("\r", "", file_get_contents($file)));

    foreach ($lines as $line) {
        if ($firstLine) {
            $firstLine = false;
            continue;
        }

        if (!strpos($line, ',')) {
            continue;
        }

        $result[] = explode(',', $line);
    }

    return $result;
}

function updateUsers($modx, $data = array())
{
    $tbl_manager_users = $modx->getFullTableName('web_users');

    foreach ($data as $row) {
        $users = $modx->db->select('id', $tbl_manager_users, 'username="' . $row[USERNAME] . '"');

        while (($user = $modx->db->getRow($users)) !== false) {
            $query = sprintf("UPDATE %s SET password = MD5('%s') WHERE id = %d", $tbl_manager_users, $row[PASSWORD], $user['id']);
            $modx->db->query($query);
        }
    }
}


if ($argc < 2) {
    echo "Use $argv[0] <path to the csv file>";
    return false;
}

$users = parseCSV($argv[1]);
updateUsers($modx, $users);