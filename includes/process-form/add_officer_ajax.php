<?php
error_reporting(0);
include_once "../../home/modxapi.php";
$modx = new MODxAPI();
$modx->connect();
$modx->startSession();

/**
 * Local
 */
$pos = strrpos(__DIR__, "htdocs");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'htdocs/';
}


/**
 * Server
 */

/*$pos = strrpos(__DIR__, "apache");
if($pos !== false){
    $new_path = substr(__DIR__, 0, $pos) . 'apache/';
}*/

require $new_path . "includes/functions.php";

if(!is_ajax()){
	die('Not permitted');
}

$data = $response = array();
parse_str($_POST['data'], $data);
array_walk($data, 'test_in_array');
$response['type'] = 'success';
$response['html'] = '';

$date = date_create($data['JOINDATE']);
if (!$date) {
	$response['type'] = 'error';
	$response['html'] .= '<p>Wrong Start Date.</p>';
}
$data['JOINDATE'] = date_format($date,"Y-m-d");

$post_data = array();
$columns = array('REGIONCD', 'TERMINATIONDATE', 'CUSTOMERCD', 'CHAPTERCD', 'CPOSITION', 'JOINDATE', 'COMMITTEESTATUSSTT', 'UPDATEUSERCD', 'UPDATETMS');
$validate_columns = array('REGIONCD', 'CUSTOMERCD', 'CHAPTERCD', 'CPOSITION', 'UPDATEUSERCD');

foreach ($validate_columns as $validate_column){
	if(!array_key_exists($validate_column, $data) || empty($data[$validate_column])){
		$response['type'] = 'error';
		$response['html'] .= '<p>Wrong '. ucfirst(strtolower($validate_column)) .'.</p>';
	}
}

if($response['type'] !== 'error'){
	foreach ($columns as $column){
		$post_data[$column] = isset($data[$column]) ? $data[$column] : null;
	}
//	$post_data['UPDATEUSERCD'] = $_SESSION['BOULENAME'];
	$post_data['COMMITTEESTATUSSTT'] = 'Active';
	$post_data['UPDATETMS'] = date('Y-m-d H:i:s');


	$db = get_db();
	$officer_id = $db->insert ('spp_officers', $post_data);
	if ($officer_id){
		$response['type'] = 'success';
		$cc = $_SESSION['webEmail'];
		$bcc = 'katrinaspp@bellsouth.net, lisa.jeter@gmail.com';
		$subject = 'NEW MEMBER BOULE OFFICER';
		$content = "<p>{$_SESSION['BOULENAME']} has a new {$data['CPOSITION']}:\n\n<p>CUSTOMERCD: {$data['CUSTOMERCD']}</p>\n\nSubmitted by Archon {$_SESSION['webFullname']}</p>\n";
		send_email($subject, $content, $cc, $bcc);
	} else{
		$response['type'] = 'error';
		$response['html'] .= '<p>Error occurred! Please try again.</p>';
	}
}

die(json_encode($response));
