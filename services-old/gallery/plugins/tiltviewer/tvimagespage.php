<?php
 /**
  * Part of Airtight Interactive gallery management package.
  *
  * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
  * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
  *
  * @package svManager
  * @author Jack Hardie {@link http://www.jhardie.com}
  * @version 1.6.3 build 091231
  * @copyright Copyright (c) 2007 - 2009, Airtight Interactive
  */
 
 /**
  * Images page
  *
  * @package svManager
  */
  class TvImagesPage extends Page
  {
   /**
    * @var string overrides some of html in parent
    */
    var $customHeadHtml = '
    <script src="scripts/mootools-core.js" type="text/javascript"></script>
    <script type="text/javascript" src="scripts/images.js"></script>
    <script src="scripts/mooformcheck.js" type="text/javascript"></script>
    ';
    
   /**
    * constructs TvImagesPage class
    * 
    */
    function TvImagesPage()
    {
      parent::Page('svManager &ndash; images', 'images', 'tiltviewer');
    }

   /**
    * get html for images page
    * Note that some class names are significant for manager.js
    *
    * @access public
    * @return string html
    * @param string gallery reference number
    * @param array image data
    */
    function getImagesHtml(&$gallery)
    {
      $imageObjects = $gallery->getImageObjects();
      $galleryRef = $gallery->getGalleryRef();
      $html = '';
      $html .= '<form class="public" action="'.$_SERVER['PHP_SELF'].'?edit='.$galleryRef.'" method="post" onsubmit="return allowImageDelete(this)" >
  <table cellspacing="0" class="imagelist" id="imagelisttable">
      <tr><th class="ilfilename">Image</th><th colspan="2" class="ilselectdelete">Delete</th><th class="ilshowflip"><img src="img/flipicon.gif" width="21" height="19" alt="Show flip button" title="Show flip button" /></th><th class="ilcaption"><span class="caption">Title</span> / <span class="tvdescription">Description</span> / <span class="tvurl">URL</span></th><th class="ilselect">Select</th></tr>';
      $i = 0;
      foreach ($imageObjects as $key => $imageObject)
      {
        $imageUrl = $gallery->getUrlRelSvm($imageObject->getImagePathRelGallery());
        $fileName = $imageObject->getImageFileName();
        if (file_exists($gallery->getThumbDirPathRelSvm().$fileName))
        {
        	$src = $gallery->getThumbDirUrlRelSvm().$fileName;
        }
        else
        {
        	$src = rtrim(IMAGE_SRC, '\\/').'/nothumb.gif';
        }
        $fileNameChopped = (strlen($fileName) < 20) ? $fileName : substr($fileName, 0, 16).'…';
        $fileNameChopped = htmlspecialchars($fileNameChopped, ENT_QUOTES, 'UTF-8');
        $imageTitle = $imageObject->getImageTitle();
        $imageDescription = $imageObject->getImageDescription();
        $linkUrl = $imageObject->getLinkUrl();
        $imageSize = $imageObject->getImageSizeArray();
        $showFlipButtonChecked = $imageObject->getShowFlipButton() ? 'checked="checked"' : '';
        if ($imageSize === false) $sizeString = '<br />unknown size';
        else $sizeString = '<br />('.$imageSize[0].' x '.$imageSize[1].')';
        $chkBoxId = 'cap'.$i;
        $html .= '<tr>
          <td rowspan="3" class="ilfilename">'.$fileNameChopped.$sizeString.'</td>
          <td rowspan="3" class="ilselectdelete"><input type="checkbox" name="del['.$i.']" id="dbox'.$i.'" class="selectdelete" /></td>
          <td rowspan="3" class="ilthumbnail"><a href="'.$imageUrl.'" rel="external"><img src="'.$src.'" width="'.THUMB_DISPLAY_SIZE.'" height="'.THUMB_DISPLAY_SIZE.'" alt="" /></a></td>
          <td rowspan="3" class="ilshowflip"><input type="checkbox" name="checkedShowFlip['.$i.']" id="showflipbox'.$i.'" class="selectshowflip" '.$showFlipButtonChecked.' /></td>
          <td class="ilcaption"><input name="cap['.$i.']" type="text" class="caption" value="'.htmlspecialchars($imageTitle, ENT_QUOTES, 'UTF-8').'" id="cap'.$i.'" /></td>
          <td class="ilselect"><input type="checkbox" name="checkedBoxes['.$i.']" id="cbox'.$i.'" class="selectcaption" /></td>
        </tr>
        <tr>
          <td class="ilcaption"><input name="des['.$i.']" type="text" class="tvdescription" value="'.htmlspecialchars($imageDescription, ENT_QUOTES, 'UTF-8').'" id="des'.$i.'" /></td>
          <td class="ilselect"><input type="checkbox" name="checkedDescriptions['.$i.']" id="desbox'.$i.'" class="selectdescription" /></td>
        </tr>
        <tr>
          <td class="ilcaption" style="padding-bottom: 10px;"><input name="url['.$i.']" type="text" class="tvurl" value="'.htmlspecialchars($linkUrl, ENT_QUOTES, 'UTF-8').'" id="url'.$i.'" /></td>
          <td class="ilselect"><input type="checkbox" name="checkedUrls['.$i.']" id="urlbox'.$i.'" class="selecturl" /></td>
        </tr>';
        $i++;
      }
      $html .= '</table>
      <table cellspacing="0" id="captioncontrols" class="imagelist" >
        <tr class="firstrow">
          <td class="ilfilename" >Select all</td>
          <td class="ilselectalldelete"><input type="checkbox" name="selectalldelete" id="selectalldelete" /></td>
          <td class="ilthumbnail">&nbsp;</td>
          <td class="ilshowflip"><input type="checkbox" name="selectallshowflip" id="selectallshowflip" /></td>
          <td class="ilcaption"><span class="caption">Select all titles</span></td>
          <td class="ilselect"><input type="checkbox" name="selectall" id="selectall" /></td>
        </tr>
        <tr>
          <td class="ilfilename" >&nbsp;</td>
          <td class="ilselectalldelete">&nbsp;</td>
          <td class="ilthumbnail">&nbsp;</td>
          <td class="ilshowflip">&nbsp;</td>
          <td class="ilcaption"><span class="tvdescription">Select all descriptions</span></td>
          <td class="ilselect"><input type="checkbox" name="selectalldescriptions" id="selectalldescriptions" /></td>
        </tr>
        <tr>
          <td class="ilfilename" >&nbsp;</td>
          <td class="ilselectalldelete">&nbsp;</td>
          <td class="ilthumbnail">&nbsp;</td>
          <td class="ilshowflip">&nbsp;</td>
          <td class="ilcaption"><span class="tvurl">Select all URLs</span></td>
          <td class="ilselect"><input type="checkbox" name="selectallurls" id="selectallurls" /></td>
        </tr>
        <tr>
          <td class="ilfilename" >&nbsp;</td>
          <td class="ilselectalldelete">&nbsp;</td>
          <td class="ilthumbnail">&nbsp;</td>
          <td class="ilshowflip">&nbsp;</td>
          <td class="ilcaption">Set selected to:</td>
          <td class="ilselect">&nbsp;</td>
        </tr>
        <tr class="lastrow">
          <td class="ilfilename" >&nbsp;</td>
          <td class="ilselectalldelete">&nbsp;</td>
          <td class="ilthumbnail">&nbsp;</td>
          <td class="ilshowflip">&nbsp;</td>
          <td class="ilcaption"><input id="copytext" name="copyText" type="text" class="newcaption" value="" /></td>
          <td class="ilselect" style="padding: 0"><input style="width: 40px; " class="formbutton" id="copy" type="button" value="Set" name="copy" /></td>
        </tr>
      </table>
      <div id="submitinputs">
        <input type="hidden" name="imagessubmitted" value="true" /><input class="formbutton" type="submit" value="Update" name="submit" />
      </div>
    </form>';
      return $html;
    }
  }
?>
