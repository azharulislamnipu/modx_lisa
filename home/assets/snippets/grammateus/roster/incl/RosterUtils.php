<?php

/*
 * Helper functions for processing a Roster
 */

require_once 'ArchonBean.php';
require_once 'ArchonDuesBean.php';

class RosterUtils {

    const REGIONCD_PACIFIC = 'PA';
    const DUES_REGION_PACIFIC = 470;
    const DUES_REGION = 480;
    const DUES_REINSTATE_PACIFIC = 320;
    const DUES_REINSTATE = 330;

	// Dues amounts for Pacific and all other regions.  Array index is year.
	private static $dues_pacific = array(
		2012 => 320,
		2013 => 320,
		2014 => 470
	);
	private static $dues_region = array(
		2012 => 320,
		2013 => 330,
		2014 => 480
	);

	// Reinstated amounts for Pacific and all other regions.  Array index is year.
	private static $reinstate_pacific = array(
		2012 => 320,
		2013 => 320,
		2014 => 320
	);

	private static $reinstate_region = array(
		2012 => 320,
		2013 => 320,
		2014 => 330
	);

    /**
     * Returns the total dues amount owed
     * @param String $regionCd Archon's region code
     * @param String $statusCurrent Archon's current status
     * @param String $statusRequested Archon's requested status
	 * @param int $year Year the dues is being requested for
     * @return Number Total dues amount owed
     */
    public static function getDuesTotalAmount($regionCd, $statusCurrent, $statusRequested, $year){
        return RosterUtils::getDuesAmount($regionCd, $statusCurrent, $statusRequested, $year) + RosterUtils::getReinstatementAmount($regionCd, $statusCurrent, $statusRequested, $year);
    }

    /**
     * Returns the dues amount owed
     * @param String $regionCd Archon's region code
     * @param String $statusCurrent Archon's current status
     * @param String $statusRequested Archon's requested status
	 * @param int $year Year the dues is being requested for
     * @return Number Dues amount owed
     */
    public static function getDuesAmount($regionCd, $statusCurrent, $statusRequested, $year){
            $amount = 0;
            if(Status::Active === $statusRequested){
                    switch($statusCurrent){
                            case Status::Inactive:
                            case Status::InvoluntaryInactive:
                            case Status::VoluntaryInactive:
                            case Status::Active:
                            case Status::Delinquent:
									if ( $regionCd === RosterUtils::REGIONCD_PACIFIC ) {
										$amount = isset( self::$dues_pacific[ $year ] ) ? self::$dues_pacific[ $year ] : 0;
									} else {
										$amount = isset( self::$dues_region[ $year ] ) ? self::$dues_region[ $year ] : 0;
									}
                    }
            }
            return $amount;
    }

    /**
     * Returns the reinstatement amount owed
     * @param String $regionCd Archon's region code
     * @param String $statusCurrent Archon's current status
     * @param String $statusRequested Archon's requested status
	 * @param int $year Year the dues is being requested for
     * @return Number Reinstatement amount owed
     */
    public static function getReinstatementAmount($regionCd, $statusCurrent, $statusRequested, $year){
            $amount = 0;
            if(Status::Active === $statusRequested) {
                    switch($statusCurrent){
                            case Status::Inactive:
                            case Status::InvoluntaryInactive:
                            case Status::VoluntaryInactive:
									if ( $regionCd === RosterUtils::REGIONCD_PACIFIC ) {
										$amount = isset( self::$reinstate_pacific[ $year ] ) ? self::$reinstate_pacific[ $year ] : 0;
									} else {
										$amount = isset( self::$reinstate_region[ $year ] ) ? self::$reinstate_region[ $year ] : 0;
									}
                    }

            }
            return $amount;
    }

    /**
     * Returns the dues year
     * @return Number 4 digit year
     */
    public static function getDuesYear(){
        date_default_timezone_set('EST');
		return intval(date('Y'));
    }

    /**
     * Returns true if current date is less than or equal to cutoff date
     * @return boolean
     */
    public static function isWithinCutoff ( $cutoff ) {
        date_default_timezone_set('EST');
		return isset($cutoff) ? $cutoff >= date('Y-m-d') : true;
    }

    /**
     * Given an array of requested dues, creates the rows required for the Dues Summary table
     * @param array $duesSummary
     * @return array $rows
     */
    public static function getDuesSummaryRows($duesSummary){
        $rows = array();
        $total_count = 0;
        $total_dues = 0;

        foreach(Status::get_all_status() as $status){
            if(isset($duesSummary[$status])){
               $rows[$status] = array(
                    'requested' => $duesSummary[$status]['status_count'],
                    'dues' => $duesSummary[$status]['dues_total']
               );
               $total_count += $duesSummary[$status]['status_count'];
               $total_dues += $duesSummary[$status]['dues_total'];
            }
        }
        $rows['Total'] = array(
            'requested' => $total_count,
            'dues' => $total_dues
        );
        return $rows;
    }
}

/**
 * Constant States
 */
final class Status {

    const Active = 'Active';
    const Deceased = 'Deceased';
    const Delinquent = 'Delinquent';
    const Dropped = 'Dropped';
    const Emeritus = 'Emeritus';
    const Inductee = 'Inductee';
    const Inactive = 'Inactive';
    const InvoluntaryInactive = 'Involuntary Inactive';
    const VoluntaryInactive = 'Voluntary Inactive';

    public static function get_all_status(){
        return array(
            Status::Active,
            Status::Deceased,
            Status::Delinquent,
            Status::Dropped,
            Status::Emeritus,
            Status::Inductee,
            Status::Inactive,
            Status::InvoluntaryInactive,
            Status::VoluntaryInactive
        );
    }

    /**
     * Usually the status name === the label, but sometimes people just want more.
     * @param type $status
     * @return string
     */
    public static function get_status_label($status){
        $label = $status;
        if($status === Status::Dropped) {
            $label = 'Dropped / Withdrawn / Resigned';
        }
        return $label;
    }

    private function __construct(){}

};










/**
 * Constant officer types
 */
final class Officers {

    const SireArchon = 'Sire Archon';
    const SireArchonElect = 'Sire Archon-Elect';
    const Grammateus = 'Grammateus';
    const Grapter = 'Grapter';
    const Thesauristes = 'Thesauristes';
    const Rhetoricos = 'Rhetoricos';
    const Agogos = 'Agogos';

    public static function get_all_officers(){
        return array(
            Officers::SireArchon,
            Officers::SireArchonElect,
            Officers::Grammateus,
            Officers::Grapter,
            Officers::Thesauristes,
            Officers::Rhetoricos,
            Officers::Agogos
        );
    }

    private function __construct(){}

};

?>
