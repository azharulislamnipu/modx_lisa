<?php
$data = $response = array();
parse_str($_POST['data'], $data);
array_walk($data, 'test_in_array');
$response['type'] = 'success';

$response['html'] = '';

if (filter_var($data['OCCUPATIONID'], FILTER_VALIDATE_INT) === false) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Profession Name is not valid</p>';
}
if (filter_var($data['TITLEID'], FILTER_VALIDATE_INT) === false) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Profession Title is not valid</p>';
}
if (empty($data['COMPANYNAME'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Company Name is not valid</p>';
}
if (empty($data['COMPANYCITY'])) {
    $response['type'] = 'error';
    $response['html'] .= '<p>Company Name is not valid</p>';
}


$date = DateTime::createFromFormat('m/Y', $data['COMPANYJOBSTART']);
if($date){
    $data['COMPANYJOBSTART'] = date_format($date,"Y-m-d");
}
//$date = date_create('1/'.$data['COMPANYJOBSTART']);

//$date = date_create($data['COMPANYJOBEND']);
$date = DateTime::createFromFormat('m/Y', $data['COMPANYJOBEND']);
if($date){
    $data['COMPANYJOBEND'] = date_format($date,"Y-m-d");
}

//die($data);
if($response['type'] !== 'error'){
    $db = get_db();
    $ndata = array (
        'OCCUPATIONID'      => $data['OCCUPATIONID'],
        'TITLEID'           => $data['TITLEID'],
        'COMPANYNAME'       => $data['COMPANYNAME'],
        'COMPANYTITLE'      => $data['COMPANYTITLE'],
        'COMPANYCITY'       => $data['COMPANYCITY'],
        'COMPANYSTCD'       => $data['COMPANYSTCD'],
        'DESCRIPTION'       => $data['DESCRIPTION'],
        'COMPANYJOBSTART'   => $data['COMPANYJOBSTART'],
        'COMPANYJOBEND'     => $data['COMPANYJOBEND'],
    );

//    print_r($extra_val);
    $db_success = false;

    if(empty($data['PROFID'])){
        $ndata['CUSTOMERID'] = $user_id;
        $ndata['CREATEDBY'] = $updated_by;
        $ndata['CREATEDATE'] = date("Y-m-d H:i:s");
        $db_success = $db->insert ('spp_prof', $ndata);
        $response['action'] = 'INSERT';
        $response['PROFID'] = $db_success;
    } else {
        $ndata['UPDATEDBY'] = $updated_by;
        $ndata['LASTUPDATE'] = date("Y-m-d H:i:s");
        $db->where ('CUSTOMERID', $user_id);
        $db->where ('PROFID', $data['PROFID']);
        $db_success = $db->update ('spp_prof', $ndata);
        $response['action'] = 'UPDATE';
        $response['PROFID'] = $data['PROFID'];
    }

    if ($db_success){
        $response['type'] = 'success';
        $response['TITLE'] = "{$data['COMPANYTITLE']}, {$ndata['COMPANYNAME']}";
        $response['html'] = "
<strong>{$data['COMPANYTITLE']}</strong>, {$ndata['COMPANYNAME']}, {$ndata['COMPANYCITY']}, {$ndata['COMPANYSTCD']}
";
        SPP_ARCHON_DB::update_lastupdate_date($user_id);
    } else{
        $response['type'] = 'error';
//        $response['html'] = $db->getLastError();
        $response['html'] = '<p>Error occurred! Please try again.</p>';
    }
}

die(json_encode($response));