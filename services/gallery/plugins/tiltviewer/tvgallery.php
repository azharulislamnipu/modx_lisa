<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Models a TiltViewer gallery
 *
 * @package svManager
 */
class TvGallery extends Gallery
{
 /**
  * @var array gallery settings to be stored in preferences file
  * TiltViewer stores all its preferences here so this just a copy of the default preferences file
  */
  var $storedInPreferences = array();
  
 /**
  * @var array gallery preferences from preferences file
  * 'sortOrder' string image sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  * 'galleryTitle' string gallery title including permitted html
  * 'htmlTitle' string gallery title excluding all html
  * 'useFlickr' boolean Flickr gallery
  * 'flickrUserId' string Flickr user ID
  * 'flickrTags' string Flickr image tag
  * 'flickrTagMode string 'any' or 'all'
  * 'flickrShowTakenByText' boolean, show taken by text for flickr images
  * 'useReloadButton' boolean, true, use reload button under images
  * 'showFlipButton' boolean, true, show flip button on all images
  * 'showLinkButton' boolean, not implemented.
  * 'columns' integer number of thumbnail columns, minimum 0, no upper limit
  * 'rows' integer number of thumbnail rows, minimum 0, no upper limit
  * 'linkLabel' string, text for flipside link 
  * 'frameColor' string, 0xFFFFFF, colour of gallery frame 
  * 'backColor' string, 0xFFFF00, background colour
  * 'bkgndInnerColor' string, 0x333333, background gradient center colour
  * 'bkgndOuterColor' string, 0x000000, background gradient edge color
  * 'frameColor' string, 0xFFFFFF, colour of gallery frame default
  * 'langGoFull' string, "Go Fullscreen", language option
  * 'langExitFull' string, "Exit Fullscreen", language option
  * 'langAbout', string, "About", language option
  * 'maxJPGSize' integer, 500, maximum image size in pixels, minimum 0, no upper limit
  
  * Pro Options
  * 'bkgndTransparent' boolean, false, hide radial gradient background
  * 'showFullscreenOption' boolean, true, show the 'Go Fullscreen' right-click option
  * 'frameWidth' integer, 40, frame width in pixels, minimum -5, no upper limit
  * 'zoomedInDistance' integer, 1400, camera distance zoomed-in
  * 'zoomedOutDistance' integer, 7500, camera distance zoomed-out
  * 'fontName' string, Arial, font for flipside text
  * 'titleFontSize' integer, 90, font size for flipside text
  * 'descriptionFontSize' integer, 32, font size for description text
  * 'linkFontSize' integer, 41, font size for link text
  * 'linkTarget' string, _blank, window or frame to open links
  * 'navButtonColor' string, 0xFFFFFF, color for reload or next/back buttons
  * 'flipButtonColor'string, 0xFFFFFF, color for flip buttons
  * 'textColor' string, 0x000000, colour of flipside text
  * 'linkTextColor string, 0xFFFFFF, color of flipside link text
  * 'linkBkgndColor string, 0x000000, color of flipside link text background
  * 'enableSounds' boolean, true, enable click sounds
  * 'tiltAmountin' integer, 75, max tilt (degrees) when zoomed-in
  * 'tiltAmountOut' integer, 120, max tilt (degrees) when zoomed-out
  
  */
  var $preferences = array();
  
 /**
  * @var string name of image class
  */
  var $imageClass = 'TvImage';
  
 /**
  * @var array of image objects
  */
  var $imageObjects = array();
  
 /**
  * @var string default path to master gallery
  */
  var $masterPath = TV_MASTER_PATH;
  
 /**
  * @var string to prepend to paths
  */
  var $pathScheme = '';
  
 /**
  * @var integer count of files in master gallery
  */
  var $masterFileCount = TV_MASTER_FILECOUNT;
  
 /**
  * @var string default image path
  */
  var $imageDirPathRelGallery = TV_DEFAULT_IMAGE_PATH;
  
 /**
  * @var integer thumb width
  */ 
  var $thumbWidth = TV_THUMB_WIDTH;

 /**
  * @var integer thumb height
  */
  var $thumbHeight = TV_THUMB_HEIGHT;
  
 /**
  * @var integer thumb quality
  */
  var $thumbQuality = TV_THUMB_QUALITY;
  
 /**
  * @var string default thumb path
  */
  var $thumbDirPathRelGallery = TV_DEFAULT_THUMB_PATH;
  
 /**
  * @var string name of viewer swf file
  */
  var $viewerSwf = 'TiltViewer.swf';
  
  /**
  * @var string acceptable html tags in captions
  */
  var $okCaptionTags = '<font><b><i><br><br />';
  
  /**
   * Constructs TvGallery
   *
   * Attempts to read local copies of preferences.txt and gallery.xml
   * Parses gallery.xml file into class settings and imageObjects array.
   * Note that the parsing translates html entities (e.g. in the title) back to html
   * If local file has been corrupted then default values are used.
   * 
   * @access public
   * @param string gallery reference (permanent reference, not the  galleriesData index)
   * @param string gallery title, empty string gives default
   * @param string gallery path
   * @param boolean new gallery
   *
   */  
  function TvGallery($ref, $path, $newGallery = false)
  {
    parent::Gallery();
    $this->preferences['sortOrder'] = TV_SORT_ORDER;
    $this->preferences['galleryTitle'] = TV_GALLERY_TITLE;
    $this->preferences['htmlTitle'] = HTML_TITLE;
    $this->preferences['useFlickr'] = TV_USE_FLICKR;
    $this->preferences['flickrUserId'] = TV_FLICKR_USER_ID;
    $this->preferences['flickrTags'] = TV_FLICKR_TAGS;
    $this->preferences['flickrTagMode'] = TV_FLICKR_TAG_MODE;
    $this->preferences['flickrShowTakenByText'] = TV_FLICKR_SHOW_TAKEN_BY_TEXT;
    $this->preferences['useReloadButton'] = TV_USE_RELOAD_BUTTON;
    $this->preferences['showFlipButton'] = TV_SHOW_FLIP_BUTTON;
    $this->preferences['showLinkButton'] = TV_SHOW_LINK_BUTTON;
    $this->preferences['columns'] = TV_COLUMNS;
    $this->preferences['rows'] = TV_ROWS;
    $this->preferences['linkLabel'] = TV_LINK_LABEL;
    $this->preferences['frameColor'] = TV_FRAME_COLOR;
    $this->preferences['backColor'] = TV_BACK_COLOR;
    $this->preferences['bkgndInnerColor'] = TV_BKGND_INNER_COLOR;
    $this->preferences['bkgndOuterColor'] = TV_BKGND_OUTER_COLOR;
    $this->preferences['frameColor'] = TV_FRAME_COLOR;
    $this->preferences['maxJPGSize'] = TV_MAX_JPG_SIZE;
    $this->preferences['langGoFull'] = LANG_GO_FULL;
    $this->preferences['langExitFull'] = LANG_EXIT_FULL;
    $this->preferences['langAbout'] = LANG_ABOUT;
    $this->preferences['urlType'] = TV_URL_TYPE;
    // Pro Options  
    $this->preferences['bkgndTransparent'] = TV_BKGND_TRANSPARENT;
    $this->preferences['showFullscreenOption'] = TV_SHOW_FULLSCREEN_OPTION;
    $this->preferences['frameWidth'] = TV_FRAME_WIDTH;
    $this->preferences['zoomedInDistance'] = TV_ZOOMED_IN_DISTANCE;
    $this->preferences['zoomedOutDistance'] = TV_ZOOMED_OUT_DISTANCE;
    $this->preferences['fontName'] = TV_FONT_NAME;
    $this->preferences['titleFontSize'] = TV_TITLE_FONT_SIZE;
    $this->preferences['descriptionFontSize'] = TV_DESCRIPTION_FONT_SIZE;
    $this->preferences['linkFontSize'] = TV_LINK_FONT_SIZE;
    $this->preferences['linkTarget'] = TV_LINK_TARGET;
    $this->preferences['navButtonColor'] = TV_NAV_BUTTON_COLOR;
    $this->preferences['flipButtonColor'] = TV_FLIP_BUTTON_COLOR;
    $this->preferences['textColor'] = TV_TEXT_COLOR;
    $this->preferences['linkTextColor'] = TV_LINK_TEXT_COLOR;
    $this->preferences['linkBkgndColor'] = TV_LINK_BKGND_COLOR;
    $this->preferences['enableSounds'] = TV_ENABLE_SOUNDS;
    $this->preferences['tiltAmountIn'] = TV_TILT_AMOUNT_IN;
    $this->preferences['tiltAmountOut'] = TV_TILT_AMOUNT_OUT;
    $this->storedInPreferences = $this->preferences;
    $this->galleryRef = $ref;
    $this->galleryPathRelSvm = $path;
    $this->pathScheme = ($this->pathScheme == '') ? '' : rtrim($this->pathScheme, '\\/').'/';
    $this->imageDirPathRelGallery = rtrim($this->imageDirPathRelGallery, '\\/').DIRECTORY_SEPARATOR;
    $this->thumbDirPathRelGallery = rtrim($this->thumbDirPathRelGallery, '\\/').DIRECTORY_SEPARATOR;
    
    if ($newGallery)
    {
      $this->addGallery($ref);
    }
    if (!file_exists($this->trimSeparator($this->galleryPathRelSvm)))
    {
      // exit
      trigger_error('cannot find gallery folder '.$this->galleryPathRelSvm, E_USER_ERROR);
    }
    $prefs = $this->readPreferences();
    if ($prefs !== false)
    {
      $this->preferences = array_merge($this->preferences, $prefs);
    }
    $this->setPathScheme();
    $xmlStruct = $this->parseXml($this->getXmlPath());
    if ($xmlStruct !== false)
    {
      $this->imageObjects = $this->parseImageTags($xmlStruct);
    }
    $this->preferences['htmlTitle'] = (USE_HTML_TITLE === false) ? strip_tags($this->htmlEntityDecode->decode($this->preferences['galleryTitle'])) : HTML_TITLE;
    $this->saveGallery();
  }
  
 /** 
   * Get stored image data from class
   *
   * @access public
   * @return array
   */
   function getImageObjects()
   {
     return $this->imageObjects;
   }

 /**
  * Get xml path
  *
  * @access private
  * @return string calculated xml path
  */
  function getXmlPath()
  {
    return str_replace('/', DIRECTORY_SEPARATOR, $this->pathParser->fix($this->galleryPathRelSvm.TV_XML_PATH_REL_GALLERY));
  } 
  
 /**
  * Get gallery title
  *
  * @access public
  * @return string
  */
  function getGalleryTitle()
  {
    return $this->preferences['galleryTitle'];
  }
  
 /**
  * Set gallery title
  * @access public
  * @return void
  * @param string
  */
  function setGalleryTitle($title)
  {
    $this->preferences['galleryTitle'] = $title;
    return;
  }
  
 /**
  * Get gallery title stripped of html
  *
  * @access public
  * @return string
  */
  function getGalleryTitleText()
  {
    return strip_tags($this->htmlEntityDecode->decode($this->preferences['galleryTitle']));
  }
  
 /**
  * Get sort order
  *
  * @access public
  * @return string
  */
  function getSortOrder()
  {
    return $this->preferences['sortOrder'];
  }
  
 /**
  * Set sort order in $this->preferences
  *
  * @access public
  * @return void
  * @param string sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  */
  function setSortOrder($sortOrder)
  {
    $this->preferences['sortOrder'] = $sortOrder;
    return;
  }

  /**
   * Set image data
   *
   * @access public
   * @return  boolean true on success
   * @param array image titles
   */
  function setImageCaptions($post)
  {
    $imageTitles = $post['cap'];
    $imageDescriptions = $post['des'];
    $imageLinkUrls = $post['url'];
    $i=0;
    //$okTags = '<font><b><i><br><br />';
    foreach (array_keys($this->imageObjects) as $key)
    {
      $showFlipButton = isset($post['checkedShowFlip'][$i]);
      $this->imageObjects[$key]->setCaption($imageTitles[$key], $imageDescriptions[$key], $imageLinkUrls[$key], $showFlipButton, $i, $this->okCaptionTags);
      $i++;
    }
    return true;
  }

 /**
  * Extract file names and captions from xml structured array
  *
  * Any empty image tags are silently ignored
  * @access private
  * @return array containing filenames and captions
  * @param values array as generated by xml_parse_into_struct
  */
  function parseImageTags($xmlStruct)
  {
    $okTags = '<font><b><u><i><br><br />';
    $vals = $xmlStruct['vals'];
    $imageObjects = array();
    $photoAttributes = array();
    $photoTagOpen = false;
    foreach ($vals as $tagInfo)
    {
      $photoTagOpen = ((strtolower($tagInfo['tag']) == 'photo' && $tagInfo['type'] == 'open') || $photoTagOpen);
      if (strtolower($tagInfo['tag']) == 'photo' && $tagInfo['type'] == 'open')
      {
        if (!isset($tagInfo['attributes'])) continue;
        $photoAttributes = array_change_key_case($tagInfo['attributes'], CASE_LOWER);
        if (!isset($tagInfo['attributes']['imageurl'])) continue;
      }
      if (strtolower($tagInfo['tag']) == 'title')
      {
        if (isset($tagInfo['value']))
        {
          $imageTitle = $this->htmlEntityDecode->decode($tagInfo['value']);
        }
        else $imageTitle = '';
      }
      if (strtolower($tagInfo['tag']) == 'description')
      {
        if(isset($tagInfo['value']))
        {
          $imageDescription = strip_tags($tagInfo['value'], $okTags);
        }
        else
        {
          $imageDescription = '';
        }
      }
      if ($photoTagOpen && strtolower($tagInfo['tag']) == 'photo' && $tagInfo['type'] == 'close')
      {
        {
          $imagePathRelGallery = $this->getPathRelGallery($photoAttributes['imageurl']);
          $fileName = basename($imagePathRelGallery);
          $imageUrl = $this->convertRelToScheme($imagePathRelGallery, $this->galleryPathRelSvm, $this->pathScheme);
          if (isset($photoAttributes['linkurl']))
          {
            $linkUrl = $photoAttributes['linkurl'];
          }
          else
          {
            $linkUrl = '';
          }
          if (isset($photoAttributes['showflipbutton']))
          {
            $showFlipButton = ($photoAttributes['showflipbutton'] == strtolower('true'));
          }
          else
          {
            $showFlipButton = 'default';
          }
          $imageSize = @getimagesize($this->galleryPathRelSvm.$imagePathRelGallery);
          if ($imageSize != false)
          {
            $imageObjects[] = new $this->imageClass($this->galleryPathRelSvm, $imagePathRelGallery, $this->thumbDirPathRelGallery.$fileName, $imageTitle, $imageDescription, $linkUrl, $showFlipButton);
          }
          $photoTagOpen = false;
          $photoAttributes = array();
        }
      }
    }
    return $imageObjects;
  }

 /**
  * Construct xml string and write to file
  *
  * @access private
  * @param string file path
  * @return boolean
  */
  function writeXml($xmlPath)
  {
    $imageObjects = $this->imageObjects;
    $xml = '<?xml version="1.0" encoding="UTF-8" ?>
  <'.TV_XML_SETTINGS_TAG.'>
    <photos>
';
    foreach ($imageObjects as $key=>$imageObject)
    {  
    	$imageUrl = $this->convertRelToScheme($imageObject->getImagePathRelGallery(), $this->galleryPathRelSvm, $this->pathScheme);
      $imageUrl = htmlspecialchars($imageUrl, ENT_QUOTES, 'UTF-8');
      $linkUrl = htmlspecialchars($imageObject->getLinkUrl(), ENT_QUOTES, 'UTF-8');
      $imageTitle = htmlspecialchars($imageObject->getImageTitle(), ENT_QUOTES, 'UTF-8');
      $showFlipButtonHtml = '';
      $linkUrlHtml = 'linkurl="'.$linkUrl.'"';
      $showFlipButtonHtml = $imageObject->getShowFlipButton() ? 'showFlipButton="true"' : 'showFlipButton="false"';
      $imageDescription = $imageObject->getImageDescription();
      $xml .= '      <photo imageurl="'.$imageUrl.'" '.$linkUrlHtml.' '.$showFlipButtonHtml.'>
        <title>'.$imageTitle.'</title>
        <description><![CDATA['.$imageDescription.']]></description>
      </photo>
';
    }
    $xml .= '    </photos>
  </'.TV_XML_SETTINGS_TAG.'>
';
    
    if(@!file_put_contents($xmlPath, $xml, FPC_LOCK))
    {
      trigger_error('unable to write xml to file '.$xmlPath, E_USER_NOTICE);
      return false;
    }
    return true; 
  }

 /** Clean data from customize form and update class properties
  *
  * @access public
  * @return array
  * @param array as in preferences file
  */
  function customize($newSettings)
  {
    $newSettings = array_map('trim', $newSettings);
    $this->preferences['galleryTitle'] = strip_tags($newSettings['galleryTitle']);
    $this->preferences['htmlTitle'] = (USE_HTML_TITLE === false) ? $this->preferences['galleryTitle'] : HTML_TITLE;
    $this->preferences['columns'] = intval(max(0, $newSettings['columns']));
    $this->preferences['rows'] = intval(max(0, $newSettings['rows']));
    $this->preferences['maxJPGSize'] = intval(max(0, $newSettings['maxJPGSize']));
    $this->preferences['frameColor'] = $this->cleanHex($newSettings['frameColor'], 6);
    $this->preferences['useReloadButton'] = isset($newSettings['useReloadButton']);
    $this->preferences['useFlickr'] = ($newSettings['useFlickr'] == 'true');
    $this->preferences['flickrUserId'] = $newSettings['flickrUserId'];
    $this->preferences['flickrTags'] = $newSettings['flickrTags'];
    $this->preferences['flickrTagMode'] = $newSettings['flickrTagMode'];
    $this->preferences['flickrShowTakenByText'] = isset($newSettings['flickrShowTakenByText']);
    // Generic flip button is currently de-activated for user simplicity
    //$this->preferences['showFlipButton'] = isset($newSettings['showFlipButton']);
    $this->preferences['backColor'] = $this->cleanHex($newSettings['backColor'], 6);
    $this->preferences['bkgndInnerColor'] = $this->cleanHex($newSettings['bkgndInnerColor'], 6);
    $this->preferences['bkgndOuterColor'] = $this->cleanHex($newSettings['bkgndOuterColor'], 6);
    $this->preferences['linkLabel'] = $newSettings['linkLabel'];
    $this->preferences['urlType'] = $newSettings['urlType'];
    return true;
  }
 /** Clean data from pro form and update class properties
  *
  * @access public
  * @return array
  * @param array as in preferences file
  */
  function customizePro($newSettings)
  {
    $newSettings = array_map('trim', $newSettings);
    $this->preferences['frameWidth'] = intval(max(-5, $newSettings['frameWidth']));
    $this->preferences['textColor'] = $this->cleanHex($newSettings['textColor'], 6);
    $this->preferences['linkTextColor'] = $this->cleanHex($newSettings['linkTextColor'], 6);
    $this->preferences['linkBkgndColor'] = $this->cleanHex($newSettings['linkBkgndColor'], 6);
    $this->preferences['navButtonColor'] = $this->cleanHex($newSettings['navButtonColor'], 6);
    $this->preferences['flipButtonColor'] = $this->cleanHex($newSettings['flipButtonColor'], 6);
    $this->preferences['zoomedInDistance'] = intval(max(0, $newSettings['zoomedInDistance']));
    $this->preferences['zoomedOutDistance'] = intval(max(0, $newSettings['zoomedOutDistance']));
    $this->preferences['fontName'] = $newSettings['fontName'];
    $this->preferences['titleFontSize'] = intval(max( 0, $newSettings['titleFontSize']));
    $this->preferences['descriptionFontSize'] = intval(max( 0, $newSettings['descriptionFontSize']));
    $this->preferences['linkFontSize'] = intval(max( 0, $newSettings['linkFontSize']));
    $this->preferences['linkTarget'] = $newSettings['linkTarget'];
    $this->preferences['tiltAmountIn'] = intval(max( 0, $newSettings['tiltAmountIn']));
    $this->preferences['tiltAmountOut'] = intval(max( 0, $newSettings['tiltAmountOut']));
    $this->preferences['bkgndTransparent'] = isset($newSettings['bkgndTransparent']);
    $this->preferences['showFullscreenOption'] = isset($newSettings['showFullscreenOption']);
    $this->preferences['enableSounds'] = isset($newSettings['enableSounds']);
    return true;
  }
 
}


?>