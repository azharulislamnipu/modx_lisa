<?php
// Dutch language file
// If you are going to make your own language file and are usign the lighbox, 
// make a language file for it too, f.ex. /maxigallery/slidebox/slidebox_lang_en.js
$strings['previous']="&laquo; vorige";
$strings['next']="volgende &raquo;";
$strings['click_to_zoom']=" &raquo; Klik hier om in te zoomen ->";
$strings['click_to_go_back']="<- Klik hier om terug te gaan";
$strings['click_to_open_original']= "Klik hier om groter plaatje te openen";
$strings['back_to_normal_view'] = "Terug naar normale grootte";
$strings['index']="index";
$strings['go_to_gallery']="Ga naar de galerie ->";
$strings['pictures_successfully_uploaded']="<b>Fotos succesvol geupload.</b>";
$strings['changes_have_been_saved']="<b>Veranderingen zijn opgeslagen.</b>";
$strings['delete']="Verwijder";
$strings['title']="Titel";
$strings['save_changes']="Veranderingen opslaan";
$strings['upload_pictures']="Upload fotos";
$strings['check_to_delete_this_picture']="Check om deze foto te verwijderen";
$strings['manage_pictures']="Beheer fotos";
$strings['date']="Datum";
$strings['description']="Omschrijving";
$strings['position']="Positie";
$strings['gallery_description']="Galerie omschrijving";
$strings['db_no_descr_support']="<em>Waarschuwing: Galerie database ondersteunt geen beschrijvingen!</em><br />";
$strings['db_no_pos_support']="<em>Waarschuwing: Galerie database ondersteunt geen fotoposities!</em><br />";
$strings['picture'] = "Picture:";
$strings['max_pics_reached'] = "U heeft uw maximale fotolimiet bereikt voor deze galerie,dus u kan geen fotos meer versturen.";
$strings['supported_types'] = "<b>Incorrect filetype!</b> Deze galerie ondersteunt de volgende filetypes: jpg/jpeg, png, gif and zip (die eerdergenoemde filetypes bevat)";
$strings['gif_not_supported'] = "<b>De GD-library op deze server ondersteunt geen .gif images</b> U kunt alleen jpg and png plaatjes gebruiken.";
$strings['supported_types_inzip'] = "Foutmelding: zip archief bevatte incorrecte filetypes die werden genegeerd.";
$strings['max_pics_reached_some_discarded'] = "<b>Maximale pictures foto limiet bereikt!</b> Bepaalde gestuurde fotos kunnen afgewezen zijn.";
$strings['zip_not_supported'] = "<b>Uw PHP-installatie ondersteunt geen .zip archieven!</b> U moet de <a href=\"http://www.php.net/manual/ref.zip.php\">PHP zip module and ZZIPlib library installeren.</a>";
$strings['database_error'] = "Foutmelding: Galerie database kon niet gecree�rd worden!";
$strings['database_error_ownid'] = "Foutmelding: own_id field naar database table kon niet worden gecree�rd!";
$strings['invalid_class'] = "Error: Cannot find maxigallery class file. Please check it! ";
$strings['invalid_name_class'] = "Error: Maxigallery class name is invalid.";
?>
