<?php require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsOfficers = "-1";
if (isset($_GET['BOULENAME'])) {
  $colname_rsOfficers = $_GET['BOULENAME'];
}
mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOfficers = sprintf("SELECT * FROM vw_boule_officers WHERE COMMITTEESTATUSSTT = 'Active' AND BOULENAME = %s", GetSQLValueString($colname_rsOfficers, "text"));
$rsOfficers = mysql_query($query_rsOfficers, $sigma_modx) or die(mysql_error());
$row_rsOfficers = mysql_fetch_assoc($rsOfficers);
$totalRows_rsOfficers = mysql_num_rows($rsOfficers);

ob_start();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | MAIL BOUL&#201; OFFICERS</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->

<div class="contentBlock">
  <?php
  $sectemailObj = new tNG_EmailPageSection();
  $sectemailObj->getCSSFrom(__FILE__);
  $sectemailObj->setTo("tonny.joseph@sigmapiphi.org");
  $sectemailObj->setFrom("{GET.DM_EMAIL}");
  $sectemailObj->setSubject("UPDATED OFFICERS' LISTING");
  $sectemailObj->setFormat("HTML/Text");
  $sectemailObj->setEncoding("ISO-8859-1");
  $sectemailObj->setImportance("Normal");
  $sectemailObj->BeginContent();
?>
    <table cellspacing="4" cellpadding="4">
      <tr>
        <td colspan="3"><h2><span class="yellow">OFFICERS' LISTING</span></h2></td>
        </tr>
      <tr>
        <td colspan="3">
          <p >The following is the updated officer listing for <?php echo $_GET['BOULENAME']; ?>.</p>
      <p>Submitted by: <?php echo $_GET['UPDATEDBY']; ?></p></td>
        </tr>
      <tr>
        <td><strong>OFFICE</strong></td>
          <td><strong>CUSTOMERCD</strong></td>
          <td><strong>ARCHON</strong></td>
      </tr>
      <?php do { ?>
        <tr>
          <td><?php echo $row_rsOfficers['CPOSITION']; ?></td>
          <td><?php echo $row_rsOfficers['CUSTOMERCD']; ?></td>
          <td><?php echo $row_rsOfficers['FULLNAME']; ?></td>
        </tr>
        <?php } while ($row_rsOfficers = mysql_fetch_assoc($rsOfficers)); ?>
        </table>
    <?php
  $sectemailObj->EndContent();
  $sectemailObj->Execute();
?></div>
<!-- InstanceEndEditable --></div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsOfficers);

//Redirect Email sectemailObj
  $redObj = new tNG_Redirect(null);
  $redObj->setURL("/home/data-manager.php");
  $redObj->setKeepURLParams(false);
  $redObj->Execute();
//End Redirect Email sectemailObj
?>
