<?php get_header(); ?>

	<div id="content">
		<div class="padder">

		<?php do_action( 'sigma_before_archive' ) ?>

		<div class="page" id="blog-archives">

			<?php
				$category = &get_category(get_query_var('cat'));
				$color = strpos('notices|documents-resources|press-releases', $category->slug) === false ? 'red' : $category->slug;
			?>


			<h2 class="color <?php echo $color ?>"><?php echo wp_title( false, false )  ?></h2>
			<div class="contentBlock">

			<?php // Catch users that shouldn't be viewing this page
			if( is_private_category( $category ) && !can_view_boule_only() ): ?>
				<p>You must be a chapter member to view this page.</p>
			</div>
			<?php else:

				// Get the category breadcrumbs
				$splitter = '@@@';
				$parents = preg_split('/'.$splitter.'/', get_category_parents($category->cat_ID, TRUE, $splitter), NULL, PREG_SPLIT_NO_EMPTY);
				$count = 0;
				$size = sizeof($parents);

				// Do we have at least one breadcrumb?
				if($size > 1): ?>
					<p class="breadcrumbs">
					<?php foreach($parents as $parent):
						if(++$count <= $size) :?>
							<?php echo $parent ?>
							<?php if($count + 1 <= $size):?>
								<span class="sep">&gt;</span>
							<?php endif; ?>
					<?php endif;
					endforeach; ?>
					</p>
				<?php endif; ?>

				<?php // Get the category description
				$category_description = category_description();
				if ( ! empty( $category_description ) ): ?>
					<?php echo $category_description . '' ?>
				<?php endif; ?>

				<ul class="cat-children">
					<?php // Get any child categories
					wp_list_categories(array(
						'child_of' 			=> $category->cat_ID,
						'depth'				=> 1,
						'title_li'  		=> '',
						'show_option_none' 	=> '',
					)); ?>
				</ul>

				<?php if ( have_posts() ) : ?>

					<ul class="<?php echo get_query_var('paged') ?> link-list">
					<?php while (have_posts()) : the_post();

						// Determine if the post should be shown
						if ( can_view_content() ): ?>

							<li>
								<a href="<?php the_permalink(); ?>">
									<strong><?php the_title(); ?></strong>
									<span class="exceprt"><?php the_excerpt(); ?></span>
								</a>
							</li>

						<?php endif; ?>
					<?php endwhile; ?>
					</ul>

					<div class="navigation">

						<div class="alignleft"><?php next_posts_link( __( '&larr; Previous Entries', 'archon-network' ) ) ?></div>
						<div class="alignright"><?php previous_posts_link( __( 'Next Entries &rarr;', 'archon-network' ) ) ?></div>

					</div>

				<?php else : ?>

					<p><?php _e( 'No content found', 'archon-network' ) ?></p>
					<?php // locate_template( array( 'searchform.php' ), true ) ?>

				<?php endif; ?>
				</div>
				<div class="contentBlockFooter">
					<?php if(!empty($category)): ?>
						<a class="icon rss" href="<?php echo get_bloginfo('url') ?>/feed/?cat=<?php echo $category->cat_ID ?>"><?php _e('RSS', 'archon-network')?></a>
					<?php endif; ?>
				</div>

			<?php endif; ?>

		</div>

		<?php do_action( 'sigma_after_archive' ) ?>

		</div><!-- .padder -->
	</div><!-- #content -->

	<?php locate_template( array( 'sidebar.php' ), true ) ?>

<?php get_footer(); ?>