<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Models a SimpleViewer gallery
 *
 * @package svManager
 */
class Sv2Gallery extends Gallery
{
 /**
  * @var array gallery settings to be stored in preferences file
  * backgroundColor, galleryWidth, galleryHeight and useFlash are stored in preferences and xml
  */
  var $storedInPreferences = array(
  'imagePath' => '',
  'thumbPath' => '',
  'sortOrder' => '',
  'backgroundColor' => '',
  'galleryWidth' => '',
  'galleryHeight' => '',
  'useFlash' => '',
  'addLinks' => '',
  'htmlTitle' => '',
  'useColorCorrection' => '',
  'urlType' =>'');
  
 /**
  * @var array preferences stored in xml or preferences file
  * image and thumb directory paths are not included
  */
  var $preferences = array();
  
 /**
  * @var array default settings for Viewer
  * Used to test if settings have been changed and should be saved
  */
  var $defaults = array();
  
 /**
  * @var string name of image class
  */
  var $imageClass = 'Sv2Image';
  
 /**
  * @var array of image objects
  */
  var $imageObjects = array();
    
 /**
  * @var string default path to master gallery
  */
  var $masterPath = SV2_MASTER_PATH;
  
 /**
  * @var string to prepend to paths
  */
  var $pathScheme = '';
  
 /**
  * @var integer count of files in master gallery
  */
  var $masterFileCount = SV2_MASTER_FILECOUNT;
  
 /**
  * @var string default image path
  */
  var $imageDirPathRelGallery = SV2_IMAGE_PATH;
  
 /**
  * @var string default thumb path
  */
  var $thumbDirPathRelGallery = SV2_THUMB_PATH;
  
 /**
  * @var integer thumb width
  */ 
  var $thumbWidth = SV2_THUMB_WIDTH;

 /**
  * @var integer thumb height
  */
  var $thumbHeight = SV2_THUMB_HEIGHT;
  
 /**
  * @var integer thumb quality
  */
  var $thumbQuality = SV2_THUMB_QUALITY;
  
 /**
  * @var string name of viewer swf file
  */
  var $viewerSwf = SV2_SWF;
  
 /**
  * @var string acceptable html tags in captions
  */
  var $okCaptionTags = '<font><b><i><br><br />';
  
 /**
  * @var string acceptable html tags in gallery title
  */
  var $okTitleTags = '<a><b><i><u><font><br><br />';
  
  /**
   * Constructs SvGallery
   *
   * Attempts to read local copies of preferences.txt and gallery.xml
   * Parses gallery.xml file into class settings and imageObjects array.
   * The gallery title from the parsing (which may contain html) takes precedence
   * Note that the parsing translates html entities (e.g. in the title) back to html
   * If local file has been corrupted then default values are used.
   * 
   * @access public
   * @param string gallery reference (permanent reference not the galleriesData index)
   * @param string gallery path
   * @param boolean new gallery
   *
   */  
  function Sv2Gallery($ref, $path, $newGallery = false)
  {
    parent::Gallery();
    $this->galleryRef = $ref;
    $this->galleryPathRelSvm = $path;
    $iniPath = 'plugins'.DIRECTORY_SEPARATOR.'simpleviewer2'.DIRECTORY_SEPARATOR.'settings.ini';
    if (!file_exists($iniPath))
    {
      trigger_error('cannot find settings ini file, '.$iniPath, E_USER_NOTICE);
      return;
    }
    if (!is_readable($iniPath))
    {
      trigger_error('cannot read settings ini file, '.$iniPath, E_USER_NOTICE);
      return;
    }
    $iniSettings = parse_ini_file($iniPath, true);
    if (!is_array($iniSettings))
    {
      trigger_error('cannot read settings ini file, '.$iniPath, E_USER_NOTICE);
      return;
    }
    $this->defaults = $iniSettings['defaults'];
    $this->preferences = $iniSettings['defaults'];
    $this->preferences['imagePath'] = SV2_IMAGE_PATH;
    $this->preferences['thumbPath'] = SV2_THUMB_PATH;
    if ($newGallery)
    {
      $this->preferences = array_merge($iniSettings['defaults'], $iniSettings['overrides']);
      if (strtoupper($this->preferences['addLinks']) == 'FALSE') $this->okCaptionTags .= '<u><a>';
      $this->preferences['htmlTitle'] = USE_HTML_TITLE ? HTML_TITLE : strip_tags($this->htmlEntityDecode->decode($this->preferences['title']));
      $this->addGallery($ref);
      $this->setPathScheme();
      $this->saveGallery();
      return;
    }
    if (!file_exists($this->trimSeparator($this->galleryPathRelSvm)))
    {
      // exit
      trigger_error('cannot find gallery folder '.$this->galleryPathRelSvm, E_USER_ERROR);
    }
    $prefs = $this->readPreferences();
    if ($prefs !== false)
    {
      // background color may be transparent
      unset($prefs['backgroundColor']);
      $this->preferences = array_merge($this->preferences, $prefs);
    }
    $this->setPathScheme();
    $xmlStruct = $this->parseXml($this->getXmlPath());
    if ($xmlStruct !== false)
    {
      $att = $this->xmlParser->parseAttributes($xmlStruct, SV2_XML_SETTINGS_TAG);
      if ($att !== false)
      {
        if (is_array($att)) $att = array_map('trim', $att);
        if (isset($att['textColor']))
        {
          $att['textColor'] = $this->cleanHex($att['textColor']);
        }
        if (isset($att['frameColor']))
        {
          $att['frameColor'] = $this->cleanHex($att['frameColor']);
        }
        if (isset($att['thumbWidth']))
        {
          $this->thumbWidth = $att['thumbWidth'];
        }
        if (isset($att['thumbHeight']))
        {
          $this->thumbHeight = $att['thumbHeight'];
        }
        // name changes in SV2
        if (!isset($att['thumbPosition']) && isset ($att['navPosition']))
        {
          $att['thumbPosition'] = $att['navPosition'];
          unset ($att['navPosition']);
        }
        if (!isset($att['thumbColumns']) && isset ($att['thumbNailColumns']))
        {
          $att['thumbColumns'] = $att['thumbNailColumns'];
          unset($att['thumbNailColumns']);
        }
        if (!isset($att['thumbRows']) && isset ($att['thumbNailRows']))
        {
          $att['thumbRows'] = $att['thumbNailRows'];
          unset($att['thumbNailRows']);
        }
        if (!isset($att['showOpenButton']) && isset($att['enableRightClickOpen']))
        {
          $att['showOpenButton'] = $att['enableRightClickOpen'];
          unset($att['enableRightClickOpen']);
        }
        $this->preferences = array_merge($this->preferences, $att);   
      }
      if ($this->preferences['imagePath'] == '') $this->preferences['imagePath'] = SV2_IMAGE_PATH;
      if ($this->preferences['thumbPath'] == '') $this->preferences['thumbPath'] = SV2_THUMB_PATH;
      $this->preferences['imagePath'] = rtrim(str_replace('\\', '/', $this->preferences['imagePath']), '/').'/';
      $this->preferences['thumbPath'] = rtrim(str_replace('\\', '/', $this->preferences['thumbPath']), '/').'/';
      $this->imageDirPathRelGallery = $this->getPathRelGallery($this->preferences['imagePath']);
      $this->thumbDirPathRelGallery = $this->getPathRelGallery($this->preferences['thumbPath']);
      if (strtoupper($this->preferences['addLinks']) == 'FALSE') $this->okCaptionTags .= '<u><a>';
      $this->imageObjects = $this->parseImageTags($xmlStruct);
    }
    $this->preferences['htmlTitle'] = USE_HTML_TITLE ? HTML_TITLE : strip_tags($this->htmlEntityDecode->decode($this->preferences['title']));
    $this->saveGallery();
  }


 /**
  * Get gallery title
  *
  * @access public
  * @return string
  */
  function getGalleryTitle()
  {
    return $this->preferences['title'];
  }
  
 /**
  * Set gallery title
  * @access public
  * @return void
  * @param string
  */
  function setGalleryTitle($title)
  {
    $this->preferences['title'] = $title;
    return;
  }
  
 /**
  * Get gallery title stripped of html
  *
  * @access public
  * @return string
  */
  function getGalleryTitleText()
  {
    return strip_tags($this->htmlEntityDecode->decode($this->preferences['title']));
  }
  
 /**
  * Get sort order
  *
  * @access public
  * @return string
  */
  function getSortOrder()
  {
    return $this->preferences['sortOrder'];
  }
  
 /**
  * Set sort order in $this->preferences
  *
  * @access public
  * @return void
  * @param string sort order ['dragndrop' | 'alpha' | 'ralpha' | 'date' | 'rdate']
  */
  function setSortOrder($sortOrder)
  {
    $this->preferences['sortOrder'] = $sortOrder;
    return;
  }

 /**
  * Get xml path
  *
  * @access private
  * @return string calculated xml path
  */
  function getXmlPath()
  {
    return $this->galleryPathRelSvm.SV2_XML_FILE;
  }

  /**
   * Set captions in image data
   *
   * @access public
   * @return  boolean true on success
   * @param array captions
   */
  function setImageCaptions($post)
  {
    $captions = $post['cap'];
    $imageLinkUrls = $post['url'];
    $i=0;
    foreach (array_keys($this->imageObjects) as $key)
    {
      $this->imageObjects[$key]->setCaption($captions[$key], $imageLinkUrls[$key], $i, $this->okCaptionTags);
      $i++;
    }
    return true;
  }
     
  /** 
   * Get stored image data from class
   *
   * @access public
   * @return array
   */
   function getImageObjects()
   {
     return $this->imageObjects;
   }


 /**
  * Extract file names and captions from xml structured array
  *
  * Any empty image tags are silently ignored
  * @access private
  * @return array of image objects
  * @param values array as generated by xml_parse_into_struct
  */
  function parseImageTags($xmlStruct)
  {
    $imageObjects = array();
    $vals = $xmlStruct['vals'];
    $imageTagOpen = false;
    $imageTagComplete = false;
    $fileName = '';
    $caption = '';
    $linkUrl ='';
    $linkTarget = '';
    foreach ($vals as $tagInfo)
    {
      $imageTagOpen = ((strtolower($tagInfo['tag']) == 'image' && $tagInfo['type'] == 'open') || $imageTagOpen);
      $imageTagComplete = (strtolower($tagInfo['tag']) == 'image' && $tagInfo['type'] == 'complete');
      if (strtolower($tagInfo['tag']) == 'image' && isset($tagInfo['attributes']))
      {
        $imageTagAttributes = array_change_key_case($tagInfo['attributes'], CASE_LOWER);
      }
      if ((strtolower($tagInfo['tag']) == 'filename') && isset($tagInfo['value']))
      {
        $fileName = $this->htmlEntityDecode->decode($tagInfo['value']);
      }
      if (strtolower($tagInfo['tag']) == 'caption')
      {
        if (isset($tagInfo['value']))
        {
          $caption = strip_tags($tagInfo['value'], $this->okCaptionTags);
        }
        else $caption = SV2_IMAGE_CAPTION;
      }
      if (($imageTagOpen || $imageTagComplete) && strtolower($tagInfo['tag']) == 'image' && ($tagInfo['type'] == 'close' || $tagInfo['type'] == 'complete'))
      {
        $imagePathRelGallery = ($fileName !='') ? $this->imageDirPathRelGallery.$fileName : '';
        if (isset($imageTagAttributes['imageurl']))
        {
          $imagePathRelGallery = $this->getPathRelGallery($imageTagAttributes['imageurl']);
        }
        $imageSize = @getimagesize($this->galleryPathRelSvm.$imagePathRelGallery);
        if ($imageSize === false)
        {
          trigger_error('image '.basename($imagePathRelGallery).' will be omitted, cannot determine size', E_USER_NOTICE);
        }
        $thumbPathRelGallery = ($fileName !='') ? $this->thumbDirPathRelGallery.$fileName : '';
        if (isset($imageTagAttributes['thumburl']))
        {
          $thumbPathRelGallery = $this->getPathRelGallery($imageTagAttributes['thumburl']);
        }
        $linkUrl = isset($imageTagAttributes['linkurl']) ? $imageTagAttributes['linkurl'] : SV2_IMAGE_LINK_URL;
        $linkTarget = isset($imageTagAttributes['linktarget']) ? $imageTagAttributes['linktarget'] : SV2_IMAGE_LINK_TARGET;
        if ($fileName != '' || $imagePathRelGallery != '')
        {
          if ($imageSize != false)
          {
            $imageObjects[] = new $this->imageClass($this->galleryPathRelSvm, $imagePathRelGallery, $thumbPathRelGallery, $caption, $linkUrl);
          }
          $fileName = '';
          $imagePathRelGallery = '';
          $thumbPathRelGallery = '';
          $caption = '';
          $linkUrl ='';
          $linkTarget = '';
          $imageTagAttributes = array();
          $imageTagOpen = false;
          $imageTagComplete = false;
        }
      }
    }
    return $imageObjects;
  }
  
 /**
   * Overrides Gallery::SavePreferences to update preferences and save
   * Difference from parent handles transparent background
   *
   * @access public
   * @return boolean success
   */
  function savePreferences()
   {
     $newPrefs = array_intersect_key($this->preferences, $this->storedInPreferences);
     if(strtoupper($this->preferences['backgroundTransparent']) != "FALSE")
     {
       $newPrefs['backgroundColor'] = 'transparent';
     }
     if (@!file_put_contents($this->galleryPathRelSvm.PREFERENCES_PATH, serialize($newPrefs), FPC_LOCK))
     {
       trigger_error('unable to save preferences to '.$this->galleryPathRelSvm.PREFERENCES_PATH, E_USER_NOTICE);
       return false;
     }
     else
     {
       return true;
     }
   }

 /**
  * Construct xml string and write to file
  * htmlspecialchars is used in case of xml reserved characters e.g. ampersand in back button label
  *
  * @access private
  * @param string file path
  * @return boolean
  */
  function writeXml($xmlPath)
  {
    $imageObjects = $this->imageObjects;
    $attributes = $this->preferences;
    unset($attributes['sortOrder'], $attributes['addLinks'], $attributes['htmlTitle'], $attributes['urlType'], $attributes['imagePath'], $attributes['thumbPath']);
    $skip = $this->defaults;
    unset($skip['title']);
    $attributes = array_diff_assoc($attributes, $skip);
    $addLinks = (strtoupper($this->preferences['addLinks']) != 'FALSE');
    $openTag = (SV2_UNDERLINE_CAPTION_LINKS) ? '<u>' : '';
    $closeTag = (SV2_UNDERLINE_CAPTION_LINKS) ? '</u>' : '';
    $relImagePath = $this->getImageDirPathRelSvm();
    $xml = '<?xml version="1.0" encoding="UTF-8"'. "?>";
    $xml .= '
<'.SV2_XML_SETTINGS_TAG;
    foreach ($attributes as $name => $value)
    {
       $xml .= '
 '.$name.' = "'.htmlspecialchars($value, ENT_QUOTES, 'UTF-8').'"';
    }
    $xml .= '>
';
    foreach ($imageObjects as $key=>$imageObject)
    {
      $fileName = htmlspecialchars($imageObject->getImageFileName(), ENT_QUOTES, 'UTF-8');
      $caption = $imageObject->getCaption();
      $imageUrl = htmlspecialchars($this->convertRelToScheme($imageObject->getImagePathRelGallery(), $this->galleryPathRelSvm, $this->pathScheme), ENT_QUOTES, 'UTF-8');
      $thumbUrl = htmlspecialchars($this->convertRelToScheme($imageObject->getThumbPathRelGallery(), $this->galleryPathRelSvm, $this->pathScheme), ENT_QUOTES, 'UTF-8');
      $imageLinkUrl = htmlspecialchars($imageObject->getImageLinkUrl(), ENT_QUOTES, 'UTF-8');
      $imageLinkTarget = $imageObject->getImageLinkTarget();
      $xml .= '  <image imageURL="'.$imageUrl.'" thumbURL="'.$thumbUrl.'" linkURL="'.$imageLinkUrl.'" linkTarget="'.$imageLinkTarget.'">
';
      if ($addLinks && (strlen($caption) > 0))
      {
        $caption = '<a href="'.$imageUrl.'" target="_blank">'.$openTag.$caption.$closeTag.'</a>';
      }
      $xml .= '    <caption><![CDATA['.$caption.']]></caption>
  </image>
';
    }
    $xml .= '</'.SV2_XML_SETTINGS_TAG.'>';
    
    if(@!file_put_contents($xmlPath, $xml, FPC_LOCK))
    {
      trigger_error('unable to write xml to file '.$xmlPath, E_USER_NOTICE);
      return false;
    }
    return true; 
  }

 /** Clean data from customize form and update class properties
  *
  * @access public
  * @return array
  * @param array as $_POST
  */
  function customize($newSettings)
  {
    $newSettings['textFields'] = array_map('trim', $newSettings['textFields']);
    $this->preferences['title'] = strip_tags($newSettings['textFields']['title'], $this->okTitleTags);
    $this->preferences['htmlTitle'] = USE_HTML_TITLE ?  HTML_TITLE : strip_tags($newSettings['textFields']['title']);
    $this->preferences['frameWidth'] = max(0, $newSettings['textFields']['frameWidth']);
    $this->preferences['thumbColumns'] = max(0, $newSettings['textFields']['thumbColumns']);
    $this->preferences['thumbRows'] = max(0, $newSettings['textFields']['thumbRows']);
    $this->preferences['galleryStyle'] = $newSettings['selectFields']['galleryStyle'];
    $this->preferences['thumbPosition'] = $newSettings['selectFields']['thumbPosition']; 
    $this->preferences['maxImageWidth'] = max(0, $newSettings['textFields']['maxImageWidth']);
    $this->preferences['maxImageHeight'] = max(0, $newSettings['textFields']['maxImageHeight']);
    $this->preferences['showOpenButton'] = isset($newSettings['checkboxes']['showOpenButton']) ? 'TRUE' : 'FALSE';
    $this->preferences['showFullscreenButton'] = isset($newSettings['checkboxes']['showFullscreenButton']) ? 'TRUE' : 'FALSE';
    $this->preferences['textColor'] = $this->cleanHex($newSettings['colorFields']['textColor'], 6);
    $this->preferences['frameColor'] = $this->cleanHex($newSettings['colorFields']['frameColor'], 6);
    $this->preferences['backgroundTransparent'] = isset($newSettings['checkboxes']['backgroundTransparent']) ? 'TRUE' : 'FALSE';
    $this->preferences['addLinks'] = isset($newSettings['checkboxes']['addLinks']) ? 'TRUE' : 'FALSE';
    $this->preferences['backgroundColor'] = $this->cleanHex($newSettings['colorFields']['backgroundColor'], 6);
    $this->preferences['useFlickr'] = isset($newSettings['radio']['useFlickr']) ? $newSettings['radio']['useFlickr'] : 'FALSE';
    $this->preferences['flickrUserName'] = $newSettings['textFields']['flickrUserName'];
    $this->preferences['flickrTags'] = $newSettings['textFields']['flickrTags'];
    $this->preferences['urlType'] = isset($newSettings['radio']['urlType']) ? $newSettings['radio']['urlType'] : 'RELATIVE';
    $this->preferences['galleryWidth'] = $newSettings['textFields']['galleryWidth'];
    $this->preferences['galleryHeight'] = $newSettings['textFields']['galleryHeight'];
    $this->preferences['useFlash'] = isset($newSettings['checkboxes']['useFlash']) ? 'TRUE' : 'FALSE';
  }
  
/** Clean data from Pro form and update class properties
  *
  * @access public
  * @return array
  * @param array as $_POST
  */
  function customizePro($newSettings)
  {
    $this->preferences = array_merge($this->preferences, $newSettings['textFields']);
    $this->preferences = array_merge($this->preferences, $newSettings['selectFields']);
    $this->preferences = array_merge($this->preferences, $newSettings['clearCheckboxes']);
    if (is_array($newSettings['checkboxes']))
    {
      $this->preferences = array_merge($this->preferences, $newSettings['checkboxes']);
    }
    foreach($newSettings['colorFields'] as $key=>$value)
    {
      $this->preferences[$key] = $this->cleanHex($value, 6);   
    }    
  }

}


?>