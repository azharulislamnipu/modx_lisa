<?php
error_reporting(0);
include_once "../home/modxapi.php";
$modx = new MODxAPI();
$modx->connect();
$modx->startSession();

if(empty($_SESSION['CUSTOMERCD'])){
	header('Location:  '.  $_SERVER['HTTP_REFERER']);
	exit;
}
$base_path = str_replace('/home', '', $_api_path);

$visitor_id = $_SESSION['CUSTOMERCD'];

$new_path = '';
$url = $_SERVER['REQUEST_URI'];
/*
 * Local
 */
$pos = strrpos(__DIR__, "htdocs");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'htdocs/';
}

/**
 * Server
 */
/*$pos = strrpos(__DIR__, "apache");
if($pos !== false){
	$new_path = substr(__DIR__, 0, $pos) . 'apache/';
}*/

if(isset($new_path)) {
	ob_start();
	define('LoggedIn', true);
	include $new_path . "includes/functions.php";
	if(!is_ajax()){
		die('Prohibited!');
	}
	include $new_path . "includes/profile_popup/index-ajax.php";
	return ob_get_clean();
}
echo "";
