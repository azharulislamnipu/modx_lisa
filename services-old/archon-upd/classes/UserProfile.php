<?php


/**
 * Manages the Archon and Archousa user profile interactions by:
 * 1. Linking married Archons and Archousas
 * 2. Granting/Revoking Archousa access
 * @author Pat Heard (http://fullahead.org)
 */
class UserProfile {

	const TYPE_ARCHON = 'Archon';
	const TYPE_ARCHOUSA = 'Archousa';

	private $modx = null;
	private $mysql = null;
	private $webInternalKey = -1;
	private $couple = false;
	private $type = null;
	private $isArchousa = false;
	private $isArchon = false;
	private $isArchousaAccessGranted = false;

	/**
	 * Constructor allows MODx or MySQL database connection
	 * @param $args array(mixed)
	 */
	public function __construct(array $args = array()) {

		// Merge the default values with passed values
		$args = array_merge(array(
			'modx'				=> null, // MODx database object
			'mysql'				=> null, // MySQL database connection and name: array('connection' => , 'database_name' =>)
			'webInternalKey'	=> -1,   // CUSTOMERCD or ARCHOUSAID of logged in user
			'webUserGroupNames'	=> null, // Logged in user's groups
			'type'				=> null  // Type of user profile we're viewing
		), $args);

		$this->modx = $args['modx'];
		$this->mysql = $args['mysql'];
		$this->webInternalKey = preg_replace('/[^0-9\-]/', '', $args['webInternalKey']);
		$this->type = $args['type'];
		$this->isArchousa = $this->isArchousaGroup($args['webUserGroupNames']);
		$this->isArchon = !$this->isArchousa;
		$this->couple = $this->getCouple();

		// Determine if the Archousa has web access: access is granted and an active entry on the spp_archousa table
		$this->isArchousaAccessGranted = $this->getArchousaAccessStatus() === 1 && $this->couple !== false;
	}

	/**
	 * Determines if a given user has the Archousa user group
	 * @param $groups array(String) from $_SESSION['webUserGroupNames']
	 * @return boolean
	 */
	private function isArchousaGroup ($groups) {
		$isArchousa = false;
		if (!empty($groups)) {
			foreach($groups as $g) {
				if($g === UserProfile::TYPE_ARCHOUSA) {
					$isArchousa = true;
					break;
				}
			}
		}
		return $isArchousa;
	}


	/**
	 * Given an Archon CUSTOMERCD or Archousa ID, retrieves the currently married partner's ID
	 * @param $id integer CUSTOMERCD or ARCHOUSAID of logged in user from $_SESSION['webInternalKey']
	 * @param $isArchousa boolean Is it an Archousa logged in?
	 * @return mixed false on failure, associative array of married CUSTOMERCD and ARCHOUSAID
	 */
	public function getCouple () {

		$result = $this->runQuery($this->getCoupleQuery());
		$row = $this->getRow($result);

		// If there were results, it means we've got a happily married couple
		if ($row) {
			return $row;
		}
		return false;
	}


	/**
	 * Determines Archousa access status
	 * @param $status integer 1 = Grant, 0 = Revoke
	 * @param $userid User performing the grant or revoke (from $_SESSION['webShortname'])
	 * @return boolean success indicator
	 */
	public function getArchousaAccessStatus() {

		if ($this->couple) {
			$result = $this->runQuery('SELECT CAST(ACCESSSTATUS AS unsigned int) AS ACCESSSTATUS FROM spp_archousa WHERE ARCHOUSAID = ' . $this->couple['ARCHOUSAID'] . ' LIMIT 1');
			$row = $this->getRow($result);
		}
		return $row ? (int)$row['ACCESSSTATUS'] : 0;
	}


	/**
	 * Grants or revokes an Archousa's user's access status
	 * @param $status integer 1 = Grant, 0 = Revoke
	 * @param $userid User performing the grant or revoke (from $_SESSION['webShortname'])
	 * @return boolean success indicator
	 */
	public function setArchousaAccessStatus($status, $userid) {

		$success = false;
		if ($this->couple && !$this->isArchousa) {
			//$this->runQuery('UPDATE spp_archousa SET ACCESSSTATUS = ' . ($status === 1 ? 1 : 0) . ', WHERE ARCHOUSAID = ' . $this->couple['ARCHOUSAID'] . ' LIMIT 1');
		}
		return $success;
	}

	/**
	 * Returns the user ID that will be used to retrieve the Archon or Archousa's personal data
	 * @return integer Archon's CUSTOMERCD or Archousa's ARCHOUSAID
	 */
	public function getUserid () {
		if ($this->couple) {
			return $this->type === UserProfile::TYPE_ARCHON ? $this->couple['CUSTOMERCD'] : $this->couple['ARCHOUSAID'];
		}
		return $this->webInternalKey;
	}

	/**
	 * Returns the Archousa's SPOUSEID
	 * @return integer Archon's SPOUSEID or -1 if it doesn't exist
	 */
	public function getSpouseid () {
		if ($this->couple) {
			return $this->couple['SPOUSEID'];
		}
		return -1;
	}
	/**
	 * Returns the Archon's CUSTOMERCD
	 * @return integer Archon's CUSTOMERCD or -1 if it doesn't exist
	 */
	public function getCustomercd () {
		if ($this->couple) {
			return $this->couple['CUSTOMERCD'];
		} else if ($this->type === UserProfile::TYPE_ARCHON) {
			return $this->webInternalKey;
		}
		return -1;
	}

	/**
	 * Returns Archousa's ARCHOUSAID
	 * @return integer Archousa's ARCHOUSAID or -1 if it doesn't exist
	 */
	public function getArchousaid () {
		if ($this->couple) {
			return $this->couple['ARCHOUSAID'];
		} else if ($this->type === UserProfile::TYPE_ARCHOUSA) {
			return $this->webInternalKey;
		}
		return -1;
	}

	/**
	 * Returns the SQL query required to link a couple
	 * @param $id integer CUSTOMERCD or ARCHOUSAID of logged in user from $_SESSION['webInternalKey']
	 * @param $isArchousa boolean Is it an Archousa logged in?
	 * @return String the appropriate query
	 */
	private function getCoupleQuery () {
		return 'SELECT SPOUSEID, ARCHOUSAID, CUSTOMERCD FROM spp_archousa WHERE ' . ($this->isArchousa ? 'ARCHOUSAID' : 'CUSTOMERCD') . ' = ' . $this->webInternalKey . ' AND MARITALSTATUS = "CUR" LIMIT 1';
	}

	/**
	 * Returns an associative array representing the next row of data in a result object
	 * @param $result MODx or MySQL result object
	 * @return mixed associative array representing row of data or false on failure
	 */
	private function getRow ($result) {
		$row = false;
		if ($result) {
			if ($this->isMODx()) {
				$row = $this->modx->db->getRow($result);
			} else if ($this->isMySQL()) {
				$row = mysql_fetch_assoc($result);
			}
		}
		return $row;
	}

	/**
	 * Runs a given query using MODx or MySQL
	 * @param $query string query to execute
	 * @return mixed MODx or MySQL result object on success, false on fail
	 */
	private function runQuery ($query) {
		$result = false;
		if ($this->isMODx()) {
			$result = $this->modx->db->query($query);
		} else if ($this->isMySQL()) {
			mysql_select_db($this->mysql['database_name'], $this->mysql['connection']);
			$result = mysql_query($query, $this->mysql['connection']) or die(mysql_error());
		}
		return $result;
	}

	private function isMODx () {
		return isset($this->modx);
	}

	private function isMySQL () {
		return isset($this->mysql) && isset($this->mysql['connection']) && isset($this->mysql['database_name']);
	}

	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function __toString() {
		return
			'UserProfile: ' .
			' type: ' .$this->type .
			' modx: ' .$this->isMODx() .
			' mysql: ' .$this->mysql .
			' webInternalKey: ' .$this->webInternalKey .
			' isArchousa: ' .$this->isArchousa .
			' couple: ' . print_r($this->couple, true);
	}
}


?>