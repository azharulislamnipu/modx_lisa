<?php
/*
	Copyright (c) InterAKT Online 2000-2006. All rights reserved.
*/

/**
 * This class is the "XML" implementation of the tNG_import class.
 * @access public
 */
class tNG_XMLImport extends tNG_import {
	/**
	 * The format for the XML source (data represented as nodes or as attributes)
	 * @var string
	 * @access private
	 */
	var $xmlFormat;
	
	/**
	 * The encoding of the XML data
	 * @var string
	 * @access private
	 */
	var $xmlEncoding;
	
	/**
	 * The encoding used in the database
	 * @var string
	 * @access private
	 */
	var $dbEncoding;
	
	/**
	 * The encoding used for the XML parsing (UTF or ISO-8859-1)
	 * @var string
	 * @access private
	 */
	var $internalEncoding;
	
	/**
	 * Stores the XML content before parsing
	 * @var string
	 * @access private
	 */
	var $content;
	
	/**
	 * Stores the depth of the XML tree
	 * @var integer
	 * @access private
	 */
	var $depth;
	
	/**
	 * Stores the significat data retrieved from the XML tree
	 * @var array
	 * @access private
	 */
	var $depthArr;
	
	/**
	 * Constructor. Sets the connection, the database name and other default values.
	 * Also sets the import type and import variables reference.
	 * @param object KT_Connection &$connection the connection object
	 * @access public
	 */
	function tNG_XMLImport(&$connection) {
		parent::tNG_import($connection);
		$this->hasHeader = true;
		$this->importType = 'XML';
		$this->importReference = 'KT_XML';
		$this->lineStart = 0;
		$this->xmlFormat = 'NODES';
		$this->xmlEncoding = 'UTF-8';
		$this->dbEncoding = 'UTF-8';
		$this->internalEncoding = 'UTF-8';
		$this->content = '';
		$this->depthArr = array();
	}
	
	/**
	 * Sets the source of the XML data
	 * Calls the parent setSource method and also registers an upload file STARTER trigger.
	 * @param string $type The type of the source (FILES only)
	 * @param string $reference The submitted variable name (if type=FILES and reference=test, value=$_FILES['test'])
	 * @access public
	 */ 
	function setSource($type, $reference) {
		parent::setSource($type, $reference);
		
		if ($this->source['type'] == 'FILES') {
			$this->registerTrigger("STARTER", "Trigger_XMLImport_FileUpload", 90, $this->source['reference']);
		}
	}
	
	/**
	 * Sets the URL source of the XML data (if the source is a remote URL)
	 * Calls the parent setSource method.
	 * @param string $type The type of the source (VALUE, GET, SESSION)
	 * @param string $reference The submitted variable name (if type=VALUE and reference='http://www.mysite.com/source.xml', value='http://www.mysite.com/source.xml')
	 * @access public
	 */ 
	function setURLSource($type, $reference) {
		parent::setSource($type, $reference);
	}
	
	/**
	 * Sets the format of the XML source
	 * @param string $xmlFormat the format (nodes or attributes)
	 * @access public
	 */ 
	function setXMLFormat($xmlFormat) {
		$this->xmlFormat = strtoupper(trim($xmlFormat));
		if ($this->xmlFormat != 'ATTRIBUTES') {
			$this->xmlFormat = 'NODES';
		}
	}
	
	/**
	 * Sets the encoding of the database
	 * @param string $dbEncoding the encoding
	 * @access public
	 */ 
	function setDBEncoding($dbEncoding) {
		$this->dbEncoding = strtoupper(trim($dbEncoding));
	}
	
	/**
	 * Set the headers and data structures associated to this transaction
	 * @return object tNG_Error or null if no error occured
	 * @access protected
	 */
	function &prepareData() {
		if ($this->source['type'] == 'FILES') {
			$ret = $this->getContentFromFile();
		} else {
			$ret = $this->getContentFromURI();
		}
		if ($ret !== null) {
			return $ret;
		}
		
		if (preg_match("/<\?xml.*?encoding\s*=\s*[\"'](.*?)[\"'].*?\?>/is", $this->content, $matches)) {
			$this->xmlEncoding = strtoupper($matches[1]);
		}
		$this->content = preg_replace('/<\?xml.*\?>/is', '', $this->content);
		
		$expatEncodings = array('ISO-8859-1', 'UTF-8');
		if (!in_array($this->xmlEncoding, $expatEncodings) || !in_array($this->dbEncoding, $expatEncodings)) {
			// we need mbstring enabled
			$this->internalEncoding = 'UTF-8';
			$ret = $this->checkMBstring();
			if ($ret !== null) {
				return $ret;
			}
			$this->content = $this->convertEncodings($this->content, $this->xmlEncoding, $this->internalEncoding);
		} else {
			if ($this->xmlEncoding == 'ISO-8859-1' && $this->dbEncoding == 'ISO-8859-1') {
				$this->internalEncoding = 'ISO-8859-1';
			} else {
				$this->internalEncoding = 'UTF-8';
				$this->content = $this->convertEncodings($this->content, $this->xmlEncoding, $this->internalEncoding);
			}
		}
		
		$this->depth = 0;
		
		$xmlParser = xml_parser_create($this->internalEncoding);
		xml_set_object($xmlParser, $this);
		xml_parser_set_option($xmlParser, XML_OPTION_CASE_FOLDING, 0);
		xml_set_element_handler($xmlParser, "tag_open", "tag_close");
		xml_set_character_data_handler($xmlParser, "cdata");
		$error = xml_parse($xmlParser, $this->content, true);
		if ($error == false) {
			$ret = new tNG_error('XML_IMPORT_INVALID_XML_ERROR', array(), array(xml_error_string(xml_get_error_code($xmlParser)), xml_get_current_line_number($xmlParser)));
			tNG_log::log('KT_ERROR');
			return $ret;
		}
		xml_parser_free($xmlParser);
		
		if ($this->xmlFormat == 'NODES') {
			foreach ($this->depthArr[0]['child'] as $i => $row) {
				$this->data[$i] = array();
				foreach ($row['child'] as $j => $header) {
					$this->data[$i][$header['name']] = $this->convertEncodings($header['text'], $this->internalEncoding, $this->dbEncoding);
				}
			}
		} else {
			foreach ($this->depthArr[0]['child'] as $i => $row) {
				$this->data[$i] = array();
				foreach ($row['attrs'] as $header => $value) {
					$this->data[$i][$header] = $this->convertEncodings($value, $this->internalEncoding, $this->dbEncoding);
				}
			}
		}
		
		$this->depthArr = array();
		$this->content = '';
		return $ret;
	}
	
	/**
	 * Retrieves XML content from an uploaded file
	 * @return object tNG_Error or null if no error occured
	 * @access private
	 */
	function getContentFromFile() {
		$ret = $this->uploadObj->errObj;
		if ($ret === null && (!isset($this->uploadObj->uploadedFileName) || $this->uploadObj->uploadedFileName == '')) {
			$ret = new tNG_error('XML_IMPORT_NO_FILE_ERROR', array(), array());
			tNG_log::log('KT_ERROR');
			return $ret;
		}
		
		$f = @fopen($this->uploadObj->dynamicFolder . $this->uploadObj->uploadedFileName, 'rb');
		if (is_resource($f)) {
			$this->content = fread($f, filesize($this->uploadObj->dynamicFolder . $this->uploadObj->uploadedFileName));
			fclose($f);
		} else {
			$ret = new tNG_error('XML_IMPORT_READ_FILE_ERROR', array(), array());
			tNG_log::log('KT_ERROR');
		}
		$this->uploadObj->RollBack();
		return $ret;
	}
	
	/**
	 * Retrieves XML content from an external URL
	 * @return object tNG_Error or null if no error occured
	 * @access private
	 */
	function getContentFromURI() {
		$ret = null;
		$uri = KT_getRealValue($this->source['type'], $this->source['reference']);
		if (!isset($uri) || $uri == '') {
			$ret = new tNG_error('XML_IMPORT_NO_URI_ERROR', array(), array());
			tNG_log::log('KT_ERROR');
			return $ret;
		}
		
		if (ini_get('allow_url_fopen')) {
			$f = @fopen($uri, 'rb');
			if (is_resource($f)) {
				while (!feof($f)) {
					$this->content .= fread($f, 8192);
				}
				fclose($f);
			} else {
				$ret = new tNG_error('XML_IMPORT_READ_URI_ERROR', array($uri), array());
				tNG_log::log('KT_ERROR');
			}
		} else {
			$ret = new tNG_error('PHP_XML_IMPORT_FOPEN_DISABLED_ERROR', array(), array());
			tNG_log::log('KT_ERROR');
		}
		
		return $ret;
	}
	
	/**
	 * Retrieves the tag data for a XML node (on node opening)
	 * Used for the XML parsing
	 * @access private
	 */
	function tag_open($parser, $node, $attributes) {
		$this->depth++;
		if ($this->depth > 3) {
			return;
		}
		array_push($this->depthArr, array('name' => $node, 'attrs' => $attributes , 'text' => '', 'child' => array()));
	}
	
	/**
	 * Retrieves the text content of an XML node
	 * Used for the XML parsing
	 * @access private
	 */
	function cdata($parser, $cdata) {
		if ($this->depth > 3) {
			return;
		}
		$this->depthArr[count($this->depthArr) - 1]['text'] .= $cdata;
	}
	
	/**
	 * Retrieves the tag data for a XML node (on node closure)
	 * Used for the XML parsing
	 * @access private
	 */
	function tag_close($parser, $node) {
		if ($this->depth > 3 || $this->depth == 1) {
			return;
		}
		$tmpNode = array_pop($this->depthArr);
		$this->depthArr[count($this->depthArr) - 1]['child'][] = $tmpNode;
		$this->depth--;
	}
	
	/**
	 * Checks if mbstring is enabled
	 * @return object tNG_Error or null if no error occured
	 * @access private
	 */
	function checkMBstring() {
		if (!function_exists('mb_convert_encoding')) {
			$ret = new tNG_error('PHP_XML_IMPORT_MBSTRING_DISABLED_ERROR', array(), array());
			tNG_log::log('KT_ERROR');
			return $ret;
		}
		return null;
	}
	
	/**
	 * Applies character conversions
	 * @param string $value
	 * @param string $inEncoding
	 * @param string $outEncoding
	 * @return string
	 * @access private
	 */
	function convertEncodings($value, $inEncoding, $outEncoding) {
		if ($inEncoding != $outEncoding) {
			if ($inEncoding == 'ISO-8859-1' && $outEncoding == 'UTF-8') {
				$value = utf8_encode($value);
			} elseif ($inEncoding == 'UTF-8' && $outEncoding == 'ISO-8859-1') {
				$value = utf8_decode($value);
			} else {
				$value = mb_convert_encoding($value, $outEncoding, $inEncoding);
			}
		}
		return $value;
	}
	
	/**
	 * Generates sample and hints for the current XML import transaction
	 * @return string
	 * @access public
	 */
	function getHints() {
		$ret = '';
		$ret .= "<div class=\"KT_xml_hints\">\r\n";
		$ret .= "<h3>". KT_getResource('XML_HINTS_SAMPLES','tNG',array()) ."</h3>\r\n";
		$ret .= "<p style=\"font: 10px verdana;\">\r\n";
		$ret .= "&lt;root&gt;\r\n";
		$ret .= "<br />\r\n";
		
		$DBColumns = array();
		
		$hasDateColumn = false;
		
		for ($i=1; $i<=2; $i++) {
			$ret .= '&nbsp;&nbsp;';
			if ($this->xmlFormat == 'NODES') {
				$ret .= "&lt;row&gt;\r\n";
			} else {
				$ret .= "&lt;row";
			}
			foreach($this->columns as $colName=>$colDetails) {
				if ($colDetails['method'] == 'XML') {
					$DBColumns[$colName] = $colDetails['reference'];
					
					$label = $colDetails['reference'];
					$colType = $colDetails['type'];
					switch ($colType) {
						case 'STRING_TYPE':
							$value = KT_getResource('XML_HINTS_STRING', 'tNG', array()) . $i;
							break;
						case 'NUMERIC_TYPE':
							$value = $i;
							break;
						case 'DOUBLE_TYPE':
							$value = $i . '.' . $i;
							break;
						case 'DATE_TYPE':
						case 'DATE_ACCESS_TYPE':
							$value = KT_DynamicData('{NOW}',null);
							$hasDateColumn = true;
							break;
						case 'CHECKBOX_YN_TYPE':
							$value = 'Y';
							break;
						case 'CHECKBOX_1_0_TYPE':
						case 'CHECKBOX_-1_0_TYPE':
							$value = '1';
							break;
						case 'CHECKBOX_TF_TYPE':
							$value = 't';
							break;
						default:
							$value = 'string' . $i;
							break;
					}
					if ($this->xmlFormat == 'NODES') {
						$ret .= "<br />\r\n";
						$ret .= '&nbsp;&nbsp;&nbsp;&nbsp;';
						$ret .= '&lt;<strong>' . $label . '</strong>&gt;';
						$ret .= $value;
						$ret .= '&lt;/<strong>' . $label . "</strong>&gt;\r\n";
					} else {
						$ret .= ' <strong>' . $label . '</strong>="';
						$ret .= $value . '" ';
					}
				}
			}
			if ($this->xmlFormat == 'NODES') {
				$ret .= "<br />\r\n";
				$ret .= '&nbsp;&nbsp;';
				$ret .= "&lt;/row&gt;<br />\r\n";
			} else {
				$ret .= " /&gt;<br />\r\n";
			}
		}
		$ret .= "&lt;/root&gt;\r\n";
		$ret .= "</p>\r\n";
		
		$ret .= "<h3>". KT_getResource('XML_HINTS_NOTES', 'tNG', array()) ."</h3>\r\n";
		$ret .= "<ul style=\"margin-top:0\">\r\t";
		if ($hasDateColumn == true) {
			$ret .= "<li>" . KT_getResource('XML_HINTS_DATE_FORMAT', 'tNG', array($GLOBALS['KT_screen_date_format'])) ."</li>\r\n";
		}
		// Unique key
		if ($this->uniqueKey != '') {
			$ret .= "<li>" . KT_getResource('XML_HINTS_COLUMNNAME_UNIQUE', 'tNG', array($DBColumns[$this->uniqueKey])) . "</li>\r\n";
		}
		
		$ret .= "</ul>\r\n";
		$ret .= "</div>";
		
		return $ret;
	}
}
?>