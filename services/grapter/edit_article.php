<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("event_date", true, "date", "", "", "", "");
$formValidation->addField("event_loc", true, "text", "", "", "", "");
$formValidation->addField("art_title", true, "text", "", "", "", "");
$formValidation->addField("art_file", true, "", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_FileUpload trigger
//remove this line if you want to edit the code by hand 
function Trigger_FileUpload(&$tNG) {
  $uploadObj = new tNG_FileUpload($tNG);
  $uploadObj->setFormFieldName("art_file");
  $uploadObj->setDbFieldName("art_file");
  $uploadObj->setFolder("images/{iss_cd}/{CHAPTERID}/");
  $uploadObj->setMaxSize(51200);
  $uploadObj->setAllowedExtensions("txt, doc, docx");
  $uploadObj->setRename("custom");
  $uploadObj->setRenameRule("{art_id}-{iss_cd}-{dept_cd}-{ORGCD}-{KT_name}.{KT_ext}");
  return $uploadObj->Execute();
}
//end Trigger_FileUpload trigger

// Make an update transaction instance
$upd_grapter_articles = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_grapter_articles);
// Register triggers
$upd_grapter_articles->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_grapter_articles->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_grapter_articles->registerTrigger("END", "Trigger_Default_Redirect", 99, "submit_article.php?art_id={art_id}&dept_cd={dept_cd}");
$upd_grapter_articles->registerTrigger("AFTER", "Trigger_FileUpload", 97);
// Add columns
$upd_grapter_articles->setTable("grapter_articles");
$upd_grapter_articles->addColumn("dept_cd", "STRING_TYPE", "POST", "dept_cd");
$upd_grapter_articles->addColumn("art_id", "NUMERIC_TYPE", "POST", "art_id");
$upd_grapter_articles->addColumn("CHAPTERID", "STRING_TYPE", "POST", "CHAPTERID");
$upd_grapter_articles->addColumn("event_date", "DATE_TYPE", "POST", "event_date");
$upd_grapter_articles->addColumn("event_loc", "STRING_TYPE", "POST", "event_loc");
$upd_grapter_articles->addColumn("art_title", "STRING_TYPE", "POST", "art_title");
$upd_grapter_articles->addColumn("art_file", "FILE_TYPE", "FILES", "art_file");
$upd_grapter_articles->addColumn("iss_cd", "STRING_TYPE", "POST", "iss_cd");
$upd_grapter_articles->addColumn("ORGCD", "NUMERIC_TYPE", "POST", "ORGCD");
$upd_grapter_articles->setPrimaryKey("art_id", "NUMERIC_TYPE", "GET", "art_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsgrapter_articles = $tNGs->getRecordset("grapter_articles");
$row_rsgrapter_articles = mysql_fetch_assoc($rsgrapter_articles);
$totalRows_rsgrapter_articles = mysql_num_rows($rsgrapter_articles);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wdg="http://ns.adobe.com/addt">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/Calendar.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/SmartDate.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar_stripped.js"></script>
<script type="text/javascript" src="../includes/wdg/calendar/calendar-setup_stripped.js"></script>
<script src="../includes/resources/calendar.js"></script>
</head>

<body>


<?php
	echo $tNGs->getErrorMsg();
?>
<form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" enctype="multipart/form-data">
  <table cellpadding="2" cellspacing="0" class="KT_tngtable">
    <tr>
      <td class="KT_th"><label for="event_date">EVENT DATE:</label></td>
      <td><input name="event_date" id="event_date" value="<?php echo KT_formatDate($row_rsgrapter_articles['event_date']); ?>" size="32" wdg:mondayfirst="false" wdg:subtype="Calendar" wdg:mask="<?php echo $KT_screen_date_format; ?>" wdg:type="widget" wdg:singleclick="true" wdg:restricttomask="no" wdg:readonly="true" />
          <?php echo $tNGs->displayFieldHint("event_date");?> <?php echo $tNGs->displayFieldError("grapter_articles", "event_date"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="event_loc">EVENT LOCATION:</label></td>
      <td><input type="text" name="event_loc" id="event_loc" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['event_loc']); ?>" size="32" />
          <?php echo $tNGs->displayFieldHint("event_loc");?> <?php echo $tNGs->displayFieldError("grapter_articles", "event_loc"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="art_title">ARTICLE TITLE:</label></td>
      <td><input type="text" name="art_title" id="art_title" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['art_title']); ?>" size="32" />
          <?php echo $tNGs->displayFieldHint("art_title");?> <?php echo $tNGs->displayFieldError("grapter_articles", "art_title"); ?> </td>
    </tr>
    <tr>
      <td class="KT_th"><label for="art_file">ARTICLE FILE:</label></td>
      <td><input type="file" name="art_file" id="art_file" size="32" />
          <?php echo $tNGs->displayFieldError("grapter_articles", "art_file"); ?> </td>
    </tr>
    <tr class="KT_buttons">
      <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="UPDATE ARTICLE" />
      </td>
    </tr>
  </table>
  <input type="hidden" name="dept_cd" id="dept_cd" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['dept_cd']); ?>" />
  <input type="hidden" name="art_id" id="art_id" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['art_id']); ?>" />
  <input type="hidden" name="CHAPTERID" id="CHAPTERID" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['CHAPTERID']); ?>" />
  <input type="hidden" name="iss_cd" id="iss_cd" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['iss_cd']); ?>" />
  <input type="hidden" name="ORGCD" id="ORGCD" value="<?php echo KT_escapeAttribute($row_rsgrapter_articles['ORGCD']); ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
