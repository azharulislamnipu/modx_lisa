<?php
require_once('../Connections/sigma_modx.php');

$errorMsg = null;
$hasErrors = true;
//$errorMsg = 'You must specify at least one field!';

$currentpage = 0;
$sort = 'LASTNAME';
$sortdir = 'ASC';
if (isset($_GET['currentpage'])) {
    $currentpage = intval($_GET['currentpage']);
}
if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
}
if (isset($_GET['sortdir'])) {
    $sortdir = $_GET['sortdir'];
}

mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsIndustry = "SELECT DESCRIPTION, OCCUPATIONCD, OCCUPATIONID  FROM spp_occupation  GROUP BY DESCRIPTION ORDER BY `DESCRIPTION` ASC";
$rsIndustry = mysqli_query($sigma_modx, $query_rsIndustry) or die(mysqli_error($sigma_modx));
$totalRows_rsIndustry = mysqli_num_rows($rsIndustry);

// built to allow key -> values relations for industries in the "You searched for" text
/*$industries = array();
if ($totalRows_rsIndustry)
{
    while ($row_rsIndustry = mysqli_fetch_assoc($rsIndustry))
    {
      $industry = mysqli_fetch_assoc($rsIndustry);
      $industries[$industry['OCCUPATIONID']] = $industry['DESCRIPTION'];
    }
    mysql_data_seek($rsIndustry, 0);
}*/

//$row_rsIndustry = mysqli_fetch_assoc($rsIndustry);


$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = mysqli_query($sigma_modx, $query_rsStates) or die(mysqli_error($sigma_modx));
$row_rsStates = mysqli_fetch_assoc($rsStates);
$totalRows_rsStates = mysqli_num_rows($rsStates);

$query_rsBoule = "SELECT * FROM spp_boule ORDER BY ORGCD ASC";
$rsBoule = mysqli_query($sigma_modx, $query_rsBoule) or die(mysqli_error($sigma_modx));
$row_rsBoule = mysqli_fetch_assoc($rsBoule);
$totalRows_rsBoule = mysqli_num_rows($rsBoule);

$query_rsRegions = "SELECT DISTINCT REGIONCD, REGIONNAME FROM spp_boule ORDER BY REGIONNAME ASC";
$rsRegions = mysqli_query($sigma_modx, $query_rsRegions) or die(mysqli_error($sigma_modx));
$row_rsRegions = mysqli_fetch_assoc($rsRegions);
$totalRows_rsRegions = mysqli_num_rows($rsRegions);

$query_rsOfficers = "SELECT * FROM spp_cposition WHERE CPOSITION NOT LIKE '%Grand%' AND CPOSITION NOT LIKE '%past regional sire archons council%' AND CPOSITION NOT LIKE '%committe%' AND CPOSITION NOT LIKE '%hair%' AND CPOSITION NOT LIKE '%ounse%' ORDER BY CPOSITIONID ASC";
$rsOfficers = mysqli_query($sigma_modx, $query_rsOfficers) or die(mysqli_error($sigma_modx));
$totalRows_rsOfficers = mysqli_num_rows($rsOfficers);


//SEARCH SQL

function keyPars($sql, $values, $fields)
{
    //$fields = Array('FIRSTNAME','LASTNAME');
    if (strlen($sql) > 0) {
        $sql .= ' AND ';
    }
    $vals = null;
    if (is_array($values)) {
        $vals = $values;
    } else {
        $vals = Array();
        $vals[0] = $values;
    }
    $insql = '';
    $insql2 = '';
    foreach ($vals as $val) {
        $insql2 = '';
        foreach ($fields as $fld) {
            if (strlen($insql2) > 0) {
                $insql2 .= ' OR ';
            }
            $insql2 .= $fld . ' LIKE "%' . $val . '%"';
        }
        if (strlen($insql) > 0) {
            $insql .= ' AND ';
        }
        $insql .= '(' . $insql2 . ')';
    }
    $sql .= '(' . $insql . ')';
    return $sql;
}

function testParm($in, $fld)
{
    if (isset($_GET[$in])) {
        $val = $_GET[$in];
        if (is_array($val)) {
            return true;
        } else {
            $val = $val . '';
            if (strlen($val) > 0) {
                return true;
            }
        }
    }
    return false;
}

$searchpars_out = null;

function addSearchPar($sql, $in, $field, $like)
{
    if (!testParm($in, $field)) {
        return $sql;
    }
    global $searchpars_out;
    $searchpars_out .= ucfirst($in) . ' = ';
    $values = $_GET[$in];
    if (strlen($sql) > 0) {
        $sql .= ' AND ';
    }
    $vals = null;
    if (is_array($values)) {
        $vals = $values;
    } else {
        $vals = Array();
        $vals[0] = $values;
    }
    $insql = '';
    foreach ($vals as $val) {
        $searchpars_out .= $val . ', ';
        if (strlen($insql) > 0) {
            $insql .= ' OR ';
        }
        if ($like) {
            $insql .= $field . ' LIKE "%' . $val . '%"';
        } else {
            $insql .= $field . ' = "' . $val . '"';
        }
    }
    $sql .= '(' . $insql . ')';
    if ($field == 'OFFICERID') {
        $sql .= ' AND COMMITTEESTATUSSTT = "Active"';
    }
//    $searchpars_out .= ', ';
    return $sql;
}

$searchsql = '';


if (isset($_GET['q'])) {
    $keys = $_GET['q'] . '';
    if (strlen($keys) > 0) {
        $searchpars_out .= 'Keywords = ' . $keys;
        $vals = explode(' ', $keys);
        $fieldssql = 'DESC vw_search2';
        $res = mysqli_query($sigma_modx, $fieldssql) or die(mysqli_error($sigma_modx));
        $fields = Array();
        while ($row = mysqli_fetch_assoc($res)) {
            array_push($fields, $row['Field']);
        }
        $searchsql = keyPars($searchsql, $vals, $fields);
        $hasErrors = false;
    }

}

$searchsql = addSearchPar($searchsql, 'firstname', 'FIRSTNAME', true);
$searchsql = addSearchPar($searchsql, 'lastname', 'LASTNAME', true);
$searchsql = addSearchPar($searchsql, 'boule', 'CHAPTERID', false);
$searchsql = addSearchPar($searchsql, 'city', 'CITY', true);
$searchsql = addSearchPar($searchsql, 'state', 'STATECD', true);
$searchsql = addSearchPar($searchsql, 'region', 'REGIONCD', false);
$searchsql = addSearchPar($searchsql, 'office', 'CPOSITION', false);
$searchsql = addSearchPar($searchsql, 'title', 'JOBTITLE', true);
$searchsql = addSearchPar($searchsql, 'company', 'COMPANYNAME', true);
$searchsql = addSearchPar($searchsql, 'school', 'INSTITUTION', true);
$searchsql = addSearchPar($searchsql, 'industry', 'OCCUPATION_TYPE', false);
$searchsql = addSearchPar($searchsql, 'company', 'COMPANYNAME', true);
$searchsql = addSearchPar($searchsql, 'company', 'COMPANYNAME', true);

if (strlen($searchsql) > 0) {
    $hasErrors = false;
}
$totalRows_rsSearch = null;
if (!$hasErrors) {
    echo "<pre>";
//    var_dump($searchsql);
    $searchressql = 'SELECT WEB_ID, FIRSTNAME, LASTNAME, L2R_FULLNAME2, BOULENAME, CITY, STATECD, EMAIL, HOMEPHONE, CPOSITION, JOB_TITLE, IMAGE, OCCUPATION_TYPE FROM vw_search_y2 WHERE ' . $searchsql . ' GROUP BY WEB_ID ORDER BY ' . $sort . ' ' . $sortdir . ' LIMIT ' . ($currentpage * 25) . ', 25';


	var_dump($searchressql);
	die();

    $rsArchons = mysqli_query($sigma_modx, $searchressql) or die(mysqli_error($sigma_modx));
    $searchpagessql = 'SELECT COUNT(*) as cnt FROM vw_search_y2 WHERE ' . $searchsql . ' GROUP BY WEB_ID';
    //echo $searchressql;
    $rsPageArchons = mysqli_query($sigma_modx, $searchpagessql) or die(mysqli_error($sigma_modx));
    //$row = mysqli_fetch_assoc($rsPageArchons);
    $totalRows_rsSearch = mysqli_num_rows($rsPageArchons);
} else {
    $errorMsg = 'You must specify at least one field!';
}

function urlWithParam($par, $value)
{
    $gets = '';
    $found = false;
    foreach ($_GET as $c => $v) {
        //echo "LOOOOK" . $c. " = > ".$v."<br>";
        if (strlen($gets) > 0) {
            $gets .= '&';
        }
        if ($c == $par) {
            $gets .= $c . '=' . $value;
            $found = true;
        } else {
            if (is_array($v)) {
                $nv = "";
                foreach ($v as $vv) {
                    if (strlen($nv) > 0) {
                        $nv .= "&";
                    }
                    $nv .= $c . '[]=' . $vv;
                }
                $gets .= $nv;
            } else {
                $gets .= $c . '=' . $v;
            }
        }
    }
    if (!$found) {
        if (strlen($gets) > 0) {
            $gets .= '&';
        }
        $gets .= $par . '=' . $value;
    }
    //echo 'http://'.getenv("HTTP_HOST").'/services/directory/search_advanced3.php?'.$gets;
    echo '?' . $gets;
}

function changePage($p, $current, $total)
{
    $fp = $current + $p;
    urlWithParam('currentpage', $fp);
}

function changeOrder($value, $curr, $dir)
{
    if ($curr == $value) {
        if ($dir == 'ASC') {
            urlWithParam('sortdir', 'DESC');
        } else {
            urlWithParam('sortdir', 'ASC');
        }
    } else {
        urlWithParam('sort', $value);
    }
}

function makeImageLink($img)
{
    if (isset($img)) {
        echo '<a href="http://' . getenv("HTTP_HOST") . '/home/assets/images/archons/' . $img . '">link for image</a>';
    }
}


$filter_class = 'filter_open';
if($totalRows_rsSearch > 0){
    $filter_class = 'filter_close';
}

?>
    <!-- InstanceBegin template="/Templates/spp-redesign-archon.dwt.php" codeOutsideHTMLIsLocked="false" --><!DOCTYPE html>
                                                                                                            <!--
                                                                                                             ____________________________________________________________
                                                                                                            |                                                            |
                                                                                                            |      DATE + 2006.11.14                                     |
                                                                                                            | COPYRIGHT + Sigma Pi Phi 2006.  All Rights Reserved.       |
                                                                                                            |____________________________________________________________|

                                                                                                            -->
                                                                                                            <!--[if lt IE 7]>
                                                                                                            <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
                                                                                                            <!--[if IE 7]>
                                                                                                            <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
                                                                                                            <!--[if IE 8]>
                                                                                                            <html class="no-js lt-ie9"> <![endif]-->
                                                                                                            <!--[if gt IE 8]><!-->
    <html class="no-js"> <!--<![endif]-->
    <head>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Sigma Pi Phi Fraternity |</title>
        <!-- InstanceEndEditable -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/head-meta.php"); ?>
        <?php include($_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/head-css-main.php"); ?>
        <?php include($_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/head-javascript.php"); ?>
        <?php include($_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/head-links.php"); ?>

        <!-- InstanceBeginEditable name="head" -->
        <style type="text/css">
            <!--
            .style1 {
                font-weight: bold
            }

            -->
        </style>
        <!-- InstanceEndEditable -->
    </head>

    <body class="home blue-bar color-override-yellow-orange">
    <ul class="visuallyhidden focusable">
        <li><a href="#nav">Skip to navigation</a></li>
        <li><a href="#main">Skip to main content</a></li>
    </ul>
    <header>
        <?php include($_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/main-nav.php"); ?>
        <div class="width">
            <?php include($_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/global-logo-tagline.php"); ?>
        </div>
    </header>
    <!-- large image slider content toggle start -->
    <?php //include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/slider-content-section.php"); ?>
    <!-- large image slider content toggle end-->

    <!-- main content body start -->
    <div id="main" role="main">
        <div class="width padding">
            <!-- shows the content, pagetitle, breadcrumb and sidebar -->
            <!-- InstanceBeginEditable name="pagetitle" -->
            <section class="span-12 margin-bottom-medium">
                <div class="inner">
                    <h1>Membership Directory</h1>
                    <ol class="breadcrumbs" style="margin-top:-18px;">
                        <li><a class="B_homeCrumb" href="/home/archon-home.php"
                               title="Welcome to Sigma Pi Phi Fraternity">Archons</a></li>
                        <li><span class="B_currentCrumb">Administration</span></li>
                    </ol>
                    <div>
            </section>

            <section class="span-2 float-left">
                <div class="inner">


                    <nav id="nav">

                        <ul>
                            <li><a href="/home/2016-annual-dues-and-induction-schedule.php"
                                   title="2016 Annual Dues and Induction Schedule">2016 Annual Dues and Induction
                                    Schedule</a></li>
                            <li><a href="http://lexusboule.sigmapiphi.org/new/" title="Lexus - Boul&#233; Certificates">Lexus
                                    - Boul&#233; Certificates</a></li>
                            <li><a href="/home/basic-search-upd.php" title="Membership Directory">Basic Directory
                                    Search</a><!-- begin innerTpl -->
                                <ul>
                                    <!-- begin innerRowTpl -->
                                    <li><a href="/services/spp-directory/search_advanced4.php"
                                           title="Advanced Search"><i class="fa fa-chevron-right"></i>Advanced
                                            Search</a></li>
                                    <!-- end innerRowTpl -->

                                </ul>
                                <!-- end innerTpl --></li>
                            <li><a href="/home/our-boule-club.php" title="Our Boul� Club">Our Boul&#233; Club</a>
                                <!-- begin innerTpl -->
                                <ul>
                                    <!-- begin innerRowTpl -->
                                    <li><a href="/home/invite-archousa.php" title="Invite Your Archousa"><i
                                                    class="fa fa-chevron-right"></i>Invite Your Archousa</a></li>
                                    <!-- end innerRowTpl -->

                                </ul>
                                <!-- end innerTpl --></li>
                            <li class="last"><a href="/home/personal-profile.php" title="Personal Profile">Personal
                                    Profile</a><!-- begin innerTpl -->
                                <ul>
                                    <!-- begin innerRowTpl -->
                                    <li><a href="/home/change-password.php" title="Change Password"><i
                                                    class="fa fa-chevron-right"></i>Change Password</a></li>
                                    <!-- end innerRowTpl -->

                                </ul>
                                <!-- end innerTpl --></li>
                        </ul>

                    </nav>

                </div>
            </section>
            <!-- InstanceEndEditable -->

            <!-- InstanceBeginEditable name="content" -->

            <div class="width margin-bottom-large span-6 float-left as_wrap">
                <h1>Advanced Search</h1>
                <div class="as_filter <?php echo $filter_class;?>">
                    <h3>Search Filters</h3>
                    <table cellpadding="2">
                        <tr valign="top">
                            <td width="50%">
                                <form name="MX_Search_Form" method="GET" action="">

                                    <table cellpadding="2">
                                        <tr>
                                            <td width="125">
                                                <div align="left">Keywords:</div>
                                            </td>
                                            <td><input name="q" type="text" id="q" size="20" value=""/></td>
                                        </tr>
                                        <tr>
                                            <td width="125">
                                                <div align="left">State:</div>
                                            </td>
                                            <td><select name="state" id="state">
                                                    <option value="">Anywhere</option>
                                                    <?php
                                                    do {
                                                        //$extra = ((isset($_GET['state']) && $row_rsStates['st_cd'] == $_GET['state'])?'selected="selected"':'');
                                                        ?>
                                                        <option value="<?php echo $row_rsStates['st_cd'] ?>"><?php echo $row_rsStates['st_nm'] ?></option>
                                                        <?php
                                                    } while ($row_rsStates = mysqli_fetch_assoc($rsStates));
                                                    $rows = mysqli_num_rows($rsStates);
                                                    if ($rows > 0) {
                                                        mysqli_data_seek($rsStates, 0);
                                                        $row_rsStates = mysqli_fetch_assoc($rsStates);
                                                    }
                                                    ?>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td width="125">
                                                <div align="left">City:</div>
                                            </td>
                                            <td><input name="city" type="text" id="city" size="20" value=""/></td>
                                        </tr>
                                    </table>


                                    <table cellpadding="2">
                                        <tr>
                                            <td width="125">
                                                <div align="left">First Name:</div>
                                            </td>
                                            <td><input name="firstname" type="text" id="firstname" size="20" value=""/></td>
                                        </tr>
                                        <tr>
                                            <td width="125">
                                                <div align="left">Last Name:</div>
                                            </td>
                                            <td><input name="lastname" type="text" id="lastname" size="20" value=""/></td>
                                        </tr>
                                        <tr>
                                            <td width="125">
                                                <div align="left">Boul&eacute;:</div>
                                            </td>
                                            <td><select name="boule" id="boule">
                                                    <option value="">Anywhere</option>
                                                    <?php
                                                    do {
                                                        //$extra = ((isset($_GET['boule']) && $row_rsBoule['CHAPTERID'] == $_GET['boule'])?'selected="selected"':'');
                                                        ?>
                                                        <option value="<?php echo $row_rsBoule['CHAPTERID'] ?>"><?php echo $row_rsBoule['BOULENAME'] ?></option>
                                                        <?php
                                                    } while ($row_rsBoule = mysqli_fetch_assoc($rsBoule));
                                                    $rows = mysqli_num_rows($rsBoule);
                                                    if ($rows > 0) {
                                                        mysql_data_seek($rsBoule, 0);
                                                        $row_rsBoule = mysqli_fetch_assoc($rsBoule);
                                                    }
                                                    ?>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td width="125">
                                                <div align="left">Region:</div>
                                            </td>
                                            <td>
                                                <select name="region" id="region">
                                                    <option value="">Anywhere</option>
                                                    <?php
                                                    do {
                                                        //$extra = ((isset($_GET['region']) && $row_rsRegions['REGIONCD'] == $_GET['region'])?'selected="selected"':'');
                                                        ?>
                                                        <option value="<?php echo $row_rsRegions['REGIONCD'] ?>"><?php echo $row_rsRegions['REGIONNAME'] ?></option>
                                                        <?php
                                                    } while ($row_rsRegions = mysqli_fetch_assoc($rsRegions));
                                                    $rows = mysqli_num_rows($rsRegions);
                                                    if ($rows > 0) {
                                                        mysql_data_seek($rsRegions, 0);
                                                        $row_rsRegions = mysqli_fetch_assoc($rsRegions);
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="125">
                                                <div align="left">Office:</div>
                                            </td>
                                            <td>
                                                <select name="office[]" size="5" multiple="multiple" id="office">
                                                    <?php
                                                    while ($row_rsOfficers = mysqli_fetch_assoc($rsOfficers)) {
                                                        //$extra = ((isset($_GET['office']) && in_array($row_rsOfficers['OFFICERID'], $_GET['office']))?'selected="selected"':'');
                                                        ?>
                                                        <option value="<?php echo $row_rsOfficers['CPOSITION'] ?>"><?php echo $row_rsOfficers['CPOSITION'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>


                                    <table cellpadding="2">
                                        <tr>
                                            <td width="125">
                                                <div align="left">Title:</div>
                                            </td>
                                            <td><input name="title" type="text" id="title" size="20" value=""/></td>
                                        </tr>
                                        <tr>
                                            <td width="125">
                                                <div align="left">Company:</div>
                                            </td>
                                            <td><input name="company" type="text" id="company" size="20" value=""/></td>
                                        </tr>
                                        <tr>
                                            <td width="125">
                                                <div align="left">School:</div>
                                            </td>
                                            <td><input name="school" type="text" id="school" size="20" value=""/></td>
                                        </tr>
                                    </table>


                                    <table cellpadding="2">
                                        <tr>
                                            <td width="125" valign="top">
                                                <div align="left">Occupation:</div>
                                            </td>
                                            <td><select name="industry[]" size="5" multiple="multiple" id="industry">
                                                    <?php
                                                    while ($row_rsIndustry = mysqli_fetch_assoc($rsIndustry)) {
                                                        // $extra = ((isset($_GET['industry']) && in_array($row_rsIndustry['OCCUPATIONID'], $_GET['industry']))?'selected="selected"':'');
                                                        //$extra = ''
                                                        ?>
                                                        <option value="<?php echo $row_rsIndustry['DESCRIPTION'] ?>"><?php echo $row_rsIndustry['DESCRIPTION'] ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select></td>
                                        </tr>
                                    </table>

                            </td>
                        </tr>

                        <!-- ERROR MESSAGES -->

                        <?php
                        // Show IF Conditional region1
                        if (@$_GET['q'] == NULL) {
                            ?>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        <?php }
                        // endif Conditional region1

                        if (isset($_GET['q'])) {
                            //echo 'results '. $totalRows_rsSearch;
                            if ($hasErrors && !is_null($errorMsg)) echo '<tr><td><p class="msg error">' . $errorMsg . '</p></td>';
                            else if (($totalRows_rsSearch == 0)) { // Show if recordset empty
                                ?>
                                <tr>
                                    <td><p class="msg error">No records found matching your criteria!</p></td>
                                </tr>
                            <?php }
                        } // Show if recordset empty ?>

                        <!-- END ERROR MESSAGES -->

                        <tr>
                            <td>
                                <div align="center">
                                    <input type="submit" name="search" id="search" value="Search" class="button"/></div>

                                </form>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="as_result">
                    <?php
                    if ($totalRows_rsSearch > 0) {
                        ?>
                        <div id="search_therms" class="clearfix">
                            <strong>Searching For:</strong> &nbsp;
                            <span>
                                <?php
                                echo substr($searchpars_out, 0, -2);;
                                ?>
                            </span>
                        </div>
                        <h3>Search Results</h3>
                        <div class="result_top clearfix">

                            <div class="as_rtr">
                                Showing
                                <?php
                                $showingto = $currentpage * 25 + 25;
                                if ($showingto > $totalRows_rsSearch) {
                                    $showingto = $totalRows_rsSearch;
                                }
                                echo ($currentpage * 25 + 1) . '-' . $showingto . ' of ' . $totalRows_rsSearch;
                                ?>
                            </div>

                            <?php $search_orders = array(
                                'FIRSTNAME' => 'First Name',
                                'LASTNAME' => 'Last Name',
                                'ORGCD,LASTNAME'  => 'Boul&eacute;',
                                'CPOSITION' => 'Office',
                                'CITY'  => 'City',
                                'STATECD'  => 'State',
                            );
                            ?>
                            <div class="as_order">
                                <span>Order By:</span>
                                <select name="sort" id="sort">
                                    <?php
                                    foreach ($search_orders as $key=>$value) {
                                        $selected = '';
                                        if($sort == $key){
                                            $selected = ' selected=selected';
                                        }
                                        echo "<option value='{$key}' $selected>" . "{$value}</option>";
                                    };?>
                                </select>
                                <?php
                                $search_dirs = array(
                                    'ASC' => 'ASC',
                                    'DESC' => 'DESC'
                                );
                                if(empty($sortdir)){
                                    $sortdir = 'DESC';
                                }
                                ?>
                                <select name="sortdir" id="sortdir">
                                    <?php
                                    foreach ($search_dirs as $key=>$value) {
                                        $selected = '';
                                        if($sortdir == $key){
                                            $selected = ' selected=selected';
                                        }
                                        echo "<option value='{$key}' $selected>" . "{$value}</option>";
                                    };?>
                                </select>
                            </div>
                        </div>



                        <a name="#search-results"></a>
                        <div class="search_result">
                            <table cellspacing="4" cellpadding="4">
                            <tr style="display: none;">
                                <td colspan="2" style=" border-bottom: solid 1px #000000;">Click to sort on
                                    field: <a href="<?php changeOrder('FIRSTNAME', $sort, $sortdir) ?>">First
                                        Name</a> <a href="<?php changeOrder('LASTNAME', $sort, $sortdir) ?>">Last
                                        Name</a>,
                                    <a href="<?php changeOrder('ORGCD,LASTNAME', $sort, $sortdir) ?>">Boul&eacute;</a>,
                                    <a href="<?php changeOrder('CPOSITION', $sort, $sortdir) ?>">Office</a>, <a
                                            href="<?php changeOrder('CITY', $sort, $sortdir) ?>">City</a>, <a
                                            href="<?php changeOrder('STATECD', $sort, $sortdir) ?>">State</a>
                                </td>

                                <!--td bgcolor="#65A9DC"><span class="style1"><a href="<?php //changeOrder('OFFICERID', $sort, $sortdir) ?>">OFFICE</a></span></td-->
                            </tr>
                            <?php while ($row_rsArchons = mysqli_fetch_assoc($rsArchons)) { ?>
                                <tr valign="top">
                                    <td width="66%" style=" border-bottom: solid 1px #dfdfdf;"><a
                                                href="/services/spp-directory/search-results.php?id=<?php echo $row_rsArchons['WEB_ID'] ?>"><strong><?php echo $row_rsArchons['L2R_FULLNAME2']; ?> </strong></a><br>
                                        <span class="font-small"><?php echo $row_rsArchons['BOULENAME']; ?></span>
                                        <div style="display:none;"><?php
                                            // Show IF Conditional region2
                                            if (@$row_rsArchons['CPOSITION'] != NULL) {
                                                ?><span class="font-small">
                                                , <em><?php echo $row_rsArchons['CPOSITION']; ?></em></span>
                                            <?php }
                                            // endif Conditional region2
                                            ?></div>
                                    </td>
                                    <td style=" border-bottom: solid 1px #dfdfdf;"><span
                                                class="font-small"><?php echo $row_rsArchons['CITY']; ?>
                                            , <?php echo $row_rsArchons['STATECD']; ?></span><br>
                                        <span class="font-small"> <a
                                                    href="mailto:<?php echo $row_rsArchons['EMAIL']; ?>"><?php echo $row_rsArchons['EMAIL']; ?> </a></span><br>
                                        <span class="font-small">PHONE:<a
                                                    href="tel:+1<?php echo $row_rsArchons['HOMEPHONE']; ?>"><?php echo $row_rsArchons['HOMEPHONE']; ?></a></span>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        </div>
                        <p>&nbsp;

                            <?php if ($currentpage > 0) { ?>
                                <a href="<?php urlWithParam('currentpage', 0) ?>">First</a> | <a
                                        href="<?php changePage(-1, $currentpage, $totalRows_rsSearch) ?>">Previous</a>
                            <?php } else { ?>
                                First | Previous
                            <?php }
                            if ($currentpage < ceil($totalRows_rsSearch / 25) - 1) { ?>
                                <a href="<?php changePage(1, $currentpage, $totalRows_rsSearch) ?>">Next</a> |
                                <a href="<?php urlWithParam('currentpage', floor(($totalRows_rsSearch - 1) / 25)) ?>">Last</a>
                            <?php } else { ?>
                                Next | Last
                            <?php } ?>

                        </p>


                        <?php
                    }
                    ?>
                </div>


                <!-- InstanceEndEditable -->
                <!-- main content body end -->

                <!-- third section event and articles start -->
                <?php // include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/archon/section-content-events-articles.php"); ?>
                <!-- third section event and articles end -->

            </div>
            <div class="clearfix"></div>
            <footer class="width padding" style="border-top: 10px #52B609 solid;">
                <!-- 3 text content blocks -->
                <div class="row grid-12">
                    <?php include($_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/footer-content-default.php"); ?>
                </div>
            </footer>

                    <?php include($_SERVER['DOCUMENT_ROOT'] . "/services/Templates/archon/footer-scripts-main.php"); ?>

    </body>
    <!-- InstanceEnd --></html>
<?php
mysqli_free_result($rsIndustry);

mysqli_free_result($rsStates);

mysqli_free_result($rsBoule);

mysqli_free_result($rsRegions);
?>