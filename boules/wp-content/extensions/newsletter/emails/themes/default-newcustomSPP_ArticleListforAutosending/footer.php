<!-- Footer -->


<div style="text-align: center; margin: 10px 0 20px;"><a href="http://www.sigmapiphi.org"><img style="max-width: 100px; text-align:center" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/footer-logo.png" /><img style="max-width: 100px; text-align:center" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/footer-sphinx.png" /></a>
<p style="font-size:11px">Modeling leadership, education and civil responsibility by supporting these endeavors and by mentoring the young men who will advance the African American community in all facets of equality, mutual respect and devotion to democratic traditions.</p>
</div>

                            <?php include WP_CONTENT_DIR . '/extensions/newsletter/emails/themes/default-newcustomSPP_ArticleListforAutosending/social_main.php'; ?>
<div style="text-align: center; font-weight: bold; margin: 40px 0 10px;"><?php echo $theme_options['main_footer_title'] ?></div>
<div style="text-align: center; margin: 10px 0 20px;"><?php echo $theme_options['main_footer_contact'] ?></div>
<div style="text-align: center; color: #888; margin-top: 20px;"><?php echo $theme_options['main_footer_legal'] ?></div>
<div style="text-align: center; color: #888">To change your subscription, <a target="_blank"  href="{profile_url}">click here</a>.</div>