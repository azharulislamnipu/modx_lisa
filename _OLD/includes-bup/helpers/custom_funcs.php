<?php


/**
 * Test input data
 * @param $data
 * @return string
 */
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/**
 * @param $helper
 * @param $current
 * @param $echo
 * @param $type
 * @return string
 */
function __checked_selected_helper( $helper, $current, $echo, $type ) {
    if ( (string) $helper === (string) $current )
        $result = " $type='$type'";
    else
        $result = '';

    if ( $echo )
        echo $result;

    return $result;
}

function selected( $selected, $current = true, $echo = true ) {
    return __checked_selected_helper( $selected, $current, $echo, 'selected' );
}

function checked( $checked, $current = true, $echo = true ) {
    return __checked_selected_helper( $checked, $current, $echo, 'checked' );
}

if (!function_exists('array_column')) {
    function array_column($input, $column_key, $index_key = null) {
        $arr = array_map(function($d) use ($column_key, $index_key) {
            if (!isset($d[$column_key])) {
                return null;
            }
            if ($index_key !== null) {
                return array($d[$index_key] => $d[$column_key]);
            }
            return $d[$column_key];
        }, $input);

        if ($index_key !== null) {
            $tmp = array();
            foreach ($arr as $ar) {
                $tmp[key($ar)] = current($ar);
            }
            $arr = $tmp;
        }
        return $arr;
    }
}

function test_in_array(&$value, $key){
    if(is_array($value)){
        $value = join(',', $value);
    }
    $value = test_input($value);
}


/**
 * Combine two arrays and keep all values
 * @param $keys
 * @param $values
 * @return array
 */
function array_combine_($keys, $values) {
    $result = array();
    foreach ($keys as $i => $k) {
        $result[$k][] = $values[$i];
    }
    array_walk($result, create_function('&$v', '$v = (count($v) == 1)? array_pop($v): $v;'));
    return $result;
}


/*Get Ip address*/
function getip(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = test_input($_SERVER['HTTP_CLIENT_IP']);
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = test_input($_SERVER['HTTP_X_FORWARDED_FOR']);
    } else {
        $ip = test_input($_SERVER['REMOTE_ADDR']);
        if ($ip == '::1') {
            $ip = '127.0.0.1';
        }
    }
    return $ip;
}

function create_suffix_prefix_from_array($arr){
    ob_start();
    foreach ($arr as $item){
        echo "<option value='$item'>$item</option>";
    }
    return ob_get_clean();
}