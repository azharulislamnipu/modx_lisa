<?php get_header(); ?>

	<div id="content">
		<div class="padder">

			<?php do_action( 'sigma_before_attachment' ); ?>

			<div class="page" id="attachments-page" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php do_action( 'sigma_before_blog_post' ); ?>

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<div class="post-content">

							<?php if ( can_view_content() ): ?>

								<h2 class="purple color"><?php the_title(); ?></h2>
								<div class="contentBlock">

									<p class="date">
										<?php the_date(); ?>
										<span class="post-utility alignright"><?php edit_post_link( __( 'Edit this entry', 'archon-network' ) ); ?></span>
									</p>

									<div class="entry">
										<?php echo wp_get_attachment_image( $post->ID, 'large', false, array( 'class' => 'size-large aligncenter' ) ); ?>

										<div class="entry-caption"><?php if ( !empty( $post->post_excerpt ) ) the_excerpt(); ?></div>
										<?php the_content(); ?>
									</div>

									<p class="postmetadata">
										<?php
											if ( wp_attachment_is_image() ) :
												$metadata = wp_get_attachment_metadata();
												printf( __( 'Full size is %s pixels', 'archon-network' ),
													sprintf( '<a href="%1$s" title="%2$s">%3$s &times; %4$s</a>',
														wp_get_attachment_url(),
														esc_attr( __( 'Link to full size image', 'archon-network' ) ),
														$metadata['width'],
														$metadata['height']
													)
												);
											endif;
										?>
										&nbsp;
									</p>

								</div>

							<?php else: ?>
								<h2 class="purple color"><?php _e( 'Unauthorized', 'archon-network' ); ?></h2>
								<div class="contentBlock">
									<div class="entry">
										<p><?php _e( 'You are not allowed to view this content.', 'archon-network' ); ?></p>
									</div>
								</div>

							<?php endif; ?>

						</div>

					</div>

					<?php do_action( 'sigma_after_blog_post' ); ?>

					<?php if ( can_view_boule_only() ): ?>
						<?php comments_template(); ?>
					<?php endif; ?>

				<?php endwhile; else: ?>

					<p><?php _e( 'Sorry, no attachments matched your criteria.', 'archon-network' ); ?></p>

				<?php endif; ?>

			</div>

		<?php do_action( 'sigma_after_attachment' ); ?>

		</div><!-- .padder -->
	</div><!-- #content -->

	<?php get_sidebar(); ?>

<?php get_footer(); ?>