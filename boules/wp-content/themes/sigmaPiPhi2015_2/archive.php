<?php get_header(); ?>

	<div id="content">
		<div class="padder width padding-top-none-20">

		<?php do_action( 'sigma_before_archive' ) ?>
		
		 <div class="row grid-12">

		<div class="page" id="blog-archives">
	   	 <div class="span-9 float-right">
	   		 <div class="inner">

			<?php
				$category = &get_category(get_query_var('cat'));
				$color = strpos('notices|documents-resources|press-releases', $category->slug) === false ? 'red' : $category->slug;
			?>


			<h2 class="color <?php echo $color ?>"><?php echo wp_title( false, false )  ?></h2>
			<div class="contentBlock">

			<?php // Catch users that shouldn't be viewing this page
			if( is_private_category( $category ) && !can_view_boule_only() ): ?>
				<p>You must be a chapter member to view this page.</p>
			</div>
			<?php else:

				// Get the category breadcrumbs
				$splitter = '@@@';
				$parents = preg_split('/'.$splitter.'/', get_category_parents($category->cat_ID, TRUE, $splitter), NULL, PREG_SPLIT_NO_EMPTY);
				$count = 0;
				$size = sizeof($parents);

				// Do we have at least one breadcrumb?
				if($size > 1): ?>
					<p class="breadcrumbs">
					<?php foreach($parents as $parent):
						if(++$count <= $size) :?>
							<?php echo $parent ?>
							<?php if($count + 1 <= $size):?>
								<span class="sep">&gt;</span>
							<?php endif; ?>
					<?php endif;
					endforeach; ?>
					</p>
				<?php endif; ?>

				<?php // Get the category description
				$category_description = category_description();
				if ( ! empty( $category_description ) ): ?>
					<?php echo $category_description . '' ?>
				<?php endif; ?>

				<ul class="cat-children">
					<?php // Get any child categories
					wp_list_categories(array(
						'child_of' 			=> $category->cat_ID,
						'depth'				=> 1,
						'title_li'  		=> '',
						'show_option_none' 	=> '',
					)); ?>
				</ul>

				<?php if ( have_posts() ) : ?>


					<ul class="<?php echo get_query_var('paged') ?> link-list">
					<?php while (have_posts()) : the_post();

						// Determine if the post should be shown
						if ( can_view_content() ): ?>
						

							<li>
									<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) {
									  echo '<div class="imgleft"><a href="';
									  echo the_permalink();
									  echo '">';
									  echo the_post_thumbnail($post->ID, 'blog-med');
									   echo '</a></div>';
									} else {
										//if(main_image() !== '') { 
	  									  echo '<div class="imgleft"><a href="';
	  									  echo the_permalink();
	  									  echo '">';
									   echo get_first_image();
									  echo '</a></div>';
									   //}
									} ?>

					
							<div class="descleft">
							<a href="<?php the_permalink(); ?>">
									<!-- <?php //if ( has_post_thumbnail() ) : ?>
									    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
											<?php //echo get_the_post_thumbnail($post->blog_id, 'post-thumbnail'); ?>
									    </a>
									<?php //endif; ?> -->
									<h3><?php the_title(); ?></h3>
								</a>
								<span class="icon archive"><?php echo get_the_date(); ?> | <?php echo "By: ".get_the_author(); ?> | <?php _e( 'Posted in', 'archon-network' ) ?> <?php the_category(', ') ?> </span> 
									<p class="excerpt"><?php //echo get_the_excerpt('150'); ?>
										
									<?php echo wpse_custom_excerpts('100'); ?></p>
								
								
								<?php $key_1_value = get_post_meta( get_the_ID(), 'teasertext', true ); ?>
								<p class="readmore"><strong><a href="<?php echo the_permalink(); ?>"><?php
								if (!empty($key_1_value)) {
									echo $key_1_value;
								} else {
								echo 'Read More &raquo;';	
								}
								?></a></strong></p>
								</div>
							</li>

						<?php endif; ?>
					<?php endwhile; ?>
					</ul>

					<div class="navigation">
						<?php kriesi_pagination();?>

						<!-- <div class="alignleft"><?php //next_posts_link( __( '&larr; Previous Entries', 'archon-network' ) ) ?></div>
						<div class="alignright"><?php //previous_posts_link( __( 'Next Entries &rarr;', 'archon-network' ) ) ?></div> -->

					</div>

				<?php else : ?>

					<p><?php _e( 'No content found', 'archon-network' ) ?></p>
					<?php // locate_template( array( 'searchform.php' ), true ) ?>

				<?php endif; ?>
				</div>
				<div class="contentBlockFooter">
					<!-- <?php //if(!empty($category)): ?>
						<a class="icon rss" href="<?php //echo get_bloginfo('url') ?>/feed/?cat=<?php //echo $category->cat_ID ?>"><?php //_e('RSS', 'archon-network')?></a>
					<?php //endif; ?> -->
				</div>

			<?php endif; ?>
			
				</div><!--inner-->
			</div><!--span-9-->

				 <div class="span-3 float-left">
					 <div class="inner">
						 <?php get_sidebar(); ?>
					</div><!-- .inner -->
				</div><!-- .span-3 -->

		</div>
		
		</div><!-- .grid-12 -->

		<?php do_action( 'sigma_after_archive' ) ?>

		</div><!-- .padder -->
	</div><!-- #content -->

	<?php locate_template( array( 'sidebar.php' ), true ) ?>

<?php get_footer(); ?>