<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("FIRSTNAME", true, "text", "", "", "", "");
$formValidation->addField("LASTNAME", true, "text", "", "", "", "");
$formValidation->addField("ADDRESS1", true, "text", "", "", "", "");
$formValidation->addField("CITY", true, "text", "", "", "", "");
$formValidation->addField("STATECD", true, "text", "", "", "", "");
$formValidation->addField("ZIP", true, "text", "", "", "", "");
$formValidation->addField("HOMEPHONE", true, "text", "", "", "", "");
$formValidation->addField("EMAIL", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsPrefix = "SELECT * FROM spp_sal ORDER BY `PREFIX` ASC";
$rsPrefix = mysql_query($query_rsPrefix, $sigma_modx) or die(mysql_error());
$row_rsPrefix = mysql_fetch_assoc($rsPrefix);
$totalRows_rsPrefix = mysql_num_rows($rsPrefix);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsSuffix = "SELECT * FROM spp_suffix ORDER BY SUFFIX ASC";
$rsSuffix = mysql_query($query_rsSuffix, $sigma_modx) or die(mysql_error());
$row_rsSuffix = mysql_fetch_assoc($rsSuffix);
$totalRows_rsSuffix = mysql_num_rows($rsSuffix);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsState = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsState = mysql_query($query_rsState, $sigma_modx) or die(mysql_error());
$row_rsState = mysql_fetch_assoc($rsState);
$totalRows_rsState = mysql_num_rows($rsState);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsHon = "SELECT * FROM spp_hon ORDER BY DESIGNATIONLST ASC";
$rsHon = mysql_query($query_rsHon, $sigma_modx) or die(mysql_error());
$row_rsHon = mysql_fetch_assoc($rsHon);
$totalRows_rsHon = mysql_num_rows($rsHon);

// Make an insert transaction instance
$ins_spp_archons = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_archons);
// Register triggers
$return = !empty( $_POST['RETURN'] ) ? $_POST['RETURN'] : '/home/personal-profile.php';
$ins_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$ins_spp_archons->setTable("spp_archons");
$ins_spp_archons->addColumn("ADDRESS2", "STRING_TYPE", "POST", "ADDRESS2");
$ins_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "LASTUPDATED", "{NOW_DT}");
$ins_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY", "{POST.UPDATEDBY}");
$ins_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_archons = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archons);
// Register triggers
$upd_spp_archons->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archons->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archons->registerTrigger("END", "Trigger_Default_Redirect", 99, $return);
// Add columns
$upd_spp_archons->setTable("spp_archons");
$upd_spp_archons->addColumn("PREFIX", "STRING_TYPE", "POST", "PREFIX");
$upd_spp_archons->addColumn("FIRSTNAME", "STRING_TYPE", "POST", "FIRSTNAME");
$upd_spp_archons->addColumn("MIDDLEINITIAL", "STRING_TYPE", "POST", "MIDDLEINITIAL");
$upd_spp_archons->addColumn("LASTNAME", "STRING_TYPE", "POST", "LASTNAME");
$upd_spp_archons->addColumn("SUFFIX", "STRING_TYPE", "POST", "SUFFIX");
$upd_spp_archons->addColumn("DESIGNATIONLST", "STRING_TYPE", "POST", "DESIGNATIONLST");
$upd_spp_archons->addColumn("ADDRESS1", "STRING_TYPE", "POST", "ADDRESS1");
$upd_spp_archons->addColumn("ADDRESS2", "STRING_TYPE", "POST", "ADDRESS2");
$upd_spp_archons->addColumn("CITY", "STRING_TYPE", "POST", "CITY");
$upd_spp_archons->addColumn("STATECD", "STRING_TYPE", "POST", "STATECD");
$upd_spp_archons->addColumn("ZIP", "STRING_TYPE", "POST", "ZIP");
$upd_spp_archons->addColumn("HOMEPHONE", "STRING_TYPE", "POST", "HOMEPHONE");
$upd_spp_archons->addColumn("MOBILEPHONE", "STRING_TYPE", "POST", "MOBILEPHONE");
$upd_spp_archons->addColumn("EMAIL", "STRING_TYPE", "POST", "EMAIL");
$upd_spp_archons->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "{NOW_DT}");
$upd_spp_archons->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "{POST.UPDATEDBY}");
$upd_spp_archons->setPrimaryKey("CUSTOMERID", "NUMERIC_TYPE", "POST", "CUSTOMERID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archons = $tNGs->getRecordset("spp_archons");
$row_rsspp_archons = mysql_fetch_assoc($rsspp_archons);
$totalRows_rsspp_archons = mysql_num_rows($rsspp_archons);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/JSRecordset.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/BaseListSorter.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MenuMover.js"></script>
<?php
//begin JSRecordset
$jsObject_rsHon = new WDG_JsRecordset("rsHon");
echo $jsObject_rsHon->getOutput();
//end JSRecordset
?>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<h1 class="yellow">EDIT CONTACT INFORMATION</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <div class="KT_tng">

    <div class="KT_tngform">
      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php
// Show IF Conditional region1
if (@$totalRows_rsspp_archons > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php }
// endif Conditional region1
?>
          <table cellpadding="2" cellspacing="0" class="KT_tngtable">
            <?php
// Show IF Conditional show_PREFIX_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="PREFIX_<?php echo $cnt1; ?>">PREFIX:</label></td>
                <td><select name="PREFIX_<?php echo $cnt1; ?>" id="PREFIX_<?php echo $cnt1; ?>">
                    <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                    <?php
do {
?>
                    <option value="<?php echo $row_rsPrefix['PREFIX']?>"<?php if (!(strcmp($row_rsPrefix['PREFIX'], $row_rsspp_archons['PREFIX']))) {echo "SELECTED";} ?>><?php echo $row_rsPrefix['PREFIX']?></option>
                    <?php
} while ($row_rsPrefix = mysql_fetch_assoc($rsPrefix));
  $rows = mysql_num_rows($rsPrefix);
  if($rows > 0) {
      mysql_data_seek($rsPrefix, 0);
	  $row_rsPrefix = mysql_fetch_assoc($rsPrefix);
  }
?>
                  </select>
                    <?php echo $tNGs->displayFieldError("spp_archons", "PREFIX", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_PREFIX_on_update_only
?>
            <?php
// Show IF Conditional show_FIRSTNAME_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="FIRSTNAME_<?php echo $cnt1; ?>">FIRST NAME:</label></td>
                <td><input type="text" name="FIRSTNAME_<?php echo $cnt1; ?>" id="FIRSTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['FIRSTNAME']); ?>" size="32" maxlength="40" />
                    <?php echo $tNGs->displayFieldHint("FIRSTNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "FIRSTNAME", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_FIRSTNAME_on_update_only
?>
            <?php
// Show IF Conditional show_MIDDLEINITIAL_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="MIDDLEINITIAL_<?php echo $cnt1; ?>">MIDDLE INITIAL:</label></td>
                <td><input type="text" name="MIDDLEINITIAL_<?php echo $cnt1; ?>" id="MIDDLEINITIAL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['MIDDLEINITIAL']); ?>" size="32" maxlength="40" />
                    <?php echo $tNGs->displayFieldHint("MIDDLEINITIAL");?> <?php echo $tNGs->displayFieldError("spp_archons", "MIDDLEINITIAL", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_MIDDLEINITIAL_on_update_only
?>
            <?php
// Show IF Conditional show_LASTNAME_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="LASTNAME_<?php echo $cnt1; ?>">LAST NAME:</label></td>
                <td><input type="text" name="LASTNAME_<?php echo $cnt1; ?>" id="LASTNAME_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['LASTNAME']); ?>" size="32" maxlength="40" />
                    <?php echo $tNGs->displayFieldHint("LASTNAME");?> <?php echo $tNGs->displayFieldError("spp_archons", "LASTNAME", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_LASTNAME_on_update_only
?>
            <?php
// Show IF Conditional show_SUFFIX_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="SUFFIX_<?php echo $cnt1; ?>">SUFFIX:</label></td>
                <td><select name="SUFFIX_<?php echo $cnt1; ?>" id="SUFFIX_<?php echo $cnt1; ?>">
                    <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                    <?php
do {
?>
                    <option value="<?php echo $row_rsSuffix['SUFFIX']?>"<?php if (!(strcmp($row_rsSuffix['SUFFIX'], $row_rsspp_archons['SUFFIX']))) {echo "SELECTED";} ?>><?php echo $row_rsSuffix['SUFFIX']?></option>
                    <?php
} while ($row_rsSuffix = mysql_fetch_assoc($rsSuffix));
  $rows = mysql_num_rows($rsSuffix);
  if($rows > 0) {
      mysql_data_seek($rsSuffix, 0);
	  $row_rsSuffix = mysql_fetch_assoc($rsSuffix);
  }
?>
                  </select>
                    <?php echo $tNGs->displayFieldError("spp_archons", "SUFFIX", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_SUFFIX_on_update_only
?>
            <?php
// Show IF Conditional show_DESIGNATIONLST_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="DESIGNATIONLST_<?php echo $cnt1; ?>">HONORIFIC:</label></td>
                <td><input name="DESIGNATIONLST_<?php echo $cnt1; ?>" id="DESIGNATIONLST_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['DESIGNATIONLST']); ?>" size="10" maxlength="40" wdg:recordset="rsHon" wdg:subtype="MenuMover" wdg:type="widget" wdg:displayfield="DESIGNATIONLST" wdg:valuefield="DESIGNATIONLST" wdg:sortselector="yes" />
                    <?php echo $tNGs->displayFieldHint("DESIGNATIONLST");?> <?php echo $tNGs->displayFieldError("spp_archons", "DESIGNATIONLST", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_DESIGNATIONLST_on_update_only
?>
            <?php
// Show IF Conditional show_ADDRESS1_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="ADDRESS1_<?php echo $cnt1; ?>">ADDRESS 1:</label></td>
                <td><input type="text" name="ADDRESS1_<?php echo $cnt1; ?>" id="ADDRESS1_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ADDRESS1']); ?>" size="32" maxlength="40" />
                    <?php echo $tNGs->displayFieldHint("ADDRESS1");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS1", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_ADDRESS1_on_update_only
?>
            <tr>
              <td class="KT_th"><label for="ADDRESS2_<?php echo $cnt1; ?>">ADDRESS 2:</label></td>
              <td><input type="text" name="ADDRESS2_<?php echo $cnt1; ?>" id="ADDRESS2_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ADDRESS2']); ?>" size="32" maxlength="40" />
                  <?php echo $tNGs->displayFieldHint("ADDRESS2");?> <?php echo $tNGs->displayFieldError("spp_archons", "ADDRESS2", $cnt1); ?> </td>
            </tr>
            <?php
// Show IF Conditional show_CITY_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="CITY_<?php echo $cnt1; ?>">CITY:</label></td>
                <td><input type="text" name="CITY_<?php echo $cnt1; ?>" id="CITY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['CITY']); ?>" size="32" maxlength="40" />
                    <?php echo $tNGs->displayFieldHint("CITY");?> <?php echo $tNGs->displayFieldError("spp_archons", "CITY", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_CITY_on_update_only
?>
            <?php
// Show IF Conditional show_STATECD_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="STATECD_<?php echo $cnt1; ?>">STATE:</label></td>
                <td><select name="STATECD_<?php echo $cnt1; ?>" id="STATECD_<?php echo $cnt1; ?>">
                    <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                    <?php
do {
?>
                    <option value="<?php echo $row_rsState['st_cd']?>"<?php if (!(strcmp($row_rsState['st_cd'], $row_rsspp_archons['STATECD']))) {echo "SELECTED";} ?>><?php echo $row_rsState['st_nm']?></option>
                    <?php
} while ($row_rsState = mysql_fetch_assoc($rsState));
  $rows = mysql_num_rows($rsState);
  if($rows > 0) {
      mysql_data_seek($rsState, 0);
	  $row_rsState = mysql_fetch_assoc($rsState);
  }
?>
                  </select>
                    <?php echo $tNGs->displayFieldError("spp_archons", "STATECD", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_STATECD_on_update_only
?>
            <?php
// Show IF Conditional show_ZIP_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="ZIP_<?php echo $cnt1; ?>">ZIP:</label></td>
                <td><input type="text" name="ZIP_<?php echo $cnt1; ?>" id="ZIP_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['ZIP']); ?>" size="32" maxlength="40" />
                    <?php echo $tNGs->displayFieldHint("ZIP");?> <?php echo $tNGs->displayFieldError("spp_archons", "ZIP", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_ZIP_on_update_only
?>
            <?php
// Show IF Conditional show_HOMEPHONE_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="HOMEPHONE_<?php echo $cnt1; ?>">HOME PHONE:</label></td>
                <td><input type="text" name="HOMEPHONE_<?php echo $cnt1; ?>" id="HOMEPHONE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['HOMEPHONE']); ?>" size="32" maxlength="40" />
                    <?php echo $tNGs->displayFieldHint("HOMEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "HOMEPHONE", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_HOMEPHONE_on_update_only
?>
            <?php
// Show IF Conditional show_MOBILEPHONE_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="MOBILEPHONE_<?php echo $cnt1; ?>">MOBILE PHONE:</label></td>
                <td><input type="text" name="MOBILEPHONE_<?php echo $cnt1; ?>" id="MOBILEPHONE_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['MOBILEPHONE']); ?>" size="32" maxlength="40" />
                    <?php echo $tNGs->displayFieldHint("MOBILEPHONE");?> <?php echo $tNGs->displayFieldError("spp_archons", "MOBILEPHONE", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_MOBILEPHONE_on_update_only
?>
            <?php
// Show IF Conditional show_EMAIL_on_update_only
if (@$_POST['CUSTOMERID'] != "") {
?>
              <tr>
                <td class="KT_th"><label for="EMAIL_<?php echo $cnt1; ?>">CONTACT EMAIL:</label></td>
                <td><input type="text" name="EMAIL_<?php echo $cnt1; ?>" id="EMAIL_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['EMAIL']); ?>" size="32" maxlength="125" />
                    <?php echo $tNGs->displayFieldHint("EMAIL");?> <?php echo $tNGs->displayFieldError("spp_archons", "EMAIL", $cnt1); ?> </td>
              </tr>
              <?php }
// endif Conditional show_EMAIL_on_update_only
?>
          </table>
          <input type="hidden" name="kt_pk_spp_archons_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_archons['kt_pk_spp_archons']); ?>" />
          <input type="hidden" name="LASTUPDATED_<?php echo $cnt1; ?>" id="LASTUPDATED_<?php echo $cnt1; ?>" value="<?php echo KT_formatDate($row_rsspp_archons['LASTUPDATED']); ?>" />
          <input type="hidden" name="UPDATEDBY_<?php echo $cnt1; ?>" id="UPDATEDBY_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_archons['UPDATEDBY']); ?>" />
          <input type="hidden" name="RETURN" id="RETURN" value="<?php echo KT_escapeAttribute($_POST['RETURN']); ?>" />
          <?php } while ($row_rsspp_archons = mysql_fetch_assoc($rsspp_archons)); ?>
        <div class="KT_bottombuttons">
          <div>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
<input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '<?php echo $return ?>')" />
          </div>
        </div>
      </form>
    </div>
    <br class="clearfixplain" />
  </div>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsPrefix);

mysql_free_result($rsSuffix);

mysql_free_result($rsState);

mysql_free_result($rsHon);
?>
