<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
  //GALLERY DEFAULT SETTINGS
  
  // image sort order
  define ('TV_SORT_ORDER', 'dragndrop');
  // default title for new galleries
  define ('TV_GALLERY_TITLE', 'New TiltViewer Gallery');
  // Default Flickr gallery or xml gallery
  define ('TV_USE_FLICKR', false);
  // Default Flickr User ID
  define ('TV_FLICKR_USER_ID', '00000000@N00');
  // Default Flickr image tag
  define ('TV_FLICKR_TAGS', '');
  // Default Flickr tag mode
  define ('TV_FLICKR_TAG_MODE', 'any');
  // Default to show taken by text on Flickr images
  define ('TV_FLICKR_SHOW_TAKEN_BY_TEXT', false);
  // default title for images
  define ('TV_IMAGE_TITLE', '');
  // default image description
  define ('TV_IMAGE_DESCRIPTION', '');
  // use reload button under images
  define ('TV_USE_RELOAD_BUTTON', true);
  // display 'flip' button on zoomed-in image
  define ('TV_SHOW_FLIP_BUTTON', true);
  // not implemented
  define ('TV_SHOW_LINK_BUTTON', true);
  // number of image columns
  define ('TV_COLUMNS', 5);
  // number of image rows
  define ('TV_ROWS', 5);
  // url for flipside link
  define ('TV_LINK_URL', '');
  // text for flipside link
  define ('TV_LINK_LABEL', 'More information');
  // colour of gallery frame
  define ('TV_FRAME_COLOR', '0xFFFFFF');
  // background colour
  define ('TV_BACK_COLOR', '0xFFFF00');
  // background gradient center colour
  define ('TV_BKGND_INNER_COLOR', '0x333333');
  // background gradient edge color
  define ('TV_BKGND_OUTER_COLOR', '0x000000');
  // maximum image size in pixels
  define ('TV_MAX_JPG_SIZE', 500);
  // language options
  define ('LANG_GO_FULL', 'Go Fullscreen');
	define('LANG_EXIT_FULL', 'Exit Fullscreen');
	define('LANG_ABOUT', 'About');
  
  // Pro Options
  
  // hide radial gradient background
  define ('TV_BKGND_TRANSPARENT', false);
  // show the 'Go Fullscreen' right-click option
  define ('TV_SHOW_FULLSCREEN_OPTION', true);
  // frame width in pixels
  define ('TV_FRAME_WIDTH', 40);
  // camera distance zoomed-in
  define ('TV_ZOOMED_IN_DISTANCE', 1400);
  // camera distance zoomed-out
  define ('TV_ZOOMED_OUT_DISTANCE', 7500);
  // font for flipside text
  define ('TV_FONT_NAME', 'Arial');
  // font size for flipside text
  define ('TV_TITLE_FONT_SIZE', 90);
  // font size for description text
  define ('TV_DESCRIPTION_FONT_SIZE', 32);
  // font size for link text
  define ('TV_LINK_FONT_SIZE', 41);
  // window or frame target to open links
  define ('TV_LINK_TARGET', '_blank');
  // color for reload or next/back buttons
  define ('TV_NAV_BUTTON_COLOR', '0xFFFFFF');
  // color for flip buttons
  define ('TV_FLIP_BUTTON_COLOR', '0xFFFFFF');
  // colour of flipside text
  define ('TV_TEXT_COLOR', '0x000000');
  // color of flipside link text
  define ('TV_LINK_TEXT_COLOR', '0xFFFFFF');
  // color of flipside link text background
  define ('TV_LINK_BKGND_COLOR', '0x000000');
  // enable click sounds
  define ('TV_ENABLE_SOUNDS', true);
  // max tilt (degrees) when zoomed-in
  define ('TV_TILT_AMOUNT_IN', 75);
  // max tilt (degrees) when zoomed-out
  define ('TV_TILT_AMOUNT_OUT', 120);
  
  // OTHER SETTINGS
  // Specify type of urls to be stored in the gallery xml for new galleries
  // Options are 'RELATIVE' (default) for relative urls (images/myimage.jpg)
  // or 'ABSOLUTE' for absolute urls (/svmanager/g1/images/myimage.jpg)
  // or 'HTTP' for http urls (http://www.myserver.com/svmanager/g1/images/myimage.jpg)
  define('TV_URL_TYPE', 'RELATIVE');
  // url of xml file relative to gallery
  define('TV_XML_PATH_REL_GALLERY', 'gallery.xml');
  define('TV_XML_SETTINGS_TAG', 'tiltviewergallery');
  define('TV_MASTER_PATH', 'plugins'.DIRECTORY_SEPARATOR.'tiltviewer'.DIRECTORY_SEPARATOR.'tvmaster'.DIRECTORY_SEPARATOR);
  define('TV_MASTER_FILECOUNT', 4);
  define('TV_DEFAULT_THUMB_PATH', 'thumbs'.DIRECTORY_SEPARATOR);
  define('TV_DEFAULT_IMAGE_PATH', 'images'.DIRECTORY_SEPARATOR);
  define('TV_THUMB_WIDTH', 65);
  define('TV_THUMB_HEIGHT', 65);
  define('TV_THUMB_QUALITY', 85);
  define('TV_CREATE_NEW', true);
?>