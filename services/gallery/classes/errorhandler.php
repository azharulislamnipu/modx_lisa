<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * Error handler class
 *
 * @package svManager
 */
Class ErrorHandler
{
  /**
   * @var array Warning messages that do not stop execution are stored here
   * One warning message per array element
   */
   var $messages=array();
   
  /**
   * Constructs ErrorHandler
   *
   * $this must be passed by reference as below
   * Also $errorhandle = new ErrorHandler() when the class is instantiated
   */
  function ErrorHandler()
  {
    if (DEBUG) return;
    set_error_handler(array(&$this, 'handleError'));
  }
   
  /**
   * Custom error handler
   *
   * Errors suppressed with @ are not dealt with
   * Errors in auth.php are dealt with even if the error level is 0
   * @return boolean true will suppress normal error messages
   * @param int error level
   * @param string error message
   * @param string php script where error occurred
   * @param int line number in php script
   * @param array global variables
   */
  function handleError($errLevel, $errorMessage, $errFile, $errLine, $errContext)
  {
    switch($errLevel)
    {
      case E_USER_NOTICE :
        $this->messages[] = 'Notice: '.$errorMessage;
        break;
      case E_NOTICE :
        if (error_reporting() !=0)
        {
          $this->messages[] = 'Notice: '.$errorMessage;
        }
        break;
      case E_USER_WARNING :
        $this->messages[] = 'Warning: '.$errorMessage.' ('.basename($errFile).', line '.$errLine.')';
        break;
      case E_WARNING :
        if (error_reporting() !=0)
        {
          $this->messages[] = 'Warning: '.$errorMessage.' ('.basename($errFile).', line '.$errLine.')';
        }
        break;
      default :
        if ((error_reporting() == 0) && (basename($errFile) != 'auth.php')) return !DEBUG;
        //ob_end_clean();
        print '<p class="error">Error '.$errorMessage.' ('.basename($errFile).', line '.$errLine.')</p>';
        print '
        </div>
  <div id="footer">
    <p>&copy; 2011 Airtight Interactive</p> 
  </div>
  </div>
</body>
</html>';
        exit;
    }
    return !DEBUG;
  }

 /**
  * returns user messages
  *
  * @access public
  * @returns string
  */
  function getMessages()
  {
    if (count ($this->messages) == 0) return '';
    $messageHtml = '<ol class="messages">';
    foreach ($this->messages as $message)
    {
      $messageHtml .= '<li class="warning">'.$message.'</li>';
    }
    $messageHtml .= '</ol>';    
    return $messageHtml;
  }
}
?>