<?php
error_reporting(6143);
$main_path = str_replace('includes/process-form', '',__DIR__);
require "../functions.php";

$user_id = (!empty($_POST['user_id'])) ? test_input($_POST['user_id']) : '';
$spouse_id = (!empty($_POST['SPOUSEID'])) ? test_input($_POST['SPOUSEID']) : '';
$archousa_id = (!empty($_POST['ARCHOUSAID'])) ? test_input($_POST['ARCHOUSAID']) : '';
$updated_by = (!empty($_POST['UPDATEDBY'])) ? test_input($_POST['UPDATEDBY']) : '';

if(!empty($_POST['extra_val'])){
    $extra_val = str_replace("\\", "",$_POST['extra_val']);
    $extra_val = json_decode($extra_val);
}
$action = (!empty($_POST['action'])) ? test_input($_POST['action']) : '';

$ip = getip();


$pform_dir = $include_path . '/process-form/';

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || empty($user_id) || empty($action)) {
    $response['type'] = 'error';
    $response['err_msg'] = "Not valid request";
    die(json_encode($response));  //try detect AJAX request, simply exist if no Ajax
}

$naction = preg_replace("/\d+$/", '', $action);

switch ($naction){
    case("archon_image_upload") :
        include $pform_dir . "archons/archon_image_upload.php";
        break;
    case("archon_edit_contact") :
        include $pform_dir . "archons/archon_edit_contact.php";
        break;
    case("archon_edit_bio") :
        include $pform_dir . "archons/archon_edit_bio.php";
        break;
    case "archon_edit_carrer_":
        include $pform_dir . "archons/archon_edit_carrer.php";
        break;
    case "archon_edit_educ_":
        include $pform_dir . "archons/archon_edit_education.php";
        break;
    case "archon_edit_children_":
        include $pform_dir . "archons/archon_edit_children.php";
        break;
    case "delete_career":
        $prof_id = $_POST['delete_id'];
        include $pform_dir . "archons/archon_delete_career.php";
        break;
    case "delete_educ":
        $educ_id = $_POST['delete_id'];
        include $pform_dir . "archons/archon_delete_education.php";
        break;
    case "delete_children":
        $prof_id = $_POST['delete_id'];
        include $pform_dir . "archons/archon_delete_children.php";
        break;
}