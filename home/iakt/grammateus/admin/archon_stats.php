<?php
//Connection statement
require_once('../../Connections/sigma_modx.php');

//Aditional Functions
require_once('../../includes/functions.inc.php');

// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the required classes
require_once('../../includes/tfi/TFI.php');
require_once('../../includes/tso/TSO.php');
require_once('../../includes/nav/NAV.php');

// Filter
$tfi_listrsStatusTotals1 = new TFI_TableFilter($sigma_modx, "tfi_listrsStatusTotals1");
$tfi_listrsStatusTotals1->addColumn("STATUS", "STRING_TYPE", "STATUS", "%");
$tfi_listrsStatusTotals1->addColumn("TOTAL", "NUMERIC_TYPE", "TOTAL", "=");
$tfi_listrsStatusTotals1->Execute();

// Sorter
$tso_listrsStatusTotals1 = new TSO_TableSorter("rsStatusTotals", "tso_listrsStatusTotals1");
$tso_listrsStatusTotals1->addColumn("STATUS");
$tso_listrsStatusTotals1->addColumn("TOTAL");
$tso_listrsStatusTotals1->setDefault("STATUS");
$tso_listrsStatusTotals1->Execute();

// Navigation
$nav_listrsStatusTotals1 = new NAV_Regular("nav_listrsStatusTotals1", "rsStatusTotals", "../../", $_SERVER['PHP_SELF'], 300);

// Begin List Recordset
$maxRows_rsStatusTotals = $_SESSION['max_rows_nav_listrsStatusTotals1'];
$pageNum_rsStatusTotals = 0;
if (isset($_GET['pageNum_rsStatusTotals'])) {
  $pageNum_rsStatusTotals = $_GET['pageNum_rsStatusTotals'];
}
$startRow_rsStatusTotals = $pageNum_rsStatusTotals * $maxRows_rsStatusTotals;
$colname__rsStatusTotals = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsStatusTotals = $_SESSION['BOULENAME'];
}
// Defining List Recordset variable
$NXTFilter__rsStatusTotals = "1=1";
if (isset($_SESSION['filter_tfi_listrsStatusTotals1'])) {
  $NXTFilter__rsStatusTotals = $_SESSION['filter_tfi_listrsStatusTotals1'];
}
// Defining List Recordset variable
$NXTSort__rsStatusTotals = "STATUS";
if (isset($_SESSION['sorter_tso_listrsStatusTotals1'])) {
  $NXTSort__rsStatusTotals = $_SESSION['sorter_tso_listrsStatusTotals1'];
}
$query_rsStatusTotals = sprintf("SELECT MAX(vw_gramm_archons.`STATUS`) AS STATUS,   COUNT(vw_gramm_archons.`STATUS`) AS TOTAL FROM vw_gramm_archons WHERE vw_gramm_archons.STATUS <> 'Deceased' AND vw_gramm_archons.BOULENAME = %s AND  {$NXTFilter__rsStatusTotals} GROUP BY STATUS ORDER BY {$NXTSort__rsStatusTotals} ", GetSQLValueString($colname__rsStatusTotals, "text"));
$rsStatusTotals = $sigma_modx->SelectLimit($query_rsStatusTotals, $maxRows_rsStatusTotals, $startRow_rsStatusTotals) or die($sigma_modx->ErrorMsg());
if (isset($_GET['totalRows_rsStatusTotals'])) {
  $totalRows_rsStatusTotals = $_GET['totalRows_rsStatusTotals'];
} else {
  $all_rsStatusTotals = $sigma_modx->SelectLimit($query_rsStatusTotals) or die($sigma_modx->ErrorMsg());
  $totalRows_rsStatusTotals = $all_rsStatusTotals->RecordCount();
}
$totalPages_rsStatusTotals = (int)(($totalRows_rsStatusTotals-1)/$maxRows_rsStatusTotals);
// End List Recordset

// begin Recordset
$colname__master1vw_gramm_archons = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__master1vw_gramm_archons = $_SESSION['BOULENAME'];
}
$query_master1vw_gramm_archons = sprintf("SELECT * FROM vw_gramm_archons WHERE BOULENAME = %s AND STATUS IS NULL ORDER BY STATUS", GetSQLValueString($colname__master1vw_gramm_archons, "text"));
$master1vw_gramm_archons = $sigma_modx->SelectLimit($query_master1vw_gramm_archons) or die($sigma_modx->ErrorMsg());
$totalRows_master1vw_gramm_archons = $master1vw_gramm_archons->RecordCount();
// end Recordset

// begin Recordset
$colname__detail2vw_gramm_archons = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__detail2vw_gramm_archons = $_SESSION['BOULENAME'];
}
$query_detail2vw_gramm_archons = sprintf("SELECT * FROM vw_gramm_archons WHERE STATUS <> 'Deceased' AND BOULENAME = %s ORDER BY FULLNAME", GetSQLValueString($colname__detail2vw_gramm_archons, "text"));
$detail2vw_gramm_archons = $sigma_modx->SelectLimit($query_detail2vw_gramm_archons) or die($sigma_modx->ErrorMsg());
$totalRows_detail2vw_gramm_archons = $detail2vw_gramm_archons->RecordCount();
// end Recordset

// begin Recordset
$colname__rsOccupTotals = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsOccupTotals = $_SESSION['BOULENAME'];
}
$query_rsOccupTotals = sprintf("SELECT MAX(TRIM(spp_archons.OCCUPATIONCD)) AS OCCUPATION,   COUNT(TRIM(spp_archons.OCCUPATIONCD)) AS TOTAL FROM spp_archons WHERE (spp_archons.CHAPTERID = %s) AND (spp_archons.OCCUPATIONCD IS NOT NULL) AND (spp_archons.OCCUPATIONCD <> '') GROUP BY spp_archons.OCCUPATIONCD ORDER BY OCCUPATION ASC", GetSQLValueString($colname__rsOccupTotals, "text"));
$rsOccupTotals = $sigma_modx->SelectLimit($query_rsOccupTotals) or die($sigma_modx->ErrorMsg());
$totalRows_rsOccupTotals = $rsOccupTotals->RecordCount();
// end Recordset

// begin Recordset
$colname__rsOccArchons = '-1';
if (isset($_SESSION['BOULENAME'])) {
  $colname__rsOccArchons = $_SESSION['BOULENAME'];
}
$query_rsOccArchons = sprintf("SELECT * FROM spp_archons WHERE CHAPTERID = %s AND OCCUPATIONCD <> '''' AND OCCUPATIONCD IS NOT NULL", GetSQLValueString($colname__rsOccArchons, "text"));
$rsOccArchons = $sigma_modx->SelectLimit($query_rsOccArchons) or die($sigma_modx->ErrorMsg());
$totalRows_rsOccArchons = $rsOccArchons->RecordCount();
// end Recordset

$nav_listrsStatusTotals1->checkBoundries();

//PHP ADODB document - made with PHAkt 3.7.1
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php //PHP ADODB document - made with PHAkt 3.7.1?>
<html xmlns="http://www.w3.org/1999/xhtml" ><!-- InstanceBegin template="/Templates/base-nextensio.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/home/iakt/Templates/base_stylesheet.php"); ?>
<!-- InstanceBeginEditable name="head" --> 
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: false,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: false,
  record_counter: false
}
</script>
<style type="text/css">
  /* NeXTensio3 List row settings */
  .KT_col_STATUS {width:140px; overflow:hidden;}
  .KT_col_TOTAL {width:140px; overflow:hidden;}
</style>
<!-- InstanceEndEditable -->
</head>
<body>
<!-- InstanceBeginEditable name="content" -->

<div class="KT_tng" id="listrsStatusTotals1">
  <h1><?php echo strtoupper($_SESSION['BOULENAME']); ?> BOUL&Eacute; BY STATUS</h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
	<p class="style28">Below is the status list for <?php echo $_SESSION['BOULENAME']; ?> Boul&eacute; with total number of archons. Click a link below to see a detailed list of archons in each category.</p>
	<p class="style29">{{dto_list_status}}</p>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th>&nbsp;</th>
            <th id="STATUS" class="KT_sorter KT_col_STATUS <?php echo $tso_listrsStatusTotals1->getSortIcon('STATUS'); ?>"> <a href="<?php echo $tso_listrsStatusTotals1->getSortLink('STATUS'); ?>">STATUS</a> </th>
            <th id="TOTAL" class="KT_sorter KT_col_TOTAL <?php echo $tso_listrsStatusTotals1->getSortIcon('TOTAL'); ?>"> <a href="<?php echo $tso_listrsStatusTotals1->getSortLink('TOTAL'); ?>">TOTAL</a> </th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php if ($totalRows_rsStatusTotals == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="4"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsStatusTotals > 0) { // Show if recordset not empty ?>
            <?php
  while (!$rsStatusTotals->EOF) { 
?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>" >
                <td><input type="hidden" name="STATUS" class="id_field" value="<?php echo $rsStatusTotals->Fields('STATUS'); ?>" />                </td>
                <td><div class="style27"><?php echo strtoupper($rsStatusTotals->Fields('STATUS')); ?></div></td>
                <td><div class="KT_col_TOTAL"><span class="style27"><?php echo KT_FormatForList($rsStatusTotals->Fields('TOTAL'), 20); ?></span></div></td>
                <td>&nbsp;</td>
              </tr>
              <?php
    $rsStatusTotals->MoveNext(); 
  }
?>
               <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td>&nbsp;</td>
                <td style="border-top:#000000 thin solid"><span class="style27"><strong>TOTAL ARCHONS</strong></span> </td>
                <td style="border-top:#000000 thin solid"><span class="style27"><strong><?php echo $totalRows_detail2vw_gramm_archons ?></strong></span> </td>
                <td>&nbsp;</td>
              </tr>
           <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
  </form>
  
  <div class="KT_tng" style="margin-top: 24px;">
<h1> <?php echo strtoupper($_SESSION['BOULENAME']); ?> BOUL&Eacute; ARCHON OCCUPATIONS</h1>
  <div class="KT_tnglist">

<form>
	<p class="style28">Below is the list of archon occupations for <?php echo $_SESSION['BOULENAME']; ?> Boul&eacute; with total number of archons per category. </p>

    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
  <tr class="KT_row_order">
 	<th class="KT_sorter">&nbsp;</th>
   	<th class="KT_sorter"><div class="style27" style="color: white;">OCCUPATION</div></th>
    <th class="KT_sorter"><div class="style27" style="color: white;">TOTAL</div></th>
 	<th class="KT_sorter">&nbsp;</th>
  </tr>
  </thead>
<tbody>
    <?php
  while (!$rsOccupTotals->EOF) { 
?>

  <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>" >
 	<td>&nbsp;</td>
      <td><div class="style27"><?php echo $rsOccupTotals->Fields('OCCUPATION'); ?></div></td>
      <td><div class="style27"><?php echo KT_FormatForList($rsOccupTotals->Fields('TOTAL'),20); ?></div></td>
 	<td>&nbsp;</td>
</tr>
      <?php
    $rsOccupTotals->MoveNext(); 
  }
?>
  <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>" >
    <td>&nbsp;</td>
    <td style="border-top:#000000 thin solid"><div class="style27"><strong>PERCENTAGE ARCHONS REPORTING</strong>:</div></td>
    <td style="border-top:#000000 thin solid"><div class="style27"><strong>
	<?php  
	
	$total_rptng =  $totalRows_rsOccArchons;
	$total_archons = $totalRows_detail2vw_gramm_archons;
	$pct_rptng = ($total_rptng/$total_archons)*100;
	echo $pct_rptng."%";
	
	?> </strong></div></td>
    <td>&nbsp;</td>
  </tr>
  <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>" >
    <td>&nbsp;</td>
    <td style="border-top:#000000 thin solid"><div class="style27">GOAL:</div></td>
    <td style="border-top:#000000 thin solid"><div class="style27">100%</div></td>
    <td>&nbsp;</td>
  </tr>
</tbody>
</table>
</form>
</div>
</div>	

	
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<?php
$rsStatusTotals->Close();

$rsStatusTotals->Close();

$master1vw_gramm_archons->Close();

$detail2vw_gramm_archons->Close();

$rsOccupTotals->Close();

$rsOccArchons->Close();
?>
