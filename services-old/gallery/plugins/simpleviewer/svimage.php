<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
/**
 * SvImage
 *
 * @package svManager
 */
class SvImage extends Image
{
 /**
  * @var string thumb url according to path scheme
  */
  var $thumbPathRelGallery = '';
  
 /**
  * @var string image caption (without CDATA tags?)
  */
  var $caption = SV_IMAGE_CAPTION;
  
 /**
  * constructs SvImage class
  *
  * @param string  gallery path rel svm passed by reference in case it changes
  * @param string image path rel gallery
  * @param string thumb path rel gallery
  * @param string caption
  */
  function SvImage(&$galleryPathRelSvm, $imagePath, $thumbPath, $caption='')
  {
    $this->galleryPathRelSvm = &$galleryPathRelSvm;
    $this->imagePathRelGallery = $imagePath;
    $this->thumbPathRelGallery = $thumbPath;
    if ($caption != '') $this->caption = $caption;
  }
  
 /**
  * get caption
  *
  * @access public
  * @return string file name
  */
  function getCaption()
  {
    return $this->caption;
  }
  
 /**
  * Set image caption
  *
  * @access public
  * @return void
  * @param string caption text
  * @param string acceptable html tags
  */
  function setCaption($caption, $i, $okTags)
  {
    $caption = strip_tags($caption, $okTags);
    $caption = str_replace('%', '%%', $caption);
    $caption = str_replace('%%1$', '%1$', $caption);
    $caption = str_replace('%%2$', '%2$', $caption);
    $caption = str_replace('{count}', '%1$s', $caption);
    $caption = str_replace('{file}', '%2$s', $caption);
    $this->caption = sprintf($caption, $i+1, $this->getCleanImageFileName());
  }
}
?>
