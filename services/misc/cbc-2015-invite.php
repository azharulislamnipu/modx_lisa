<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("fullname", true, "text", "", "", "", "");
$formValidation->addField("num_rsvps", true, "numeric", "", "", "", "");
$formValidation->addField("CHAPTERNAME", true, "text", "", "", "", "");
$formValidation->addField("address", true, "text", "", "", "", "");
$formValidation->addField("city", true, "text", "", "", "", "");
$formValidation->addField("st_cd", true, "text", "", "", "", "");
$formValidation->addField("email", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("do-not-reply@sigmapiphi.org");
  $emailObj->setTo("{email}");
  $emailObj->setCC("{email}");
  $emailObj->setBCC("sigmapiphi.webmaster@gmail.com,nikita.richardson@sigmapiphi.org");
  $emailObj->setSubject("Invitation Waiting List Request: Joint Reception During Congressional Black Caucus Annual Conference");
  //WriteContent method
  $emailObj->setContent("<p>Invitations are for Boul� members and Archousai (or guests) only.</p>\n<p>You will be notified at this email address if your request is confirmed.</p>\n<p>The following Archon has submitted a request to be placed on the waiting list to attend the Joint Reception during the Congressional Black Caucus Foundation Annual Conference:</p> \n\n<p>Number of Invitations Requested: {num_rsvps}</p>\n<P>Archon: {fullname}<br />\nArchousa/Guest: {guest_fullname}</P>\n<P>{CHAPTERNAME} Boul&#233;</P>\n<P>{address}<br />\n<P>{city}, {state_cd} {zip}<br />\n<P>{email}</P>\n\n<P>Date/Time Submitted: {rsvp_date}</P>\n\n\n");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsStates = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsStates = mysql_query($query_rsStates, $sigma_modx) or die(mysql_error());
$row_rsStates = mysql_fetch_assoc($rsStates);
$totalRows_rsStates = mysql_num_rows($rsStates);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsBoules = "SELECT BOULENAME, concat_ws('', BOULENAME, ' (', CITY, ', ', STATECD, ')') AS LOCATION, ORGCD FROM spp_boule ORDER BY ORGCD ASC";
$rsBoules = mysql_query($query_rsBoules, $sigma_modx) or die(mysql_error());
$row_rsBoules = mysql_fetch_assoc($rsBoules);
$totalRows_rsBoules = mysql_num_rows($rsBoules);

// Make an insert transaction instance
$ins_spp_rsvp_2 = new tNG_multipleInsert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_rsvp_2);
// Register triggers
$ins_spp_rsvp_2->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_rsvp_2->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_rsvp_2->registerTrigger("END", "Trigger_Default_Redirect", 99, "/services/misc/thank-you.php");
$ins_spp_rsvp_2->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_rsvp_2->setTable("spp_rsvp_2");
$ins_spp_rsvp_2->addColumn("rsvp_id", "NUMERIC_TYPE", "POST", "rsvp_id");
$ins_spp_rsvp_2->addColumn("fullname", "STRING_TYPE", "POST", "fullname");
$ins_spp_rsvp_2->addColumn("guest_fullname", "STRING_TYPE", "POST", "guest_fullname");
$ins_spp_rsvp_2->addColumn("num_rsvps", "NUMERIC_TYPE", "POST", "num_rsvps");
$ins_spp_rsvp_2->addColumn("CHAPTERNAME", "STRING_TYPE", "POST", "CHAPTERNAME");
$ins_spp_rsvp_2->addColumn("address", "STRING_TYPE", "POST", "address");
$ins_spp_rsvp_2->addColumn("city", "STRING_TYPE", "POST", "city");
$ins_spp_rsvp_2->addColumn("zip", "STRING_TYPE", "POST", "zip");
$ins_spp_rsvp_2->addColumn("state_cd", "STRING_TYPE", "POST", "state_cd");
$ins_spp_rsvp_2->addColumn("email", "STRING_TYPE", "POST", "email");
$ins_spp_rsvp_2->addColumn("event_name", "STRING_TYPE", "POST", "event_name", "2015 CBC Joint Reception");
$ins_spp_rsvp_2->addColumn("rsvp_date", "DATE_TYPE", "VALUE", "{NOW_DT}");
$ins_spp_rsvp_2->setPrimaryKey("rsvp_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_spp_rsvp_2 = new tNG_multipleUpdate($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_rsvp_2);
// Register triggers
$upd_spp_rsvp_2->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_rsvp_2->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_rsvp_2->registerTrigger("END", "Trigger_Default_Redirect", 99, "/services/misc/thank-you.php");
$upd_spp_rsvp_2->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$upd_spp_rsvp_2->setTable("spp_rsvp_2");
$upd_spp_rsvp_2->addColumn("rsvp_id", "NUMERIC_TYPE", "POST", "rsvp_id");
$upd_spp_rsvp_2->addColumn("fullname", "STRING_TYPE", "POST", "fullname");
$upd_spp_rsvp_2->addColumn("guest_fullname", "STRING_TYPE", "POST", "guest_fullname");
$upd_spp_rsvp_2->addColumn("num_rsvps", "NUMERIC_TYPE", "POST", "num_rsvps");
$upd_spp_rsvp_2->addColumn("CHAPTERNAME", "STRING_TYPE", "POST", "CHAPTERNAME");
$upd_spp_rsvp_2->addColumn("address", "STRING_TYPE", "POST", "address");
$upd_spp_rsvp_2->addColumn("city", "STRING_TYPE", "POST", "city");
$upd_spp_rsvp_2->addColumn("zip", "STRING_TYPE", "POST", "zip");
$upd_spp_rsvp_2->addColumn("state_cd", "STRING_TYPE", "POST", "state_cd");
$upd_spp_rsvp_2->addColumn("email", "STRING_TYPE", "POST", "email");
$upd_spp_rsvp_2->addColumn("event_name", "STRING_TYPE", "POST", "event_name");
$upd_spp_rsvp_2->addColumn("rsvp_date", "DATE_TYPE", "VALUE", "{NOW_DT}");
$upd_spp_rsvp_2->setPrimaryKey("rsvp_id", "NUMERIC_TYPE", "GET", "rsvp_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_rsvp_2 = $tNGs->getRecordset("spp_rsvp_2");
$row_rsspp_rsvp_2 = mysql_fetch_assoc($rsspp_rsvp_2);
$totalRows_rsspp_rsvp_2 = mysql_num_rows($rsspp_rsvp_2);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | RSVP for an Invitation to the Congressional Black Caucus Foundation Annual Conference Joint Reception</title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag --><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_default.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: false,
  show_as_grid: false,
  merge_down_value: false
}
</script>
<h1 class="yellow">Waiting List to Receive CBC Joint Reception Invitations</h1>
<div class="contentBlock">
<p style="width:475px;">Sigma Pi Phi Fraternity will co-sponsor with the Links and the Executive Leadership Council a Joint Reception during the Congressional Black Caucus Foundation Annual Conference on Thursday, September 17, 2015 from 4:30 pm to 6:30 pm. It will be held on the rooftop at the offices of Skadden Arps in Washington D.C. We have already extended invitations to officers and committee chairs and have a limited number of invitations remaining to offer other Archons and their Archousai or guests.</p>
<p style="width:475px;"><strong>Invitations will be awarded on a &#8220;first-come, first-served basis&#8221; from the  waiting list.</strong></p>
<p style="width:475px;">There is a limit of one guest per Archon, for a maximum of two (2) invitations (Archon + Guest). You do not need to request a reservation for the waiting list if you are attending this event as a result of another organization's invitation. </p>
<p style="width:475px;">Please complete and submit the form below in order to be placed on the waiting list.</p>

  <p>&nbsp;
    <?php
	echo $tNGs->getErrorMsg();
?>

      <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
        <?php $cnt1 = 0; ?>
        <?php do { ?>
          <?php $cnt1++; ?>
          <?php 
// Show IF Conditional region1 
if (@$totalRows_rsspp_rsvp_2 > 1) {
?>
            <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
            <?php } 
// endif Conditional region1
?>
            <table cellpadding="2" cellspacing="0" class="KT_tngtable">
              <tr>
                <td class="KT_th"><label for="fullname_<?php echo $cnt1; ?>">FULL NAME:</label></td>
                <td><input type="text" name="fullname_<?php echo $cnt1; ?>" id="fullname_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['fullname']); ?>" size="32" maxlength="50" />
                    <?php echo $tNGs->displayFieldHint("fullname");?> <?php echo $tNGs->displayFieldError("spp_rsvp_2", "fullname", $cnt1); ?> </td>
              </tr>
              <tr>
                <td class="KT_th"><label for="guest_fullname_<?php echo $cnt1; ?>">ARCHOUSA/GUEST FULL NAME:</label></td>
                <td><input type="text" name="guest_fullname_<?php echo $cnt1; ?>" id="guest_fullname_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['guest_fullname']); ?>" size="32" maxlength="50" />
                    <?php echo $tNGs->displayFieldHint("guest_fullname");?> <?php echo $tNGs->displayFieldError("spp_rsvp_2", "guest_fullname", $cnt1); ?> </td>
              </tr>
              <tr>
                <td class="KT_th"><label for="num_rsvps_<?php echo $cnt1; ?>">NUMBER OF RSVPSs:</label></td>
                <td><select name="num_rsvps_<?php echo $cnt1; ?>" id="num_rsvps_<?php echo $cnt1; ?>">
                    <option value="" >Choose one:</option>
                    <option value="1" <?php if (!(strcmp(1, KT_escapeAttribute($row_rsspp_rsvp_2['num_rsvps'])))) {echo "SELECTED";} ?>>1</option>
                    <option value="2" <?php if (!(strcmp(2, KT_escapeAttribute($row_rsspp_rsvp_2['num_rsvps'])))) {echo "SELECTED";} ?>>2</option>
                  </select>
                    <?php echo $tNGs->displayFieldError("spp_rsvp_2", "num_rsvps", $cnt1); ?> </td>
              </tr>
              <tr>
                <td class="KT_th"><label for="CHAPTERNAME_<?php echo $cnt1; ?>">BOUL&#201; NAME:</label></td>
                <td><select name="CHAPTERNAME_<?php echo $cnt1; ?>" id="CHAPTERNAME_<?php echo $cnt1; ?>">
                    <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                    <?php 
do {  
?>
                    <option value="<?php echo $row_rsBoules['BOULENAME']?>"<?php if (!(strcmp($row_rsBoules['BOULENAME'], $row_rsspp_rsvp_2['CHAPTERNAME']))) {echo "SELECTED";} ?>><?php echo $row_rsBoules['BOULENAME']?></option>
                    <?php
} while ($row_rsBoules = mysql_fetch_assoc($rsBoules));
  $rows = mysql_num_rows($rsBoules);
  if($rows > 0) {
      mysql_data_seek($rsBoules, 0);
	  $row_rsBoules = mysql_fetch_assoc($rsBoules);
  }
?>
                  </select>
                    <?php echo $tNGs->displayFieldError("spp_rsvp_2", "CHAPTERNAME", $cnt1); ?> </td>
              </tr>
              <tr>
                <td class="KT_th"><label for="address_<?php echo $cnt1; ?>">ADDRESS:</label></td>
                <td><input type="text" name="address_<?php echo $cnt1; ?>" id="address_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['address']); ?>" size="20" maxlength="20" />
                    <?php echo $tNGs->displayFieldHint("address");?> <?php echo $tNGs->displayFieldError("spp_rsvp_2", "address", $cnt1); ?> </td>
              </tr>
              <tr>
                <td class="KT_th"><label for="city_<?php echo $cnt1; ?>">CITY:</label></td>
                <td><input type="text" name="city_<?php echo $cnt1; ?>" id="city_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['city']); ?>" size="32" maxlength="50" />
                    <?php echo $tNGs->displayFieldHint("city");?> <?php echo $tNGs->displayFieldError("spp_rsvp_2", "city", $cnt1); ?> </td>
              </tr>
<tr>
                <td class="KT_th"><label for="state_cd_<?php echo $cnt1; ?>">STATE:</label></td>
                <td><select name="state_cd_<?php echo $cnt1; ?>" id="state_cd_<?php echo $cnt1; ?>">
                    <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
                    <?php 
do {  
?>
                    <option value="<?php echo $row_rsStates['st_cd']?>"<?php if (!(strcmp($row_rsStates['st_cd'], $row_rsspp_rsvp_2['state_cd']))) {echo "SELECTED";} ?>><?php echo $row_rsStates['st_nm']?></option>
                    <?php
} while ($row_rsStates = mysql_fetch_assoc($rsStates));
  $rows = mysql_num_rows($rsStates);
  if($rows > 0) {
      mysql_data_seek($rsStates, 0);
	  $row_rsStates = mysql_fetch_assoc($rsStates);
  }
?>
                  </select>
                    <?php echo $tNGs->displayFieldError("spp_rsvp_2", "state_cd", $cnt1); ?> </td>
              </tr>
              <tr>
                <td class="KT_th"><label for="zip_<?php echo $cnt1; ?>">ZIP:</label></td>
                <td><input type="text" name="zip_<?php echo $cnt1; ?>" id="zip_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['zip']); ?>" size="32" />
                    <?php echo $tNGs->displayFieldHint("zip");?> <?php echo $tNGs->displayFieldError("spp_rsvp_2", "zip", $cnt1); ?> </td>
              </tr>
              
              <tr>
                <td class="KT_th"><label for="email_<?php echo $cnt1; ?>">EMAIL:</label></td>
                <td><input type="text" name="email_<?php echo $cnt1; ?>" id="email_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['email']); ?>" size="32" maxlength="50" />
                    <?php echo $tNGs->displayFieldHint("email");?> <?php echo $tNGs->displayFieldError("spp_rsvp_2", "email", $cnt1); ?> </td>
              </tr>
            </table>
            <input type="hidden" name="rsvp_date_<?php echo $cnt1; ?>" id="rsvp_date_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['rsvp_date']); ?>" />
<input type="hidden" name="kt_pk_spp_rsvp_2_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['kt_pk_spp_rsvp_2']); ?>" />
<input type="hidden" name="event_name_<?php echo $cnt1; ?>" id="event_name_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsspp_rsvp_2['event_name']); ?>" />
<?php } while ($row_rsspp_rsvp_2 = mysql_fetch_assoc($rsspp_rsvp_2)); ?>
        <div class="KT_bottombuttons">
          <div>
            <?php 
      // Show IF Conditional region1
      if (@$_GET['rsvp_id'] == "") {
      ?>
              <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
              <?php 
      // else Conditional region1
      } else { ?>
              <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
              <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
              <?php }
      // endif Conditional region1
      ?>
            <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
          </div>
        </div>
      </form>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
<!-- Piwik -->
<a href="http://piwik.org" title="open source Google Analytics" onclick="window.open(this.href);return(false);">
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.sigmapiphi.org/piwik/" : "http://www.sigmapiphi.org/piwik/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
piwik_action_name = '';
piwik_idsite = 1;
piwik_url = pkBaseURL + "piwik.php";
piwik_log(piwik_action_name, piwik_idsite, piwik_url);
</script>
<object><noscript><p>open source Google Analytics <img src="http://www.sigmapiphi.org/piwik/piwik.php?idsite=1" style="border:0" alt=""/></p></noscript></object></a>
<!-- End Piwik Tag -->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsStates);

mysql_free_result($rsBoules);
?>
