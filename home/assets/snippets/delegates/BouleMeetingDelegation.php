<?php

/**
 * Logic for displaying the delegations attending a meeting
 * @author Pat Heard {http://fullahead.org}
 */
 
require_once('BouleMeetingBean.php'); 
 
class BouleMeetingDelegation {

    private $id = -1;	
    private $name = '';
    private $date = null;
    private $location = ''; 
    private $delegations = array(); 
    private $regions = array();
    private $messages = array();    

    public function __construct(array $args = null){

        // Set default arguments
        $args = array_merge(array(
            'id'           => -1,
            'name'         => '',
            'date'         => '',
            'location'     => '',
            'delegations'  => array()
        ), (is_array($args) ? $args : array()));

        $this->id = $args['id'];
        $this->name = $args['name'];
        $this->date = $args['date'];
        $this->location = $args['location'];
        $this->delegations = $args['delegations'];

    }
    
    /** Loads up the region counts from the delegations */
    public function setRegionCounts(){
        if(!empty($this->delegations)){
            foreach($this->delegations as $d){
                
                // If we've found a region that doesn't have counts yet, create the new region bean
                if(!isset($this->regions[$d->region])){
                    $this->regions[$d->region] = array(
                        'DEL_TOTAL'         => 0,
                        'DEL_APPT'          => 0,
                        'DEL_APPT_REMAIN'   => 0,
                        'ALT_TOTAL'         => 0,
                        'ALT_APPT'          => 0,
                        'ALT_APPT_REMAIN'   => 0, 
                        'DEL_CERT'          => 0,
                        'DEL_CERT_REMAIN'   => 0
                        
                    );
                }                
                $this->regions[$d->region]['DEL_TOTAL'] += $d->count_delegates;
                $this->regions[$d->region]['DEL_APPT'] += $d->count_delegates_appointed;
                $this->regions[$d->region]['DEL_APPT_REMAIN'] += $d->count_delegates_appointed_remain;
                
                $this->regions[$d->region]['ALT_TOTAL'] += $d->count_alternates;
                $this->regions[$d->region]['ALT_APPT'] += $d->count_alternates_appointed;
                $this->regions[$d->region]['ALT_APPT_REMAIN'] += $d->count_alternates_appointed_remain;
                
                $this->regions[$d->region]['DEL_CERT'] += $d->count_delegates_certified;
                $this->regions[$d->region]['DEL_CERT_REMAIN'] += $d->count_delegates_certified_remain;
                
            }
        }
    }
    
    public function isMessages() {
        return !empty($this->messages);
    }   

    public function clearMessages() {
        $this->messages = array();
    }         

    public function addMessage($msg) {
        if(!empty($msg)){
            $this->messages[] = $msg;
        }    
    }        

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __toString(){
        return
            ' id :' . $this->id .
            ' name :' . $this->name .
            ' date :' . $this->date .
            ' location :' . $this->location .
            ' delegations :' . print_r($this->delegations, true) .
            ' regions :' . print_r($this->regions, true);
    }  
}

?>
