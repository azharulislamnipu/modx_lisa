<?php
error_reporting(0);
/*if(!empty($_POST)){
    var_dump($_POST);
    die();
}*/
require_once('../../Connections/sigma_modx.php'); ?>
<?php
// Load the common classes
require_once('../../includes/common/KT_common.php');

// Load the tNG classes
require_once('../../includes/tng/tNG.inc.php');


// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("chnge_stat_to", true, "text", "", "", "", "");
$formValidation->addField("req_cmt", true, "text", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

//start Trigger_SendEmail trigger
//remove this line if you want to edit the code by hand
function Trigger_SendEmail(&$tNG) {
  $emailObj = new tNG_Email($tNG);
  $emailObj->setFrom("no-reply@sigmapiphi.org");
  $emailObj->setTo("tonny.joseph@sigmapiphi.org");
  $emailObj->setCC("{rsDataManager.WEB_EMAIL}");
  $emailObj->setBCC("webmaster@sigmapiphi.org");
  $emailObj->setSubject("STATUS CHANGE REQUEST | {rsArchons.BOULENAME}");
  //WriteContent method
  $emailObj->setContent("<p>The following boul� wishes to change an archon's status:</p>\n<table border=\"0\">\n<tr valign=\"top\"><td>BOUL�:</td>\n<td>{rsArchons.BOULENAME}</td></tr>\n<tr valign=\"top\"><td>ARCHON:</td>\n<td>{rsArchons.L2R_FULLNAME}</td></tr>\n<tr valign=\"top\"><td>REQUESTED STATUS:</td>\n<td>{chnge_stat_to}</td></tr>\n<tr valign=\"top\"><td>CUSTOMERCD:</td>\n<td>{archon_ref_id}</td></tr>\n<tr valign=\"top\"><td>COMMENT:</td>\n<td>{req_cmt}</td></tr>\n<tr valign=\"top\"><td>SUBMITTED BY:</td>\n<td>{rsDataManager.L2R_FULLNAME}, Grammateus, {rsArchons.BOULENAME} Boul�</td></tr>\n</table>");
  $emailObj->setEncoding("UTF-8");
  $emailObj->setFormat("HTML/Text");
  $emailObj->setImportance("Normal");
  var_dump($emailObj);
  die();
  return $emailObj->Execute();
}
//end Trigger_SendEmail trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
	global $sigma_modx;
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

	$theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($sigma_modx, $theValue) : mysqli_escape_string($sigma_modx, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
	global $sigma_modx;
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

	$theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($sigma_modx, $theValue) : mysqli_escape_string($sigma_modx, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_rsArchons = "-1";
if (isset($_GET['CUSTOMERID'])) {
  $colname_rsArchons = $_GET['CUSTOMERID'];
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsArchons = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s ORDER BY FULLNAME ASC", GetSQLValueString($colname_rsArchons, "text"));
$rsArchons = mysqli_query($sigma_modx, $query_rsArchons) or die(mysqli_error($sigma_modx));
$row_rsArchons = mysqli_fetch_assoc($rsArchons);
$totalRows_rsArchons = mysqli_num_rows($rsArchons);

$colname_rsDataManager = "-1";
if (isset($_GET['UPDATEDBY'])) {
  $colname_rsDataManager = $_GET['UPDATEDBY'];
}
mysqli_select_db($sigma_modx, $database_sigma_modx);
$query_rsDataManager = sprintf("SELECT * FROM vw_gramm_archons WHERE CUSTOMERID = %s", GetSQLValueString($colname_rsDataManager, "int"));
$rsDataManager = mysqli_query($sigma_modx, $query_rsDataManager) or die(mysqli_error($sigma_modx));
$row_rsDataManager = mysqli_fetch_assoc($rsDataManager);
$totalRows_rsDataManager = mysqli_num_rows($rsDataManager);

// Make an insert transaction instance
$ins_spp_request = new tNG_insert($conn_sigma_modx);
$tNGs->addTransaction($ins_spp_request);
// Register triggers
$ins_spp_request->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_spp_request->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_spp_request->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archon-management-update.php?BOULENAME={rsArchon.CHAPTERID}");
$ins_spp_request->registerTrigger("AFTER", "Trigger_SendEmail", 98);
// Add columns
$ins_spp_request->setTable("spp_request");
$ins_spp_request->addColumn("archon_ref_id", "NUMERIC_TYPE", "POST", "archon_ref_id", "");
$ins_spp_request->addColumn("chnge_stat_to", "STRING_TYPE", "POST", "chnge_stat_to");
$ins_spp_request->addColumn("req_cmt", "STRING_TYPE", "POST", "req_cmt");
$ins_spp_request->addColumn("req_by", "NUMERIC_TYPE", "POST", "req_by", "{GET.webInternalKey}");
$ins_spp_request->addColumn("type_id", "NUMERIC_TYPE", "POST", "type_id", "1");
$ins_spp_request->addColumn("req_date", "DATE_TYPE", "POST", "req_date", "{NOW_DT}");
$ins_spp_request->addColumn("last_update", "DATE_TYPE", "POST", "last_update", "{now_dt}");
$ins_spp_request->setPrimaryKey("req_id", "NUMERIC_TYPE");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_request = $tNGs->getRecordset("spp_request");
$row_rsspp_request = mysqli_fetch_assoc($rsspp_request);
$totalRows_rsspp_request = mysqli_num_rows($rsspp_request);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEditableHeadTag -->
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<link href="../../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../../includes/common/js/base.js" type="text/javascript"></script>
<script src="../../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../../includes/skins/style.js" type="text/javascript"></script><!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav_data_man.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">STATUS CHANGE REQUEST</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td colspan="2" style="padding:6px;"><p style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">Complete the information below in order  to request a change in an Archon's Grand Boul&eacute; status. This request will be forwarded to the Office of the Grand Boul&eacute;. for processing.</p></td>
        </tr>
      <tr>
        <td class="KT_th"><label for="archon_ref_id">ARCHON:</label></td>
        <td><p style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;"><strong><?php echo $row_rsArchons['L2R_FULLNAME']; ?></strong></p></td>
      </tr>
      <tr>
        <td class="KT_th"><label for="archon_ref_id">CURRENT STATUS</label></td>
        <td><p style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;"><?php echo $row_rsArchons['STATUS']; ?></p></td>
      </tr>
      <tr>
        <td class="KT_th"><label for="chnge_stat_to_1">REQUESTED STATUS:</label><br />
Please select the correct status for this Archon.</td>
        <td><div style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">
            <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_request['chnge_stat_to']),"Active"))) {echo "CHECKED";} ?> type="radio" name="chnge_stat_to" id="chnge_stat_to_1" value="Active" />
            <label for="chnge_stat_to_1">Active</label>
          </div>
            <div style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">
              <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_request['chnge_stat_to']),"Deceased"))) {echo "CHECKED";} ?> type="radio" name="chnge_stat_to" id="chnge_stat_to_2" value="Deceased" />
              <label for="chnge_stat_to_2">Deceased</label>
            </div>
          <div style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">
              <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_request['chnge_stat_to']),"Delinquent"))) {echo "CHECKED";} ?> type="radio" name="chnge_stat_to" id="chnge_stat_to_3" value="Delinquent" />
              <label for="chnge_stat_to_3">Delinquent</label>
            </div>
          <div style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">
              <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_request['chnge_stat_to']),"Dropped"))) {echo "CHECKED";} ?> type="radio" name="chnge_stat_to" id="chnge_stat_to_4" value="Dropped" />
              <label for="chnge_stat_to_4">Dropped</label>
            </div>
          <div style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">
              <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_request['chnge_stat_to']),"Emeritus"))) {echo "CHECKED";} ?> type="radio" name="chnge_stat_to" id="chnge_stat_to_5" value="Emeritus" />
              <label for="chnge_stat_to_5">Emeritus</label>
            </div>
          <div style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">
              <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_request['chnge_stat_to']),"Involuntary Inactive"))) {echo "CHECKED";} ?> type="radio" name="chnge_stat_to" id="chnge_stat_to_6" value="Involuntary Inactive" />
              <label for="chnge_stat_to_6">Involuntary Inactive</label>
             (You will be asked if the related paper work has been submitted.)                                    </div>
          <div style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">
              <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_request['chnge_stat_to']),"Voluntary Inactive"))) {echo "CHECKED";} ?> type="radio" name="chnge_stat_to" id="chnge_stat_to_7" value="Voluntary Inactive" />
              <label for="chnge_stat_to_7">Voluntary Inactive</label>
             (You will be asked if the related paper work has been submitted.)                                    </div>
          <div style="font-size: 14px; margin-bottom: 6px;; margin-right:200px;">
              <input <?php if (!(strcmp(KT_escapeAttribute($row_rsspp_request['chnge_stat_to']),"Widow"))) {echo "CHECKED";} ?> type="radio" name="chnge_stat_to" id="chnge_stat_to_8" value="Widow" />
              <label for="chnge_stat_to_8">Widow</label>
            (If the Archon has passed and leaves a widow, please select this status and list her name below.)</div>
          <?php echo $tNGs->displayFieldError("spp_request", "chnge_stat_to"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="req_cmt">REQUEST/COMMENT:</label><br />
<br />
Please explain the status change in the text box.</td>
        <td><textarea name="req_cmt" id="req_cmt" cols="80" rows="8" style="font-size: 14px;;"><?php echo KT_escapeAttribute($row_rsspp_request['req_cmt']); ?></textarea>
            <?php echo $tNGs->displayFieldHint("req_cmt");?> <?php echo $tNGs->displayFieldError("spp_request", "req_cmt"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Insert1" id="KT_Insert1" value="SUBMIT REQUEST" /> <input type="button" name="KT_Cancel1" value="CANCEL" onclick="return UNI_navigateCancel(event, '/home/data-manager.php?BOULENAME=<?php echo $row_rsArchon['CHAPTERID']; ?>')" /></td>
      </tr>
    </table>
    <input type="hidden" name="req_by" id="req_by" value="<?php echo KT_escapeAttribute($_GET['UPDATEDBY']); ?>" />
    <input type="hidden" name="type_id" id="type_id" value="<?php echo KT_escapeAttribute($row_rsspp_request['type_id']); ?>" />
    <input type="hidden" name="req_date" id="req_date" value="<?php echo KT_formatDate($row_rsspp_request['req_date']); ?>" />
    <input type="hidden" name="last_update" id="last_update" value="<?php echo KT_formatDate($row_rsspp_request['last_update']); ?>" />
    <input name="archon_ref_id" type="hidden" id="archon_ref_id" value="<?php echo KT_escapeAttribute($_GET['CUSTOMERID']); ?>" />
    <input name="DM_EMAIL" type="hidden" id="DM_EMAIL" value="<?php echo $_GET['DM_EMAIL']; ?>" />
  </form>
  
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsArchons);

mysql_free_result($rsDataManager);

// mysql_free_result($rsArchons);
?>
