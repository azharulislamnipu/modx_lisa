<?php
/**
 * Part of Airtight Interactive gallery management package.
 *
 * AutoViewer, PostcardViewer, SimpleViewer and TiltViewer are the free, customizable Flash image viewing applications from {@link http://www.airtightinteractive.com/viewers/}
 * Use svManager to upload images, create thumbnails, sort images, change gallery appearance and add captions and links
 *
 * @package svManager
 * @author Jack Hardie {@link http://www.jhardie.com}
 * @version 1.7.7 build 110429
 * @copyright Copyright (c) 2007 - 2011, Airtight Interactive
 */
 
  //GALLERY DEFAULT SETTINGS
  
  // image sort order
  define ('AV_SORT_ORDER', 'dragndrop');
  // image path as in gallery.xml with trailing separator
  define ('AV_GALLERY_TITLE', 'New AutoViewer Gallery');
  // default image caption
  define ('AV_IMAGE_CAPTION', '');
  // default image frame color
  define ('AV_FRAME_COLOR', '0xFFFFFF');
  // default image frame width in pixels
  define ('AV_FRAME_WIDTH', 10);
  // default distance between images in pixels
  define ('AV_IMAGE_PADDING', 20);
  // default display time for each image in seconds
  define ('AV_DISPLAY_TIME', 6);
  // default show right click menu option
  define ('AV_ENABLE_RIGHT_CLICK_OPEN', 'true');
  // language options
  define ('AV_LANG_OPEN_IMAGE', 'Open Image in New Window');
	define ('AV_LANG_ABOUT', 'About');
  
  // OTHER SETTINGS
  // Specify type of urls to be stored in the gallery xml for new galleries
  // Options are 'RELATIVE' (default) for relative urls (images/myimage.jpg)
  // or 'ABSOLUTE' for absolute urls (/svmanager/g1/images/myimage.jpg)
  // or 'HTTP' for http urls (http://www.myserver.com/svmanager/g1/images/myimage.jpg)
  define('AV_URL_TYPE', 'RELATIVE');
  define('AV_BACKGROUND_COLOR', '0x181818');
  define('AV_MAX_IMAGE_WIDTH', 640);
  define('AV_MAX_IMAGE_HEIGHT', 480);
  // Url of gallery xml file relative to gallery folder
  define('AV_XML_PATH_REL_GALLERY', 'gallery.xml');
  define('AV_XML_SETTINGS_TAG', 'gallery');
  define('AV_MASTER_PATH', 'plugins'.DIRECTORY_SEPARATOR.'autoviewer'.DIRECTORY_SEPARATOR.'avmaster'.DIRECTORY_SEPARATOR);
  define('AV_MASTER_FILECOUNT', 4);
  define('AV_DEFAULT_THUMB_PATH', 'thumbs'.DIRECTORY_SEPARATOR);
  define('AV_DEFAULT_IMAGE_PATH', 'images'.DIRECTORY_SEPARATOR);
  define('AV_DEFAULT_SWF', 'autoviewer.swf');
  define('AV_THUMB_WIDTH', 65);
  define('AV_THUMB_HEIGHT', 65);
  define('AV_THUMB_QUALITY', 85);
  define('AV_CREATE_NEW', true);
?>