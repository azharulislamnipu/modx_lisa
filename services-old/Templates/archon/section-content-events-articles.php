<section id="events" style="display:visible;">
        <div class="width padding">
            <div class="row grid-12">
                <div class="span-4">
                    <div class="inner">
 						{{section-content-events}}
                    </div>
                </div>
                <div class="span-4">
                    <div class="inner">
						{{section-content-articles}}
                    </div>
                </div>
                <div class="span-4">
                    <div class="inner">
						{{section-content-articles}}
                    </div>
                </div>
            </div>
        </div>
    </section>
