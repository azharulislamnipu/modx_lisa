<tr valign="top">
	<td bgcolor="F4F4F4">
		<script language="JavaScript">
            function edit_[+CUSTOMERID+](){
                document.forms.form_[+CUSTOMERID+].submit();
            }
		</script>
		<form name="form_[+CUSTOMERID+]" action="/services/grammateus/member/archons_edit.php" method="get">
			<input type="hidden" name="CUSTOMERID" value="[+CUSTOMERID+]">
			<input type="hidden" name="GRAMMID" value="[+WEB_ID+]">
			<input type="hidden" name="UPDATEDBY" value="[[svVars? &var=`webFullname`]]">
			<input type="hidden" name="BOULENAME" value="[+BOULENAME+]">
			<input type="hidden" name="REGIONNAME" value="[+REGIONNAME+]">
		</form>
		<a href="#" rel="[+WEB_ID+]" onclick="edit_[+CUSTOMERID+]();return(FALSE);">[+FULLNAME+]</a>
		<div id="[+WEB_ID+]" class="balloonstyle">
			<strong>[+PREFIX+] [+L2R_FULLNAME+]</strong><br />
			[+ADDRESS1+]<br />
			[+CITY+], [+STATECD+] [+ZIP+]<br />
			HOME PHONE: [+HOMEPHONE+]<br />
			CELL PHONE: [+MOBILEPHONE+]<br />
			EMAIL: [+EMAIL+]<br /><br />
			BUSINESS PHONE: [+WORKPHONE+]<br />
			BUSINESS EMAIL: [+ALTEMAIL+]
		</div>


	</td>
	<td bgcolor="F4F4F4"><strong>[+STATUS+] {{showsEmeritus}}</strong><br />
		<span style="font-size: 10px;"><a href="/services/grammateus/member/status_request.php?CUSTOMERID=[+CUSTOMERID+]&amp;UPDATEDBY=[[svVars? &var=`webInternalKey`]]&amp;DM_EMAIL=[[svVars? &var=`webEmail`]]&amp;KT_back=1">Request Status Change</a></span></td>
</tr>
