<?php require_once('../Connections/sigma_modx.php'); ?>
<?php
//MX Widgets3 include
require_once('../includes/wdg/WDG.php');

// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_sigma_modx = new KT_connection($sigma_modx, $database_sigma_modx);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsOccup = "SELECT * FROM spp_occupation ORDER BY `DESCRIPTION` ASC";
$rsOccup = mysql_query($query_rsOccup, $sigma_modx) or die(mysql_error());
$row_rsOccup = mysql_fetch_assoc($rsOccup);
$totalRows_rsOccup = mysql_num_rows($rsOccup);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsTitle = "SELECT SKILLID, SKILLCD, `DESCRIPTION` FROM spp_skillref ORDER BY `DESCRIPTION` ASC";
$rsTitle = mysql_query($query_rsTitle, $sigma_modx) or die(mysql_error());
$row_rsTitle = mysql_fetch_assoc($rsTitle);
$totalRows_rsTitle = mysql_num_rows($rsTitle);

mysql_select_db($database_sigma_modx, $sigma_modx);
$query_rsState = "SELECT * FROM spp_state ORDER BY st_nm ASC";
$rsState = mysql_query($query_rsState, $sigma_modx) or die(mysql_error());
$row_rsState = mysql_fetch_assoc($rsState);
$totalRows_rsState = mysql_num_rows($rsState);

// Make an update transaction instance
$upd_spp_archousa = new tNG_update($conn_sigma_modx);
$tNGs->addTransaction($upd_spp_archousa);
// Register triggers
$upd_spp_archousa->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_spp_archousa->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_spp_archousa->registerTrigger("END", "Trigger_Default_Redirect", 99, "/home/archousa-profile.php");
// Add columns
$upd_spp_archousa->setTable("spp_archousa");
$upd_spp_archousa->addColumn("OCCUPATIONCD", "STRING_TYPE", "POST", "OCCUPATIONCD");
$upd_spp_archousa->addColumn("JOBTITLE", "STRING_TYPE", "POST", "JOBTITLE");
$upd_spp_archousa->addColumn("ORGNAME", "STRING_TYPE", "POST", "ORGNAME");
$upd_spp_archousa->addColumn("ALTCITY", "STRING_TYPE", "POST", "ALTCITY");
$upd_spp_archousa->addColumn("ALTSTATE", "STRING_TYPE", "POST", "ALTSTATE");
$upd_spp_archousa->addColumn("ALTZIP", "STRING_TYPE", "POST", "ALTZIP");
$upd_spp_archousa->addColumn("WORKPHONE", "STRING_TYPE", "POST", "WORKPHONE");
$upd_spp_archousa->addColumn("ALTEMAIL", "STRING_TYPE", "POST", "ALTEMAIL");
$upd_spp_archousa->addColumn("UPDATEDBY", "STRING_TYPE", "POST", "UPDATEDBY");
$upd_spp_archousa->addColumn("LASTUPDATED", "DATE_TYPE", "POST", "LASTUPDATED");
$upd_spp_archousa->setPrimaryKey("ARCHOUSAID", "NUMERIC_TYPE", "POST", "ARCHOUSAID");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsspp_archousa = $tNGs->getRecordset("spp_archousa");
$row_rsspp_archousa = mysql_fetch_assoc($rsspp_archousa);
$totalRows_rsspp_archousa = mysql_num_rows($rsspp_archousa);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-EN" lang="en-EN"  dir="ltr" xmlns:wdg="http://ns.adobe.com/addt"><!-- InstanceBegin template="/Templates/spp_default.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Sigma Pi Phi Fraternity | </title>
<!-- InstanceEndEditable -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_meta.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_stylesheet.php"); ?>
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_scripts.php"); ?>

<!-- InstanceBeginEditable name="head" -->
<script>
$MXW_relPath = '../';
</script>
<!-- InstanceEditableHeadTag -->
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/common/js/sigslot_core.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/MXWidgets.js.php"></script>
<script type="text/javascript" src="../includes/wdg/classes/JSRecordset.js"></script>
<script type="text/javascript" src="../includes/wdg/classes/DynamicInput.js"></script><?php
//begin JSRecordset
$jsObject_rsTitle = new WDG_JsRecordset("rsTitle");
echo $jsObject_rsTitle->getOutput();
//end JSRecordset
?>
<?php
//begin JSRecordset
$jsObject_rsOccup = new WDG_JsRecordset("rsOccup");
echo $jsObject_rsOccup->getOutput();
//end JSRecordset
?>
<!-- InstanceEndEditable -->
</head>
<body>

<!-- #CONTENT:  -->
<div id="content">

<!-- #HEADER: Holds title, and logo -->
<div id="header">
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_header.php"); ?>
</div>
<!--  #END HEADER -->

<!-- #PAGE CONTENT BEGINS -->
<div id="page">

<!-- #SIDENAV: side navigation, logo and search box -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_sidenav.php"); ?>
<!-- #END SIDENAV -->

<!-- #MAIN COLUMN -->
<div class="floatLeft width804">
<!-- InstanceBeginEditable name="content" --><?php echo $tNGs->displayValidationRules();?>
<h1 class="yellow">Page Name</h1>
<div class="contentBlock">

  <?php
	echo $tNGs->getErrorMsg();
?>
  <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
    <table cellpadding="2" cellspacing="0" class="KT_tngtable">
      <tr>
        <td class="KT_th"><label for="OCCUPATIONCD">PROFESSION:</label></td>
        <td><select name="OCCUPATIONCD" id="OCCUPATIONCD" wdg:subtype="DynamicInput" wdg:type="widget" wdg:recordset="rsOccup" wdg:displayfield="DESCRIPTION" wdg:valuefield="DESCRIPTION" wdg:norec="50" wdg:singleclickselect="yes" wdg:defaultoptiontext="no" wdg:restrict="No" wdg:ext="php" wdg:fakeattribute="<?php WDG_registerRecordInsert("sigma_modx", "rsOccup", "DESCRIPTION", "DESCRIPTION");?>">
                                          </select>
            <?php echo $tNGs->displayFieldError("spp_archousa", "OCCUPATIONCD"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="JOBTITLE">TITLE:</label></td>
        <td><select name="JOBTITLE" id="JOBTITLE" wdg:subtype="DynamicInput" wdg:type="widget" wdg:recordset="rsTitle" wdg:displayfield="DESCRIPTION" wdg:valuefield="DESCRIPTION" wdg:norec="50" wdg:singleclickselect="yes" wdg:defaultoptiontext="no" wdg:restrict="No" wdg:ext="php" wdg:selected="<?php echo $row_rsspp_archousa['JOBTITLE'] ?>" wdg:fakeattribute="<?php WDG_registerRecordInsert("sigma_modx", "rsTitle", "DESCRIPTION", "DESCRIPTION");?>">
                          </select>
            <?php echo $tNGs->displayFieldError("spp_archousa", "JOBTITLE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ORGNAME">ORGANIZATION NAME:</label></td>
        <td><input type="text" name="ORGNAME" id="ORGNAME" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ORGNAME']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ORGNAME");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ORGNAME"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTCITY">CITY:</label></td>
        <td><input type="text" name="ALTCITY" id="ALTCITY" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ALTCITY']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTCITY");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ALTCITY"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTSTATE">STATE:</label></td>
        <td><select name="ALTSTATE" id="ALTSTATE">
            <?php
do {  
?><option value="<?php echo $row_rsState['st_cd']?>"<?php if (!(strcmp($row_rsState['st_cd'], $row_rsspp_archousa['ALTSTATE']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rsState['st_nm']?></option>
            <?php
} while ($row_rsState = mysql_fetch_assoc($rsState));
  $rows = mysql_num_rows($rsState);
  if($rows > 0) {
      mysql_data_seek($rsState, 0);
	  $row_rsState = mysql_fetch_assoc($rsState);
  }
?>
          </select>
            <?php echo $tNGs->displayFieldError("spp_archousa", "ALTSTATE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTZIP">ZIP:</label></td>
        <td><input type="text" name="ALTZIP" id="ALTZIP" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ALTZIP']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTZIP");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ALTZIP"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="WORKPHONE">WORK PHONE:</label></td>
        <td><input type="text" name="WORKPHONE" id="WORKPHONE" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['WORKPHONE']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("WORKPHONE");?> <?php echo $tNGs->displayFieldError("spp_archousa", "WORKPHONE"); ?> </td>
      </tr>
      <tr>
        <td class="KT_th"><label for="ALTEMAIL">WORK EMAIL:</label></td>
        <td><input type="text" name="ALTEMAIL" id="ALTEMAIL" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ALTEMAIL']); ?>" size="32" />
            <?php echo $tNGs->displayFieldHint("ALTEMAIL");?> <?php echo $tNGs->displayFieldError("spp_archousa", "ALTEMAIL"); ?> </td>
      </tr>
      <tr class="KT_buttons">
        <td colspan="2"><input type="submit" name="KT_Update1" id="KT_Update1" value="Update Employment" /> <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '/home/archousa-profile.php')" />
        </td>
      </tr>
    </table>
    <input name="ARCHOUSAID" type="hidden" id="ARCHOUSAID" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['ARCHOUSAID']); ?>" />
    <input type="hidden" name="UPDATEDBY" id="UPDATEDBY" value="<?php echo KT_escapeAttribute($row_rsspp_archousa['UPDATEDBY']); ?>" />
    <input type="hidden" name="LASTUPDATED" id="LASTUPDATED" value="<?php echo KT_formatDate($row_rsspp_archousa['LASTUPDATED']); ?>" />
  </form>
  <p>&nbsp;</p>
</div>
<!-- InstanceEndEditable -->
</div>
<!-- #END MAIN COLUMN -->

</div>
<!-- #PAGE CONTENT ENDS -->

</div>
<!-- #CONTENT ENDS -->

<!-- #FOOTER: Site copyright -->
<?php include ($_SERVER['DOCUMENT_ROOT']."/services/Templates/base_footer.php"); ?>
<!-- #END FOOTER-->
</body>
<!-- InstanceEnd --></html>
<?php
mysql_free_result($rsOccup);

mysql_free_result($rsTitle);

mysql_free_result($rsState);
?>
